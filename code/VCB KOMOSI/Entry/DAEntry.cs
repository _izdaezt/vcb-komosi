﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
namespace VCB_KOMOSI
{
    class DAEntry_Entry
    {
        // bool selecthipoint = true;      
        private string stringconnecttion = String.Format("Data Source=" + Program.server + ";Initial Catalog=VCB_KOMOSI_2017;User Id=entrykomosi;Password=entrykomosi;Integrated Security=no;MultipleActiveResultSets=False;Packet Size=4096;Pooling=false;Connection Timeout=10;");

        /// <summary>
        /// Set status = lock hay free cho record
        /// </summary>
        /// <param name="id">Id của image</param>
        /// <param name="status">lock: không được nhập/ free: được nhập</param>
        public void ReturnPairANDHitpointEntry(int ImageId, int pair)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "returnPairEntry";
                    sqlCommand.Parameters.Add("@ImageId", SqlDbType.Int).Value = ImageId;
                    sqlCommand.Parameters.Add("@pair", SqlDbType.Int).Value = pair;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Set lại hitpoint trong image khi chương trình bị tắt đột ngột
        /// </summary>
        /// <param name="id"></param>
        public void SetHitPointImageKetBatch(int id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE db_owner.Image SET HitPoint = CASE WHEN HitPoint <= 1 THEN 0 ELSE 1 END WHERE Id = " + id;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Set lại pair trong image khi chương trình bị tắt đột ngột
        /// </summary>
        /// <param name="id"></param>
        public void SetPairImageKetBatch(int id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE db_owner.Image SET Pair = 0, Identry = 0  WHERE Id = " + id;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Set hitpoint cho batch
        /// </summary>
        /// <param name="batchId">Id của Batch</param>
        /// <param name="hitPoint">0: chua entry het/ 1: da entry het/ 2: da check het</param>
        public void SetStatusQC(int imageid, int userid)
        {
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "UpdateStatusNG";
                sqlCommand.Parameters.Add("@imageid", SqlDbType.Int).Value = imageid;
                sqlCommand.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                con.Open();
                sqlCommand.ExecuteNonQuery();
            }
        }
        public int GetIdImageEntrySecond(string batchname, int template, int pair)
        {
            int id = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "INFUSER";
                    sqlCommand.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.Add("@Pair", SqlDbType.Int).Value = pair;
                    sqlCommand.Parameters.Add("@Bname", SqlDbType.NVarChar).Value = batchname;
                    sqlCommand.Parameters.Add("@template", SqlDbType.Int).Value = template;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                    id = int.Parse(sqlCommand.Parameters["@ID"].Value.ToString());
                }
            }
            catch
            {

            }
            return id;
        }

        public int GetIdImageE3(string tableform)
        {
            int id = 0;
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(stringconnecttion))
                    {
                        SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.Connection = con;
                        sqlCommand.CommandText = "select top 1 @ID = id from dbo.[" + tableform + "] where Content1_OCR is not null and Content3 is null and datediff(ms, InputDate3,GETDATE())>600000;" +
                            "Update dbo.[" + tableform + "]  set InputDate3  = GETDATE() where Id=@ID";
                        sqlCommand.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;
                        con.Open();
                        sqlCommand.ExecuteNonQuery();
                        id = Convert.ToInt32(sqlCommand.Parameters["@ID"].Value);
                    }
                }
                catch
                { id = 0; }
            }
            return id;
        }
        /// <summary>
        /// set hitpoint batch = 1 neu ton tai image chua nhap
        /// </summary>
        /// <param name="batchId"></param>
        public void SetHitpointBatch(string batchname, int template)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "SetHitpointBatch";
                    sqlCommand.Parameters.Add("@batchname", SqlDbType.NVarChar).Value = batchname;
                    sqlCommand.Parameters.Add("@Template", SqlDbType.NChar).Value = template;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            {

            }
        }
        /// <summary>
        /// Lấy đối tượng image để nhập content
        /// </summary>
        /// <param name="imageId">Id của image</param>
        /// <returns>Image</returns>
        public BOImage_Entry GetImage(int imageId, string tableform)
        {
            BOImage_Entry img = new BOImage_Entry();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT ImageName as 'PUrl',Content1_OCR,Content_OCR FROM dbo.[" + tableform + "] WHERE Id = " + imageId;
                    con.Open();
                    using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                        {
                            img.Id = imageId;
                            img.PageUrl = sqlDataReader["PUrl"].ToString();
                            img.Content1_OCR = sqlDataReader["Content1_OCR"].ToString().Split('|');
                            img.ContentOCR = sqlDataReader["Content_OCR"].ToString().Split('|');
                        }
                        sqlDataReader.Close();
                    }
                }
            }
            catch
            {

            }
            return img;
        }

        /// <summary>
        /// Insert or Update field Content1 or Content2 to 1 record table image content
        /// </summary>
        /// <param name="imgContentImageId"></param>
        /// <param name="imgContentContent"></param>
        /// <param name="imgContentUserId"></param>
        /// <param name="imgContentInputDate"></param>
        /// <param name="imgContentTypeRate"></param>
        /// <param name="imgContentSelected"></param>
        /// <param name="imgContentState"></param>
        public void AddImageContent(string imgContentImageId, string imgContentContent, string imgContentUserId, string leng, string pair)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "AddImageContent";
                    sqlCommand.Parameters.Add("@imgContentImageId", SqlDbType.Int).Value = imgContentImageId;
                    sqlCommand.Parameters.Add("@imgContentContent", SqlDbType.NVarChar).Value = imgContentContent;
                    sqlCommand.Parameters.Add("@imgContentUserId", SqlDbType.Int).Value = imgContentUserId;
                    sqlCommand.Parameters.Add("@leng", SqlDbType.Int).Value = leng;
                    sqlCommand.Parameters.Add("@Pair", SqlDbType.NVarChar, 2).Value = pair;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            { }
        }
        /// <summary>
        /// Insert or Update field Content1 or Content2 to 1 record table image content when click Notgood
        /// </summary>
        /// <param name="imgContentImageId"></param>
        /// <param name="imgContentContent"></param>
        /// <param name="imgContentUserId"></param>
        /// <param name="imgContentInputDate"></param>
        /// <param name="imgContentTypeRate"></param>
        /// <param name="imgContentSelected"></param>
        /// <param name="imgContentState"></param>
        public void AddImageContentNG(string imgContentImageId, string imgContentContent, string imgContentUserId, string leng, string pair)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "AddImageContentNG";
                    sqlCommand.Parameters.Add("@imgContentImageId", SqlDbType.Int).Value = imgContentImageId;
                    sqlCommand.Parameters.Add("@imgContentContent", SqlDbType.NVarChar).Value = imgContentContent;
                    sqlCommand.Parameters.Add("@imgContentUserId", SqlDbType.Int).Value = imgContentUserId;
                    sqlCommand.Parameters.Add("@leng", SqlDbType.Int).Value = leng;
                    sqlCommand.Parameters.Add("@Pair", SqlDbType.NVarChar, 2).Value = pair;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch { }
        }

        /// <summary>
        /// Lấy thông tin level của user từ user id
        /// </summary>
        /// <param name="userId">id của user</param>
        /// <returns>level</returns>
        public int GetLevel(int userId)
        {
            int returnVal = 0;
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = "SELECT Lvl FROM db_owner.[User] WHERE Id = " + userId;
                con.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    returnVal = int.Parse(sqlDataReader["Lvl"].ToString().Trim());
                }
                sqlDataReader.Close();
            }
            return returnVal;
        }
        //Kiểm tra username,pass login
        public string password(string username)
        {
            string pass = "";
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = @"select Password from db_owner.[User] where UPPER(Name) = N'" + username + "'";
                con.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    pass = sqlDataReader["Password"].ToString().Trim();
                    break;
                }
                sqlDataReader.Close();
            }
            return pass;
        }
        //
        //update password
        //
        //update password
        public void Updatepassword(string pass, string name)
        {
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = "UPDATE db_owner.[User] SET Password = N'" + pass + "' where UPPER(Name) ='" + name + "'";
                con.Open();
                sqlCommand.ExecuteNonQuery();
            }
        }
        public string[] InsertPerformance(int batchId, int userid)
        {
            string[] id = null;
            id = new string[2];
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "InsertPerformance";
                    sqlCommand.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.Add("@StartTime", SqlDbType.DateTime).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.Add("@BatchID", SqlDbType.Int, 30).Value = batchId;
                    sqlCommand.Parameters.Add("@UserID", SqlDbType.Int, 10).Value = userid;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                    try
                    {
                        id[0] = sqlCommand.Parameters["@ID"].Value.ToString();
                        id[1] = sqlCommand.Parameters["@StartTime"].Value.ToString();
                    }
                    catch
                    {
                        id[0] = "";
                    }
                }
            }
            catch
            {

            }
            return id;
        }
        public bool UpdatePerformance(string starttime, int ID)
        {

            bool id = false;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "UpdatePerformance";
                    sqlCommand.Parameters.Add("@Starttime", SqlDbType.DateTime, 30).Value = starttime;
                    sqlCommand.Parameters.Add("@ID", SqlDbType.Int, 10).Value = ID;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                    id = true;
                }

            }
            catch
            {

            }
            return id;
        }
        /// <summary>
        /// Lấy thông tin số ảnh còn lại
        /// </summary>
        /// <param name="userId">id của user</param>
        /// <returns>level</returns>
        public int ImageExist(string formtable, int pair)
        {
            int id = 0;
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(stringconnecttion))
                    {
                        SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.Connection = con;
                        sqlCommand.CommandText = @"Select COUNT(Id) from dbo.[" + formtable + "] where Content" + pair + " is null";
                        con.Open();
                        id = (int)sqlCommand.ExecuteScalar();
                    }
                }
                catch { }
            }
            return id;
        }

        public DataTable select_MBD_ko_todoufuken(string mbd)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = @"select  (ISNULL([市区町村],'')+ISNULL([町域],'')+ISNULL([字丁目],'')) as 'kb' from dbo.[MBD-ko-todoufuken] where [郵便番号]=N'" + mbd + "'";

                    SqlDataAdapter da = new SqlDataAdapter();
                    con.Open();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }
            }
            catch
            {

            }
            return dt;

        }

        /// <summary>
        /// Get list SubField by FormTemplateId
        /// </summary>
        /// <param name="formTemplateId"></param>
        /// <returns></returns>
        public DataTable GetDSSubFieldByFormTemplateId(int formTemplateId)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = @"select  SubField.* from SubField join db_owner.Field on SubField.FieldId = Field.Id where FormId = " + formTemplateId;
                    SqlDataAdapter da = new SqlDataAdapter();
                    con.Open();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }
            }
            catch
            {

            }
            return dt;

        }

        /// <summary>
        /// Check co ton tai record image content ko
        /// </summary>
        /// <param name="imageId">id cua image</param>
        /// <returns>ton tai hay ko</returns>
        public byte[] getImageOnServer(string Name)
        {
            byte[] returnVal = null;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT BinaryImage FROM dbo.ServerImage WHERE NameImage = N'" + Name + "'";
                    sqlCommand.Parameters.Add("@NameImage", SqlDbType.NVarChar).Value = Name;
                    con.Open();
                    returnVal = (byte[])sqlCommand.ExecuteScalar();
                }
            }
            catch
            { }
            return returnVal;
        }
        public string[] GetbatchNew(int formid, int batchid)
        {
            string[] returnValue = new string[] { "", "" };
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = "SELECT CAST(Batch.Id as nvarchar(10))+'|'+Name FROM dbo.Batch WHERE Hitpoint=0 and Batch.id >" + batchid + " and TempID = " + formid;
                con.Open();
                if (sqlCommand.ExecuteScalar() != null)
                    returnValue = sqlCommand.ExecuteScalar().ToString().Split('|');
            }
            return returnValue;
        }
        public string[] GetbatchNewHCheck(int formid, int BatchId)
        {
            string[] returnValue = new string[] { "", "" };
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = "SELECT CAST(Batch.Id as nvarchar(10))+'|'+Name FROM dbo.Batch WHERE Hitpoint=1 and Batch.id >" + BatchId + " and TempID = " + formid;
                con.Open();
                if (sqlCommand.ExecuteScalar() != null)
                    returnValue = sqlCommand.ExecuteScalar().ToString().Split('|');
            }
            return returnValue;
        }
        public DataTable GetNameTemplate()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = "select Name,Id from db_owner.FormTemplate";
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlCommand;
                da.Fill(dt);
            }
            return dt;
        }
        public byte[] getImageOnServer(string Name, string nameTable)
        {
            byte[] returnVal = null;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT Binarymage FROM dbo.[" + nameTable + "] WHERE ImageName =@NameImage";
                    sqlCommand.Parameters.Add("@NameImage", SqlDbType.NVarChar).Value = Name;
                    con.Open();
                    returnVal = (byte[])sqlCommand.ExecuteScalar();
                    int ss = returnVal.Length;
                }
            }
            catch
            { }
            return returnVal;
        }
        public void Add_Content_LV3_Like(int imgContentImageId, string imgContentContent, int imgContentUserId, int leng, int time, int pair, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + table + "] SET Content" + pair + " = @Content, UserId"+ pair +"= " + imgContentUserId + ", InputDate" + pair + "=getdate(), Tongkytu" + pair + "= " + leng + ", TimeMiliseconds" + pair + "=" + time + ", Content1_OCR='', Result= @Content, ResultLC=@Content, Tongkytu= " + leng + ", Content3 = '' where Id = " + imgContentImageId;
                    sqlCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = imgContentContent;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Result\n" + sqlException.Message);
            }
        }

        public void Add_Content_LV3_ContainsSao(int imgContentImageId, string imgContentContent, int imgContentUserId, int leng, int time, int pair, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + table + "] SET Content" + pair + " = @Content, UserId" + pair + "= " + imgContentUserId + ", InputDate" + pair + "=getdate(), Tongkytu" + pair + "= " + leng + ", TimeMiliseconds" + pair + "=" + time + ", Content1_OCR='', Content3 = '', CheckedDate = NULL, QCDate = dateadd(MS, -6000000 , getdate()) where Id = " + imgContentImageId;
                    sqlCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = imgContentContent;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Result\n" + sqlException.Message);
            }
        }

        public void Add_Content_LV3_Unlike(int imgContentImageId, string imgContentContent, int imgContentUserId, int leng, int time, int pair, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + table + "] SET Content" + pair + " = @Content, UserId" + pair + "= " + imgContentUserId + ", InputDate" + pair + "=getdate(), Tongkytu" + pair + "= " + leng + ", TimeMiliseconds" + pair + "=" + time + ", Content1_OCR='', Content3 = '', CheckedDate = dateadd(MS, -6000000 , getdate()), QCDate=NULL where Id = " + imgContentImageId;
                    sqlCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = imgContentContent;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Result\n" + sqlException.Message);
            }
        }

        public void Add_Content_P1(int imgContentImageId, string imgContentContent, int imgContentUserId, int leng, int time, string imgContentContent1, string imgContentContent2)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "Add_Content_P1";
                    sqlCommand.Parameters.Add("@ImageId", SqlDbType.Int).Value = imgContentImageId;
                    sqlCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = imgContentContent;
                    sqlCommand.Parameters.Add("@Content1", SqlDbType.NVarChar).Value = imgContentContent1;
                    sqlCommand.Parameters.Add("@Content2", SqlDbType.NVarChar).Value = imgContentContent2;
                    sqlCommand.Parameters.Add("@UserId", SqlDbType.Int).Value = imgContentUserId;
                    sqlCommand.Parameters.Add("@leng", SqlDbType.Int).Value = leng;
                    sqlCommand.Parameters.Add("@time", SqlDbType.Int).Value = time;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            { }
        }
        public void Add_Content_OCR_Like(int imgContentImageId, string imgContentContent, int imgContentUserId, int leng, int time, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + table + "] SET Content1 ='',Content2 ='', UserId2=0, UserId1= " + imgContentUserId + ", InputDate1=getdate(), Tongkytu1= " + leng + ", TimeMiliseconds1=" + time + ", Content1_OCR=@Content, Result= @Content, ResultLC=@Content, Tongkytu= " + leng + ", Content3 = '' where Id = " + imgContentImageId;
                    sqlCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = imgContentContent;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Result\n" + sqlException.Message);
            }
        }

        public void Add_Content_OCR_UnLike(int imgContentImageId, string imgContentContent, int imgContentUserId, int leng, int time, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + table + "] SET Content1 ='',Content2 ='', UserId2=0, UserId1= " + imgContentUserId + ", InputDate1=getdate(), Tongkytu1= " + leng + ", TimeMiliseconds1=" + time + ", Content1_OCR=@Content, InputDate3 = dateadd(MS, -600000 , getdate()) where Id = " + imgContentImageId;
                    sqlCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = imgContentContent;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Result\n" + sqlException.Message);
            }
        }

        public void Add_Content_kojin_E3_Like(int imgContentImageId, string imgContentContent, int imgContentUserId, int leng, int time, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + table + "] SET Content3 =@Content, UserId3= " + imgContentUserId + ", InputDate3=getdate(), Tongkytu3= " + leng + ", TimeMiliseconds3= " + time + ", Result= @Content, ResultLC=@Content, Tongkytu= " + leng + ", CheckedDate = NULL, QCDate=NULL where Id = " + imgContentImageId;
                    sqlCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = imgContentContent;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Result\n" + sqlException.Message);
            }
        }

        public void Add_Content_kojin_E3_Unlike(int imgContentImageId, string imgContentContent, int imgContentUserId, int leng, int time, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + table + "] SET Content3 =@Content, UserId3= " + imgContentUserId + ", InputDate3=getdate(), Tongkytu3= " + leng + ", TimeMiliseconds3= " + time + ", CheckedDate = dateadd(MS, -6000000 , getdate()), QCDate=NULL where Id = " + imgContentImageId;
                    sqlCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = imgContentContent;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Result\n" + sqlException.Message);
            }
        }

        public void Add_Content_E3(int imgContentImageId, string imgContentContent, int imgContentUserId, int leng, int time)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "Add_Content_E3";
                    sqlCommand.Parameters.Add("@ImageId", SqlDbType.Int).Value = imgContentImageId;
                    sqlCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = imgContentContent;
                    sqlCommand.Parameters.Add("@UserId", SqlDbType.Int).Value = imgContentUserId;
                    sqlCommand.Parameters.Add("@leng", SqlDbType.Int).Value = leng;
                    sqlCommand.Parameters.Add("@time", SqlDbType.Int).Value = time;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            { }
        }
        public void Add_Content_OCR_NG(int imgContentImageId, int imgContentUserId, int leng, int time, string imgContentContent, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + table + "] SET Content1 ='',Content2 ='', UserId2=0, UserId1= " + imgContentUserId + ", InputDate1=getdate(), Tongkytu1= " + leng + ", TimeMiliseconds1=" + time + ", Content1_OCR=@Content, QCDate = dateadd(MS, -6000000 , getdate()), NG1=" + imgContentUserId + ", InputDate3=dateadd(MS, -600000 , getdate()) where Id = " + imgContentImageId;
                    sqlCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = imgContentContent;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Result\n" + sqlException.Message);
            }
        }
        public void Add_Content_kojin_NG_E3(int imgContentImageId, int imgContentUserId, int leng, int time, string imgContentContent, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + table + "] SET Content3 =@Content, UserId3= " + imgContentImageId + ", InputDate3=getdate(), Tongkytu3= " + leng + ", TimeMiliseconds3= " + time + ", NG3 = " + imgContentUserId + ", CheckedDate = NULL, QCDate=dateadd(MS, -6000000 , getdate()) where Id = " + imgContentImageId;
                    sqlCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = imgContentContent;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Result\n" + sqlException.Message);
            }
        }
        public void Add_Content_NG_E3(int imgContentImageId, int imgContentUserId, int leng, int time, string imgContentContent)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "Add_Content_NG_E3";
                    sqlCommand.Parameters.Add("@imgContentImageId", SqlDbType.Int).Value = imgContentImageId;
                    sqlCommand.Parameters.Add("@imgContentContent", SqlDbType.NVarChar).Value = imgContentContent;
                    sqlCommand.Parameters.Add("@imgContentUserId", SqlDbType.Int).Value = imgContentUserId;
                    sqlCommand.Parameters.Add("@leng", SqlDbType.Int).Value = leng;
                    sqlCommand.Parameters.Add("@time", SqlDbType.Int).Value = time;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch { }
        }
        public void Add_Content_NG_LV3(int imgContentImageId, int imgContentUserId, int leng, int time, string imgContentContent, int pair, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + table + "] SET Content" + pair + " = @Content, UserId" + pair + "= " + imgContentUserId + ", InputDate" + pair + "=getdate(), NG" + pair + " = " + imgContentUserId + ", Tongkytu" + pair + "= " + leng + ", TimeMiliseconds" + pair + "=" + time + ", Content1_OCR='', Content3 = '', CheckedDate = NULL, QCDate=dateadd(MS, -6000000 , getdate()) where Id = " + imgContentImageId;
                    sqlCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = imgContentContent;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Result\n" + sqlException.Message);
            }
        }
        public void Add_Content_NG_P1(int imgContentImageId, int imgContentUserId, int leng, int time, string imgContentContent1, string imgContentContent2)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "Add_Content_NG_P1";
                    sqlCommand.Parameters.Add("@ImageId", SqlDbType.Int).Value = imgContentImageId;
                    sqlCommand.Parameters.Add("@Content1", SqlDbType.NVarChar).Value = imgContentContent1;
                    sqlCommand.Parameters.Add("@Content2", SqlDbType.NVarChar).Value = imgContentContent2;
                    sqlCommand.Parameters.Add("@UserId", SqlDbType.Int).Value = imgContentUserId;
                    sqlCommand.Parameters.Add("@leng", SqlDbType.Int).Value = leng;
                    sqlCommand.Parameters.Add("@time", SqlDbType.Int).Value = time;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch { }
        }
        public void Add_Content_P2(int imgContentImageId, string imgContentContent, int imgContentUserId, int leng, int time)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "Add_Content_P2";
                    sqlCommand.Parameters.Add("@ImageId", SqlDbType.Int).Value = imgContentImageId;
                    sqlCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = imgContentContent;
                    sqlCommand.Parameters.Add("@UserId", SqlDbType.Int).Value = imgContentUserId;
                    sqlCommand.Parameters.Add("@leng", SqlDbType.Int).Value = leng;
                    sqlCommand.Parameters.Add("@time", SqlDbType.Int).Value = time;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            { }
        }
        public void Add_Content_NG_P2(int imgContentImageId, string imgContentContent, int imgContentUserId, int leng, int time)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "AddContent_NG_P2";
                    sqlCommand.Parameters.Add("@ImageId", SqlDbType.Int).Value = imgContentImageId;
                    sqlCommand.Parameters.Add("@Content", SqlDbType.NVarChar).Value = imgContentContent;
                    sqlCommand.Parameters.Add("@UserId", SqlDbType.Int).Value = imgContentUserId;
                    sqlCommand.Parameters.Add("@leng", SqlDbType.Int).Value = leng;
                    sqlCommand.Parameters.Add("@time", SqlDbType.Int).Value = time;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch { }
        }

        public int ImageExistE3(string batchname, int template)
        {
            int returnVal = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select count(1) from db_owner.[Image] join db_owner.ImageContent on Image.Id=ImageId where Template=" + template + " AND date3 is not null and Content3 is Null and BatchName=N'" + batchname + "'";
                    con.Open();
                    returnVal = Convert.ToInt32(sqlCommand.ExecuteScalar());
                }
            }
            catch
            { }
            return returnVal;
        }
        public void Set_clE3(int id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE db_owner.[Image] SET clE3= 1 WHERE Id = " + id;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            {

            }
        }
        public int batchE3(string table)
        {
            int returnVal = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select 1 from dbo.[" + table + "] where content1 is not null";
                    con.Open();
                    returnVal = Convert.ToInt32(sqlCommand.ExecuteScalar());
                }
            }
            catch
            { }
            return returnVal;
        }
        public int batchE3_Zoukusei(string batchname, int template)
        {
            int returnVal = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select 1 from db_owner.[Image] join db_owner.Imagecontent on image.id=Imageid where content1 is not null And Content2 Is not Null and BatchName=N'" + batchname + "' AND Template=" + template;
                    con.Open();
                    returnVal = Convert.ToInt32(sqlCommand.ExecuteScalar());
                }
            }
            catch
            { }
            return returnVal;
        }
        public bool checkUser(string username, string pass)
        {
            bool bl = false;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = @"select 1 from db_owner.[user] Where Upper(name)=@name and Upper(password)=@pass and ([Role]='ADMIN' or [Role]='CROP')";
                    sqlCommand.Parameters.Add("@name", SqlDbType.NVarChar, 50).Value = username.Trim();
                    sqlCommand.Parameters.Add("@pass", SqlDbType.NVarChar, 50).Value = pass.Trim();
                    con.Open();
                    if (sqlCommand.ExecuteScalar() != null)
                    {
                        bl = true;
                    }
                }
            }
            catch
            { }
            return bl;
        }

        public int Get_IdImage(string tableform, int pair)
        {
            int id = 0;
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(stringconnecttion))
                    {
                        SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.Connection = con;
                        sqlCommand.CommandText = "select top 1 @ID = id from dbo.[" + tableform + "] where Content" + pair + " is null and datediff(ms, InputDate" + pair + ",GETDATE())>600000;" +
                            "Update dbo.[" + tableform + "]  set InputDate" + pair + "  = GETDATE() where Id=@ID";
                        sqlCommand.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;
                        con.Open();
                        sqlCommand.ExecuteNonQuery();
                        id = Convert.ToInt32(sqlCommand.Parameters["@ID"].Value);
                    }
                }
                catch
                { id = 0; }
            }
            return id;
        }

        public int Count_ImageExits(string formtable, int pair)
        {
            int id = -1;
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(stringconnecttion))
                    {
                        SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.Connection = con;
                        sqlCommand.CommandText = @"Select COUNT(Id) from dbo.[" + formtable + "] where Content" + pair + " is null";
                        con.Open();
                        id = (int)sqlCommand.ExecuteScalar();
                    }
                }
                catch { id = -1; }
            }
            return id;
        }

        public void UpdateHitpoint_Batch(int batchid, int hitpoint)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Update dbo.Batch set HitPoint = " + hitpoint + " where Id =" + batchid;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            { }
        }

        public string getContentByPair(string table, int pair, int ImageId)
        {
            string val = "";
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = @"select Content" + pair + " as [Content] from dbo.[" + table + "] where Id = " + ImageId;
                con.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    if (!sqlDataReader.IsDBNull(0))
                    {
                        val = sqlDataReader["Content"].ToString().Trim();
                    }
                    else
                        val = "null";
                    break;
                }
                sqlDataReader.Close();
            }
            return val;
        }

        public void Return_HitpointEntry(int imageId, int pair, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + table + "] SET InputDate" + pair + " = dateadd(MS, -6000000 , getdate()) where Id = " + imageId;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Result\n" + sqlException.Message);
            }
        }

        public int Count_UpHitpointLC(string formtable)
        {
            int id = -1;
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(stringconnecttion))
                    {
                        SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.Connection = con;
                        sqlCommand.CommandText = @"Select COUNT(Id) from dbo.[" + formtable + "] where ResultLC is null";
                        con.Open();
                        id = (int)sqlCommand.ExecuteScalar();
                    }
                }
                catch { id = -1; }
            }
            return id;
        }

        public int Count_UpHitpointEntry(string formtable)
        {
            int id = -1;
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(stringconnecttion))
                    {
                        SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.Connection = con;
                        sqlCommand.CommandText = @"Select COUNT(Id) from dbo.[" + formtable + "] where Content1 <> '' and Content1_OCR is null  and Content2 <> '' and Content3 is null";
                        con.Open();
                        id = (int)sqlCommand.ExecuteScalar();
                    }
                }
                catch { id = -1; }
            }
            return id;
        }

        public int Count_UpHitpointCheck(string formtable)
        {
            int id = -1;
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(stringconnecttion))
                    {
                        SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.Connection = con;
                        sqlCommand.CommandText = @"Select COUNT(Id) from dbo.[" + formtable + "] where Result is null and ResultLC is null";
                        con.Open();
                        id = (int)sqlCommand.ExecuteScalar();
                    }
                }
                catch { id = -1; }
            }
            return id;
        }

        public int Count_UpHitpointLC_KojinLV1(string formtable)
        {
            int id = -1;
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(stringconnecttion))
                    {
                        SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.Connection = con;
                        sqlCommand.CommandText = @"Select COUNT(Id) from dbo.[" + formtable + "] where ResultLC is not null";
                        con.Open();
                        id = (int)sqlCommand.ExecuteScalar();
                    }
                }
                catch { id = -1; }
            }
            return id;
        }
        public int Count_Content_KojinLV1(string formtable,int pair)
        {
            int id = -1;
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(stringconnecttion))
                    {
                        SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.Connection = con;
                        sqlCommand.CommandText = @"Select COUNT(Id) from dbo.[" + formtable + "] where Content1 is  null or Content2 is  null";
                        con.Open();
                        id = (int)sqlCommand.ExecuteScalar();
                    }
                }
                catch { id = -1; }
            }
            return id;
        }
        public int Count_KojinLV1(string formtable)
        {
            int id = -1;
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(stringconnecttion))
                    {
                        SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.Connection = con;
                        sqlCommand.CommandText = @"Select COUNT(Id) from dbo.[" + formtable + "]";
                        con.Open();
                        id = (int)sqlCommand.ExecuteScalar();
                    }
                }
                catch { id = -1; }
            }
            return id;
        }
        public int Count_kt_cten(string formtable)
        {
            int id = -1;
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(stringconnecttion))
                    {
                        SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.Connection = con;
                        sqlCommand.CommandText = @"Select COUNT(Id) from dbo.[" + formtable + "] where Content1 is null and Content2 is null and Content3 is null";
                        con.Open();
                        id = (int)sqlCommand.ExecuteScalar();
                    }
                }
                catch { id = -1; }
            }
            return id;
        }
        public int getEntryinBatch(string nameTable)
        {
            int returnVal = -1;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "select COUNT(*) as 'Số lượng nhập' from dbo.[" + nameTable + "] join Batch on Batch.Id = cast(LEFT('" + nameTable + "',CHARINDEX('--','" + nameTable + "') - 1) as INT) WHERE dbo.[" + nameTable + "].Content_OCR LIKE '%?%'";
                    con.Open();
                    returnVal = (int)sqlCommand.ExecuteScalar();
                }
            }
            catch
            { returnVal = -1; }
            return returnVal;
        }
    }
}
