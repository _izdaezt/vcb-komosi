﻿namespace VCB_KOMOSI
{
    partial class frmEntry_Zokusei
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEntry_Zokusei));
            this.txtOCR = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.lblpage = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.lblpagename = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.lblField = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.lblerror = new System.Windows.Forms.ToolStripLabel();
            this.txtInf = new System.Windows.Forms.ToolStripLabel();
            this.timerEntry = new System.Windows.Forms.Timer(this.components);
            this.btnExit = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnNotgood = new System.Windows.Forms.Button();
            this.lblPressSubmit = new System.Windows.Forms.Label();
            this.imageViewerTR1 = new ImageViewerTR.ImageViewerTR();
            this.txt1 = new System.Windows.Forms.TextBox();
            this.txt3 = new System.Windows.Forms.TextBox();
            this.txt4 = new System.Windows.Forms.TextBox();
            this.txt5 = new System.Windows.Forms.TextBox();
            this.txt6 = new System.Windows.Forms.TextBox();
            this.txt7 = new System.Windows.Forms.TextBox();
            this.txt9 = new System.Windows.Forms.TextBox();
            this.txt10 = new System.Windows.Forms.TextBox();
            this.txt11 = new System.Windows.Forms.TextBox();
            this.txt13 = new System.Windows.Forms.TextBox();
            this.txt14 = new System.Windows.Forms.TextBox();
            this.txt15 = new System.Windows.Forms.TextBox();
            this.txt16 = new System.Windows.Forms.TextBox();
            this.txt18 = new System.Windows.Forms.TextBox();
            this.txt19 = new System.Windows.Forms.TextBox();
            this.txt20 = new System.Windows.Forms.TextBox();
            this.txt2 = new System.Windows.Forms.TextBox();
            this.txt8 = new System.Windows.Forms.TextBox();
            this.txt12 = new System.Windows.Forms.TextBox();
            this.txt17 = new System.Windows.Forms.TextBox();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOCR
            // 
            this.txtOCR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOCR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtOCR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtOCR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtOCR.Location = new System.Drawing.Point(1179, 736);
            this.txtOCR.Multiline = true;
            this.txtOCR.Name = "txtOCR";
            this.txtOCR.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOCR.Size = new System.Drawing.Size(486, 265);
            this.txtOCR.TabIndex = 51;
            this.txtOCR.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.lblpage,
            this.toolStripLabel2,
            this.lblpagename,
            this.toolStripLabel3,
            this.lblField,
            this.toolStripLabel4,
            this.lblerror,
            this.txtInf});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1350, 25);
            this.toolStrip1.TabIndex = 60;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(22, 22);
            this.toolStripLabel1.Text = "     ";
            // 
            // lblpage
            // 
            this.lblpage.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblpage.ForeColor = System.Drawing.Color.Red;
            this.lblpage.Name = "lblpage";
            this.lblpage.Size = new System.Drawing.Size(17, 22);
            this.lblpage.Text = "0";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(22, 22);
            this.toolStripLabel2.Text = "     ";
            // 
            // lblpagename
            // 
            this.lblpagename.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblpagename.ForeColor = System.Drawing.Color.Blue;
            this.lblpagename.Name = "lblpagename";
            this.lblpagename.Size = new System.Drawing.Size(67, 22);
            this.lblpagename.Text = "PageName";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(22, 22);
            this.toolStripLabel3.Text = "     ";
            // 
            // lblField
            // 
            this.lblField.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblField.ForeColor = System.Drawing.Color.Blue;
            this.lblField.Name = "lblField";
            this.lblField.Size = new System.Drawing.Size(87, 22);
            this.lblField.Text = "SBD,Ngày sinh";
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(22, 22);
            this.toolStripLabel4.Text = "     ";
            // 
            // lblerror
            // 
            this.lblerror.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblerror.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblerror.ForeColor = System.Drawing.Color.Red;
            this.lblerror.Name = "lblerror";
            this.lblerror.Size = new System.Drawing.Size(0, 22);
            // 
            // txtInf
            // 
            this.txtInf.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInf.ForeColor = System.Drawing.Color.Blue;
            this.txtInf.Name = "txtInf";
            this.txtInf.Size = new System.Drawing.Size(39, 22);
            this.txtInf.Text = "Batch";
            // 
            // timerEntry
            // 
            this.timerEntry.Enabled = true;
            this.timerEntry.Interval = 200;
            this.timerEntry.Tick += new System.EventHandler(this.timerEntry_Tick);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(1317, 1);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(33, 24);
            this.btnExit.TabIndex = 81;
            this.btnExit.TabStop = false;
            this.btnExit.Text = "X";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(1210, 703);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 16);
            this.label2.TabIndex = 111;
            this.label2.Text = "Ctr + N";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSubmit.BackgroundImage")));
            this.btnSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSubmit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnSubmit.Location = new System.Drawing.Point(964, 631);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(160, 60);
            this.btnSubmit.TabIndex = 108;
            this.btnSubmit.TabStop = false;
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnNotgood
            // 
            this.btnNotgood.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNotgood.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNotgood.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnNotgood.ForeColor = System.Drawing.Color.Red;
            this.btnNotgood.Location = new System.Drawing.Point(1155, 631);
            this.btnNotgood.Name = "btnNotgood";
            this.btnNotgood.Size = new System.Drawing.Size(160, 60);
            this.btnNotgood.TabIndex = 109;
            this.btnNotgood.TabStop = false;
            this.btnNotgood.Text = "Not Good";
            this.btnNotgood.UseVisualStyleBackColor = true;
            this.btnNotgood.Click += new System.EventHandler(this.btnNotgood_Click);
            // 
            // lblPressSubmit
            // 
            this.lblPressSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPressSubmit.AutoSize = true;
            this.lblPressSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblPressSubmit.ForeColor = System.Drawing.Color.Red;
            this.lblPressSubmit.Location = new System.Drawing.Point(1024, 703);
            this.lblPressSubmit.Name = "lblPressSubmit";
            this.lblPressSubmit.Size = new System.Drawing.Size(44, 16);
            this.lblPressSubmit.TabIndex = 110;
            this.lblPressSubmit.Text = "Enter";
            // 
            // imageViewerTR1
            // 
            this.imageViewerTR1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.imageViewerTR1.BackColor = System.Drawing.Color.LightGray;
            this.imageViewerTR1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imageViewerTR1.CurrentZoom = 1F;
            this.imageViewerTR1.Image = null;
            this.imageViewerTR1.Location = new System.Drawing.Point(78, 61);
            this.imageViewerTR1.MaxZoom = 20F;
            this.imageViewerTR1.MinZoom = 0.05F;
            this.imageViewerTR1.Name = "imageViewerTR1";
            this.imageViewerTR1.Size = new System.Drawing.Size(577, 534);
            this.imageViewerTR1.TabIndex = 112;
            // 
            // txt1
            // 
            this.txt1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt1.Location = new System.Drawing.Point(686, 118);
            this.txt1.MaxLength = 2;
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(100, 80);
            this.txt1.TabIndex = 1;
            // 
            // txt3
            // 
            this.txt3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt3.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt3.Location = new System.Drawing.Point(917, 118);
            this.txt3.MaxLength = 2;
            this.txt3.Name = "txt3";
            this.txt3.Size = new System.Drawing.Size(100, 80);
            this.txt3.TabIndex = 3;
            // 
            // txt4
            // 
            this.txt4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt4.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt4.Location = new System.Drawing.Point(1034, 118);
            this.txt4.MaxLength = 10;
            this.txt4.Name = "txt4";
            this.txt4.Size = new System.Drawing.Size(168, 80);
            this.txt4.TabIndex = 4;
            // 
            // txt5
            // 
            this.txt5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt5.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt5.Location = new System.Drawing.Point(1219, 118);
            this.txt5.MaxLength = 1;
            this.txt5.Name = "txt5";
            this.txt5.Size = new System.Drawing.Size(100, 80);
            this.txt5.TabIndex = 5;
            // 
            // txt6
            // 
            this.txt6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt6.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt6.Location = new System.Drawing.Point(686, 228);
            this.txt6.MaxLength = 2;
            this.txt6.Name = "txt6";
            this.txt6.Size = new System.Drawing.Size(100, 80);
            this.txt6.TabIndex = 6;
            // 
            // txt7
            // 
            this.txt7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt7.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt7.Location = new System.Drawing.Point(802, 228);
            this.txt7.MaxLength = 2;
            this.txt7.Name = "txt7";
            this.txt7.Size = new System.Drawing.Size(100, 80);
            this.txt7.TabIndex = 7;
            // 
            // txt9
            // 
            this.txt9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt9.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt9.Location = new System.Drawing.Point(1034, 228);
            this.txt9.MaxLength = 10;
            this.txt9.Name = "txt9";
            this.txt9.Size = new System.Drawing.Size(168, 80);
            this.txt9.TabIndex = 9;
            // 
            // txt10
            // 
            this.txt10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt10.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt10.Location = new System.Drawing.Point(1219, 228);
            this.txt10.MaxLength = 1;
            this.txt10.Name = "txt10";
            this.txt10.Size = new System.Drawing.Size(100, 80);
            this.txt10.TabIndex = 10;
            // 
            // txt11
            // 
            this.txt11.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt11.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt11.Location = new System.Drawing.Point(686, 345);
            this.txt11.MaxLength = 2;
            this.txt11.Name = "txt11";
            this.txt11.Size = new System.Drawing.Size(100, 80);
            this.txt11.TabIndex = 11;
            // 
            // txt13
            // 
            this.txt13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt13.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt13.Location = new System.Drawing.Point(917, 345);
            this.txt13.MaxLength = 2;
            this.txt13.Name = "txt13";
            this.txt13.Size = new System.Drawing.Size(100, 80);
            this.txt13.TabIndex = 13;
            // 
            // txt14
            // 
            this.txt14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt14.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt14.Location = new System.Drawing.Point(1034, 345);
            this.txt14.MaxLength = 10;
            this.txt14.Name = "txt14";
            this.txt14.Size = new System.Drawing.Size(168, 80);
            this.txt14.TabIndex = 14;
            // 
            // txt15
            // 
            this.txt15.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt15.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt15.Location = new System.Drawing.Point(1219, 345);
            this.txt15.MaxLength = 1;
            this.txt15.Name = "txt15";
            this.txt15.Size = new System.Drawing.Size(100, 80);
            this.txt15.TabIndex = 15;
            // 
            // txt16
            // 
            this.txt16.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt16.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt16.Location = new System.Drawing.Point(686, 456);
            this.txt16.MaxLength = 2;
            this.txt16.Name = "txt16";
            this.txt16.Size = new System.Drawing.Size(100, 80);
            this.txt16.TabIndex = 16;
            // 
            // txt18
            // 
            this.txt18.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt18.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt18.Location = new System.Drawing.Point(917, 456);
            this.txt18.MaxLength = 2;
            this.txt18.Name = "txt18";
            this.txt18.Size = new System.Drawing.Size(100, 80);
            this.txt18.TabIndex = 18;
            // 
            // txt19
            // 
            this.txt19.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt19.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt19.Location = new System.Drawing.Point(1034, 456);
            this.txt19.MaxLength = 10;
            this.txt19.Name = "txt19";
            this.txt19.Size = new System.Drawing.Size(168, 80);
            this.txt19.TabIndex = 19;
            // 
            // txt20
            // 
            this.txt20.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt20.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt20.Location = new System.Drawing.Point(1219, 456);
            this.txt20.MaxLength = 1;
            this.txt20.Name = "txt20";
            this.txt20.Size = new System.Drawing.Size(100, 80);
            this.txt20.TabIndex = 20;
            // 
            // txt2
            // 
            this.txt2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt2.Location = new System.Drawing.Point(802, 118);
            this.txt2.MaxLength = 2;
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(100, 80);
            this.txt2.TabIndex = 2;
            // 
            // txt8
            // 
            this.txt8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt8.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt8.Location = new System.Drawing.Point(917, 228);
            this.txt8.MaxLength = 2;
            this.txt8.Name = "txt8";
            this.txt8.Size = new System.Drawing.Size(100, 80);
            this.txt8.TabIndex = 8;
            // 
            // txt12
            // 
            this.txt12.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt12.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt12.Location = new System.Drawing.Point(802, 345);
            this.txt12.MaxLength = 2;
            this.txt12.Name = "txt12";
            this.txt12.Size = new System.Drawing.Size(100, 80);
            this.txt12.TabIndex = 12;
            // 
            // txt17
            // 
            this.txt17.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt17.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txt17.Location = new System.Drawing.Point(802, 456);
            this.txt17.MaxLength = 2;
            this.txt17.Name = "txt17";
            this.txt17.Size = new System.Drawing.Size(100, 80);
            this.txt17.TabIndex = 17;
            // 
            // frmEntry_Zokusei
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.ClientSize = new System.Drawing.Size(1350, 730);
            this.Controls.Add(this.txt17);
            this.Controls.Add(this.txt12);
            this.Controls.Add(this.txt8);
            this.Controls.Add(this.txt2);
            this.Controls.Add(this.txt20);
            this.Controls.Add(this.txt19);
            this.Controls.Add(this.txt18);
            this.Controls.Add(this.txt16);
            this.Controls.Add(this.txt15);
            this.Controls.Add(this.txt14);
            this.Controls.Add(this.txt13);
            this.Controls.Add(this.txt11);
            this.Controls.Add(this.txt10);
            this.Controls.Add(this.txt9);
            this.Controls.Add(this.txt7);
            this.Controls.Add(this.txt6);
            this.Controls.Add(this.txt5);
            this.Controls.Add(this.txt4);
            this.Controls.Add(this.txt3);
            this.Controls.Add(this.txt1);
            this.Controls.Add(this.imageViewerTR1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnNotgood);
            this.Controls.Add(this.lblPressSubmit);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.txtOCR);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEntry_Zokusei";
            this.Text = "VCB - Entry ";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEntry_FormClosing);
            this.Load += new System.EventHandler(this.frmEntry_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEntry_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmEntry_KeyPress);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public static System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.TextBox txtOCR;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel lblpage;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel lblpagename;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripLabel lblField;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.Timer timerEntry;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel lblerror;
        private System.Windows.Forms.Button btnExit;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnNotgood;
        internal System.Windows.Forms.Label lblPressSubmit;
        private ImageViewerTR.ImageViewerTR imageViewerTR1;
        private System.Windows.Forms.ToolStripLabel txtInf;
        private System.Windows.Forms.TextBox txt1;
        private System.Windows.Forms.TextBox txt3;
        private System.Windows.Forms.TextBox txt4;
        private System.Windows.Forms.TextBox txt5;
        private System.Windows.Forms.TextBox txt6;
        private System.Windows.Forms.TextBox txt7;
        private System.Windows.Forms.TextBox txt9;
        private System.Windows.Forms.TextBox txt10;
        private System.Windows.Forms.TextBox txt11;
        private System.Windows.Forms.TextBox txt13;
        private System.Windows.Forms.TextBox txt14;
        private System.Windows.Forms.TextBox txt15;
        private System.Windows.Forms.TextBox txt16;
        private System.Windows.Forms.TextBox txt18;
        private System.Windows.Forms.TextBox txt19;
        private System.Windows.Forms.TextBox txt20;
        private System.Windows.Forms.TextBox txt2;
        private System.Windows.Forms.TextBox txt8;
        private System.Windows.Forms.TextBox txt12;
        private System.Windows.Forms.TextBox txt17;
    }
}