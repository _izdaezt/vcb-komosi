﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
namespace VCB_KOMOSI
{
    public partial class FrmEntryTemp2_2 : Form
    {
        #region var
        public static Point locationOnForm;
        public static bool selecthitpoint = true;
        public string[] INFperformance = new string[2];
        public int batchId;
        public int userId;
        public int formid;
        public int pair;
        private double scale = 1;
        public string userName;
        public string batchName;
        public string template;
        public string level;
        public string group;
        public string formName = "";
        Image imageOriginal = null;
        Bitmap imageSource;
        private DAEntry_Entry dAEntry = new DAEntry_Entry();
        private BOImage_Entry img = new BOImage_Entry();
        private bool finish = false;
        bool eter = false;
        bool space = false;
        bool check = false;
        int[] INFImage;
        public static int locationX = 0;
        public static int locationY = 0;
        double tyle;
        double tyleImageNew = 1;
        Bitmap bm_out;
        int zom = 11;
        // value bool Japan
        bool jp = true;
        List<TextBox> lstTxt = new List<TextBox>();
        List<Label> lstLblSubFieldName = new List<Label>();
        List<Label> lstLblSubFieldLimit = new List<Label>();
        List<BOImage_Entry> ListImage = new List<BOImage_Entry>();
        DataRow[] subField = null;
        string pathLog = "";
        DBsql dbsql = new DBsql();
        bool isErrorCong = false;
        int limitTime = 0;
        DateTime dtimeBefore = new DateTime();
        #endregion
        public FrmEntryTemp2_2()
        {
            InitializeComponent();

            int screenW = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            this.Width = screenW;   
       

            lstTxt.Add(txt1);
            lstTxt.Add(txt2);
        }

        private void frmEntry_Load(object sender, EventArgs e)
        {
            // hiển thị thông tin user          
            txtInf.Text = "VCB Entry  --  UserName: " + userName + "  --  Form: " + frmLogIn.form + "  -- Template: " + template + "  --  Batch: " + batchName + "  --  Level: " + level;
            if (txtInf == null) return;
            SizeF stringSize;
            // create a graphics object for this form
            using (Graphics gfx = this.CreateGraphics())
            {
                // Get the size given the string and the font
                stringSize = gfx.MeasureString(txtInf.Text, txtInf.Font);
            }
            // Resize the textbox 
            txtInf.Size = new Size((int)Math.Round(stringSize.Width, 0), 23);
            string nameFile = "Log" + System.IO.Path.GetFileName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "Entry.txt";
            pathLog = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\" + nameFile;
            //Lây thư mục gốc từ batchname
            timerEntry.Start();
            try
            {
                if (batchName[7] == 'A')
                {
                    txt1.Enabled = false;
                    lbl1.Enabled = false;
                }
            }
            catch { }

            //userid
            int uID = userId;

            // hien thi giua man hinh
            this.CenterToScreen();
            dAEntry = new DAEntry_Entry();

            // lấy id image free
            int id = 0;
            try
            {
                id = dAEntry.GetIdImageEntrySecond(batchId, userId, pair);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            if (id == 0)
            {
                finish = true;
                this.Close();
                return;
            }
            // lay image free
            try
            {
                img = dAEntry.GetImage(id);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            //Get Image
            try
            {
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageUrl, batchName.Substring(7, batchName.Length - 7))));
                imageViewerTR1.Image = imageSource;
                imageViewerTR1.CurrentZoom = 0.7f;
            }
            catch
            {
                MessageBox.Show("Server chưa có ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // hiển thị tên field name          
            lblpagename.Text = img.PageUrl;
            bgwGetImage.RunWorkerAsync();
            dtimeBefore = DateTime.Now;
        }

        //int a = 1;
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            string imgContentContent = txt1.Text + "|" + txt2.Text;
            int lng = imgContentContent.Length - 1;
            bm_out = null;
            int uID = userId;
            tyleImageNew = 1;
            zom = 11;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                dAEntry.AddImageContent2(imgContentImageId, imgContentContent, imgContentUserId, lng, ms);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }


            // load image mới lên form
            // lấy id image free         
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                if (!bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                {
                    finish = true;
                    this.Close();
                    return;
                }
                else
                {
                    while (ListImage.Count == 0)
                    { }
                    img = ListImage[0];
                    ListImage.RemoveAt(0);
                }
            }

            try
            {
                //Get Image
                imageSource = (Bitmap)img.Imagesource.Clone();
                imageViewerTR1.Image = imageSource;
                imageViewerTR1.CurrentZoom = 0.7f;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // hiển thị tên field name       
            dtimeBefore = DateTime.Now;
            lblpagename.Text = img.PageUrl;
            txt1.Text = ""; txt2.Text = "";
            btnSubmit.Enabled = true;
            btnNotgood.Enabled = true;
            if (txt1.Enabled)
                txt1.Focus();
            else
                txt2.Focus();
            if (!bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                bgwGetImage1.RunWorkerAsync();
            else if (bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                bgwGetImage1.RunWorkerAsync();
            else if (!bgwGetImage.IsBusy && bgwGetImage1.IsBusy)
                bgwGetImage.RunWorkerAsync();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmEntry_FormClosing(object sender, FormClosingEventArgs e)
        {          
            try
            {
                if (!finish)
                {
                    if (bgwGetImage.IsBusy || bgwGetImage1.IsBusy)
                        e.Cancel = true;
                    dAEntry.ReturnPairANDHitpointEntry(img.Id, userId);
                    // unlock image       
                    for (int i = 0; i < ListImage.Count; i++)
                    {
                        dAEntry.ReturnPairANDHitpointEntry(ListImage[i].Id, userId);
                    }                  
                }
                else
                {
                    frmLogIn.fn = true;
                }
                if (File.Exists(pathLog))
                {
                    File.Delete(pathLog);
                }
                dAEntry.SetHitpointBatch(batchId, pair);
                timerEntry.Stop();
            }
            catch { }
        }

        private void btnNotgood_Click(object sender, EventArgs e)
        {
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            string imgContentContent = txt1.Text + "|" + txt2.Text;
            int lng = imgContentContent.Length - 1;
            bm_out = null;
            int uID = userId;
            tyleImageNew = 1;
            zom = 11;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                dAEntry.AddImageContentNG2(imgContentImageId, imgContentContent, imgContentUserId, lng, ms);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // load image mới lên form
            // lấy id image free
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                if (!bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                {
                    dAEntry.SetHitpointBatch(batchId, pair);
                    finish = true;
                    this.Close();
                    return;
                }
                else
                {
                    while (ListImage.Count == 0)
                    { }
                    img = ListImage[0];
                    ListImage.RemoveAt(0);
                }
            }

            try
            {
                //Get Image
                imageSource = (Bitmap)img.Imagesource.Clone();
                imageViewerTR1.Image = imageSource;
                imageViewerTR1.CurrentZoom = 0.7f;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // hiển thị tên field name       

            lblpagename.Text = img.PageUrl;
            dtimeBefore = DateTime.Now;
            btnSubmit.Enabled = true;
            btnNotgood.Enabled = true;
            if (txt1.Enabled)
                txt1.Focus();
            else
                txt2.Focus();
            txt1.Text = ""; txt2.Text = "";
            if (!bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                bgwGetImage1.RunWorkerAsync();
            else if (bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                bgwGetImage1.RunWorkerAsync();
            else if (!bgwGetImage.IsBusy && bgwGetImage1.IsBusy)
                bgwGetImage.RunWorkerAsync();
        }

        private void chkFullimage_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void frmEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && btnSubmit.Enabled && btnNotgood.Enabled)
            {
                btnSubmit_Click(sender, e);
                e.Handled = true;
            }
            if (e.Control)
            {
                if (e.KeyCode == Keys.N && btnSubmit.Enabled && btnNotgood.Enabled)
                    btnNotgood_Click(sender, e);
            }
            if (e.KeyCode == Keys.LWin || e.KeyCode == Keys.RWin)
            {
                if (e.KeyCode == Keys.D)
                {
                    this.Activate();
                }
                SendKeys.Send("{Esc}");
                SendKeys.Send("{Esc}");
                e.Handled = true;
            }
        }

        private void txt1_Enter(object sender, EventArgs e)
        {
            txt1.BackColor = Color.PowderBlue;
            txt2.BackColor = SystemColors.Window;
            foreach (InputLanguage lang in InputLanguage.InstalledInputLanguages)
                if (lang.LayoutName.Contains("US"))
                {
                    InputLanguage.CurrentInputLanguage = lang; break;
                }
        }

        private void txt2_Enter(object sender, EventArgs e)
        {
            txt2.BackColor = Color.PowderBlue;
            txt1.BackColor = SystemColors.Window;
            foreach (InputLanguage lang in InputLanguage.InstalledInputLanguages)
                if (lang.LayoutName.Contains("Japanese"))
                {
                    InputLanguage.CurrentInputLanguage = lang; break;
                }
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
        }
        private void timerEntry_Tick(object sender, EventArgs e)
        {
            this.Activate();
            limitTime++;
            try
            {
                string content = INFperformance[0] + "\r\n" + img.Id + "," + String.Join(",", ListImage.Select(x => x.Id).ToArray()) + "\r\n" + DateTime.Now + "\r\n" + pair;
                File.WriteAllText(pathLog, content, Encoding.UTF8);
            }
            catch
            { }
        }

        private void frmEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void btnZmIn_Click(object sender, EventArgs e)
        {
            imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
        }

        private void btnZmOut_Click(object sender, EventArgs e)
        {
            imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
        }

        private void btnRR_Click(object sender, EventArgs e)
        {
            imageViewerTR1.RotateImage("270");
        }

        private void btnRL_Click(object sender, EventArgs e)
        {
            imageViewerTR1.RotateImage("90");
        }

        private void chkFullimage_Click(object sender, EventArgs e)
        {
            imageViewerTR1.CurrentZoom = 0.7f;
        }

        private void bgwGetImage_DoWork(object sender, DoWorkEventArgs e)
        {
            while (ListImage.Count < 2)
            {
                int id2 = dAEntry.GetIdImageEntrySecond(batchId, userId, pair);
                if (id2 > 0)
                {
                    BOImage_Entry img2 = dAEntry.GetImage(id2);
                    try
                    {
                        img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img2.PageUrl, batchName.Substring(7, batchName.Length - 7))));
                    }
                    catch { img2.Imagesource = null; }
                    ListImage.Add(img2);
                }
                else
                    break;
            }
        }

        private void bgwGetImage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblpage.Text = (dAEntry.ImageExist(batchId, pair)).ToString();
        }

        private void bgwGetImage1_DoWork(object sender, DoWorkEventArgs e)
        {
            while (ListImage.Count < 2)
            {
                int id2 = dAEntry.GetIdImageEntrySecond(batchId, userId, pair);
                if (id2 > 0)
                {
                    BOImage_Entry img2 = dAEntry.GetImage(id2);
                    try
                    {
                        img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img2.PageUrl, batchName.Substring(7, batchName.Length - 7))));
                    }
                    catch { img2.Imagesource = null; }
                    ListImage.Add(img2);
                }
                else
                    break;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txt1_TextChanged(object sender, EventArgs e)
        {
            if (txt1.Text.Length == 1)
                txt2.Focus();
        }

        private void txt2_KeyDown(object sender, KeyEventArgs e)
        {
            if (txt1.Enabled)
                if (e.KeyCode == Keys.Back && txt2.Text == "")
                {
                    txt1.Focus();
                    txt1.SelectionStart = txt1.Text.Length;
                }
        }
    }
}
