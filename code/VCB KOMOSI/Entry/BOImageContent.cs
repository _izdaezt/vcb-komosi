﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VCB_KOMOSI
{
    class BOImageContent_Entry
    {
        private int imageId;
        public int ImageId
        {
            get { return imageId; }
            set { imageId = value; }
        }

        private string content;
        public string Content
        {
            get { return content; }
            set { content = value; }
        }

        private int userId;
        public int UserId
        {
            get { return userId; }
            set { userId = value; }
        }

        private string inputDate;
        public string InputDate
        {
            get { return inputDate; }
            set { inputDate = value; }
        }

        private decimal typeRate;
        public decimal TypeRate
        {
            get { return typeRate; }
            set { typeRate = value; }
        }

        private byte selected;
        public byte Selected
        {
            get { return selected; }
            set { selected = value; }
        }

        private string state;
        public string State
        {
            get { return state; }
            set { state = value; }
        }
    }
}
