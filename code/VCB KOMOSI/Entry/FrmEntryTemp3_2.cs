﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
namespace VCB_KOMOSI
{
    public partial class FrmEntryTemp3_2 : Form
    {
        #region var
        public static Point locationOnForm;
        public static bool selecthitpoint = true;
        public string[] INFperformance = new string[2];
        public int batchId;
        public int userId;
        public int formid;
        public int pair;
        public string userName;
        public string batchName;
        public string template;
        public string level;
        public string group;
        public string formName = "";
        Bitmap imageSource;
        private DAEntry_Entry dAEntry = new DAEntry_Entry();
        private BOImage_Entry img = new BOImage_Entry();
        private bool finish = false;
        int[] INFImage;
        float currentzoom;
        public static int locationX = 0;
        public static int locationY = 0;
        // value bool Japan      
        DateTime dtimeBefore = new DateTime();
        List<BOImage_Entry> ListImage = new List<BOImage_Entry>();
        string pathLog = "";
        DBsql dbsql = new DBsql();
        List<Label> lstlbl1;
        List<Label> lstlbl2;
        List<Label> lstlbl3;
        List<Label> lstlbl4;
        List<SplitContainer> lstSPC;
        string batchNameLocal;
        #endregion
        public FrmEntryTemp3_2()
        {
            InitializeComponent();
            lstlbl1 = new List<Label>() { lbl11, lbl12 };
            lstlbl2 = new List<Label>() { lbl21, lbl22, lbl23, lbl24 };
            lstlbl3 = new List<Label>() { lbl31, lbl32, lbl33, lbl34, lbl35, lbl36, lbl37 };
            lstlbl4 = new List<Label>() { lbl41, lbl42, lbl43, lbl44, lbl45, lbl46, lbl47 };
            lstSPC = new List<SplitContainer>() { spc1, spc2, spc3, spc4 };
            lstSPC.ForEach(a =>
            {
                a.Enter += new System.EventHandler(this.spc_Enter);
                a.Leave += new System.EventHandler(this.spc_Leave);
                a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.spc_KeyDown);
            });
        }

        private void frmEntry_Load(object sender, EventArgs e)
        {
            // hiển thị thông tin user          
            txtInf.Text = "VCB Entry  --  UserName: " + userName + "  -- Template: " + template + "  --  Batch: " + batchName;

            //Lây thư mục gốc từ batchname      

            batchNameLocal = batchName.Substring(7);

            //userid
            int uID = userId;

            // hien thi giua man hinh
            this.CenterToScreen();
            dAEntry = new DAEntry_Entry();

            // lấy id image free
            int id = 0;
            try
            {
                id = dAEntry.GetIdImageEntrySecond(batchNameLocal, formid, pair);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            if (id == 0)
            {
                finish = true;
                this.Close();
                return;
            }

            timerEntry.Start();

            // lay image free
            try
            {
                //img = dAEntry.GetImage(id);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            //Get Image
            try
            {
                //imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageUrl, batchName.Substring(7, batchName.Length - 7))));
                //imageViewerTR1.Image = imageSource;
                //currentzoom = imageViewerTR1.CurrentZoom;
                //nam 21/7
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageUrl, batchNameLocal)));
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 10 / 100, bm1.Height * 7 / 80, (bm1.Width * 38 / 100), (bm1.Height * 29 / 50));
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Server chưa có ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // hiển thị tên field name          
            lblpagename.Text = img.PageUrl;
            //lblpage.Text = dAEntry.ImageExist(batchNameLocal, pair, formid).ToString();
            dtimeBefore = DateTime.Now;
            //SendKeys.Send("{tab}");
            spc1.Focus();
        }
        //int a = 1;
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            string[] arr2 = new string[4];
            string[] arr3 = new string[7];
            string[] arr4 = new string[7];
            string imgContent1 = (lstlbl1[0].BackColor == Color.YellowGreen ? lstlbl1[0].Text : "") + (lstlbl1[1].BackColor == Color.YellowGreen ? lstlbl1[1].Text : "");
            int countchars = 1;
            for (int i = 0; i < 7; i++)
            {
                if (i < 4)
                {
                    if (lstlbl2[i].BackColor == Color.YellowGreen)
                    {
                        arr2[i] = lstlbl2[i].Text;
                        countchars++;
                    }
                }
                if (lstlbl3[i].BackColor == Color.YellowGreen)
                {
                    arr3[i] = lstlbl3[i].Text;
                    countchars++;
                }
                if (lstlbl4[i].BackColor == Color.YellowGreen)
                {
                    arr4[i] = lstlbl4[i].Text;
                    countchars++;
                }
            }
            string imgContent2 = String.Join("|", arr2);
            string imgContent3 = String.Join("|", arr3);
            string imgContent4 = String.Join("|", arr4);
            string imgContentContent = imgContent1 + "|" + imgContent2 + "|" + imgContent3 + "|" + imgContent4;          
            int lng = countchars;
            int imgContentImageId = img.Id;
            int uID = userId;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                dAEntry.Add_Content_P2(imgContentImageId, imgContentContent, imgContentUserId, countchars, ms);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // load image mới lên form
            // lấy id image free         
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                dAEntry.SetHitpointBatch(batchNameLocal, formid);
                finish = true;
                this.Close();
                return;
            }

            try
            {
                //Get Image
                //imageSource = (Bitmap)img.Imagesource.Clone();
                //imageViewerTR1.Image = imageSource;

                //nam 21/7
                imageSource = img.Imagesource;
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 10 / 100, bm1.Height * 7 / 80, (bm1.Width * 38 / 100), (bm1.Height * 29 / 50));
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // hiển thị tên field name        
            lblpagename.Text = img.PageUrl;
            dtimeBefore = DateTime.Now;
            btnSubmit.Enabled = true;
            btnNotgood.Enabled = true;
            lstlbl1.ForEach(s => s.BackColor = SystemColors.Window);
            lstlbl2.ForEach(s => s.BackColor = SystemColors.Window);
            lstlbl3.ForEach(s => s.BackColor = SystemColors.Window);
            lstlbl4.ForEach(s => s.BackColor = SystemColors.Window);
            //SendKeys.Send("{tab}");
            spc1.Focus();
            //lblpage.Text = dAEntry.ImageExist(batchNameLocal, pair, formid).ToString();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmEntry_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerEntry.Stop();
            try
            {
                if (!finish)
                {
                    dAEntry.ReturnPairANDHitpointEntry(img.Id, pair);
                    // unlock image       
                    for (int i = 0; i < ListImage.Count; i++)
                    {
                        dAEntry.ReturnPairANDHitpointEntry(ListImage[i].Id, pair);
                    }
                }
                else
                {
                    frmLogIn.fn = true;
                    dAEntry.SetHitpointBatch(batchNameLocal, formid);
                }

            }
            catch { }
        }

        private void btnNotgood_Click(object sender, EventArgs e)
        {
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            string[] arr2 = new string[4];
            string[] arr3 = new string[7];
            string[] arr4 = new string[7];
            string imgContent1 = (lstlbl1[0].BackColor == Color.YellowGreen ? lstlbl1[0].Text : "") + (lstlbl1[1].BackColor == Color.YellowGreen ? lstlbl1[1].Text : "");
            int countchars = 1;
            for (int i = 0; i < 7; i++)
            {
                if (i < 4)
                {
                    if (lstlbl2[i].BackColor == Color.YellowGreen)
                    {
                        arr2[i] = lstlbl2[i].Text;
                        countchars++;
                    }
                }
                if (lstlbl3[i].BackColor == Color.YellowGreen)
                {
                    arr3[i] = lstlbl3[i].Text;
                    countchars++;
                }
                if (lstlbl4[i].BackColor == Color.YellowGreen)
                {
                    arr4[i] = lstlbl4[i].Text;
                    countchars++;
                }
            }
            string imgContent2 = String.Join("|", arr2);
            string imgContent3 = String.Join("|", arr3);
            string imgContent4 = String.Join("|", arr4);
            string imgContentContent = imgContent1 + "|" + imgContent2 + "|" + imgContent3 + "|" + imgContent4;
            int lng = countchars;
            int uID = userId;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                dAEntry.Add_Content_NG_P2(imgContentImageId, imgContentContent, imgContentUserId, countchars, ms);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // load image mới lên form
            // lấy id image free
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                dAEntry.SetHitpointBatch(batchNameLocal, formid);
                finish = true;
                this.Close();
                return;
            }

            try
            {
                //Get Image
                //imageSource = (Bitmap)img.Imagesource.Clone();
                //imageViewerTR1.Image = imageSource;

                //nam 21/7
                imageSource = img.Imagesource;
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 10 / 100, bm1.Height * 7 / 80, (bm1.Width * 38 / 100), (bm1.Height * 29 / 50));
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            btnSubmit.Enabled = true;
            btnNotgood.Enabled = true;
            // hiển thị tên field name        
            dtimeBefore = DateTime.Now;
            lblpagename.Text = img.PageUrl;
            lstlbl1.ForEach(s => s.BackColor = SystemColors.Window);
            lstlbl2.ForEach(s => s.BackColor = SystemColors.Window);
            lstlbl3.ForEach(s => s.BackColor = SystemColors.Window);
            lstlbl4.ForEach(s => s.BackColor = SystemColors.Window);
            //SendKeys.Send("{tab}");
            spc1.Focus();
            //lblpage.Text = dAEntry.ImageExist(batchNameLocal, pair, formid).ToString();
        }

        private void chkFullimage_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void frmEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (spc1.Focused)
            {
                if (e.KeyCode == Keys.NumPad1)
                {
                    if (lstlbl1[0].BackColor == SystemColors.Window)
                    {
                        lstlbl1[0].BackColor = Color.YellowGreen;
                        lstlbl1[1].BackColor = SystemColors.Window;
                    }
                    else
                    {
                        lstlbl1[0].BackColor = SystemColors.Window;
                    }
                }
                if (e.KeyCode == Keys.NumPad2)
                {
                    if (lstlbl1[1].BackColor == SystemColors.Window)
                    {
                        lstlbl1[1].BackColor = Color.YellowGreen;
                        lstlbl1[0].BackColor = SystemColors.Window;
                    }
                    else
                    {
                        lstlbl1[1].BackColor = SystemColors.Window;
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    foreach (Control ctrl in spc1.Panel1.Controls)
                        if (ctrl.GetType() == typeof(Label))
                            ctrl.BackColor = SystemColors.Window;
                }
            }
            else if (spc2.Focused)
            {
                if (e.KeyValue > 96 && e.KeyValue < 101)
                {
                    if (lstlbl2[e.KeyValue - 97].BackColor == SystemColors.Window)
                    {
                        lstlbl2[e.KeyValue - 97].BackColor = Color.YellowGreen;
                    }
                    else
                    {
                        lstlbl2[e.KeyValue - 97].BackColor = SystemColors.Window;
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    foreach (Control ctrl in spc2.Panel1.Controls)
                        if (ctrl.GetType() == typeof(Label))
                            ctrl.BackColor = SystemColors.Window;
                }
            }
            else if (spc3.Focused)
            {
                if (e.KeyValue > 96 && e.KeyValue < 104)
                {
                    if (lstlbl3[e.KeyValue - 97].BackColor == SystemColors.Window)
                    {
                        lstlbl3[e.KeyValue - 97].BackColor = Color.YellowGreen;
                    }
                    else
                    {
                        lstlbl3[e.KeyValue - 97].BackColor = SystemColors.Window;
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    foreach (Control ctrl in spc3.Panel1.Controls)
                        if (ctrl.GetType() == typeof(Label))
                            ctrl.BackColor = SystemColors.Window;
                }
            }
            else if (spc4.Focused)
            {
                if (e.KeyValue > 96 && e.KeyValue < 104)
                {
                    if (lstlbl4[e.KeyValue - 97].BackColor == SystemColors.Window)
                    {
                        lstlbl4[e.KeyValue - 97].BackColor = Color.YellowGreen;
                    }
                    else
                    {
                        lstlbl4[e.KeyValue - 97].BackColor = SystemColors.Window;
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    foreach (Control ctrl in spc4.Panel1.Controls)
                        if (ctrl.GetType() == typeof(Label))
                            ctrl.BackColor = SystemColors.Window;
                }
            }
            if (e.KeyCode == Keys.Enter && btnSubmit.Enabled && btnNotgood.Enabled)
            {
                btnSubmit_Click(sender, e);
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Escape)
                this.Close();
            if (e.Control)
            {
                if (e.KeyCode == Keys.N && btnSubmit.Enabled && btnNotgood.Enabled)
                    btnNotgood_Click(sender, e);
                if (e.KeyCode == Keys.Up)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Down)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    imageViewerTR1.RotateImage("270");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Left)
                {
                    imageViewerTR1.RotateImage("90");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.F)
                {
                    imageViewerTR1.CurrentZoom = currentzoom;
                    e.Handled = true;
                }
            }
            if (e.KeyCode == Keys.LWin || e.KeyCode == Keys.RWin)
            {
                if (e.KeyCode == Keys.D)
                {
                    this.Activate();
                }
                SendKeys.Send("{Esc}");
                SendKeys.Send("{Esc}");
                e.Handled = true;
            }
        }

        private void timerEntry_Tick(object sender, EventArgs e)
        {
            this.Activate();
            if (ListImage.Count < 3)
            {
                int id2 = dAEntry.GetIdImageEntrySecond(batchNameLocal, formid, pair);
                if (id2 > 0)
                {
                    //BOImage_Entry img2 = dAEntry.GetImage(id2);
                    //img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img2.PageUrl, batchNameLocal)));
                    //ListImage.Add(img2);
                }
            }
        }

        private void frmEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void chkAutoComplete_Click(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void spc_Enter(object sender, EventArgs e)
        {
            SplitContainer p = (SplitContainer)(sender);
            p.BackColor = Color.PowderBlue;
        }

        private void spc_Leave(object sender, EventArgs e)
        {
            SplitContainer p = (SplitContainer)(sender);
            p.BackColor = SystemColors.Control;
        }

        private void spc_KeyDown(object sender, KeyEventArgs e)
        {
            SplitContainer p = (SplitContainer)(sender);
            if (e.KeyCode == Keys.Right)
            {
                lstSPC[p.TabIndex == 4 ? 0 : p.TabIndex].Focus();

            }
            else if (e.KeyCode == Keys.Left)
            {
                lstSPC[p.TabIndex == 1 ? 3 : p.TabIndex - 2].Focus();
            }           
        }
        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }
    }
}
