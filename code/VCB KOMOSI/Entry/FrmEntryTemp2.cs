﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
namespace VCB_KOMOSI
{
    public partial class FrmEntryTemp2 : Form
    {
        #region var
        public static Point locationOnForm;
        public static bool selecthitpoint = true;
        public int batchId;
        public int userId;
        public int formid;
        public int pair;
        public string userName;
        public string batchName;
        public string template;
        public string level;
        public string group;
        public string formName = "";
        Bitmap imageSource;
        private DAEntry_Entry dAEntry = new DAEntry_Entry();
        private BOImage_Entry img = new BOImage_Entry();
        private bool finish = false;
        int[] INFImage;
        public static int locationX = 0;
        public static int locationY = 0;
        float currentzoom;
        List<Label> lstLblSubFieldName = new List<Label>();
        List<Label> lstLblSubFieldLimit = new List<Label>();
        List<BOImage_Entry> ListImage = new List<BOImage_Entry>();
        DBsql dbsql = new DBsql();
        DateTime dtimeBefore = new DateTime();
        string batchNameLocal;
        //nam 20/7 tao bien kiem tra chu cai dau thu muc
        string type = "";
        int typeyear = 1;
        string tableform;
        #endregion
        public FrmEntryTemp2()
        {
            InitializeComponent();
        }

        private void frmEntry_Load(object sender, EventArgs e)
        {
            foreach (InputLanguage lang in InputLanguage.InstalledInputLanguages)
                if (lang.LayoutName.ToUpper() == "JAPANESE")
                {
                    InputLanguage.CurrentInputLanguage = lang; break;
                }
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            // hiển thị thông tin user          
            txtInf.Text = "VCB Entry  --  UserName: " + userName + "  -- Template: " + template + "  --  Batch: " + batchName;
            //Lây thư mục gốc từ batchname         
            try
            {
                if (batchName[7] == 'A')
                {
                    type = "A";
                    
                }
                //nam 20/7 gan bien khi folder C
                if (batchName[7] == 'C')
                {
                    type = "C";
                    typeyear =Int32.Parse(batchName.Substring(16, 1)) ;
                }
            }
            catch { }
            batchNameLocal = batchName.Substring(7);
            //userid
            int uID = userId;

            // hien thi giua man hinh
            this.CenterToScreen();
            dAEntry = new DAEntry_Entry();

            // lấy id image free
            tableform = batchId + "--" + batchNameLocal + "--lv3";
            int id = 0;
            try
            {
                id = dAEntry.Get_IdImage(tableform, pair);
                if (id == 0)
                {
                    if (dAEntry.Count_UpHitpointLC(tableform) == 0)
                    { dAEntry.UpdateHitpoint_Batch(batchId, 2); }
                    else
                    {
                        if (dAEntry.Count_ImageExits(tableform, 1) == 0 && dAEntry.Count_ImageExits(tableform, 2) == 0 && dAEntry.Count_ImageExits(tableform, 3) == 0)
                        { dAEntry.UpdateHitpoint_Batch(batchId, 1); }
                    }
                    finish = true;
                    //MessageBox.Show(this,"đã hoàn thành nhập");
                    this.Close();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            timerEntry.Start();

            // lay image free
            try
            {
                img = dAEntry.GetImage(id, tableform);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            //Get Image
            try
            {
                //nam 20/7 cắt ảnh
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageUrl, batchNameLocal)));
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();

                if (type == "A")
                {
                    imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 53 / 100, 0, bm1.Width * 47 / 100, bm1.Height);
                }
                else
                {
                    if(typeyear==1)
                    imageViewerTR1.Image = CropBitmap(bm2, (bm2.Width * 40 / 100), (bm2.Height * 50 / 100), (bm2.Width * 50 / 100), (bm2.Height * 50 / 100));
                    else
                        imageViewerTR1.Image = CropBitmap(bm2, (bm2.Width * 40 / 100), 220, (bm2.Width * 50 / 100), (bm2.Height * 30 / 100));
                    imageViewerTR1.CurrentZoom = 1.2f;
                }
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Server chưa có ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // hiển thị tên field name          
            lblpagename.Text = img.PageUrl;
            lblpage.Text = dAEntry.ImageExist(tableform, pair).ToString();
            dtimeBefore = DateTime.Now;
        }

        //int a = 1;
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string imgContentContent = txtKana.Text;
            if (imgContentContent.Contains('*'))
            {
                btnNotgood_Click(sender, e);
                return;
            }
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            int lng = 0;
            if (imgContentContent.Length - 1 > 0)
            {
                lng = imgContentContent.Length - 1;
            }
            int uID = userId;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                //if (type == "C")
                //{
                    int pairtemp = 1;
                    if (pair == 1)
                    {
                        pairtemp = 2;
                    }
                    string getContent = dAEntry.getContentByPair(tableform, pairtemp, imgContentImageId);
                    if (getContent != "null")
                    {
                        if (getContent == imgContentContent)
                        {
                            dAEntry.Add_Content_LV3_Like(imgContentImageId, imgContentContent, imgContentUserId, lng, ms, pair, tableform);
                        }
                        else
                        {
                            dAEntry.Add_Content_LV3_Unlike(imgContentImageId, imgContentContent, imgContentUserId, lng, ms, pair, tableform);
                        }
                    }
                    else
                    {
                        dAEntry.Add_Content_LV3_Unlike(imgContentImageId, imgContentContent, imgContentUserId, lng, ms, pair, tableform);
                    }
                //}
                //else
                //{
                //    dAEntry.Add_Content_LV3_Unlike(imgContentImageId, imgContentContent, imgContentUserId, lng, ms, pair, tableform);
                //}
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // load image mới lên form
            // lấy id image free         
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                if (dAEntry.Count_UpHitpointLC(tableform) == 0)
                { dAEntry.UpdateHitpoint_Batch(batchId, 2); }
                else
                {
                    if (dAEntry.Count_ImageExits(tableform, 1) == 0 && dAEntry.Count_ImageExits(tableform, 2) == 0 && dAEntry.Count_ImageExits(tableform, 3) == 0)
                    { dAEntry.UpdateHitpoint_Batch(batchId, 1); }
                }
                finish = true;
                //MessageBox.Show(this,"đã hoàn thành nhập");
                this.Close();
                return;
            }

            try
            {
                //Get Image
                //imageSource = (Bitmap)img.Imagesource.Clone();
                //imageViewerTR1.Image = imageSource;

                //nam 20/7 cắt ảnh
                imageSource = img.Imagesource;
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();

                if (type == "A")
                {
                    imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 53 / 100, 0, bm1.Width * 47 / 100, bm1.Height);
                }
                else
                {
                    if (typeyear == 1)
                        imageViewerTR1.Image = CropBitmap(bm2, (bm2.Width * 40 / 100), (bm2.Height * 50 / 100), (bm2.Width * 50 / 100), (bm2.Height * 50 / 100));
                    else
                        imageViewerTR1.Image = CropBitmap(bm2, (bm2.Width * 40 / 100), 220, (bm2.Width * 50 / 100), (bm2.Height * 30 / 100));
                    //imageViewerTR1.CurrentZoom = currentzoom;
                }
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // hiển thị tên field name       
            lblpagename.Text = img.PageUrl;
            txtKana.Text = "";
            dtimeBefore = DateTime.Now;
            btnSubmit.Enabled = true;
            btnNotgood.Enabled = true;
            txtKana.Focus();
            lblpage.Text = dAEntry.ImageExist(tableform, pair).ToString();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmEntry_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerEntry.Stop();
            try
            {
                if (!finish)
                {
                    dAEntry.Return_HitpointEntry(img.Id, pair, tableform);
                    for (int i = 0; i < ListImage.Count; i++)
                    { dAEntry.Return_HitpointEntry(ListImage[i].Id, pair, tableform); }
                    VCB_KOMOSI.frmLogIn.fn = false;
                }
                else
                { VCB_KOMOSI.frmLogIn.fn = true; }
            }
            catch { }
        }

        private void btnNotgood_Click(object sender, EventArgs e)
        {
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            string imgContentContent = txtKana.Text;
            int lng = 0;
            if (imgContentContent.Length - 1 > 0)
            {
                lng = imgContentContent.Length - 1;
            }
            int uID = userId;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                dAEntry.Add_Content_NG_LV3(imgContentImageId, imgContentUserId, lng, ms, imgContentContent, pair, tableform);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // load image mới lên form
            // lấy id image free
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                if (dAEntry.Count_UpHitpointLC(tableform) == 0)
                { dAEntry.UpdateHitpoint_Batch(batchId, 2); }
                else
                {
                    if (dAEntry.Count_ImageExits(tableform, 1) == 0 && dAEntry.Count_ImageExits(tableform, 2) == 0 && dAEntry.Count_ImageExits(tableform, 3) == 0)
                    { dAEntry.UpdateHitpoint_Batch(batchId, 1); }
                }
                finish = true;
                //MessageBox.Show(this,"đã hoàn thành nhập");
                this.Close();
                return;
            }

            try
            {
                //Get Image
                //imageSource = (Bitmap)img.Imagesource.Clone();
                //imageViewerTR1.Image = imageSource;

                //nam 20/7 cắt ảnh
                imageSource = img.Imagesource;
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();

                if (type == "A")
                {
                    imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 53 / 100, 0, bm1.Width * 47 / 100, bm1.Height);
                }
                else
                {
                    //tr1:chữ kana tr2:giới tính
                    if (formid == 1)
                    {
                        imageViewerTR1.Image = CropBitmap(bm2, (bm2.Width * 30 / 100), 180, (bm2.Width * 70 / 100), (bm2.Height - 180));
                    }
                    else
                    {
                        if (typeyear == 1)
                            imageViewerTR1.Image = CropBitmap(bm2, (bm2.Width * 40 / 100), (bm2.Height * 50 / 100), (bm2.Width * 50 / 100), (bm2.Height * 50 / 100));
                        else
                            imageViewerTR1.Image = CropBitmap(bm2, (bm2.Width * 40 / 100), 220, (bm2.Width * 50 / 100), (bm2.Height * 30 / 100));
                    }
                }
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // hiển thị tên field name       
            dtimeBefore = DateTime.Now;
            lblpagename.Text = img.PageUrl;
            btnSubmit.Enabled = true;
            btnNotgood.Enabled = true;
            txtKana.Focus();
            txtKana.Text = "";
            lblpage.Text = dAEntry.ImageExist(tableform, pair).ToString();
        }

        private void chkFullimage_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void frmEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && btnSubmit.Enabled && btnNotgood.Enabled)
            {
                btnSubmit_Click(sender, e);
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Escape)
                this.Close();
            if (e.Control)
            {
                if (e.KeyCode == Keys.N && btnSubmit.Enabled && btnNotgood.Enabled)
                    btnNotgood_Click(sender, e);
                if (e.KeyCode == Keys.Up)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Down)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    imageViewerTR1.RotateImage("270");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Left)
                {
                    imageViewerTR1.RotateImage("90");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.F)
                {
                    imageViewerTR1.CurrentZoom = currentzoom;
                    e.Handled = true;
                }
            }
            if (e.KeyCode == Keys.LWin || e.KeyCode == Keys.RWin)
            {
                if (e.KeyCode == Keys.D)
                {
                    this.Activate();
                }
                SendKeys.Send("{Esc}");
                SendKeys.Send("{Esc}");
                e.Handled = true;
            }
        }


        private void txt1_Enter(object sender, EventArgs e)
        {
            txtKana.BackColor = SystemColors.Window;
        }

        private void txt2_Enter(object sender, EventArgs e)
        {
            txtKana.BackColor = Color.PowderBlue;

        }
        private void timerEntry_Tick(object sender, EventArgs e)
        {
            this.Activate();
            if (ListImage.Count < 3)
            {
                int id2 = dAEntry.Get_IdImage(tableform, pair);
                if (id2 > 0)
                {
                    BOImage_Entry img2 = dAEntry.GetImage(id2, tableform);
                    img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img2.PageUrl, batchNameLocal)));
                    ListImage.Add(img2);
                }
            }
        }

        private void frmEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txt1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txt2_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void txt1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }
        private const uint LOCALE_SYSTEM_DEFAULT = 0x0800;
        private const uint LCMAP_HALFWIDTH = 0x00400000;

        public static string ToHalfWidth(string fullWidth)
        {
            StringBuilder sb = new StringBuilder(256);
            LCMapString(LOCALE_SYSTEM_DEFAULT, LCMAP_HALFWIDTH, fullWidth, -1, sb, sb.Capacity);
            return sb.ToString();
        }
        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        private static extern int LCMapString(uint Locale, uint dwMapFlags, string lpSrcStr, int cchSrc, StringBuilder lpDestStr, int cchDest);

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
