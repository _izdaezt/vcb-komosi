﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
namespace VCB_KOMOSI
{
    public partial class frmEntry_Kojin : Form
    {
        #region var
        public static Point locationOnForm;
        public static bool selecthitpoint = true;
        public int batchId;
        public int userId;
        public int formid;
        public int pair;
        public string userName;
        public string batchName;
        public string template;
        public string level;
        public string group;
        public string formName = "";
        Bitmap imageSource;
        private DAEntry_Entry dAEntry = new DAEntry_Entry();
        private BOImage_Entry img = new BOImage_Entry();
        private bool finish = false;
        int[] INFImage;
        public static int locationX = 0;
        public static int locationY = 0;
        float currentzoom;
        // value bool Japan
        public static DataTable dtMBD;
        DataTable dtSubField = new DataTable();
        List<TextBox> lstTxt = new List<TextBox>();
        List<Label> lstLblSubFieldName = new List<Label>();
        List<Label> lstLblSubFieldLimit = new List<Label>();
        List<BOImage_Entry> ListImage = new List<BOImage_Entry>();
        DBsql dbsql = new DBsql();
        string batchNameLocal;
        DateTime dtimeBefore = new DateTime();

        //nam 20/7 tao bien kiem tra chu cai dau thu muc
        string type = "";
        int typeyear = 0;
        string tableform;
        string ocrSBD = "";
        string ocrNTN = "";

        #endregion
        public frmEntry_Kojin()
        {
            InitializeComponent();
        }

        private void frmEntry_Load(object sender, EventArgs e)
        {
            // hiển thị thông tin user          
            txtInf.Text = "VCB Entry  --  UserName: " + userName + "  -- Template: " + template + "  --  Batch: " + batchName;
            //Lây thư mục gốc từ batchname            
            if (batchName[7] == 'A')
            {
                //txt2.Enabled = false;
                //lbl2.Enabled = false;

                //nam 20/7 ẩn mục ngày sinh
                type = "A";
                txt2.Visible = false;
                lbl2.Visible = false;
                imageViewerTR2.Visible = false;
            }
            batchNameLocal = batchName.Substring(7);
            //nam 20/7 gan bien khi folder C
            if (batchName[7] == 'C')
            {
                type = "C";
                typeyear = Int32.Parse(batchName.Substring(16, 1));
            }
            //userid
            int uID = userId;

            // hien thi giua man hinh
            this.CenterToScreen();
            dAEntry = new DAEntry_Entry();

            // lấy id image free
            tableform = batchId + "--" + batchNameLocal;
            int id = 0;
            try
            {
                id = dAEntry.Get_IdImage(tableform, pair);
                if (id == 0)
                {
                    if (dAEntry.Count_kt_cten(tableform) == 0)
                    {
                        if (dAEntry.Count_UpHitpointLC_KojinLV1(tableform) == dAEntry.getEntryinBatch(tableform))
                        { dAEntry.UpdateHitpoint_Batch(batchId, 2); }
                        else if (dAEntry.Count_Content_KojinLV1(tableform, pair) == 0)
                        { dAEntry.UpdateHitpoint_Batch(batchId, 1); }
                    }
                    else
                    {
                        if (dAEntry.Count_ImageExits(tableform, 1) == 0 && dAEntry.Count_ImageExits(tableform, 2) == 0 && dAEntry.Count_ImageExits(tableform, 3) == 0)
                        { dAEntry.UpdateHitpoint_Batch(batchId, 1); }
                    }
                    finish = true;
                    //MessageBox.Show("đã hoàn thành nhập");
                    this.Close();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            timerEntry.Start();


            // lay image free
            try
            {
                img = dAEntry.GetImage(id, tableform);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            //Get Image
            try
            {
                //nam 20/7 cắt ảnh
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageUrl, batchNameLocal)));
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();

                if (type == "A")
                {
                    imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 20 / 80, 0, bm1.Width * 13 / 80, bm1.Height);
                }
                else
                {
                    if (typeyear == 2)
                    {
                        imageViewerTR1.Image = CropBitmap(bm1, (bm1.Width * 10 / 100), 150, (bm1.Width * 20 / 100), (bm1.Height - 150));
                        imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 25 / 100), 150, (bm2.Width * 20 / 100), (bm2.Height - 150));
                        imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 225, (bm1.Width * 15 / 100), (bm1.Height * 15 / 100));
                        imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 28 / 100), 225, (bm2.Width * 15 / 100), (bm2.Height * 15 / 100));
                    }
                    else
                    {
                        imageViewerTR1.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 150, (bm1.Width * 15 / 100), (bm1.Height - 150));
                        imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 32 / 100), 150, (bm2.Width * 15 / 100), (bm2.Height - 150));
                        if (typeyear == 1)
                        {
                            imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 225, (bm1.Width * 15 / 100), (bm1.Height * 20 / 100));
                            imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 31 / 100), 225, (bm2.Width * 15 / 100), (bm2.Height * 20 / 100));
                        }
                        else
                        {
                            imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 210, (bm1.Width * 15 / 100), (bm1.Height * 14 / 100));
                            imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 32 / 100), 210, (bm2.Width * 15 / 100), (bm2.Height * 14 / 100));
                        }

                        //imageViewerTR1.Image = CropBitmap(bm1, (bm1.Width * 5 / 100), 150, (bm1.Width * 15 / 100), (bm1.Height - 150));
                        //imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 25 / 100), 150, (bm2.Width * 15 / 100), (bm2.Height - 150));
                        //if (typeyear == 1)
                        //{
                        //    imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 225, (bm1.Width * 15 / 100), (bm1.Height * 20 / 100));
                        //    imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 31 / 100), 225, (bm2.Width * 15 / 100), (bm2.Height * 20 / 100));
                        //}
                        //else
                        //{
                        //    imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 5 / 100), 210, (bm1.Width * 15 / 100), (bm1.Height * 14 / 100));
                        //    imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 26 / 100), 210, (bm2.Width * 15 / 100), (bm2.Height * 14 / 100));
                        //}
                    }
                }
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Server chưa có ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            try
            {
                ocrSBD = img.ContentOCR[0];
                if (!ocrSBD.Contains('?'))
                {
                    imageViewerTR1.Visible = false;
                    txt1.Visible = false;
                    imageViewerTR3.Visible = false;
                    lblField1.Visible = false;
                }
                else
                {
                    imageViewerTR1.Visible = true;
                    txt1.Visible = true;
                    imageViewerTR3.Visible = true;
                    lblField1.Visible = true;
                }

                ocrNTN = img.ContentOCR[1];
                if (!ocrNTN.Contains('?'))
                {
                    imageViewerTR2.Visible = false;
                    txt2.Visible = false;
                    imageViewerTR4.Visible = false;
                    lbl2.Visible = false;
                }
                else
                {
                    imageViewerTR2.Visible = true;
                    txt2.Visible = true;
                    imageViewerTR4.Visible = true;
                    lbl2.Visible = true;
                }
            }
            catch
            { }

            // hiển thị tên field name         
            lblpagename.Text = img.PageUrl;
            dtimeBefore = DateTime.Now;
            lblpage.Text = (dAEntry.ImageExist(tableform, pair)).ToString();
            txt1.Focus();
        }
        //int a = 1;
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string imgContentContentTemp = txt1.Text + "|" + txt2.Text;
            string imgContentContent = "";
            if (!ocrSBD.Contains('?'))
            {
                imgContentContent = ocrSBD + "|" + txt2.Text;
            }
            else if (!ocrNTN.Contains('?'))
            {
                imgContentContent = txt1.Text + "|" + ocrNTN;
            }
            else
            {
                imgContentContent = txt1.Text + "|" + txt2.Text;
            }
            if (imgContentContent.Contains('*'))
            {
                btnNotgood_Click(sender, e);
                return;
            }
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            int lng = imgContentContentTemp.Length - 1;
            int uID = userId;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                int pairtemp = 1;
                if (pair == 1)
                {
                    pairtemp = 2;
                }
                string getContent = dAEntry.getContentByPair(tableform, pairtemp, imgContentImageId);
                if (getContent == imgContentContent)
                {
                    dAEntry.Add_Content_LV3_Like(imgContentImageId, imgContentContent, imgContentUserId, lng, ms, pair, tableform);
                }
                else
                {
                    dAEntry.Add_Content_LV3_Unlike(imgContentImageId, imgContentContent, imgContentUserId, lng, ms, pair, tableform);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // load image mới lên form
            // lấy id image free         
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                if (dAEntry.Count_kt_cten(tableform) == 0)
                {
                    if (dAEntry.Count_UpHitpointLC_KojinLV1(tableform) == dAEntry.getEntryinBatch(tableform))
                    { dAEntry.UpdateHitpoint_Batch(batchId, 2); }
                }
                else
                {
                    if (dAEntry.Count_ImageExits(tableform, 1) == 0 && dAEntry.Count_ImageExits(tableform, 2) == 0 && dAEntry.Count_ImageExits(tableform, 3) == 0)
                    { dAEntry.UpdateHitpoint_Batch(batchId, 1); }
                }
                finish = true;
                //MessageBox.Show(this,"đã hoàn thành nhập");
                this.Close();
                return;
            }

            try
            {

                //nam 20/7 cắt ảnh
                imageSource = img.Imagesource;
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();

                if (type == "A")
                {
                    imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 20 / 80, 0, bm1.Width * 13 / 80, bm1.Height);
                }
                else
                {
                    if (typeyear == 2)
                    {
                        imageViewerTR1.Image = CropBitmap(bm1, (bm1.Width * 10 / 100), 150, (bm1.Width * 20 / 100), (bm1.Height - 150));
                        imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 25 / 100), 150, (bm2.Width * 20 / 100), (bm2.Height - 150));
                        imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 225, (bm1.Width * 15 / 100), (bm1.Height * 15 / 100));
                        imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 28 / 100), 225, (bm2.Width * 15 / 100), (bm2.Height * 15 / 100));
                    }
                    else
                    {
                        imageViewerTR1.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 150, (bm1.Width * 15 / 100), (bm1.Height - 150));
                        imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 32 / 100), 150, (bm2.Width * 15 / 100), (bm2.Height - 150));
                        if (typeyear == 1)
                        {
                            imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 225, (bm1.Width * 15 / 100), (bm1.Height * 20 / 100));
                            imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 31 / 100), 225, (bm2.Width * 15 / 100), (bm2.Height * 20 / 100));
                        }
                        else
                        {
                            imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 210, (bm1.Width * 15 / 100), (bm1.Height * 14 / 100));
                            imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 32 / 100), 210, (bm2.Width * 15 / 100), (bm2.Height * 14 / 100));
                        }

                        //imageViewerTR1.Image = CropBitmap(bm1, (bm1.Width * 5 / 100), 150, (bm1.Width * 15 / 100), (bm1.Height - 150));
                        //imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 25 / 100), 150, (bm2.Width * 15 / 100), (bm2.Height - 150));
                        //if (typeyear == 1)
                        //{
                        //    imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 225, (bm1.Width * 15 / 100), (bm1.Height * 20 / 100));
                        //    imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 31 / 100), 225, (bm2.Width * 15 / 100), (bm2.Height * 20 / 100));
                        //}
                        //else
                        //{
                        //    imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 5 / 100), 210, (bm1.Width * 15 / 100), (bm1.Height * 14 / 100));
                        //    imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 26 / 100), 210, (bm2.Width * 15 / 100), (bm2.Height * 14 / 100));
                        //}
                    }
                }
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            try
            {
                ocrSBD = img.ContentOCR[0];
                if (!ocrSBD.Contains('?'))
                {
                    imageViewerTR1.Visible = false;
                    txt1.Visible = false;
                    imageViewerTR3.Visible = false;
                    lblField1.Visible = false;
                }
                else
                {
                    imageViewerTR1.Visible = true;
                    txt1.Visible = true;
                    imageViewerTR3.Visible = true;
                    lblField1.Visible = true;
                }

                ocrNTN = img.ContentOCR[1];
                if (!ocrNTN.Contains('?'))
                {
                    imageViewerTR2.Visible = false;
                    txt2.Visible = false;
                    imageViewerTR4.Visible = false;
                    lbl2.Visible = false;
                }
                else
                {
                    imageViewerTR2.Visible = true;
                    txt2.Visible = true;
                    imageViewerTR4.Visible = true;
                    lbl2.Visible = true;
                }
            }
            catch
            { }

            btnSubmit.Enabled = true;
            btnNotgood.Enabled = true;
            // hiển thị tên field name       
            dtimeBefore = DateTime.Now;
            lblpagename.Text = img.PageUrl;
            txt1.Text = ""; txt2.Text = "";
            lblpage.Text = (dAEntry.ImageExist(tableform, pair)).ToString();
            txt1.Focus();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmEntry_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerEntry.Stop();
            try
            {
                if (!finish)
                {
                    dAEntry.Return_HitpointEntry(img.Id, pair, tableform);
                    for (int i = 0; i < ListImage.Count; i++)
                    { dAEntry.Return_HitpointEntry(ListImage[i].Id, pair, tableform); }
                    VCB_KOMOSI.frmLogIn.fn = false;
                }
                else
                { VCB_KOMOSI.frmLogIn.fn = true; }
            }
            catch { }
            return;
        }

        private void btnNotgood_Click(object sender, EventArgs e)
        {
            string imgContentContentTemp = txt1.Text + "|" + txt2.Text;
            string imgContentContent = "";
            if (!ocrSBD.Contains('?'))
            {
                imgContentContent = ocrSBD + "|" + txt2.Text;
            }
            else if (!ocrNTN.Contains('?'))
            {
                imgContentContent = txt1.Text + "|" + ocrNTN;
            }
            else
            {
                imgContentContent = txt1.Text + "|" + txt2.Text;
            }
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            int lng = imgContentContentTemp.Length - 1;
            int uID = userId;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                dAEntry.Add_Content_NG_LV3(imgContentImageId, imgContentUserId, lng, ms, imgContentContent, pair, tableform);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            // load image mới lên form
            // lấy id image free
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                if (dAEntry.Count_kt_cten(tableform) == 0)
                {
                    if (dAEntry.Count_UpHitpointLC_KojinLV1(tableform) == dAEntry.getEntryinBatch(tableform))
                    { dAEntry.UpdateHitpoint_Batch(batchId, 2); }
                }
                else
                {
                    if (dAEntry.Count_ImageExits(tableform, 1) == 0 && dAEntry.Count_ImageExits(tableform, 2) == 0 && dAEntry.Count_ImageExits(tableform, 3) == 0)
                    { dAEntry.UpdateHitpoint_Batch(batchId, 1); }
                }
                finish = true;
                //MessageBox.Show(this,"đã hoàn thành nhập");
                this.Close();
                return;
            }

            try
            {

                //nam 20/7 cắt ảnh
                imageSource = img.Imagesource;
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();

                if (type == "A")
                {
                    imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 20 / 80, 0, bm1.Width * 13 / 80, bm1.Height);
                }
                else
                {
                    if (typeyear == 2)
                    {
                        imageViewerTR1.Image = CropBitmap(bm1, (bm1.Width * 10 / 100), 150, (bm1.Width * 20 / 100), (bm1.Height - 150));
                        imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 25 / 100), 150, (bm2.Width * 20 / 100), (bm2.Height - 150));
                        imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 225, (bm1.Width * 15 / 100), (bm1.Height * 15 / 100));
                        imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 28 / 100), 225, (bm2.Width * 15 / 100), (bm2.Height * 15 / 100));
                    }
                    else
                    {
                        imageViewerTR1.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 150, (bm1.Width * 15 / 100), (bm1.Height - 150));
                        imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 32 / 100), 150, (bm2.Width * 15 / 100), (bm2.Height - 150));
                        if (typeyear == 1)
                        {
                            imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 225, (bm1.Width * 15 / 100), (bm1.Height * 20 / 100));
                            imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 31 / 100), 225, (bm2.Width * 15 / 100), (bm2.Height * 20 / 100));
                        }
                        else
                        {
                            imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 210, (bm1.Width * 15 / 100), (bm1.Height * 14 / 100));
                            imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 32 / 100), 210, (bm2.Width * 15 / 100), (bm2.Height * 14 / 100));
                        }

                        //imageViewerTR1.Image = CropBitmap(bm1, (bm1.Width * 5 / 100), 150, (bm1.Width * 15 / 100), (bm1.Height - 150));
                        //imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 25 / 100), 150, (bm2.Width * 15 / 100), (bm2.Height - 150));
                        //if (typeyear == 1)
                        //{
                        //    imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 12 / 100), 225, (bm1.Width * 15 / 100), (bm1.Height * 20 / 100));
                        //    imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 31 / 100), 225, (bm2.Width * 15 / 100), (bm2.Height * 20 / 100));
                        //}
                        //else
                        //{
                        //    imageViewerTR3.Image = CropBitmap(bm1, (bm1.Width * 5 / 100), 210, (bm1.Width * 15 / 100), (bm1.Height * 14 / 100));
                        //    imageViewerTR4.Image = CropBitmap(bm2, (bm2.Width * 26 / 100), 210, (bm2.Width * 15 / 100), (bm2.Height * 14 / 100));
                        //}
                    }
                }
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            try
            {
                ocrSBD = img.ContentOCR[0];
                if (!ocrSBD.Contains('?'))
                {
                    imageViewerTR1.Visible = false;
                    txt1.Visible = false;
                    imageViewerTR3.Visible = false;
                    lblField1.Visible = false;
                }
                else
                {
                    imageViewerTR1.Visible = true;
                    txt1.Visible = true;
                    imageViewerTR3.Visible = true;
                    lblField1.Visible = true;
                }

                ocrNTN = img.ContentOCR[1];
                if (!ocrNTN.Contains('?'))
                {
                    imageViewerTR2.Visible = false;
                    txt2.Visible = false;
                    imageViewerTR4.Visible = false;
                    lbl2.Visible = false;
                }
                else
                {
                    imageViewerTR2.Visible = true;
                    txt2.Visible = true;
                    imageViewerTR4.Visible = true;
                    lbl2.Visible = true;
                }
            }
            catch
            { }

            // hiển thị tên field name
            btnNotgood.Enabled = true;
            btnSubmit.Enabled = true;
            dtimeBefore = DateTime.Now;
            lblpagename.Text = img.PageUrl;
            lblpage.Text = (dAEntry.ImageExist(tableform, pair)).ToString();
            txt1.Text = ""; txt2.Text = "";
            txt1.Focus();
        }

        private void chkFullimage_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void frmEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && btnSubmit.Enabled && btnNotgood.Enabled)
            {
                btnSubmit_Click(sender, e);
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Escape)
                this.Close();
            if (e.Control)
            {
                if (e.KeyCode == Keys.N && btnNotgood.Enabled && btnSubmit.Enabled)
                    btnNotgood_Click(sender, e);
                if (e.Control)
                {
                    if (e.KeyCode == Keys.Up)
                    {
                        imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
                        e.Handled = true;
                    }
                    else if (e.KeyCode == Keys.Down)
                    {
                        imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
                        e.Handled = true;
                    }
                    else if (e.KeyCode == Keys.Right)
                    {
                        imageViewerTR1.RotateImage("270");
                        e.Handled = true;
                    }
                    else if (e.KeyCode == Keys.Left)
                    {
                        imageViewerTR1.RotateImage("90");
                        e.Handled = true;
                    }
                    else if (e.KeyCode == Keys.F)
                    {
                        imageViewerTR1.CurrentZoom = currentzoom;
                        e.Handled = true;
                    }
                }
                if (e.KeyCode == Keys.Escape)
                    this.Close();
            }
            if (e.KeyCode == Keys.LWin || e.KeyCode == Keys.RWin)
            {
                if (e.KeyCode == Keys.D)
                {
                    this.Activate();
                }
                SendKeys.Send("{Esc}");
                SendKeys.Send("{Esc}");
                e.Handled = true;
            }
        }


        private void txt1_Enter(object sender, EventArgs e)
        {
            txt1.BackColor = Color.PowderBlue;
            txt2.BackColor = SystemColors.Window;
        }

        private void txt2_Enter(object sender, EventArgs e)
        {
            txt2.BackColor = Color.PowderBlue;
            txt1.BackColor = SystemColors.Window;
        }
        private void timerEntry_Tick(object sender, EventArgs e)
        {
            this.Activate();
            if (ListImage.Count < 4)
            {
                int id2 = dAEntry.Get_IdImage(tableform, pair);
                if (id2 > 0)
                {
                    BOImage_Entry img2 = dAEntry.GetImage(id2, tableform);
                    img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img2.PageUrl, batchNameLocal)));
                    ListImage.Add(img2);
                }
            }
        }

        private void frmEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txt1_TextChanged(object sender, EventArgs e)
        {
            if (txt1.Text.Length == 6)
                txt2.Focus();
        }

        private void imageViewerTR1_Load(object sender, EventArgs e)
        {


        }
        //nam 20/7 hàm cắt ảnh        
        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }

    }
}
