﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
namespace VCB_KOMOSI
{
    public partial class FrmEntryTemp4_2 : Form
    {
        #region var
        public static Point locationOnForm;
        public static bool selecthitpoint = true;
        public string[] INFperformance = new string[2];
        public int batchId;
        public int userId;
        public int formid;
        public int pair;
        private double scale = 1;
        public string userName;
        public string batchName;
        public string template;
        public string level;
        public string group;
        public string formName = "";
        Image imageOriginal = null;
        Bitmap imageSource;
        float currentzoom;
        private DAEntry_Entry dAEntry = new DAEntry_Entry();
        private BOImage_Entry img = new BOImage_Entry();
        private bool finish = false;
        bool eter = false;
        bool space = false;
        bool check = false;
        int[] INFImage;
        public static int locationX = 0;
        public static int locationY = 0;
        double tyle;
        double tyleImageNew = 1;
        Bitmap bm_out;
        int zom = 11;
        // value bool Japan
        bool jp = true;
        public static DataTable dtMBD;
        DataTable dtSubField = new DataTable();
        List<TextBox> lsttxt = new List<TextBox>();
        List<TextBox> lsttxt1 = new List<TextBox>();
        List<TextBox> lsttxt2 = new List<TextBox>();
        List<TextBox> lsttxt3 = new List<TextBox>();
        List<TextBox> lsttxt4 = new List<TextBox>();
        List<Label> lstLblSubFieldName = new List<Label>();
        List<Label> lstLblSubFieldLimit = new List<Label>();
        List<BOImage_Entry> ListImage = new List<BOImage_Entry>();
        DataRow[] subField = null;
        string pathLog = "";
        DBsql dbsql = new DBsql();
        bool isErrorCong = false;
        int positionField;
        int limitTime = 0;
        int focus = 0;
        DateTime dtimeBefore = new DateTime();
        string batchNameLocal;
        #endregion
        public FrmEntryTemp4_2()
        {
            InitializeComponent();
            lsttxt = new List<TextBox>() { txt1, txt2, txt3, txt4 };
            lsttxt1 = new List<TextBox>() { txt111, txt112, txt113, txt114, txt115, txt116, txt117 };
            lsttxt2 = new List<TextBox>() { txtl21, txtl22, txtl23, txtl24, txtl25, txtl26, txtl27 };
            lsttxt3 = new List<TextBox>() { txtl31, txtl32, txtl33, txtl34, txtl35, txtl36, txtl37 };
            lsttxt4 = new List<TextBox>() { txtl41, txtl42, txtl43, txtl44, txtl45, txtl46, txtl47, txtl48 };
        }

        private void frmEntry_Load(object sender, EventArgs e)
        {
            txtInf.Text = "VCB Entry  --  UserName: " + userName + "  -- Template: " + template + "  --  Batch: " + batchName;
            lsttxt.ForEach(a =>
            {
                a.Enter += new System.EventHandler(this.textBox1_Enter);
                a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
                a.Leave += new System.EventHandler(this.textBox1_Leave);
                a.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
                a.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            });
            //Lây thư mục gốc từ batchname
          
            //userid
            int uID = userId;
            // hien thi giua man hinh
            this.CenterToScreen();
            dAEntry = new DAEntry_Entry();
            batchNameLocal = batchName.Substring(7);
            // lấy id image free
            int id = 0;
            try
            {
                id = dAEntry.GetIdImageEntrySecond(batchNameLocal, formid, pair);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            if (id == 0)
            {
                finish = true;
                this.Close();
                return;
            }

            timerEntry.Start();

            // lay image free
            try
            {
                //img = dAEntry.GetImage(id);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            //Get Image
            try
            {
                //imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageUrl, batchName.Substring(7, batchName.Length - 7))));
                //imageViewerTR1.Image = imageSource;
                //currentzoom = imageViewerTR1.CurrentZoom;

                //nam 21/7 cắt ảnh
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageUrl, batchNameLocal)));
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 8 / 100, bm1.Height * 18 / 100, (bm1.Width * 40 / 100), (bm1.Height * 52 / 100));
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Server chưa có ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // hiển thị tên field name       
            lblpagename.Text = img.PageUrl;
            //lblpage.Text =dAEntry.ImageExist(batchNameLocal, pair, formid).ToString();
            dtimeBefore = DateTime.Now;
            txt1.Focus();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (e.KeyCode == Keys.Down || e.KeyCode == Keys.S)
            {
                if (tb.TabIndex != 4)
                {
                    lsttxt[tb.TabIndex].Focus();
                    e.Handled = true;
                }
                if (tb.TabIndex == 4)
                {
                    lsttxt[0].Focus();
                    e.Handled = true;
                }
            }
            else if (e.KeyCode == Keys.Up || e.KeyCode == Keys.W)
            {
                if (tb.TabIndex == 1)
                {
                    lsttxt[3].Focus();
                    e.Handled = true;
                }
                else if (tb.TabIndex > 1 && tb.TabIndex <= 4)
                {
                    lsttxt[tb.TabIndex - 2].Focus();
                    e.Handled = true;
                }
            }
            else if (e.KeyCode == Keys.Right || e.KeyCode == Keys.D)
            {
                if (tb.TabIndex >= 1 && tb.TabIndex < 4)
                {
                    lsttxt[tb.TabIndex].Focus();
                    e.Handled = true;
                }
                else if (tb.TabIndex == 4)
                {
                    lsttxt[0].Focus();
                }
            }
            else if (e.KeyCode == Keys.Left || e.KeyCode == Keys.A)
            {
                if (tb.TabIndex > 1 && tb.TabIndex <= 4)
                {
                    lsttxt[tb.TabIndex - 2].Focus();
                }
                if (tb.TabIndex == 1)
                {
                    lsttxt[3].Focus();
                }
            }
            else if (e.KeyCode == Keys.Oemtilde)
            {
                if (tb.TabIndex > 1)
                {
                    lsttxt[tb.TabIndex - 2].Focus();
                    e.Handled = true;
                }
                else if (tb.TabIndex == 1)
                {
                    lsttxt[3].Focus();
                    e.Handled = true;
                }
            }
            if (tb.TabIndex == 1)
            {
                if (e.KeyCode == Keys.NumPad1)
                {
                    if (lsttxt1[0].BackColor == SystemColors.Window)
                    {
                        lsttxt1[0].BackColor = Color.YellowGreen;
                        lsttxt1[1].BackColor = SystemColors.Window;
                    }
                    else
                    {
                        lsttxt1[0].BackColor = SystemColors.Window;
                    }
                }
                if (e.KeyCode == Keys.NumPad2)
                {
                    if (lsttxt1[1].BackColor == SystemColors.Window)
                    {
                        lsttxt1[1].BackColor = Color.YellowGreen;
                        lsttxt1[0].BackColor = SystemColors.Window;
                    }
                    else
                    {
                        lsttxt1[1].BackColor = SystemColors.Window;
                    }
                }
                if (e.KeyCode == Keys.NumPad3)
                {
                    if (lsttxt1[2].BackColor == SystemColors.Window)
                    {
                        lsttxt1[2].BackColor = Color.YellowGreen;
                        lsttxt1[3].BackColor = SystemColors.Window;
                        lsttxt1[4].BackColor = SystemColors.Window;
                        lsttxt1[5].BackColor = SystemColors.Window;
                        lsttxt1[6].BackColor = SystemColors.Window;
                    }
                    else
                    {
                        lsttxt1[2].BackColor = SystemColors.Window;
                    }
                }
                if (e.KeyCode == Keys.NumPad4)
                {
                    if (lsttxt1[3].BackColor == SystemColors.Window)
                    {
                        lsttxt1[3].BackColor = Color.YellowGreen;
                        lsttxt1[2].BackColor = SystemColors.Window;
                        lsttxt1[4].BackColor = SystemColors.Window;
                        lsttxt1[5].BackColor = SystemColors.Window;
                        lsttxt1[6].BackColor = SystemColors.Window;
                    }
                    else
                    {
                        lsttxt1[3].BackColor = SystemColors.Window;
                    }
                }
                if (e.KeyCode == Keys.NumPad5)
                {
                    if (lsttxt1[4].BackColor == SystemColors.Window)
                    {
                        lsttxt1[4].BackColor = Color.YellowGreen;
                        lsttxt1[3].BackColor = SystemColors.Window;
                        lsttxt1[2].BackColor = SystemColors.Window;
                        lsttxt1[5].BackColor = SystemColors.Window;
                        lsttxt1[6].BackColor = SystemColors.Window;
                    }
                    else
                    {
                        lsttxt1[4].BackColor = SystemColors.Window;
                    }
                }
                if (e.KeyCode == Keys.NumPad6)
                {
                    if (lsttxt1[5].BackColor == SystemColors.Window)
                    {
                        lsttxt1[5].BackColor = Color.YellowGreen;
                        lsttxt1[3].BackColor = SystemColors.Window;
                        lsttxt1[4].BackColor = SystemColors.Window;
                        lsttxt1[2].BackColor = SystemColors.Window;
                        lsttxt1[6].BackColor = SystemColors.Window;
                    }
                    else
                    {
                        lsttxt1[5].BackColor = SystemColors.Window;
                    }
                }
                if (e.KeyCode == Keys.NumPad7)
                {
                    if (lsttxt1[6].BackColor == SystemColors.Window)
                    {
                        lsttxt1[6].BackColor = Color.YellowGreen;
                        lsttxt1[3].BackColor = SystemColors.Window;
                        lsttxt1[4].BackColor = SystemColors.Window;
                        lsttxt1[5].BackColor = SystemColors.Window;
                        lsttxt1[2].BackColor = SystemColors.Window;
                    }
                    else
                    {
                        lsttxt1[6].BackColor = SystemColors.Window;
                    }
                }
            }
            if (tb.TabIndex == 2)
            {
                if (e.KeyValue > 96 && e.KeyValue < 104)
                {
                    if (lsttxt2[e.KeyValue - 97].BackColor == SystemColors.Window)
                    {
                        lsttxt2[e.KeyValue - 97].BackColor = Color.YellowGreen;
                    }
                    else
                    {
                        lsttxt2[e.KeyValue - 97].BackColor = SystemColors.Window;
                    }
                }
            }
            if (tb.TabIndex == 3)
            {
                if (e.KeyValue > 96 && e.KeyValue < 104)
                {
                    if (lsttxt3[e.KeyValue - 97].BackColor == SystemColors.Window)
                    {
                        lsttxt3[e.KeyValue - 97].BackColor = Color.YellowGreen;
                    }
                    else
                    {
                        lsttxt3[e.KeyValue - 97].BackColor = SystemColors.Window;
                    }
                }
            }
            if (tb.TabIndex == 4)
            {
                if (e.KeyValue > 96 && e.KeyValue < 105)
                {
                    if (lsttxt4[e.KeyValue - 97].BackColor == SystemColors.Window)
                    {
                        lsttxt4[e.KeyValue - 97].BackColor = Color.YellowGreen;
                    }
                    else
                    {
                        lsttxt4[e.KeyValue - 97].BackColor = SystemColors.Window;
                    }
                }
            }
        }
        private void textBox1_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.BackColor = Color.PowderBlue;
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.BackColor = SystemColors.Window;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
        }
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            string[] arr1 = new string[2];
            string[] arr2 = new string[7];
            string[] arr3 = new string[7];
            string[] arr4 = new string[8];
            int countchars1 = 0;
            int countchars2 = 0;
            int countchars3 = 0;
            int countchars4 = 0;
            arr1[0] = (lsttxt1[0].BackColor == Color.YellowGreen ? lsttxt1[0].Text : "") + (lsttxt1[1].BackColor == Color.YellowGreen ? lsttxt1[1].Text : "");
            arr1[1] = (lsttxt1[2].BackColor == Color.YellowGreen ? lsttxt1[2].Text : "") + (lsttxt1[3].BackColor == Color.YellowGreen ? lsttxt1[3].Text : "") +
                      (lsttxt1[4].BackColor == Color.YellowGreen ? lsttxt1[4].Text : "") + (lsttxt1[5].BackColor == Color.YellowGreen ? lsttxt1[5].Text : "") +
                      (lsttxt1[6].BackColor == Color.YellowGreen ? lsttxt1[6].Text : "");
            countchars1 = arr1[0].Length + arr1[1].Length;
            for (int i = 0; i < 8; i++)
            {
                if (i < 7)
                {
                    if (lsttxt2[i].BackColor == Color.YellowGreen)
                    {
                        arr2[i] = lsttxt2[i].Text;
                        countchars2++;
                    }

                    if (lsttxt3[i].BackColor == Color.YellowGreen)
                    {
                        arr3[i] = lsttxt3[i].Text;
                        countchars3++;
                    }
                }
                if (lsttxt4[i].BackColor == Color.YellowGreen)
                {
                    arr4[i] = lsttxt4[i].Text;
                    countchars4++;
                }
            }
            string imgContent1 = String.Join("|", arr1);
            string imgContent2 = String.Join("|", arr2);
            string imgContent3 = String.Join("|", arr3);
            string imgContent4 = String.Join("|", arr4);
            string imgContentContent2 = imgContent1 + "|" + imgContent2 + "|" + imgContent3 + "|" + imgContent4;           
            int lng = countchars1 + countchars2 + countchars3 + countchars4;
            int uID = userId;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                dAEntry.Add_Content_P2(imgContentImageId, imgContentContent2, imgContentUserId, lng, ms);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // load image mới lên form
            // lấy id image free         
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                dAEntry.SetHitpointBatch(batchNameLocal, formid);
                finish = true;
                this.Close();
                return;
            }

            try
            {
                ////Get Image
                //imageSource = (Bitmap)img.Imagesource.Clone();
                //imageViewerTR1.Image = imageSource;
                //nam 21/7 cắt ảnh
                imageSource = img.Imagesource;
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 8 / 100, bm1.Height * 18 / 100, (bm1.Width * 40 / 100), (bm1.Height * 52 / 100));
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            btnSubmit.Enabled = true;
            btnNotgood.Enabled = true;
            // hiển thị tên field name        
            lblpagename.Text = img.PageUrl;
            dtimeBefore = DateTime.Now;
            txt1.Focus();
            //lblpage.Text = dAEntry.ImageExist(batchNameLocal, pair, formid).ToString();
            lsttxt1.ForEach(s => s.BackColor = SystemColors.Window);
            lsttxt2.ForEach(s => s.BackColor = SystemColors.Window);
            lsttxt3.ForEach(s => s.BackColor = SystemColors.Window);
            lsttxt4.ForEach(s => s.BackColor = SystemColors.Window);
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmEntry_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerEntry.Stop();
            try
            {
                if (!finish)
                {
                    dAEntry.ReturnPairANDHitpointEntry(img.Id, pair);
                    // unlock image       
                    for (int i = 0; i < ListImage.Count; i++)
                    {
                        dAEntry.ReturnPairANDHitpointEntry(ListImage[i].Id, pair);
                    }
                }
                else
                {
                    frmLogIn.fn = true;
                    dAEntry.SetHitpointBatch(batchNameLocal, formid);
                }

            }
            catch { }
        }

        private void btnNotgood_Click(object sender, EventArgs e)
        {
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            string[] arr1 = new string[2];
            string[] arr2 = new string[lsttxt2.Count];
            string[] arr3 = new string[lsttxt3.Count];
            string[] arr4 = new string[lsttxt4.Count];
            int countchars1 = 0;
            int countchars2 = 0;
            int countchars3 = 0;
            int countchars4 = 0;
            arr1[0] = (lsttxt1[0].BackColor == Color.YellowGreen ? lsttxt1[0].Text : "") + (lsttxt1[1].BackColor == Color.YellowGreen ? lsttxt1[1].Text : "");
            arr1[1] = (lsttxt1[2].BackColor == Color.YellowGreen ? lsttxt1[2].Text : "") + (lsttxt1[3].BackColor == Color.YellowGreen ? lsttxt1[3].Text : "") +
                      (lsttxt1[4].BackColor == Color.YellowGreen ? lsttxt1[4].Text : "") + (lsttxt1[5].BackColor == Color.YellowGreen ? lsttxt1[5].Text : "") +
                      (lsttxt1[6].BackColor == Color.YellowGreen ? lsttxt1[6].Text : "");
            countchars1 = arr1[0].Length + arr1[1].Length;
            for (int i = 0; i < 8; i++)
            {
                if (i < 7)
                {
                    if (lsttxt2[i].BackColor == Color.YellowGreen)
                    {
                        arr2[i] = lsttxt2[i].Text;
                        countchars2++;
                    }

                    if (lsttxt3[i].BackColor == Color.YellowGreen)
                    {
                        arr3[i] = lsttxt3[i].Text;
                        countchars3++;
                    }
                }
                if (lsttxt4[i].BackColor == Color.YellowGreen)
                {
                    arr4[i] = lsttxt4[i].Text;
                    countchars4++;
                }
            }
            string imgContent1 = String.Join("|", arr1);
            string imgContent2 = String.Join("|", arr2);
            string imgContent3 = String.Join("|", arr3);
            string imgContent4 = String.Join("|", arr4);
            string imgContentContent2 = imgContent1 + "|" + imgContent2 + "|" + imgContent3 + "|" + imgContent4;
            int lng = countchars1 + countchars2 + countchars3 + countchars4;
            int uID = userId;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                dAEntry.Add_Content_NG_P2(imgContentImageId, imgContentContent2, imgContentUserId, lng, ms);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // load image mới lên form
            // lấy id image free
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                dAEntry.SetHitpointBatch(batchNameLocal, formid);
                finish = true;
                this.Close();
                return;
            }

            try
            {
                ////Get Image
                //imageSource = (Bitmap)img.Imagesource.Clone();
                //imageViewerTR1.Image = imageSource;
                //nam 21/7 cắt ảnh
                imageSource = img.Imagesource;
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 8 / 100, bm1.Height * 18 / 100, (bm1.Width * 40 / 100), (bm1.Height * 52 / 100));
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // hiển thị tên field name        
            btnSubmit.Enabled = true;
            btnNotgood.Enabled = true;
            dtimeBefore = DateTime.Now;
            lblpagename.Text = img.PageUrl;
            txt1.Focus();
            //lblpage.Text = dAEntry.ImageExist(batchNameLocal, pair, formid).ToString();
            lsttxt1.ForEach(s => s.BackColor = SystemColors.Window);
            lsttxt2.ForEach(s => s.BackColor = SystemColors.Window);
            lsttxt3.ForEach(s => s.BackColor = SystemColors.Window);
            lsttxt4.ForEach(s => s.BackColor = SystemColors.Window);
        }

        private void chkFullimage_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void frmEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && btnSubmit.Enabled && btnNotgood.Enabled)
            {
                btnSubmit_Click(sender, e);
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Escape)
                this.Close();
            if (e.Control)
            {
                if (e.KeyCode == Keys.N && btnSubmit.Enabled && btnNotgood.Enabled)
                    btnNotgood_Click(sender, e);
                if (e.KeyCode == Keys.Up)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Down)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    imageViewerTR1.RotateImage("270");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Left)
                {
                    imageViewerTR1.RotateImage("90");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.F)
                {
                    imageViewerTR1.CurrentZoom = currentzoom;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.A)
                {
                    for (int i = 0; i < subField.Length; i++)
                    {
                        lsttxt[i].Select(0, lsttxt[i].Text.Length);
                    }
                }
            }
            if (e.KeyCode == Keys.LWin || e.KeyCode == Keys.RWin)
            {
                if (e.KeyCode == Keys.D)
                {
                    this.Activate();
                }
                SendKeys.Send("{Esc}");
                SendKeys.Send("{Esc}");
                e.Handled = true;
            }
        }

        private void timerEntry_Tick(object sender, EventArgs e)
        {
            this.Activate();
            if (ListImage.Count < 3)
            {
                int id2 = dAEntry.GetIdImageEntrySecond(batchNameLocal, formid, pair);
                if (id2 > 0)
                {
                    //BOImage_Entry img2 = dAEntry.GetImage(id2);
                    //img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img2.PageUrl, batchNameLocal)));
                    //ListImage.Add(img2);
                }
            }
        }

        private void frmEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtInf_Click(object sender, EventArgs e)
        {

        }
        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }
    }
}
