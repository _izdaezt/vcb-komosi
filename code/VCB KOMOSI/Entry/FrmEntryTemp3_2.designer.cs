﻿namespace VCB_KOMOSI
{
    partial class FrmEntryTemp3_2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEntryTemp3_2));
            this.txtOCR = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.lblpage = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.lblpagename = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.lblField = new System.Windows.Forms.ToolStripLabel();
            this.txtInf = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.lblerror = new System.Windows.Forms.ToolStripLabel();
            this.timerEntry = new System.Windows.Forms.Timer(this.components);
            this.btnExit = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnNotgood = new System.Windows.Forms.Button();
            this.lblPressSubmit = new System.Windows.Forms.Label();
            this.imageViewerTR1 = new ImageViewerTR.ImageViewerTR();
            this.panel1 = new System.Windows.Forms.Panel();
            this.spc4 = new System.Windows.Forms.SplitContainer();
            this.lbl43 = new System.Windows.Forms.Label();
            this.lbl47 = new System.Windows.Forms.Label();
            this.lbl42 = new System.Windows.Forms.Label();
            this.lbl41 = new System.Windows.Forms.Label();
            this.lbl44 = new System.Windows.Forms.Label();
            this.lbl45 = new System.Windows.Forms.Label();
            this.lbl46 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.spc3 = new System.Windows.Forms.SplitContainer();
            this.lbl37 = new System.Windows.Forms.Label();
            this.lbl36 = new System.Windows.Forms.Label();
            this.lbl35 = new System.Windows.Forms.Label();
            this.lbl34 = new System.Windows.Forms.Label();
            this.lbl33 = new System.Windows.Forms.Label();
            this.lbl32 = new System.Windows.Forms.Label();
            this.lbl31 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.spc2 = new System.Windows.Forms.SplitContainer();
            this.lbl24 = new System.Windows.Forms.Label();
            this.lbl23 = new System.Windows.Forms.Label();
            this.lbl22 = new System.Windows.Forms.Label();
            this.lbl21 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.spc1 = new System.Windows.Forms.SplitContainer();
            this.label7 = new System.Windows.Forms.Label();
            this.lbl11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl12 = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spc4)).BeginInit();
            this.spc4.Panel1.SuspendLayout();
            this.spc4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spc3)).BeginInit();
            this.spc3.Panel1.SuspendLayout();
            this.spc3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spc2)).BeginInit();
            this.spc2.Panel1.SuspendLayout();
            this.spc2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spc1)).BeginInit();
            this.spc1.Panel1.SuspendLayout();
            this.spc1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOCR
            // 
            this.txtOCR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOCR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtOCR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtOCR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtOCR.Location = new System.Drawing.Point(1179, 736);
            this.txtOCR.Multiline = true;
            this.txtOCR.Name = "txtOCR";
            this.txtOCR.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOCR.Size = new System.Drawing.Size(486, 265);
            this.txtOCR.TabIndex = 51;
            this.txtOCR.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.lblpage,
            this.toolStripLabel2,
            this.lblpagename,
            this.toolStripLabel3,
            this.lblField,
            this.txtInf,
            this.toolStripLabel4,
            this.lblerror});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1350, 25);
            this.toolStrip1.TabIndex = 60;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(22, 22);
            this.toolStripLabel1.Text = "     ";
            // 
            // lblpage
            // 
            this.lblpage.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblpage.ForeColor = System.Drawing.Color.Red;
            this.lblpage.Name = "lblpage";
            this.lblpage.Size = new System.Drawing.Size(17, 22);
            this.lblpage.Text = "0";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(22, 22);
            this.toolStripLabel2.Text = "     ";
            // 
            // lblpagename
            // 
            this.lblpagename.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblpagename.ForeColor = System.Drawing.Color.Blue;
            this.lblpagename.Name = "lblpagename";
            this.lblpagename.Size = new System.Drawing.Size(67, 22);
            this.lblpagename.Text = "PageName";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(22, 22);
            this.toolStripLabel3.Text = "     ";
            // 
            // lblField
            // 
            this.lblField.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblField.ForeColor = System.Drawing.Color.Blue;
            this.lblField.Name = "lblField";
            this.lblField.Size = new System.Drawing.Size(0, 22);
            // 
            // txtInf
            // 
            this.txtInf.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInf.ForeColor = System.Drawing.Color.Blue;
            this.txtInf.Name = "txtInf";
            this.txtInf.Size = new System.Drawing.Size(39, 22);
            this.txtInf.Text = "Batch";
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(22, 22);
            this.toolStripLabel4.Text = "     ";
            // 
            // lblerror
            // 
            this.lblerror.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblerror.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblerror.ForeColor = System.Drawing.Color.Red;
            this.lblerror.Name = "lblerror";
            this.lblerror.Size = new System.Drawing.Size(0, 22);
            // 
            // timerEntry
            // 
            this.timerEntry.Enabled = true;
            this.timerEntry.Interval = 200;
            this.timerEntry.Tick += new System.EventHandler(this.timerEntry_Tick);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(1317, 1);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(33, 24);
            this.btnExit.TabIndex = 81;
            this.btnExit.TabStop = false;
            this.btnExit.Text = "X";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(1159, 542);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 16);
            this.label2.TabIndex = 111;
            this.label2.Text = "Ctr + N";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSubmit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSubmit.BackgroundImage")));
            this.btnSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSubmit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnSubmit.Location = new System.Drawing.Point(979, 561);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(129, 55);
            this.btnSubmit.TabIndex = 108;
            this.btnSubmit.TabStop = false;
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnNotgood
            // 
            this.btnNotgood.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNotgood.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNotgood.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnNotgood.ForeColor = System.Drawing.Color.Red;
            this.btnNotgood.Location = new System.Drawing.Point(1162, 561);
            this.btnNotgood.Name = "btnNotgood";
            this.btnNotgood.Size = new System.Drawing.Size(129, 55);
            this.btnNotgood.TabIndex = 109;
            this.btnNotgood.TabStop = false;
            this.btnNotgood.Text = "Not Good";
            this.btnNotgood.UseVisualStyleBackColor = true;
            this.btnNotgood.Click += new System.EventHandler(this.btnNotgood_Click);
            // 
            // lblPressSubmit
            // 
            this.lblPressSubmit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblPressSubmit.AutoSize = true;
            this.lblPressSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblPressSubmit.ForeColor = System.Drawing.Color.Red;
            this.lblPressSubmit.Location = new System.Drawing.Point(982, 542);
            this.lblPressSubmit.Name = "lblPressSubmit";
            this.lblPressSubmit.Size = new System.Drawing.Size(44, 16);
            this.lblPressSubmit.TabIndex = 110;
            this.lblPressSubmit.Text = "Enter";
            // 
            // imageViewerTR1
            // 
            this.imageViewerTR1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.imageViewerTR1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imageViewerTR1.CurrentZoom = 1F;
            this.imageViewerTR1.Image = null;
            this.imageViewerTR1.Location = new System.Drawing.Point(73, 28);
            this.imageViewerTR1.MaxZoom = 20F;
            this.imageViewerTR1.MinZoom = 0.05F;
            this.imageViewerTR1.Name = "imageViewerTR1";
            this.imageViewerTR1.Size = new System.Drawing.Size(854, 615);
            this.imageViewerTR1.TabIndex = 176;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Controls.Add(this.spc4);
            this.panel1.Controls.Add(this.spc3);
            this.panel1.Controls.Add(this.spc2);
            this.panel1.Controls.Add(this.spc1);
            this.panel1.Location = new System.Drawing.Point(933, 132);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(404, 370);
            this.panel1.TabIndex = 177;
            // 
            // spc4
            // 
            this.spc4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.spc4.Location = new System.Drawing.Point(315, 88);
            this.spc4.Name = "spc4";
            this.spc4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spc4.Panel1
            // 
            this.spc4.Panel1.Controls.Add(this.lbl43);
            this.spc4.Panel1.Controls.Add(this.lbl47);
            this.spc4.Panel1.Controls.Add(this.lbl42);
            this.spc4.Panel1.Controls.Add(this.lbl41);
            this.spc4.Panel1.Controls.Add(this.lbl44);
            this.spc4.Panel1.Controls.Add(this.lbl45);
            this.spc4.Panel1.Controls.Add(this.lbl46);
            this.spc4.Panel1.Controls.Add(this.label18);
            this.spc4.Panel1.Controls.Add(this.label20);
            this.spc4.Panel1.Controls.Add(this.label24);
            this.spc4.Panel1.Controls.Add(this.label19);
            this.spc4.Panel1.Controls.Add(this.label13);
            this.spc4.Panel1.Controls.Add(this.label15);
            this.spc4.Panel1.Controls.Add(this.label16);
            this.spc4.Size = new System.Drawing.Size(82, 341);
            this.spc4.SplitterDistance = 281;
            this.spc4.TabIndex = 4;
            // 
            // lbl43
            // 
            this.lbl43.AutoSize = true;
            this.lbl43.BackColor = System.Drawing.SystemColors.Window;
            this.lbl43.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl43.ForeColor = System.Drawing.Color.Black;
            this.lbl43.Location = new System.Drawing.Point(24, 91);
            this.lbl43.Name = "lbl43";
            this.lbl43.Size = new System.Drawing.Size(51, 25);
            this.lbl43.TabIndex = 111;
            this.lbl43.Text = "453";
            // 
            // lbl47
            // 
            this.lbl47.AutoSize = true;
            this.lbl47.BackColor = System.Drawing.SystemColors.Window;
            this.lbl47.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl47.ForeColor = System.Drawing.Color.Black;
            this.lbl47.Location = new System.Drawing.Point(24, 236);
            this.lbl47.Name = "lbl47";
            this.lbl47.Size = new System.Drawing.Size(51, 25);
            this.lbl47.TabIndex = 116;
            this.lbl47.Text = "543";
            // 
            // lbl42
            // 
            this.lbl42.AutoSize = true;
            this.lbl42.BackColor = System.Drawing.SystemColors.Window;
            this.lbl42.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl42.ForeColor = System.Drawing.Color.Black;
            this.lbl42.Location = new System.Drawing.Point(24, 56);
            this.lbl42.Name = "lbl42";
            this.lbl42.Size = new System.Drawing.Size(51, 25);
            this.lbl42.TabIndex = 110;
            this.lbl42.Text = "452";
            // 
            // lbl41
            // 
            this.lbl41.AutoSize = true;
            this.lbl41.BackColor = System.Drawing.SystemColors.Window;
            this.lbl41.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl41.ForeColor = System.Drawing.Color.Black;
            this.lbl41.Location = new System.Drawing.Point(24, 22);
            this.lbl41.Name = "lbl41";
            this.lbl41.Size = new System.Drawing.Size(51, 25);
            this.lbl41.TabIndex = 109;
            this.lbl41.Text = "451";
            // 
            // lbl44
            // 
            this.lbl44.AutoSize = true;
            this.lbl44.BackColor = System.Drawing.SystemColors.Window;
            this.lbl44.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl44.ForeColor = System.Drawing.Color.Black;
            this.lbl44.Location = new System.Drawing.Point(24, 127);
            this.lbl44.Name = "lbl44";
            this.lbl44.Size = new System.Drawing.Size(51, 25);
            this.lbl44.TabIndex = 115;
            this.lbl44.Text = "513";
            // 
            // lbl45
            // 
            this.lbl45.AutoSize = true;
            this.lbl45.BackColor = System.Drawing.SystemColors.Window;
            this.lbl45.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl45.ForeColor = System.Drawing.Color.Black;
            this.lbl45.Location = new System.Drawing.Point(24, 164);
            this.lbl45.Name = "lbl45";
            this.lbl45.Size = new System.Drawing.Size(51, 25);
            this.lbl45.TabIndex = 114;
            this.lbl45.Text = "523";
            // 
            // lbl46
            // 
            this.lbl46.AutoSize = true;
            this.lbl46.BackColor = System.Drawing.SystemColors.Window;
            this.lbl46.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl46.ForeColor = System.Drawing.Color.Black;
            this.lbl46.Location = new System.Drawing.Point(24, 201);
            this.lbl46.Name = "lbl46";
            this.lbl46.Size = new System.Drawing.Size(51, 25);
            this.lbl46.TabIndex = 113;
            this.lbl46.Text = "533";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.SystemColors.Control;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(3, 92);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(20, 24);
            this.label18.TabIndex = 99;
            this.label18.Text = "3";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.SystemColors.Control;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Blue;
            this.label20.Location = new System.Drawing.Point(3, 237);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(20, 24);
            this.label20.TabIndex = 108;
            this.label20.Text = "7";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.SystemColors.Control;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Blue;
            this.label24.Location = new System.Drawing.Point(3, 56);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(20, 24);
            this.label24.TabIndex = 98;
            this.label24.Text = "2";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.SystemColors.Control;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(3, 23);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(20, 24);
            this.label19.TabIndex = 97;
            this.label19.Text = "1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.SystemColors.Control;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(3, 127);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 24);
            this.label13.TabIndex = 106;
            this.label13.Text = "4";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.Control;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(3, 165);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(20, 24);
            this.label15.TabIndex = 105;
            this.label15.Text = "5";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.SystemColors.Control;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(3, 201);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(20, 24);
            this.label16.TabIndex = 104;
            this.label16.Text = "6";
            // 
            // spc3
            // 
            this.spc3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.spc3.Location = new System.Drawing.Point(229, 89);
            this.spc3.Name = "spc3";
            this.spc3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spc3.Panel1
            // 
            this.spc3.Panel1.Controls.Add(this.lbl37);
            this.spc3.Panel1.Controls.Add(this.lbl36);
            this.spc3.Panel1.Controls.Add(this.lbl35);
            this.spc3.Panel1.Controls.Add(this.lbl34);
            this.spc3.Panel1.Controls.Add(this.lbl33);
            this.spc3.Panel1.Controls.Add(this.lbl32);
            this.spc3.Panel1.Controls.Add(this.lbl31);
            this.spc3.Panel1.Controls.Add(this.label1);
            this.spc3.Panel1.Controls.Add(this.label23);
            this.spc3.Panel1.Controls.Add(this.label11);
            this.spc3.Panel1.Controls.Add(this.label22);
            this.spc3.Panel1.Controls.Add(this.label12);
            this.spc3.Panel1.Controls.Add(this.label21);
            this.spc3.Panel1.Controls.Add(this.label14);
            this.spc3.Size = new System.Drawing.Size(85, 337);
            this.spc3.SplitterDistance = 280;
            this.spc3.TabIndex = 3;
            // 
            // lbl37
            // 
            this.lbl37.AutoSize = true;
            this.lbl37.BackColor = System.Drawing.SystemColors.Window;
            this.lbl37.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl37.ForeColor = System.Drawing.Color.Black;
            this.lbl37.Location = new System.Drawing.Point(26, 236);
            this.lbl37.Name = "lbl37";
            this.lbl37.Size = new System.Drawing.Size(51, 25);
            this.lbl37.TabIndex = 112;
            this.lbl37.Text = "542";
            // 
            // lbl36
            // 
            this.lbl36.AutoSize = true;
            this.lbl36.BackColor = System.Drawing.SystemColors.Window;
            this.lbl36.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl36.ForeColor = System.Drawing.Color.Black;
            this.lbl36.Location = new System.Drawing.Point(26, 202);
            this.lbl36.Name = "lbl36";
            this.lbl36.Size = new System.Drawing.Size(51, 25);
            this.lbl36.TabIndex = 111;
            this.lbl36.Text = "532";
            // 
            // lbl35
            // 
            this.lbl35.AutoSize = true;
            this.lbl35.BackColor = System.Drawing.SystemColors.Window;
            this.lbl35.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl35.ForeColor = System.Drawing.Color.Black;
            this.lbl35.Location = new System.Drawing.Point(26, 166);
            this.lbl35.Name = "lbl35";
            this.lbl35.Size = new System.Drawing.Size(51, 25);
            this.lbl35.TabIndex = 110;
            this.lbl35.Text = "522";
            // 
            // lbl34
            // 
            this.lbl34.AutoSize = true;
            this.lbl34.BackColor = System.Drawing.SystemColors.Window;
            this.lbl34.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl34.ForeColor = System.Drawing.Color.Black;
            this.lbl34.Location = new System.Drawing.Point(26, 128);
            this.lbl34.Name = "lbl34";
            this.lbl34.Size = new System.Drawing.Size(51, 25);
            this.lbl34.TabIndex = 109;
            this.lbl34.Text = "512";
            // 
            // lbl33
            // 
            this.lbl33.AutoSize = true;
            this.lbl33.BackColor = System.Drawing.SystemColors.Window;
            this.lbl33.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl33.ForeColor = System.Drawing.Color.Black;
            this.lbl33.Location = new System.Drawing.Point(26, 92);
            this.lbl33.Name = "lbl33";
            this.lbl33.Size = new System.Drawing.Size(51, 25);
            this.lbl33.TabIndex = 108;
            this.lbl33.Text = "432";
            // 
            // lbl32
            // 
            this.lbl32.AutoSize = true;
            this.lbl32.BackColor = System.Drawing.SystemColors.Window;
            this.lbl32.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl32.ForeColor = System.Drawing.Color.Black;
            this.lbl32.Location = new System.Drawing.Point(26, 58);
            this.lbl32.Name = "lbl32";
            this.lbl32.Size = new System.Drawing.Size(51, 25);
            this.lbl32.TabIndex = 107;
            this.lbl32.Text = "422";
            // 
            // lbl31
            // 
            this.lbl31.AutoSize = true;
            this.lbl31.BackColor = System.Drawing.SystemColors.Window;
            this.lbl31.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl31.ForeColor = System.Drawing.Color.Black;
            this.lbl31.Location = new System.Drawing.Point(26, 24);
            this.lbl31.Name = "lbl31";
            this.lbl31.Size = new System.Drawing.Size(51, 25);
            this.lbl31.TabIndex = 106;
            this.lbl31.Text = "412";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(6, 167);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 24);
            this.label1.TabIndex = 104;
            this.label1.Text = "5";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.SystemColors.Control;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Blue;
            this.label23.Location = new System.Drawing.Point(5, 58);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(20, 24);
            this.label23.TabIndex = 96;
            this.label23.Text = "2";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.Control;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(4, 203);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 24);
            this.label11.TabIndex = 103;
            this.label11.Text = "6";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.SystemColors.Control;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Blue;
            this.label22.Location = new System.Drawing.Point(5, 24);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(20, 24);
            this.label22.TabIndex = 95;
            this.label22.Text = "1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.Control;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(3, 238);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(20, 24);
            this.label12.TabIndex = 102;
            this.label12.Text = "7";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.SystemColors.Control;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Blue;
            this.label21.Location = new System.Drawing.Point(5, 93);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(20, 24);
            this.label21.TabIndex = 97;
            this.label21.Text = "3";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.SystemColors.Control;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(4, 129);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 24);
            this.label14.TabIndex = 98;
            this.label14.Text = "4";
            // 
            // spc2
            // 
            this.spc2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.spc2.Location = new System.Drawing.Point(121, 89);
            this.spc2.Name = "spc2";
            this.spc2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spc2.Panel1
            // 
            this.spc2.Panel1.Controls.Add(this.lbl24);
            this.spc2.Panel1.Controls.Add(this.lbl23);
            this.spc2.Panel1.Controls.Add(this.lbl22);
            this.spc2.Panel1.Controls.Add(this.lbl21);
            this.spc2.Panel1.Controls.Add(this.label5);
            this.spc2.Panel1.Controls.Add(this.label10);
            this.spc2.Panel1.Controls.Add(this.label8);
            this.spc2.Panel1.Controls.Add(this.label9);
            this.spc2.Size = new System.Drawing.Size(105, 334);
            this.spc2.SplitterDistance = 280;
            this.spc2.TabIndex = 2;
            // 
            // lbl24
            // 
            this.lbl24.AutoSize = true;
            this.lbl24.BackColor = System.Drawing.SystemColors.Window;
            this.lbl24.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl24.ForeColor = System.Drawing.Color.Black;
            this.lbl24.Location = new System.Drawing.Point(30, 231);
            this.lbl24.Name = "lbl24";
            this.lbl24.Size = new System.Drawing.Size(62, 31);
            this.lbl24.TabIndex = 180;
            this.lbl24.Text = "301";
            // 
            // lbl23
            // 
            this.lbl23.AutoSize = true;
            this.lbl23.BackColor = System.Drawing.SystemColors.Window;
            this.lbl23.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl23.ForeColor = System.Drawing.Color.Black;
            this.lbl23.Location = new System.Drawing.Point(30, 159);
            this.lbl23.Name = "lbl23";
            this.lbl23.Size = new System.Drawing.Size(62, 31);
            this.lbl23.TabIndex = 179;
            this.lbl23.Text = "202";
            // 
            // lbl22
            // 
            this.lbl22.AutoSize = true;
            this.lbl22.BackColor = System.Drawing.SystemColors.Window;
            this.lbl22.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl22.ForeColor = System.Drawing.Color.Black;
            this.lbl22.Location = new System.Drawing.Point(30, 92);
            this.lbl22.Name = "lbl22";
            this.lbl22.Size = new System.Drawing.Size(62, 31);
            this.lbl22.TabIndex = 178;
            this.lbl22.Text = "203";
            // 
            // lbl21
            // 
            this.lbl21.AutoSize = true;
            this.lbl21.BackColor = System.Drawing.SystemColors.Window;
            this.lbl21.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl21.ForeColor = System.Drawing.Color.Black;
            this.lbl21.Location = new System.Drawing.Point(30, 20);
            this.lbl21.Name = "lbl21";
            this.lbl21.Size = new System.Drawing.Size(62, 31);
            this.lbl21.TabIndex = 177;
            this.lbl21.Text = "101";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(5, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 25);
            this.label5.TabIndex = 172;
            this.label5.Text = "3";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.Control;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(5, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 25);
            this.label10.TabIndex = 164;
            this.label10.Text = "2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.Control;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(5, 236);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 25);
            this.label8.TabIndex = 171;
            this.label8.Text = "4";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.Control;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(5, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 25);
            this.label9.TabIndex = 165;
            this.label9.Text = "1";
            // 
            // spc1
            // 
            this.spc1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.spc1.Location = new System.Drawing.Point(10, 250);
            this.spc1.Name = "spc1";
            this.spc1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spc1.Panel1
            // 
            this.spc1.Panel1.Controls.Add(this.label7);
            this.spc1.Panel1.Controls.Add(this.lbl11);
            this.spc1.Panel1.Controls.Add(this.label6);
            this.spc1.Panel1.Controls.Add(this.lbl12);
            this.spc1.Size = new System.Drawing.Size(101, 169);
            this.spc1.SplitterDistance = 121;
            this.spc1.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(61, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 25);
            this.label7.TabIndex = 174;
            this.label7.Text = "2";
            // 
            // lbl11
            // 
            this.lbl11.AutoSize = true;
            this.lbl11.BackColor = System.Drawing.SystemColors.Window;
            this.lbl11.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl11.ForeColor = System.Drawing.Color.Black;
            this.lbl11.Location = new System.Drawing.Point(6, 6);
            this.lbl11.Name = "lbl11";
            this.lbl11.Size = new System.Drawing.Size(39, 42);
            this.lbl11.TabIndex = 175;
            this.lbl11.Text = "1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(10, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 25);
            this.label6.TabIndex = 173;
            this.label6.Text = "1";
            // 
            // lbl12
            // 
            this.lbl12.AutoSize = true;
            this.lbl12.BackColor = System.Drawing.SystemColors.Window;
            this.lbl12.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl12.ForeColor = System.Drawing.Color.Black;
            this.lbl12.Location = new System.Drawing.Point(54, 7);
            this.lbl12.Name = "lbl12";
            this.lbl12.Size = new System.Drawing.Size(39, 42);
            this.lbl12.TabIndex = 176;
            this.lbl12.Text = "2";
            // 
            // FrmEntryTemp3_2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.ClientSize = new System.Drawing.Size(1350, 730);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.imageViewerTR1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnNotgood);
            this.Controls.Add(this.lblPressSubmit);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.txtOCR);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEntryTemp3_2";
            this.Text = "VCB - Entry ";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEntry_FormClosing);
            this.Load += new System.EventHandler(this.frmEntry_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEntry_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmEntry_KeyPress);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.spc4.Panel1.ResumeLayout(false);
            this.spc4.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spc4)).EndInit();
            this.spc4.ResumeLayout(false);
            this.spc3.Panel1.ResumeLayout(false);
            this.spc3.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spc3)).EndInit();
            this.spc3.ResumeLayout(false);
            this.spc2.Panel1.ResumeLayout(false);
            this.spc2.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spc2)).EndInit();
            this.spc2.ResumeLayout(false);
            this.spc1.Panel1.ResumeLayout(false);
            this.spc1.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spc1)).EndInit();
            this.spc1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public static System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.TextBox txtOCR;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel lblpage;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel lblpagename;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripLabel lblField;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.Timer timerEntry;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel lblerror;
        private System.Windows.Forms.Button btnExit;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnNotgood;
        internal System.Windows.Forms.Label lblPressSubmit;
        private ImageViewerTR.ImageViewerTR imageViewerTR1;
        private System.Windows.Forms.ToolStripLabel txtInf;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer spc2;
        private System.Windows.Forms.Label lbl24;
        private System.Windows.Forms.Label lbl23;
        private System.Windows.Forms.Label lbl22;
        private System.Windows.Forms.Label lbl21;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.SplitContainer spc1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbl11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl12;
        private System.Windows.Forms.SplitContainer spc4;
        private System.Windows.Forms.Label lbl43;
        private System.Windows.Forms.Label lbl47;
        private System.Windows.Forms.Label lbl42;
        private System.Windows.Forms.Label lbl41;
        private System.Windows.Forms.Label lbl44;
        private System.Windows.Forms.Label lbl45;
        private System.Windows.Forms.Label lbl46;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.SplitContainer spc3;
        private System.Windows.Forms.Label lbl37;
        private System.Windows.Forms.Label lbl36;
        private System.Windows.Forms.Label lbl35;
        private System.Windows.Forms.Label lbl34;
        private System.Windows.Forms.Label lbl33;
        private System.Windows.Forms.Label lbl32;
        private System.Windows.Forms.Label lbl31;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label14;
    }
}