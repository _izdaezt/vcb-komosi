﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
namespace VCB_KOMOSI
{
    public partial class FrmEntryTemp4_1_E3 : Form
    {
        #region var
        public static Point locationOnForm;
        public static bool selecthitpoint = true;
        public string[] INFperformance = new string[2];
        public int batchId;
        public int userId;
        public int formid;
        public int pair;
        private double scale = 1;
        public string userName;
        public string batchName;
        public string template;
        public string level;
        public string group;
        public string formName = "";
        Image imageOriginal = null;
        Bitmap imageSource;
        private DAEntry_Entry dAEntry = new DAEntry_Entry();
        private BOImage_Entry img = new BOImage_Entry();
        private bool finish = false;
        bool eter = false;
        bool space = false;
        bool check = false;
        int[] INFImage;
        public static int locationX = 0;
        public static int locationY = 0;
        double tyle;
        double tyleImageNew = 1;
        Bitmap bm_out;
        int zom = 11;
        float currentzoom;
        // value bool Japan
        bool jp = true;
        List<TextBox> lsttxt = new List<TextBox>();
        List<BOImage_Entry> ListImage = new List<BOImage_Entry>();
        DataRow[] subField = null;
        DBsql dbsql = new DBsql();
        bool isErrorCong = false;
        int limitTime = 0;
        int focus = 0;
        List<Label> lstlbl;
        DateTime dtimeBefore = new DateTime();
        string batchNameLocal;
        int vlfocus;
        #endregion
        public FrmEntryTemp4_1_E3()
        {
            InitializeComponent();
            lstlbl = new List<Label>() { lbl1, lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8 };
            lsttxt = new List<TextBox>() { txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8 };
        }

        private void frmEntry_Load(object sender, EventArgs e)
        {

            lsttxt.ForEach(a =>
            {
                a.Enter += new System.EventHandler(this.textBox1_Enter);
                a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
                a.Leave += new System.EventHandler(this.textBox1_Leave);
                a.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
                a.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            });
            // hiển thị thông tin user          
            txtInf.Text = "VCB Entry  --  UserName: " + userName + "  -- Template: " + template + "  --  Batch: " + batchName;
            //Lây thư mục gốc từ batchname   

            batchNameLocal = batchName.Substring(7);

            //userid
            int uID = userId;

            // hien thi giua man hinh
            this.CenterToScreen();
            dAEntry = new DAEntry_Entry();

            // lấy id image free
            int id = 0;
            try
            {
                //id = dAEntry.GetIdImageE3(batchNameLocal, formid);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            if (id == 0)
            {
                dAEntry.SetHitpointBatch(batchNameLocal, formid);
                finish = true;
                this.Close();
                return;
            }

            timerEntry.Start();

            // lay image free
            try
            {
                //img = dAEntry.GetImage(id);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            //Get Image
            try
            {
                //imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageUrl, batchName.Substring(7, batchName.Length - 7))));
                //imageViewerTR1.Image = imageSource;
                //currentzoom = imageViewerTR1.CurrentZoom;

                //nam 21/7 cắt ảnh
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageUrl, batchName.Substring(7, batchName.Length - 7))));
                Bitmap bm2 = (Bitmap)imageSource.Clone();
                imageViewerTR1.Image = CropBitmap(bm2, bm2.Width * 60 / 100, bm2.Height * 14 / 100, bm2.Width * 40 / 100, bm2.Height * 45 / 100);
                currentzoom = imageViewerTR1.CurrentZoom;

            }
            catch
            {
                MessageBox.Show("Server chưa có ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }


            int bl = 0;
            for (int i = 7; i > -1; i--)
            {
                if (img.Content1_OCR[i] == img.ContentOCR[i])
                {
                    lsttxt[i].Text = img.Content1_OCR[i];
                    lstlbl[i].Visible = false;
                    lsttxt[i].Visible = false;
                }
                else
                {
                    bl = i;
                }
            }
            // hiển thị tên field name     
            lblpagename.Text = img.PageUrl;
            lblpage.Text = dAEntry.ImageExistE3(batchNameLocal, formid).ToString();
            dtimeBefore = DateTime.Now;
            lsttxt[bl].Focus();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (e.KeyCode == Keys.Down)
                lsttxt[tb.TabIndex == 8 ? 0 : (tb.TabIndex)].Focus();
            if (e.KeyCode == Keys.Up)
                lsttxt[tb.TabIndex == 1 ? 7 : (tb.TabIndex - 2)].Focus();
        }
        private void textBox1_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            vlfocus = tb.TabIndex;
            tb.BackColor = Color.PowderBlue;
            if (tb.Text != "")
                tb.SelectionStart = tb.Text.Length + 1;
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.BackColor = SystemColors.Window;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.TabIndex < 8)
            {
                if (tb.Text.Length == 10)
                {
                    int idex = tb.TabIndex;
                    while (idex < 8)
                    {
                        if (lsttxt[idex].Visible)
                        {
                            lsttxt[idex].Focus();
                            break;
                        }
                        idex++;
                    }
                }
            }
        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.TabIndex >= 0 && tb.TabIndex <= 5)
            {
                if (e.KeyChar == 43)
                    e.Handled = true;
            }
            if (e.KeyChar == 13)
                e.Handled = true;
            if (e.KeyChar == 102)
                e.Handled = true;
            if (e.KeyChar == 97)
                e.Handled = true;
            if (e.KeyChar == 122)
                e.Handled = true;
            if (e.KeyChar == 110)
                e.Handled = true;
            if (e.KeyChar == 113)
                e.Handled = true;
            if (e.KeyChar == 96)
                e.Handled = true;
            if (e.KeyChar == 32)
                e.Handled = true;
            if (e.KeyChar == 101)
                e.Handled = true;
            if (e.KeyChar == 100)
                e.Handled = true;
            if (e.KeyChar == 115)
                e.Handled = true;
            if (e.KeyChar == 119)
                e.Handled = true;
        }
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            string imgContentContent = String.Join("|", lsttxt.Select(x => x.Text.Trim()));
            if (imgContentContent.Contains('*'))
            {
                btnNotgood_Click(sender, e);
                return;
            }
            int lng = imgContentContent.Length - 7;
            int uID = userId;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                dAEntry.Add_Content_E3(imgContentImageId, imgContentContent, imgContentUserId, lng, ms);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // load image mới lên form
            // lấy id image free         
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                dAEntry.SetHitpointBatch(batchNameLocal, formid);
                finish = true;
                this.Close();
                return;
            }

            try
            {
                ////Get Image
                //imageSource = (Bitmap)img.Imagesource.Clone();
                //imageViewerTR1.Image = imageSource;

                //nam 21/7 cắt ảnh
                imageSource = img.Imagesource;
                Bitmap bm2 = (Bitmap)imageSource.Clone();
                imageViewerTR1.Image = CropBitmap(bm2, bm2.Width * 60 / 100, bm2.Height * 14 / 100, bm2.Width * 40 / 100, bm2.Height * 45 / 100);
                currentzoom = imageViewerTR1.CurrentZoom;

            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            int bl = 0;
            for (int i = 7; i > -1; i--)
            {
                lsttxt[i].Text = "";
                lstlbl[i].Visible = true;
                lsttxt[i].Visible = true;
                if (img.Content1_OCR[i] == img.ContentOCR[i])
                {
                    lsttxt[i].Text = img.Content1_OCR[i];
                    lstlbl[i].Visible = false;
                    lsttxt[i].Visible = false;
                }
                else
                {
                    bl = i;
                }
            }

            // hiển thị tên field name        
            btnSubmit.Enabled = true;
            btnNotgood.Enabled = true;
            dtimeBefore = DateTime.Now;
            lblpagename.Text = img.PageUrl;
            lblpage.Text = dAEntry.ImageExistE3(batchNameLocal, formid).ToString();
            lsttxt[bl].Focus();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmEntry_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerEntry.Stop();
            try
            {
                if (!finish)
                {
                    dAEntry.ReturnPairANDHitpointEntry(img.Id, pair);
                    // unlock image       
                    for (int i = 0; i < ListImage.Count; i++)
                    {
                        dAEntry.ReturnPairANDHitpointEntry(ListImage[i].Id, pair);
                    }
                }
                else
                {
                    frmLogIn.fn = true;
                    dAEntry.SetHitpointBatch(batchNameLocal, formid);
                }


            }
            catch { }
        }

        private void btnNotgood_Click(object sender, EventArgs e)
        {
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            string imgContentContent = String.Join("|", lsttxt.Select(x => x.Text.Trim()));
            int lng = imgContentContent.Length - 7;
            int uID = userId;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                dAEntry.Add_Content_NG_E3(imgContentImageId, imgContentUserId, lng, ms, imgContentContent);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // load image mới lên form
            // lấy id image free
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                dAEntry.SetHitpointBatch(batchNameLocal, formid);
                finish = true;
                this.Close();
                return;
            }

            try
            {
                //Get Image
                imageSource = (Bitmap)img.Imagesource.Clone();
                imageViewerTR1.Image = imageSource;
                //nam 21/7 cắt ảnh
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageUrl, batchName.Substring(7, batchName.Length - 7))));
                Bitmap bm2 = (Bitmap)imageSource.Clone();
                imageViewerTR1.Image = CropBitmap(bm2, bm2.Width * 60 / 100, bm2.Height * 14 / 100, bm2.Width * 40 / 100, bm2.Height * 45 / 100);
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // hiển thị tên field name         
            int bl = 0;
            for (int i = 7; i > -1; i--)
            {
                lsttxt[i].Text = "";
                lstlbl[i].Visible = true;
                lsttxt[i].Visible = true;
                if (img.Content1_OCR[i] == img.ContentOCR[i])
                {
                    lsttxt[i].Text = img.Content1_OCR[i];
                    lstlbl[i].Visible = false;
                    lsttxt[i].Visible = false;
                }
                else
                {
                    bl = i;
                }
            }
            btnSubmit.Enabled = true;
            btnNotgood.Enabled = true;
            lblpagename.Text = img.PageUrl;
            dtimeBefore = DateTime.Now;
            lblpage.Text = dAEntry.ImageExistE3(batchNameLocal, formid).ToString();
            lsttxt[bl].Focus();
        }

        private void chkFullimage_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void frmEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && btnSubmit.Enabled && btnNotgood.Enabled)
            {
                btnSubmit_Click(sender, e);
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Escape)
                this.Close();
            if (e.Control)
            {
                if (e.KeyCode == Keys.N && btnSubmit.Enabled && btnNotgood.Enabled)
                    btnNotgood_Click(sender, e);
                if (e.KeyCode == Keys.Up)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Down)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    imageViewerTR1.RotateImage("270");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Left)
                {
                    imageViewerTR1.RotateImage("90");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.F)
                {
                    imageViewerTR1.CurrentZoom = currentzoom;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.A)
                {
                    for (int i = 0; i < subField.Length; i++)
                    {
                        lsttxt[i].Select(0, lsttxt[i].Text.Length);
                    }
                }
            }
            if (e.KeyCode == Keys.LWin || e.KeyCode == Keys.RWin)
            {
                if (e.KeyCode == Keys.D)
                {
                    this.Activate();
                }
                SendKeys.Send("{Esc}");
                SendKeys.Send("{Esc}");
                e.Handled = true;
            }
        }

        private void timerEntry_Tick(object sender, EventArgs e)
        {
            this.Activate();
            if (ListImage.Count < 2)
            {
                //int id2 = dAEntry.GetIdImageE3(batchNameLocal, formid);
                //if (id2 > 0)
                //{
                //    //BOImage_Entry img2 = dAEntry.GetImage(id2);
                //    //img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img2.PageUrl, batchNameLocal)));
                //    //ListImage.Add(img2);
                //}
            }

            try
            {
                //if (graphics != null)
                //    graphics.Dispose();
                // Create a new pen.

                Pen skyBluePen = new Pen(new SolidBrush(Color.FromArgb(255, 255, 255, 255)));
                // Set the pen's width.
                skyBluePen.Width = 30.0F;

                // Set the LineJoin property.
                skyBluePen.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
                Graphics graphics1 = imageViewerTR1.CreateGraphics();
                graphics1.DrawLine(skyBluePen, imageViewerTR1.Width, imageViewerTR1.Height / 9, imageViewerTR1.Width - 50, imageViewerTR1.Height / 9);
                Graphics graphics2 = imageViewerTR1.CreateGraphics();
                graphics2.DrawLine(skyBluePen, imageViewerTR1.Width, imageViewerTR1.Height * 2 / 9, imageViewerTR1.Width - 50, imageViewerTR1.Height * 2 / 9);
                Graphics graphics3 = imageViewerTR1.CreateGraphics();
                graphics3.DrawLine(skyBluePen, imageViewerTR1.Width, imageViewerTR1.Height / 3, imageViewerTR1.Width - 50, imageViewerTR1.Height / 3);
                Graphics graphics4 = imageViewerTR1.CreateGraphics();
                graphics4.DrawLine(skyBluePen, imageViewerTR1.Width, imageViewerTR1.Height * 4 / 9, imageViewerTR1.Width - 50, imageViewerTR1.Height * 4 / 9);
                Graphics graphics5 = imageViewerTR1.CreateGraphics();
                graphics5.DrawLine(skyBluePen, imageViewerTR1.Width, imageViewerTR1.Height * 5 / 9, imageViewerTR1.Width - 50, imageViewerTR1.Height * 5 / 9);
                Graphics graphics6 = imageViewerTR1.CreateGraphics();
                graphics6.DrawLine(skyBluePen, imageViewerTR1.Width, imageViewerTR1.Height * 2 / 3, imageViewerTR1.Width - 50, imageViewerTR1.Height * 2 / 3);
                Graphics graphics7 = imageViewerTR1.CreateGraphics();
                graphics7.DrawLine(skyBluePen, imageViewerTR1.Width, imageViewerTR1.Height * 7 / 9, imageViewerTR1.Width - 50, imageViewerTR1.Height * 7 / 9);
                Graphics graphics8 = imageViewerTR1.CreateGraphics();
                graphics8.DrawLine(skyBluePen, imageViewerTR1.Width, imageViewerTR1.Height * 8 / 9, imageViewerTR1.Width - 50, imageViewerTR1.Height * 8 / 9);

                Pen skyBluePen1 = new Pen(Brushes.Red);
                // Set the pen's width.
                skyBluePen1.Width = 30.0F;

                // Set the LineJoin property.
                skyBluePen1.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
                Graphics graphics = imageViewerTR1.CreateGraphics();
                if (vlfocus == 1)
                    graphics.DrawLine(skyBluePen1, imageViewerTR1.Width, imageViewerTR1.Height / 9, imageViewerTR1.Width - 50, imageViewerTR1.Height / 9);
                else if (vlfocus == 2)
                    graphics.DrawLine(skyBluePen1, imageViewerTR1.Width, imageViewerTR1.Height * 2 / 9, imageViewerTR1.Width - 50, imageViewerTR1.Height * 2 / 9);
                else if (vlfocus == 3)
                    graphics.DrawLine(skyBluePen1, imageViewerTR1.Width, imageViewerTR1.Height / 3, imageViewerTR1.Width - 50, imageViewerTR1.Height / 3);
                else if (vlfocus == 4)
                    graphics.DrawLine(skyBluePen1, imageViewerTR1.Width, imageViewerTR1.Height * 4 / 9, imageViewerTR1.Width - 50, imageViewerTR1.Height * 4 / 9);
                else if (vlfocus == 5)
                    graphics.DrawLine(skyBluePen1, imageViewerTR1.Width, imageViewerTR1.Height * 5 / 9, imageViewerTR1.Width - 50, imageViewerTR1.Height * 5 / 9);
                else if (vlfocus == 6)
                    graphics.DrawLine(skyBluePen1, imageViewerTR1.Width, imageViewerTR1.Height * 2 / 3, imageViewerTR1.Width - 50, imageViewerTR1.Height * 2 / 3);
                else if (vlfocus == 7)
                    graphics.DrawLine(skyBluePen1, imageViewerTR1.Width, imageViewerTR1.Height * 7 / 9, imageViewerTR1.Width - 50, imageViewerTR1.Height * 7 / 9);
                else if (vlfocus == 8)
                    graphics.DrawLine(skyBluePen1, imageViewerTR1.Width, imageViewerTR1.Height * 8 / 9, imageViewerTR1.Width - 50, imageViewerTR1.Height * 8 / 9);
            }
            catch { };
        }

        private void frmEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtInf_Click(object sender, EventArgs e)
        {

        }
        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }

    }
}
