﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
namespace VCB_KOMOSI
{
    public partial class frmEntry_E3 : Form
    {
        #region var
        public static Point locationOnForm;
        public static bool selecthitpoint = true;
        public int batchId;
        public int userId;
        public int formid;
        public int pair;
        public string userName;
        public string batchName;
        public string template;
        public string level;
        public string group;
        public string formName = "";
        Bitmap imageSource;
        private DAEntry_Entry dAEntry = new DAEntry_Entry();
        private BOImage_Entry img = new BOImage_Entry();
        private bool finish = false;
        int[] INFImage;
        float currentzoom;
        public static int locationX = 0;
        public static int locationY = 0;
        public static DataTable dtMBD;
        DataTable dtSubField = new DataTable();
        List<TextBox> lstTxt = new List<TextBox>();
        List<Label> lstLblSubFieldName = new List<Label>();
        List<Label> lstLblSubFieldLimit = new List<Label>();
        List<BOImage_Entry> ListImage = new List<BOImage_Entry>();
        DBsql dbsql = new DBsql();
        DateTime dtimeBefore = new DateTime();
        string batchNameLocal;
        //nam 20/7 tao bien kiem tra chu cai dau thu muc
        string type = "";
        string tableform;

        #endregion
        public frmEntry_E3()
        {
            InitializeComponent();
        }

        private void frmEntry_Load(object sender, EventArgs e)
        {
            // hiển thị thông tin user          
            txtInf.Text = "VCB Entry  --  UserName: " + userName + "  -- Template: " + template + "  --  Batch: " + batchName;
            //Lây thư mục gốc từ batchname         
            try
            {
                if (batchName[7] == 'A')
                {
                    //txt2.Enabled = false;
                    //lbl2.Enabled = false;

                    //nam 20/7 ẩn mục ngày sinh
                    type = "A";
                    txt2.Visible = false;
                    lbl2.Visible = false;
                    imageViewerTR2.Visible = false;
                }
                //nam 20/7 gan bien khi folder C
                if (batchName[7] == 'C')
                {
                    type = "C";
                }
            }
            catch { }
            batchNameLocal = batchName.Substring(7);
            //userid
            int uID = userId;

            // hien thi giua man hinh
            this.CenterToScreen();
            dAEntry = new DAEntry_Entry();

            // lấy id image free
            tableform = batchId + "--" + batchNameLocal;
            int id = 0;
            try
            {
                id = dAEntry.GetIdImageE3(tableform);
                if (id == 0)
                {
                    if (dAEntry.Count_UpHitpointLC(tableform) == 0)
                    { dAEntry.UpdateHitpoint_Batch(batchId, 2); }
                    else
                    {
                        if (dAEntry.Count_ImageExits(tableform, 1) == 0 && dAEntry.Count_ImageExits(tableform, 2) == 0 && dAEntry.Count_ImageExits(tableform, 3) == 0)
                        { dAEntry.UpdateHitpoint_Batch(batchId, 1); }
                    }
                    finish = true;
                    //MessageBox.Show(this,"đã hoàn thành nhập");
                    this.Close();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            
            timerEntry.Start();

            // lay image free
            try
            {
                img = dAEntry.GetImage(id, tableform);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            //Get Image
            try
            {

                //nam 20/7 cắt ảnh
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageUrl, batchNameLocal)));
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();

                if (type == "A")
                {
                    imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 20 / 80, 0, bm1.Width * 13 / 80, bm1.Height);
                }
                else
                {
                    imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 3 / 40, 0, (bm1.Width * 9 / 40), bm1.Height);
                    imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 30 / 40), 0, (bm2.Width * 7 / 40), bm2.Height);
                }
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Server chưa có ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            if (img.Content1_OCR[0] == img.ContentOCR[0])
            {
                txt1.Text = img.Content1_OCR[0];
                lbl1.Enabled = false;
                txt1.Enabled = false;
            }
            else
                txt1.Focus();
            if (img.Content1_OCR[1] == img.ContentOCR[1])
            {
                txt2.Text = img.Content1_OCR[1];
                lbl2.Enabled = false;
                txt2.Enabled = false;
            }
            else
            {
                if (!txt1.Visible)
                    txt2.Focus();
            }

            // hiển thị tên field name         
            lblpagename.Text = img.PageUrl;
            lblpage.Text = dAEntry.ImageExist(tableform, 3).ToString();
            dtimeBefore = DateTime.Now;
        }

        //int a = 1;
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string imgContentContent = txt1.Text + "|" + txt2.Text;
            if (imgContentContent.Contains('*'))
            {
                btnNotgood_Click(sender, e);
                return;
            }
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;          
            int lng = imgContentContent.Length - 1;
            int uID = userId;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                if (imgContentContent == (img.Content1_OCR[0] + "|" + img.Content1_OCR[1]))
                {
                    dAEntry.Add_Content_kojin_E3_Like(imgContentImageId, imgContentContent, imgContentUserId, lng, ms, tableform);
                }
                else
                {
                    dAEntry.Add_Content_kojin_E3_Unlike(imgContentImageId, imgContentContent, imgContentUserId, lng, ms, tableform);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // load image mới lên form
            // lấy id image free         
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                if (dAEntry.Count_UpHitpointLC(tableform) == 0)
                { dAEntry.UpdateHitpoint_Batch(batchId, 2); }
                else
                {
                    if (dAEntry.Count_ImageExits(tableform, 1) == 0 && dAEntry.Count_ImageExits(tableform, 2) == 0 && dAEntry.Count_ImageExits(tableform, 3) == 0)
                    { dAEntry.UpdateHitpoint_Batch(batchId, 1); }
                }
                finish = true;
                //MessageBox.Show(this,"đã hoàn thành nhập");
                this.Close();
                return;
            }

            try
            {
                //Get Image

                //nam 20/7 cắt ảnh
                imageSource = img.Imagesource;
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();

                if (type == "A")
                {
                    imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 20 / 80, 0, bm1.Width * 13 / 80, bm1.Height);
                }
                else
                {
                    imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 3 / 40, 0, (bm1.Width * 9 / 40), bm1.Height);
                    imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 30 / 40), 0, (bm2.Width * 7 / 40), bm2.Height);
                }
                currentzoom = imageViewerTR2.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            // hiển thị tên field name       
            lblpagename.Text = img.PageUrl;
            txt1.Text = ""; txt2.Text = "";
            lbl1.Enabled = true; lbl2.Enabled = true;
            txt1.Enabled = true; txt2.Enabled = true;
            if (img.Content1_OCR[0] == img.ContentOCR[0])
            {
                txt1.Text = img.Content1_OCR[0];
                lbl1.Enabled = false;
                txt1.Enabled = false;
            }
            else
                txt1.Focus();
            if (img.Content1_OCR[1] == img.ContentOCR[1])
            {
                txt2.Text = img.Content1_OCR[1];
                lbl2.Enabled = false;
                txt2.Enabled = false;
            }
            else
            {
                if (!txt1.Visible)
                    txt2.Focus();
            }
            btnSubmit.Enabled = true;
            btnNotgood.Enabled = true;
            dtimeBefore = DateTime.Now;
            //lblpage.Text = dAEntry.ImageExistE3(batchNameLocal, formid).ToString();
            lblpage.Text = dAEntry.ImageExist(tableform, 3).ToString();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmEntry_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerEntry.Stop();
            try
            {
                if (!finish)
                {
                    dAEntry.Return_HitpointEntry(img.Id, pair, tableform);
                    for (int i = 0; i < ListImage.Count; i++)
                    { dAEntry.Return_HitpointEntry(ListImage[i].Id, pair, tableform); }
                    VCB_KOMOSI.frmLogIn.fn = false;
                }
                else
                { VCB_KOMOSI.frmLogIn.fn = true; }
            }
            catch { }
            return;
        }

        private void btnNotgood_Click(object sender, EventArgs e)
        {
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            string imgContentContent = txt1.Text + "|" + txt2.Text;
            int lng = imgContentContent.Length - 1;
            int uID = userId;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                dAEntry.Add_Content_kojin_NG_E3(imgContentImageId, imgContentUserId, lng, ms, imgContentContent, tableform);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // load image mới lên form
            // lấy id image free
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                if (dAEntry.Count_UpHitpointLC(tableform) == 0)
                { dAEntry.UpdateHitpoint_Batch(batchId, 2); }
                else
                {
                    if (dAEntry.Count_ImageExits(tableform, 1) == 0 && dAEntry.Count_ImageExits(tableform, 2) == 0 && dAEntry.Count_ImageExits(tableform, 3) == 0)
                    { dAEntry.UpdateHitpoint_Batch(batchId, 1); }
                }
                finish = true;
                //MessageBox.Show(this,"đã hoàn thành nhập");
                this.Close();
                return;
            }

            try
            {
                //Get Image
                //imageSource = (Bitmap)img.Imagesource.Clone();
                //imageViewerTR1.Image = imageSource;

                //nam 20/7 cắt ảnh
                imageSource = img.Imagesource;
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();

                if (type == "A")
                {
                    imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 20 / 80, 0, bm1.Width * 13 / 80, bm1.Height);
                }
                else
                {
                    imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 3 / 40, 0, (bm1.Width * 9 / 40), bm1.Height);
                    imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 30 / 40), 0, (bm2.Width * 7 / 40), bm2.Height);
                }
                currentzoom = imageViewerTR1.CurrentZoom;
                currentzoom = imageViewerTR2.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // hiển thị tên field name
            txt1.Text = ""; txt2.Text = "";
            lbl1.Enabled = true; lbl2.Enabled = true;
            txt1.Enabled = true; txt2.Enabled = true;
            if (img.Content1_OCR[0] == img.ContentOCR[0])
            {
                txt1.Text = img.Content1_OCR[0];
                lbl1.Enabled = false;
                txt1.Enabled = false;
            }
            else
                txt1.Focus();
            if (img.Content1_OCR[1] == img.ContentOCR[1])
            {
                txt2.Text = img.Content1_OCR[1];
                lbl2.Enabled = false;
                txt2.Enabled = false;
            }
            else
            {
                if (!txt1.Enabled)
                    txt2.Focus();
            }
            btnNotgood.Enabled = true;
            btnSubmit.Enabled = true;
            dtimeBefore = DateTime.Now;
            lblpagename.Text = img.PageUrl;
            //lblpage.Text = dAEntry.ImageExistE3(batchNameLocal, formid).ToString();
            lblpage.Text = dAEntry.ImageExist(tableform, 3).ToString();
        }

        private void chkFullimage_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void frmEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && btnSubmit.Enabled && btnNotgood.Enabled)
            {
                btnSubmit_Click(sender, e);
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Escape)
                this.Close();
            if (e.Control)
            {
                if (e.KeyCode == Keys.N && btnSubmit.Enabled && btnNotgood.Enabled)
                    btnNotgood_Click(sender, e);
                if (e.KeyCode == Keys.Up)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Down)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    imageViewerTR1.RotateImage("270");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Left)
                {
                    imageViewerTR1.RotateImage("90");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.F)
                {
                    imageViewerTR1.CurrentZoom = currentzoom;
                    e.Handled = true;
                }
            }
            if (e.KeyCode == Keys.LWin || e.KeyCode == Keys.RWin)
            {
                if (e.KeyCode == Keys.D)
                {
                    this.Activate();
                }
                SendKeys.Send("{Esc}");
                SendKeys.Send("{Esc}");
                e.Handled = true;
            }
        }

        private void txt1_Enter(object sender, EventArgs e)
        {
            txt1.BackColor = Color.PowderBlue;
            txt2.BackColor = SystemColors.Window;
        }

        private void txt2_Enter(object sender, EventArgs e)
        {
            txt2.BackColor = Color.PowderBlue;
            txt1.BackColor = SystemColors.Window;
        }
        private void timerEntry_Tick(object sender, EventArgs e)
        {
            this.Activate();
            if (ListImage.Count < 4)
            {
                int id2 = dAEntry.GetIdImageE3(tableform);
                if (id2 > 0)
                {
                    BOImage_Entry img2 = dAEntry.GetImage(id2, tableform);
                    img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img2.PageUrl, batchNameLocal)));
                    ListImage.Add(img2);
                }
            }
        }

        private void frmEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txt1_TextChanged(object sender, EventArgs e)
        {
            if (txt1.Text.Length == 6)
                txt2.Focus();
        }
        //nam 20/7 hàm cắt ảnh   
        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }

        private void lbl1_Click(object sender, EventArgs e)
        {

        }
    }
}
