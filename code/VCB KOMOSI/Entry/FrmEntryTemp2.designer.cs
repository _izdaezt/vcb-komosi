﻿namespace VCB_KOMOSI
{
    partial class FrmEntryTemp2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEntryTemp2));
            this.txtOCR = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.lblpage = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.lblpagename = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.lblField = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.lblerror = new System.Windows.Forms.ToolStripLabel();
            this.txtInf = new System.Windows.Forms.ToolStripLabel();
            this.timerEntry = new System.Windows.Forms.Timer(this.components);
            this.btnExit = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblField2 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnNotgood = new System.Windows.Forms.Button();
            this.lblPressSubmit = new System.Windows.Forms.Label();
            this.txtKana = new System.Windows.Forms.TextBox();
            this.imageViewerTR1 = new ImageViewerTR.ImageViewerTR();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOCR
            // 
            this.txtOCR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOCR.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtOCR.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtOCR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtOCR.Location = new System.Drawing.Point(1179, 736);
            this.txtOCR.Multiline = true;
            this.txtOCR.Name = "txtOCR";
            this.txtOCR.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOCR.Size = new System.Drawing.Size(486, 265);
            this.txtOCR.TabIndex = 51;
            this.txtOCR.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.lblpage,
            this.toolStripLabel2,
            this.lblpagename,
            this.toolStripLabel3,
            this.lblField,
            this.toolStripLabel4,
            this.lblerror,
            this.txtInf});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1350, 25);
            this.toolStrip1.TabIndex = 60;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(22, 22);
            this.toolStripLabel1.Text = "     ";
            // 
            // lblpage
            // 
            this.lblpage.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblpage.ForeColor = System.Drawing.Color.Red;
            this.lblpage.Name = "lblpage";
            this.lblpage.Size = new System.Drawing.Size(17, 22);
            this.lblpage.Text = "0";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(22, 22);
            this.toolStripLabel2.Text = "     ";
            // 
            // lblpagename
            // 
            this.lblpagename.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblpagename.ForeColor = System.Drawing.Color.Blue;
            this.lblpagename.Name = "lblpagename";
            this.lblpagename.Size = new System.Drawing.Size(67, 22);
            this.lblpagename.Text = "PageName";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(22, 22);
            this.toolStripLabel3.Text = "     ";
            // 
            // lblField
            // 
            this.lblField.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblField.ForeColor = System.Drawing.Color.Blue;
            this.lblField.Name = "lblField";
            this.lblField.Size = new System.Drawing.Size(142, 22);
            this.lblField.Text = "Giới tính, họ và tên kana";
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(22, 22);
            this.toolStripLabel4.Text = "     ";
            // 
            // lblerror
            // 
            this.lblerror.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblerror.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblerror.ForeColor = System.Drawing.Color.Red;
            this.lblerror.Name = "lblerror";
            this.lblerror.Size = new System.Drawing.Size(0, 22);
            // 
            // txtInf
            // 
            this.txtInf.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInf.ForeColor = System.Drawing.Color.Blue;
            this.txtInf.Name = "txtInf";
            this.txtInf.Size = new System.Drawing.Size(39, 22);
            this.txtInf.Text = "Batch";
            // 
            // timerEntry
            // 
            this.timerEntry.Enabled = true;
            this.timerEntry.Interval = 200;
            this.timerEntry.Tick += new System.EventHandler(this.timerEntry_Tick);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(1317, 1);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(33, 24);
            this.btnExit.TabIndex = 81;
            this.btnExit.Text = "X";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(720, 670);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 16);
            this.label2.TabIndex = 111;
            this.label2.Text = "Ctr + N";
            // 
            // lblField2
            // 
            this.lblField2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblField2.AutoSize = true;
            this.lblField2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblField2.ForeColor = System.Drawing.Color.Blue;
            this.lblField2.Location = new System.Drawing.Point(601, 425);
            this.lblField2.Name = "lblField2";
            this.lblField2.Size = new System.Drawing.Size(101, 18);
            this.lblField2.TabIndex = 107;
            this.lblField2.Text = "Họ tên Kana";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSubmit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSubmit.BackgroundImage")));
            this.btnSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSubmit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnSubmit.Location = new System.Drawing.Point(475, 594);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(151, 73);
            this.btnSubmit.TabIndex = 108;
            this.btnSubmit.TabStop = false;
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnNotgood
            // 
            this.btnNotgood.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNotgood.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNotgood.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnNotgood.ForeColor = System.Drawing.Color.Red;
            this.btnNotgood.Location = new System.Drawing.Point(668, 594);
            this.btnNotgood.Name = "btnNotgood";
            this.btnNotgood.Size = new System.Drawing.Size(151, 72);
            this.btnNotgood.TabIndex = 109;
            this.btnNotgood.TabStop = false;
            this.btnNotgood.Text = "Not Good";
            this.btnNotgood.UseVisualStyleBackColor = true;
            this.btnNotgood.Click += new System.EventHandler(this.btnNotgood_Click);
            // 
            // lblPressSubmit
            // 
            this.lblPressSubmit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblPressSubmit.AutoSize = true;
            this.lblPressSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblPressSubmit.ForeColor = System.Drawing.Color.Red;
            this.lblPressSubmit.Location = new System.Drawing.Point(529, 670);
            this.lblPressSubmit.Name = "lblPressSubmit";
            this.lblPressSubmit.Size = new System.Drawing.Size(44, 16);
            this.lblPressSubmit.TabIndex = 110;
            this.lblPressSubmit.Text = "Enter";
            // 
            // txtKana
            // 
            this.txtKana.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtKana.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtKana.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtKana.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKana.Location = new System.Drawing.Point(239, 453);
            this.txtKana.Name = "txtKana";
            this.txtKana.Size = new System.Drawing.Size(832, 116);
            this.txtKana.TabIndex = 2;
            this.txtKana.Enter += new System.EventHandler(this.txt2_Enter);
            this.txtKana.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt2_KeyDown);
            // 
            // imageViewerTR1
            // 
            this.imageViewerTR1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.imageViewerTR1.BackColor = System.Drawing.Color.LightGray;
            this.imageViewerTR1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imageViewerTR1.CurrentZoom = 1F;
            this.imageViewerTR1.Image = null;
            this.imageViewerTR1.Location = new System.Drawing.Point(87, 50);
            this.imageViewerTR1.MaxZoom = 20F;
            this.imageViewerTR1.MinZoom = 0.05F;
            this.imageViewerTR1.Name = "imageViewerTR1";
            this.imageViewerTR1.Size = new System.Drawing.Size(1156, 367);
            this.imageViewerTR1.TabIndex = 113;
            // 
            // FrmEntryTemp2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.ClientSize = new System.Drawing.Size(1350, 730);
            this.Controls.Add(this.imageViewerTR1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblField2);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnNotgood);
            this.Controls.Add(this.lblPressSubmit);
            this.Controls.Add(this.txtKana);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.txtOCR);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEntryTemp2";
            this.Text = "VCB - Entry ";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEntry_FormClosing);
            this.Load += new System.EventHandler(this.frmEntry_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEntry_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmEntry_KeyPress);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public static System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.TextBox txtOCR;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel lblpage;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel lblpagename;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripLabel lblField;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.Timer timerEntry;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel lblerror;
        private System.Windows.Forms.Button btnExit;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblField2;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnNotgood;
        internal System.Windows.Forms.Label lblPressSubmit;
        private System.Windows.Forms.TextBox txtKana;
        private ImageViewerTR.ImageViewerTR imageViewerTR1;
        private System.Windows.Forms.ToolStripLabel txtInf;
    }
}