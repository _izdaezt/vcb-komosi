﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace VCB_KOMOSI
{
    class BOImage_Entry
    {
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private int fieldId;
        public int FieldId
        {
            get { return fieldId; }
            set { fieldId = value; }
        }

        private string fieldName;
        public string FieldName
        {
            get { return fieldName; }
            set { fieldName = value; }
        }

        private string pageUrl;
        public string PageUrl
        {
            get { return pageUrl; }
            set { pageUrl = value; }
        }

        private string[] content1_OCR;
        public string[] Content1_OCR
        {
            get { return content1_OCR; }
            set { content1_OCR = value; }
        }
        private string[] contentOCR;
        public string[] ContentOCR
        {
            get { return contentOCR; }
            set { contentOCR = value; }
        }

        private string pathUri;
        public string PathUri
        {
            get { return pathUri; }
            set { pathUri = value; }
        }

        private int hitPoint;
        public int HitPoint
        {
            get { return hitPoint; }
            set { hitPoint = value; }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        private byte[] image;
        public byte[] Image
        {
            get { return image; }
            set { image = value; }
        }
        private int leng;
        public int Leng
        {
            get { return leng; }
            set { leng = value; }
        }

        private string pageName;
        public string PageName
        {
            get { return pageName; }
            set { pageName = value; }
        }
        private string fieldType;
        public string FieldType
        {
            get { return fieldType; }
            set { fieldType = value; }
        }
        private Bitmap imagesource;
        public Bitmap Imagesource
        {
            get { return imagesource; }
            set { imagesource = value; }
        }
    }
}
