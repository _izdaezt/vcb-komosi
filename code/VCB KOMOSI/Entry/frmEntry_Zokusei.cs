﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
namespace VCB_KOMOSI
{
    public partial class frmEntry_Zokusei : Form
    {
        #region var
        public static Point locationOnForm;
        public static bool selecthitpoint = true;
        public int batchId;
        public int userId;
        public int formid;
        public int pair;
        public string userName;
        public string batchName;
        public string template;
        public string level;
        public string group;
        public string formName = "";
        Bitmap imageSource;
        private DAEntry_Entry dAEntry = new DAEntry_Entry();
        private BOImage_Entry img = new BOImage_Entry();
        private bool finish = false;
        int[] INFImage;
        public static int locationX = 0;
        public static int locationY = 0;
        float currentzoom;
        // value bool Japan
        public static DataTable dtMBD;
        DataTable dtSubField = new DataTable();
        List<TextBox> lsttxt = new List<TextBox>();
        List<Label> lstLblSubFieldName = new List<Label>();
        List<Label> lstLblSubFieldLimit = new List<Label>();
        List<BOImage_Entry> ListImage = new List<BOImage_Entry>();
        DBsql dbsql = new DBsql();
        string batchNameLocal;
        DateTime dtimeBefore = new DateTime();

        //nam 20/7 tao bien kiem tra chu cai dau thu muc
        string tableform;

        #endregion
        public frmEntry_Zokusei()
        {
            InitializeComponent();
            lsttxt = new List<TextBox>() { txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9, txt10, txt11, txt12, txt13, txt14, txt15, txt16, txt17, txt18, txt19, txt20 };
        }

        private void frmEntry_Load(object sender, EventArgs e)
        {
            // hiển thị thông tin user          
            txtInf.Text = "VCB Entry  --  UserName: " + userName + "  -- Template: " + formName + "  --  Batch: " + batchName;
            batchNameLocal = batchName.Substring(7);
            //userid
            int uID = userId;

            // hien thi giua man hinh
            this.CenterToScreen();
            dAEntry = new DAEntry_Entry();

            // lấy id image free
            tableform = batchId + "--" + batchNameLocal;
            int id = 0;
            try
            {
                id = dAEntry.Get_IdImage(tableform, pair);
                if (id == 0)
                {
                    if (dAEntry.Count_UpHitpointLC(tableform) == 0)
                    { dAEntry.UpdateHitpoint_Batch(batchId, 2); }
                    else
                    {
                        if (dAEntry.Count_ImageExits(tableform, 1) == 0 && dAEntry.Count_ImageExits(tableform, 2) == 0 && dAEntry.Count_ImageExits(tableform, 3) == 0)
                        { dAEntry.UpdateHitpoint_Batch(batchId, 1); }
                    }
                    finish = true;
                    //MessageBox.Show("đã hoàn thành nhập");
                    this.Close();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            timerEntry.Start();


            // lay image free
            try
            {
                img = dAEntry.GetImage(id, tableform);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            //Get Image
            try
            {
                //nam 20/7 cắt ảnh
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageUrl, batchNameLocal)));
                Bitmap bm = CropBitmap(imageSource, imageSource.Width / 2, 0, imageSource.Width / 2, imageSource.Height / 2);
                imageViewerTR1.Image = bm;
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Server chưa có ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // hiển thị tên field name         
            lblpagename.Text = img.PageUrl;
            dtimeBefore = DateTime.Now;
            lblpage.Text = (dAEntry.ImageExist(tableform, pair)).ToString();
            lsttxt.ForEach(a =>
            {
                a.Enter += new System.EventHandler(this.textBox1_Enter);
                a.Leave += new System.EventHandler(this.textBox1_Leave);
                a.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            });
            txt1.Focus();
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.BackColor = SystemColors.Window;
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.BackColor = Color.PowderBlue;
            tb.SelectAll();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            string autoxh = tb.Text;
            if (tb.TabIndex == 1 || tb.TabIndex == 2 || tb.TabIndex == 3 || tb.TabIndex == 6 || tb.TabIndex == 7 || tb.TabIndex == 8 || tb.TabIndex == 11 
                || tb.TabIndex == 12 || tb.TabIndex == 13 || tb.TabIndex == 16 || tb.TabIndex == 17 || tb.TabIndex == 18)
            {
                if (autoxh.Length == 2)
                    lsttxt[tb.TabIndex].Focus();
            }
            //else if (tb.TabIndex == 4 || tb.TabIndex == 9 || tb.TabIndex == 14 || tb.TabIndex == 19)
            //{
            //    if (autoxh.Length == 4)
            //        lsttxt[tb.TabIndex].Focus();
            //}
            else if (tb.TabIndex == 5 || tb.TabIndex == 10 || tb.TabIndex == 15)
            {
                if (autoxh.Length == 1)
                    lsttxt[tb.TabIndex].Focus();
            }
        }

        static bool IsValid(string value)
        {
            return Regex.IsMatch(value, @"^[0-9.* ]*$");
        }

        //int a = 1;
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (!IsValid(txt1.Text))
                txt1.Text = "";
            if (!IsValid(txt2.Text))
                txt2.Text = "";
            if (!IsValid(txt3.Text))
                txt3.Text = "";
            if (txt4.Text.Length == 4)
            {
                if (!IsValid(txt4.Text) || (txt4.Text[0] != ' ' && txt4.Text[1] == ' ' && txt4.Text[2] == ' ' && txt4.Text[3] != ' ') || (txt4.Text[0] != ' ' && txt4.Text[1] == ' ' && txt4.Text[2] != ' ' && txt4.Text[3] != ' ') || (txt4.Text[0] != ' ' && txt4.Text[1] != ' ' && txt4.Text[2] == ' ' && txt4.Text[3] != ' '))
                    txt4.Text = "";
            }
            else if (txt4.Text.Length > 4)
            {
                txt4.Text = "";
            }
            else if (txt4.Text.Length == 3)
            {
                if (!IsValid(txt4.Text) || (txt4.Text[0] != ' ' && txt4.Text[1] == ' ' && txt4.Text[2] != ' '))
                    txt4.Text = "";
            }
            else
            { 
                if (!IsValid(txt4.Text))
                    txt4.Text = "";
            }
            if (!IsValid(txt5.Text))
                txt5.Text = "";
            if (!IsValid(txt6.Text))
                txt6.Text = "";
            if (!IsValid(txt7.Text))
                txt7.Text = "";
            if (!IsValid(txt8.Text))
                txt8.Text = "";
            if (txt9.Text.Length == 4)
            {
                if (!IsValid(txt9.Text) || (txt9.Text[0] != ' ' && txt9.Text[1] == ' ' && txt9.Text[2] == ' ' && txt9.Text[3] != ' ') || (txt9.Text[0] != ' ' && txt9.Text[1] == ' ' && txt9.Text[2] != ' ' && txt9.Text[3] != ' ') || (txt9.Text[0] != ' ' && txt9.Text[1] != ' ' && txt9.Text[2] == ' ' && txt9.Text[3] != ' '))
                    txt9.Text = "";
            }
            else if (txt9.Text.Length > 4)
            {
                txt9.Text = "";
            }
            else if (txt9.Text.Length == 3)
            {
                if (!IsValid(txt9.Text) || (txt9.Text[0] != ' ' && txt9.Text[1] == ' ' && txt9.Text[2] != ' '))
                    txt9.Text = "";
            }
            else 
            {
                if (!IsValid(txt9.Text))
                    txt9.Text = "";
            }
            if (!IsValid(txt10.Text))
                txt10.Text = "";
            if (!IsValid(txt11.Text))
                txt11.Text = "";
            if (!IsValid(txt12.Text))
                txt12.Text = "";
            if (!IsValid(txt13.Text))
                txt13.Text = "";
            if (txt14.Text.Length == 4)
            {
                if (!IsValid(txt14.Text) || (txt14.Text[0] != ' ' && txt14.Text[1] == ' ' && txt14.Text[2] == ' ' && txt14.Text[3] != ' ') || (txt14.Text[0] != ' ' && txt14.Text[1] == ' ' && txt14.Text[2] != ' ' && txt14.Text[3] != ' ') || (txt14.Text[0] != ' ' && txt14.Text[1] != ' ' && txt14.Text[2] == ' ' && txt14.Text[3] != ' '))
                    txt14.Text = "";
            }
            else if (txt14.Text.Length > 4)
            {
                txt14.Text = "";
            }
            else if (txt14.Text.Length == 3)
            {
                if (!IsValid(txt14.Text) || (txt14.Text[0] != ' ' && txt14.Text[1] == ' ' && txt14.Text[2] != ' '))
                    txt14.Text = "";
            }
            else
            {
                if (!IsValid(txt14.Text))
                    txt4.Text = "";
            }
            if (!IsValid(txt15.Text))
                txt15.Text = "";
            if (!IsValid(txt16.Text))
                txt16.Text = "";
            if (!IsValid(txt17.Text))
                txt17.Text = "";
            if (!IsValid(txt18.Text))
                txt18.Text = "";
            if (txt19.Text.Length == 4)
            {
                if (!IsValid(txt19.Text) || (txt19.Text[0] != ' ' && txt19.Text[1] == ' ' && txt19.Text[2] == ' ' && txt19.Text[3] != ' ') || (txt19.Text[0] != ' ' && txt19.Text[1] == ' ' && txt19.Text[2] != ' ' && txt19.Text[3] != ' ') || (txt19.Text[0] != ' ' && txt19.Text[1] != ' ' && txt19.Text[2] == ' ' && txt19.Text[3] != ' '))
                    txt19.Text = "";
            }
            else if (txt19.Text.Length > 4)
            {
                txt19.Text = "";
            }
            else if (txt19.Text.Length == 3)
            {
                if (!IsValid(txt19.Text) || (txt19.Text[0] != ' ' && txt19.Text[1] == ' ' && txt19.Text[2] != ' '))
                    txt19.Text = "";
            }
            else
            {
                if (!IsValid(txt19.Text))
                    txt19.Text = "";
            }
            if (!IsValid(txt20.Text))
                txt20.Text = "";
            string imgContentContent = txt1.Text.Trim() + "|" + txt2.Text.Trim() + "|" + txt3.Text.Trim() + "|" + txt4.Text.Trim() + "|" + txt5.Text.Trim() + "|" + txt6.Text.Trim() + "|" + txt7.Text.Trim() + "|" + txt8.Text.Trim() + "|" + txt9.Text.Trim() + "|" + txt10.Text.Trim() +
                 "|" + txt11.Text.Trim() + "|" + txt12.Text.Trim() + "|" + txt13.Text.Trim() + "|" + txt14.Text.Trim() + "|" + txt15.Text.Trim() + "|" + txt16.Text.Trim() + "|" + txt17.Text.Trim() + "|" + txt18.Text.Trim() + "|" + txt19.Text.Trim() + "|" + txt20.Text.Trim();
            if (imgContentContent.Contains('*'))
            {
                btnNotgood_Click(sender, e);
                return;
            }
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            int lng = imgContentContent.Length - 19;
            int uID = userId;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                int pairtemp = 1;
                if (pair == 1)
                {
                    pairtemp = 2;
                }
                string getContent = dAEntry.getContentByPair(tableform, pairtemp, imgContentImageId);
                if (getContent != "null")
                {
                    if (getContent == imgContentContent)
                    {
                        dAEntry.Add_Content_LV3_Like(imgContentImageId, imgContentContent, imgContentUserId, lng, ms, pair, tableform);
                    }
                    else
                    {
                        if (getContent.Contains("*"))
                        {
                            dAEntry.Add_Content_LV3_ContainsSao(imgContentImageId, imgContentContent, imgContentUserId, lng, ms, pair, tableform);
                        }
                        else
                        {
                            dAEntry.Add_Content_LV3_Unlike(imgContentImageId, imgContentContent, imgContentUserId, lng, ms, pair, tableform);
                        }
                    }
                }
                else
                {
                    dAEntry.Add_Content_LV3_Unlike(imgContentImageId, imgContentContent, imgContentUserId, lng, ms, pair, tableform);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // load image mới lên form
            // lấy id image free         
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                if (dAEntry.Count_UpHitpointLC(tableform) == 0)
                { dAEntry.UpdateHitpoint_Batch(batchId, 2); }
                else
                {
                    if (dAEntry.Count_ImageExits(tableform, 1) == 0 && dAEntry.Count_ImageExits(tableform, 2) == 0 && dAEntry.Count_ImageExits(tableform, 3) == 0)
                    { dAEntry.UpdateHitpoint_Batch(batchId, 1); }
                }
                finish = true;
                //MessageBox.Show(this,"đã hoàn thành nhập");
                this.Close();
                return;
            }

            try
            {

                //nam 20/7 cắt ảnh
                imageSource = img.Imagesource;
                Bitmap bm = CropBitmap(imageSource, imageSource.Width / 2, 0, imageSource.Width / 2, imageSource.Height / 2);
                imageViewerTR1.Image = bm;
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            btnSubmit.Enabled = true;
            btnNotgood.Enabled = true;
            // hiển thị tên field name       
            dtimeBefore = DateTime.Now;
            lblpagename.Text = img.PageUrl;
            txt1.Text = ""; txt2.Text = ""; txt3.Text = ""; txt4.Text = ""; txt5.Text = ""; txt6.Text = ""; txt7.Text = ""; txt8.Text = ""; txt9.Text = ""; txt10.Text = "";
            txt11.Text = ""; txt12.Text = ""; txt13.Text = ""; txt14.Text = ""; txt15.Text = ""; txt16.Text = ""; txt17.Text = ""; txt18.Text = ""; txt19.Text = ""; txt20.Text = "";
            lblpage.Text = (dAEntry.ImageExist(tableform, pair)).ToString();
            txt1.Focus();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmEntry_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerEntry.Stop();
            try
            {
                if (!finish)
                {
                    dAEntry.Return_HitpointEntry(img.Id, pair, tableform);
                    for (int i = 0; i < ListImage.Count; i++)
                    { dAEntry.Return_HitpointEntry(ListImage[i].Id, pair, tableform); }
                    VCB_KOMOSI.frmLogIn.fn = false;
                }
                else
                { VCB_KOMOSI.frmLogIn.fn = true; }
            }
            catch { }
            return;
        }

        private void btnNotgood_Click(object sender, EventArgs e)
        {
            if (!IsValid(txt1.Text))
                txt1.Text = "";
            if (!IsValid(txt2.Text))
                txt2.Text = "";
            if (!IsValid(txt3.Text))
                txt3.Text = "";
            if (txt4.Text.Length == 4)
            {
                if (!IsValid(txt4.Text) || (txt4.Text[0] != ' ' && txt4.Text[1] == ' ' && txt4.Text[2] == ' ' && txt4.Text[3] != ' ') || (txt4.Text[0] != ' ' && txt4.Text[1] == ' ' && txt4.Text[2] != ' ' && txt4.Text[3] != ' ') || (txt4.Text[0] != ' ' && txt4.Text[1] != ' ' && txt4.Text[2] == ' ' && txt4.Text[3] != ' '))
                    txt4.Text = "";
            }
            else if (txt4.Text.Length > 4)
            {
                txt4.Text = "";
            }
            else if (txt4.Text.Length == 3)
            {
                if (!IsValid(txt4.Text) || (txt4.Text[0] != ' ' && txt4.Text[1] == ' ' && txt4.Text[2] != ' '))
                    txt4.Text = "";
            }
            else
            {
                if (!IsValid(txt4.Text))
                    txt4.Text = "";
            }
            if (!IsValid(txt5.Text))
                txt5.Text = "";
            if (!IsValid(txt6.Text))
                txt6.Text = "";
            if (!IsValid(txt7.Text))
                txt7.Text = "";
            if (!IsValid(txt8.Text))
                txt8.Text = "";
            if (txt9.Text.Length == 4)
            {
                if (!IsValid(txt9.Text) || (txt9.Text[0] != ' ' && txt9.Text[1] == ' ' && txt9.Text[2] == ' ' && txt9.Text[3] != ' ') || (txt9.Text[0] != ' ' && txt9.Text[1] == ' ' && txt9.Text[2] != ' ' && txt9.Text[3] != ' ') || (txt9.Text[0] != ' ' && txt9.Text[1] != ' ' && txt9.Text[2] == ' ' && txt9.Text[3] != ' '))
                    txt9.Text = "";
            }
            else if (txt9.Text.Length > 4)
            {
                txt9.Text = "";
            }
            else if (txt9.Text.Length == 3)
            {
                if (!IsValid(txt9.Text) || (txt9.Text[0] != ' ' && txt9.Text[1] == ' ' && txt9.Text[2] != ' '))
                    txt9.Text = "";
            }
            else
            {
                if (!IsValid(txt9.Text))
                    txt9.Text = "";
            }
            if (!IsValid(txt10.Text))
                txt10.Text = "";
            if (!IsValid(txt11.Text))
                txt11.Text = "";
            if (!IsValid(txt12.Text))
                txt12.Text = "";
            if (!IsValid(txt13.Text))
                txt13.Text = "";
            if (txt14.Text.Length == 4)
            {
                if (!IsValid(txt14.Text) || (txt14.Text[0] != ' ' && txt14.Text[1] == ' ' && txt14.Text[2] == ' ' && txt14.Text[3] != ' ') || (txt14.Text[0] != ' ' && txt14.Text[1] == ' ' && txt14.Text[2] != ' ' && txt14.Text[3] != ' ') || (txt14.Text[0] != ' ' && txt14.Text[1] != ' ' && txt14.Text[2] == ' ' && txt14.Text[3] != ' '))
                    txt14.Text = "";
            }
            else if (txt14.Text.Length > 4)
            {
                txt14.Text = "";
            }
            else if (txt14.Text.Length == 3)
            {
                if (!IsValid(txt14.Text) || (txt14.Text[0] != ' ' && txt14.Text[1] == ' ' && txt14.Text[2] != ' '))
                    txt14.Text = "";
            }
            else
            {
                if (!IsValid(txt14.Text))
                    txt14.Text = "";
            }
            if (!IsValid(txt15.Text))
                txt15.Text = "";
            if (!IsValid(txt16.Text))
                txt16.Text = "";
            if (!IsValid(txt17.Text))
                txt17.Text = "";
            if (!IsValid(txt18.Text))
                txt18.Text = "";
            if (txt19.Text.Length == 4)
            {
                if (!IsValid(txt19.Text) || (txt19.Text[0] != ' ' && txt19.Text[1] == ' ' && txt19.Text[2] == ' ' && txt19.Text[3] != ' ') || (txt19.Text[0] != ' ' && txt19.Text[1] == ' ' && txt19.Text[2] != ' ' && txt19.Text[3] != ' ') || (txt19.Text[0] != ' ' && txt19.Text[1] != ' ' && txt19.Text[2] == ' ' && txt19.Text[3] != ' '))
                    txt19.Text = "";
            }
            else if (txt19.Text.Length > 4)
            {
                txt19.Text = "";
            }
            else if (txt19.Text.Length == 3)
            {
                if (!IsValid(txt19.Text) || (txt19.Text[0] != ' ' && txt19.Text[1] == ' ' && txt19.Text[2] != ' '))
                    txt19.Text = "";
            }
            else
            {
                if (!IsValid(txt19.Text))
                    txt19.Text = "";
            }
            if (!IsValid(txt20.Text))
                txt20.Text = "";
            string imgContentContent = txt1.Text.Trim() + "|" + txt2.Text.Trim() + "|" + txt3.Text.Trim() + "|" + txt4.Text.Trim() + "|" + txt5.Text.Trim() + "|" + txt6.Text.Trim() + "|" + txt7.Text.Trim() + "|" + txt8.Text.Trim() + "|" + txt9.Text.Trim() + "|" + txt10.Text.Trim() +
                 "|" + txt11.Text.Trim() + "|" + txt12.Text.Trim() + "|" + txt13.Text.Trim() + "|" + txt14.Text.Trim() + "|" + txt15.Text.Trim() + "|" + txt16.Text.Trim() + "|" + txt17.Text.Trim() + "|" + txt18.Text.Trim() + "|" + txt19.Text.Trim() + "|" + txt20.Text.Trim();
            imageViewerTR1.Dispose();
            btnSubmit.Enabled = false;
            btnNotgood.Enabled = false;
            int lng = imgContentContent.Length - 19;
            int uID = userId;
            int imgContentImageId = img.Id;
            int imgContentUserId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            try
            {
                dAEntry.Add_Content_NG_LV3(imgContentImageId, imgContentUserId, lng, ms, imgContentContent, pair, tableform);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            // load image mới lên form
            // lấy id image free
            imageSource = null;
            INFImage = new int[4];
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                if (dAEntry.Count_UpHitpointLC(tableform) == 0)
                { dAEntry.UpdateHitpoint_Batch(batchId, 2); }
                else
                {
                    if (dAEntry.Count_ImageExits(tableform, 1) == 0 && dAEntry.Count_ImageExits(tableform, 2) == 0 && dAEntry.Count_ImageExits(tableform, 3) == 0)
                    { dAEntry.UpdateHitpoint_Batch(batchId, 1); }
                }
                finish = true;
                //MessageBox.Show(this,"đã hoàn thành nhập");
                this.Close();
                return;
            }

            try
            {

                //nam 20/7 cắt ảnh
                imageSource = img.Imagesource;
                Bitmap bm = CropBitmap(imageSource, imageSource.Width / 2, 0, imageSource.Width / 2, imageSource.Height / 2);
                imageViewerTR1.Image = bm;
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageUrl, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // hiển thị tên field name
            btnNotgood.Enabled = true;
            btnSubmit.Enabled = true;
            dtimeBefore = DateTime.Now;
            lblpagename.Text = img.PageUrl;
            lblpage.Text = (dAEntry.ImageExist(tableform, pair)).ToString();
            txt1.Text = ""; txt2.Text = ""; txt3.Text = ""; txt4.Text = ""; txt5.Text = ""; txt6.Text = ""; txt7.Text = ""; txt8.Text = ""; txt9.Text = ""; txt10.Text = "";
            txt11.Text = ""; txt12.Text = ""; txt13.Text = ""; txt14.Text = ""; txt15.Text = ""; txt16.Text = ""; txt17.Text = ""; txt18.Text = ""; txt19.Text = ""; txt20.Text = "";
            txt1.Focus();
        }

        private void chkFullimage_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void frmEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && btnSubmit.Enabled && btnNotgood.Enabled)
            {
                btnSubmit_Click(sender, e);
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Escape)
                this.Close();
            else if (e.KeyCode == Keys.Right)
            {
                if (lsttxt[lsttxt.Count - 1].Focused)
                {
                    lsttxt[0].Focus();
                }
                else
                {
                    for (int i = lsttxt.Count - 1; i >= 0; i--)
                    {
                        if (lsttxt[i].Focused)
                        {
                            lsttxt[i + 1].Focus();
                        }
                    }
                }
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Left)
            {
                if (lsttxt[0].Focused)
                {
                    lsttxt[lsttxt.Count - 1].Focus();
                }
                else
                {
                    for (int i = 0; i < lsttxt.Count; i++)
                    {
                        if (lsttxt[i].Focused)
                        {
                            lsttxt[i - 1].Focus();
                        }
                    }
                }
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Up)
            {
                if (lsttxt[0].Focused)
                {
                    lsttxt[15].Focus();
                }
                else if (lsttxt[1].Focused)
                {
                    lsttxt[16].Focus();
                }
                else if (lsttxt[2].Focused)
                {
                    lsttxt[17].Focus();
                }
                else if (lsttxt[3].Focused)
                {
                    lsttxt[18].Focus();
                }
                else if (lsttxt[4].Focused)
                {
                    lsttxt[19].Focus();
                }
                else
                {
                    for (int i = 5; i < lsttxt.Count; i++)
                    {
                        if (lsttxt[i].Focused)
                            lsttxt[i - 5].Focus();
                    }
                }
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Down)
            {
                if (lsttxt[15].Focused)
                {
                    lsttxt[0].Focus();
                }
                else if (lsttxt[16].Focused)
                {
                    lsttxt[1].Focus();
                }
                else if (lsttxt[17].Focused)
                {
                    lsttxt[2].Focus();
                }
                else if (lsttxt[18].Focused)
                {
                    lsttxt[3].Focus();
                }
                else if (lsttxt[19].Focused)
                {
                    lsttxt[4].Focus();
                }
                else
                {
                    for (int i = 14; i >= 0; i--)
                    {
                        if (lsttxt[i].Focused)
                        {
                            lsttxt[i + 5].Focus();
                        }
                    }
                }
                e.Handled = true;
            }
            if (e.Control)
            {
                if (e.KeyCode == Keys.N && btnNotgood.Enabled && btnSubmit.Enabled)
                    btnNotgood_Click(sender, e);
                if (e.Control)
                {
                    if (e.KeyCode == Keys.Up)
                    {
                        imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
                        e.Handled = true;
                    }
                    else if (e.KeyCode == Keys.Down)
                    {
                        imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
                        e.Handled = true;
                    }
                    else if (e.KeyCode == Keys.Right)
                    {
                        imageViewerTR1.RotateImage("270");
                        e.Handled = true;
                    }
                    else if (e.KeyCode == Keys.Left)
                    {
                        imageViewerTR1.RotateImage("90");
                        e.Handled = true;
                    }
                    else if (e.KeyCode == Keys.F)
                    {
                        imageViewerTR1.CurrentZoom = currentzoom;
                        e.Handled = true;
                    }
                }
                if (e.KeyCode == Keys.Escape)
                    this.Close();
            }
            if (e.KeyCode == Keys.LWin || e.KeyCode == Keys.RWin)
            {
                if (e.KeyCode == Keys.D)
                {
                    this.Activate();
                }
                SendKeys.Send("{Esc}");
                SendKeys.Send("{Esc}");
                e.Handled = true;
            }
        }

        private void timerEntry_Tick(object sender, EventArgs e)
        {
            this.Activate();
            if (ListImage.Count < 4)
            {
                int id2 = dAEntry.Get_IdImage(tableform, pair);
                if (id2 > 0)
                {
                    BOImage_Entry img2 = dAEntry.GetImage(id2, tableform);
                    img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img2.PageUrl, batchNameLocal)));
                    ListImage.Add(img2);
                }
            }
        }

        private void frmEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
            }
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //nam 20/7 hàm cắt ảnh        
        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }

    }
}
