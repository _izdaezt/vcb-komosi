﻿namespace VCB_KOMOSI
{
    partial class FrmEntryTemp4_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEntryTemp4_1));
            this.timerEntry = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.lblpage = new System.Windows.Forms.ToolStripTextBox();
            this.lblpagename = new System.Windows.Forms.ToolStripTextBox();
            this.txtInf = new System.Windows.Forms.ToolStripTextBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.lblField1 = new System.Windows.Forms.Label();
            this.btnNotgood = new System.Windows.Forms.Button();
            this.lblPressSubmit = new System.Windows.Forms.Label();
            this.txt5 = new System.Windows.Forms.TextBox();
            this.txt6 = new System.Windows.Forms.TextBox();
            this.txt7 = new System.Windows.Forms.TextBox();
            this.txt8 = new System.Windows.Forms.TextBox();
            this.txt9 = new System.Windows.Forms.TextBox();
            this.txt10 = new System.Windows.Forms.TextBox();
            this.txt11 = new System.Windows.Forms.TextBox();
            this.txt12 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtl48 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtl45 = new System.Windows.Forms.TextBox();
            this.txtl47 = new System.Windows.Forms.TextBox();
            this.txtl46 = new System.Windows.Forms.TextBox();
            this.txtl42 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtl41 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtl44 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtl43 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtl35 = new System.Windows.Forms.TextBox();
            this.txtl37 = new System.Windows.Forms.TextBox();
            this.txtl36 = new System.Windows.Forms.TextBox();
            this.txtl32 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtl31 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtl34 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtl33 = new System.Windows.Forms.TextBox();
            this.txt113 = new System.Windows.Forms.TextBox();
            this.txt112 = new System.Windows.Forms.TextBox();
            this.txt114 = new System.Windows.Forms.TextBox();
            this.txt115 = new System.Windows.Forms.TextBox();
            this.txt116 = new System.Windows.Forms.TextBox();
            this.txt111 = new System.Windows.Forms.TextBox();
            this.txt117 = new System.Windows.Forms.TextBox();
            this.txtl22 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtl21 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtl24 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtl23 = new System.Windows.Forms.TextBox();
            this.txt1 = new System.Windows.Forms.TextBox();
            this.txt4 = new System.Windows.Forms.TextBox();
            this.txt3 = new System.Windows.Forms.TextBox();
            this.txt2 = new System.Windows.Forms.TextBox();
            this.imageViewerTR1 = new ImageViewerTR.ImageViewerTR();
            this.txtl25 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtl27 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtl26 = new System.Windows.Forms.TextBox();
            this.imageViewerTR2 = new ImageViewerTR.ImageViewerTR();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerEntry
            // 
            this.timerEntry.Enabled = true;
            this.timerEntry.Interval = 200;
            this.timerEntry.Tick += new System.EventHandler(this.timerEntry_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblpage,
            this.lblpagename,
            this.txtInf});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1350, 33);
            this.menuStrip1.TabIndex = 61;
            this.menuStrip1.Text = "Zoom Out";
            // 
            // lblpage
            // 
            this.lblpage.Enabled = false;
            this.lblpage.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpage.ForeColor = System.Drawing.Color.Red;
            this.lblpage.Name = "lblpage";
            this.lblpage.ReadOnly = true;
            this.lblpage.Size = new System.Drawing.Size(50, 29);
            // 
            // lblpagename
            // 
            this.lblpagename.Enabled = false;
            this.lblpagename.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpagename.ForeColor = System.Drawing.Color.Blue;
            this.lblpagename.Name = "lblpagename";
            this.lblpagename.ReadOnly = true;
            this.lblpagename.Size = new System.Drawing.Size(300, 29);
            // 
            // txtInf
            // 
            this.txtInf.AutoToolTip = true;
            this.txtInf.Enabled = false;
            this.txtInf.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInf.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.txtInf.Name = "txtInf";
            this.txtInf.ReadOnly = true;
            this.txtInf.Size = new System.Drawing.Size(700, 29);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(1317, 1);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(33, 24);
            this.btnExit.TabIndex = 81;
            this.btnExit.TabStop = false;
            this.btnExit.Text = "X";
            this.btnExit.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(21, 628);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 32);
            this.label2.TabIndex = 111;
            this.label2.Text = "Ctr + N\r\n(Not Good)";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSubmit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSubmit.BackgroundImage")));
            this.btnSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSubmit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnSubmit.Location = new System.Drawing.Point(348, 314);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(90, 35);
            this.btnSubmit.TabIndex = 108;
            this.btnSubmit.TabStop = false;
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // lblField1
            // 
            this.lblField1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblField1.AutoSize = true;
            this.lblField1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblField1.ForeColor = System.Drawing.Color.Blue;
            this.lblField1.Location = new System.Drawing.Point(743, 561);
            this.lblField1.Name = "lblField1";
            this.lblField1.Size = new System.Drawing.Size(20, 24);
            this.lblField1.TabIndex = 106;
            this.lblField1.Text = "1";
            // 
            // btnNotgood
            // 
            this.btnNotgood.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNotgood.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNotgood.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnNotgood.ForeColor = System.Drawing.Color.Red;
            this.btnNotgood.Location = new System.Drawing.Point(262, 330);
            this.btnNotgood.Name = "btnNotgood";
            this.btnNotgood.Size = new System.Drawing.Size(83, 34);
            this.btnNotgood.TabIndex = 109;
            this.btnNotgood.TabStop = false;
            this.btnNotgood.Text = "Not Good";
            this.btnNotgood.UseVisualStyleBackColor = true;
            this.btnNotgood.Click += new System.EventHandler(this.btnNotgood_Click);
            // 
            // lblPressSubmit
            // 
            this.lblPressSubmit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblPressSubmit.AutoSize = true;
            this.lblPressSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblPressSubmit.ForeColor = System.Drawing.Color.Red;
            this.lblPressSubmit.Location = new System.Drawing.Point(21, 536);
            this.lblPressSubmit.Name = "lblPressSubmit";
            this.lblPressSubmit.Size = new System.Drawing.Size(65, 32);
            this.lblPressSubmit.TabIndex = 110;
            this.lblPressSubmit.Text = "Enter\r\n(Submit)";
            // 
            // txt5
            // 
            this.txt5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt5.BackColor = System.Drawing.SystemColors.Window;
            this.txt5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt5.Location = new System.Drawing.Point(766, 550);
            this.txt5.MaxLength = 10;
            this.txt5.Name = "txt5";
            this.txt5.ShortcutsEnabled = false;
            this.txt5.Size = new System.Drawing.Size(211, 44);
            this.txt5.TabIndex = 5;
            // 
            // txt6
            // 
            this.txt6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt6.BackColor = System.Drawing.SystemColors.Window;
            this.txt6.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6.Location = new System.Drawing.Point(766, 600);
            this.txt6.MaxLength = 10;
            this.txt6.Name = "txt6";
            this.txt6.ShortcutsEnabled = false;
            this.txt6.Size = new System.Drawing.Size(211, 44);
            this.txt6.TabIndex = 6;
            // 
            // txt7
            // 
            this.txt7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt7.BackColor = System.Drawing.SystemColors.Window;
            this.txt7.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt7.Location = new System.Drawing.Point(766, 650);
            this.txt7.MaxLength = 10;
            this.txt7.Name = "txt7";
            this.txt7.ShortcutsEnabled = false;
            this.txt7.Size = new System.Drawing.Size(211, 44);
            this.txt7.TabIndex = 7;
            // 
            // txt8
            // 
            this.txt8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt8.BackColor = System.Drawing.SystemColors.Window;
            this.txt8.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8.Location = new System.Drawing.Point(766, 700);
            this.txt8.MaxLength = 10;
            this.txt8.Name = "txt8";
            this.txt8.ShortcutsEnabled = false;
            this.txt8.Size = new System.Drawing.Size(211, 44);
            this.txt8.TabIndex = 8;
            // 
            // txt9
            // 
            this.txt9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt9.BackColor = System.Drawing.SystemColors.Window;
            this.txt9.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt9.Location = new System.Drawing.Point(1086, 550);
            this.txt9.MaxLength = 10;
            this.txt9.Name = "txt9";
            this.txt9.ShortcutsEnabled = false;
            this.txt9.Size = new System.Drawing.Size(211, 44);
            this.txt9.TabIndex = 9;
            // 
            // txt10
            // 
            this.txt10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt10.BackColor = System.Drawing.SystemColors.Window;
            this.txt10.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt10.Location = new System.Drawing.Point(1086, 600);
            this.txt10.MaxLength = 10;
            this.txt10.Name = "txt10";
            this.txt10.ShortcutsEnabled = false;
            this.txt10.Size = new System.Drawing.Size(211, 44);
            this.txt10.TabIndex = 10;
            // 
            // txt11
            // 
            this.txt11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt11.BackColor = System.Drawing.SystemColors.Window;
            this.txt11.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt11.Location = new System.Drawing.Point(1086, 650);
            this.txt11.MaxLength = 10;
            this.txt11.Name = "txt11";
            this.txt11.ShortcutsEnabled = false;
            this.txt11.Size = new System.Drawing.Size(211, 44);
            this.txt11.TabIndex = 11;
            // 
            // txt12
            // 
            this.txt12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt12.BackColor = System.Drawing.SystemColors.Window;
            this.txt12.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12.Location = new System.Drawing.Point(1086, 699);
            this.txt12.MaxLength = 10;
            this.txt12.Name = "txt12";
            this.txt12.ShortcutsEnabled = false;
            this.txt12.Size = new System.Drawing.Size(211, 44);
            this.txt12.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(743, 609);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 24);
            this.label1.TabIndex = 127;
            this.label1.Text = "2";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(1063, 709);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 24);
            this.label6.TabIndex = 131;
            this.label6.Text = "8";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(1063, 659);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 24);
            this.label7.TabIndex = 132;
            this.label7.Text = "7";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(1063, 610);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(20, 24);
            this.label8.TabIndex = 133;
            this.label8.Text = "6";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(1063, 558);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(20, 24);
            this.label9.TabIndex = 134;
            this.label9.Text = "5";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(743, 658);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(20, 24);
            this.label10.TabIndex = 135;
            this.label10.Text = "3";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(743, 708);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 24);
            this.label11.TabIndex = 136;
            this.label11.Text = "4";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Blue;
            this.label20.Location = new System.Drawing.Point(302, 260);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(18, 20);
            this.label20.TabIndex = 92;
            this.label20.Text = "8";
            // 
            // txtl48
            // 
            this.txtl48.BackColor = System.Drawing.SystemColors.Window;
            this.txtl48.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl48.Location = new System.Drawing.Point(324, 257);
            this.txtl48.MaxLength = 3;
            this.txtl48.Name = "txtl48";
            this.txtl48.ReadOnly = true;
            this.txtl48.ShortcutsEnabled = false;
            this.txtl48.Size = new System.Drawing.Size(52, 26);
            this.txtl48.TabIndex = 91;
            this.txtl48.TabStop = false;
            this.txtl48.Text = "543";
            this.txtl48.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(302, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 20);
            this.label3.TabIndex = 90;
            this.label3.Text = "5";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(302, 204);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 20);
            this.label13.TabIndex = 89;
            this.label13.Text = "6";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(302, 233);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(18, 20);
            this.label15.TabIndex = 88;
            this.label15.Text = "7";
            // 
            // txtl45
            // 
            this.txtl45.BackColor = System.Drawing.SystemColors.Window;
            this.txtl45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl45.Location = new System.Drawing.Point(324, 173);
            this.txtl45.MaxLength = 3;
            this.txtl45.Name = "txtl45";
            this.txtl45.ReadOnly = true;
            this.txtl45.ShortcutsEnabled = false;
            this.txtl45.Size = new System.Drawing.Size(52, 26);
            this.txtl45.TabIndex = 86;
            this.txtl45.TabStop = false;
            this.txtl45.Text = "513";
            this.txtl45.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtl47
            // 
            this.txtl47.BackColor = System.Drawing.SystemColors.Window;
            this.txtl47.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl47.Location = new System.Drawing.Point(324, 229);
            this.txtl47.MaxLength = 3;
            this.txtl47.Name = "txtl47";
            this.txtl47.ReadOnly = true;
            this.txtl47.ShortcutsEnabled = false;
            this.txtl47.Size = new System.Drawing.Size(52, 26);
            this.txtl47.TabIndex = 87;
            this.txtl47.TabStop = false;
            this.txtl47.Text = "533";
            this.txtl47.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtl46
            // 
            this.txtl46.BackColor = System.Drawing.SystemColors.Window;
            this.txtl46.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl46.Location = new System.Drawing.Point(324, 201);
            this.txtl46.MaxLength = 3;
            this.txtl46.Name = "txtl46";
            this.txtl46.ReadOnly = true;
            this.txtl46.ShortcutsEnabled = false;
            this.txtl46.Size = new System.Drawing.Size(52, 26);
            this.txtl46.TabIndex = 85;
            this.txtl46.TabStop = false;
            this.txtl46.Text = "523";
            this.txtl46.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtl42
            // 
            this.txtl42.BackColor = System.Drawing.SystemColors.Window;
            this.txtl42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl42.Location = new System.Drawing.Point(323, 89);
            this.txtl42.MaxLength = 3;
            this.txtl42.Name = "txtl42";
            this.txtl42.ReadOnly = true;
            this.txtl42.ShortcutsEnabled = false;
            this.txtl42.Size = new System.Drawing.Size(52, 26);
            this.txtl42.TabIndex = 76;
            this.txtl42.TabStop = false;
            this.txtl42.Text = "452";
            this.txtl42.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(302, 148);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(18, 20);
            this.label16.TabIndex = 84;
            this.label16.Text = "4";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Blue;
            this.label17.Location = new System.Drawing.Point(302, 120);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(18, 20);
            this.label17.TabIndex = 83;
            this.label17.Text = "3";
            // 
            // txtl41
            // 
            this.txtl41.BackColor = System.Drawing.SystemColors.Window;
            this.txtl41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl41.Location = new System.Drawing.Point(323, 61);
            this.txtl41.MaxLength = 2;
            this.txtl41.Name = "txtl41";
            this.txtl41.ReadOnly = true;
            this.txtl41.ShortcutsEnabled = false;
            this.txtl41.Size = new System.Drawing.Size(52, 26);
            this.txtl41.TabIndex = 75;
            this.txtl41.TabStop = false;
            this.txtl41.Text = "451";
            this.txtl41.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(300, 63);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(18, 20);
            this.label18.TabIndex = 81;
            this.label18.Text = "1";
            // 
            // txtl44
            // 
            this.txtl44.BackColor = System.Drawing.SystemColors.Window;
            this.txtl44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl44.Location = new System.Drawing.Point(323, 145);
            this.txtl44.MaxLength = 3;
            this.txtl44.Name = "txtl44";
            this.txtl44.ReadOnly = true;
            this.txtl44.ShortcutsEnabled = false;
            this.txtl44.Size = new System.Drawing.Size(52, 26);
            this.txtl44.TabIndex = 77;
            this.txtl44.TabStop = false;
            this.txtl44.Text = "454";
            this.txtl44.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(301, 91);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(18, 20);
            this.label19.TabIndex = 82;
            this.label19.Text = "2";
            // 
            // txtl43
            // 
            this.txtl43.BackColor = System.Drawing.SystemColors.Window;
            this.txtl43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl43.Location = new System.Drawing.Point(323, 117);
            this.txtl43.MaxLength = 3;
            this.txtl43.Name = "txtl43";
            this.txtl43.ReadOnly = true;
            this.txtl43.ShortcutsEnabled = false;
            this.txtl43.Size = new System.Drawing.Size(52, 26);
            this.txtl43.TabIndex = 74;
            this.txtl43.TabStop = false;
            this.txtl43.Text = "453";
            this.txtl43.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(156, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 20);
            this.label4.TabIndex = 90;
            this.label4.Text = "5";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(156, 231);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 20);
            this.label5.TabIndex = 89;
            this.label5.Text = "6";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(155, 257);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(18, 20);
            this.label12.TabIndex = 88;
            this.label12.Text = "7";
            // 
            // txtl35
            // 
            this.txtl35.BackColor = System.Drawing.SystemColors.Window;
            this.txtl35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl35.Location = new System.Drawing.Point(180, 199);
            this.txtl35.MaxLength = 3;
            this.txtl35.Name = "txtl35";
            this.txtl35.ReadOnly = true;
            this.txtl35.ShortcutsEnabled = false;
            this.txtl35.Size = new System.Drawing.Size(52, 26);
            this.txtl35.TabIndex = 86;
            this.txtl35.TabStop = false;
            this.txtl35.Text = "522";
            this.txtl35.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtl37
            // 
            this.txtl37.BackColor = System.Drawing.SystemColors.Window;
            this.txtl37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl37.Location = new System.Drawing.Point(180, 254);
            this.txtl37.MaxLength = 3;
            this.txtl37.Name = "txtl37";
            this.txtl37.ReadOnly = true;
            this.txtl37.ShortcutsEnabled = false;
            this.txtl37.Size = new System.Drawing.Size(52, 26);
            this.txtl37.TabIndex = 87;
            this.txtl37.TabStop = false;
            this.txtl37.Text = "542";
            this.txtl37.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtl36
            // 
            this.txtl36.BackColor = System.Drawing.SystemColors.Window;
            this.txtl36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl36.Location = new System.Drawing.Point(180, 226);
            this.txtl36.MaxLength = 3;
            this.txtl36.Name = "txtl36";
            this.txtl36.ReadOnly = true;
            this.txtl36.ShortcutsEnabled = false;
            this.txtl36.Size = new System.Drawing.Size(52, 26);
            this.txtl36.TabIndex = 85;
            this.txtl36.TabStop = false;
            this.txtl36.Text = "532";
            this.txtl36.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtl32
            // 
            this.txtl32.BackColor = System.Drawing.SystemColors.Window;
            this.txtl32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl32.Location = new System.Drawing.Point(180, 94);
            this.txtl32.MaxLength = 3;
            this.txtl32.Name = "txtl32";
            this.txtl32.ReadOnly = true;
            this.txtl32.ShortcutsEnabled = false;
            this.txtl32.Size = new System.Drawing.Size(52, 26);
            this.txtl32.TabIndex = 76;
            this.txtl32.TabStop = false;
            this.txtl32.Text = "422";
            this.txtl32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(156, 177);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(18, 20);
            this.label14.TabIndex = 84;
            this.label14.Text = "4";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Blue;
            this.label21.Location = new System.Drawing.Point(156, 122);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(18, 20);
            this.label21.TabIndex = 83;
            this.label21.Text = "3";
            // 
            // txtl31
            // 
            this.txtl31.BackColor = System.Drawing.SystemColors.Window;
            this.txtl31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl31.Location = new System.Drawing.Point(180, 66);
            this.txtl31.MaxLength = 2;
            this.txtl31.Name = "txtl31";
            this.txtl31.ReadOnly = true;
            this.txtl31.ShortcutsEnabled = false;
            this.txtl31.Size = new System.Drawing.Size(52, 26);
            this.txtl31.TabIndex = 75;
            this.txtl31.TabStop = false;
            this.txtl31.Text = "412";
            this.txtl31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Blue;
            this.label22.Location = new System.Drawing.Point(157, 68);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(18, 20);
            this.label22.TabIndex = 81;
            this.label22.Text = "1";
            // 
            // txtl34
            // 
            this.txtl34.BackColor = System.Drawing.SystemColors.Window;
            this.txtl34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl34.Location = new System.Drawing.Point(179, 171);
            this.txtl34.MaxLength = 3;
            this.txtl34.Name = "txtl34";
            this.txtl34.ReadOnly = true;
            this.txtl34.ShortcutsEnabled = false;
            this.txtl34.Size = new System.Drawing.Size(52, 26);
            this.txtl34.TabIndex = 77;
            this.txtl34.TabStop = false;
            this.txtl34.Text = "512";
            this.txtl34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Blue;
            this.label23.Location = new System.Drawing.Point(157, 95);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(18, 20);
            this.label23.TabIndex = 82;
            this.label23.Text = "2";
            // 
            // txtl33
            // 
            this.txtl33.BackColor = System.Drawing.SystemColors.Window;
            this.txtl33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl33.Location = new System.Drawing.Point(180, 122);
            this.txtl33.MaxLength = 3;
            this.txtl33.Name = "txtl33";
            this.txtl33.ReadOnly = true;
            this.txtl33.ShortcutsEnabled = false;
            this.txtl33.Size = new System.Drawing.Size(52, 26);
            this.txtl33.TabIndex = 74;
            this.txtl33.TabStop = false;
            this.txtl33.Text = "432";
            this.txtl33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt113
            // 
            this.txt113.BackColor = System.Drawing.SystemColors.Window;
            this.txt113.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt113.Location = new System.Drawing.Point(134, 11);
            this.txt113.MaxLength = 3;
            this.txt113.Name = "txt113";
            this.txt113.ReadOnly = true;
            this.txt113.ShortcutsEnabled = false;
            this.txt113.Size = new System.Drawing.Size(39, 35);
            this.txt113.TabIndex = 76;
            this.txt113.TabStop = false;
            this.txt113.Text = "3";
            this.txt113.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt112
            // 
            this.txt112.BackColor = System.Drawing.SystemColors.Window;
            this.txt112.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt112.Location = new System.Drawing.Point(61, 12);
            this.txt112.MaxLength = 2;
            this.txt112.Name = "txt112";
            this.txt112.ReadOnly = true;
            this.txt112.ShortcutsEnabled = false;
            this.txt112.Size = new System.Drawing.Size(39, 35);
            this.txt112.TabIndex = 75;
            this.txt112.TabStop = false;
            this.txt112.Text = "2";
            this.txt112.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt114
            // 
            this.txt114.BackColor = System.Drawing.SystemColors.Window;
            this.txt114.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt114.Location = new System.Drawing.Point(186, 12);
            this.txt114.MaxLength = 3;
            this.txt114.Name = "txt114";
            this.txt114.ReadOnly = true;
            this.txt114.ShortcutsEnabled = false;
            this.txt114.Size = new System.Drawing.Size(39, 35);
            this.txt114.TabIndex = 77;
            this.txt114.TabStop = false;
            this.txt114.Text = "4";
            this.txt114.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt115
            // 
            this.txt115.BackColor = System.Drawing.SystemColors.Window;
            this.txt115.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt115.Location = new System.Drawing.Point(240, 12);
            this.txt115.MaxLength = 2;
            this.txt115.Name = "txt115";
            this.txt115.ReadOnly = true;
            this.txt115.ShortcutsEnabled = false;
            this.txt115.Size = new System.Drawing.Size(39, 35);
            this.txt115.TabIndex = 78;
            this.txt115.TabStop = false;
            this.txt115.Text = "5";
            this.txt115.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt116
            // 
            this.txt116.BackColor = System.Drawing.SystemColors.Window;
            this.txt116.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt116.Location = new System.Drawing.Point(292, 11);
            this.txt116.MaxLength = 2;
            this.txt116.Name = "txt116";
            this.txt116.ReadOnly = true;
            this.txt116.ShortcutsEnabled = false;
            this.txt116.Size = new System.Drawing.Size(39, 35);
            this.txt116.TabIndex = 79;
            this.txt116.TabStop = false;
            this.txt116.Text = "6";
            this.txt116.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt111
            // 
            this.txt111.BackColor = System.Drawing.SystemColors.Window;
            this.txt111.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt111.Location = new System.Drawing.Point(11, 12);
            this.txt111.MaxLength = 3;
            this.txt111.Name = "txt111";
            this.txt111.ReadOnly = true;
            this.txt111.ShortcutsEnabled = false;
            this.txt111.Size = new System.Drawing.Size(39, 35);
            this.txt111.TabIndex = 1;
            this.txt111.TabStop = false;
            this.txt111.Text = "1";
            this.txt111.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt117
            // 
            this.txt117.BackColor = System.Drawing.SystemColors.Window;
            this.txt117.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt117.Location = new System.Drawing.Point(349, 11);
            this.txt117.MaxLength = 3;
            this.txt117.Name = "txt117";
            this.txt117.ReadOnly = true;
            this.txt117.ShortcutsEnabled = false;
            this.txt117.Size = new System.Drawing.Size(39, 35);
            this.txt117.TabIndex = 80;
            this.txt117.TabStop = false;
            this.txt117.Text = "7";
            this.txt117.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtl22
            // 
            this.txtl22.BackColor = System.Drawing.SystemColors.Window;
            this.txtl22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl22.Location = new System.Drawing.Point(52, 95);
            this.txtl22.MaxLength = 3;
            this.txtl22.Name = "txtl22";
            this.txtl22.ReadOnly = true;
            this.txtl22.ShortcutsEnabled = false;
            this.txtl22.Size = new System.Drawing.Size(52, 26);
            this.txtl22.TabIndex = 76;
            this.txtl22.TabStop = false;
            this.txtl22.Text = "102";
            this.txtl22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Blue;
            this.label24.Location = new System.Drawing.Point(21, 175);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(18, 20);
            this.label24.TabIndex = 84;
            this.label24.Text = "4";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Blue;
            this.label25.Location = new System.Drawing.Point(21, 125);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(18, 20);
            this.label25.TabIndex = 83;
            this.label25.Text = "3";
            // 
            // txtl21
            // 
            this.txtl21.BackColor = System.Drawing.SystemColors.Window;
            this.txtl21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl21.Location = new System.Drawing.Point(52, 68);
            this.txtl21.MaxLength = 2;
            this.txtl21.Name = "txtl21";
            this.txtl21.ReadOnly = true;
            this.txtl21.ShortcutsEnabled = false;
            this.txtl21.Size = new System.Drawing.Size(52, 26);
            this.txtl21.TabIndex = 75;
            this.txtl21.TabStop = false;
            this.txtl21.Text = "101";
            this.txtl21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Blue;
            this.label26.Location = new System.Drawing.Point(21, 71);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(18, 20);
            this.label26.TabIndex = 81;
            this.label26.Text = "1";
            // 
            // txtl24
            // 
            this.txtl24.BackColor = System.Drawing.SystemColors.Window;
            this.txtl24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl24.Location = new System.Drawing.Point(52, 172);
            this.txtl24.MaxLength = 3;
            this.txtl24.Name = "txtl24";
            this.txtl24.ReadOnly = true;
            this.txtl24.ShortcutsEnabled = false;
            this.txtl24.Size = new System.Drawing.Size(52, 26);
            this.txtl24.TabIndex = 77;
            this.txtl24.TabStop = false;
            this.txtl24.Text = "203";
            this.txtl24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Blue;
            this.label27.Location = new System.Drawing.Point(21, 95);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(18, 20);
            this.label27.TabIndex = 82;
            this.label27.Text = "2";
            // 
            // txtl23
            // 
            this.txtl23.BackColor = System.Drawing.SystemColors.Window;
            this.txtl23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl23.Location = new System.Drawing.Point(52, 123);
            this.txtl23.MaxLength = 3;
            this.txtl23.Name = "txtl23";
            this.txtl23.ReadOnly = true;
            this.txtl23.ShortcutsEnabled = false;
            this.txtl23.Size = new System.Drawing.Size(52, 26);
            this.txtl23.TabIndex = 74;
            this.txtl23.TabStop = false;
            this.txtl23.Text = "103";
            this.txtl23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt1
            // 
            this.txt1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txt1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txt1.BackColor = System.Drawing.SystemColors.Window;
            this.txt1.Font = new System.Drawing.Font("Times New Roman", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt1.Location = new System.Drawing.Point(4, 6);
            this.txt1.MaxLength = 8;
            this.txt1.Multiline = true;
            this.txt1.Name = "txt1";
            this.txt1.ReadOnly = true;
            this.txt1.Size = new System.Drawing.Size(399, 47);
            this.txt1.TabIndex = 1;
            this.txt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt4
            // 
            this.txt4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txt4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txt4.BackColor = System.Drawing.SystemColors.Window;
            this.txt4.Font = new System.Drawing.Font("Times New Roman", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt4.Location = new System.Drawing.Point(277, 60);
            this.txt4.MaxLength = 8;
            this.txt4.Multiline = true;
            this.txt4.Name = "txt4";
            this.txt4.ReadOnly = true;
            this.txt4.Size = new System.Drawing.Size(126, 225);
            this.txt4.TabIndex = 4;
            this.txt4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt3
            // 
            this.txt3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txt3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txt3.BackColor = System.Drawing.SystemColors.Window;
            this.txt3.Font = new System.Drawing.Font("Times New Roman", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3.Location = new System.Drawing.Point(131, 59);
            this.txt3.MaxLength = 8;
            this.txt3.Multiline = true;
            this.txt3.Name = "txt3";
            this.txt3.ReadOnly = true;
            this.txt3.Size = new System.Drawing.Size(126, 226);
            this.txt3.TabIndex = 3;
            this.txt3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt2
            // 
            this.txt2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txt2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txt2.BackColor = System.Drawing.SystemColors.Window;
            this.txt2.Font = new System.Drawing.Font("Times New Roman", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt2.Location = new System.Drawing.Point(4, 59);
            this.txt2.MaxLength = 8;
            this.txt2.Multiline = true;
            this.txt2.Name = "txt2";
            this.txt2.ReadOnly = true;
            this.txt2.Size = new System.Drawing.Size(113, 226);
            this.txt2.TabIndex = 2;
            this.txt2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // imageViewerTR1
            // 
            this.imageViewerTR1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.imageViewerTR1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imageViewerTR1.CurrentZoom = 1F;
            this.imageViewerTR1.Image = null;
            this.imageViewerTR1.Location = new System.Drawing.Point(24, 32);
            this.imageViewerTR1.MaxZoom = 20F;
            this.imageViewerTR1.MinZoom = 0.05F;
            this.imageViewerTR1.Name = "imageViewerTR1";
            this.imageViewerTR1.Size = new System.Drawing.Size(549, 449);
            this.imageViewerTR1.TabIndex = 137;
            this.imageViewerTR1.TabStop = false;
            // 
            // txtl25
            // 
            this.txtl25.BackColor = System.Drawing.SystemColors.Window;
            this.txtl25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl25.Location = new System.Drawing.Point(52, 199);
            this.txtl25.MaxLength = 3;
            this.txtl25.Name = "txtl25";
            this.txtl25.ReadOnly = true;
            this.txtl25.ShortcutsEnabled = false;
            this.txtl25.Size = new System.Drawing.Size(52, 26);
            this.txtl25.TabIndex = 139;
            this.txtl25.TabStop = false;
            this.txtl25.Text = "202";
            this.txtl25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Blue;
            this.label28.Location = new System.Drawing.Point(21, 256);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(18, 20);
            this.label28.TabIndex = 143;
            this.label28.Text = "7";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Blue;
            this.label29.Location = new System.Drawing.Point(21, 229);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(18, 20);
            this.label29.TabIndex = 142;
            this.label29.Text = "6";
            // 
            // txtl27
            // 
            this.txtl27.BackColor = System.Drawing.SystemColors.Window;
            this.txtl27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl27.Location = new System.Drawing.Point(52, 254);
            this.txtl27.MaxLength = 3;
            this.txtl27.Name = "txtl27";
            this.txtl27.ReadOnly = true;
            this.txtl27.ShortcutsEnabled = false;
            this.txtl27.Size = new System.Drawing.Size(52, 26);
            this.txtl27.TabIndex = 140;
            this.txtl27.TabStop = false;
            this.txtl27.Text = "301";
            this.txtl27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Blue;
            this.label30.Location = new System.Drawing.Point(21, 202);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(18, 20);
            this.label30.TabIndex = 141;
            this.label30.Text = "5";
            // 
            // txtl26
            // 
            this.txtl26.BackColor = System.Drawing.SystemColors.Window;
            this.txtl26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtl26.Location = new System.Drawing.Point(52, 226);
            this.txtl26.MaxLength = 3;
            this.txtl26.Name = "txtl26";
            this.txtl26.ReadOnly = true;
            this.txtl26.ShortcutsEnabled = false;
            this.txtl26.Size = new System.Drawing.Size(52, 26);
            this.txtl26.TabIndex = 138;
            this.txtl26.TabStop = false;
            this.txtl26.Text = "201";
            this.txtl26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // imageViewerTR2
            // 
            this.imageViewerTR2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.imageViewerTR2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imageViewerTR2.CurrentZoom = 1F;
            this.imageViewerTR2.Image = null;
            this.imageViewerTR2.Location = new System.Drawing.Point(711, 32);
            this.imageViewerTR2.MaxZoom = 20F;
            this.imageViewerTR2.MinZoom = 0.05F;
            this.imageViewerTR2.Name = "imageViewerTR2";
            this.imageViewerTR2.Size = new System.Drawing.Size(614, 512);
            this.imageViewerTR2.TabIndex = 144;
            this.imageViewerTR2.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.txtl25);
            this.panel1.Controls.Add(this.txtl48);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txt111);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.txt117);
            this.panel1.Controls.Add(this.txtl45);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.txtl47);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtl46);
            this.panel1.Controls.Add(this.txt116);
            this.panel1.Controls.Add(this.txtl42);
            this.panel1.Controls.Add(this.txtl27);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.txt115);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtl41);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.txt114);
            this.panel1.Controls.Add(this.txtl44);
            this.panel1.Controls.Add(this.txtl26);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.txtl35);
            this.panel1.Controls.Add(this.txtl43);
            this.panel1.Controls.Add(this.txt112);
            this.panel1.Controls.Add(this.txt4);
            this.panel1.Controls.Add(this.txt113);
            this.panel1.Controls.Add(this.txt1);
            this.panel1.Controls.Add(this.txtl37);
            this.panel1.Controls.Add(this.txtl23);
            this.panel1.Controls.Add(this.txtl22);
            this.panel1.Controls.Add(this.txtl36);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.txtl24);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.txtl32);
            this.panel1.Controls.Add(this.txtl21);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txt2);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.txtl33);
            this.panel1.Controls.Add(this.txtl31);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.txtl34);
            this.panel1.Controls.Add(this.txt3);
            this.panel1.Location = new System.Drawing.Point(120, 477);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(409, 292);
            this.panel1.TabIndex = 145;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel2.Controls.Add(this.label38);
            this.panel2.Controls.Add(this.label37);
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.label35);
            this.panel2.Controls.Add(this.label34);
            this.panel2.Controls.Add(this.label33);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.label31);
            this.panel2.Location = new System.Drawing.Point(652, 32);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(59, 512);
            this.panel2.TabIndex = 146;
            // 
            // label38
            // 
            this.label38.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Blue;
            this.label38.Location = new System.Drawing.Point(20, 447);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(36, 37);
            this.label38.TabIndex = 114;
            this.label38.Text = "8";
            // 
            // label37
            // 
            this.label37.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Blue;
            this.label37.Location = new System.Drawing.Point(18, 390);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(36, 37);
            this.label37.TabIndex = 113;
            this.label37.Text = "7";
            // 
            // label36
            // 
            this.label36.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Blue;
            this.label36.Location = new System.Drawing.Point(18, 335);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(36, 37);
            this.label36.TabIndex = 112;
            this.label36.Text = "6";
            // 
            // label35
            // 
            this.label35.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Blue;
            this.label35.Location = new System.Drawing.Point(18, 156);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(36, 37);
            this.label35.TabIndex = 111;
            this.label35.Text = "3";
            // 
            // label34
            // 
            this.label34.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Blue;
            this.label34.Location = new System.Drawing.Point(18, 100);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(36, 37);
            this.label34.TabIndex = 110;
            this.label34.Text = "2";
            // 
            // label33
            // 
            this.label33.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Blue;
            this.label33.Location = new System.Drawing.Point(18, 276);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(36, 37);
            this.label33.TabIndex = 109;
            this.label33.Text = "5";
            // 
            // label32
            // 
            this.label32.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Blue;
            this.label32.Location = new System.Drawing.Point(18, 217);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(36, 37);
            this.label32.TabIndex = 108;
            this.label32.Text = "4";
            // 
            // label31
            // 
            this.label31.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Blue;
            this.label31.Location = new System.Drawing.Point(20, 42);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(34, 37);
            this.label31.TabIndex = 107;
            this.label31.Text = "1";
            // 
            // FrmEntryTemp4_1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.ClientSize = new System.Drawing.Size(1350, 730);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.imageViewerTR2);
            this.Controls.Add(this.imageViewerTR1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt12);
            this.Controls.Add(this.txt11);
            this.Controls.Add(this.txt10);
            this.Controls.Add(this.txt9);
            this.Controls.Add(this.txt8);
            this.Controls.Add(this.txt7);
            this.Controls.Add(this.txt6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.lblField1);
            this.Controls.Add(this.lblPressSubmit);
            this.Controls.Add(this.txt5);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.btnNotgood);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEntryTemp4_1";
            this.Text = "VCB - Entry ";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEntry_FormClosing);
            this.Load += new System.EventHandler(this.frmEntry_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEntry_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmEntry_KeyPress);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public static System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Timer timerEntry;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Button btnExit;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label lblField1;
        private System.Windows.Forms.Button btnNotgood;
        internal System.Windows.Forms.Label lblPressSubmit;
        private System.Windows.Forms.TextBox txt5;
        private System.Windows.Forms.TextBox txt6;
        private System.Windows.Forms.TextBox txt7;
        private System.Windows.Forms.TextBox txt8;
        private System.Windows.Forms.TextBox txt9;
        private System.Windows.Forms.TextBox txt10;
        private System.Windows.Forms.TextBox txt11;
        private System.Windows.Forms.TextBox txt12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtl48;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtl45;
        private System.Windows.Forms.TextBox txtl47;
        private System.Windows.Forms.TextBox txtl46;
        private System.Windows.Forms.TextBox txtl42;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtl41;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtl44;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtl43;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtl35;
        private System.Windows.Forms.TextBox txtl37;
        private System.Windows.Forms.TextBox txtl36;
        private System.Windows.Forms.TextBox txtl32;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtl31;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtl34;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtl33;
        private System.Windows.Forms.TextBox txt113;
        private System.Windows.Forms.TextBox txt112;
        private System.Windows.Forms.TextBox txt114;
        private System.Windows.Forms.TextBox txt115;
        private System.Windows.Forms.TextBox txt116;
        private System.Windows.Forms.TextBox txt111;
        private System.Windows.Forms.TextBox txt117;
        private System.Windows.Forms.TextBox txtl22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtl21;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtl24;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtl23;
        private System.Windows.Forms.TextBox txt1;
        private System.Windows.Forms.TextBox txt4;
        private System.Windows.Forms.TextBox txt3;
        private System.Windows.Forms.TextBox txt2;
        private System.Windows.Forms.ToolStripTextBox lblpage;
        private System.Windows.Forms.ToolStripTextBox lblpagename;
        private ImageViewerTR.ImageViewerTR imageViewerTR1;
        private System.Windows.Forms.ToolStripTextBox txtInf;
        private System.Windows.Forms.TextBox txtl25;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtl27;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtl26;
        private ImageViewerTR.ImageViewerTR imageViewerTR2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
    }
}