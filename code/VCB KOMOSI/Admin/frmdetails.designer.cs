﻿namespace VCB_KOMOSI
{
    partial class frmdetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmdetails));
            this.grddetails = new DevExpress.XtraGrid.GridControl();
            this.grvdetails = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.grddetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvdetails)).BeginInit();
            this.SuspendLayout();
            // 
            // grddetails
            // 
            this.grddetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grddetails.Location = new System.Drawing.Point(0, 0);
            this.grddetails.MainView = this.grvdetails;
            this.grddetails.Name = "grddetails";
            this.grddetails.Size = new System.Drawing.Size(953, 474);
            this.grddetails.TabIndex = 2;
            this.grddetails.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvdetails});
            // 
            // grvdetails
            // 
            this.grvdetails.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grvdetails.Appearance.HeaderPanel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvdetails.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.grvdetails.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.grvdetails.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvdetails.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.grvdetails.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvdetails.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvdetails.GridControl = this.grddetails;
            this.grvdetails.IndicatorWidth = 30;
            this.grvdetails.Name = "grvdetails";
            this.grvdetails.OptionsBehavior.Editable = false;
            this.grvdetails.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grvdetails.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvdetails.OptionsView.ShowFooter = true;
            this.grvdetails.OptionsView.ShowGroupPanel = false;
            // 
            // frmdetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(953, 474);
            this.Controls.Add(this.grddetails);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmdetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Details";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmdetails_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmdetails_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.grddetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvdetails)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grddetails;
        private DevExpress.XtraGrid.Views.Grid.GridView grvdetails;
    }
}