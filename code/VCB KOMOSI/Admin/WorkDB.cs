﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace VCB_KOMOSI
{
    class WorkDB_Admin
    {
        //Chuoi ket noi co sdl
        private string stringconnecttion = String.Format("Data Source=" + Program.server + ";Initial Catalog=VCB_KOMOSI_2017;User Id=entrykomosi;Password=entrykomosi;Integrated Security=no;MultipleActiveResultSets=True;Packet Size=4096;Pooling=false;");

        #region Entry_date
        //Lấy User đã nhập
        public DataTable GetUserID_Date(string startdate, string group)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "SelectUser";
                    sqlCommand.Parameters.Add("@GROUP", SqlDbType.NVarChar, 30).Value = group;
                    sqlCommand.Parameters.Add("@startdate", SqlDbType.NVarChar, 10).Value = startdate;
                    SqlDataAdapter daUser = new SqlDataAdapter();
                    daUser.SelectCommand = sqlCommand;
                    con.Open();
                    daUser.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy Entry\n" + sqlException.Message);
            }
            return dt;
        }

        //Tổng ký tự nhập của từng user1
        public double Tongkytu1_Date(string MSNV, string startdate, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select  SUM(Tongkytu1) as 'tt' From DB_owner.[Imagecontent] left join db_owner.Image on Image.Id=ImageId join db_owner.Page on page.Id=PageId join dbo.Batch on Batch.Id=BatchId JOIN db_owner.[User] on userid1=[User].id Where LEFT(CONVERT(VARCHAR,InputDate1, 112), 10) ='" + startdate + "' and [MSNV]='" + MSNV + "' and [Lvl]=" + lvl + " and Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {

                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }
                //return tkt;
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng ký tự nhập của từng user2
        public double Tongkytu2_Date(string MSNV, string startdate, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select  SUM(Tongkytu2) as 'tt' From DB_owner.[Imagecontent] left join db_owner.Image on Image.Id=ImageId join db_owner.Page on page.Id=PageId join dbo.Batch on Batch.Id=BatchId JOIN db_owner.[User] on userid2=[User].id Where LEFT(CONVERT(VARCHAR,InputDate2, 112), 10) ='" + startdate + "' and [MSNV]='" + MSNV + "' and [Lvl]=" + lvl + " and Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {

                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng truong của từng user1
        public double TongTruong1_Date(string MSNV, string startdate, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select  Count(1) as 'tt' From DB_owner.[Imagecontent] left  join db_owner.Image on Image.Id=ImageId join db_owner.Page on page.Id=PageId join dbo.Batch on Batch.Id=BatchId JOIN db_owner.[User] on userid1=[User].id Where LEFT(CONVERT(VARCHAR,InputDate1, 112), 10) ='" + startdate + "' and [MSNV]='" + MSNV + "' and [Lvl]=" + lvl + " and Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {

                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }
                //return tkt;
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng truong của từng user2
        public double TongTruong2_Date(string MSNV, string startdate, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select  Count(1) as 'tt' From DB_owner.[Imagecontent] Left join db_owner.Image on Image.Id=ImageId join db_owner.Page on page.Id=PageId join dbo.Batch on Batch.Id=BatchId JOIN db_owner.[User] on userid2=[User].id Where LEFT(CONVERT(VARCHAR,InputDate2, 112), 10) ='" + startdate + "' and [MSNV]='" + MSNV + "' and [Lvl]=" + lvl + " and Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {

                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng truong NG của từng user1
        public double TongTruongNG1_Date(string MSNV, string startdate, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select  Count(1) as 'tt' From DB_owner.[Imagecontent] left join db_owner.Image on Image.Id=ImageId join db_owner.Page on page.Id=PageId join dbo.Batch on Batch.Id=BatchId Join db_owner.[User] on NG1=[User].id Where LEFT(CONVERT(VARCHAR,InputDate1, 112), 10) ='" + startdate + "' and [MSNV]='" + MSNV + "' and [Lvl]=" + lvl + " and Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {

                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }
                //return tkt;
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng truong của từng user2
        public double TongTruongNG2_Date(string MSNV, string startdate, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select  Count(1) as 'tt' From DB_owner.[Imagecontent] Left join db_owner.Image on Image.Id=ImageId join db_owner.Page on page.Id=PageId join dbo.Batch on Batch.Id=BatchId join db_owner.[User] on NG2=[User].id Where LEFT(CONVERT(VARCHAR,InputDate2, 112), 10) ='" + startdate + "' and [MSNV]='" + MSNV + "' and [Lvl]=" + lvl + " and Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {

                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng Loi sai của từng user1    
        public double Loisai1_Date(string MSNV, string startdate, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select  SUM(Loisai1) as 'tt' From DB_owner.[Imagecontent] Left join db_owner.Image on Image.Id=ImageId join db_owner.Page on page.Id=PageId join dbo.Batch on Batch.Id=BatchId JOIN db_owner.[User] on userid1=[User].id Where LEFT(CONVERT(VARCHAR,InputDate1, 112), 10) ='" + startdate + "' and [MSNV]='" + MSNV + "' and [Lvl]=" + lvl + " and Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {

                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy lỗi sai\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng Loi sai của từng user1    
        public double Loisai2_Date(string MSNV, string startdate, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select  SUM(Loisai2) as 'tt' From DB_owner.[Imagecontent] left join db_owner.Image on Image.Id=ImageId join db_owner.Page on page.Id=PageId join dbo.Batch on Batch.Id=BatchId JOIN db_owner.[User] on userid2=[User].id Where LEFT(CONVERT(VARCHAR,InputDate2, 112), 10) ='" + startdate + "' and [MSNV]='" + MSNV + "' and [Lvl]=" + lvl + " and Batch.Hitpoint =2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {

                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng ký tự nhập của từng user1
        public double Tongtime_Date(string MSNV, string startdate, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select  SUM(Timework) as 'tt' From Dbo.[Performance] Join Dbo.Batch on Batchid=Batch.id JOIN db_owner.[User] on Userid=[User].id Where  MSNV='" + MSNV + "' and Lvl=" + lvl + " and LEFT(CONVERT(VARCHAR,Starttime, 112), 10) ='" + startdate + "' and Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }
                //return tkt;
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        #endregion
        #region Entry_TemplateBatch
        //Lấy User đã nhập
        public DataTable getPerformaceEntry(string listBatch, string group, string lstemplate)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    if (group == "ALL")
                        sqlCommand.CommandText = @"select us1.Email as 'FullName',us1.Lvl as 'Level', us1.[Group] as N'Trung Tâm',Tongkytu1 as N'Tổng ký tự',Loisai1 as N'Lỗi sai',N'Tỉ lệ sai'=0.0, TimeMiliseconds1 as 'Thời gian',NG1 as 'NG',MSNV from db_owner.[User] as us1 join db_owner.ImageContent on us1.Id=ImageContent.UserId1 join db_owner.Image on Image.Id = ImageContent.ImageId where BatchName in (" + listBatch + ") AND Template in (" + lstemplate + ") " +
                                                  "union all (select us2.Email as 'FullName',us2.Lvl as 'Level', us2.[Group] as N'Trung Tâm',Tongkytu2 as N'Tổng ký tự',Loisai2 as N'Lỗi sai',N'Tỉ lệ sai'=0.0, TimeMiliseconds2 as 'Thời gian', NG2 as 'NG',MSNV from db_owner.[User] as us2 join db_owner.ImageContent on us2.Id=ImageContent.UserId2 join db_owner.Image on Image.Id = ImageContent.ImageId where BatchName in (" + listBatch + ") AND Template in (" + lstemplate + ")) " +
                                                  "union all (select us3.Email as 'FullName',us3.Lvl as 'Level', us3.[Group] as N'Trung Tâm',Tongkytu3 as N'Tổng ký tự',Loisai3 as N'Lỗi sai',N'Tỉ lệ sai'=0.0, TimeMiliseconds3 as 'Thời gian', NG3 as 'NG',MSNV from db_owner.[User] as us3 join db_owner.ImageContent on us3.Id=ImageContent.UserId3 join db_owner.Image on Image.Id = ImageContent.ImageId where BatchName in (" + listBatch + ") AND Template in (" + lstemplate + "))";
                    else
                        sqlCommand.CommandText = @"select us1.Email as 'FullName',us1.Lvl as 'Level', us1.[Group] as N'Trung Tâm',Tongkytu1 as N'Tổng ký tự',Loisai1 as N'Lỗi sai',N'Tỉ lệ sai'=0.0, TimeMiliseconds1 as 'Thời gian',NG1 as 'NG',MSNV from db_owner.[User] as us1 join db_owner.ImageContent on us1.Id=ImageContent.UserId1 join db_owner.Image on Image.Id = ImageContent.ImageId where BatchName in (" + listBatch + ") AND Template in (" + lstemplate + ") and us1.[Group]=N'" + group + "' " +
                                                  "union all (select us2.Email as 'FullName',us2.Lvl as 'Level', us2.[Group] as N'Trung Tâm',Tongkytu2 as N'Tổng ký tự',Loisai2 as N'Lỗi sai',N'Tỉ lệ sai'=0.0, TimeMiliseconds2 as 'Thời gian', NG2 as 'NG',MSNV from db_owner.[User] as us2 join db_owner.ImageContent on us2.Id=ImageContent.UserId2 join db_owner.Image on Image.Id = ImageContent.ImageId where BatchName in (" + listBatch + ") AND Template in (" + lstemplate + ") and us2.[Group]=N'" + group + "') " +
                                                  "union all (select us3.Email as 'FullName',us3.Lvl as 'Level', us3.[Group] as N'Trung Tâm',Tongkytu3 as N'Tổng ký tự',Loisai3 as N'Lỗi sai',N'Tỉ lệ sai'=0.0, TimeMiliseconds3 as 'Thời gian', NG3 as 'NG',MSNV from db_owner.[User] as us3 join db_owner.ImageContent on us3.Id=ImageContent.UserId3 join db_owner.Image on Image.Id = ImageContent.ImageId where BatchName in (" + listBatch + ") AND Template in (" + lstemplate + ") and us3.[Group]=N'" + group + "')";
                    con.Open();
                    SqlDataAdapter daUser = new SqlDataAdapter();
                    daUser.SelectCommand = sqlCommand;
                    daUser.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy Performance\n" + sqlException.Message);
            }
            return dt;
        }

        //Tổng ký tự nhập của từng user1
        public double Tongkytu1_TemplateBatch(string MSNV, string listBatch, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Select SUM(Tongkytu1) as 'tt' from db_owner.[User] as us1 join db_owner.ImageContent on us1.Id=ImageContent.UserId1 left join db_owner.Image on Image.Id=ImageContent.ImageId join dbo.Batch on Batch.Id=Image.IDBatch Where IDBatch in (" + listBatch + ") and [MSNV]='" + MSNV + "' and [Lvl]='" + lvl + "' AND Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {

                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }
                //return tkt;
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng ký tự nhập của từng user2
        public double Tongkytu2_TemplateBatch(string MSNV, string listBatch, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Select SUM(Tongkytu2) as 'tt' from db_owner.[User] as us2 join db_owner.ImageContent on us2.Id=ImageContent.UserId2 left join db_owner.Image on Image.Id=ImageContent.ImageId join dbo.Batch on Batch.Id=Image.IDBatch Where IDBatch in (" + listBatch + ") and [MSNV]='" + MSNV + "' and [Lvl]='" + lvl + "' AND Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {

                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng trường của từng user1
        public double TongTruong1_TemplateBatch(string MSNV, string listBatch, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Select  Count(ImageContent.Id) as 'tt' from db_owner.[User] as us1 join db_owner.ImageContent on us1.Id=ImageContent.UserId1 left join db_owner.Image on Image.Id=ImageContent.ImageId join dbo.Batch on Batch.Id=Image.IDBatch Where IDBatch in (" + listBatch + ") and [MSNV]='" + MSNV + "' and [Lvl]=" + lvl + " AND Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {

                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }
                //return tkt;
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng trường nhập của từng user2
        public double TongTruong2_TemplateBatch(string MSNV, string listBatch, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Select  Count(ImageContent.Id) as 'tt' from db_owner.[User] as us2 join db_owner.ImageContent on us2.Id=ImageContent.UserId2 left join db_owner.Image on Image.Id=ImageContent.ImageId join dbo.Batch on Batch.Id=Image.IDBatch Where IDBatch in (" + listBatch + ") and [MSNV]='" + MSNV + "' and [Lvl]=" + lvl + " AND Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {

                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng trường not good của từng user1
        public double TongTruongNG1_TemplateBatch(string MSNV, string listBatch, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "select COUNT(NG1) as 'tt' from db_owner.[User] as us1 join db_owner.ImageContent on us1.Id=ImageContent.UserId1 left join db_owner.Image on ImageContent.ImageId=Image.Id join dbo.Batch on Batch.Id=Image.IDBatch where IDBatch in (" + listBatch + ") and [MSNV]='" + MSNV + "' and [Lvl]=" + lvl + " AND Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }
                //return tkt;
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng trường not good nhập của từng user2
        public double TongTruongNG2_TemplateBatch(string MSNV, string listBatch, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "select COUNT(NG2) as 'tt' from db_owner.[User] as us2 join db_owner.ImageContent on us2.Id=ImageContent.UserId2 left join db_owner.Image on ImageContent.ImageId=Image.Id join dbo.Batch on Batch.Id=Image.IDBatch where IDBatch in (" + listBatch + ") and [MSNV]='" + MSNV + "' and [Lvl]=" + lvl + " AND Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {

                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng Loi sai của từng user1    
        public double Loisai1_TemplateBatch(string MSNV, string listBatch, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Select SUM(Loisai1) as 'tt' from db_owner.[User] as us1 join db_owner.ImageContent on us1.Id=ImageContent.UserId1 left join db_owner.Image on Image.Id=ImageContent.ImageId join dbo.Batch on Batch.Id=Image.IDBatch Where IDBatch in (" + listBatch + ") and [MSNV]='" + MSNV + "' and [Lvl]='" + lvl + "' AND Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {

                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy lỗi sai\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng Loi sai của từng user1    
        public double Loisai2_TemplateBatch(string MSNV, string listBatch, int lvl, int idtemplate)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Select SUM(Loisai2) as 'tt' from db_owner.[User] as us2 join db_owner.ImageContent on us2.Id=ImageContent.UserId2 left join db_owner.Image on Image.Id=ImageContent.ImageId join dbo.Batch on Batch.Id=Image.IDBatch Where IDBatch in (" + listBatch + ") and [MSNV]='" + MSNV + "' and [Lvl]='" + lvl + "' AND Batch.Hitpoint=2 Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        break;
                    }
                    sqlDataReader.Close();
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        //Tổng ký tự nhập của từng user1
        public double Tongtime_TemplateBatch1(string MSNV, string listBatch, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Select SUM(TimeMiliseconds1) as 'tt' from db_owner.[User] as us1 join db_owner.ImageContent on us1.Id=ImageContent.UserId1 left join db_owner.Image on Image.Id=ImageContent.ImageId join dbo.Batch on Batch.Id=Image.IDBatch Where IDBatch in (" + listBatch + ") and [MSNV]='" + MSNV + "' and [Lvl]='" + lvl + "' AND Batch.Hitpoint=2 Group by MSNV";
                    //else
                    //    sqlCommand.CommandText = "Select  SUM(Timework) as 'tt' From Dbo.[Performance] JOIN db_owner.[User] on Userid=[User].id Where  MSNV='" + MSNV + "' and Lvl=" + lvl + " and Batchid in (" + listBatch + ") Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        try
                        {

                            tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        }
                        catch { }
                        break;
                    }
                    sqlDataReader.Close();
                }
                //return tkt;
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        public double Tongtime_TemplateBatch2(string MSNV, string listBatch, int lvl)
        {
            double tkt = 0.0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Select SUM(TimeMiliseconds2) as 'tt' from db_owner.[User] as us2 join db_owner.ImageContent on us2.Id=ImageContent.UserId2 left join db_owner.Image on Image.Id=ImageContent.ImageId join dbo.Batch on Batch.Id=Image.IDBatch Where IDBatch in (" + listBatch + ") and [MSNV]='" + MSNV + "' and [Lvl]='" + lvl + "' AND Batch.Hitpoint=2 Group by MSNV";
                    //else
                    //    sqlCommand.CommandText = "Select  SUM(Timework) as 'tt' From Dbo.[Performance] JOIN db_owner.[User] on Userid=[User].id Where  MSNV='" + MSNV + "' and Lvl=" + lvl + " and Batchid in (" + listBatch + ") Group by MSNV";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        try
                        {

                            tkt = double.Parse(sqlDataReader["tt"].ToString().Trim());
                        }
                        catch { }
                        break;
                    }
                    sqlDataReader.Close();
                }
                //return tkt;
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return tkt;
        }
        #endregion
        public DataTable dtResult(string batchName, string template)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "SELECT NameImage as PathUri,Anh='',us1.Fullname,db_owner.ImageContent.Content1+'|'+Content1_OCR,Loisai1,ISNULL(us2.Fullname,'ENTRY2')+'|'+ISNULL(us4.Fullname,'ENTRY3'),ISNULL(db_owner.ImageContent.Content2,'')+'|'+ISNULL(Content3,''),Cast(Loisai2 as nvarchar(3))+'|'+Cast(Loisai3 as nvarchar(3)),us3.Fullname,db_owner.ImageContent.Result FROM db_owner.Image JOIN db_owner.Imagecontent ON imageID = [image].ID Join db_owner.[User] as us1 ON [Us1].id=Userid1 left Join db_owner.[User] as us2 ON [us2].id=Userid2 left Join db_owner.[User] as us4 ON [us4].id=Userid3 Join db_owner.[User] as us3 ON [us3].id=Checkerid WHERE BatchName = N'"
                                              + batchName + "' AND Template=" + template + " AND db_owner.Image.HitPoint =3 AND dateCheck IS NOT NULL";
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy table batch\n" + sqlException.Message);
            }
            return dt;
        }
        public DataTable dtResultQA(string batchName, string template)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "SELECT NameImage as PathUri,Anh='',us1.Fullname,db_owner.ImageContent.Content1+'|'+Content1_OCR,ISNULL(us2.Fullname,'ENTRY2')+'|'+ISNULL(us4.Fullname,'ENTRY3'),ISNULL(db_owner.ImageContent.Content2,'')+'|'+ISNULL(Content3,''),us3.Fullname,db_owner.ImageContent.Result FROM db_owner.Image Join db_owner.Imagecontent ON imageID = [image].ID Join db_owner.[User] as us1 ON [Us1].id=Userid1 left Join db_owner.[User] as us2 ON [us2].id=Userid2 left Join db_owner.[User] as us4 ON [us4].id=Userid3 Join db_owner.[User] as us3 ON [us3].id=CheckerId WHERE BatchName = N'"
                                              + batchName + "' AND Template=" + template + " AND dateQC IS NOT NULL";
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy table batch\n" + sqlException.Message);
            }
            return dt;
        }
        #region Check
        public DataTable SelectCheck_Date(string dateString, string group)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "SelectCheck_Date";
                    sqlCommand.Parameters.Add("@GROUP", SqlDbType.NVarChar, 30).Value = group;
                    sqlCommand.Parameters.Add("@startdate", SqlDbType.NVarChar, 10).Value = dateString;
                    con.Open();
                    SqlDataAdapter daUser = new SqlDataAdapter();
                    daUser.SelectCommand = sqlCommand;
                    daUser.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy Entry\n" + sqlException.Message);
            }
            return dt;
        }
        public DataTable Tongtime_check(string dateString, string group)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    if (group == "ALL")
                        sqlCommand.CommandText = @"Select MSNV,Lvl,SUM(timework) From DB_owner.[User] Join dbo.PerformanceCheck on userid=[User].ID Where LEFT(CONVERT(VARCHAR,Starttime, 112), 10) ='" + dateString + "' and timework>0 group by MSNV,lvl order by MSNV";
                    else
                        sqlCommand.CommandText = @"Select MSNV,Lvl,SUM(timework) From DB_owner.[User] Join dbo.PerformanceCheck on userid=[User].ID Where LEFT(CONVERT(VARCHAR,Starttime, 112), 10) ='" + dateString + "' and [Group]='" + group + "' and timework>0  group by MSNV,lvl order by MSNV";
                    con.Open();
                    SqlDataAdapter daUser = new SqlDataAdapter();
                    daUser.SelectCommand = sqlCommand;
                    daUser.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return dt;
        }

        public DataTable All_Check(string listBatch, string group, string lstemplate)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    if (group == "ALL")
                        sqlCommand.CommandText = @"select Fullname,[Group] as N'Trung tâm',MSNV,TimeMilisecondsCheck from db_owner.[User] join db_owner.ImageContent on [User].Id=ImageContent.CheckerId join db_owner.[Image] on Image.Id=ImageContent.ImageId where BatchName in (" + listBatch + ") AND Template in (" + lstemplate + ") and CheckerId is not null";
                    else
                        sqlCommand.CommandText = @"select Fullname,[Group] as N'Trung tâm',MSNV,TimeMilisecondsCheck from db_owner.[User] join db_owner.ImageContent on [User].Id=ImageContent.CheckerId join db_owner.[Image] on Image.Id=ImageContent.ImageId where BatchName in (" + listBatch + ") AND Template in (" + lstemplate + ") and [User].[Group] = N'" + group + "' and CheckerId is not null";
                    con.Open();
                    SqlDataAdapter daUser = new SqlDataAdapter();
                    daUser.SelectCommand = sqlCommand;
                    daUser.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return dt;
        }
        #endregion
        #region Lastcheck
        public DataTable All_Lastcheck(string listBatch, string group, string lstemplate)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    if (group == "ALL")
                        sqlCommand.CommandText = @"select BatchName,Fullname,[Group] as N'Trung tâm',TimeMilisecondsLC,Template from db_owner.ImageContent join db_owner.Image on Image.Id=ImageId join db_owner.[User]  on LastCheckId= [User].Id where BatchName in (" + listBatch + ") AND Template in (" + lstemplate + ")";
                    else
                        sqlCommand.CommandText = @"select BatchName,Fullname,[Group] as N'Trung tâm',TimeMilisecondsLC,Template from db_owner.ImageContent join db_owner.Image on Image.Id=ImageId join db_owner.[User]  on LastCheckId= [User].Id where BatchName in (" + listBatch + ") AND Template in (" + lstemplate + ") AND [User].[Group] = N'" + group + "'";
                    con.Open();
                    SqlDataAdapter daUser = new SqlDataAdapter();
                    daUser.SelectCommand = sqlCommand;
                    daUser.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return dt;
        }
        public DataTable All_NouHin(string listBatch, string group, string lstemplate)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    if (group == "ALL")
                        sqlCommand.CommandText = @"select BatchName,Fullname,[Group] as N'Trung tâm',TimeNouhin,Template from db_owner.[User] join db_owner.ImageContent on UserNouhin= [User].Id join db_owner.Image on Image.Id=ImageId where BatchName in (" + listBatch + ") AND Template in (" + lstemplate + ")";
                    else
                        sqlCommand.CommandText = @"select BatchName,Fullname,[Group] as N'Trung tâm',TimeNouhin,Template from db_owner.[User] join db_owner.ImageContent on UserNouhin= [User].Id join db_owner.Image on Image.Id=ImageId where BatchName in (" + listBatch + ") AND Template in (" + lstemplate + ") AND [User].[Group] = N'" + group + "'";
                    con.Open();
                    SqlDataAdapter daUser = new SqlDataAdapter();
                    daUser.SelectCommand = sqlCommand;
                    daUser.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy tổng ký tự\n" + sqlException.Message);
            }
            return dt;
        }
        #endregion
        //Get User
        public DataTable daUser(string username)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = @"select Id,Name,Password,Role from db_owner.[user] Where Upper(name) Like @name ";
                sqlCommand.Parameters.Add("@name", SqlDbType.NVarChar, 50).Value = username.Trim();
                SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                con.Open();
                sqldataAdapter.SelectCommand = sqlCommand;
                sqldataAdapter.Fill(dt);
            }
            return dt;

        }
        //Kiểm tra username,pass login
        public string password(string username)
        {
            string pass = "";
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = @"select Password from db_owner.[User] where UPPER(Name) = N'" + username + "'";
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        pass = sqlDataReader["Password"].ToString().Trim();
                        break;
                    }
                    sqlDataReader.Close();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình kiem tra password\n" + sqlException.Message);
            }
            return pass;
        }
        //update password
        public void Updatepassword(string pass, string name)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE db_owner.[User] SET Password = N'" + pass + "' where UPPER(Name) ='" + name + "'";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình update mật khẩu\n" + sqlException.Message);
            }
        }
        //Create New User
        public void NewUser(string username, string fulname, string lvl, string pair, string group, string role, string MSNV)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "INSERT INTO db_owner.[user] (Role,Name,Password,lvl,Pair,[Center],MSNV,Fullname) VALUES(N'" + role + "',N'" + username.Trim().ToUpper() + "','123456',N'" + lvl + "',N'" + pair + "',N'" + group + "',N'" + MSNV + "',N'" + fulname + "')";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Connection failed!" + sqlException.Message);
            }
        }
        //Update User
        public void UpdateUser(int id, string fulname, string lvl, string pair, string group, string role, string MSNV)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Update db_owner.[user] Set Role=N'" + role + "',lvl=N'" + lvl + "',Pair=N'" + pair + "',[Center]=N'" + group + "',MSNV=N'" + MSNV + "',Fullname=N'" + fulname + "' where id=" + id;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Connection failed!" + sqlException.Message);
            }
        }
        //Get table User
        public DataTable daAllUser()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = @"select Id, role,Name as 'User',Fullname,Center,MSNV,Pair,Lvl from db_owner.[User] where [Role] <> 'UPDATE'";
                SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                con.Open();
                sqldataAdapter.SelectCommand = sqlCommand;
                sqldataAdapter.Fill(dt);
            }
            return dt;

        }
        //Get table User
        public DataTable daAllFullname()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = @"select Distinct(Fullname) from db_owner.[user] order by Fullname";
                SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                con.Open();
                sqldataAdapter.SelectCommand = sqlCommand;
                sqldataAdapter.Fill(dt);
            }
            return dt;

        }
        //Delete User
        public void DeleteUser(int id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;

                    sqlCommand.CommandText = "Delete db_owner.[imagecontent] from db_owner.[imagecontent] join db_owner.[User] on userid1=[user].id where userid1=" + id;
                    SqlCommand sqlCommand1 = new SqlCommand();
                    sqlCommand1.Connection = con;
                    sqlCommand1.CommandText = "Delete db_owner.[imagecontent] from db_owner.[imagecontent] join db_owner.[User] on userid2=[user].id where userid2=" + id;
                    SqlCommand sqlCommand2 = new SqlCommand();
                    sqlCommand2.Connection = con;
                    sqlCommand2.CommandText = "Delete db_owner.[user] where id=" + id;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                    sqlCommand1.ExecuteNonQuery();
                    sqlCommand2.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Connection failed!" + sqlException.Message);
            }
        }
        //Get table User
        public DataTable daTemplate()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = @"select id,name from db_owner.[Formtemplate]";
                SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                con.Open();
                sqldataAdapter.SelectCommand = sqlCommand;
                sqldataAdapter.Fill(dt);
            }
            return dt;

        }
        //Get table User
        public DataTable daBatch()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = @"select * from dbo.Batch";
                SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                con.Open();
                sqldataAdapter.SelectCommand = sqlCommand;
                sqldataAdapter.Fill(dt);
            }
            return dt;

        }


        //get SoPhieu, SoDong in Batch by UploadDate
        public DataTable GetSoPhieu_SoDongByUpdateDate(string date)
        {
            DataTable dt = new DataTable();
            if (!date.Equals(""))
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(stringconnecttion))
                    {
                        SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.Connection = con;
                        sqlCommand.CommandText = "select SoPhieu, SoDong from dbo.Batch where CAST(UploadDate as date) = '" + date + "'";
                        SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                        con.Open();
                        sqldataAdapter.SelectCommand = sqlCommand;
                        sqldataAdapter.Fill(dt);
                    }
                }
                catch (Exception ex)
                {
                    throw new System.Exception("Lỗi khi lây du liêu \n" + ex.Message);
                }
            }
            return dt;
        }

        public void Update_Password(string id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE db_owner.[User] SET Password = '123456' where id=" + id;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình update mật khẩu\n" + sqlException.Message);
            }
        }

        public DataTable Get_status(string table)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = "select [" + table + "].ID, "
                                          + "Content1,ISNULL(UserId1, 0 ) as 'UserId1', InputDate1, ISNULL(us1.Center, 0 ) as 'g1', ISNULL(cast(round(TimeMiliseconds1/60000.0,2) as numeric(36,2)), 0) as TimeE1, "
                                          + "Content2,ISNULL(UserId2, 0 ) as 'UserId2', InputDate2, ISNULL(us2.Center, 0 ) as 'g2', ISNULL(cast(round(TimeMiliseconds2/60000.0,2) as numeric(36,2)), 0) as TimeE2, "
                                          + "Content3,ISNULL(UserId3, 0 ) as 'UserId3', InputDate3, ISNULL(us3.Center, 0 ) as 'g3', ISNULL(cast(round(TimeMiliseconds3/60000.0,2) as numeric(36,2)), 0) as TimeE3, "
                                          + "CheckedDate,QCDate,ISNULL(CheckerId, 0 ) as CheckerId, ISNULL(us4.Center, 0 ) as 'g4', Result, us5.FullName as LC, us6.FullName as NouHin, Batch.Id as IDBatch, Batch.Name, Batch.TempID  "
                                          + "from dbo.[" + table + "] "
                                          + "left join db_owner.[User] as us1 on [" + table + "].UserId1=us1.id "
                                          + "left join db_owner.[User] as us2 on [" + table + "].UserId2=us2.id "
                                          + "left join db_owner.[User] as us3 on [" + table + "].UserID3=us3.id "
                                          + "left join db_owner.[User] as us4 on [" + table + "].CheckerId=us4.id "
                                          + "left join db_owner.[User] as us5 on [" + table + "].LastCheckId =us5.id "
                                          + "left join db_owner.[User] as us6 on [" + table + "].UserNouhin =us6.id "
                                          + "join Batch on Batch.Id = cast(LEFT('" + table + "',CHARINDEX('--','" + table + "') - 1) as INT)";
                SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                con.Open();
                sqldataAdapter.SelectCommand = sqlCommand;
                sqldataAdapter.Fill(dt);
            }
            return dt;
        }
        public DataTable Get_Data_Save(string listBatch, string lstemplate)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = "select NameImage,Content1,Content1_OCR,us1.FullName as Name1,InputDate1,Loisai1,TongKyTu1,TimeMiliseconds1,Content2,us2.FullName as Name2,InputDate2,Loisai2,Tongkytu2,TimeMiliseconds2,Result,us3.FullName as Checker,CheckedDate,us4.FullName as LastCheck,Tongkytu,TimeMilisecondsCheck,TimeMilisecondsLC,TimeNouhin,us5.FullName as UserNouhin,ResultLC,Loisai3,Tongkytu3,TimeMiliseconds3,Content3,us6.FullName as Name3,InputDate3,BatchName,us1.[Group] as TT1,us2.[Group] as TT2,us3.[Group] as TT3,us4.[Group] as TT4,us5.[Group] as TT5,us6.[Group] as TT6,Template from db_owner.[Image] join db_owner.ImageContent on db_owner.ImageContent.ImageId = db_owner.Image.Id left join db_owner.[user] as us1 ON UserId1=us1.id left join db_owner.[user] as us2 on UserId2=us2.id left join db_owner.[user] as us3 on CheckerId=us3.id left join db_owner.[user] as us4 on LastcheckID=us4.id left join db_owner.[user] as us5 on UserNouhin=us5.id left join db_owner.[user] as us6 on UserId3=us6.id where BatchName in (" + listBatch + ") AND Template in (" + lstemplate + ")";
                SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                con.Open();
                sqldataAdapter.SelectCommand = sqlCommand;
                sqldataAdapter.Fill(dt);
            }
            return dt;
        }
        public DataTable Get_batch(int formid)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = "select Id,Name from dbo.Batch where formid=" + formid;
                SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                con.Open();
                sqldataAdapter.SelectCommand = sqlCommand;
                sqldataAdapter.Fill(dt);
            }
            return dt;
        }
        public DataTable Get_alldetails(string batchName, string template)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = "select NameImage, (us1.Email + '-' + us1.[Group] + ':     ' + Content1+'|' + Content1_OCR) as 'Entry1',Content_OCR,(us2.Email + '-' + us2.[Group] + ':     ' + Content2) as 'Entry2',(us3.Email + '-' + us3.[Group] + ':     ' + Content3) as 'Entry3',(us4.Email + '-' + us4.[Group] + ':     ' + Result) as 'Checker' ,(us5.Email + '-' + us5.[Group] + ':     ' + ResultLC) as 'Lastcheck' from db_owner.ImageContent join db_owner.Image on Image.Id=ImageContent.ImageId left join db_owner.[User] as us1 on us1.Id=ImageContent.UserId1 left join db_owner.[User] as us2 on us2.Id=ImageContent.UserId2 left join db_owner.[User] as us3 on us3.Id=ImageContent.UserId3 left join db_owner.[User] as us4 on us4.Id=ImageContent.CheckerId left join db_owner.[User] as us5 on us5.Id=ImageContent.LastCheckId  where Template='" + template + "' AND BatchName= N'" + batchName + "'";
                SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                con.Open();
                sqldataAdapter.SelectCommand = sqlCommand;
                sqldataAdapter.Fill(dt);
            }
            return dt;
        }
        public DataTable Ctrl_1(int Id)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "Fix_Error_null1";
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                    SqlDataAdapter daUser = new SqlDataAdapter();
                    daUser.SelectCommand = sqlCommand;
                    con.Open();
                    daUser.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy Entry\n" + sqlException.Message);
            }
            return dt;
        }
        public DataTable Search_All(string ab)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "search_all";
                    sqlCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = ab;
                    SqlDataAdapter daUser = new SqlDataAdapter();
                    daUser.SelectCommand = sqlCommand;
                    con.Open();
                    daUser.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy Entry\n" + sqlException.Message);
            }
            return dt;
        }

        public DataTable Ctrl_2(int Id)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "Fix_Result_null2";
                    sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
                    SqlDataAdapter daUser = new SqlDataAdapter();
                    daUser.SelectCommand = sqlCommand;
                    con.Open();
                    daUser.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy Entry\n" + sqlException.Message);
            }
            return dt;
        }
        public void Ctrl_3(int id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Delete from dbo.LastCheck where BatchID=" + id;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Connection failed!" + sqlException.Message);
            }
        }
        public DataTable dttrungtam()
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "select distinct([Center]) as trungtam from db_owner.[User]";
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy trung tam\n" + sqlException.Message);
            }
            if (dt.Rows.Count > 0)
                dt.Rows.Add("ALL");
            return dt;
        }

        public byte[] GetImageBatch(string batchName, string imageName, int template)
        {
            byte[] image = null;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "select BinaryImage from db_owner.[Image] join dbo.[" + batchName + "] on Image.NameImage = [" + batchName + "].NameImage where Image.NameImage = '" + imageName + "' and BatchName = '" + batchName + "' AND Template=" + template;
                    con.Open();
                    image = (byte[])sqlCommand.ExecuteScalar();
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy image\n" + sqlException.Message);
            }
            return image;
        }
        public DataTable All_User()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = "Select Id,[Role],Name,Fullname,[Group] from db_owner.[User]";
                SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                con.Open();
                sqldataAdapter.SelectCommand = sqlCommand;
                sqldataAdapter.Fill(dt);
            }
            return dt;

        }
        public DataTable All_details(int id)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = "Select MSNV,Email,Lvl,Pair from db_owner.[User] where Id = " + id + "";
                SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                con.Open();
                sqldataAdapter.SelectCommand = sqlCommand;
                sqldataAdapter.Fill(dt);
            }
            return dt;

        }
        public int ktuser(string user)
        {
            int id = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "select count(Id) from db_owner.[User] where Name = N'" + user + "'";
                    con.Open();
                    id = Convert.ToInt32(sqlCommand.ExecuteScalar().ToString());
                }
            }
            catch
            { }
            return id;
        }

        public void Update_CD(string cd)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.CSPD SET Content = N'" + cd + "'";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình update mật khẩu\n" + sqlException.Message);
            }
        }
        public void Update_CS(string cs)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.CSPS SET Content = N'" + cs + "'";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình update mật khẩu\n" + sqlException.Message);
            }
        }

        public void Insert_CD(string ct)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Insert into dbo.CSPD (Content) values (N'" + ct + "')";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình update mật khẩu\n" + sqlException.Message);
            }
        }
        public void Insert_CS(string ct)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Insert into dbo.CSPS (Content) values (N'" + ct + "')";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình update mật khẩu\n" + sqlException.Message);
            }
        }
        public int ktlc(int lc)
        {
            int id = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "select COUNT(id) from dbo.LastCheck where BatchID = " + lc + "";
                    con.Open();
                    id = Convert.ToInt32(sqlCommand.ExecuteScalar().ToString());
                }
            }
            catch
            { }
            return id;
        }
        public int get_timecheck(string lstbatch, string FullName)
        {
            int id = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "select Sum(TimeWork) from dbo.PerformanceCheck Join db_owner.[User] on [User].id=UserId where BatchID in (" + lstbatch + ") and Fullname=N'" + FullName + "'";
                    con.Open();
                    id = Convert.ToInt32(sqlCommand.ExecuteScalar().ToString());
                }
            }
            catch
            { }
            return id;
        }
        public int get_timelcheck(string lstbatch, string FullName)
        {
            int id = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "select Sum(TimeWork) from dbo.PerformanceLastCheck Join db_owner.[User] on [User].id=UserId where BatchID in (" + lstbatch + ") and Fullname=N'" + FullName + "'";
                    con.Open();
                    id = Convert.ToInt32(sqlCommand.ExecuteScalar().ToString());
                }
            }
            catch
            { }
            return id;
        }
        public DataTable GetNameTemplate()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = "select Name,Id from db_owner.FormTemplate";
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlCommand;
                da.Fill(dt);
            }
            return dt;
        }

        public DataTable GetBatch()
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select Batch.Id, Batch.Name,TempID as Template,convert(varchar, UploadDate, 103)  as 'Ngày up',sysindexes.Rows as N'Số lượng up' from dbo.Batch INNER JOIN sysobjects ON sysobjects.Name = Batch.Name INNER JOIN sysindexes  ON sysobjects.id = sysindexes.id WHERE len(sysobjects.Name)=21 and Hitpoint<3 and sysindexes.Rows > 0";
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return dt;
        }

        public DataTable GetBatch_Hide()
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select Batch.Name,TempID as Template,UploadDate as 'Ngày up',sysindexes.Rows as N'Số lượng' from dbo.Batch INNER JOIN sysobjects ON sysobjects.Name = Batch.Name INNER JOIN sysindexes  ON sysobjects.id = sysindexes.id WHERE len(sysobjects.Name)=21 and Hitpoint=3";
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return dt;
        }
        public void DeleteTalble(string batchName)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "DROP TABLE [" + batchName + "]";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }

            }
            catch
            { }
        }
        public void DeleteBatch(string batchname)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "delete db_owner.[imagecontent] from db_owner.imagecontent JOIN db_owner.[image] ON imageid=[image].id where BatchName=N'" + batchname + "';" +
                                             "delete db_owner.[image] from db_owner.[image] where BatchName=N'" + batchname + "';" +
                                             "delete dbo.[batch] where Name=N'" + batchname + "';";
                    sqlCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = batchname;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình Delete Batch\n" + sqlException.Message);
            }

        }
        public void UpdateHitpointBatch(string idbatch, string template)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Update dbo.Batch set Hitpoint=3 where Name=N'" + idbatch + "' AND TempID=" + template;
                    con.Open();
                    sqlCommand.ExecuteReader();
                }
            }
            catch { }
        }
        public void UpdateHitpointBatch_UnHide(string idbatch, string template)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Update dbo.Batch set Hitpoint=2 where Name=N'" + idbatch + "' AND TempID=" + template;
                    con.Open();
                    sqlCommand.ExecuteReader();
                }
            }
            catch { }
        }
        public void UpdateHitpointBatchAll()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Update dbo.Batch set Hitpoint=2 where Hitpoint=3";
                    con.Open();
                    sqlCommand.ExecuteReader();
                }
            }
            catch { }
        }

        public int getEntryinBatch(string nameTable)
        {
            int returnVal = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "select COUNT(*) as 'Số lượng nhập' from dbo.[" + nameTable + "] join Batch on Batch.Id = cast(LEFT('" + nameTable + "',CHARINDEX('--','" + nameTable + "') - 1) as INT) WHERE dbo.[" + nameTable + "].Content_OCR LIKE '%?%'";
                    con.Open();
                    returnVal = (int)sqlCommand.ExecuteScalar();
                }
            }
            catch
            { }
            return returnVal;
        }

        public string get_NameTable(string like)
        {

            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "select name from sys.tables where name like '" + like + "%'";
                    con.Open();
                    var res = sqlCommand.ExecuteScalar();
                    if (res == null)
                        return "";
                    else
                        return res.ToString();

                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Error! Get batchname user!\n" + sqlException.Message);
            }

        }

        public DataTable get_alltable(string tableName, int user1_3) //yyyy-mm-dd
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = " select Fullname,Lvl as 'Level',MSNV,Center as N'Trung Tâm',ImageName,BatchName,Content" + user1_3 + " as 'Content',UserId" + user1_3 + " as 'UserId',COALESCE(NG" + user1_3 + ", 0) as 'NG', Tongkytu" + user1_3 + " as N'Tổng ký tự',COALESCE(Loisai" + user1_3 + ", 0) as N'Lỗi sai',TimeMiliseconds" + user1_3 + " as 'Thời gian' "
                                + "from [" + tableName + "],db_owner.[User] where UserId" + user1_3 + "!=0 and [" + tableName + "].UserId" + user1_3 + "=db_owner.[User].Id";
                    SqlDataAdapter da = new SqlDataAdapter();
                    con.Open();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Error! Get batchname user!\n" + sqlException.Message);
            }
            return dt;
        }

        public DataTable get_alltable_check(string tableName, string chekc_QC, string check_lc) //yyyy-mm-dd
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    if (tableName.IndexOf("lv3") != -1)
                        sqlCommand.CommandText = " select Fullname,MSNV,BatchName,'Kojin_LV3' as 'Template',Center as N'Trung Tâm',TimeMiliseconds" + check_lc
                                    + " from [" + tableName + "],db_owner.[User] where " + chekc_QC + "ID >0 and [" + tableName + "]." + chekc_QC + "ID=db_owner.[User].Id";
                    else
                        sqlCommand.CommandText = " select Fullname,MSNV,BatchName,'Kojin_LV1' as 'Template',Center as N'Trung Tâm',TimeMiliseconds" + check_lc
                                    + " from [" + tableName + "],db_owner.[User] where " + chekc_QC + "ID >0 and [" + tableName + "]." + chekc_QC + "ID=db_owner.[User].Id";
                    SqlDataAdapter da = new SqlDataAdapter();
                    con.Open();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Error! Get batchname user!\n" + sqlException.Message);
            }
            return dt;
        }

        public DataTable get_alltable_NH(string tableName) //yyyy-mm-dd
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    if (tableName.IndexOf("lv3") != -1)
                        sqlCommand.CommandText = " select Fullname,MSNV,BatchName,'Kojin_LV3' as 'Template',Center as N'Trung Tâm',TimeNouhin"
                                    + " from [" + tableName + "],db_owner.[User] where UserNouhin >0 and [" + tableName + "].UserNouhin=db_owner.[User].Id";
                    else
                        sqlCommand.CommandText = " select Fullname,MSNV,BatchName,'Kojin_LV1' as 'Template',Center as N'Trung Tâm',TimeNouhin"
                                    + " from [" + tableName + "],db_owner.[User] where UserNouhin >0 and [" + tableName + "].UserNouhin=db_owner.[User].Id";
                    SqlDataAdapter da = new SqlDataAdapter();
                    con.Open();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Error! Get batchname user!\n" + sqlException.Message);
            }
            return dt;
        }

        public DataTable dtResult(string batchName)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "SELECT ImageName as PathUri,Anh='',us1.Fullname,Content1+'|'+Content1_OCR,Loisai1,ISNULL(us2.Fullname,'ENTRY2')+'|'+ISNULL(us4.Fullname,'ENTRY3'),ISNULL(Content2,'')+'|'+ISNULL(Content3,''),Cast(Loisai2 as nvarchar(3))+'|'+Cast(ISNULL(Loisai3,'') as nvarchar(3)),us3.Fullname,Result "
                                + "FROM [" + batchName + "] Join db_owner.[User] as us1 ON [Us1].id=Userid1 left Join db_owner.[User] as us2 ON [us2].id=Userid2 left Join db_owner.[User] as us4 ON [us4].id=Userid3 Join db_owner.[User] as us3 ON [us3].id=Checkerid "
                                + "where  CheckedDate is not NULL";
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy table batch\n" + sqlException.Message);
            }
            return dt;
        }

        public byte[] GetImageBatch(string batchName, string imageName)
        {
            byte[] image = null;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "select Binarymage from [" + batchName + "] where ImageName = '" + imageName + "'";
                    con.Open();
                    image = (byte[])sqlCommand.ExecuteScalar();
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy image\n" + sqlException.Message);
            }
            return image;
        }

        public DataTable dtResultQA(string batchName)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "SELECT ImageName as PathUri,Anh='',us1.Fullname,Content1+'|'+Content1_OCR,Loisai1,ISNULL(us2.Fullname,'ENTRY2')+'|'+ISNULL(us4.Fullname,'ENTRY3'),ISNULL(Content2,'')+'|'+ISNULL(Content3,''),Cast(Loisai2 as nvarchar(3))+'|'+Cast(ISNULL(Loisai3,'') as nvarchar(3)),us3.Fullname,Result "
                                + "FROM [" + batchName + "] Join db_owner.[User] as us1 ON [Us1].id=Userid1 left Join db_owner.[User] as us2 ON [us2].id=Userid2 left Join db_owner.[User] as us4 ON [us4].id=Userid3 Join db_owner.[User] as us3 ON [us3].id=Checkerid "
                                + "where  CheckedDate is NULL";
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình lấy table batch\n" + sqlException.Message);
            }
            return dt;
        }
    }
}