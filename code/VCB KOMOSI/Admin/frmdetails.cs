﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VCB_KOMOSI  
{
    public partial class frmdetails : Form
    {
        public frmdetails()
        {
            InitializeComponent();
        }
        public string batchname;
        public string template;
        WorkDB_Admin workdb = new WorkDB_Admin();
        System.Data.DataTable dtAlldetails;
        private void frmdetails_Load(object sender, EventArgs e)
        {  
            grddetails.DataSource = null;
            dtAlldetails = workdb.Get_alldetails(batchname,template);
            grddetails.DataSource = dtAlldetails;
            //grvdetails.Columns[0].Visible = false;
            grvdetails.BestFitColumns();
        }

        private void frmdetails_KeyDown(object sender, KeyEventArgs e)
        {            
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }
    }
}
