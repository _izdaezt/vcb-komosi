﻿namespace VCB_KOMOSI
{
    partial class frmAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        // Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAdmin));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            this.quanlyuer = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveFD = new System.Windows.Forms.SaveFileDialog();
            this.Status = new System.Windows.Forms.TabControl();
            this.tabUser = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtsearch = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grdUsers = new DevExpress.XtraGrid.GridControl();
            this.grvUsers = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.UserId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Role = new DevExpress.XtraGrid.Columns.GridColumn();
            this.User = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Fullname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Group = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MSNV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lblID = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btndel = new System.Windows.Forms.Button();
            this.btnedit = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.cboFullname = new System.Windows.Forms.ComboBox();
            this.lblError = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cboGroup = new System.Windows.Forms.ComboBox();
            this.lblGroup = new System.Windows.Forms.Label();
            this.cboPair = new System.Windows.Forms.ComboBox();
            this.lblPair = new System.Windows.Forms.Label();
            this.cboLevel = new System.Windows.Forms.ComboBox();
            this.lblLevel = new System.Windows.Forms.Label();
            this.cboRole = new System.Windows.Forms.ComboBox();
            this.lblRole = new System.Windows.Forms.Label();
            this.lblFulname = new System.Windows.Forms.Label();
            this.txtFullname = new System.Windows.Forms.TextBox();
            this.lblMSNV = new System.Windows.Forms.Label();
            this.txtMSNV = new System.Windows.Forms.TextBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tabPerformance = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.rdbNouhin = new System.Windows.Forms.RadioButton();
            this.rdbLastCheck = new System.Windows.Forms.RadioButton();
            this.rdbEntry = new System.Windows.Forms.RadioButton();
            this.rdbCheck = new System.Windows.Forms.RadioButton();
            this.cboTrungTam = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.prgbExcel = new System.Windows.Forms.ProgressBar();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.grThongke = new DevExpress.XtraGrid.GridControl();
            this.grThongkeV = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.grcPerformance = new DevExpress.XtraGrid.GridControl();
            this.grvPerformance = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.tabFeedback = new System.Windows.Forms.TabPage();
            this.lblInformation = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.prgExcel = new System.Windows.Forms.ProgressBar();
            this.btnExportQC = new System.Windows.Forms.Button();
            this.btnExportCheck = new System.Windows.Forms.Button();
            this.tabstatus = new System.Windows.Forms.TabPage();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnviewstt = new System.Windows.Forms.Button();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.grdstatus = new DevExpress.XtraGrid.GridControl();
            this.grvstatus = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.rtxtGrid = new DevExpress.XtraEditors.Repository.RepositoryItemRichTextEdit();
            this.tabData = new System.Windows.Forms.TabPage();
            this.btnSaveDate = new System.Windows.Forms.Button();
            this.splitContainer7 = new System.Windows.Forms.SplitContainer();
            this.grcViewData = new DevExpress.XtraGrid.GridControl();
            this.grvViewData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemRichTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemRichTextEdit();
            this.tabBatchHide = new System.Windows.Forms.TabPage();
            this.grcBatchHide = new DevExpress.XtraGrid.GridControl();
            this.grvBatchHide = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox5 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox6 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.bwExportExcel = new System.ComponentModel.BackgroundWorker();
            this.bwExportExcelQC = new System.ComponentModel.BackgroundWorker();
            this.bgwXem = new System.ComponentModel.BackgroundWorker();
            this.Status.SuspendLayout();
            this.tabUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.tabPerformance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grThongke)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grThongkeV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcPerformance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvPerformance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            this.tabFeedback.SuspendLayout();
            this.tabstatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdstatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvstatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtxtGrid)).BeginInit();
            this.tabData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).BeginInit();
            this.splitContainer7.Panel2.SuspendLayout();
            this.splitContainer7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvViewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRichTextEdit1)).BeginInit();
            this.tabBatchHide.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grcBatchHide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvBatchHide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).BeginInit();
            this.SuspendLayout();
            // 
            // quanlyuer
            // 
            this.quanlyuer.Name = "quanlyuer";
            this.quanlyuer.Size = new System.Drawing.Size(32, 19);
            // 
            // Status
            // 
            this.Status.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Status.Controls.Add(this.tabUser);
            this.Status.Controls.Add(this.tabPerformance);
            this.Status.Controls.Add(this.tabFeedback);
            this.Status.Controls.Add(this.tabstatus);
            this.Status.Controls.Add(this.tabData);
            this.Status.Controls.Add(this.tabBatchHide);
            this.Status.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Status.Location = new System.Drawing.Point(3, 1);
            this.Status.Name = "Status";
            this.Status.SelectedIndex = 0;
            this.Status.Size = new System.Drawing.Size(926, 645);
            this.Status.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.Status.TabIndex = 0;
            this.Status.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControlAdmin_Selecting);
            // 
            // tabUser
            // 
            this.tabUser.Controls.Add(this.splitContainer1);
            this.tabUser.Location = new System.Drawing.Point(4, 22);
            this.tabUser.Name = "tabUser";
            this.tabUser.Padding = new System.Windows.Forms.Padding(3);
            this.tabUser.Size = new System.Drawing.Size(918, 619);
            this.tabUser.TabIndex = 2;
            this.tabUser.Text = "User managerment";
            this.tabUser.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.splitContainer1.Panel2.Controls.Add(this.lblID);
            this.splitContainer1.Panel2.Controls.Add(this.lblTitle);
            this.splitContainer1.Panel2.Controls.Add(this.btndel);
            this.splitContainer1.Panel2.Controls.Add(this.btnedit);
            this.splitContainer1.Panel2.Controls.Add(this.btnadd);
            this.splitContainer1.Panel2.Controls.Add(this.cboFullname);
            this.splitContainer1.Panel2.Controls.Add(this.lblError);
            this.splitContainer1.Panel2.Controls.Add(this.btnOk);
            this.splitContainer1.Panel2.Controls.Add(this.btnCancel);
            this.splitContainer1.Panel2.Controls.Add(this.cboGroup);
            this.splitContainer1.Panel2.Controls.Add(this.lblGroup);
            this.splitContainer1.Panel2.Controls.Add(this.cboPair);
            this.splitContainer1.Panel2.Controls.Add(this.lblPair);
            this.splitContainer1.Panel2.Controls.Add(this.cboLevel);
            this.splitContainer1.Panel2.Controls.Add(this.lblLevel);
            this.splitContainer1.Panel2.Controls.Add(this.cboRole);
            this.splitContainer1.Panel2.Controls.Add(this.lblRole);
            this.splitContainer1.Panel2.Controls.Add(this.lblFulname);
            this.splitContainer1.Panel2.Controls.Add(this.txtFullname);
            this.splitContainer1.Panel2.Controls.Add(this.lblMSNV);
            this.splitContainer1.Panel2.Controls.Add(this.txtMSNV);
            this.splitContainer1.Panel2.Controls.Add(this.lblUsername);
            this.splitContainer1.Panel2.Controls.Add(this.txtUsername);
            this.splitContainer1.Panel2.Controls.Add(this.statusStrip1);
            this.splitContainer1.Size = new System.Drawing.Size(912, 613);
            this.splitContainer1.SplitterDistance = 457;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.grdUsers);
            this.splitContainer4.Size = new System.Drawing.Size(457, 613);
            this.splitContainer4.SplitterDistance = 25;
            this.splitContainer4.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel2.Controls.Add(this.txtsearch);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(457, 25);
            this.panel2.TabIndex = 0;
            // 
            // txtsearch
            // 
            this.txtsearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtsearch.Location = new System.Drawing.Point(63, 2);
            this.txtsearch.Name = "txtsearch";
            this.txtsearch.Size = new System.Drawing.Size(391, 20);
            this.txtsearch.TabIndex = 30;
            this.txtsearch.TextChanged += new System.EventHandler(this.txtsearch_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 16);
            this.label2.TabIndex = 29;
            this.label2.Text = "Search:";
            // 
            // grdUsers
            // 
            this.grdUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdUsers.Location = new System.Drawing.Point(0, 0);
            this.grdUsers.MainView = this.grvUsers;
            this.grdUsers.Name = "grdUsers";
            this.grdUsers.Size = new System.Drawing.Size(457, 584);
            this.grdUsers.TabIndex = 1;
            this.grdUsers.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvUsers,
            this.gridView1});
            // 
            // grvUsers
            // 
            this.grvUsers.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grvUsers.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvUsers.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvUsers.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvUsers.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.grvUsers.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.UserId,
            this.Role,
            this.User,
            this.Fullname,
            this.Group,
            this.MSNV});
            this.grvUsers.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grvUsers.GridControl = this.grdUsers;
            this.grvUsers.IndicatorWidth = 42;
            this.grvUsers.Name = "grvUsers";
            this.grvUsers.OptionsBehavior.ReadOnly = true;
            this.grvUsers.OptionsCustomization.AllowColumnMoving = false;
            this.grvUsers.OptionsCustomization.AllowColumnResizing = false;
            this.grvUsers.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grvUsers.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.grvUsers.OptionsSelection.EnableAppearanceHideSelection = false;
            this.grvUsers.OptionsSelection.InvertSelection = true;
            this.grvUsers.OptionsSelection.MultiSelect = true;
            this.grvUsers.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grvUsers.OptionsSelection.UseIndicatorForSelection = false;
            this.grvUsers.OptionsView.ShowGroupPanel = false;
            this.grvUsers.DoubleClick += new System.EventHandler(this.grvUsers_DoubleClick);
            // 
            // UserId
            // 
            this.UserId.Caption = "Id";
            this.UserId.FieldName = "Id";
            this.UserId.Name = "UserId";
            this.UserId.Visible = true;
            this.UserId.VisibleIndex = 0;
            // 
            // Role
            // 
            this.Role.Caption = "Vai Trò";
            this.Role.FieldName = "role";
            this.Role.Name = "Role";
            this.Role.Visible = true;
            this.Role.VisibleIndex = 1;
            // 
            // User
            // 
            this.User.Caption = "User";
            this.User.FieldName = "User";
            this.User.Name = "User";
            this.User.Visible = true;
            this.User.VisibleIndex = 2;
            // 
            // Fullname
            // 
            this.Fullname.Caption = "Fullname";
            this.Fullname.FieldName = "Fullname";
            this.Fullname.Name = "Fullname";
            this.Fullname.Visible = true;
            this.Fullname.VisibleIndex = 3;
            // 
            // Group
            // 
            this.Group.Caption = "Trungtam";
            this.Group.FieldName = "Center";
            this.Group.Name = "Group";
            this.Group.Visible = true;
            this.Group.VisibleIndex = 4;
            // 
            // MSNV
            // 
            this.MSNV.Caption = "MSNV";
            this.MSNV.FieldName = "MSNV";
            this.MSNV.Name = "MSNV";
            this.MSNV.Visible = true;
            this.MSNV.VisibleIndex = 5;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.grdUsers;
            this.gridView1.Name = "gridView1";
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Enabled = false;
            this.lblID.Location = new System.Drawing.Point(407, 6);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(0, 13);
            this.lblID.TabIndex = 29;
            this.lblID.Visible = false;
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.DarkOrange;
            this.lblTitle.Location = new System.Drawing.Point(3, 114);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(451, 23);
            this.lblTitle.TabIndex = 28;
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btndel
            // 
            this.btndel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btndel.BackgroundImage")));
            this.btndel.Location = new System.Drawing.Point(306, 26);
            this.btndel.Name = "btndel";
            this.btndel.Size = new System.Drawing.Size(70, 70);
            this.btndel.TabIndex = 26;
            this.btndel.UseVisualStyleBackColor = true;
            this.btndel.Click += new System.EventHandler(this.btndel_Click);
            // 
            // btnedit
            // 
            this.btnedit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnedit.BackgroundImage")));
            this.btnedit.Location = new System.Drawing.Point(201, 26);
            this.btnedit.Name = "btnedit";
            this.btnedit.Size = new System.Drawing.Size(70, 70);
            this.btnedit.TabIndex = 25;
            this.btnedit.UseVisualStyleBackColor = true;
            this.btnedit.Click += new System.EventHandler(this.btnedit_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnadd.BackgroundImage")));
            this.btnadd.Location = new System.Drawing.Point(91, 26);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(70, 70);
            this.btnadd.TabIndex = 24;
            this.btnadd.UseVisualStyleBackColor = true;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // cboFullname
            // 
            this.cboFullname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboFullname.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboFullname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFullname.FormattingEnabled = true;
            this.cboFullname.Items.AddRange(new object[] {
            "ENTRY",
            "CHECK",
            "LASTCHECK",
            "CROP",
            "ADMIN"});
            this.cboFullname.Location = new System.Drawing.Point(135, 251);
            this.cboFullname.Name = "cboFullname";
            this.cboFullname.Size = new System.Drawing.Size(263, 24);
            this.cboFullname.TabIndex = 22;
            this.cboFullname.Visible = false;
            this.cboFullname.SelectedValueChanged += new System.EventHandler(this.cboFullname_SelectedValueChanged);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(85, 422);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 21);
            this.lblError.TabIndex = 21;
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(108, 456);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(87, 51);
            this.btnOk.TabIndex = 9;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Visible = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(317, 456);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 51);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cboGroup
            // 
            this.cboGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGroup.Enabled = false;
            this.cboGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboGroup.FormattingEnabled = true;
            this.cboGroup.Items.AddRange(new object[] {
            "DN",
            "PY",
            "HUE",
            "DL"});
            this.cboGroup.Location = new System.Drawing.Point(303, 376);
            this.cboGroup.Name = "cboGroup";
            this.cboGroup.Size = new System.Drawing.Size(121, 24);
            this.cboGroup.TabIndex = 8;
            this.cboGroup.SelectedValueChanged += new System.EventHandler(this.cboGroup_SelectedValueChanged);
            // 
            // lblGroup
            // 
            this.lblGroup.AutoSize = true;
            this.lblGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGroup.Location = new System.Drawing.Point(256, 379);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(45, 16);
            this.lblGroup.TabIndex = 15;
            this.lblGroup.Text = "Group";
            // 
            // cboPair
            // 
            this.cboPair.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboPair.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboPair.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPair.Enabled = false;
            this.cboPair.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPair.FormattingEnabled = true;
            this.cboPair.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cboPair.Location = new System.Drawing.Point(92, 379);
            this.cboPair.Name = "cboPair";
            this.cboPair.Size = new System.Drawing.Size(121, 24);
            this.cboPair.TabIndex = 7;
            this.cboPair.SelectedValueChanged += new System.EventHandler(this.cboPair_SelectedValueChanged);
            // 
            // lblPair
            // 
            this.lblPair.AutoSize = true;
            this.lblPair.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPair.Location = new System.Drawing.Point(49, 382);
            this.lblPair.Name = "lblPair";
            this.lblPair.Size = new System.Drawing.Size(32, 16);
            this.lblPair.TabIndex = 13;
            this.lblPair.Text = "Pair";
            // 
            // cboLevel
            // 
            this.cboLevel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboLevel.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLevel.Enabled = false;
            this.cboLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboLevel.FormattingEnabled = true;
            this.cboLevel.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.cboLevel.Location = new System.Drawing.Point(303, 323);
            this.cboLevel.Name = "cboLevel";
            this.cboLevel.Size = new System.Drawing.Size(121, 24);
            this.cboLevel.TabIndex = 6;
            this.cboLevel.SelectedValueChanged += new System.EventHandler(this.cboLevel_SelectedValueChanged);
            // 
            // lblLevel
            // 
            this.lblLevel.AutoSize = true;
            this.lblLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevel.Location = new System.Drawing.Point(256, 326);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(41, 16);
            this.lblLevel.TabIndex = 11;
            this.lblLevel.Text = "Level";
            // 
            // cboRole
            // 
            this.cboRole.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboRole.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRole.Enabled = false;
            this.cboRole.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRole.FormattingEnabled = true;
            this.cboRole.Items.AddRange(new object[] {
            "ENTRY",
            "CHECK",
            "LASTCHECK",
            "CROP",
            "ADMIN"});
            this.cboRole.Location = new System.Drawing.Point(92, 323);
            this.cboRole.Name = "cboRole";
            this.cboRole.Size = new System.Drawing.Size(121, 24);
            this.cboRole.TabIndex = 5;
            this.cboRole.SelectedValueChanged += new System.EventHandler(this.cboRole_SelectedValueChanged);
            // 
            // lblRole
            // 
            this.lblRole.AutoSize = true;
            this.lblRole.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRole.Location = new System.Drawing.Point(49, 326);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(37, 16);
            this.lblRole.TabIndex = 9;
            this.lblRole.Text = "Role";
            // 
            // lblFulname
            // 
            this.lblFulname.AutoSize = true;
            this.lblFulname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFulname.Location = new System.Drawing.Point(51, 256);
            this.lblFulname.Name = "lblFulname";
            this.lblFulname.Size = new System.Drawing.Size(63, 16);
            this.lblFulname.TabIndex = 8;
            this.lblFulname.Text = "Fullname";
            // 
            // txtFullname
            // 
            this.txtFullname.Enabled = false;
            this.txtFullname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFullname.Location = new System.Drawing.Point(136, 253);
            this.txtFullname.Name = "txtFullname";
            this.txtFullname.Size = new System.Drawing.Size(263, 22);
            this.txtFullname.TabIndex = 3;
            // 
            // lblMSNV
            // 
            this.lblMSNV.AutoSize = true;
            this.lblMSNV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMSNV.Location = new System.Drawing.Point(51, 202);
            this.lblMSNV.Name = "lblMSNV";
            this.lblMSNV.Size = new System.Drawing.Size(47, 16);
            this.lblMSNV.TabIndex = 6;
            this.lblMSNV.Text = "MSNV";
            // 
            // txtMSNV
            // 
            this.txtMSNV.Enabled = false;
            this.txtMSNV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMSNV.Location = new System.Drawing.Point(135, 199);
            this.txtMSNV.Name = "txtMSNV";
            this.txtMSNV.Size = new System.Drawing.Size(263, 22);
            this.txtMSNV.TabIndex = 2;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.Location = new System.Drawing.Point(51, 147);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(71, 16);
            this.lblUsername.TabIndex = 3;
            this.lblUsername.Text = "Username";
            // 
            // txtUsername
            // 
            this.txtUsername.Enabled = false;
            this.txtUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsername.Location = new System.Drawing.Point(135, 144);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(263, 22);
            this.txtUsername.TabIndex = 1;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 591);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(451, 22);
            this.statusStrip1.TabIndex = 23;
            // 
            // tabPerformance
            // 
            this.tabPerformance.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPerformance.Controls.Add(this.splitContainer2);
            this.tabPerformance.Location = new System.Drawing.Point(4, 22);
            this.tabPerformance.Name = "tabPerformance";
            this.tabPerformance.Padding = new System.Windows.Forms.Padding(3);
            this.tabPerformance.Size = new System.Drawing.Size(918, 619);
            this.tabPerformance.TabIndex = 3;
            this.tabPerformance.Text = "Performance";
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.rdbNouhin);
            this.splitContainer2.Panel1.Controls.Add(this.rdbLastCheck);
            this.splitContainer2.Panel1.Controls.Add(this.rdbEntry);
            this.splitContainer2.Panel1.Controls.Add(this.rdbCheck);
            this.splitContainer2.Panel1.Controls.Add(this.cboTrungTam);
            this.splitContainer2.Panel1.Controls.Add(this.label4);
            this.splitContainer2.Panel1.Controls.Add(this.prgbExcel);
            this.splitContainer2.Panel1.Controls.Add(this.btnSave);
            this.splitContainer2.Panel1.Controls.Add(this.btnView);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer5);
            this.splitContainer2.Size = new System.Drawing.Size(912, 613);
            this.splitContainer2.SplitterDistance = 95;
            this.splitContainer2.TabIndex = 0;
            // 
            // rdbNouhin
            // 
            this.rdbNouhin.AutoSize = true;
            this.rdbNouhin.Location = new System.Drawing.Point(345, 10);
            this.rdbNouhin.Name = "rdbNouhin";
            this.rdbNouhin.Size = new System.Drawing.Size(59, 17);
            this.rdbNouhin.TabIndex = 61;
            this.rdbNouhin.Text = "Nouhin";
            this.rdbNouhin.UseVisualStyleBackColor = true;
            // 
            // rdbLastCheck
            // 
            this.rdbLastCheck.AutoSize = true;
            this.rdbLastCheck.Location = new System.Drawing.Point(229, 12);
            this.rdbLastCheck.Name = "rdbLastCheck";
            this.rdbLastCheck.Size = new System.Drawing.Size(75, 17);
            this.rdbLastCheck.TabIndex = 4;
            this.rdbLastCheck.Text = "Lastcheck";
            this.rdbLastCheck.UseVisualStyleBackColor = true;
            // 
            // rdbEntry
            // 
            this.rdbEntry.AutoSize = true;
            this.rdbEntry.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.rdbEntry.Location = new System.Drawing.Point(20, 12);
            this.rdbEntry.Name = "rdbEntry";
            this.rdbEntry.Size = new System.Drawing.Size(49, 17);
            this.rdbEntry.TabIndex = 2;
            this.rdbEntry.Text = "Entry";
            this.rdbEntry.UseVisualStyleBackColor = true;
            // 
            // rdbCheck
            // 
            this.rdbCheck.AutoSize = true;
            this.rdbCheck.Location = new System.Drawing.Point(96, 12);
            this.rdbCheck.Name = "rdbCheck";
            this.rdbCheck.Size = new System.Drawing.Size(77, 17);
            this.rdbCheck.TabIndex = 3;
            this.rdbCheck.Text = "Check, QC";
            this.rdbCheck.UseVisualStyleBackColor = true;
            // 
            // cboTrungTam
            // 
            this.cboTrungTam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cboTrungTam.DisplayMember = "trungtam";
            this.cboTrungTam.FormattingEnabled = true;
            this.cboTrungTam.Location = new System.Drawing.Point(96, 67);
            this.cboTrungTam.Name = "cboTrungTam";
            this.cboTrungTam.Size = new System.Drawing.Size(108, 21);
            this.cboTrungTam.TabIndex = 60;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 16);
            this.label4.TabIndex = 59;
            this.label4.Text = "Trung tâm";
            // 
            // prgbExcel
            // 
            this.prgbExcel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.prgbExcel.Location = new System.Drawing.Point(411, 77);
            this.prgbExcel.Name = "prgbExcel";
            this.prgbExcel.Size = new System.Drawing.Size(494, 11);
            this.prgbExcel.TabIndex = 42;
            this.prgbExcel.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSave.BackgroundImage")));
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Location = new System.Drawing.Point(281, 33);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(59, 56);
            this.btnSave.TabIndex = 1;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnView
            // 
            this.btnView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnView.Location = new System.Drawing.Point(345, 33);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(59, 56);
            this.btnView.TabIndex = 0;
            this.btnView.Text = "View";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // splitContainer5
            // 
            this.splitContainer5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.grThongke);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.grcPerformance);
            this.splitContainer5.Size = new System.Drawing.Size(912, 514);
            this.splitContainer5.SplitterDistance = 304;
            this.splitContainer5.TabIndex = 0;
            // 
            // grThongke
            // 
            this.grThongke.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.RelationName = "Level1";
            this.grThongke.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.grThongke.Location = new System.Drawing.Point(0, 0);
            this.grThongke.MainView = this.grThongkeV;
            this.grThongke.Name = "grThongke";
            this.grThongke.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox3,
            this.repositoryItemComboBox4});
            this.grThongke.Size = new System.Drawing.Size(300, 510);
            this.grThongke.TabIndex = 38;
            this.grThongke.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grThongkeV});
            // 
            // grThongkeV
            // 
            this.grThongkeV.Appearance.ColumnFilterButton.Options.UseTextOptions = true;
            this.grThongkeV.Appearance.ColumnFilterButton.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grThongkeV.Appearance.Row.Options.UseTextOptions = true;
            this.grThongkeV.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grThongkeV.Appearance.ViewCaption.Options.UseTextOptions = true;
            this.grThongkeV.Appearance.ViewCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grThongkeV.GridControl = this.grThongke;
            this.grThongkeV.IndicatorWidth = 30;
            this.grThongkeV.Name = "grThongkeV";
            this.grThongkeV.OptionsBehavior.Editable = false;
            this.grThongkeV.OptionsFilter.DefaultFilterEditorView = DevExpress.XtraEditors.FilterEditorViewMode.VisualAndText;
            this.grThongkeV.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.grThongkeV.OptionsFilter.UseNewCustomFilterDialog = true;
            this.grThongkeV.OptionsSelection.MultiSelect = true;
            this.grThongkeV.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways;
            this.grThongkeV.OptionsView.ShowGroupPanel = false;
            this.grThongkeV.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grThongkeV_CustomDrawRowIndicator);
            this.grThongkeV.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grThongkeV_MouseUp);
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // grcPerformance
            // 
            this.grcPerformance.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode2.RelationName = "Level1";
            this.grcPerformance.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.grcPerformance.Location = new System.Drawing.Point(0, 0);
            this.grcPerformance.MainView = this.grvPerformance;
            this.grcPerformance.Name = "grcPerformance";
            this.grcPerformance.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2});
            this.grcPerformance.Size = new System.Drawing.Size(600, 510);
            this.grcPerformance.TabIndex = 1;
            this.grcPerformance.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvPerformance});
            // 
            // grvPerformance
            // 
            this.grvPerformance.GridControl = this.grcPerformance;
            this.grvPerformance.IndicatorWidth = 30;
            this.grvPerformance.Name = "grvPerformance";
            this.grvPerformance.OptionsBehavior.Editable = false;
            this.grvPerformance.OptionsCustomization.AllowColumnResizing = false;
            this.grvPerformance.OptionsCustomization.AllowFilter = false;
            this.grvPerformance.OptionsCustomization.AllowGroup = false;
            this.grvPerformance.OptionsCustomization.AllowSort = false;
            this.grvPerformance.OptionsFilter.DefaultFilterEditorView = DevExpress.XtraEditors.FilterEditorViewMode.VisualAndText;
            this.grvPerformance.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.grvPerformance.OptionsFilter.UseNewCustomFilterDialog = true;
            this.grvPerformance.OptionsSelection.MultiSelect = true;
            this.grvPerformance.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grvPerformance.OptionsView.ShowGroupPanel = false;
            this.grvPerformance.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grvPerformance_MouseUp);
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // tabFeedback
            // 
            this.tabFeedback.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabFeedback.Controls.Add(this.lblInformation);
            this.tabFeedback.Controls.Add(this.panel1);
            this.tabFeedback.Controls.Add(this.prgExcel);
            this.tabFeedback.Controls.Add(this.btnExportQC);
            this.tabFeedback.Controls.Add(this.btnExportCheck);
            this.tabFeedback.Location = new System.Drawing.Point(4, 22);
            this.tabFeedback.Name = "tabFeedback";
            this.tabFeedback.Padding = new System.Windows.Forms.Padding(3);
            this.tabFeedback.Size = new System.Drawing.Size(918, 619);
            this.tabFeedback.TabIndex = 4;
            this.tabFeedback.Text = "Feedback";
            // 
            // lblInformation
            // 
            this.lblInformation.AutoSize = true;
            this.lblInformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInformation.ForeColor = System.Drawing.Color.Blue;
            this.lblInformation.Location = new System.Drawing.Point(525, 149);
            this.lblInformation.Name = "lblInformation";
            this.lblInformation.Size = new System.Drawing.Size(0, 20);
            this.lblInformation.TabIndex = 40;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(516, 616);
            this.panel1.TabIndex = 39;
            // 
            // prgExcel
            // 
            this.prgExcel.Location = new System.Drawing.Point(524, 108);
            this.prgExcel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.prgExcel.Name = "prgExcel";
            this.prgExcel.Size = new System.Drawing.Size(256, 16);
            this.prgExcel.TabIndex = 38;
            this.prgExcel.Visible = false;
            // 
            // btnExportQC
            // 
            this.btnExportQC.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnExportQC.Location = new System.Drawing.Point(694, 5);
            this.btnExportQC.Name = "btnExportQC";
            this.btnExportQC.Size = new System.Drawing.Size(86, 59);
            this.btnExportQC.TabIndex = 37;
            this.btnExportQC.Text = "Export QC";
            this.btnExportQC.UseVisualStyleBackColor = true;
            this.btnExportQC.Click += new System.EventHandler(this.btnExportQC_Click);
            // 
            // btnExportCheck
            // 
            this.btnExportCheck.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnExportCheck.Location = new System.Drawing.Point(524, 5);
            this.btnExportCheck.Name = "btnExportCheck";
            this.btnExportCheck.Size = new System.Drawing.Size(86, 59);
            this.btnExportCheck.TabIndex = 36;
            this.btnExportCheck.Text = "Export Check";
            this.btnExportCheck.UseVisualStyleBackColor = true;
            this.btnExportCheck.Click += new System.EventHandler(this.btnExportCheck_Click);
            // 
            // tabstatus
            // 
            this.tabstatus.Controls.Add(this.splitContainer3);
            this.tabstatus.Location = new System.Drawing.Point(4, 22);
            this.tabstatus.Name = "tabstatus";
            this.tabstatus.Padding = new System.Windows.Forms.Padding(3);
            this.tabstatus.Size = new System.Drawing.Size(918, 619);
            this.tabstatus.TabIndex = 5;
            this.tabstatus.Text = "Status";
            this.tabstatus.UseVisualStyleBackColor = true;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(3, 3);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.panel3);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer6);
            this.splitContainer3.Size = new System.Drawing.Size(912, 613);
            this.splitContainer3.SplitterDistance = 43;
            this.splitContainer3.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel3.Controls.Add(this.btnviewstt);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(912, 43);
            this.panel3.TabIndex = 0;
            // 
            // btnviewstt
            // 
            this.btnviewstt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnviewstt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnviewstt.Location = new System.Drawing.Point(796, 2);
            this.btnviewstt.Name = "btnviewstt";
            this.btnviewstt.Size = new System.Drawing.Size(112, 38);
            this.btnviewstt.TabIndex = 0;
            this.btnviewstt.Text = "View";
            this.btnviewstt.UseVisualStyleBackColor = true;
            this.btnviewstt.Click += new System.EventHandler(this.btnviewstt_Click);
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this.grdstatus);
            this.splitContainer6.Size = new System.Drawing.Size(912, 566);
            this.splitContainer6.SplitterDistance = 304;
            this.splitContainer6.TabIndex = 2;
            // 
            // grdstatus
            // 
            this.grdstatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdstatus.Location = new System.Drawing.Point(0, 0);
            this.grdstatus.MainView = this.grvstatus;
            this.grdstatus.Name = "grdstatus";
            this.grdstatus.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rtxtGrid});
            this.grdstatus.Size = new System.Drawing.Size(604, 566);
            this.grdstatus.TabIndex = 1;
            this.grdstatus.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvstatus});
            // 
            // grvstatus
            // 
            this.grvstatus.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grvstatus.Appearance.HeaderPanel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvstatus.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.grvstatus.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.grvstatus.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvstatus.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.grvstatus.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvstatus.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvstatus.GridControl = this.grdstatus;
            this.grvstatus.IndicatorWidth = 30;
            this.grvstatus.Name = "grvstatus";
            this.grvstatus.OptionsBehavior.Editable = false;
            this.grvstatus.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grvstatus.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.grvstatus.OptionsSelection.EnableAppearanceHideSelection = false;
            this.grvstatus.OptionsSelection.MultiSelect = true;
            this.grvstatus.OptionsView.RowAutoHeight = true;
            this.grvstatus.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvstatus.OptionsView.ShowFooter = true;
            this.grvstatus.OptionsView.ShowGroupPanel = false;
            this.grvstatus.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.grvstatus_RowCellClick);
            this.grvstatus.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grvstatus_CustomDrawRowIndicator);
            this.grvstatus.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.grvstatus_RowCellStyle);
            // 
            // rtxtGrid
            // 
            this.rtxtGrid.Name = "rtxtGrid";
            this.rtxtGrid.ShowCaretInReadOnly = false;
            // 
            // tabData
            // 
            this.tabData.Controls.Add(this.btnSaveDate);
            this.tabData.Controls.Add(this.splitContainer7);
            this.tabData.Location = new System.Drawing.Point(4, 22);
            this.tabData.Name = "tabData";
            this.tabData.Padding = new System.Windows.Forms.Padding(3);
            this.tabData.Size = new System.Drawing.Size(918, 619);
            this.tabData.TabIndex = 6;
            this.tabData.Text = "Data";
            this.tabData.UseVisualStyleBackColor = true;
            // 
            // btnSaveDate
            // 
            this.btnSaveDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveDate.Location = new System.Drawing.Point(4, 8);
            this.btnSaveDate.Name = "btnSaveDate";
            this.btnSaveDate.Size = new System.Drawing.Size(87, 37);
            this.btnSaveDate.TabIndex = 4;
            this.btnSaveDate.Text = "Save Data";
            this.btnSaveDate.UseVisualStyleBackColor = true;
            this.btnSaveDate.Click += new System.EventHandler(this.btnSaveDate_Click);
            // 
            // splitContainer7
            // 
            this.splitContainer7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer7.Location = new System.Drawing.Point(0, 52);
            this.splitContainer7.Name = "splitContainer7";
            // 
            // splitContainer7.Panel2
            // 
            this.splitContainer7.Panel2.Controls.Add(this.grcViewData);
            this.splitContainer7.Size = new System.Drawing.Size(915, 564);
            this.splitContainer7.SplitterDistance = 232;
            this.splitContainer7.TabIndex = 3;
            // 
            // grcViewData
            // 
            this.grcViewData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grcViewData.Location = new System.Drawing.Point(4, 3);
            this.grcViewData.MainView = this.grvViewData;
            this.grcViewData.Name = "grcViewData";
            this.grcViewData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemRichTextEdit1});
            this.grcViewData.Size = new System.Drawing.Size(668, 553);
            this.grcViewData.TabIndex = 1;
            this.grcViewData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvViewData});
            // 
            // grvViewData
            // 
            this.grvViewData.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grvViewData.Appearance.HeaderPanel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvViewData.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.grvViewData.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.grvViewData.Appearance.HeaderPanel.Options.UseFont = true;
            this.grvViewData.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.grvViewData.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.grvViewData.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvViewData.GridControl = this.grcViewData;
            this.grvViewData.IndicatorWidth = 30;
            this.grvViewData.Name = "grvViewData";
            this.grvViewData.OptionsBehavior.Editable = false;
            this.grvViewData.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grvViewData.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.grvViewData.OptionsSelection.EnableAppearanceHideSelection = false;
            this.grvViewData.OptionsSelection.MultiSelect = true;
            this.grvViewData.OptionsView.RowAutoHeight = true;
            this.grvViewData.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grvViewData.OptionsView.ShowFooter = true;
            this.grvViewData.OptionsView.ShowGroupPanel = false;
            // 
            // repositoryItemRichTextEdit1
            // 
            this.repositoryItemRichTextEdit1.Name = "repositoryItemRichTextEdit1";
            this.repositoryItemRichTextEdit1.ShowCaretInReadOnly = false;
            // 
            // tabBatchHide
            // 
            this.tabBatchHide.Controls.Add(this.grcBatchHide);
            this.tabBatchHide.Location = new System.Drawing.Point(4, 22);
            this.tabBatchHide.Name = "tabBatchHide";
            this.tabBatchHide.Padding = new System.Windows.Forms.Padding(3);
            this.tabBatchHide.Size = new System.Drawing.Size(918, 619);
            this.tabBatchHide.TabIndex = 7;
            this.tabBatchHide.Text = "Batchs hide";
            this.tabBatchHide.UseVisualStyleBackColor = true;
            // 
            // grcBatchHide
            // 
            this.grcBatchHide.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode3.RelationName = "Level1";
            this.grcBatchHide.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode3});
            this.grcBatchHide.Location = new System.Drawing.Point(3, 3);
            this.grcBatchHide.MainView = this.grvBatchHide;
            this.grcBatchHide.Name = "grcBatchHide";
            this.grcBatchHide.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox5,
            this.repositoryItemComboBox6});
            this.grcBatchHide.Size = new System.Drawing.Size(912, 613);
            this.grcBatchHide.TabIndex = 39;
            this.grcBatchHide.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvBatchHide});
            // 
            // grvBatchHide
            // 
            this.grvBatchHide.Appearance.ColumnFilterButton.Options.UseTextOptions = true;
            this.grvBatchHide.Appearance.ColumnFilterButton.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvBatchHide.Appearance.Row.Options.UseTextOptions = true;
            this.grvBatchHide.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvBatchHide.Appearance.ViewCaption.Options.UseTextOptions = true;
            this.grvBatchHide.Appearance.ViewCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grvBatchHide.GridControl = this.grcBatchHide;
            this.grvBatchHide.IndicatorWidth = 30;
            this.grvBatchHide.Name = "grvBatchHide";
            this.grvBatchHide.OptionsBehavior.Editable = false;
            this.grvBatchHide.OptionsFilter.DefaultFilterEditorView = DevExpress.XtraEditors.FilterEditorViewMode.VisualAndText;
            this.grvBatchHide.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.grvBatchHide.OptionsFilter.UseNewCustomFilterDialog = true;
            this.grvBatchHide.OptionsSelection.MultiSelect = true;
            this.grvBatchHide.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways;
            this.grvBatchHide.OptionsView.ShowGroupPanel = false;
            this.grvBatchHide.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grvBatchHide_MouseUp);
            // 
            // repositoryItemComboBox5
            // 
            this.repositoryItemComboBox5.Name = "repositoryItemComboBox5";
            // 
            // repositoryItemComboBox6
            // 
            this.repositoryItemComboBox6.Name = "repositoryItemComboBox6";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "ADMIN.png");
            this.imageList1.Images.SetKeyName(1, "CHECK.png");
            this.imageList1.Images.SetKeyName(2, "ENTRY.png");
            this.imageList1.Images.SetKeyName(3, "LC.png");
            this.imageList1.Images.SetKeyName(4, "CROP.png");
            // 
            // bwExportExcel
            // 
            this.bwExportExcel.WorkerReportsProgress = true;
            this.bwExportExcel.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwExportExcel_DoWork);
            this.bwExportExcel.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwExportExcel_ProgressChanged);
            this.bwExportExcel.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwExportExcel_RunWorkerCompleted);
            // 
            // bwExportExcelQC
            // 
            this.bwExportExcelQC.WorkerReportsProgress = true;
            this.bwExportExcelQC.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwExportExcelQC_DoWork);
            this.bwExportExcelQC.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwExportExcel_ProgressChanged);
            this.bwExportExcelQC.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwExportExcel_RunWorkerCompleted);
            // 
            // bgwXem
            // 
            this.bgwXem.WorkerReportsProgress = true;
            this.bgwXem.WorkerSupportsCancellation = true;
            this.bgwXem.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwXem_DoWork);
            this.bgwXem.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwXem_ProgressChanged);
            this.bgwXem.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwXem_RunWorkerCompleted);
            // 
            // frmAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(930, 647);
            this.Controls.Add(this.Status);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmAdmin";
            this.Text = "VCB Admin";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VCB_Admin_FormClosing);
            this.Load += new System.EventHandler(this.VCB_Admin_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAdmin_KeyDown);
            this.Status.ResumeLayout(false);
            this.tabUser.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.tabPerformance.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grThongke)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grThongkeV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcPerformance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvPerformance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            this.tabFeedback.ResumeLayout(false);
            this.tabFeedback.PerformLayout();
            this.tabstatus.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
            this.splitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdstatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvstatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtxtGrid)).EndInit();
            this.tabData.ResumeLayout(false);
            this.splitContainer7.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).EndInit();
            this.splitContainer7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvViewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRichTextEdit1)).EndInit();
            this.tabBatchHide.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grcBatchHide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvBatchHide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem quanlyuer;
        private System.Windows.Forms.SaveFileDialog SaveFD;
        private System.Windows.Forms.TabControl Status;
        private System.Windows.Forms.TabPage tabUser;
        private System.Windows.Forms.TabPage tabPerformance;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label lblMSNV;
        private System.Windows.Forms.TextBox txtMSNV;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cboGroup;
        private System.Windows.Forms.Label lblGroup;
        private System.Windows.Forms.ComboBox cboPair;
        private System.Windows.Forms.Label lblPair;
        private System.Windows.Forms.ComboBox cboLevel;
        private System.Windows.Forms.Label lblLevel;
        private System.Windows.Forms.ComboBox cboRole;
        private System.Windows.Forms.Label lblRole;
        private System.Windows.Forms.Label lblFulname;
        private System.Windows.Forms.TextBox txtFullname;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TabPage tabFeedback;
        private System.Windows.Forms.ProgressBar prgExcel;
        private System.Windows.Forms.Button btnExportQC;
        private System.Windows.Forms.Button btnExportCheck;
        private System.Windows.Forms.ComboBox cboFullname;
        private System.ComponentModel.BackgroundWorker bwExportExcel;
        private System.ComponentModel.BackgroundWorker bwExportExcelQC;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.RadioButton rdbLastCheck;
        private System.Windows.Forms.RadioButton rdbCheck;
        private System.Windows.Forms.RadioButton rdbEntry;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnView;
        private DevExpress.XtraGrid.GridControl grcPerformance;
        private DevExpress.XtraGrid.Views.Grid.GridView grvPerformance;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private System.Windows.Forms.ProgressBar prgbExcel;
        private System.ComponentModel.BackgroundWorker bgwXem;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TabPage tabstatus;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnviewstt;
        private DevExpress.XtraGrid.GridControl grdstatus;
        private DevExpress.XtraGrid.Views.Grid.GridView grvstatus;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboTrungTam;
        private System.Windows.Forms.Button btndel;
        private System.Windows.Forms.Button btnedit;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraGrid.GridControl grdUsers;
        private DevExpress.XtraGrid.Views.Grid.GridView grvUsers;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.TextBox txtsearch;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraGrid.Columns.GridColumn UserId;
        private DevExpress.XtraGrid.Columns.GridColumn Role;
        private DevExpress.XtraGrid.Columns.GridColumn User;
        private DevExpress.XtraGrid.Columns.GridColumn Fullname;
        private DevExpress.XtraGrid.Columns.GridColumn Group;
        private DevExpress.XtraGrid.Columns.GridColumn MSNV;
        private System.Windows.Forms.Label lblID;
        private DevExpress.XtraEditors.Repository.RepositoryItemRichTextEdit rtxtGrid;
        private DevExpress.XtraGrid.GridControl grThongke;
        private DevExpress.XtraGrid.Views.Grid.GridView grThongkeV;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblInformation;
        private System.Windows.Forms.RadioButton rdbNouhin;
        private System.Windows.Forms.TabPage tabData;
        private System.Windows.Forms.Button btnSaveDate;
        private System.Windows.Forms.SplitContainer splitContainer7;
        private DevExpress.XtraGrid.GridControl grcViewData;
        private DevExpress.XtraGrid.Views.Grid.GridView grvViewData;
        private DevExpress.XtraEditors.Repository.RepositoryItemRichTextEdit repositoryItemRichTextEdit1;
        private System.Windows.Forms.TabPage tabBatchHide;
        private DevExpress.XtraGrid.GridControl grcBatchHide;
        private DevExpress.XtraGrid.Views.Grid.GridView grvBatchHide;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox6;

    }
}

