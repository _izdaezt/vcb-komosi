﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using COMExcel = Microsoft.Office.Interop.Excel;
using Microsoft.VisualBasic.FileIO;
using System.Configuration;
using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Windows.Forms.Design;
using System.Reflection;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;
using System.Text.RegularExpressions;
namespace VCB_KOMOSI
{
    public partial class frmAdmin : Form
    {
        DataSet ds = new DataSet();

        public Int32 formid, formiduser, formidngay, formidcherker;
        // string cthuc = "";     
        System.Data.DataTable dt = new System.Data.DataTable();
        System.Data.DataTable dtc = new System.Data.DataTable();
        System.Data.DataTable dtlc = new System.Data.DataTable();
        System.Data.DataTable dtResultsEntry = new System.Data.DataTable();
        int tongsoluong, soluonge1, soluonge2, slcheck, slqc;
        //private static string imgLocation = ConfigurationManager.AppSettings["LocalImage"].ToString();
        //Đường dẫn đối tượng excel
        string ph = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\tyleloitrenkytu.xls";

        #region Variable
        // Khởi động chtr Excell
        Microsoft.Office.Interop.Excel.Application exApp;
        // Thêm file temp xls
        Microsoft.Office.Interop.Excel.Workbook oBook;
        // Lấy sheet 1.
        Microsoft.Office.Interop.Excel.Worksheet oSheet;
        //class workDB      
        WorkDB_Admin workdb = new WorkDB_Admin();
        Io_Entry Io = new Io_Entry();
        System.Data.DataTable dtAllUser;
        System.Data.DataTable dtbatch = new System.Data.DataTable();
        System.Data.DataTable dtresult = new System.Data.DataTable();
        System.Data.DataTable dtstatus = new System.Data.DataTable();
        System.Data.DataTable tableall = new System.Data.DataTable();
        System.Data.DataTable dtPFM = new System.Data.DataTable();
        System.Data.DataTable dtPFMCheck = new System.Data.DataTable();
        System.Data.DataTable dtPFMLastCheck = new System.Data.DataTable();
        System.Data.DataTable dtBatchHide = new System.Data.DataTable();
        string  datestringFB = "";
        string expression;
        string batchname;
        int trangthai = 0;
        string filename = "";
        // 3 variable temp
        string typeUser = "";
        string typeGroup = "";
        public int iddetails;
        //List Batch;
        string lstTemplate;
        string lstBatchPFM;
        List<string> listBatchArray = new List<string>();
        List<string> listBatchNameArray = new List<string>();
        int rowExcel;
        int columExcel;
        string trungtam = "";
        string[] arrTemplate = new string[4] { "Kojin_LV1", "Kojin_LV3", "Zokusei  2", "Zokusei  3" };
        List<string> lst_idBatch = new List<string>();
        System.Data.DataTable dtPfm_nam1 = new System.Data.DataTable();
        System.Data.DataTable dtPfm_nam3 = new System.Data.DataTable();
        #endregion


        public frmAdmin()
        {
            InitializeComponent();
            this.CenterToScreen();
            dtPFM.Columns.Add("FullName", typeof(string));
            dtPFM.Columns.Add("Level", typeof(string));
            dtPFM.Columns.Add("Trung tâm", typeof(string));
            dtPFM.Columns.Add("Tổng ký tự", typeof(double));
            dtPFM.Columns.Add("Lỗi sai", typeof(double));
            dtPFM.Columns.Add("Tỉ lệ sai", typeof(double));
            dtPFM.Columns.Add("Thời gian", typeof(double));
            dtPFM.Columns.Add("Tốc độ", typeof(double));
            dtPFM.Columns.Add("Đảm nhận", typeof(double));
            dtPFM.Columns.Add("Tổng trường", typeof(double));
            dtPFM.Columns.Add("Số trường NG", typeof(double));
            //
            dtPFMCheck.Columns.Add("FullName", typeof(string));
            dtPFMCheck.Columns.Add("Trung tâm", typeof(string));
            dtPFMCheck.Columns.Add("Tổng trường", typeof(double));
            dtPFMCheck.Columns.Add("Thời gian", typeof(double));
            dtPFMCheck.Columns.Add("Đảm nhận", typeof(double));
            //
            dtPFMLastCheck.Columns.Add("FullName", typeof(string));
            dtPFMLastCheck.Columns.Add("Trung tâm", typeof(string));
            dtPFMLastCheck.Columns.Add("Tên Batch", typeof(string));
            dtPFMLastCheck.Columns.Add("Template", typeof(string));
            dtPFMLastCheck.Columns.Add("Số Ảnh", typeof(Int32));
            dtPFMLastCheck.Columns.Add("Thời gian", typeof(double));

            //status            
            dtResultsEntry.Columns.Add("BatchName", typeof(String));
            dtResultsEntry.Columns.Add("Template", typeof(String));
            dtResultsEntry.Columns.Add("Tổng trường", typeof(Int32));
            dtResultsEntry.Columns.Add("E1 đã nhập", typeof(Int32));
            dtResultsEntry.Columns.Add("Entry 1", typeof(string));
            dtResultsEntry.Columns.Add("E2 đã nhập", typeof(Int32));
            dtResultsEntry.Columns.Add("Entry 2", typeof(string));
            dtResultsEntry.Columns.Add("Tổng trường E3", typeof(int));
            dtResultsEntry.Columns.Add("Entry 3", typeof(string));
            dtResultsEntry.Columns.Add("Tổng trường Check", typeof(Int32));
            dtResultsEntry.Columns.Add("Tổng trường QC", typeof(Int32));
            dtResultsEntry.Columns.Add("LastCheck", typeof(string));
            dtResultsEntry.Columns.Add("CountChar", typeof(Int32));
            dtResultsEntry.Columns.Add("Nouhin", typeof(string));
            dtbatch.Columns.Add("Id", typeof(int));
            dtbatch.Columns.Add("BatchName", typeof(string));
            dtbatch.Columns.Add("Template", typeof(string));
            dtbatch.Columns.Add("Số lượng up", typeof(string));
            dtbatch.Columns.Add("Số lượng nhập", typeof(string));
            dtbatch.Columns.Add("Ngày up", typeof(string));
            dtbatch.Columns.Add("TemplateId", typeof(string));
            dtBatchHide.Columns.Add("Id", typeof(int));
            dtBatchHide.Columns.Add("BatchName", typeof(string));
            dtBatchHide.Columns.Add("Template", typeof(string));
            dtBatchHide.Columns.Add("Số lượng", typeof(string));
            dtBatchHide.Columns.Add("Ngày up", typeof(string));
        }

        //Get all User
        private void DtAllUser()
        {
            //ThanhNH 140116
            dtAllUser = new System.Data.DataTable();
            dtAllUser = workdb.daAllUser();
            //dtAllUser = dtAllUser.AsEnumerable().OrderBy(x => x.Field<string>("Role")).ThenBy(x => x.Field<string>("Username")).CopyToDataTable();
            grdUsers.DataSource = null;
            grdUsers.DataSource = dtAllUser;
            grvUsers.RowCellClick += gridView1_RowCellClick;
            grvUsers.Columns[0].Visible = false;

        }
        void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Clicks == 1)
            {
                lblID.Text = grvUsers.GetRowCellValue(e.RowHandle, "Id").ToString();
                txtUsername.Text = grvUsers.GetRowCellValue(e.RowHandle, "User").ToString();
                txtUsername.Enabled = false;
                txtFullname.Text = grvUsers.GetRowCellValue(e.RowHandle, "Fullname").ToString();
                txtMSNV.Text = grvUsers.GetRowCellValue(e.RowHandle, "MSNV").ToString();
                cboGroup.Text = grvUsers.GetRowCellValue(e.RowHandle, "Center").ToString(); ;
                cboRole.Text = grvUsers.GetRowCellValue(e.RowHandle, "role").ToString();
                cboPair.Text = grvUsers.GetRowCellValue(e.RowHandle, "Pair").ToString();
                cboLevel.Text = grvUsers.GetRowCellValue(e.RowHandle, "Lvl").ToString();
                PExitNewUser();
            }
        }
        private void VCB_Admin_Load(object sender, EventArgs e)
        {
            //CreatFileExcel();
            DtAllUser();
            rdbEntry.Checked = true;
            datestringFB = DateTime.Now.Year.ToString().Substring(2, 2) + String.Format("{0:00}", int.Parse(DateTime.Now.Month.ToString())) + String.Format("{0:00}", int.Parse(DateTime.Now.Day.ToString()));
            DataView dv = workdb.dttrungtam().AsDataView();
            dv.Sort = "trungtam";
            cboTrungTam.DataSource = dv.ToTable();
            var results = workdb.GetBatch();
            for (int i = 0; i < results.Rows.Count; i++)
            {
                var row = dtbatch.NewRow();
                row["BatchName"] = results.Rows[i][1];
                row["Id"] = results.Rows[i][0];
                row["Template"] = arrTemplate[Convert.ToInt16(results.Rows[i][2]) - 1];
                row["Số lượng up"] = results.Rows[i][4];
                if (results.Rows[i][2].ToString().Trim() == "1" && results.Rows[i][1].ToString()[0] == 'C')
                {
                    row["Số lượng nhập"] = workdb.getEntryinBatch(results.Rows[i][0] + "--" + results.Rows[i][1]);
                }
                else
                {
                    row["Số lượng nhập"] = results.Rows[i][4];
                }
                row["Ngày up"] = results.Rows[i][3];
                row["TemplateId"] = results.Rows[i][2];
                dtbatch.Rows.Add(row);
            }
            grThongke.DataSource = dtbatch;
            grThongkeV.Columns["Id"].Visible = false;
            grThongkeV.Columns["TemplateId"].Visible = false;
        }

        private void VCB_Admin_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                System.IO.File.Delete(ph);
            }
            catch
            {
            }
        }


        public static Image byteArrayToImage(byte[] byteArrayIn)
        {

            using (MemoryStream ms = new MemoryStream(byteArrayIn))
            {
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (txtUsername.Text.Trim() == "")
            { lblError.Text = "Username is empty"; return; }
            if (txtMSNV.Text.Trim() == "")
            { lblError.Text = "MSNV is empty"; return; }
            if (txtFullname.Text.Trim() == "")
            { lblError.Text = "Fullname is empty"; return; }
            if (cboRole.SelectedIndex == -1)
            { lblError.Text = "Role is incorrect"; return; }
            if (trangthai == 1)
            {
                int ktus = workdb.ktuser(txtUsername.Text.Trim());
                if (ktus == 1)
                {
                    lblTitle.Text = "User already exists";
                }
                else
                {
                    //New User
                    workdb.NewUser(txtUsername.Text.Trim(), txtFullname.Text.Trim(), cboLevel.Text, cboPair.Text, cboGroup.Text, cboRole.Text, txtMSNV.Text.Trim());
                    lblTitle.Text = "Success!!!";
                }
            }
            else if (trangthai == 2)
            {

                workdb.UpdateUser(Convert.ToInt32(lblID.Text), txtFullname.Text.Trim(), cboLevel.Text, cboPair.Text, cboGroup.Text, cboRole.Text, txtMSNV.Text.Trim());
                lblTitle.Text = "Success!!!";
            }
            dtAllUser = workdb.daAllUser();
            grdUsers.DataSource = dtAllUser;
            PExitNewUser();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (lblTitle.Text == "Find user")
            { DtAllUser(); txtUsername.Text = ""; }
            //set properties
            PExitNewUser();
        }

        //set properties when exit new user
        private void PExitNewUser()
        {
            btnOk.Visible = false; btnCancel.Visible = false;
            txtFullname.Enabled = false; txtMSNV.Enabled = false; txtUsername.Enabled = false;
            cboLevel.Enabled = false; cboPair.Enabled = false; cboRole.Enabled = false; cboGroup.Enabled = false; cboFullname.Visible = false;
        }
        //set properties when click New user or Edit user
        private void PNewUserAndEditUser()
        {
            btnOk.Visible = true; btnCancel.Visible = true;
            txtFullname.Enabled = true; txtMSNV.Enabled = true;
            cboLevel.Enabled = true; cboPair.Enabled = true; cboRole.Enabled = true; cboGroup.Enabled = true; cboFullname.Visible = false;
        }

        private void cboRole_SelectedValueChanged(object sender, EventArgs e)
        {
            if (lblTitle.Text == "Find user")
            {

                txtUsername.Text = ""; cboPair.Text = ""; cboFullname.Text = ""; cboLevel.Text = ""; cboGroup.Text = "";

                expression = "Role = '" + cboRole.Text + "'";
            }
        }

        private void cboLevel_SelectedValueChanged(object sender, EventArgs e)
        {
            if (lblTitle.Text == "Find user")
            {
                txtUsername.Text = ""; cboPair.Text = ""; cboFullname.Text = ""; cboRole.Text = ""; cboGroup.Text = "";
                expression = "Lvl = '" + cboLevel.Text + "'";
            }
        }

        private void cboPair_SelectedValueChanged(object sender, EventArgs e)
        {
            if (lblTitle.Text == "Find user")
            {
                txtUsername.Text = ""; cboLevel.Text = ""; cboFullname.Text = ""; cboRole.Text = ""; cboGroup.Text = "";
                expression = "Pair = '" + cboPair.Text + "'";
            }
        }

        private void cboGroup_SelectedValueChanged(object sender, EventArgs e)
        {
            if (lblTitle.Text == "Find user")
            {
                txtUsername.Text = ""; cboPair.Text = ""; cboFullname.Text = ""; cboRole.Text = ""; cboLevel.Text = "";
                expression = "Group = '" + cboGroup.Text + "'";
            }
        }

        private void tabControlAdmin_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (Status.SelectedTab.Name == "tabPerformance")
                this.splitContainer5.Panel1.Controls.Add(this.grThongke);
            else if (Status.SelectedTab.Name == "tabstatus")
                this.splitContainer6.Panel1.Controls.Add(this.grThongke);
            else if (Status.SelectedTab.Name == "tabFeedback")
                panel1.Controls.Add(this.grThongke);
            else if (Status.SelectedTab.Name == "tabData")
                this.splitContainer7.Panel1.Controls.Add(this.grThongke);
            else if (Status.SelectedTab.Name == "tabBatchHide")
            {
                var results = workdb.GetBatch_Hide();
                for (int i = 0; i < results.Rows.Count; i++)
                {
                    var row = dtBatchHide.NewRow();
                    row["BatchName"] = results.Rows[i][0];
                    row["Id"] = results.Rows[i][1];
                    row["Template"] = arrTemplate[Convert.ToInt16(results.Rows[i][1]) - 1];
                    row["Số lượng"] = results.Rows[i][3];
                    row["Ngày up"] = results.Rows[i][2];
                    dtBatchHide.Rows.Add(row);
                }
                grcBatchHide.DataSource = dtBatchHide;
                grvBatchHide.Columns["Id"].Visible = false;
            }
        }

        private void cboFullname_SelectedValueChanged(object sender, EventArgs e)
        {
            txtUsername.Text = ""; cboPair.Text = ""; cboGroup.Text = ""; cboRole.Text = ""; cboLevel.Text = "";
            expression = "Fullname = '" + cboFullname.Text + "'";
        }

        private void btnExportCheck_Click(object sender, EventArgs e)
        {
            SaveFileDialog svFD = new SaveFileDialog();
            svFD.RestoreDirectory = true;
            svFD.Title = "Save to file excel";
            svFD.Filter = "Excel 2007 file|*.xlsx";
            svFD.FileName = "C";
            if (svFD.ShowDialog() == DialogResult.OK)
            {

                filename = svFD.FileName;
                if (!bwExportExcel.IsBusy)
                {
                    //listBatchArray = new List<string>();
                    listBatchNameArray = new List<string>();
                    int rowbatch = grThongkeV.RowCount;
                    for (int i = 0; i < rowbatch; i++)
                    {
                        string nametable = workdb.get_NameTable(grThongkeV.GetRowCellValue(i, grThongkeV.Columns["Id"]).ToString() + "--");
                        listBatchNameArray.Add(nametable);
                    }
                    prgExcel.Value = 0;
                    prgExcel.Visible = true;
                    bwExportExcel.RunWorkerAsync();
                }

            }
        }

        private void bwExportExcel_DoWork(object sender, DoWorkEventArgs e)
        {
            int progress1 = 0;
            int row = listBatchNameArray.Count;
            BackgroundWorker worker = sender as BackgroundWorker;
            for (int a = 0; a < listBatchNameArray.Count; a++)
            {
                try
                {
                    dtresult = new System.Data.DataTable();
                    //batchId = Convert.ToInt32(listBatchArray[a]);
                    batchname = listBatchNameArray[a];
                    dtresult = workdb.dtResult(listBatchNameArray[a]);
                    if (dtresult.Rows.Count == 0)
                        continue;
                    string imagString;

                    // Khởi động chtr Excell
                    exApp = new Microsoft.Office.Interop.Excel.Application();
                    // Thêm file temp xls
                    oBook = exApp.Workbooks.Add(Microsoft.Office.Interop.Excel.XlWBATemplate.xlWBATWorksheet);
                    // Lấy sheet 1.
                    oSheet = (Microsoft.Office.Interop.Excel.Worksheet)oBook.Worksheets[1];

                    oSheet.Cells[1, 1].Value2 = "Batch : " + batchname;
                    oSheet.Cells[1, 1].Font.Size = 24;
                    oSheet.Range["A1:I1"].Merge();
                    oSheet.Cells[3, 1].Value2 = "STT";
                    oSheet.Columns["A", Type.Missing].ColumnWidth = 5;
                    oSheet.Cells[3, 2].Value2 = "Tên ảnh";
                    oSheet.Columns["B", Type.Missing].ColumnWidth = 20;
                    oSheet.Cells[3, 3].Value2 = "Ảnh";
                    oSheet.Columns["C", Type.Missing].ColumnWidth = 42;
                    oSheet.Cells[3, 4].Value2 = "Entry1";
                    oSheet.Columns["D", Type.Missing].ColumnWidth = 10;
                    oSheet.Columns["E", Type.Missing].Font.Color = Color.Red;
                    oSheet.Cells[3, 5].Value2 = "Nội dung nhập 1";
                    oSheet.Cells[3, 5].Font.Color = Color.Black;
                    oSheet.Columns["E", Type.Missing].ColumnWidth = 20;
                    oSheet.Cells[3, 6].Value2 = "Lỗi sai 1";
                    oSheet.Columns["F", Type.Missing].ColumnWidth = 10;
                    oSheet.Cells[3, 7].Value2 = "Entry2+Entry3";
                    oSheet.Columns["G", Type.Missing].ColumnWidth = 10;
                    oSheet.Columns["H", Type.Missing].Font.Color = Color.Red;
                    oSheet.Cells[3, 8].Value2 = "Nội dung nhập 2+3";
                    oSheet.Cells[3, 8].Font.Color = Color.Black;
                    oSheet.Columns["H", Type.Missing].ColumnWidth = 20;
                    oSheet.Cells[3, 9].Value2 = "Lỗi sai 2+3";
                    oSheet.Columns["I", Type.Missing].ColumnWidth = 10;
                    oSheet.Cells[3, 10].Value2 = "Checker";
                    oSheet.Columns["J", Type.Missing].ColumnWidth = 10;
                    oSheet.Cells[3, 11].Value2 = "Nội dung chọn";
                    oSheet.Columns["K", Type.Missing].ColumnWidth = 20;
                    oSheet.Range["A3:K3"].Font.Bold = true;


                    string rwcow = (dtresult.Rows.Count + 3).ToString();
                    oSheet.Range["A3:K" + rwcow].Borders[XlBordersIndex.xlInsideHorizontal].LineStyle = XlLineStyle.xlContinuous;
                    oSheet.Range["A3:K" + rwcow].Borders[XlBordersIndex.xlInsideVertical].LineStyle = XlLineStyle.xlContinuous;
                    oSheet.Range["A3:K" + rwcow].Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
                    oSheet.Range["A3:K" + rwcow].Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
                    oSheet.Range["A3:K" + rwcow].Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;
                    oSheet.Range["A3:K" + rwcow].Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
                    oSheet.Range["A3:K" + rwcow].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                    oSheet.Range["A3:K" + rwcow].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Range["A3:K" + rwcow].WrapText = true;
                    oSheet.Range["A3:K" + rwcow].Font.Size = 13;
                    string content1 = "";
                    string content2 = "";
                    string check = "";
                    for (int i = 0; i < dtresult.Rows.Count; i++)
                    {
                        content1 = ""; content2 = ""; check = ""; rowExcel = i;
                        oSheet.Cells[i + 4, 1].Value2 = i + 1;
                        for (int j = 0; j < dtresult.Columns.Count; j++)
                        {
                            oSheet.Cells[i + 4, j + 2].Value2 = dtresult.Rows[i][j].ToString().Replace("|", Environment.NewLine);
                            if (j == 3)
                            {
                                content1 = oSheet.Cells[i + 4, j + 2].Value2;
                            }
                            if (j == 6)
                            {
                                content2 = oSheet.Cells[i + 4, j + 2].Value2;
                            }
                            if (j == 9)
                            {
                                check = oSheet.Cells[i + 4, j + 2].Value2;
                                columExcel = 5;
                                c = null;
                                c = new int[content1.Length + 1, check.Length + 1];
                                LCS(content1, check);
                                BackTrack(content1, check, content1.Length, check.Length);
                                columExcel = 8;
                                c = null;
                                c = new int[content2.Length + 1, check.Length + 1];
                                LCS(content2, check);
                                BackTrack(content2, check, content2.Length, check.Length);
                            }
                            if (j == 1)
                            {
                                imagString = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\" + "ImageCheck.jpg";
                                byte[] img = null;
                                img = workdb.GetImageBatch(batchname.Trim().Split('-')[2], dtresult.Rows[i][j - 1].ToString().Trim());
                                Bitmap bm = new Bitmap(Io_Entry.byteArrayToImage(img));
                                bm.Save(imagString, System.Drawing.Imaging.ImageFormat.Jpeg);
                                Range oRange = (Range)oSheet.Cells[i + 4, j + 2];
                                float Left = (float)((double)oRange.Left);
                                float Top = (float)((double)oRange.Top);
                                float ImageSize1 = float.Parse((bm.Width / 3).ToString());
                                float ImageSize2 = float.Parse((bm.Height / 3).ToString());
                                oSheet.Shapes.AddPicture(imagString, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoCTrue, Left, Top, ImageSize1 / 2, ImageSize2 / 2);
                                try
                                {
                                    oRange.RowHeight = ImageSize2 / 2;
                                }
                                catch
                                {
                                    oRange.RowHeight = 408;
                                }
                            }
                        }
                    }
                    exApp.Visible = false;
                    // Save file
                    oBook.Application.DisplayAlerts = false;
                    filename = Path.GetDirectoryName(filename) + @"\" + batchname + "_" + a + ".xlsx";
                    oBook.SaveAs(filename, Type.Missing, Type.Missing, Type.Missing, false, Type.Missing);
                    oBook.Close(false, false, false);
                    exApp.Quit();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oBook);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(exApp);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                progress1 = progress1 + 1;
                worker.ReportProgress(progress1 * 100 / row);
            }
        }

        private void bwExportExcel_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            prgExcel.Value = e.ProgressPercentage;
        }

        private void bwExportExcel_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                prgExcel.Value = 0;
                prgExcel.Visible = false;
                lblInformation.Text = "Error!, " + e.Error.Message.ToString();
            }
            else
            {
                lblInformation.Text = "Completed!";
                prgExcel.Value = prgExcel.Maximum;
                prgExcel.Visible = false;
            }
        }

        private void cbobatchF_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cbobatchF.SelectedIndex != -1)
            //{
            //    lblInformation.Text = "";
            //    batchId = int.Parse(dtbatch.Rows[cbobatchF.SelectedIndex]["id"].ToString());
            //    batchname = dtbatch.Rows[cbobatchF.SelectedIndex]["name"].ToString();
            //    btnExportCheck.Enabled = true; btnExportQC.Enabled = true;
            //}
        }

        private void btnExportQC_Click(object sender, EventArgs e)
        {
            SaveFileDialog svFD = new SaveFileDialog();
            svFD.RestoreDirectory = true;
            svFD.Title = "Save to file excel";
            svFD.Filter = "Excel 2007 file|*.xlsx";
            svFD.FileName = batchname + "_QC";
            if (svFD.ShowDialog() == DialogResult.OK)
            {

                filename = svFD.FileName;
                if (!bwExportExcelQC.IsBusy)
                {
                    //listBatchArray = new List<string>();
                    listBatchNameArray = new List<string>();
                    int rowbatch = grThongkeV.RowCount;
                    for (int i = 0; i < rowbatch; i++)
                    {
                        string nametable = workdb.get_NameTable(grThongkeV.GetRowCellValue(i, grThongkeV.Columns["Id"]).ToString() + "--");
                        listBatchNameArray.Add(nametable);
                    }
                    prgExcel.Value = 0;
                    prgExcel.Visible = true;
                    bwExportExcelQC.RunWorkerAsync();
                }

            }
        }

        private void bwExportExcelQC_DoWork(object sender, DoWorkEventArgs e)
        {
            int progress1 = 0;
            int row = listBatchNameArray.Count;
            BackgroundWorker worker = sender as BackgroundWorker;
            for (int a = 0; a < listBatchNameArray.Count; a++)
            {
                try
                {
                    dtresult = new System.Data.DataTable();
                    //  batchId = Convert.ToInt32(listBatchArray[a]);
                    batchname = listBatchNameArray[a];
                    dtresult = workdb.dtResultQA(listBatchNameArray[a]);
                    if (dtresult.Rows.Count == 0)
                        continue;
                    string imagString;

                    // Khởi động chtr Excell
                    Microsoft.Office.Interop.Excel.Application exApp = new Microsoft.Office.Interop.Excel.Application();
                    // Thêm file temp xls
                    Microsoft.Office.Interop.Excel.Workbook oBook = exApp.Workbooks.Add(Microsoft.Office.Interop.Excel.XlWBATemplate.xlWBATWorksheet);
                    // Lấy sheet 1.
                    Microsoft.Office.Interop.Excel.Worksheet oSheet = (Microsoft.Office.Interop.Excel.Worksheet)oBook.Worksheets[1];

                    oSheet.Cells[1, 1].Value2 = "Batch : " + batchname;
                    oSheet.Cells[1, 1].Font.Size = 24;
                    oSheet.Range["A1:I1"].Merge();
                    oSheet.Cells[3, 1].Value2 = "STT";
                    oSheet.Columns["A", Type.Missing].ColumnWidth = 5;
                    oSheet.Cells[3, 2].Value2 = "Tên ảnh";
                    oSheet.Columns["B", Type.Missing].ColumnWidth = 20;
                    oSheet.Cells[3, 3].Value2 = "Ảnh";
                    oSheet.Columns["C", Type.Missing].ColumnWidth = 42;
                    oSheet.Cells[3, 4].Value2 = "Entry1";
                    oSheet.Columns["D", Type.Missing].ColumnWidth = 10;
                    oSheet.Cells[3, 5].Value2 = "Nội dung nhập 1";
                    oSheet.Columns["E", Type.Missing].ColumnWidth = 20;
                    oSheet.Cells[3, 6].Value2 = "Entry2 + Entry3";
                    oSheet.Columns["F", Type.Missing].ColumnWidth = 10;
                    oSheet.Cells[3, 7].Value2 = "Nội dung nhập 2+3";
                    oSheet.Columns["G", Type.Missing].ColumnWidth = 20;
                    oSheet.Cells[3, 8].Value2 = "Checker";
                    oSheet.Columns["H", Type.Missing].ColumnWidth = 10;
                    oSheet.Cells[3, 9].Value2 = "Nội dung chọn";
                    oSheet.Columns["I", Type.Missing].ColumnWidth = 20;
                    oSheet.Range["A3:I3"].Font.Bold = true;


                    string rwcow = Convert.ToString(row + 3);
                    oSheet.Range["A3:I" + rwcow].Borders[XlBordersIndex.xlInsideHorizontal].LineStyle = XlLineStyle.xlContinuous;
                    oSheet.Range["A3:I" + rwcow].Borders[XlBordersIndex.xlInsideVertical].LineStyle = XlLineStyle.xlContinuous;
                    oSheet.Range["A3:I" + rwcow].Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
                    oSheet.Range["A3:I" + rwcow].Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
                    oSheet.Range["A3:I" + rwcow].Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;
                    oSheet.Range["A3:I" + rwcow].Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
                    oSheet.Range["A3:I" + rwcow].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                    oSheet.Range["A3:I" + rwcow].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Range["A3:I" + rwcow].WrapText = true;
                    oSheet.Range["A3:I" + rwcow].Font.Size = 13;

                    for (int i = 0; i < dtresult.Rows.Count; i++)
                    {

                        oSheet.Cells[i + 4, 1].Value2 = i + 1;
                        for (int j = 0; j < dtresult.Columns.Count; j++)
                        {
                            oSheet.Cells[i + 4, j + 2].Value2 = dtresult.Rows[i][j].ToString().Replace("|", Environment.NewLine);
                            if (j == 1)
                            {
                                imagString = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\" + "ImageQC.jpg";
                                byte[] img = null;
                                img = workdb.GetImageBatch(batchname.Trim().Split('-')[2], dtresult.Rows[i][j - 1].ToString().Trim());
                                Bitmap bm = new Bitmap(Io_Entry.byteArrayToImage(img));
                                bm.Save(imagString, System.Drawing.Imaging.ImageFormat.Jpeg);
                                Range oRange = (Range)oSheet.Cells[i + 4, j + 2];
                                float Left = (float)((double)oRange.Left);
                                float Top = (float)((double)oRange.Top);
                                float ImageSize1 = float.Parse((bm.Width / 3).ToString());
                                float ImageSize2 = float.Parse((bm.Height / 3).ToString());
                                oSheet.Shapes.AddPicture(imagString, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoCTrue, Left, Top, ImageSize1 / 2, ImageSize2 / 2);
                                try
                                {
                                    oRange.RowHeight = ImageSize2 / 2;
                                }
                                catch { oRange.RowHeight = 408; }

                            }
                        }
                    }
                    exApp.Visible = false;
                    // Save file
                    oBook.Application.DisplayAlerts = false;
                    filename = Path.GetDirectoryName(filename) + @"\" + batchname + "_" + a + "_QC";
                    oBook.SaveAs(filename, Type.Missing, Type.Missing, Type.Missing, false, Type.Missing);
                    oBook.Close(false, false, false);
                    exApp.Quit();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oBook);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(exApp);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                progress1 = progress1 + 1;
                worker.ReportProgress(progress1 * 100 / row);
            }
        }



        private void txtPhieu_KeyDown(object sender, KeyEventArgs e)
        {
            //noncharacter = false;
            //var aa = (char)e.KeyCode;
            //if (e. && e.KeyValue != 8)
            //    noncharacter = true;
        }

        private void txtPhieu_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char.IsLetter)(e.KeyChar))
                e.Handled = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (grvPerformance.Columns.Count > 0)
            {
                using (SaveFileDialog saveDialog = new SaveFileDialog())
                {
                    saveDialog.FileName = "";
                    saveDialog.Filter = "Excel (2007) (.xlsx)|*.xlsx";
                    if (saveDialog.ShowDialog() != DialogResult.Cancel)
                    {
                        grvPerformance.ClearColumnsFilter();
                        grvPerformance.ClearSorting();
                        grvPerformance.ClearGrouping();
                        grvPerformance.ClearSelection();
                        grvPerformance.FindFilterText = "";
                        string exportFilePath = saveDialog.FileName;
                        string fileExtenstion = new FileInfo(exportFilePath).Extension;
                        //NImageExporter imageExporter = chartControl.ImageExporter;
                        grvPerformance.ExportToXlsx(exportFilePath);
                    }
                }
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            lst_idBatch.Clear();
            dtPFM.Clear();
            dtPFMCheck.Clear();
            dtPFMLastCheck.Clear();
            if (grThongkeV.RowCount == 0)
            {
                MessageBox.Show("Bạn chưa chọn Batch");
                return;
            }
            if (rdbEntry.Checked)
                typeUser = "ENTRY";
            else if (rdbCheck.Checked)
                typeUser = "CHECK";
            else if (rdbNouhin.Checked)
                typeUser = "NOUHIN";
            else
                typeUser = "LASTCHECK";
            lstBatchPFM = "N'";
            lstTemplate = "";
            if (!bgwXem.IsBusy)
            {
                dtPFM.Rows.Clear();
                prgbExcel.Value = 0;
                prgbExcel.Visible = true;
                dt = new System.Data.DataTable();
                grcPerformance.DataSource = null;
                grvPerformance.Columns.Clear();
                for (int i = 0; i < grThongkeV.RowCount; i++)
                {
                    string idtemp = grThongkeV.GetRowCellValue(i, grThongkeV.Columns["Id"]).ToString();
                    lstBatchPFM = lstBatchPFM + grThongkeV.GetRowCellValue(i, grThongkeV.Columns["BatchName"]).ToString() + "',N'";
                    lstTemplate = (lstTemplate.Contains(idtemp) ? lstTemplate : lstTemplate + idtemp + ",");
                    lst_idBatch.Add(idtemp);
                }
                lstBatchPFM = lstBatchPFM.Substring(0, lstBatchPFM.Length - 3);
                lstTemplate = lstTemplate.TrimEnd(',');
                trungtam = cboTrungTam.Text;
                if (lstBatchPFM != "")
                {
                    bgwXem.RunWorkerAsync();
                }
            }
        }

        private void bgwXem_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            #region ENTRY
            if (typeUser == "ENTRY")
            {
                dtPfm_nam1.Clear();
                dtPfm_nam3.Clear();
                for (int i = 0; i < lst_idBatch.Count; i++)
                {
                    System.Data.DataTable dt_temp = new System.Data.DataTable();
                    string nametable = workdb.get_NameTable(lst_idBatch[i] + "--");
                    if (nametable.IndexOf("lv3") == -1)
                        for (int j = 0; j < 3; j++)
                        {
                            dt_temp = workdb.get_alltable(nametable, j + 1);
                            if (dtPfm_nam1.Rows.Count < 1)
                            {
                                dtPfm_nam1 = dt_temp;
                            }
                            else
                                dtPfm_nam1.Merge(dt_temp);
                        }
                    else
                        for (int j = 0; j < 3; j++)
                        {
                            dt_temp = workdb.get_alltable(nametable, j + 1);
                            if (dtPfm_nam3.Rows.Count < 1)
                            {
                                dtPfm_nam3 = dt_temp;
                            }
                            else
                                dtPfm_nam3.Merge(dt_temp);
                        }
                }
                //  dt = workdb.getPerformaceEntry(lstBatchPFM, trungtam, lstTemplate);
                for (int i = 0; i < dtPfm_nam1.Rows.Count; i++) 
                {
                  if (dtPfm_nam1.Rows[i]["Level"].ToString() != "1") 
                    {
                        dtPfm_nam1.Rows.RemoveAt(i);
                        dtPfm_nam1.AcceptChanges();
                        i--;
                    }
                }
                    dtPfm_nam1.Merge(dtPfm_nam3);
                var results = from status in dtPfm_nam1.AsEnumerable()
                              group status by new { status2 = status.Field<string>("FullName"), status3 = status.Field<int>("Level") } into status
                              select new
                              {
                                  Fullname = status.Select(x => x.Field<string>("FullName")).FirstOrDefault(),
                                  Level = status.Select(x => x.Field<int>("Level")).FirstOrDefault(),
                                  Trungtam = status.Select(x => x.Field<string>("Trung Tâm")).FirstOrDefault(),
                                  Tongkytu = status.Select(x => x.Field<int>("Tổng ký tự")).Sum(),
                                  Loisai = status.Select(x => x.Field<int>("Lỗi sai")).Sum(),
                                  Thoigian = ((status.Select(x => x.Field<int>("Thời gian")).Sum()) / 1000.0) / 60.0,
                                  Tongtruong = status.Select(x => x.Field<string>("MSNV")).Count(),
                                  NotGood = status.Select(x => x.Field<int>("NG")).Where(x => x != 0).Count(),
                              };
                double tongkt = results.Sum(x => x.Tongkytu);
                double tongls = results.Sum(x => x.Loisai);
                double tongtg = Math.Round(results.Sum(x => x.Thoigian), 2);
                double tongtruong = results.Sum(x => x.Tongtruong);
                prgbExcel.Invoke((System.Action)(() =>
                {
                    prgbExcel.Maximum = results.Count();
                }));
                foreach (var element in results)
                {
                    var rowindex = dtPFM.NewRow();
                    rowindex["FullName"] = element.Fullname;
                    rowindex["Level"] = element.Level;
                    rowindex["Trung tâm"] = element.Trungtam;
                    rowindex["Tổng ký tự"] = element.Tongkytu;
                    rowindex["Lỗi sai"] = element.Loisai;
                    rowindex["Tỉ lệ sai"] = Math.Round((Convert.ToDouble(element.Loisai) / Convert.ToDouble(element.Tongkytu)) * 100.0, 2);
                    rowindex["Thời gian"] = Math.Round(element.Thoigian, 2);
                    rowindex["Tốc độ"] = Math.Round(element.Tongkytu / element.Thoigian, 2);
                    rowindex["Đảm nhận"] = Math.Round((element.Tongkytu / tongkt) * 100, 2);
                    rowindex["Tổng trường"] = element.Tongtruong;
                    rowindex["Số trường NG"] = element.NotGood;
                    dtPFM.Rows.Add(rowindex);
                }
                DataView view = new DataView(dtPFM);
                System.Data.DataTable distinctValues = view.ToTable(true, "Trung tâm");
                DataRow newRow = dtPFM.NewRow();
                DataRow newRow1 = dtPFM.NewRow();
                DataRow newRow2 = dtPFM.NewRow();
                DataRow newRow3 = dtPFM.NewRow();
                DataRow newRow4 = dtPFM.NewRow();
                //Max,Min,average 
                newRow1[2] = "Cao nhất";
                newRow1[3] = dtPFM.Compute("Max ([Tổng ký tự])", "");
                newRow1[4] = dtPFM.Compute("Max ([Lỗi sai])", "");
                newRow1[5] = dtPFM.Compute("Max ([Tỉ lệ sai])", "");
                newRow1[6] = dtPFM.Compute("Max ([Thời gian])", "");
                newRow1[7] = dtPFM.Compute("Max ([Tốc độ])", "");
                newRow1[8] = dtPFM.Compute("Max ([Đảm nhận])", "");
                newRow1[9] = dtPFM.Compute("Max ([Tổng trường])", "");
                newRow1[10] = dtPFM.Compute("Max ([Số trường NG])", "");
                newRow2[2] = "Thấp nhất";
                newRow2[3] = dtPFM.Compute("Min ([Tổng ký tự])", "");
                newRow2[4] = dtPFM.Compute("Min ([Lỗi sai])", "");
                newRow2[5] = dtPFM.Compute("Min ([Tỉ lệ sai])", "");
                newRow2[6] = dtPFM.Compute("Min ([Thời gian])", "");
                newRow2[7] = dtPFM.Compute("Min ([Tốc độ])", "");
                newRow2[8] = dtPFM.Compute("Min ([Đảm nhận])", "");
                newRow2[9] = dtPFM.Compute("Min ([Tổng trường])", "");
                newRow2[10] = dtPFM.Compute("Min ([Số trường NG])", "");
                try
                {
                    newRow3[2] = "Trung bình";
                    newRow3[3] = Math.Round(double.Parse(dtPFM.Compute("Avg ([Tổng ký tự])", "").ToString()), 0);
                    newRow3[4] = Math.Round(double.Parse(dtPFM.Compute("Avg ([Lỗi sai])", "").ToString()), 0);
                    newRow3[5] = Math.Round(double.Parse(dtPFM.Compute("Avg ([Tỉ lệ sai])", "").ToString()), 2);
                    newRow3[6] = Math.Round(double.Parse(dtPFM.Compute("Avg ([Thời gian])", "").ToString()), 0);
                    newRow3[7] = Math.Round(double.Parse(dtPFM.Compute("Avg ([Tốc độ])", "").ToString()), 0);
                    newRow3[8] = Math.Round(double.Parse(dtPFM.Compute("Avg ([Đảm nhận])", "").ToString()), 0);
                    newRow3[9] = Math.Round(double.Parse(dtPFM.Compute("Avg ([Tổng trường])", "").ToString()), 0);
                    newRow3[10] = Math.Round(double.Parse(dtPFM.Compute("Avg ([Số trường NG])", "").ToString()), 0);
                }
                catch
                {
                }
                dtPFM.Rows.Add(newRow);
                dtPFM.Rows.Add(newRow1);
                dtPFM.Rows.Add(newRow2);
                dtPFM.Rows.Add(newRow3);
                if (trungtam == "ALL")
                {
                    DataRow newRow5 = dtPFM.NewRow();
                    DataRow newRow7 = dtPFM.NewRow();
                    dtPFM.Rows.Add(newRow5);
                    newRow7[2] = "ALL";
                    newRow7[3] = tongkt;
                    newRow7[4] = tongls;
                    newRow7[5] = Math.Round((Convert.ToDouble(tongls) / Convert.ToDouble(tongkt)) * 100.0, 2); //tỉ lệ sai
                    newRow7[6] = tongtg;
                    newRow7[7] = Math.Round(Convert.ToDouble(tongkt) / Convert.ToDouble(tongtg), 2);
                    newRow7[8] = "100";
                    newRow7[9] = tongtruong;
                    newRow7[10] = dtPFM.Compute("Sum ([Số trường NG])", "Level>0");
                    dtPFM.Rows.Add(newRow7);
                    for (int i = 0; i < distinctValues.Rows.Count; i++)
                    {
                        string tt = distinctValues.Rows[i]["Trung tâm"].ToString();
                        DataRow newRows = dtPFM.NewRow();
                        string tkuGroup = dtPFM.Compute("Sum ([Tổng ký tự])", "[Trung tâm] = '" + tt + "'").ToString();
                        string timeGroup = dtPFM.Compute("Sum ([Thời gian])", "[Trung tâm] ='" + tt + "'").ToString();
                        double tlsGroup = double.Parse(dtPFM.Compute("Sum ([Lỗi sai])", "[Trung tâm] = '" + tt + "'").ToString());
                        double tongtruongdn = double.Parse(dtPFM.Compute("Sum ([Tổng trường])", "[Trung tâm] = '" + tt + "'").ToString());
                        newRows[2] = tt;
                        newRows[3] = tkuGroup;
                        newRows[4] = tlsGroup;
                        newRows[5] = Math.Round((Convert.ToDouble(tlsGroup) / Convert.ToDouble(tkuGroup)) * 100.0, 2); //tỉ lệ sai
                        newRows[6] = timeGroup; //thời gian
                        newRows[7] = Math.Round(Convert.ToDouble(tkuGroup) / Convert.ToDouble(timeGroup), 2);     //tốc độ
                        newRows[8] = Math.Round((Convert.ToDouble(tongtruongdn) * 100.0 / Convert.ToInt32(tongtruong)), 2);
                        newRows[9] = tongtruongdn;
                        newRows[10] = dtPFM.Compute("SUM ([Số trường NG])", "[Trung tâm] = '" + tt + "'");
                        dtPFM.Rows.Add(newRows);
                    }
                }
                else
                {
                    DataRow newRow5 = dtPFM.NewRow();
                    DataRow newRow6 = dtPFM.NewRow();
                    DataRow newRow7 = dtPFM.NewRow();
                    dtPFM.Rows.Add(newRow5);
                    dtPFM.Rows.Add(newRow6);
                    newRow7[2] = typeGroup;
                    newRow7[3] = tongkt;
                    newRow7[4] = tongls;
                    newRow7[5] = tongkt - tongls;
                    newRow7[6] = tongtg;
                    newRow7[9] = dtPFM.Compute("SUM ([Tổng trường])", "Level>0");
                    newRow7[10] = dtPFM.Compute("SUM ([Số trường NG])", "Level>0");
                    dtPFM.Rows.Add(newRow7);
                }
                worker.ReportProgress(dtPFM.Rows.Count);
            }
            #endregion
            #region CHECK
            else if (typeUser == "CHECK")
            {
                System.Data.DataTable dtQC = new System.Data.DataTable();
                System.Data.DataTable dtcheck = new System.Data.DataTable();
                for (int i = 0; i < lst_idBatch.Count; i++)
                {
                    System.Data.DataTable dt_temp = new System.Data.DataTable();
                    string nametable = workdb.get_NameTable(lst_idBatch[i] + "--");
                    if (nametable.IndexOf("lv3") == -1)
                    {
                        dt_temp = workdb.get_alltable_check(nametable, "Checker", "Check");
                        if (dtcheck.Rows.Count < 1)
                        {
                            dtcheck = dt_temp;
                        }
                        else
                            dtcheck.Merge(dt_temp);
                    }
                    else
                    {
                        dt_temp = workdb.get_alltable_check(nametable, "Checker", "Check");
                        if (dtQC.Rows.Count < 1)
                        {
                            dtQC = dt_temp;
                        }
                        else
                            dtQC.Merge(dt_temp);
                    }
                }
                //  dt = workdb.getPerformaceEntry(lstBatchPFM, trungtam, lstTemplate);
                dtcheck.Merge(dtQC);


                dtc.Clear();
                dtc = dtcheck;//workdb.All_Check(lstBatchPFM, trungtam, lstTemplate);
                var results = from status in dtc.AsEnumerable()
                              group status by (status.Field<string>("MSNV")) into status
                              select new
                              {
                                  Fullname = status.Select(x => x.Field<string>("FullName")).FirstOrDefault(),
                                  Trungtam = status.Select(x => x.Field<string>("Trung Tâm")).FirstOrDefault(),
                                  Tongtruong = status.Select(x => x.Field<string>("MSNV")).Count(),
                                  Thoigian = (status.Select(x => x.Field<int>("TimeMilisecondsCheck")).Sum()),
                              };
                double tt = results.Sum(x => x.Tongtruong);
                prgbExcel.Invoke((System.Action)(() =>
                {
                    prgbExcel.Maximum = results.Count();
                }));
                foreach (var element in results)
                {
                    var rowcheck1 = dtPFMCheck.NewRow();
                    rowcheck1["FullName"] = element.Fullname;
                    rowcheck1["Trung tâm"] = element.Trungtam;
                    rowcheck1["Tổng trường"] = element.Tongtruong;
                    rowcheck1["Thời gian"] = Math.Round(element.Thoigian / 60000.0, 2);
                    rowcheck1["Đảm nhận"] = Math.Round(element.Tongtruong / tt * 100, 2);
                    dtPFMCheck.Rows.Add(rowcheck1);
                }
                DataView view = new DataView(dtPFMCheck);
                System.Data.DataTable distinctValues = view.ToTable(true, "Trung tâm");
                DataRow newRowCheck = dtPFMCheck.NewRow();
                DataRow newRowCheck1 = dtPFMCheck.NewRow();
                DataRow newRowCheck2 = dtPFMCheck.NewRow();
                DataRow newRowCheck3 = dtPFMCheck.NewRow();
                DataRow newRowCheck4 = dtPFMCheck.NewRow();
                //Max,Min,average 
                newRowCheck1[1] = "Cao nhất";
                newRowCheck1[2] = dtPFMCheck.Compute("Max ([Tổng trường])", "");
                newRowCheck1[3] = dtPFMCheck.Compute("Max ([Thời gian])", "");
                newRowCheck1[4] = dtPFMCheck.Compute("Max ([Đảm nhận])", "");
                newRowCheck2[1] = "Thấp nhất";
                newRowCheck2[2] = dtPFMCheck.Compute("Min ([Tổng trường])", "");
                newRowCheck2[3] = dtPFMCheck.Compute("Min ([Thời gian])", "");
                newRowCheck2[4] = dtPFMCheck.Compute("Min ([Đảm nhận])", "");
                try
                {
                    newRowCheck3[1] = "Trung bình";
                    newRowCheck3[2] = Math.Round(double.Parse(dtPFMCheck.Compute("Avg ([Tổng trường])", "").ToString()), 2);
                    newRowCheck3[3] = Math.Round(double.Parse(dtPFMCheck.Compute("Avg ([Thời gian])", "").ToString()), 2);
                    newRowCheck3[4] = Math.Round(double.Parse(dtPFMCheck.Compute("Avg ([Đảm nhận])", "").ToString()), 2);
                }
                catch
                {
                }
                dtPFMCheck.Rows.Add(newRowCheck);
                dtPFMCheck.Rows.Add(newRowCheck1);
                dtPFMCheck.Rows.Add(newRowCheck2);
                dtPFMCheck.Rows.Add(newRowCheck3);
                if (trungtam == "ALL")
                {
                    DataRow newRowCheck5 = dtPFMCheck.NewRow();
                    DataRow newRowCheck7 = dtPFMCheck.NewRow();
                    dtPFMCheck.Rows.Add(newRowCheck5);
                    newRowCheck7[1] = "ALL";
                    newRowCheck7[2] = tt;
                    newRowCheck7[3] = dtPFMCheck.Compute("Sum ([Thời gian])", "[FullName]<>''");
                    newRowCheck7[4] = "100";
                    dtPFMCheck.Rows.Add(newRowCheck7);
                    for (int i = 0; i < distinctValues.Rows.Count; i++)
                    {
                        string trtam = distinctValues.Rows[i]["Trung tâm"].ToString();
                        DataRow newRows = dtPFMCheck.NewRow();
                        string tkuGroup = dtPFMCheck.Compute("Sum ([Tổng trường])", "[Trung tâm] = '" + trtam + "'").ToString();
                        string timeGroup = dtPFMCheck.Compute("Sum ([Thời gian])", "[Trung tâm] =  '" + trtam + "'").ToString();
                        double ttgGroup = double.Parse(dtPFMCheck.Compute("Sum ([Thời gian])", "[Trung tâm] = '" + trtam + "'").ToString());
                        double tdnGroup = double.Parse(dtPFMCheck.Compute("Sum ([Đảm nhận])", "[Trung tâm] =  '" + trtam + "'").ToString());
                        newRows[1] = trtam;
                        newRows[2] = tkuGroup;
                        newRows[3] = ttgGroup;
                        newRows[4] = tdnGroup;
                        dtPFMCheck.Rows.Add(newRows);
                    }
                }
                else
                {
                    DataRow newRowCheck5 = dtPFMCheck.NewRow();
                    DataRow newRowCheck7 = dtPFMCheck.NewRow();
                    dtPFMCheck.Rows.Add(newRowCheck5);
                    newRowCheck7[1] = typeGroup;
                    newRowCheck7[2] = tt;
                    newRowCheck7[3] = dtPFMCheck.Compute("Sum ([Thời gian])", "[FullName]<>''");
                    newRowCheck7[4] = "100";
                    dtPFMCheck.Rows.Add(newRowCheck7);
                }
                worker.ReportProgress(dtPFMCheck.Rows.Count);
            }
            #endregion
            #region LASTCHECK
            else if (typeUser == "LASTCHECK")
            {

                System.Data.DataTable dtLC = new System.Data.DataTable();
                for (int i = 0; i < lst_idBatch.Count; i++)
                {
                    System.Data.DataTable dt_temp = new System.Data.DataTable();
                    string nametable = workdb.get_NameTable(lst_idBatch[i] + "--");

                    dt_temp = workdb.get_alltable_check(nametable, "LastCheck", "LC");
                    if (dtLC.Rows.Count < 1)
                    {
                        dtLC = dt_temp;
                    }
                    else
                        dtLC.Merge(dt_temp);

                }
                //  dt = workdb.getPerformaceEntry(lstBatchPFM, trungtam, lstTemplate);

                dtlc.Clear();

                dtlc = dtLC;//workdb.All_Lastcheck(lstBatchPFM, trungtam, lstTemplate);
                var results = from status in dtlc.AsEnumerable()
                              group status by new { status1 = status.Field<string>("BatchName"), status2 = status.Field<string>("FullName"), status3 = status.Field<string>("Template") } into status
                              select new
                              {
                                  BatchName = status.Select(x => x.Field<String>("BatchName")).FirstOrDefault(),
                                  Template = status.Select(x => x.Field<string>("Template")).FirstOrDefault(),
                                  Fullname = status.Select(x => x.Field<string>("FullName")).FirstOrDefault(),
                                  Trungtam = status.Select(x => x.Field<string>("Trung Tâm")).FirstOrDefault(),
                                  TimeWork = status.Select(x => x.Field<int>("TimeMilisecondsLC")).Sum()
                              };
                //double tt = results.Sum(x => x.TenBatch);                
                prgbExcel.Invoke((System.Action)(() =>
                {
                    prgbExcel.Maximum = results.Count();
                }));
                foreach (var element in results)
                {
                    var rowcheck1 = dtPFMLastCheck.NewRow();
                    rowcheck1["FullName"] = element.Fullname;
                    rowcheck1["Trung tâm"] = element.Trungtam;
                    rowcheck1["Template"] = element.Template;
                    rowcheck1["Tên Batch"] = element.BatchName;
                    rowcheck1["Số Ảnh"] = dtbatch.Select("BatchName='" + element.BatchName + "' AND Template='" + element.Template + "'")[0].ItemArray[3];
                    rowcheck1["Thời gian"] = Math.Round(element.TimeWork / 60000.0, 2);
                    dtPFMLastCheck.Rows.Add(rowcheck1);
                }
                DataView view = new DataView(dtPFMLastCheck);
                System.Data.DataTable distinctValues = view.ToTable(true, "Trung tâm");
                DataRow newRowCheck = dtPFMLastCheck.NewRow();
                DataRow newRowCheck1 = dtPFMLastCheck.NewRow();
                DataRow newRowCheck2 = dtPFMLastCheck.NewRow();
                DataRow newRowCheck3 = dtPFMLastCheck.NewRow();
                DataRow newRowCheck4 = dtPFMLastCheck.NewRow();
                //Max,Min,average 
                newRowCheck1[2] = "Cao nhất";
                newRowCheck1[4] = dtPFMLastCheck.Compute("Max ([Số Ảnh])", "");
                newRowCheck1[5] = dtPFMLastCheck.Compute("Max ([Thời gian])", "");
                newRowCheck2[2] = "Thấp nhất";
                newRowCheck2[4] = dtPFMLastCheck.Compute("Min ([Số Ảnh])", "");
                newRowCheck2[5] = dtPFMLastCheck.Compute("Min ([Thời gian])", "");
                try
                {
                    newRowCheck3[2] = "Trung bình";
                    newRowCheck3[4] = Math.Round(double.Parse(dtPFMLastCheck.Compute("Avg ([Số Ảnh])", "").ToString()), 2);
                    newRowCheck3[5] = Math.Round(double.Parse(dtPFMLastCheck.Compute("Avg ([Thời gian])", "").ToString()), 2);
                }
                catch
                {
                }
                dtPFMLastCheck.Rows.Add(newRowCheck);
                dtPFMLastCheck.Rows.Add(newRowCheck1);
                dtPFMLastCheck.Rows.Add(newRowCheck2);
                dtPFMLastCheck.Rows.Add(newRowCheck3);
                if (trungtam == "ALL")
                {
                    DataRow newRowCheck5 = dtPFMLastCheck.NewRow();
                    DataRow newRowCheck7 = dtPFMLastCheck.NewRow();
                    dtPFMLastCheck.Rows.Add(newRowCheck5);
                    newRowCheck7[2] = "ALL";
                    newRowCheck7[4] = dtPFMLastCheck.Compute("Sum ([Số Ảnh])", "[FullName]<>''");
                    newRowCheck7[5] = dtPFMLastCheck.Compute("Sum ([Thời gian])", "[FullName]<>''");
                    dtPFMLastCheck.Rows.Add(newRowCheck7);
                    for (int i = 0; i < distinctValues.Rows.Count; i++)
                    {
                        string trtam = distinctValues.Rows[i]["Trung tâm"].ToString();
                        DataRow newRows = dtPFMLastCheck.NewRow();
                        string tkuGroup = dtPFMLastCheck.Compute("Sum ([Số Ảnh])", "[Trung tâm] = '" + trtam + "'").ToString();
                        string timeGroup = dtPFMLastCheck.Compute("Sum ([Thời gian])", "[Trung tâm] =  '" + trtam + "'").ToString();
                        double ttgGroup = double.Parse(dtPFMLastCheck.Compute("Sum ([Thời gian])", "[Trung tâm] =  '" + trtam + "'").ToString());
                        newRows[2] = trtam;
                        newRows[4] = tkuGroup;
                        newRows[5] = ttgGroup;
                        dtPFMLastCheck.Rows.Add(newRows);
                    }
                }
                else
                {
                    DataRow newRowCheck5 = dtPFMLastCheck.NewRow();
                    DataRow newRowCheck7 = dtPFMLastCheck.NewRow();
                    dtPFMLastCheck.Rows.Add(newRowCheck5);
                    newRowCheck7[2] = typeGroup;
                    newRowCheck7[4] = dtPFMLastCheck.Compute("Sum ([Số Ảnh])", "");
                    newRowCheck7[5] = dtPFMLastCheck.Compute("Sum ([Thời gian])", "");
                    dtPFMLastCheck.Rows.Add(newRowCheck7);
                }
                worker.ReportProgress(dtPFMLastCheck.Rows.Count);
            }
            #endregion
            #region NOUHIN
            else
            {
                System.Data.DataTable dtNH = new System.Data.DataTable();
                for (int i = 0; i < lst_idBatch.Count; i++)
                {
                    System.Data.DataTable dt_temp = new System.Data.DataTable();
                    string nametable = workdb.get_NameTable(lst_idBatch[i] + "--");

                    dt_temp = workdb.get_alltable_NH(nametable);
                    if (dtNH.Rows.Count < 1)
                    {
                        dtNH = dt_temp;
                    }
                    else
                        dtNH.Merge(dt_temp);
                }
                dtlc.Clear();
                dtlc = dtNH;// workdb.All_NouHin(lstBatchPFM, trungtam, lstTemplate);
                var results = from status in dtlc.AsEnumerable()
                              group status by new { status1 = status.Field<string>("BatchName"), status2 = status.Field<string>("FullName"), status3 = status.Field<string>("Template") } into status
                              select new
                              {
                                  BatchName = status.Select(x => x.Field<String>("BatchName")).FirstOrDefault(),
                                  Template = status.Select(x => x.Field<string>("Template")).FirstOrDefault(),
                                  Fullname = status.Select(x => x.Field<string>("FullName")).FirstOrDefault(),
                                  Trungtam = status.Select(x => x.Field<string>("Trung Tâm")).FirstOrDefault(),
                                  TimeWork = status.Select(x => x.Field<int>("TimeNouhin")).Sum()
                              };
                //double tt = results.Sum(x => x.TenBatch);                
                prgbExcel.Invoke((System.Action)(() =>
                {
                    prgbExcel.Maximum = results.Count();
                }));
                foreach (var element in results)
                {
                    var rowcheck1 = dtPFMLastCheck.NewRow();
                    rowcheck1["FullName"] = element.Fullname;
                    rowcheck1["Trung tâm"] = element.Trungtam;
                    rowcheck1["Template"] = element.Template;
                    rowcheck1["Tên Batch"] = element.BatchName;
                    rowcheck1["Số Ảnh"] = dtbatch.Select("BatchName='" + element.BatchName + "' AND Template='" + element.Template + "'")[0].ItemArray[3];
                    rowcheck1["Thời gian"] = Math.Round(element.TimeWork / 60000.0, 2);
                    dtPFMLastCheck.Rows.Add(rowcheck1);
                }
                DataView view = new DataView(dtPFMLastCheck);
                System.Data.DataTable distinctValues = view.ToTable(true, "Trung tâm");
                DataRow newRowCheck = dtPFMLastCheck.NewRow();
                DataRow newRowCheck1 = dtPFMLastCheck.NewRow();
                DataRow newRowCheck2 = dtPFMLastCheck.NewRow();
                DataRow newRowCheck3 = dtPFMLastCheck.NewRow();
                DataRow newRowCheck4 = dtPFMLastCheck.NewRow();
                //Max,Min,average 
                newRowCheck1[2] = "Cao nhất";
                newRowCheck1[4] = dtPFMLastCheck.Compute("Max ([Số Ảnh])", "");
                newRowCheck1[5] = dtPFMLastCheck.Compute("Max ([Thời gian])", "");
                newRowCheck2[2] = "Thấp nhất";
                newRowCheck2[4] = dtPFMLastCheck.Compute("Min ([Số Ảnh])", "");
                newRowCheck2[5] = dtPFMLastCheck.Compute("Min ([Thời gian])", "");
                try
                {
                    newRowCheck3[2] = "Trung bình";
                    newRowCheck3[4] = Math.Round(double.Parse(dtPFMLastCheck.Compute("Avg ([Số Ảnh])", "").ToString()), 2);
                    newRowCheck3[5] = Math.Round(double.Parse(dtPFMLastCheck.Compute("Avg ([Thời gian])", "").ToString()), 2);
                }
                catch
                {
                }
                dtPFMLastCheck.Rows.Add(newRowCheck);
                dtPFMLastCheck.Rows.Add(newRowCheck1);
                dtPFMLastCheck.Rows.Add(newRowCheck2);
                dtPFMLastCheck.Rows.Add(newRowCheck3);
                if (trungtam == "ALL")
                {
                    DataRow newRowCheck5 = dtPFMLastCheck.NewRow();
                    DataRow newRowCheck7 = dtPFMLastCheck.NewRow();
                    dtPFMLastCheck.Rows.Add(newRowCheck5);
                    newRowCheck7[2] = "ALL";
                    newRowCheck7[4] = dtPFMLastCheck.Compute("Sum ([Số Ảnh])", "[FullName]<>''");
                    newRowCheck7[5] = dtPFMLastCheck.Compute("Sum ([Thời gian])", "[FullName]<>''");
                    dtPFMLastCheck.Rows.Add(newRowCheck7);
                    for (int i = 0; i < distinctValues.Rows.Count; i++)
                    {
                        string trtam = distinctValues.Rows[i]["Trung tâm"].ToString();
                        DataRow newRows = dtPFMLastCheck.NewRow();
                        string tkuGroup = dtPFMLastCheck.Compute("Sum ([Số Ảnh])", "[Trung tâm] = '" + trtam + "'").ToString();
                        string timeGroup = dtPFMLastCheck.Compute("Sum ([Thời gian])", "[Trung tâm] =  '" + trtam + "'").ToString();
                        double ttgGroup = double.Parse(dtPFMLastCheck.Compute("Sum ([Thời gian])", "[Trung tâm] =  '" + trtam + "'").ToString());
                        newRows[2] = trtam;
                        newRows[4] = tkuGroup;
                        newRows[5] = ttgGroup;
                        dtPFMLastCheck.Rows.Add(newRows);
                    }
                }
                else
                {
                    DataRow newRowCheck5 = dtPFMLastCheck.NewRow();
                    DataRow newRowCheck7 = dtPFMLastCheck.NewRow();
                    dtPFMLastCheck.Rows.Add(newRowCheck5);
                    newRowCheck7[2] = typeGroup;
                    newRowCheck7[4] = dtPFMLastCheck.Compute("Sum ([Số Ảnh])", "");
                    newRowCheck7[5] = dtPFMLastCheck.Compute("Sum ([Thời gian])", "");
                    dtPFMLastCheck.Rows.Add(newRowCheck7);
                }
                worker.ReportProgress(dtPFMLastCheck.Rows.Count);
            }
            #endregion
        }

        private void bgwXem_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                prgbExcel.Value = e.ProgressPercentage;
            }
            catch
            {
            }
        }

        private void bgwXem_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            grcPerformance.DataSource = null;
            prgbExcel.Value = prgbExcel.Maximum;
            prgbExcel.Visible = false;
            if (typeUser == "ENTRY")
            {
                grcPerformance.DataSource = dtPFM;
            }
            else if (typeUser == "CHECK")
            {
                grcPerformance.DataSource = dtPFMCheck;
            }
            else if (typeUser == "LASTCHECK" || typeUser == "NOUHIN")
            {
                grcPerformance.DataSource = dtPFMLastCheck;
            }
        }

        private void frmAdmin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && btnOk.Visible == true)
                btnOk_Click(sender, e);
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }
        static int max(int a, int b)
        {
            return (a > b) ? a : b;
        }
        static int[,] c;
        //Prints one LCS
        private string BackTrack(string s1, string s2, int i, int j)
        {
            if (i == 0 || j == 0)
                return "";
            if (s1[i - 1] == s2[j - 1])
            {
                oSheet.Cells[rowExcel + 4, columExcel].Characters(i, 1).Font.color = Color.Black;
                return BackTrack(s1, s2, i - 1, j - 1) + s1[i - 1];
            }
            else if (c[i - 1, j] > c[i, j - 1])
                return BackTrack(s1, s2, i - 1, j);
            else
                return BackTrack(s1, s2, i, j - 1);

        }
        static int LCS(string s1, string s2)
        {
            for (int i = 1; i <= s1.Length; i++)
                c[i, 0] = 0;
            for (int i = 1; i <= s2.Length; i++)
                c[0, i] = 0;

            for (int i = 1; i <= s1.Length; i++)
                for (int j = 1; j <= s2.Length; j++)
                {
                    if (s1[i - 1] == s2[j - 1])
                        c[i, j] = c[i - 1, j - 1] + 1;
                    else
                    {
                        c[i, j] = max(c[i - 1, j], c[i, j - 1]);

                    }

                }

            return c[s1.Length, s2.Length];

        }

        private void txtDong_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char.IsLetter)(e.KeyChar))
                e.Handled = true;
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            lblTitle.Text = "New user"; lblError.Text = "";
            txtFullname.Text = ""; txtMSNV.Text = ""; txtUsername.Text = ""; txtUsername.Enabled = true;
            cboLevel.SelectedIndex = -1; cboPair.SelectedIndex = -1; cboRole.SelectedIndex = -1; cboGroup.SelectedIndex = -1;
            trangthai = 1;
            PNewUserAndEditUser();
            txtUsername.Focus();

        }

        private void btndel_Click(object sender, EventArgs e)
        {
            if (lblID.Text != "")
                if (MessageBox.Show("Delete this user?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    workdb.DeleteUser(Convert.ToInt32(lblID.Text));
                    dtAllUser = workdb.daAllUser();
                    grdUsers.DataSource = dtAllUser;
                }
            trangthai = 3;
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            PNewUserAndEditUser();
            trangthai = 2;
        }

        private void btnviewstt_Click(object sender, EventArgs e)
        {
            dtResultsEntry.Clear();
            tableall.Clear();
            lstTemplate = "";
            for (int i = 0; i < grThongkeV.RowCount; i++)
            {
                string idtemp = grThongkeV.GetRowCellValue(i, grThongkeV.Columns["TemplateId"]).ToString().Trim();
                string id = grThongkeV.GetRowCellValue(i, grThongkeV.Columns["Id"]).ToString();
                string batchname = grThongkeV.GetRowCellValue(i, grThongkeV.Columns["BatchName"]).ToString();
                string tableform = "";
                if (batchname.Substring(0, 1) != "Z")
                {
                    if (idtemp == "1")
                    {
                        tableform = id + "--" + batchname;
                    }
                    else
                    {
                        tableform = id + "--" + batchname + "--lv3";
                    }
                }
                else {
                    if (idtemp == "3")
                    {
                        tableform = id + "--" + batchname;
                    }
                }
                if (tableform == "")
                { continue; }
                System.Data.DataTable dtBatch = new System.Data.DataTable();
                dtBatch = workdb.Get_status(tableform);
                tableall.Merge(dtBatch);
            }
            //Lay tất cả các batch trong form         
            if (grThongkeV.RowCount > 0)
            {
                var results = from status in tableall.AsEnumerable()
                              group status by new { status1 = status.Field<string>("Name"), status3 = status.Field<string>("TempID") } into status
                              select new
                              {
                                  BatchName = status.Key.status1,
                                  Template = status.Key.status3,
                                  Count = status.Count(),
                                  Entry1 = status.Count(x => x.Field<string>("Content1") != null),
                                  User1 = status.GroupBy(x => new { y = x.Field<int>("UserId1"), z = x.Field<string>("g1") }).Select(x => x.Key).GroupBy(x => x.z).Select(x => new { TT = x.Key, SL = x.Count() }),
                                  Entry2 = status.Count(x => x.Field<string>("Content2") != null),
                                  User2 = status.GroupBy(x => new { y = x.Field<int>("UserId2"), z = x.Field<string>("g2") }).Select(x => x.Key).GroupBy(x => x.z).Select(x => new { TT = x.Key, SL = x.Count() }),
                                  Entry3 = status.Count(x => x.Field<int>("UserId3") != 0),
                                  User3 = status.GroupBy(x => new { y = x.Field<int>("UserId3"), z = x.Field<string>("g3") }).Select(x => x.Key).GroupBy(x => x.z).Select(x => new { TT = x.Key, SL = x.Count() }),
                                  Check = status.Count(x => x.Field<DateTime?>("CheckedDate") != null && x.Field<string>("Result") == null),
                                  QC = status.Count(x => x.Field<DateTime?>("QCDate") != null && x.Field<string>("Result") == null),
                                  LCID = status.GroupBy(x => x.Field<string>("LC")).Select(x => x.Key),
                                  Nouhin = status.GroupBy(x => x.Field<string>("NouHin")).Select(x => x.Key),
                                  CountChar = status.GroupBy(x => x.Field<string>("Result") == null ? -1 : x.Field<string>("Result").IndexOfAny(new char[] { '*', '/' })).Select(x => x.Key).Count()
                              };
                foreach (var element in results)
                {
                    var arrTow1 = element.User1.ToList();
                    arrTow1.RemoveAll(x => x.TT == "0");
                    int tong1 = arrTow1.Select(x => x.SL).Sum();
                    string strarr1 = String.Join(Environment.NewLine, arrTow1).Replace("{", "").Replace("}", "").Replace(" ", "").Replace(",", " : ").Replace("TT=", "").Replace("SL=", "");
                    var arrTow2 = element.User2.ToList();
                    arrTow2.RemoveAll(x => x.TT == "0");
                    int tong2 = arrTow2.Select(x => x.SL).Sum();
                    string strarr2 = String.Join(Environment.NewLine, arrTow2).Replace("{", "").Replace("}", "").Replace(" ", "").Replace(",", " : ").Replace("TT=", "").Replace("SL=", "");
                    var arrTow3 = element.User3.ToList();
                    arrTow3.RemoveAll(x => x.TT == "0");
                    int tong3 = arrTow3.Select(x => x.SL).Sum();
                    string strarr3 = String.Join(Environment.NewLine, arrTow3).Replace("{", "").Replace("}", "").Replace(" ", "").Replace(",", " : ").Replace("TT=", "").Replace("SL=", "");
                    var row = dtResultsEntry.NewRow();

                    //var arrTowLC = element.LCID.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    //var arrTowNouHin = element.Nouhin.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                    row["BatchName"] = element.BatchName;
                    row["Template"] = arrTemplate[Convert.ToInt32(element.Template) - 1];
                    row["Tổng trường"] = element.Count;
                    row["E1 đã nhập"] = element.Entry1;
                    row["Entry 1"] = strarr1 + Environment.NewLine + "--------" + Environment.NewLine + "ALL: " + tong1;
                    if (row["Template"].ToString() == "Kojin_LV1" && element.BatchName.ToString()[0] == 'A')
                    {
                        row["Entry 2"] = "";
                    }
                    else
                    {
                        row["E2 đã nhập"] = element.Entry2;
                        row["Entry 2"] = strarr2 + Environment.NewLine + "--------" + Environment.NewLine + "ALL: " + tong2;
                    }
                    if (row["Template"].ToString() == "Kojin_LV1" && element.BatchName.ToString()[0] == 'A')
                    {
                        row["Tổng trường E3"] = element.Entry3;
                        row["Entry 3"] = strarr3 + Environment.NewLine + "--------" + Environment.NewLine + "ALL: " + tong3;
                    }
                    else
                    {
                        row["Entry 3"] = "";
                    }
                    row["Tổng trường Check"] = element.Check;
                    row["Tổng trường QC"] = element.QC;
                    row["LastCheck"] = convertToUnSign3(String.Join(Environment.NewLine, element.LCID));
                    row["CountChar"] = element.CountChar;
                    row["Nouhin"] = convertToUnSign3(String.Join(Environment.NewLine, element.Nouhin));
                    dtResultsEntry.Rows.Add(row);
                }
                grdstatus.DataSource = dtResultsEntry;
                grvstatus.Columns["Entry 1"].ColumnEdit = rtxtGrid;
                grvstatus.Columns["Entry 2"].ColumnEdit = rtxtGrid;
                grvstatus.Columns["Entry 3"].ColumnEdit = rtxtGrid;
                grvstatus.Columns["LastCheck"].ColumnEdit = rtxtGrid;
                grvstatus.Columns["Nouhin"].ColumnEdit = rtxtGrid;
                grvstatus.Columns["Tổng trường"].Summary.Clear();
                grvstatus.Columns["E1 đã nhập"].Summary.Clear();
                grvstatus.Columns["E2 đã nhập"].Summary.Clear();
                int tongSL = Convert.ToInt32(dtResultsEntry.Compute("SUM ([Tổng trường])", ""));
                int tongSLE2 = 0;
                try
                {
                    tongSLE2 = Convert.ToInt32(dtResultsEntry.Compute("SUM ([Tổng trường])", "[Template] <> 'Kojin_LV1'"));
                }
                catch { }
                grvstatus.Columns["Tổng trường"].Summary.Add(DevExpress.Data.SummaryItemType.Custom, "Count", "Tổng trường");
                grvstatus.Columns["Tổng trường"].Summary.Add(DevExpress.Data.SummaryItemType.Custom, "Count", "Tổng nhập");
                grvstatus.Columns["Tổng trường"].Summary.Add(DevExpress.Data.SummaryItemType.Custom, "Count", "Còn lại");
                grvstatus.Columns["E1 đã nhập"].Summary.Add(DevExpress.Data.SummaryItemType.Custom, "Entry1", tongSL.ToString());
                grvstatus.Columns["E1 đã nhập"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "E1 đã nhập", "{0}");
                int tongEntry1Nhap = Convert.ToInt32(dtResultsEntry.Compute("SUM ([E1 đã nhập])", ""));
                grvstatus.Columns["E1 đã nhập"].Summary.Add(DevExpress.Data.SummaryItemType.Custom, "E1 đã nhập", (tongSL - tongEntry1Nhap).ToString());
                grvstatus.Columns["E2 đã nhập"].Summary.Add(DevExpress.Data.SummaryItemType.Custom, "E2 đã nhập", tongSL.ToString());
                grvstatus.Columns["E2 đã nhập"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "E2 đã nhập", "{0}");
                int tongEntry2Nhap = Convert.ToInt32(dtResultsEntry.Compute("SUM ([E2 đã nhập])", ""));
                grvstatus.Columns["E2 đã nhập"].Summary.Add(DevExpress.Data.SummaryItemType.Custom, "E2 đã nhập", (tongSL - tongEntry2Nhap).ToString());
                grvstatus.Columns["CountChar"].Visible = false;
            }
        }

        public static string convertToUnSign3(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        private void grvstatus_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Clicks == 2)
            {
                frmdetails frmdta = new frmdetails();
                frmdta.batchname = grvstatus.GetRowCellValue(e.RowHandle, "BatchName").ToString();
                frmdta.template = (Array.IndexOf(arrTemplate, grvstatus.GetRowCellValue(e.RowHandle, "Template")) + 1).ToString();
                frmdta.ShowDialog();
            }
        }

        private void grvUsers_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView view = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void grdUsers_MouseHover(object sender, EventArgs e)
        {
            grvUsers.Focus();
        }

        private void txtsearch_TextChanged(object sender, EventArgs e)
        {
            grvUsers.FindFilterText = txtsearch.Text;

        }

        private void grvstatus_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            int r = e.RowHandle;
            var cl = e.Column;
            //string vl = e.CellValue.ToString();
            if (r > -1)
            {
                if (cl.FieldName == "Tổng trường")
                {
                    tongsoluong = Convert.ToInt32(e.CellValue);
                }
                if (cl.FieldName == "E1 đã nhập")
                {
                    soluonge1 = Convert.ToInt32(e.CellValue);
                    if (soluonge1 == tongsoluong)
                    {
                        e.Appearance.BackColor = Color.YellowGreen;
                    }
                }
                if (cl.FieldName == "E2 đã nhập" && e.CellValue.ToString() != "")
                {
                    soluonge2 = Convert.ToInt32(e.CellValue);
                    if (soluonge2 == tongsoluong)
                    {
                        e.Appearance.BackColor = Color.YellowGreen;
                    }
                    tongsoluong = 0;
                }
                if (cl.FieldName == "Tổng trường Check")
                {
                    slcheck = Convert.ToInt32(e.CellValue);
                    if (slcheck == 0)
                    {
                        e.Appearance.BackColor = Color.YellowGreen;
                    }
                }
                if (cl.FieldName == "Tổng trường QC")
                {
                    slqc = Convert.ToInt32(e.CellValue);
                    if (slqc == 0)
                    {
                        e.Appearance.BackColor = Color.YellowGreen;
                    }
                }
                if (cl.FieldName == "LastCheck")
                {
                    string vl = grvstatus.GetRowCellValue(e.RowHandle, grvstatus.Columns["LastCheck"]).ToString();
                    string vlcount = grvstatus.GetRowCellValue(e.RowHandle, grvstatus.Columns["CountChar"]).ToString();
                    if (vl != "")
                    {
                        e.Appearance.BackColor = Color.YellowGreen;
                    }
                    if (Convert.ToInt32(vlcount) > 1)
                    {
                        e.Appearance.BackColor = Color.Red;
                    }
                }
                if (cl.FieldName == "NouHin")
                {
                    string vl = grvstatus.GetRowCellValue(e.RowHandle, grvstatus.Columns["Nouhin"]).ToString();
                    if (vl != "")
                    {
                        e.Appearance.BackColor = Color.YellowGreen;
                    }
                }
            }
        }
        private void cboTemplateFB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void grThongkeV_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView view = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void grvPerformance_MouseUp(object sender, MouseEventArgs e)
        {

        }
        void DeleteAction(object sender, EventArgs e)
        {
            ColumnView View = (ColumnView)grThongke.FocusedView;
            GridColumn column = View.Columns[grThongkeV.FocusedColumn.FieldName];
            if (grThongkeV.SelectedRowsCount == 0)
            {
                return;
            }
            else if (grThongkeV.SelectedRowsCount > 1)
            {
                for (int i = grThongkeV.SelectedRowsCount - 1; i >= 0; i--)
                {
                    int rw = grThongkeV.GetSelectedRows()[i];
                    string batchName = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["BatchName"]).ToString();
                    string id = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["Id"]).ToString();
                    workdb.DeleteBatch(batchName);
                    workdb.DeleteTalble(batchName);
                    dtbatch.Rows.RemoveAt(rw);
                }
            }
            else
            {
                int rw = grThongkeV.GetSelectedRows()[0];
                string batchName = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["BatchName"]).ToString();
                string id = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["Id"]).ToString();
                workdb.DeleteBatch(batchName);
                workdb.DeleteTalble(batchName);
            }
            dtbatch.Rows.Clear();
            var results = workdb.GetBatch();
            for (int i = 0; i < results.Rows.Count; i++)
            {
                var row = dtbatch.NewRow();
                row["BatchName"] = results.Rows[i][0];
                row["Id"] = results.Rows[i][1];
                row["Template"] = arrTemplate[Convert.ToInt16(results.Rows[i][1]) - 1];
                row["Số lượng"] = results.Rows[i][3];
                row["Ngày up"] = results.Rows[i][2];
                dtbatch.Rows.Add(row);
            }
            grThongke.DataSource = dtbatch;
            MessageBox.Show("Completed!");
        }
        void HideAction(object sender, EventArgs e)
        {
            ColumnView View = (ColumnView)grThongke.FocusedView;
            GridColumn column = View.Columns[grThongkeV.FocusedColumn.FieldName];
            if (grThongkeV.SelectedRowsCount == 0)
            {
                return;
            }
            else if (grThongkeV.SelectedRowsCount > 1)
            {
                for (int i = grThongkeV.SelectedRowsCount - 1; i >= 0; i--)
                {
                    int rw = grThongkeV.GetSelectedRows()[i];
                    string batchName = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["BatchName"]).ToString();
                    string id = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["Id"]).ToString();
                    workdb.UpdateHitpointBatch(batchName, id);
                }
            }
            else
            {
                int rw = grThongkeV.GetSelectedRows()[0];
                string batchName = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["BatchName"]).ToString();
                string id = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["Id"]).ToString();
                workdb.UpdateHitpointBatch(batchName, id);
            }
            dtbatch.Rows.Clear();
            var results = workdb.GetBatch();
            for (int i = 0; i < results.Rows.Count; i++)
            {
                var row = dtbatch.NewRow();
                row["BatchName"] = results.Rows[i][0];
                row["Id"] = results.Rows[i][1];
                row["Template"] = arrTemplate[Convert.ToInt16(results.Rows[i][1]) - 1];
                row["Số lượng"] = results.Rows[i][3];
                row["Ngày up"] = results.Rows[i][2];
                dtbatch.Rows.Add(row);
            }
            grThongke.DataSource = dtbatch;
            MessageBox.Show("Completed!");
        }
        void UnHideAction(object sender, EventArgs e)
        {
            ColumnView View = (ColumnView)grcBatchHide.FocusedView;
            GridColumn column = View.Columns[grvBatchHide.FocusedColumn.FieldName];
            if (grvBatchHide.SelectedRowsCount == 0)
            {
                return;
            }
            else if (grvBatchHide.SelectedRowsCount > 1)
            {
                for (int i = grvBatchHide.SelectedRowsCount - 1; i >= 0; i--)
                {
                    int rw = grvBatchHide.GetSelectedRows()[i];
                    string batchName = grvBatchHide.GetRowCellValue(rw, grvBatchHide.Columns["BatchName"]).ToString();
                    string id = grThongkeV.GetRowCellValue(rw, grvBatchHide.Columns["Template"]).ToString();
                    workdb.UpdateHitpointBatch_UnHide(batchName, id);
                }
            }
            else
            {
                int rw = grvBatchHide.GetSelectedRows()[0];
                string batchName = grvBatchHide.GetRowCellValue(rw, grvBatchHide.Columns["BatchName"]).ToString();
                string id = grvBatchHide.GetRowCellValue(rw, grvBatchHide.Columns["Id"]).ToString();
                workdb.UpdateHitpointBatch_UnHide(batchName, id);
            }
            dtBatchHide.Rows.Clear();
            var results = workdb.GetBatch_Hide();
            for (int i = 0; i < results.Rows.Count; i++)
            {
                var row = dtBatchHide.NewRow();
                row["BatchName"] = results.Rows[i][0];
                row["Id"] = results.Rows[i][1];
                row["Template"] = arrTemplate[Convert.ToInt16(results.Rows[i][1]) - 1];
                row["Số lượng"] = results.Rows[i][3];
                row["Ngày up"] = results.Rows[i][2];
                dtBatchHide.Rows.Add(row);
            }
            grcBatchHide.DataSource = dtBatchHide;
            MessageBox.Show("Completed!");
        }

        private void grThongkeV_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {   //click event
                //MessageBox.Show("you got it!");
                ContextMenu contextMenu = new System.Windows.Forms.ContextMenu();
                System.Windows.Forms.MenuItem menuItem = new System.Windows.Forms.MenuItem("Hide batchs");
                menuItem.Click += new EventHandler(HideAction);
                contextMenu.MenuItems.Add(menuItem);
                System.Windows.Forms.MenuItem menuItem1 = new System.Windows.Forms.MenuItem("Delete batchs");
                menuItem1.Click += new EventHandler(DeleteAction);
                contextMenu.MenuItems.Add(menuItem1);
                grThongke.ContextMenu = contextMenu;
            }
        }

        private void btnSaveDate_Click(object sender, EventArgs e)
        {
            if (grThongkeV.RowCount > 0)
            {
                if (MessageBox.Show("Bạn có muốn lưu các batch hiện tại?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    string Batchs = "N'";
                    string Template = "";
                    for (int i = 0; i < grThongkeV.RowCount; i++)
                    {
                        string idtemp = grThongkeV.GetRowCellValue(i, grThongkeV.Columns["Id"]).ToString();
                        Batchs = Batchs + grThongkeV.GetRowCellValue(i, grThongkeV.Columns["BatchName"]).ToString() + "',N'";
                        Template = (Template.Contains(idtemp) ? Template : Template + idtemp + ",");
                    }
                    Batchs = Batchs.Substring(0, Batchs.Length - 3);
                    Template = Template.TrimEnd(',');
                    //Lay tất cả các batch trong form

                    System.Data.DataTable dtSave = workdb.Get_Data_Save(Batchs, Template);
                    //Database_Admin dbData = new Database_Admin();
                    //dbData.Save_DATA(dtSave);
                    MessageBox.Show("Completed!", "Thông báo");
                }
            }
        }

        private void grvUsers_DoubleClick(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn muốn thiết lập user này về password mặt định 123456?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                string idvl = grvUsers.GetRowCellValue(grvUsers.FocusedRowHandle, "Id").ToString();
                workdb.Update_Password(idvl);
            }
        }

        private void grvBatchHide_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {   //click event
                //MessageBox.Show("you got it!");
                ContextMenu contextMenu = new System.Windows.Forms.ContextMenu();
                System.Windows.Forms.MenuItem menuItem2 = new System.Windows.Forms.MenuItem("Unhide batchs");
                menuItem2.Click += new EventHandler(UnHideAction);
                contextMenu.MenuItems.Add(menuItem2);
                grcBatchHide.ContextMenu = contextMenu;
            }
        }

        private void grvstatus_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

    }
    #region add button to statusTrip
    [ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.MenuStrip |
                                       ToolStripItemDesignerAvailability.ContextMenuStrip |
                                       ToolStripItemDesignerAvailability.StatusStrip)]
    public class ButtonStripItem : ToolStripControlHost
    {
        private System.Windows.Forms.Button button;

        public ButtonStripItem()
            : base(new System.Windows.Forms.Button())
        {
            this.button = this.Control as System.Windows.Forms.Button;
        }

        // Add properties, events etc. you want to expose...
    }
    #endregion
}

