﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
namespace VCB_KOMOSI
{
    class DB_Update
    {
        private string stringconnecttion = String.Format("Data Source=" + Program.server + ";Initial Catalog=VCB_KOMOSI_2017;User Id=entrykomosi;Password=entrykomosi;;Integrated Security=no;MultipleActiveResultSets=True;Packet Size=4096;Pooling=false;");
        private SqlConnection sqlConnection = new SqlConnection(String.Format("Data Source=" + Program.server + ";Initial Catalog=VCB_KOMOSI_2017;User Id=entrykomosi;Password=entrykomosi;;Integrated Security=no;MultipleActiveResultSets=True;Packet Size=4096;Pooling=false;"));
        /// <summary>
        /// Open connection
        /// </summary>
        private void OpenCnn()
        {
            try
            {
                if (sqlConnection.State.Equals(System.Data.ConnectionState.Closed))
                {
                    sqlConnection.Open();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi kết nối cơ sở dữ liệu\n" + sqlException.Message);
            }
        }

        /// <summary>
        /// Close connection
        /// </summary>
        private void CloseCnn()
        {
            if (sqlConnection.State.Equals(System.Data.ConnectionState.Open))
            {
                sqlConnection.Close();
            }
        }       

        public bool check_update(string appname)
        {
            bool id = true;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select 1 from dbo.KOMOSI where AppName=N'" + appname + "'";
                    con.Open();
                    if ((sqlCommand.ExecuteScalar()) !=null)
                        id = false;
                }
            }
            catch { }
            return id;
        }
        /// <summary>
        /// Lấy thông tin số ảnh còn lại
        /// </summary>
        /// <param name="userId">id của user</param>
        /// <returns>level</returns>
        public int active_App_Update(string projectname)
        {
            int returnVal = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select Active from dbo.App_Auto_Update where ProjectName='" + projectname + "'";
                    con.Open();
                    returnVal = (int)(sqlCommand.ExecuteScalar());
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return returnVal;
        }      
        /// <summary>
        /// Check co ton tai record image content ko
        /// </summary>
        /// <param name="imageId">id cua image</param>
        /// <returns>ton tai hay ko</returns>
        public byte[] get_app_auto_update(string projectname)
        {
            byte[] returnVal = null;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT FileUpdate FROM dbo.[App_Auto_Update] WHERE ProjectName = @projectname";
                    sqlCommand.Parameters.Add("@projectname", SqlDbType.NVarChar).Value = projectname;
                    con.Open();
                    returnVal = (byte[])sqlCommand.ExecuteScalar();
                }
            }
            catch
            { }
            return returnVal;
        }

        public string getTen()
        {
            try
            {
                //if (frmEntry.selecthitpoint == true)
                //{
                OpenCnn();
                //sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT top 1  TenFile from dbo.AutoUpdate order by ID desc";
                string res = (string)sqlCommand.ExecuteScalar();
                sqlConnection.Close();
                CloseCnn();
                return res;

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình đọc dữ liệu\n" + sqlException.Message);
            }
        }

        public byte[] getFile()
        {
            byte[] bt;
            try
            {
                //if (frmEntry.selecthitpoint == true)
                //{

                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT top 1  FileCapNhat from dbo.AutoUpdate order by ID desc";
                bt = (byte[])sqlCommand.ExecuteScalar();
                sqlConnection.Close();
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình đọc dữ liệu\n" + sqlException.Message);
            }
            return bt;
        }

        public void addFile(string tenFile, byte[] file, string date)
        {

            sqlConnection.Open();
            SqlCommand command = new SqlCommand("insert into AutoUpdate(FileCapNhat,TenFile,NgayCN) values(@file,@ten,@date)");
            command.Connection = sqlConnection;
            command.Parameters.Add(new SqlParameter("file", file));
            //doc anh anh thanh cac bit nhi phan           
            command.Parameters.Add(new SqlParameter("ten", tenFile));
            command.Parameters.Add(new SqlParameter("date", date));
            command.ExecuteNonQuery();
            sqlConnection.Close();
        }

        public DataTable getTenFile()
        {
            DataTable dt = new DataTable();

            try
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT top 1 ID,TenFile,NgayCN from dbo.AutoUpdate order by ID desc";
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlCommand;
                da.Fill(dt);
                sqlConnection.Close();

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình đọc dữ liệu\n" + sqlException.Message);
            }
            finally
            {
                CloseCnn();
            }
            return dt;
        }
        public DataTable getTenFile2()
        {
            DataTable dt = new DataTable();
            try
            {
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT ID,CONVERT(nvarchar,ID)+'-'+TenFile+'-'+CONVERT(nvarchar,NgayCN) as N'Tên File' from dbo.AutoUpdate order by ID desc";
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlCommand;
                da.Fill(dt);
                sqlConnection.Close();

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình đọc dữ liệu\n" + sqlException.Message);
            }
            finally
            {
                CloseCnn();
            }
            return dt;
        }

        public byte[] getFile(string id)
        {
            byte[] bt;
            try
            {
                //if (frmEntry.selecthitpoint == true)
                //{

                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "SELECT  FileCapNhat from dbo.AutoUpdate where ID='" + id + "'";
                bt = (byte[])sqlCommand.ExecuteScalar();
                sqlConnection.Close();
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình đọc dữ liệu\n" + sqlException.Message);
            }
            return bt;
        }
    }
}
