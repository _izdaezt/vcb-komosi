﻿namespace VCB_KOMOSI
{
    partial class UpdateTool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.lbTenFile = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grdFile = new DevExpress.XtraGrid.GridControl();
            this.grvFile = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.cbTenFile = new System.Windows.Forms.ComboBox();
            this.btDownLoad = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvFile)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(12, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 33);
            this.button1.TabIndex = 0;
            this.button1.Text = "Open...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbTenFile
            // 
            this.lbTenFile.AutoSize = true;
            this.lbTenFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenFile.Location = new System.Drawing.Point(107, 20);
            this.lbTenFile.Name = "lbTenFile";
            this.lbTenFile.Size = new System.Drawing.Size(0, 20);
            this.lbTenFile.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(370, 35);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 58);
            this.button2.TabIndex = 2;
            this.button2.Text = "UP";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.grdFile);
            this.panel1.Location = new System.Drawing.Point(2, 116);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(546, 164);
            this.panel1.TabIndex = 3;
            // 
            // grdFile
            // 
            this.grdFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdFile.Location = new System.Drawing.Point(0, 0);
            this.grdFile.MainView = this.grvFile;
            this.grdFile.Name = "grdFile";
            this.grdFile.Size = new System.Drawing.Size(546, 164);
            this.grdFile.TabIndex = 0;
            this.grdFile.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvFile});
            // 
            // grvFile
            // 
            this.grvFile.GridControl = this.grdFile;
            this.grvFile.Name = "grvFile";
            this.grvFile.OptionsBehavior.ReadOnly = true;
            this.grvFile.OptionsView.ShowGroupPanel = false;
            // 
            // cbTenFile
            // 
            this.cbTenFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTenFile.FormattingEnabled = true;
            this.cbTenFile.Location = new System.Drawing.Point(12, 65);
            this.cbTenFile.Name = "cbTenFile";
            this.cbTenFile.Size = new System.Drawing.Size(352, 28);
            this.cbTenFile.TabIndex = 4;
            // 
            // btDownLoad
            // 
            this.btDownLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDownLoad.Location = new System.Drawing.Point(473, 82);
            this.btDownLoad.Name = "btDownLoad";
            this.btDownLoad.Size = new System.Drawing.Size(75, 28);
            this.btDownLoad.TabIndex = 5;
            this.btDownLoad.Text = "Down";
            this.btDownLoad.UseVisualStyleBackColor = true;
            this.btDownLoad.Click += new System.EventHandler(this.btDownLoad_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(473, 35);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 37);
            this.button3.TabIndex = 6;
            this.button3.Text = "Add Dll";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // UpdateTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 282);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btDownLoad);
            this.Controls.Add(this.cbTenFile);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.lbTenFile);
            this.Controls.Add(this.button1);
            this.Name = "UpdateTool";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.UpdateTool_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvFile)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lbTenFile;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraGrid.GridControl grdFile;
        private DevExpress.XtraGrid.Views.Grid.GridView grvFile;
        private System.Windows.Forms.ComboBox cbTenFile;
        private System.Windows.Forms.Button btDownLoad;
        private System.Windows.Forms.Button button3;
    }
}