﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Net.NetworkInformation;
using IWshRuntimeLibrary;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Net.Sockets;
namespace VCB_KOMOSI
{
    public partial class frmLogIn : Form
    {
        public string formtemplate;
        public int formid;
        public static bool fn = false;
        public string batchname;
        public int batchId;
        public int uID;
        public static string form;
        int pair;
        private DAEntry_Entry daEntry;
        private DAEntry_Check daCheck;
        private WorkDB_LC wb;
        SqlDataAdapter da = new SqlDataAdapter();
        bool Rpass = false;
        private Io_Check clUntil = new Io_Check();
        DataTable dtbatch = new DataTable();
        DataTable dtform = new DataTable();
        DataTable dtuser = new DataTable();
        public static string dataSource = "";
        public static DateTime time_update_old;
        public static string sever = "";
        public static string path_file_config = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\KOMOSI.txt";
        public static string path_image;
        public static string[] arrConfig = new string[7] { "false", "", "", "", "false", "", DateTime.Now.ToString() };
        bool off = false;
        bool min = false;
        public static string username = "";
        public frmLogIn()
        {
            InitializeComponent();
            if (System.IO.File.Exists(path_file_config))
            {
                arrConfig = System.IO.File.ReadAllLines(path_file_config);
                //try {Rpass = Convert.ToBoolean(arrConfig[0]); }
                //catch { }r
            }
            string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            if (userName.ToUpper().IndexOf("THANHNH") == -1 && userName.ToUpper().IndexOf("NAMNN") == -1)
            {
                 CreateStartupShortcut();
                TopMost = true;
            } 
                this.CenterToScreen();
        }

        private void frmLogIn_Load(object sender, EventArgs e)
        {

            //if (Rpass == true)
            //{
            //    txtPassword.Text = arrConfig[2];
            //    txtUserId.Text = arrConfig[1];
            //    chbRememberpasss.Checked = true;
            //}
            dtform = new DataTable();
            daCheck = new DAEntry_Check();
            wb = new WorkDB_LC();
            daEntry = new DAEntry_Entry();
            grpLogin.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
        }
        //Button login
        private void butLogin_Click(object sender, EventArgs e)
        {
            TopMost = false;
            //try
            //{
            dtuser = new DataTable();
            dtuser = daCheck.dtUser(txtUserId.Text);
            lblError.Text = "";
            if (dtuser.Rows.Count > 0)
            {
                if (dtuser.Rows[0]["password"].ToString() != txtPassword.Text)
                {
                    lblError.Text = "Password is incorrect";
                    return;
                }
                if (dtuser.Rows[0]["password"].ToString() == "123456")
                {
                    MessageBox.Show("Bạn đang sử dụng password mặt định vui lòng đổi password mới", "Thông báo");
                    return;
                }
                string role = dtuser.Rows[0]["Role"].ToString();
                if (role != "CROP" && role != "ADMIN" && role != "UPDATE")
                {
                    if (cboTemplate.SelectedIndex == -1)
                    {
                        lblError.Text = "Template is incorrect"; return;
                    }
                    if (batchId == 0)
                    {
                        lblError.Text = "Batch is incorrect"; return;
                    }
                }
                uID = Convert.ToInt32(dtuser.Rows[0]["ID"].ToString());
                pair = Convert.ToInt32(dtuser.Rows[0]["Pair"].ToString());
                switch (role)
                {
                    #region
                    case "ENTRY":
                        bool blLoop1 = false;
                        do
                        {
                            fn = false;
                            if (formid == 1)
                            {
                                batchname = (batchname[2] == ' ' ? batchname : batchname.Remove(3, 1));
                                if (batchname[7] == 'A')
                                {
                                    if (pair == 3)
                                    {
                                        string table = batchId + "--" + batchname.Substring(7);
                                        if (daEntry.batchE3(table) > 0)
                                        {
                                            frmEntry_E3 frm1 = new frmEntry_E3();
                                            frm1.batchId = batchId;
                                            frm1.userId = uID;
                                            frm1.userName = txtUserId.Text;
                                            frm1.batchName = batchname;
                                            frm1.pair = pair;
                                            frm1.formName = formtemplate;
                                            frm1.formid = formid;
                                            frm1.ShowDialog();
                                        }
                                        else
                                        {
                                            lblError.Text = "Batch chưa nhập xong";
                                            return;
                                        }
                                    }
                                    else if (pair == 1)
                                    {
                                        frmEntry frm1 = new frmEntry();
                                        frm1.batchId = batchId;
                                        frm1.userId = uID;
                                        frm1.userName = txtUserId.Text;
                                        frm1.batchName = batchname;
                                        frm1.pair = pair;
                                        frm1.formName = formtemplate;
                                        frm1.formid = formid;
                                        frm1.ShowDialog();
                                    }
                                }
                                else
                                {
                                    frmEntry_Kojin frm1 = new frmEntry_Kojin();
                                    frm1.batchId = batchId;
                                    frm1.userId = uID;
                                    frm1.userName = txtUserId.Text;
                                    frm1.batchName = batchname;
                                    frm1.pair = pair;
                                    frm1.formName = formtemplate;
                                    frm1.formid = formid;
                                    frm1.ShowDialog();
                                }
                            }
                            else if (formid == 2)
                            {
                                FrmEntryTemp2 frm2 = new FrmEntryTemp2();
                                frm2.batchId = batchId;
                                frm2.userId = uID;
                                frm2.userName = txtUserId.Text;
                                frm2.batchName = (batchname[2] == ' ' ? batchname : batchname.Remove(3, 1));
                                frm2.pair = pair;
                                frm2.formName = formtemplate;
                                frm2.formid = formid;
                                this.TopMost = false;
                                frm2.ShowDialog();
                                this.TopMost = true;
                            }
                            else if(formid == 3)
                            {
                                frmEntry_Zokusei frm1 = new frmEntry_Zokusei();
                                frm1.batchId = batchId;
                                frm1.userId = uID;
                                frm1.userName = txtUserId.Text;
                                frm1.batchName = (batchname[2] == ' ' ? batchname : batchname.Remove(3, 1));
                                frm1.pair = pair;
                                frm1.formName = formtemplate;
                                frm1.formid = formid;
                                frm1.ShowDialog();
                            }
                            cboBatch.Text = "";
                            string[] arrbatch = daEntry.GetbatchNew(formid, batchId);
                            if (arrbatch[0] != "" && fn)
                            {
                                var msb = MessageBox.Show(this, "Đã hoàn thành Batch " + batchname + "." + Environment.NewLine + "Bạn có muốn check tiếp batch " + arrbatch[1] + " ko?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (msb == DialogResult.Yes)
                                {
                                    batchId = Convert.ToInt32(arrbatch[0]);
                                    batchname = "       " + arrbatch[1];
                                    blLoop1 = true;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                        while (blLoop1);
                        break;
                    #endregion
                    case "CHECK":
                        bool blLoop = false;
                        string typecheck = (txtUserId.Text.ToUpper().Substring(0, 2) == "QC" ? "QC" : "Check");
                        do
                        {
                            fn = false;
                            if (formid == 1)
                            {
                                frmCheck1 frmc = new frmCheck1();
                                frmc.batchId = batchId;
                                frmc.userId = uID;
                                frmc.userName = txtUserId.Text;
                                frmc.batchName = (batchname[2] == ' ' ? batchname : batchname.Remove(3, 1)); ;
                                frmc.template = formtemplate;
                                frmc.level = dtuser.Rows[0]["Lvl"].ToString().Trim();
                                frmc.formId = formid;
                                frmc.typeCheck = typecheck;
                                frmc.ShowDialog();
                            }
                            else if (formid == 2)
                            {
                                frmCheck2 frmc = new frmCheck2();
                                frmc.batchId = batchId;
                                frmc.userId = uID;
                                frmc.userName = txtUserId.Text;
                                frmc.batchName = (batchname[2] == ' ' ? batchname : batchname.Remove(3, 1)); ;
                                frmc.template = formtemplate;
                                frmc.level = dtuser.Rows[0]["Lvl"].ToString().Trim();
                                frmc.formId = formid;
                                frmc.typeCheck = typecheck;
                                this.TopMost = false;
                                frmc.ShowDialog();
                                this.TopMost = true;
                            }
                            else if (formid == 3)
                            {
                                frmCheckZokusei frmc = new frmCheckZokusei();
                                frmc.batchId = batchId;
                                frmc.userId = uID;
                                frmc.userName = txtUserId.Text;
                                frmc.batchName = (batchname[2] == ' ' ? batchname : batchname.Remove(3, 1)); ;
                                frmc.template = formtemplate;
                                frmc.level = dtuser.Rows[0]["Lvl"].ToString().Trim();
                                frmc.formId = formid;
                                frmc.typeCheck = typecheck;
                                this.TopMost = false;
                                frmc.ShowDialog();
                                this.TopMost = true;
                            }
                            cboBatch.Text = "";
                            string[] arrbatch = daEntry.GetbatchNewHCheck(formid, batchId);
                            if (arrbatch[0] != "" && fn)
                            {
                                var msb = MessageBox.Show(this, "Đã hoàn thành Batch " + batchname + "." + Environment.NewLine + "Bạn có muốn check tiếp batch " + arrbatch[1] + " ko?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (msb == DialogResult.Yes)
                                {
                                    batchId = Convert.ToInt32(arrbatch[0]);
                                    batchname = "       " + arrbatch[1];
                                    blLoop = true;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                        while (blLoop);
                        break;
                    case "LASTCHECK":
                        FormLC frml = new FormLC();
                        FormLC.dtbatch = dtbatch;
                        FormLC.batchID = batchId;
                        FormLC.batchname = (batchname[2] == ' ' ? batchname : batchname.Remove(3, 1)); ;
                        FormLC.formID = formid;
                        FormLC.formName = formtemplate;
                        FormLC.userID = uID;
                        FormLC.LastcheckID = wb.CheckBatch(batchId);
                        if (FormLC.LastcheckID != 0)
                        {
                            FormLC.db = true;
                        }
                        frml.ShowDialog();
                        break;
                    case "CROP":
                        frmUpdateImage frmcrop = new frmUpdateImage();
                        frmcrop.ShowDialog();
                        break;
                    case "ADMIN":
                        frmAdmin frmAdmin = new frmAdmin();
                        frmAdmin.ShowDialog();
                        break;
                    case "UPDATE":
                        UpdateTool frm = new UpdateTool();
                        frm.ShowDialog();
                        break;
                }
                cboTemplate_SelectedIndexChanged(sender, e);
            }
            else
            {
                lblError.Text = "Username is Incorrect!";
            }

            //}
            //catch { }
            cboBatch.Text = "";
            cboTemplate.SelectedIndex = formid - 1;
            batchId = 0;
            TopMost = true;
        }
        //form dang ky
        private void llblRegister_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }
        //Chon item cbotemplate
        private void cboTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboTemplate.SelectedIndex > -1)
            {
                formid = cboTemplate.SelectedIndex + 1;
                lblError.Text = "";
                int hitpoint = 10;
                dtuser = new DataTable();
                dtuser = daCheck.dtUser(txtUserId.Text);
                //Check role
                if (dtuser.Rows.Count > 0)
                {
                    switch (dtuser.Rows[0]["Role"].ToString())
                    {
                        case "ENTRY": hitpoint = 0; break;
                        case "CHECK": hitpoint = 1; break;
                        case "LASTCHECK": hitpoint = 2; break;
                        case "CROP": return;
                        case "ADMIN": return;
                        default: hitpoint = 10; lblError.Text = "Username is Incorrect"; return;
                    }
                    formtemplate = cboTemplate.Text;
                    cboBatch.Text = "";
                    cboBatch.DataSource = null;
                    DataTable temp = new DataTable();
                    dtbatch = new DataTable();
                    try
                    {
                        if (dtuser.Rows[0]["Lvl"].ToString() == "3" && formid != 2)
                            return;
                        else if (dtuser.Rows[0]["Lvl"].ToString() == "1" && formid == 2)
                            return;
                        //else if (dtuser.Rows[0]["Lvl"].ToString() == "1" && formid == 1 && dtuser.Rows[0]["Pair"].ToString() == "2")
                            //return;
                        else if (dtuser.Rows[0]["Lvl"].ToString() == "2" && formid == 2)
                            return;
                        else if (dtuser.Rows[0]["Lvl"].ToString() == "4" && formid != 2)
                            return;
                        else if (dtuser.Rows[0]["Role"].ToString() != "LASTCHECK")
                            if (Convert.ToInt32(dtuser.Rows[0]["Lvl"].ToString()) > 4)
                                return;
                    }
                    catch { }
                    temp = daCheck.dtBatch(formid);
                    if (temp.Rows.Count > 0)
                    {
                        dtbatch = temp.Clone();

                        var datarows = temp.Select("Hitpoint=" + hitpoint);

                        for (int i = 0; i < datarows.Length; i++)
                        {
                            dtbatch.ImportRow(datarows[i]);
                        }

                        cboBatch.DataSource = dtbatch;
                        cboBatch.DisplayMember = "Name";
                    }
                }
                else
                {
                    lblError.Text = "Username is Incorrect";
                }
            }
        }
        //Chon item cboBatch
        private void cboBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(cboBatch.SelectedIndex.ToString()) != -1)
            {
                batchId = int.Parse(dtbatch.Rows[cboBatch.SelectedIndex][1].ToString());
                batchname = dtbatch.Rows[cboBatch.SelectedIndex][0].ToString();
            }
        }

        private void chbRememberpasss_CheckedChanged(object sender, EventArgs e)
        {
            if (chbRememberpasss.Checked == false)
            {
                arrConfig[1] = "";
                arrConfig[2] = "";
                arrConfig[0] = false.ToString();
            }
            else if (chbRememberpasss.Checked == true)
            {
                arrConfig[1] = txtUserId.Text;
                arrConfig[2] = txtPassword.Text;
                arrConfig[0] = true.ToString();
            }
        }

        private void frmLogIn_FormClosing(object sender, FormClosingEventArgs e)
        {
            //chbRememberpasss_CheckedChanged(sender, e);
            //System.IO.File.WriteAllLines(path_file_config, arrConfig);
            if (off)
            {
                chbRememberpasss_CheckedChanged(sender, e);
                System.IO.File.WriteAllLines(path_file_config, arrConfig);
            }
            else
            {
                string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                if (userName.ToUpper().IndexOf("THANHNH") == -1 && userName.ToUpper().IndexOf("NAMNN") == -1)
                {
                    e.Cancel = true;
                }
            }        
        }

        private void txtUserId_TextChanged(object sender, EventArgs e)
        {
            cboTemplate.SelectedIndex = -1;
            dtbatch = new DataTable();
            cboBatch.DataSource = null;
            if (txtUserId.Text.Length > 3)
            {
                if (txtUserId.Text.Substring(0, 2).ToUpper() == "E3")
                {
                    cboTemplate.SelectedIndex = 1;
                }
                else if (formid == 2)
                    cboTemplate.SelectedIndex = 0;
                else
                    cboTemplate.SelectedIndex = formid - 1;
            }
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            username = txtUserId.Text;
            frmChangePassword frmchangepass = new frmChangePassword();
            frmchangepass.ShowDialog();
            txtUserId_TextChanged(sender, e);
        }

        private void frmLogIn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                butLogin_Click(sender, e);
            if (e.Control)
            {
                var arrTemp = txtUserId.Text.Split('-');
                if (arrTemp.Length == 2)
                {
                    if (e.KeyCode == Keys.NumPad1)
                    {
                        if (arrTemp[0].ToUpper() == "E11")
                            arrTemp[0] = "E12";
                        else if (arrTemp[0].ToUpper() == "E12")
                            arrTemp[0] = "E13";
                        else if (arrTemp[0].ToUpper() == "E13")
                            arrTemp[0] = "E11";
                        else
                            arrTemp[0] = "E11";
                        txtUserId.Text = string.Join("-", arrTemp);

                    }
                    else if (e.KeyCode == Keys.NumPad2)
                    {
                        if (arrTemp[0].ToUpper() == "C2")
                            arrTemp[0] = "QC2";
                        else if (arrTemp[0].ToUpper() == "QC2")
                            arrTemp[0] = "C2";
                        else
                            arrTemp[0] = "C2";
                        txtUserId.Text = string.Join("-", arrTemp);

                    }
                    else if (e.KeyCode == Keys.NumPad3)
                    {
                        if (arrTemp[0].ToUpper() == "E31")
                            arrTemp[0] = "E32";
                        else if (arrTemp[0].ToUpper() == "E32")
                            arrTemp[0] = "E31";
                        else
                            arrTemp[0] = "E31";
                        txtUserId.Text = string.Join("-", arrTemp);

                    }
                    else if (e.KeyCode == Keys.NumPad4)
                    {
                        if (arrTemp[0].ToUpper() == "C4")
                            arrTemp[0] = "QC4";
                        else if (arrTemp[0].ToUpper() == "QC4")
                            arrTemp[0] = "C4";
                        else
                            arrTemp[0] = "C4";
                        txtUserId.Text = string.Join("-", arrTemp);

                    }
                    else if (e.KeyCode == Keys.NumPad5)
                    {
                        if (arrTemp[0].ToUpper() == "E51")
                            arrTemp[0] = "E52";
                        else if (arrTemp[0].ToUpper() == "E52")
                            arrTemp[0] = "E51";
                        else
                            arrTemp[0] = "E51";
                        txtUserId.Text = string.Join("-", arrTemp);

                    }
                    else if (e.KeyCode == Keys.NumPad6)
                    {
                        if (arrTemp[0].ToUpper() == "C6")
                            arrTemp[0] = "QC6";
                        else if (arrTemp[0].ToUpper() == "QC6")
                            arrTemp[0] = "C6";
                        else
                            arrTemp[0] = "C6";
                        txtUserId.Text = string.Join("-", arrTemp);

                    }
                    else if (e.KeyCode == Keys.NumPad7)
                    {
                        if (arrTemp[0].ToUpper() == "E71")
                            arrTemp[0] = "E72";
                        else if (arrTemp[0].ToUpper() == "E72")
                            arrTemp[0] = "E71";
                        else
                            arrTemp[0] = "E71";
                        txtUserId.Text = string.Join("-", arrTemp);

                    }
                    else if (e.KeyCode == Keys.NumPad8)
                    {
                        if (arrTemp[0].ToUpper() == "C8")
                            arrTemp[0] = "QC8";
                        else if (arrTemp[0].ToUpper() == "QC8")
                            arrTemp[0] = "C8";
                        else
                            arrTemp[0] = "C8";
                        txtUserId.Text = string.Join("-", arrTemp);

                    }
                    else if (e.KeyCode == Keys.NumPad9)
                    {
                        arrTemp[0] = "LC";
                        txtUserId.Text = string.Join("-", arrTemp);

                    }
                    else if (e.KeyCode == Keys.Subtract)
                    {
                        arrTemp[0] = "CR";
                        txtUserId.Text = string.Join("-", arrTemp);

                    }
                    else if (e.KeyCode == Keys.Add)
                    {
                        arrTemp[0] = "AD";
                        txtUserId.Text = string.Join("-", arrTemp);

                    }
                }
            }
            if (e.KeyCode == Keys.LWin || e.KeyCode == Keys.RWin)
            {
                this.Activate();
                if (e.KeyCode == Keys.D)
                {
                    this.Activate();
                }
                SendKeys.Send("{Esc}");
                SendKeys.Send("{Esc}");
                e.Handled = true;
            }
        }



        private void txtUserId_Leave(object sender, EventArgs e)
        {
            if (txtUserId.Text.Substring(0, 2).ToUpper() == "E1" || txtUserId.Text.Substring(0, 2).ToUpper() == "C2")
            {
                cboTemplate.SelectedIndex = 0;
            }
            else if (txtUserId.Text.Substring(0, 2).ToUpper() == "E3" || txtUserId.Text.Substring(0, 2).ToUpper() == "C4")
            {
                cboTemplate.SelectedIndex = 1;
            }
            else
            {
                cboTemplate.SelectedIndex = 0;
            }
        }

        public static bool pingServer(string ip)
        {
            bool result = false;
            try
            {
                Ping ping = new Ping();
                PingReply pingReply = ping.Send(ip);
                if (pingReply.Status == IPStatus.Success)
                    return true;
            }
            catch { }
            return result;
        }
        void CreateStartupShortcut()
        {
            string startupFolder = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
            WshShell shell = new WshShell();
            string shortcutAddress = startupFolder + @"\Komosi.lnk";
            IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutAddress);
            shortcut.Description = "A startup shortcut. If you delete this shortcut from your computer, LaunchOnStartup.exe will not launch on Windows Startup"; // set the description of the shortcut
            shortcut.WorkingDirectory = Application.StartupPath; /* working directory */
            shortcut.TargetPath = Application.ExecutablePath; /* path of the executable */
            // optionally, you can set the arguments in shortcut.Arguments
            // For example, shortcut.Arguments = "/a 1";
            shortcut.Save();
        }

        private void btnlogoff_Click(object sender, EventArgs e)
        {
            if ((System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper() == "KOMOSI01\\KOMOSI") || (System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper() == "KOMOSI02\\KOMOSI"))
            {
                if (!min)
                    Process.Start(Path.Combine(Environment.GetEnvironmentVariable("windir"), "explorer.exe"));
                min = true;
                this.WindowState = FormWindowState.Minimized;
                chbRememberpasss_CheckedChanged(sender, e);
                clUntil.WriteToFile(Rpass, "", "", dataSource, time_update_old.ToString());
                return;
            }
            else
            {
                try
                {
                    Process[] processes = Process.GetProcesses();
                    foreach (Process p in processes)
                    {
                        if (!String.IsNullOrEmpty(p.ProcessName))
                        {
                            try
                            {
                                Process[] myProcesses;
                                // Returns array containing all instances of Notepad.
                                myProcesses = Process.GetProcessesByName(p.ProcessName);
                                foreach (Process myProcess in myProcesses)
                                {
                                    myProcess.CloseMainWindow();
                                }
                            }
                            catch { }
                        }
                    }
                    off = true;
                    Application.Exit();
                    ExitWindowsEx(0, 0);
                    return;
                }
                catch { }
            }
        }
        [DllImport("user32")]
        public static extern bool ExitWindowsEx(uint uFlags, uint dwReason);

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Activate();
        }

        private void grpLogin_Paint(object sender, PaintEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
