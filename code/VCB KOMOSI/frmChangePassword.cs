﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VCB_KOMOSI
{
    public partial class frmChangePassword : Form
    {
        DAEntry_Entry daentry = new DAEntry_Entry();
        public frmChangePassword()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(txtpassnew.Text.Trim(), @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$"))
            {
                MessageBox.Show("Pass phải tối thiểu 8 ký tự, ký tự in hoa, ký tự đặt biệt và số", "Thông báo");
                return;
            }
            if (txtusername.Text.Equals(""))
            { MessageBox.Show("Username is empty", "Information"); return; }
            if (txtpassold.Text.Equals(""))
            { MessageBox.Show("Old password is empty", "Information"); return; }
            if (txtpassnew.Text.Equals(""))
            { MessageBox.Show("New password is empty", "Information"); return; }
            if (txtRepassnew.Text.Equals(""))
            { MessageBox.Show("Re-new password is empty", "Information"); return; }
            if (!txtpassnew.Text.Equals(txtRepassnew.Text.Trim()))
            { MessageBox.Show("Re-new password incorrect", "Information"); return; }
            if (daentry.password(txtusername.Text.Trim()).Equals(""))
            { MessageBox.Show("Username does not exist ", "Information"); return; }
            if (!txtpassold.Text.Trim().Equals(daentry.password(txtusername.Text.ToUpper().Trim())))
            { MessageBox.Show("Password is incorrect", "Information"); return; }
            daentry.Updatepassword(txtRepassnew.Text.Trim(), txtusername.Text.ToUpper().Trim());
            this.Close();
        }

        private void frmChangePassword_Load(object sender, EventArgs e)
        {
            txtusername.Text = frmLogIn.username;
        }
    }
}
