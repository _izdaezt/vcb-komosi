﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VCB_KOMOSI
{
    class BOImageContent_Check
    {
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int pair1;
        public int Pair1
        {
            get { return pair1; }
            set { pair1 = value; }
        }
        private int pair2;
        public int Pair2
        {
            get { return pair2; }
            set { pair2 = value; }
        }          

        private List<string> content1;
        public List<string> Content1
        {
            get { return content1; }
            set { content1 = value; }
        }

        private string group1="";
        public string Group1
        {
            get { return group1; }
            set { group1 = value; }
        }   

        private string group2="";
        public string Group2
        {
            get { return group2; }
            set { group2 = value; }
        }

        private string group3="";
        public string Group3
        {
            get { return group3; }
            set { group3 = value; }
        }

        private List<string> content2;
        public List<string> Content2
        {
            get { return content2; }
            set { content2 = value; }
        }
        private List<string> content1_OCR;
        public List<string> Content1_OCR
        {
            get { return content1_OCR; }
            set { content1_OCR = value; }
        }

        private List<string> content_OCR;
        public List<string> Content_OCR
        {
            get { return content_OCR; }
            set { content_OCR = value; }
        }

        private List<string> content3;
        public List<string> Content3
        {
            get { return content3; }
            set { content3 = value; }
        }       

        private int checkerId;
        public int CheckerId
        {
            get { return checkerId; }
            set { checkerId = value; }
        }

        private string checkedDate;
        public string CheckedDate
        {
            get { return checkedDate; }
            set { checkedDate = value; }
        }

        private string result;
        public string Result
        {
            get { return result; }
            set { result = value; }
        }

        private int lastCheckId;
        public int LastCheckId
        {
            get { return lastCheckId; }
            set { lastCheckId = value; }
        }
        private int loisai1;
        public int Loisai1
        {
            get { return loisai1; }
            set { loisai1 = value; }
        }
        private int loisai2;
        public int Loisai2
        {
            get { return loisai2; }
            set { loisai2 = value; }
        }
        private int loisai3;
        public int Loisai3
        {
            get { return loisai3; }
            set { loisai3 = value; }
        }
        private int tongkytu;
        public int Tongkytu
        {
            get { return tongkytu; }
            set { tongkytu = value; }
        }

        private string name1="";
        public string Name1
        {
            get { return name1; }
            set { name1 = value; }
        }

        private string name2="";
        public string Name2
        {
            get { return name2; }
            set { name2 = value; }
        }
        private string name3="";
        public string Name3
        {
            get { return name3; }
            set { name3 = value; }
        }     
    }
}
