﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
namespace VCB_KOMOSI
{
    public partial class frmCheck2 : Form
    {
        public int formId;
        public int batchId;
        public int userId;
        public string userName;
        public string batchName;
        public string template;
        public string level;
        Bitmap imageSource;
        string[] arrsh = new string[0];
        string[] arrsl = new string[0];
        string[] arrgm = new string[0];
        string[] arrgb = new string[0];
        private int numbertrtxt = 0;
        private DAEntry_Check dACheck = new DAEntry_Check();
        private BOImage_Check img = new BOImage_Check();
        private BOImageContent_Check imgContent = new BOImageContent_Check();
        List<BOImage_Check> ListImage = new List<BOImage_Check>();
        private bool finish = false;
        private System.Windows.Forms.TextBox txtAutoComplete = new System.Windows.Forms.TextBox();
        DataTable dthct = new DataTable();
        int id = 0;
        int saveimageid = 0;
        public string[] INFperformance = new string[2];
        List<RichTextBox> lsttxt = new List<RichTextBox>();
        List<Label> lstlbl = new List<Label>();
        List<Image> lstimageload = new List<Image>();
        float currentzoom;
        //nam 
        string type = "";
        private DAEntry_Entry dAEntry = new DAEntry_Entry();
        public string typeCheck = "";
        public int typeyear = 0;
        string batchnameLocal;
        DateTime dtimeBefore = new DateTime();
        string tableform;
        public frmCheck2()
        {
            InitializeComponent();
            lsttxt = new List<RichTextBox>() { txtResult };
            lstlbl = new List<Label>() { lbl2 };
        }
        private void frmCheck_Load(object sender, EventArgs e)
        {
            foreach (InputLanguage lang in InputLanguage.InstalledInputLanguages)
                if (lang.LayoutName.ToUpper() == "JAPANESE")
                {
                    InputLanguage.CurrentInputLanguage = lang; break;
                }
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            // set bien toan cuc         
            lsttxt.ForEach(a =>
            {
                a.Enter += new System.EventHandler(this.textBox1_Enter);
                a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
                a.Leave += new System.EventHandler(this.textBox1_Leave);
                a.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
                a.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
                a.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rtxt_MouseDown);
            });

            //Sửa lỗi trùng qua check
            //dACheck.Update_Hitpoint_Result(batchId, 2);

            try
            {
                if (batchName[7] == 'A')
                {
                    type = "A";
                }
                //nam 25/7 gan bien khi folder C
                if (batchName[7] == 'C')
                {
                    type = "C";
                    typeyear = Int32.Parse(batchName.Substring(16, 1));
                }
            }
            catch { }

            batchnameLocal = batchName.Substring(7);

            tableform = batchId + "--" + batchnameLocal + "--lv3";

            // lấy id image đã entry để check            
            // check đã entry
            try
            {
                if (typeCheck == "Check")
                {
                    id = dACheck.GetIdCheck(tableform);
                }
                else
                {
                    id = dACheck.GetIdCheckQC(tableform);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }         

            if (id == 0)
            {
                if (dAEntry.Count_UpHitpointLC(tableform) == 0)
                {
                    dAEntry.UpdateHitpoint_Batch(batchId, 2);
                }
                finish = true;
                this.Close();
                return;
            }

            timegetanh.Start();

            // lay image đã entry
            try
            {
                img = dACheck.GetImage(id, tableform);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            ////Get Image
            //imageSource = new Bitmap(Io_Check.byteArrayToImage(dACheck.getImageOnServer(img.PageName, batchName.Substring(7, batchName.Length - 7))));
            //imageViewerTR1.Image = imageSource;
            //currentzoom = imageViewerTR1.CurrentZoom;
            //// lấy image content đã entry

            //nam 25/7 cắt ảnh
            imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageName, batchName.Substring(7, batchName.Length - 7))));
            Bitmap bm1 = (Bitmap)imageSource.Clone();
            Bitmap bm2 = (Bitmap)imageSource.Clone();

            if (type == "A")
            {
                imageViewerTR2.Image = CropBitmap(bm1, bm1.Width * 57 / 100, 0, bm1.Width * 43 / 100, bm1.Height);
            }
            else
            {
                if (typeyear == 1)
                    imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 40 / 100), (bm2.Height * 50 / 100), (bm2.Width * 50 / 100), (bm2.Height * 50 / 100));
                else
                    imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 40 / 100), 220, (bm2.Width * 50 / 100), (bm2.Height * 30 / 100));
                imageViewerTR2.CurrentZoom = 1.2f;
            }
            currentzoom = imageViewerTR2.CurrentZoom;          
            try
            {
                imgContent = dACheck.GetImageContent(id, tableform);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            for (int i = 0; i < imgContent.Content1.Count; i++)
            {
               txtE1.ForeColor = Color.Red;
                txtE1.Text = imgContent.Content1[i];
                txtE2.ForeColor = Color.Red;
                txtE2.Text = imgContent.Content2[i];

                numbertrtxt = 0;

                string s1, s2;
                s1 = imgContent.Content1[i];
                s2 = imgContent.Content2[i];

                lstlbl[i].Text = lstlbl[i].Text + "   E1";
                if (s1 != s2)
                {
                    lstlbl[i].BackColor = Color.Red;
                }
                c = null;
                c = new int[s1.Length + 1, s2.Length + 1];
                LCS(s1, s2);
                BackTrack(s1, s2, s1.Length, s2.Length,txtE1);

                c = null;
                c = new int[s2.Length + 1, s1.Length + 1];
                LCS(s2, s1);
                BackTrack(s2, s1, s2.Length, s1.Length, txtE2);
            }

            // hiển thị user name của user đã entry            
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblEntry3.Text = imgContent.Name3;
            lblGroup3.Text = imgContent.Group3;
            lblbatchname.Text = batchName;
            txtE1.Focus();
            if (img.NG1 != 0 && typeCheck == "QC")
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0 && typeCheck == "QC")
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;
            if (img.NG3 != 0 && typeCheck == "QC")
                lblEntry3.BackColor = Color.Red;
            else
                lblEntry3.BackColor = SystemColors.Window;
            dtimeBefore = DateTime.Now;
            if (typeCheck == "QC")
                lblImage.Text = dACheck.ImageExistQC(tableform);
            else
                lblImage.Text = dACheck.ImageExistCheck(tableform);
        }
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCheck_FormClosing(object sender, FormClosingEventArgs e)
        {
            timegetanh.Stop();
            try
            {
                if (!finish)
                {
                    // unlock image
                    if (typeCheck == "Check")
                    {
                        dACheck.SetHitPointImageCheck(img.Id, tableform);
                        // unlock image       
                        for (int i = 0; i < ListImage.Count; i++)
                        {
                            dACheck.SetHitPointImageCheck(ListImage[i].Id, tableform);
                        }
                    }
                    else
                    {
                        dACheck.SetHitPointImageCheckQC(img.Id, tableform);
                        // unlock image       
                        for (int i = 0; i < ListImage.Count; i++)
                        {
                            dACheck.SetHitPointImageCheckQC(ListImage[i].Id, tableform);
                        }
                    }
                }
                else
                {
                    frmLogIn.fn = true;
                    //dACheck.UpdateHitpoint_Batch(batchId, 2);
                }
            }
            catch { }
        }
        private void btnRL_Click(object sender, EventArgs e)
        {
            imageViewerTR2.RotateImage("90");
        }

        private void btnRR_Click(object sender, EventArgs e)
        {
            imageViewerTR2.RotateImage("270");
        }


        #region Compare string
        static int max(int a, int b)
        {
            return (a > b) ? a : b;
        }
        static int[,] c;
        //Prints one LCS
        private string BackTrack(string s1, string s2, int i, int j,RichTextBox rtb)
        {
            if (i == 0 || j == 0)
                return "";
            if (s1[i - 1] == s2[j - 1])
            {
                rtb.SelectionStart = i - 1;
                rtb.SelectionLength = 1;
                rtb.SelectionColor = Color.Black;
                return BackTrack(s1, s2, i - 1, j - 1, rtb) + s1[i - 1];
            }
            else if (c[i - 1, j] > c[i, j - 1])
                return BackTrack(s1, s2, i - 1, j, rtb);

            else
                return BackTrack(s1, s2, i, j - 1, rtb);

        }
        //Nghịch       
        private string BackTrack2(string s1, string s2, int i, int j)
        {
            if (i == 0 || j == 0)
                return "";
            if (s1[i - 1] == s2[j - 1])
            {
                return BackTrack2(s1, s2, i - 1, j - 1) + s1[i - 1];
            }
            else if (c[i - 1, j] > c[i, j - 1])
                return BackTrack2(s1, s2, i - 1, j);

            else
                return BackTrack2(s1, s2, i, j - 1);

        }
        static int LCS(string s1, string s2)
        {
            for (int i = 1; i <= s1.Length; i++)
                c[i, 0] = 0;
            for (int i = 1; i <= s2.Length; i++)
                c[0, i] = 0;

            for (int i = 1; i <= s1.Length; i++)
                for (int j = 1; j <= s2.Length; j++)
                {
                    if (s1[i - 1] == s2[j - 1])
                        c[i, j] = c[i - 1, j - 1] + 1;
                    else
                    {
                        c[i, j] = max(c[i - 1, j], c[i, j - 1]);

                    }

                }

            return c[s1.Length, s2.Length];

        }
        #endregion


        private void frmCheck_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
            if (e.Control)
            {
                if (e.KeyCode == Keys.Up)
                {
                    imageViewerTR2.CurrentZoom = imageViewerTR2.CurrentZoom + 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Down)
                {
                    imageViewerTR2.CurrentZoom = imageViewerTR2.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR2.CurrentZoom - 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    imageViewerTR2.RotateImage("270");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Left)
                {
                    imageViewerTR2.RotateImage("90");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.F)
                {
                    imageViewerTR2.CurrentZoom = currentzoom;
                    e.Handled = true;
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            imageViewerTR2.CurrentZoom = currentzoom;
        }

        private void btnout_Click(object sender, EventArgs e)
        {
            imageViewerTR2.CurrentZoom = imageViewerTR2.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR2.CurrentZoom - 0.1f;
        }

        private void btnin_Click(object sender, EventArgs e)
        {
            imageViewerTR2.CurrentZoom = imageViewerTR2.CurrentZoom + 0.1f;
        }
        private void textBox1_Enter(object sender, EventArgs e)
        {

        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {

        }
        private void textBox1_Leave(object sender, EventArgs e)
        {

        }
        private void rtxt_MouseDown(object sender, MouseEventArgs e)
        {
            RichTextBox rtxt = (RichTextBox)sender;
            if (e.Button == MouseButtons.Right)
            {
                if (rtxt.Text == imgContent.Content1[0])
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content2[0];
                    lstlbl[0].Text = lstlbl[0].Text.Replace("E1", "E2");
                    numbertrtxt = 0;
                    string s1, s2;
                    s1 = imgContent.Content2[0];
                    s2 = imgContent.Content1[0];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length, txtE1);
                }
                else if (rtxt.Text == imgContent.Content2[0])
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1[0];
                    lstlbl[0].Text = lstlbl[0].Text.Replace("E2", "E1");
                    numbertrtxt = 0;
                    string s1, s2;
                    s1 = imgContent.Content1[0];
                    s2 = imgContent.Content2[0];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length,txtE1);
                }
                else
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1[0];
                    lstlbl[0].Text = lstlbl[0].Text.Replace("E2", "E1");
                    numbertrtxt = 0;
                    string s1, s2;
                    s1 = imgContent.Content1[0];
                    s2 = imgContent.Content2[0];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length, txtE1);
                }
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            btnSubmit.Enabled = false;
            imageViewerTR2.Dispose();
            // update image content vao DB
            string rsEntr1 = String.Join("|", imgContent.Content1);
            string rsEntr2 = String.Join("|", imgContent.Content2);
            string rsCheck = "";
            rsCheck = String.Join("|", lsttxt.Select(x => x.Text.Trim()).ToArray());
            if (typeCheck == "Check")
            {
                //Lỗi sai E1
                c = null;
                c = new int[rsCheck.Length + 1, rsEntr1.Length + 1];
                int vlLCS = LCS(rsCheck, rsEntr1);
                if (rsEntr1 == rsCheck)
                    imgContent.Loisai1 = 0;
                else if (rsEntr1.Length > rsCheck.Length)
                    imgContent.Loisai1 = rsEntr1.Length - vlLCS;
                else
                    imgContent.Loisai1 = rsCheck.Length - vlLCS;
                //Lỗi sai E2 
                c = null;
                c = new int[rsCheck.Length + 1, rsEntr2.Length + 1];
                vlLCS = LCS(rsCheck, rsEntr2);
                if (rsEntr2 == rsCheck)
                    imgContent.Loisai2 = 0;
                else if (rsEntr2.Length > rsCheck.Length)
                    imgContent.Loisai2 = rsEntr2.Length - vlLCS;
                else
                    imgContent.Loisai2 = rsCheck.Length - vlLCS;
            }
            // set image content           
            imgContent.Result = rsCheck;
            imgContent.Tongkytu = imgContent.Result.Length - 1;
            imgContent.CheckerId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            // add image content
            try
            {
                txtE1.Text = "";
                if (typeCheck == "Check")
                    dACheck.UpdateResutl2(imgContent, userId, ms, tableform);
                else
                    dACheck.UpdateQC(imgContent, userId, ms, tableform);
                imgContent = new BOImageContent_Check();
            }
            catch (Exception exception)
            {
                // rool back hitpoint              
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            saveimageid = img.Id;
            // load image mới lên form
            // lấy id image entry
            id = 0;

            imageSource = null;
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                if (dAEntry.Count_UpHitpointLC(tableform) == 0)
                {
                    dAEntry.UpdateHitpoint_Batch(batchId, 2);
                }
                finish = true;
                this.Close();
                return;
            }

            // lay image đã entry
            try
            {
                ////Get Image
                //imageSource = (Bitmap)img.Imagesource.Clone();
                //imageViewerTR1.Image = imageSource;

                //nam 25/7 cắt ảnh
                imageSource = img.Imagesource;
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();

                if (type == "A")
                {
                    imageViewerTR2.Image = CropBitmap(bm1, bm1.Width * 57 / 100, 0, bm1.Width * 43 / 100, bm1.Height);
                }
                else
                {
                    //tr2:chữ kana tr1:giới tính
                    if (typeyear == 1)
                        imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 40 / 100), (bm2.Height * 50 / 100), (bm2.Width * 50 / 100), (bm2.Height * 50 / 100));
                    else
                        imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 40 / 100), 220, (bm2.Width * 50 / 100), (bm2.Height * 30 / 100));
                }
                currentzoom = imageViewerTR2.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageName, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            // lấy image content đã entry
            try
            {
                imgContent = dACheck.GetImageContent(img.Id, tableform);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            for (int i = 0; i < imgContent.Content1.Count; i++)
            {
                lstlbl[i].BackColor = SystemColors.Window;

                txtE1.ForeColor = Color.Red;
                txtE1.Text = imgContent.Content1[i];
                txtE2.ForeColor = Color.Red;
                txtE2.Text = imgContent.Content2[i];

                numbertrtxt = 0;

                string s1, s2;
                s1 = imgContent.Content1[i];
                s2 = imgContent.Content2[i];

                lstlbl[i].Text = lstlbl[i].Text.Substring(0, lstlbl[i].Text.Length - 1) + "1";
                if (s1 != s2)
                {
                    lstlbl[i].BackColor = Color.Red;
                }
                c = null;
                c = new int[s1.Length + 1, s2.Length + 1];
                LCS(s1, s2);
                BackTrack(s1, s2, s1.Length, s2.Length, txtE1);

                c = null;
                c = new int[s2.Length + 1, s1.Length + 1];
                LCS(s2, s1);
                BackTrack(s2, s1, s2.Length, s1.Length, txtE2);      
               
            }

            // hiển thị user name của user đã entry
            btnSubmit.Enabled = true;
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblEntry3.Text = imgContent.Name3;
            lblGroup3.Text = imgContent.Group3;

            if (img.NG1 != 0 && typeCheck == "QC")
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0 && typeCheck == "QC")
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;
            if (img.NG3 != 0 && typeCheck == "QC")
                lblEntry3.BackColor = Color.Red;
            else
                lblEntry3.BackColor = SystemColors.Window;
            dtimeBefore = DateTime.Now;
            if (typeCheck == "QC")
                lblImage.Text = dACheck.ImageExistQC(tableform);
            else
                lblImage.Text = dACheck.ImageExistCheck(tableform);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (saveimageid == 0)
            {
                MessageBox.Show("Can not back", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            // load image mới lên form
            // lấy id image entry
            id = 0;
            try
            {
                if (typeCheck == "Check")
                {
                    dACheck.SetHitPointImageCheck(img.Id, tableform);
                }
                else
                {
                    dACheck.SetHitPointImageCheckQC(img.Id, tableform);
                }
                dACheck.Set_result_null(saveimageid, tableform);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            //lấy Imageid trước           
            try
            {
                id = saveimageid;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            saveimageid = 0;

            // lay image đã entry
            try
            {
                img = dACheck.GetImage(id, tableform);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            try
            {
                imageViewerTR2.Dispose();
                //Get Image
                //imageSource = new Bitmap(Io_Entry.byteArrayToImage(dACheck.getImageOnServer(img.PageName, batchName.Substring(7, batchName.Length - 7))));
                //imageViewerTR1.Image = imageSource;

                //nam 25/7 cắt ảnh
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageName, batchName.Substring(7, batchName.Length - 7))));
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();

                if (type == "A")
                {
                    imageViewerTR2.Image = CropBitmap(bm1, bm1.Width * 57 / 100, 0, bm1.Width * 43 / 100, bm1.Height);
                }
                else
                {
                    //tr2:chữ kana tr1:giới tính
                    if (typeyear == 1)
                        imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 40 / 100), (bm2.Height * 50 / 100), (bm2.Width * 50 / 100), (bm2.Height * 50 / 100));
                    else
                        imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 40 / 100), 220, (bm2.Width * 50 / 100), (bm2.Height * 30 / 100));
                }
                currentzoom = imageViewerTR2.CurrentZoom;              
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageName, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // lấy image content đã entry
            try
            {
                imgContent = dACheck.GetImageContent(id, tableform);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            for (int i = 0; i < imgContent.Content1.Count; i++)
            {
                lstlbl[i].Text = lstlbl[i].Text.Substring(0, lstlbl[i].Text.Length - 1) + "1";
                txtE1.ForeColor = Color.Red;
                txtE1.Text = imgContent.Content1[i];
                txtE2.ForeColor = Color.Red;
                txtE2.Text = imgContent.Content2[i];
                numbertrtxt = 0;
                string s1, s2;
                s1 = imgContent.Content1[i];
                s2 = imgContent.Content2[i];

                lstlbl[i].Text = lstlbl[i].Text.Substring(0, lstlbl[i].Text.Length - 1) + "1";
                if (s1 != s2)
                {
                    lstlbl[i].BackColor = Color.Red;
                }
                c = null;
                c = new int[s1.Length + 1, s2.Length + 1];
                LCS(s1, s2);
                BackTrack(s1, s2, s1.Length, s2.Length, txtE1);

                c = null;
                c = new int[s2.Length + 1, s1.Length + 1];
                LCS(s2, s1);
                BackTrack(s2, s1, s2.Length, s1.Length, txtE2);
            }

            // hiển thị user name của user đã entry            
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblEntry3.Text = imgContent.Name3;
            lblGroup3.Text = imgContent.Group3;
            if (img.NG1 != 0 && typeCheck == "QC")
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0 && typeCheck == "QC")
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;
            if (img.NG3 != 0 && typeCheck == "QC")
                lblEntry3.BackColor = Color.Red;
            else
                lblEntry3.BackColor = SystemColors.Window;
            dtimeBefore = DateTime.Now;
            if (typeCheck == "QC")
                lblImage.Text = dACheck.ImageExistQC(tableform);
            else
                lblImage.Text = dACheck.ImageExistCheck(tableform);
        }
        private void rtxtsbd_MouseDown(object sender, MouseEventArgs e)
        {
            RichTextBox rtxt = (RichTextBox)sender;
            if (e.Button == MouseButtons.Right)
            {
                if (rtxt.Text == imgContent.Content1_OCR[rtxt.TabIndex])
                {
                    lstlbl[rtxt.TabIndex - 1].Text = lstlbl[rtxt.TabIndex - 1].Text.Substring(0, lstlbl[rtxt.TabIndex - 1].Text.Length - 1) + "2";
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content3[rtxt.TabIndex];
                    numbertrtxt = rtxt.TabIndex;
                    string s1, s2;
                    s1 = imgContent.Content3[rtxt.TabIndex];
                    s2 = imgContent.Content1_OCR[rtxt.TabIndex];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length, txtE1);
                }
                else if (rtxt.Text == imgContent.Content3[rtxt.TabIndex])
                {
                    lstlbl[rtxt.TabIndex - 1].Text = lstlbl[rtxt.TabIndex - 1].Text.Substring(0, lstlbl[rtxt.TabIndex - 1].Text.Length - 1) + "1";
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1_OCR[rtxt.TabIndex];
                    numbertrtxt = rtxt.TabIndex;
                    string s1, s2;
                    s1 = imgContent.Content1_OCR[rtxt.TabIndex];
                    s2 = imgContent.Content3[rtxt.TabIndex];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length, txtE1);
                }
                else
                {
                    lstlbl[rtxt.TabIndex - 1].Text = lstlbl[rtxt.TabIndex - 1].Text.Substring(0, lstlbl[rtxt.TabIndex - 1].Text.Length - 1) + "1";
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1_OCR[rtxt.TabIndex];
                    numbertrtxt = rtxt.TabIndex;
                    string s1, s2;
                    s1 = imgContent.Content1_OCR[rtxt.TabIndex];
                    s2 = imgContent.Content3[rtxt.TabIndex];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length, txtE1);
                }
            }
        }       

        private void txt1_Enter(object sender, EventArgs e)
        {
            txtE1.BackColor = SystemColors.Window;
            foreach (InputLanguage lang in InputLanguage.InstalledInputLanguages)
                if (lang.LayoutName.Contains("US"))
                {
                    InputLanguage.CurrentInputLanguage = lang; break;
                }
        }

        private void txt2_Enter(object sender, EventArgs e)
        {
            txtE1.BackColor = Color.PowderBlue;
            foreach (InputLanguage lang in InputLanguage.InstalledInputLanguages)
                if (lang.LayoutName.ToUpper() == "JAPANESE")
                {
                    InputLanguage.CurrentInputLanguage = lang; break;
                }
            txtE1.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
        }
        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }

        private void timegetanh_Tick(object sender, EventArgs e)
        {
            if (ListImage.Count < 2)
            {
                int id2 = 0;
                if (typeCheck == "Check")
                {
                    id2 = dACheck.GetIdCheck(tableform);
                }
                else
                {
                    id2 = dACheck.GetIdCheckQC(tableform);
                }
                if (id2 > 0)
                {
                    BOImage_Check img2 = new BOImage_Check();
                    img2 = dACheck.GetImage(id2, tableform);
                    img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dACheck.getImageOnServer(img2.PageName,batchnameLocal)));
                    ListImage.Add(img2);
                }

            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btSelectE1_Click(object sender, EventArgs e)
        {
            txtResult.Text = txtE1.Text;
        }

        private void btSelectE2_Click(object sender, EventArgs e)
        {
            txtResult.Text = txtE2.Text;
        }
    }
}

