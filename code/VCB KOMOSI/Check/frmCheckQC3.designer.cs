﻿namespace VCB_KOMOSI
{
    partial class frmCheckQC3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCheckQC3));
            this.btnRL = new System.Windows.Forms.Button();
            this.btnRR = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.txt3 = new System.Windows.Forms.RichTextBox();
            this.txt2 = new System.Windows.Forms.RichTextBox();
            this.txt1 = new System.Windows.Forms.RichTextBox();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.txt4 = new System.Windows.Forms.RichTextBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnout = new System.Windows.Forms.Button();
            this.btnin = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblEntry1 = new System.Windows.Forms.Label();
            this.lblEntry2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblfilename = new System.Windows.Forms.Label();
            this.lblImage = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblGroup2 = new System.Windows.Forms.Label();
            this.lblGroup1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblbatchname = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.bgwGetImage = new System.ComponentModel.BackgroundWorker();
            this.bgwGetImage1 = new System.ComponentModel.BackgroundWorker();
            this.lblGroup3 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblEntry3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.imageViewerTR1 = new ImageViewerTR.ImageViewerTR();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.imageViewerTR2 = new ImageViewerTR.ImageViewerTR();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ctxt17 = new System.Windows.Forms.TextBox();
            this.clbl17 = new System.Windows.Forms.Label();
            this.ctxt20 = new System.Windows.Forms.TextBox();
            this.ctxt19 = new System.Windows.Forms.TextBox();
            this.clbl20 = new System.Windows.Forms.Label();
            this.clbl19 = new System.Windows.Forms.Label();
            this.ctxt15 = new System.Windows.Forms.TextBox();
            this.ctxt16 = new System.Windows.Forms.TextBox();
            this.ctxt18 = new System.Windows.Forms.TextBox();
            this.ctxt14 = new System.Windows.Forms.TextBox();
            this.clbl15 = new System.Windows.Forms.Label();
            this.clbl14 = new System.Windows.Forms.Label();
            this.clbl16 = new System.Windows.Forms.Label();
            this.clbl18 = new System.Windows.Forms.Label();
            this.ctxt12 = new System.Windows.Forms.TextBox();
            this.ctxt13 = new System.Windows.Forms.TextBox();
            this.ctxt11 = new System.Windows.Forms.TextBox();
            this.clbl12 = new System.Windows.Forms.Label();
            this.clbl11 = new System.Windows.Forms.Label();
            this.clbl13 = new System.Windows.Forms.Label();
            this.ctxt8 = new System.Windows.Forms.TextBox();
            this.ctxt9 = new System.Windows.Forms.TextBox();
            this.ctxt10 = new System.Windows.Forms.TextBox();
            this.ctxt7 = new System.Windows.Forms.TextBox();
            this.clbl8 = new System.Windows.Forms.Label();
            this.clbl7 = new System.Windows.Forms.Label();
            this.clbl9 = new System.Windows.Forms.Label();
            this.clbl10 = new System.Windows.Forms.Label();
            this.ctxt4 = new System.Windows.Forms.TextBox();
            this.ctxt5 = new System.Windows.Forms.TextBox();
            this.ctxt6 = new System.Windows.Forms.TextBox();
            this.ctxt3 = new System.Windows.Forms.TextBox();
            this.ctxt2 = new System.Windows.Forms.TextBox();
            this.ctxt1 = new System.Windows.Forms.TextBox();
            this.clbl2 = new System.Windows.Forms.Label();
            this.clbl4 = new System.Windows.Forms.Label();
            this.clbl1 = new System.Windows.Forms.Label();
            this.clbl3 = new System.Windows.Forms.Label();
            this.clbl5 = new System.Windows.Forms.Label();
            this.clbl6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRL
            // 
            this.btnRL.BackgroundImage = global::VCBHL_Entry.Properties.Resources.object_rotate_left;
            this.btnRL.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRL.Location = new System.Drawing.Point(80, 3);
            this.btnRL.Name = "btnRL";
            this.btnRL.Size = new System.Drawing.Size(29, 25);
            this.btnRL.TabIndex = 53;
            this.btnRL.UseVisualStyleBackColor = true;
            this.btnRL.Click += new System.EventHandler(this.btnRL_Click);
            // 
            // btnRR
            // 
            this.btnRR.BackgroundImage = global::VCBHL_Entry.Properties.Resources.object_rotate_right;
            this.btnRR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRR.Location = new System.Drawing.Point(45, 3);
            this.btnRR.Name = "btnRR";
            this.btnRR.Size = new System.Drawing.Size(29, 25);
            this.btnRR.TabIndex = 54;
            this.btnRR.UseVisualStyleBackColor = true;
            this.btnRR.Click += new System.EventHandler(this.btnRR_Click);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBack.BackgroundImage")));
            this.btnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBack.Location = new System.Drawing.Point(10, 663);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(98, 65);
            this.btnBack.TabIndex = 13;
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // txt3
            // 
            this.txt3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt3.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3.ForeColor = System.Drawing.Color.Red;
            this.txt3.Location = new System.Drawing.Point(894, 635);
            this.txt3.Name = "txt3";
            this.txt3.Size = new System.Drawing.Size(201, 45);
            this.txt3.TabIndex = 3;
            this.txt3.Text = "";
            // 
            // txt2
            // 
            this.txt2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt2.ForeColor = System.Drawing.Color.Red;
            this.txt2.Location = new System.Drawing.Point(894, 588);
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(201, 45);
            this.txt2.TabIndex = 2;
            this.txt2.Text = "";
            // 
            // txt1
            // 
            this.txt1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt1.ForeColor = System.Drawing.Color.Red;
            this.txt1.Location = new System.Drawing.Point(894, 542);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(201, 45);
            this.txt1.TabIndex = 1;
            this.txt1.Text = "";
            // 
            // lbl4
            // 
            this.lbl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl4.AutoSize = true;
            this.lbl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl4.ForeColor = System.Drawing.Color.Blue;
            this.lbl4.Location = new System.Drawing.Point(828, 691);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(62, 18);
            this.lbl4.TabIndex = 194;
            this.lbl4.Text = "Code 4";
            // 
            // lbl3
            // 
            this.lbl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl3.AutoSize = true;
            this.lbl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl3.ForeColor = System.Drawing.Color.Blue;
            this.lbl3.Location = new System.Drawing.Point(828, 647);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(62, 18);
            this.lbl3.TabIndex = 193;
            this.lbl3.Text = "Code 3";
            // 
            // lbl2
            // 
            this.lbl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl2.AutoSize = true;
            this.lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.ForeColor = System.Drawing.Color.Blue;
            this.lbl2.Location = new System.Drawing.Point(828, 600);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(62, 18);
            this.lbl2.TabIndex = 192;
            this.lbl2.Text = "Code 2";
            // 
            // lbl1
            // 
            this.lbl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.ForeColor = System.Drawing.Color.Blue;
            this.lbl1.Location = new System.Drawing.Point(828, 555);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(62, 18);
            this.lbl1.TabIndex = 191;
            this.lbl1.Text = "Code 1";
            // 
            // txt4
            // 
            this.txt4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt4.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt4.ForeColor = System.Drawing.Color.Red;
            this.txt4.Location = new System.Drawing.Point(894, 682);
            this.txt4.Name = "txt4";
            this.txt4.Size = new System.Drawing.Size(201, 45);
            this.txt4.TabIndex = 4;
            this.txt4.Text = "";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSubmit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSubmit.BackgroundImage")));
            this.btnSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSubmit.Location = new System.Drawing.Point(1135, 623);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(187, 92);
            this.btnSubmit.TabIndex = 12;
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSumit2_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(10, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 25);
            this.button1.TabIndex = 59;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnout
            // 
            this.btnout.BackgroundImage = global::VCBHL_Entry.Properties.Resources.Zoom_Out_icon;
            this.btnout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnout.Location = new System.Drawing.Point(115, 3);
            this.btnout.Name = "btnout";
            this.btnout.Size = new System.Drawing.Size(29, 25);
            this.btnout.TabIndex = 60;
            this.btnout.UseVisualStyleBackColor = true;
            this.btnout.Click += new System.EventHandler(this.btnout_Click);
            // 
            // btnin
            // 
            this.btnin.BackgroundImage = global::VCBHL_Entry.Properties.Resources.Zoom_In_icon;
            this.btnin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnin.Location = new System.Drawing.Point(150, 3);
            this.btnin.Name = "btnin";
            this.btnin.Size = new System.Drawing.Size(29, 25);
            this.btnin.TabIndex = 61;
            this.btnin.UseVisualStyleBackColor = true;
            this.btnin.Click += new System.EventHandler(this.btnin_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(588, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 62;
            this.label2.Text = "User1:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(588, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 16);
            this.label6.TabIndex = 63;
            this.label6.Text = "User2:";
            // 
            // lblEntry1
            // 
            this.lblEntry1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEntry1.AutoSize = true;
            this.lblEntry1.Location = new System.Drawing.Point(626, 4);
            this.lblEntry1.Name = "lblEntry1";
            this.lblEntry1.Size = new System.Drawing.Size(21, 16);
            this.lblEntry1.TabIndex = 64;
            this.lblEntry1.Text = "E1";
            // 
            // lblEntry2
            // 
            this.lblEntry2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEntry2.AutoSize = true;
            this.lblEntry2.Location = new System.Drawing.Point(626, 27);
            this.lblEntry2.Name = "lblEntry2";
            this.lblEntry2.Size = new System.Drawing.Size(21, 16);
            this.lblEntry2.TabIndex = 65;
            this.lblEntry2.Text = "E2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 16);
            this.label10.TabIndex = 66;
            this.label10.Text = "Image Name:";
            // 
            // lblfilename
            // 
            this.lblfilename.AutoSize = true;
            this.lblfilename.ForeColor = System.Drawing.Color.Blue;
            this.lblfilename.Location = new System.Drawing.Point(87, 31);
            this.lblfilename.Name = "lblfilename";
            this.lblfilename.Size = new System.Drawing.Size(49, 16);
            this.lblfilename.TabIndex = 67;
            this.lblfilename.Text = "0001.jpg";
            // 
            // lblImage
            // 
            this.lblImage.AutoSize = true;
            this.lblImage.ForeColor = System.Drawing.Color.Blue;
            this.lblImage.Location = new System.Drawing.Point(535, 4);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(32, 16);
            this.lblImage.TabIndex = 94;
            this.lblImage.Text = "1211";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(490, 4);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 16);
            this.label13.TabIndex = 93;
            this.label13.Text = "Count:";
            // 
            // lblGroup2
            // 
            this.lblGroup2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGroup2.AutoSize = true;
            this.lblGroup2.Location = new System.Drawing.Point(893, 27);
            this.lblGroup2.Name = "lblGroup2";
            this.lblGroup2.Size = new System.Drawing.Size(28, 16);
            this.lblGroup2.TabIndex = 98;
            this.lblGroup2.Text = "TT2";
            // 
            // lblGroup1
            // 
            this.lblGroup1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGroup1.AutoSize = true;
            this.lblGroup1.Location = new System.Drawing.Point(893, 4);
            this.lblGroup1.Name = "lblGroup1";
            this.lblGroup1.Size = new System.Drawing.Size(28, 16);
            this.lblGroup1.TabIndex = 97;
            this.lblGroup1.Text = "TT1";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(840, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 16);
            this.label4.TabIndex = 96;
            this.label4.Text = "Trung tâm:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(839, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 16);
            this.label5.TabIndex = 95;
            this.label5.Text = "Trung tâm:";
            // 
            // lblbatchname
            // 
            this.lblbatchname.AutoSize = true;
            this.lblbatchname.ForeColor = System.Drawing.Color.Blue;
            this.lblbatchname.Location = new System.Drawing.Point(367, 31);
            this.lblbatchname.Name = "lblbatchname";
            this.lblbatchname.Size = new System.Drawing.Size(49, 16);
            this.lblbatchname.TabIndex = 137;
            this.lblbatchname.Text = "0001.jpg";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(290, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 16);
            this.label8.TabIndex = 136;
            this.label8.Text = "Batch Name:";
            // 
            // bgwGetImage
            // 
            this.bgwGetImage.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwGetImage_DoWork);
            this.bgwGetImage.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwGetImage_RunWorkerCompleted);
            // 
            // bgwGetImage1
            // 
            this.bgwGetImage1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwGetImage1_DoWork);
            this.bgwGetImage1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwGetImage_RunWorkerCompleted);
            // 
            // lblGroup3
            // 
            this.lblGroup3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGroup3.AutoSize = true;
            this.lblGroup3.Location = new System.Drawing.Point(1267, 7);
            this.lblGroup3.Name = "lblGroup3";
            this.lblGroup3.Size = new System.Drawing.Size(28, 16);
            this.lblGroup3.TabIndex = 141;
            this.lblGroup3.Text = "TT3";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1211, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 16);
            this.label3.TabIndex = 140;
            this.label3.Text = "Trung tâm:";
            // 
            // lblEntry3
            // 
            this.lblEntry3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEntry3.AutoSize = true;
            this.lblEntry3.Location = new System.Drawing.Point(1064, 7);
            this.lblEntry3.Name = "lblEntry3";
            this.lblEntry3.Size = new System.Drawing.Size(21, 16);
            this.lblEntry3.TabIndex = 139;
            this.lblEntry3.Text = "E3";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1028, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 16);
            this.label9.TabIndex = 138;
            this.label9.Text = "User3:";
            // 
            // imageViewerTR1
            // 
            this.imageViewerTR1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.imageViewerTR1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imageViewerTR1.CurrentZoom = 1F;
            this.imageViewerTR1.Image = null;
            this.imageViewerTR1.Location = new System.Drawing.Point(4, 50);
            this.imageViewerTR1.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.imageViewerTR1.MaxZoom = 20F;
            this.imageViewerTR1.MinZoom = 0.05F;
            this.imageViewerTR1.Name = "imageViewerTR1";
            this.imageViewerTR1.Size = new System.Drawing.Size(648, 479);
            this.imageViewerTR1.TabIndex = 195;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Location = new System.Drawing.Point(681, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(73, 479);
            this.panel1.TabIndex = 197;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(18, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 55);
            this.label1.TabIndex = 178;
            this.label1.Text = "1";
            // 
            // label25
            // 
            this.label25.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Blue;
            this.label25.Location = new System.Drawing.Point(18, 347);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(52, 55);
            this.label25.TabIndex = 181;
            this.label25.Text = "4";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(18, 199);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 55);
            this.label7.TabIndex = 179;
            this.label7.Text = "2";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Blue;
            this.label17.Location = new System.Drawing.Point(18, 276);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 55);
            this.label17.TabIndex = 180;
            this.label17.Text = "3";
            // 
            // imageViewerTR2
            // 
            this.imageViewerTR2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.imageViewerTR2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imageViewerTR2.CurrentZoom = 1F;
            this.imageViewerTR2.Image = null;
            this.imageViewerTR2.Location = new System.Drawing.Point(755, 50);
            this.imageViewerTR2.Margin = new System.Windows.Forms.Padding(3, 7, 3, 7);
            this.imageViewerTR2.MaxZoom = 20F;
            this.imageViewerTR2.MinZoom = 0.05F;
            this.imageViewerTR2.Name = "imageViewerTR2";
            this.imageViewerTR2.Size = new System.Drawing.Size(596, 479);
            this.imageViewerTR2.TabIndex = 198;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.ctxt17);
            this.panel2.Controls.Add(this.clbl17);
            this.panel2.Controls.Add(this.ctxt20);
            this.panel2.Controls.Add(this.ctxt19);
            this.panel2.Controls.Add(this.clbl20);
            this.panel2.Controls.Add(this.clbl19);
            this.panel2.Controls.Add(this.ctxt15);
            this.panel2.Controls.Add(this.ctxt16);
            this.panel2.Controls.Add(this.ctxt18);
            this.panel2.Controls.Add(this.ctxt14);
            this.panel2.Controls.Add(this.clbl15);
            this.panel2.Controls.Add(this.clbl14);
            this.panel2.Controls.Add(this.clbl16);
            this.panel2.Controls.Add(this.clbl18);
            this.panel2.Controls.Add(this.ctxt12);
            this.panel2.Controls.Add(this.ctxt13);
            this.panel2.Controls.Add(this.ctxt11);
            this.panel2.Controls.Add(this.clbl12);
            this.panel2.Controls.Add(this.clbl11);
            this.panel2.Controls.Add(this.clbl13);
            this.panel2.Controls.Add(this.ctxt8);
            this.panel2.Controls.Add(this.ctxt9);
            this.panel2.Controls.Add(this.ctxt10);
            this.panel2.Controls.Add(this.ctxt7);
            this.panel2.Controls.Add(this.clbl8);
            this.panel2.Controls.Add(this.clbl7);
            this.panel2.Controls.Add(this.clbl9);
            this.panel2.Controls.Add(this.clbl10);
            this.panel2.Controls.Add(this.ctxt4);
            this.panel2.Controls.Add(this.ctxt5);
            this.panel2.Controls.Add(this.ctxt6);
            this.panel2.Controls.Add(this.ctxt3);
            this.panel2.Controls.Add(this.ctxt2);
            this.panel2.Controls.Add(this.ctxt1);
            this.panel2.Controls.Add(this.clbl2);
            this.panel2.Controls.Add(this.clbl4);
            this.panel2.Controls.Add(this.clbl1);
            this.panel2.Controls.Add(this.clbl3);
            this.panel2.Controls.Add(this.clbl5);
            this.panel2.Controls.Add(this.clbl6);
            this.panel2.Location = new System.Drawing.Point(122, 535);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(442, 212);
            this.panel2.TabIndex = 199;
            // 
            // ctxt17
            // 
            this.ctxt17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt17.Location = new System.Drawing.Point(368, 93);
            this.ctxt17.MaxLength = 8;
            this.ctxt17.Name = "ctxt17";
            this.ctxt17.ReadOnly = true;
            this.ctxt17.ShortcutsEnabled = false;
            this.ctxt17.Size = new System.Drawing.Size(54, 26);
            this.ctxt17.TabIndex = 209;
            this.ctxt17.TabStop = false;
            this.ctxt17.Text = "513";
            this.ctxt17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl17
            // 
            this.clbl17.AutoSize = true;
            this.clbl17.BackColor = System.Drawing.SystemColors.Window;
            this.clbl17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl17.ForeColor = System.Drawing.Color.Blue;
            this.clbl17.Location = new System.Drawing.Point(342, 97);
            this.clbl17.Name = "clbl17";
            this.clbl17.Size = new System.Drawing.Size(19, 20);
            this.clbl17.TabIndex = 210;
            this.clbl17.Text = "4";
            // 
            // ctxt20
            // 
            this.ctxt20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt20.Location = new System.Drawing.Point(368, 180);
            this.ctxt20.MaxLength = 8;
            this.ctxt20.Name = "ctxt20";
            this.ctxt20.ReadOnly = true;
            this.ctxt20.ShortcutsEnabled = false;
            this.ctxt20.Size = new System.Drawing.Size(54, 26);
            this.ctxt20.TabIndex = 204;
            this.ctxt20.TabStop = false;
            this.ctxt20.Text = "543";
            this.ctxt20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt19
            // 
            this.ctxt19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt19.Location = new System.Drawing.Point(368, 152);
            this.ctxt19.MaxLength = 8;
            this.ctxt19.Name = "ctxt19";
            this.ctxt19.ReadOnly = true;
            this.ctxt19.ShortcutsEnabled = false;
            this.ctxt19.Size = new System.Drawing.Size(54, 26);
            this.ctxt19.TabIndex = 203;
            this.ctxt19.TabStop = false;
            this.ctxt19.Text = "533";
            this.ctxt19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl20
            // 
            this.clbl20.AutoSize = true;
            this.clbl20.BackColor = System.Drawing.SystemColors.Window;
            this.clbl20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl20.ForeColor = System.Drawing.Color.Blue;
            this.clbl20.Location = new System.Drawing.Point(342, 182);
            this.clbl20.Name = "clbl20";
            this.clbl20.Size = new System.Drawing.Size(19, 20);
            this.clbl20.TabIndex = 206;
            this.clbl20.Text = "7";
            // 
            // clbl19
            // 
            this.clbl19.AutoSize = true;
            this.clbl19.BackColor = System.Drawing.SystemColors.Window;
            this.clbl19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl19.ForeColor = System.Drawing.Color.Blue;
            this.clbl19.Location = new System.Drawing.Point(342, 154);
            this.clbl19.Name = "clbl19";
            this.clbl19.Size = new System.Drawing.Size(19, 20);
            this.clbl19.TabIndex = 207;
            this.clbl19.Text = "6";
            // 
            // ctxt15
            // 
            this.ctxt15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt15.Location = new System.Drawing.Point(368, 35);
            this.ctxt15.MaxLength = 8;
            this.ctxt15.Name = "ctxt15";
            this.ctxt15.ReadOnly = true;
            this.ctxt15.ShortcutsEnabled = false;
            this.ctxt15.Size = new System.Drawing.Size(54, 26);
            this.ctxt15.TabIndex = 196;
            this.ctxt15.TabStop = false;
            this.ctxt15.Text = "452";
            this.ctxt15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt16
            // 
            this.ctxt16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt16.Location = new System.Drawing.Point(368, 64);
            this.ctxt16.MaxLength = 8;
            this.ctxt16.Name = "ctxt16";
            this.ctxt16.ReadOnly = true;
            this.ctxt16.ShortcutsEnabled = false;
            this.ctxt16.Size = new System.Drawing.Size(54, 26);
            this.ctxt16.TabIndex = 197;
            this.ctxt16.TabStop = false;
            this.ctxt16.Text = "453";
            this.ctxt16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt18
            // 
            this.ctxt18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt18.Location = new System.Drawing.Point(368, 123);
            this.ctxt18.MaxLength = 8;
            this.ctxt18.Name = "ctxt18";
            this.ctxt18.ReadOnly = true;
            this.ctxt18.ShortcutsEnabled = false;
            this.ctxt18.Size = new System.Drawing.Size(54, 26);
            this.ctxt18.TabIndex = 198;
            this.ctxt18.TabStop = false;
            this.ctxt18.Text = "523";
            this.ctxt18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt14
            // 
            this.ctxt14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt14.Location = new System.Drawing.Point(368, 6);
            this.ctxt14.MaxLength = 8;
            this.ctxt14.Name = "ctxt14";
            this.ctxt14.ReadOnly = true;
            this.ctxt14.ShortcutsEnabled = false;
            this.ctxt14.Size = new System.Drawing.Size(54, 26);
            this.ctxt14.TabIndex = 195;
            this.ctxt14.TabStop = false;
            this.ctxt14.Text = "451";
            this.ctxt14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl15
            // 
            this.clbl15.AutoSize = true;
            this.clbl15.BackColor = System.Drawing.SystemColors.Window;
            this.clbl15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl15.ForeColor = System.Drawing.Color.Blue;
            this.clbl15.Location = new System.Drawing.Point(343, 41);
            this.clbl15.Name = "clbl15";
            this.clbl15.Size = new System.Drawing.Size(19, 20);
            this.clbl15.TabIndex = 199;
            this.clbl15.Text = "2";
            // 
            // clbl14
            // 
            this.clbl14.AutoSize = true;
            this.clbl14.BackColor = System.Drawing.SystemColors.Window;
            this.clbl14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl14.ForeColor = System.Drawing.Color.Blue;
            this.clbl14.Location = new System.Drawing.Point(342, 11);
            this.clbl14.Name = "clbl14";
            this.clbl14.Size = new System.Drawing.Size(19, 20);
            this.clbl14.TabIndex = 200;
            this.clbl14.Text = "1";
            // 
            // clbl16
            // 
            this.clbl16.AutoSize = true;
            this.clbl16.BackColor = System.Drawing.SystemColors.Window;
            this.clbl16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl16.ForeColor = System.Drawing.Color.Blue;
            this.clbl16.Location = new System.Drawing.Point(343, 69);
            this.clbl16.Name = "clbl16";
            this.clbl16.Size = new System.Drawing.Size(19, 20);
            this.clbl16.TabIndex = 202;
            this.clbl16.Text = "3";
            // 
            // clbl18
            // 
            this.clbl18.AutoSize = true;
            this.clbl18.BackColor = System.Drawing.SystemColors.Window;
            this.clbl18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl18.ForeColor = System.Drawing.Color.Blue;
            this.clbl18.Location = new System.Drawing.Point(341, 126);
            this.clbl18.Name = "clbl18";
            this.clbl18.Size = new System.Drawing.Size(19, 20);
            this.clbl18.TabIndex = 201;
            this.clbl18.Text = "5";
            // 
            // ctxt12
            // 
            this.ctxt12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt12.Location = new System.Drawing.Point(254, 152);
            this.ctxt12.MaxLength = 8;
            this.ctxt12.Name = "ctxt12";
            this.ctxt12.ReadOnly = true;
            this.ctxt12.ShortcutsEnabled = false;
            this.ctxt12.Size = new System.Drawing.Size(54, 26);
            this.ctxt12.TabIndex = 190;
            this.ctxt12.TabStop = false;
            this.ctxt12.Text = "532";
            this.ctxt12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt13
            // 
            this.ctxt13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt13.Location = new System.Drawing.Point(254, 180);
            this.ctxt13.MaxLength = 8;
            this.ctxt13.Name = "ctxt13";
            this.ctxt13.ReadOnly = true;
            this.ctxt13.ShortcutsEnabled = false;
            this.ctxt13.Size = new System.Drawing.Size(54, 26);
            this.ctxt13.TabIndex = 191;
            this.ctxt13.TabStop = false;
            this.ctxt13.Text = "542";
            this.ctxt13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt11
            // 
            this.ctxt11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt11.Location = new System.Drawing.Point(254, 123);
            this.ctxt11.MaxLength = 8;
            this.ctxt11.Name = "ctxt11";
            this.ctxt11.ReadOnly = true;
            this.ctxt11.ShortcutsEnabled = false;
            this.ctxt11.Size = new System.Drawing.Size(54, 26);
            this.ctxt11.TabIndex = 189;
            this.ctxt11.TabStop = false;
            this.ctxt11.Text = "522";
            this.ctxt11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl12
            // 
            this.clbl12.AutoSize = true;
            this.clbl12.BackColor = System.Drawing.SystemColors.Window;
            this.clbl12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl12.ForeColor = System.Drawing.Color.Blue;
            this.clbl12.Location = new System.Drawing.Point(233, 156);
            this.clbl12.Name = "clbl12";
            this.clbl12.Size = new System.Drawing.Size(19, 20);
            this.clbl12.TabIndex = 192;
            this.clbl12.Text = "6";
            // 
            // clbl11
            // 
            this.clbl11.AutoSize = true;
            this.clbl11.BackColor = System.Drawing.SystemColors.Window;
            this.clbl11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl11.ForeColor = System.Drawing.Color.Blue;
            this.clbl11.Location = new System.Drawing.Point(231, 126);
            this.clbl11.Name = "clbl11";
            this.clbl11.Size = new System.Drawing.Size(19, 20);
            this.clbl11.TabIndex = 193;
            this.clbl11.Text = "5";
            // 
            // clbl13
            // 
            this.clbl13.AutoSize = true;
            this.clbl13.BackColor = System.Drawing.SystemColors.Window;
            this.clbl13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl13.ForeColor = System.Drawing.Color.Blue;
            this.clbl13.Location = new System.Drawing.Point(233, 183);
            this.clbl13.Name = "clbl13";
            this.clbl13.Size = new System.Drawing.Size(19, 20);
            this.clbl13.TabIndex = 194;
            this.clbl13.Text = "7";
            // 
            // ctxt8
            // 
            this.ctxt8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt8.Location = new System.Drawing.Point(254, 36);
            this.ctxt8.MaxLength = 8;
            this.ctxt8.Name = "ctxt8";
            this.ctxt8.ReadOnly = true;
            this.ctxt8.ShortcutsEnabled = false;
            this.ctxt8.Size = new System.Drawing.Size(54, 26);
            this.ctxt8.TabIndex = 182;
            this.ctxt8.TabStop = false;
            this.ctxt8.Text = "422";
            this.ctxt8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt9
            // 
            this.ctxt9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt9.Location = new System.Drawing.Point(254, 65);
            this.ctxt9.MaxLength = 8;
            this.ctxt9.Name = "ctxt9";
            this.ctxt9.ReadOnly = true;
            this.ctxt9.ShortcutsEnabled = false;
            this.ctxt9.Size = new System.Drawing.Size(54, 26);
            this.ctxt9.TabIndex = 183;
            this.ctxt9.TabStop = false;
            this.ctxt9.Text = "432";
            this.ctxt9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt10
            // 
            this.ctxt10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt10.Location = new System.Drawing.Point(254, 94);
            this.ctxt10.MaxLength = 8;
            this.ctxt10.Name = "ctxt10";
            this.ctxt10.ReadOnly = true;
            this.ctxt10.ShortcutsEnabled = false;
            this.ctxt10.Size = new System.Drawing.Size(54, 26);
            this.ctxt10.TabIndex = 184;
            this.ctxt10.TabStop = false;
            this.ctxt10.Text = "512";
            this.ctxt10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt7
            // 
            this.ctxt7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt7.Location = new System.Drawing.Point(254, 7);
            this.ctxt7.MaxLength = 8;
            this.ctxt7.Name = "ctxt7";
            this.ctxt7.ReadOnly = true;
            this.ctxt7.ShortcutsEnabled = false;
            this.ctxt7.Size = new System.Drawing.Size(54, 26);
            this.ctxt7.TabIndex = 181;
            this.ctxt7.TabStop = false;
            this.ctxt7.Text = "412";
            this.ctxt7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl8
            // 
            this.clbl8.AutoSize = true;
            this.clbl8.BackColor = System.Drawing.SystemColors.Window;
            this.clbl8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl8.ForeColor = System.Drawing.Color.Blue;
            this.clbl8.Location = new System.Drawing.Point(232, 40);
            this.clbl8.Name = "clbl8";
            this.clbl8.Size = new System.Drawing.Size(19, 20);
            this.clbl8.TabIndex = 185;
            this.clbl8.Text = "2";
            // 
            // clbl7
            // 
            this.clbl7.AutoSize = true;
            this.clbl7.BackColor = System.Drawing.SystemColors.Window;
            this.clbl7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl7.ForeColor = System.Drawing.Color.Blue;
            this.clbl7.Location = new System.Drawing.Point(233, 12);
            this.clbl7.Name = "clbl7";
            this.clbl7.Size = new System.Drawing.Size(19, 20);
            this.clbl7.TabIndex = 186;
            this.clbl7.Text = "1";
            // 
            // clbl9
            // 
            this.clbl9.AutoSize = true;
            this.clbl9.BackColor = System.Drawing.SystemColors.Window;
            this.clbl9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl9.ForeColor = System.Drawing.Color.Blue;
            this.clbl9.Location = new System.Drawing.Point(232, 68);
            this.clbl9.Name = "clbl9";
            this.clbl9.Size = new System.Drawing.Size(19, 20);
            this.clbl9.TabIndex = 188;
            this.clbl9.Text = "3";
            // 
            // clbl10
            // 
            this.clbl10.AutoSize = true;
            this.clbl10.BackColor = System.Drawing.SystemColors.Window;
            this.clbl10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl10.ForeColor = System.Drawing.Color.Blue;
            this.clbl10.Location = new System.Drawing.Point(232, 97);
            this.clbl10.Name = "clbl10";
            this.clbl10.Size = new System.Drawing.Size(19, 20);
            this.clbl10.TabIndex = 187;
            this.clbl10.Text = "4";
            // 
            // ctxt4
            // 
            this.ctxt4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt4.Location = new System.Drawing.Point(148, 57);
            this.ctxt4.MaxLength = 8;
            this.ctxt4.Name = "ctxt4";
            this.ctxt4.ReadOnly = true;
            this.ctxt4.ShortcutsEnabled = false;
            this.ctxt4.Size = new System.Drawing.Size(54, 38);
            this.ctxt4.TabIndex = 8;
            this.ctxt4.TabStop = false;
            this.ctxt4.Text = "203";
            this.ctxt4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt5
            // 
            this.ctxt5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt5.Location = new System.Drawing.Point(148, 104);
            this.ctxt5.MaxLength = 8;
            this.ctxt5.Name = "ctxt5";
            this.ctxt5.ReadOnly = true;
            this.ctxt5.ShortcutsEnabled = false;
            this.ctxt5.Size = new System.Drawing.Size(54, 38);
            this.ctxt5.TabIndex = 9;
            this.ctxt5.TabStop = false;
            this.ctxt5.Text = "202";
            this.ctxt5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt6
            // 
            this.ctxt6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt6.Location = new System.Drawing.Point(148, 152);
            this.ctxt6.MaxLength = 8;
            this.ctxt6.Name = "ctxt6";
            this.ctxt6.ReadOnly = true;
            this.ctxt6.ShortcutsEnabled = false;
            this.ctxt6.Size = new System.Drawing.Size(54, 38);
            this.ctxt6.TabIndex = 10;
            this.ctxt6.TabStop = false;
            this.ctxt6.Text = "301";
            this.ctxt6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt3
            // 
            this.ctxt3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt3.Location = new System.Drawing.Point(148, 10);
            this.ctxt3.MaxLength = 8;
            this.ctxt3.Name = "ctxt3";
            this.ctxt3.ReadOnly = true;
            this.ctxt3.ShortcutsEnabled = false;
            this.ctxt3.Size = new System.Drawing.Size(54, 38);
            this.ctxt3.TabIndex = 7;
            this.ctxt3.TabStop = false;
            this.ctxt3.Text = "101";
            this.ctxt3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt2
            // 
            this.ctxt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt2.Location = new System.Drawing.Point(70, 11);
            this.ctxt2.MaxLength = 8;
            this.ctxt2.Name = "ctxt2";
            this.ctxt2.ReadOnly = true;
            this.ctxt2.ShortcutsEnabled = false;
            this.ctxt2.Size = new System.Drawing.Size(45, 38);
            this.ctxt2.TabIndex = 6;
            this.ctxt2.TabStop = false;
            this.ctxt2.Text = "2";
            this.ctxt2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt1
            // 
            this.ctxt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt1.Location = new System.Drawing.Point(8, 11);
            this.ctxt1.MaxLength = 8;
            this.ctxt1.Name = "ctxt1";
            this.ctxt1.ReadOnly = true;
            this.ctxt1.ShortcutsEnabled = false;
            this.ctxt1.Size = new System.Drawing.Size(43, 38);
            this.ctxt1.TabIndex = 5;
            this.ctxt1.TabStop = false;
            this.ctxt1.Text = "1";
            this.ctxt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl2
            // 
            this.clbl2.AutoSize = true;
            this.clbl2.BackColor = System.Drawing.SystemColors.Window;
            this.clbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl2.ForeColor = System.Drawing.Color.Blue;
            this.clbl2.Location = new System.Drawing.Point(81, 53);
            this.clbl2.Name = "clbl2";
            this.clbl2.Size = new System.Drawing.Size(24, 25);
            this.clbl2.TabIndex = 180;
            this.clbl2.Text = "2";
            // 
            // clbl4
            // 
            this.clbl4.AutoSize = true;
            this.clbl4.BackColor = System.Drawing.SystemColors.Window;
            this.clbl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl4.ForeColor = System.Drawing.Color.Blue;
            this.clbl4.Location = new System.Drawing.Point(121, 62);
            this.clbl4.Name = "clbl4";
            this.clbl4.Size = new System.Drawing.Size(24, 25);
            this.clbl4.TabIndex = 175;
            this.clbl4.Text = "2";
            // 
            // clbl1
            // 
            this.clbl1.AutoSize = true;
            this.clbl1.BackColor = System.Drawing.SystemColors.Window;
            this.clbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl1.ForeColor = System.Drawing.Color.Blue;
            this.clbl1.Location = new System.Drawing.Point(16, 53);
            this.clbl1.Name = "clbl1";
            this.clbl1.Size = new System.Drawing.Size(24, 25);
            this.clbl1.TabIndex = 179;
            this.clbl1.Text = "1";
            // 
            // clbl3
            // 
            this.clbl3.AutoSize = true;
            this.clbl3.BackColor = System.Drawing.SystemColors.Window;
            this.clbl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl3.ForeColor = System.Drawing.Color.Blue;
            this.clbl3.Location = new System.Drawing.Point(121, 16);
            this.clbl3.Name = "clbl3";
            this.clbl3.Size = new System.Drawing.Size(24, 25);
            this.clbl3.TabIndex = 176;
            this.clbl3.Text = "1";
            // 
            // clbl5
            // 
            this.clbl5.AutoSize = true;
            this.clbl5.BackColor = System.Drawing.SystemColors.Window;
            this.clbl5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl5.ForeColor = System.Drawing.Color.Blue;
            this.clbl5.Location = new System.Drawing.Point(121, 109);
            this.clbl5.Name = "clbl5";
            this.clbl5.Size = new System.Drawing.Size(24, 25);
            this.clbl5.TabIndex = 178;
            this.clbl5.Text = "3";
            // 
            // clbl6
            // 
            this.clbl6.AutoSize = true;
            this.clbl6.BackColor = System.Drawing.SystemColors.Window;
            this.clbl6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl6.ForeColor = System.Drawing.Color.Blue;
            this.clbl6.Location = new System.Drawing.Point(121, 157);
            this.clbl6.Name = "clbl6";
            this.clbl6.Size = new System.Drawing.Size(24, 25);
            this.clbl6.TabIndex = 177;
            this.clbl6.Text = "4";
            // 
            // frmCheckQC3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1356, 742);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.imageViewerTR2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.imageViewerTR1);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.txt3);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lblGroup3);
            this.Controls.Add(this.txt2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt1);
            this.Controls.Add(this.lblEntry3);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.lblbatchname);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblGroup2);
            this.Controls.Add(this.lblGroup1);
            this.Controls.Add(this.txt4);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.lblImage);
            this.Controls.Add(this.lblfilename);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lblEntry2);
            this.Controls.Add(this.lblEntry1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnin);
            this.Controls.Add(this.btnout);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnRL);
            this.Controls.Add(this.btnRR);
            this.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "frmCheckQC3";
            this.Text = "VCB - QC";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCheck_FormClosing);
            this.Load += new System.EventHandler(this.frmCheck_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCheck_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRL;
        private System.Windows.Forms.Button btnRR;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnout;
        private System.Windows.Forms.Button btnin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblEntry1;
        private System.Windows.Forms.Label lblEntry2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblfilename;
        private System.Windows.Forms.Label lblImage;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblGroup2;
        private System.Windows.Forms.Label lblGroup1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblbatchname;
        private System.Windows.Forms.Label label8;
        private System.ComponentModel.BackgroundWorker bgwGetImage;
        private System.ComponentModel.BackgroundWorker bgwGetImage1;
        private System.Windows.Forms.Label lblGroup3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblEntry3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox txt3;
        private System.Windows.Forms.RichTextBox txt2;
        private System.Windows.Forms.RichTextBox txt1;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.RichTextBox txt4;
        private ImageViewerTR.ImageViewerTR imageViewerTR1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label17;
        private ImageViewerTR.ImageViewerTR imageViewerTR2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox ctxt17;
        private System.Windows.Forms.Label clbl17;
        private System.Windows.Forms.TextBox ctxt20;
        private System.Windows.Forms.TextBox ctxt19;
        private System.Windows.Forms.Label clbl20;
        private System.Windows.Forms.Label clbl19;
        private System.Windows.Forms.TextBox ctxt15;
        private System.Windows.Forms.TextBox ctxt16;
        private System.Windows.Forms.TextBox ctxt18;
        private System.Windows.Forms.TextBox ctxt14;
        private System.Windows.Forms.Label clbl15;
        private System.Windows.Forms.Label clbl14;
        private System.Windows.Forms.Label clbl16;
        private System.Windows.Forms.Label clbl18;
        private System.Windows.Forms.TextBox ctxt12;
        private System.Windows.Forms.TextBox ctxt13;
        private System.Windows.Forms.TextBox ctxt11;
        private System.Windows.Forms.Label clbl12;
        private System.Windows.Forms.Label clbl11;
        private System.Windows.Forms.Label clbl13;
        private System.Windows.Forms.TextBox ctxt8;
        private System.Windows.Forms.TextBox ctxt9;
        private System.Windows.Forms.TextBox ctxt10;
        private System.Windows.Forms.TextBox ctxt7;
        private System.Windows.Forms.Label clbl8;
        private System.Windows.Forms.Label clbl7;
        private System.Windows.Forms.Label clbl9;
        private System.Windows.Forms.Label clbl10;
        private System.Windows.Forms.TextBox ctxt4;
        private System.Windows.Forms.TextBox ctxt5;
        private System.Windows.Forms.TextBox ctxt6;
        private System.Windows.Forms.TextBox ctxt3;
        private System.Windows.Forms.TextBox ctxt2;
        private System.Windows.Forms.TextBox ctxt1;
        private System.Windows.Forms.Label clbl2;
        private System.Windows.Forms.Label clbl4;
        private System.Windows.Forms.Label clbl1;
        private System.Windows.Forms.Label clbl3;
        private System.Windows.Forms.Label clbl5;
        private System.Windows.Forms.Label clbl6;
    }
}