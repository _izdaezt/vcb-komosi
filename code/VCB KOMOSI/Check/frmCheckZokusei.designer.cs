﻿namespace VCB_KOMOSI
{
    partial class frmCheckZokusei
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCheckZokusei));
            this.btnRL = new System.Windows.Forms.Button();
            this.btnRR = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnout = new System.Windows.Forms.Button();
            this.btnin = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblEntry1 = new System.Windows.Forms.Label();
            this.lblEntry2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblfilename = new System.Windows.Forms.Label();
            this.lblImage = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblGroup2 = new System.Windows.Forms.Label();
            this.lblGroup1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblbatchname = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.bgwGetImage1 = new System.ComponentModel.BackgroundWorker();
            this.txt1 = new System.Windows.Forms.RichTextBox();
            this.imageViewerTR1 = new ImageViewerTR.ImageViewerTR();
            this.timegetanh = new System.Windows.Forms.Timer(this.components);
            this.txt6 = new System.Windows.Forms.RichTextBox();
            this.txt11 = new System.Windows.Forms.RichTextBox();
            this.txt16 = new System.Windows.Forms.RichTextBox();
            this.txt17 = new System.Windows.Forms.RichTextBox();
            this.txt12 = new System.Windows.Forms.RichTextBox();
            this.txt7 = new System.Windows.Forms.RichTextBox();
            this.txt2 = new System.Windows.Forms.RichTextBox();
            this.txt18 = new System.Windows.Forms.RichTextBox();
            this.txt13 = new System.Windows.Forms.RichTextBox();
            this.txt8 = new System.Windows.Forms.RichTextBox();
            this.txt3 = new System.Windows.Forms.RichTextBox();
            this.txt19 = new System.Windows.Forms.RichTextBox();
            this.txt14 = new System.Windows.Forms.RichTextBox();
            this.txt9 = new System.Windows.Forms.RichTextBox();
            this.txt4 = new System.Windows.Forms.RichTextBox();
            this.txt20 = new System.Windows.Forms.RichTextBox();
            this.txt15 = new System.Windows.Forms.RichTextBox();
            this.txt10 = new System.Windows.Forms.RichTextBox();
            this.txt5 = new System.Windows.Forms.RichTextBox();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lbl9 = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.lbl11 = new System.Windows.Forms.Label();
            this.lbl12 = new System.Windows.Forms.Label();
            this.lbl13 = new System.Windows.Forms.Label();
            this.lbl14 = new System.Windows.Forms.Label();
            this.lbl15 = new System.Windows.Forms.Label();
            this.lbl16 = new System.Windows.Forms.Label();
            this.lbl17 = new System.Windows.Forms.Label();
            this.lbl18 = new System.Windows.Forms.Label();
            this.lbl19 = new System.Windows.Forms.Label();
            this.lbl20 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnRL
            // 
            this.btnRL.BackgroundImage = global::VCBHL_Entry.Properties.Resources.object_rotate_left;
            this.btnRL.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRL.Location = new System.Drawing.Point(85, 12);
            this.btnRL.Name = "btnRL";
            this.btnRL.Size = new System.Drawing.Size(29, 25);
            this.btnRL.TabIndex = 53;
            this.btnRL.UseVisualStyleBackColor = true;
            this.btnRL.Click += new System.EventHandler(this.btnRL_Click);
            // 
            // btnRR
            // 
            this.btnRR.BackgroundImage = global::VCBHL_Entry.Properties.Resources.object_rotate_right;
            this.btnRR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRR.Location = new System.Drawing.Point(50, 12);
            this.btnRR.Name = "btnRR";
            this.btnRR.Size = new System.Drawing.Size(29, 25);
            this.btnRR.TabIndex = 54;
            this.btnRR.UseVisualStyleBackColor = true;
            this.btnRR.Click += new System.EventHandler(this.btnRR_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(15, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 25);
            this.button1.TabIndex = 59;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnout
            // 
            this.btnout.BackgroundImage = global::VCBHL_Entry.Properties.Resources.Zoom_Out_icon;
            this.btnout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnout.Location = new System.Drawing.Point(120, 12);
            this.btnout.Name = "btnout";
            this.btnout.Size = new System.Drawing.Size(29, 25);
            this.btnout.TabIndex = 60;
            this.btnout.UseVisualStyleBackColor = true;
            this.btnout.Click += new System.EventHandler(this.btnout_Click);
            // 
            // btnin
            // 
            this.btnin.BackgroundImage = global::VCBHL_Entry.Properties.Resources.Zoom_In_icon;
            this.btnin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnin.Location = new System.Drawing.Point(155, 12);
            this.btnin.Name = "btnin";
            this.btnin.Size = new System.Drawing.Size(29, 25);
            this.btnin.TabIndex = 61;
            this.btnin.UseVisualStyleBackColor = true;
            this.btnin.Click += new System.EventHandler(this.btnin_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(992, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 62;
            this.label2.Text = "User1:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(992, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 16);
            this.label6.TabIndex = 63;
            this.label6.Text = "User2:";
            // 
            // lblEntry1
            // 
            this.lblEntry1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEntry1.AutoSize = true;
            this.lblEntry1.Location = new System.Drawing.Point(1036, 4);
            this.lblEntry1.Name = "lblEntry1";
            this.lblEntry1.Size = new System.Drawing.Size(0, 16);
            this.lblEntry1.TabIndex = 64;
            // 
            // lblEntry2
            // 
            this.lblEntry2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEntry2.AutoSize = true;
            this.lblEntry2.Location = new System.Drawing.Point(1036, 27);
            this.lblEntry2.Name = "lblEntry2";
            this.lblEntry2.Size = new System.Drawing.Size(0, 16);
            this.lblEntry2.TabIndex = 65;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(278, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 16);
            this.label10.TabIndex = 66;
            this.label10.Text = "Image Name:";
            // 
            // lblfilename
            // 
            this.lblfilename.AutoSize = true;
            this.lblfilename.ForeColor = System.Drawing.Color.Blue;
            this.lblfilename.Location = new System.Drawing.Point(355, 16);
            this.lblfilename.Name = "lblfilename";
            this.lblfilename.Size = new System.Drawing.Size(49, 16);
            this.lblfilename.TabIndex = 67;
            this.lblfilename.Text = "0001.jpg";
            // 
            // lblImage
            // 
            this.lblImage.AutoSize = true;
            this.lblImage.ForeColor = System.Drawing.Color.Blue;
            this.lblImage.Location = new System.Drawing.Point(897, 16);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(32, 16);
            this.lblImage.TabIndex = 94;
            this.lblImage.Text = "1211";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(852, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 16);
            this.label13.TabIndex = 93;
            this.label13.Text = "Count:";
            // 
            // lblGroup2
            // 
            this.lblGroup2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGroup2.AutoSize = true;
            this.lblGroup2.Location = new System.Drawing.Point(1226, 27);
            this.lblGroup2.Name = "lblGroup2";
            this.lblGroup2.Size = new System.Drawing.Size(0, 16);
            this.lblGroup2.TabIndex = 98;
            // 
            // lblGroup1
            // 
            this.lblGroup1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGroup1.AutoSize = true;
            this.lblGroup1.Location = new System.Drawing.Point(1226, 4);
            this.lblGroup1.Name = "lblGroup1";
            this.lblGroup1.Size = new System.Drawing.Size(0, 16);
            this.lblGroup1.TabIndex = 97;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1160, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 16);
            this.label4.TabIndex = 96;
            this.label4.Text = "Trung tâm:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1157, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 16);
            this.label5.TabIndex = 95;
            this.label5.Text = "Trung tâm:";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSubmit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSubmit.BackgroundImage")));
            this.btnSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSubmit.Location = new System.Drawing.Point(620, 590);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(258, 125);
            this.btnSubmit.TabIndex = 114;
            this.btnSubmit.TabStop = false;
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBack.BackgroundImage")));
            this.btnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBack.Location = new System.Drawing.Point(293, 644);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(129, 71);
            this.btnBack.TabIndex = 115;
            this.btnBack.TabStop = false;
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblbatchname
            // 
            this.lblbatchname.AutoSize = true;
            this.lblbatchname.ForeColor = System.Drawing.Color.Blue;
            this.lblbatchname.Location = new System.Drawing.Point(596, 16);
            this.lblbatchname.Name = "lblbatchname";
            this.lblbatchname.Size = new System.Drawing.Size(49, 16);
            this.lblbatchname.TabIndex = 135;
            this.lblbatchname.Text = "0001.jpg";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(519, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 16);
            this.label8.TabIndex = 134;
            this.label8.Text = "Batch Name:";
            // 
            // txt1
            // 
            this.txt1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt1.ForeColor = System.Drawing.Color.Red;
            this.txt1.Location = new System.Drawing.Point(689, 95);
            this.txt1.Multiline = false;
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(100, 80);
            this.txt1.TabIndex = 1;
            this.txt1.Text = "";
            // 
            // imageViewerTR1
            // 
            this.imageViewerTR1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.imageViewerTR1.BackColor = System.Drawing.Color.LightGray;
            this.imageViewerTR1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imageViewerTR1.CurrentZoom = 1F;
            this.imageViewerTR1.Image = null;
            this.imageViewerTR1.Location = new System.Drawing.Point(28, 68);
            this.imageViewerTR1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.imageViewerTR1.MaxZoom = 20F;
            this.imageViewerTR1.MinZoom = 0.05F;
            this.imageViewerTR1.Name = "imageViewerTR1";
            this.imageViewerTR1.Size = new System.Drawing.Size(559, 493);
            this.imageViewerTR1.TabIndex = 146;
            // 
            // timegetanh
            // 
            this.timegetanh.Interval = 200;
            this.timegetanh.Tick += new System.EventHandler(this.timegetanh_Tick);
            // 
            // txt6
            // 
            this.txt6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt6.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6.ForeColor = System.Drawing.Color.Red;
            this.txt6.Location = new System.Drawing.Point(689, 214);
            this.txt6.Multiline = false;
            this.txt6.Name = "txt6";
            this.txt6.Size = new System.Drawing.Size(100, 80);
            this.txt6.TabIndex = 6;
            this.txt6.Text = "";
            // 
            // txt11
            // 
            this.txt11.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt11.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt11.ForeColor = System.Drawing.Color.Red;
            this.txt11.Location = new System.Drawing.Point(689, 333);
            this.txt11.Multiline = false;
            this.txt11.Name = "txt11";
            this.txt11.Size = new System.Drawing.Size(100, 80);
            this.txt11.TabIndex = 11;
            this.txt11.Text = "";
            // 
            // txt16
            // 
            this.txt16.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt16.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt16.ForeColor = System.Drawing.Color.Red;
            this.txt16.Location = new System.Drawing.Point(689, 448);
            this.txt16.Multiline = false;
            this.txt16.Name = "txt16";
            this.txt16.Size = new System.Drawing.Size(100, 80);
            this.txt16.TabIndex = 16;
            this.txt16.Text = "";
            // 
            // txt17
            // 
            this.txt17.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt17.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt17.ForeColor = System.Drawing.Color.Red;
            this.txt17.Location = new System.Drawing.Point(814, 448);
            this.txt17.Multiline = false;
            this.txt17.Name = "txt17";
            this.txt17.Size = new System.Drawing.Size(100, 80);
            this.txt17.TabIndex = 17;
            this.txt17.Text = "";
            // 
            // txt12
            // 
            this.txt12.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt12.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt12.ForeColor = System.Drawing.Color.Red;
            this.txt12.Location = new System.Drawing.Point(814, 333);
            this.txt12.Multiline = false;
            this.txt12.Name = "txt12";
            this.txt12.Size = new System.Drawing.Size(100, 80);
            this.txt12.TabIndex = 12;
            this.txt12.Text = "";
            // 
            // txt7
            // 
            this.txt7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt7.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt7.ForeColor = System.Drawing.Color.Red;
            this.txt7.Location = new System.Drawing.Point(814, 214);
            this.txt7.Multiline = false;
            this.txt7.Name = "txt7";
            this.txt7.Size = new System.Drawing.Size(100, 80);
            this.txt7.TabIndex = 7;
            this.txt7.Text = "";
            // 
            // txt2
            // 
            this.txt2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt2.ForeColor = System.Drawing.Color.Red;
            this.txt2.Location = new System.Drawing.Point(814, 95);
            this.txt2.Multiline = false;
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(100, 80);
            this.txt2.TabIndex = 2;
            this.txt2.Text = "";
            // 
            // txt18
            // 
            this.txt18.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt18.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt18.ForeColor = System.Drawing.Color.Red;
            this.txt18.Location = new System.Drawing.Point(939, 448);
            this.txt18.Multiline = false;
            this.txt18.Name = "txt18";
            this.txt18.Size = new System.Drawing.Size(100, 80);
            this.txt18.TabIndex = 18;
            this.txt18.Text = "";
            // 
            // txt13
            // 
            this.txt13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt13.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt13.ForeColor = System.Drawing.Color.Red;
            this.txt13.Location = new System.Drawing.Point(939, 333);
            this.txt13.Multiline = false;
            this.txt13.Name = "txt13";
            this.txt13.Size = new System.Drawing.Size(100, 80);
            this.txt13.TabIndex = 13;
            this.txt13.Text = "";
            // 
            // txt8
            // 
            this.txt8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt8.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8.ForeColor = System.Drawing.Color.Red;
            this.txt8.Location = new System.Drawing.Point(939, 214);
            this.txt8.Multiline = false;
            this.txt8.Name = "txt8";
            this.txt8.Size = new System.Drawing.Size(100, 80);
            this.txt8.TabIndex = 8;
            this.txt8.Text = "";
            // 
            // txt3
            // 
            this.txt3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt3.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3.ForeColor = System.Drawing.Color.Red;
            this.txt3.Location = new System.Drawing.Point(939, 95);
            this.txt3.Multiline = false;
            this.txt3.Name = "txt3";
            this.txt3.Size = new System.Drawing.Size(100, 80);
            this.txt3.TabIndex = 3;
            this.txt3.Text = "";
            // 
            // txt19
            // 
            this.txt19.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt19.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt19.ForeColor = System.Drawing.Color.Red;
            this.txt19.Location = new System.Drawing.Point(1061, 448);
            this.txt19.Multiline = false;
            this.txt19.Name = "txt19";
            this.txt19.Size = new System.Drawing.Size(168, 80);
            this.txt19.TabIndex = 19;
            this.txt19.Text = "";
            // 
            // txt14
            // 
            this.txt14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt14.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt14.ForeColor = System.Drawing.Color.Red;
            this.txt14.Location = new System.Drawing.Point(1061, 333);
            this.txt14.Multiline = false;
            this.txt14.Name = "txt14";
            this.txt14.Size = new System.Drawing.Size(168, 80);
            this.txt14.TabIndex = 14;
            this.txt14.Text = "";
            // 
            // txt9
            // 
            this.txt9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt9.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt9.ForeColor = System.Drawing.Color.Red;
            this.txt9.Location = new System.Drawing.Point(1061, 214);
            this.txt9.Multiline = false;
            this.txt9.Name = "txt9";
            this.txt9.Size = new System.Drawing.Size(168, 80);
            this.txt9.TabIndex = 9;
            this.txt9.Text = "";
            // 
            // txt4
            // 
            this.txt4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt4.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt4.ForeColor = System.Drawing.Color.Red;
            this.txt4.Location = new System.Drawing.Point(1061, 95);
            this.txt4.Multiline = false;
            this.txt4.Name = "txt4";
            this.txt4.Size = new System.Drawing.Size(168, 80);
            this.txt4.TabIndex = 4;
            this.txt4.Text = "";
            // 
            // txt20
            // 
            this.txt20.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt20.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt20.ForeColor = System.Drawing.Color.Red;
            this.txt20.Location = new System.Drawing.Point(1244, 448);
            this.txt20.Multiline = false;
            this.txt20.Name = "txt20";
            this.txt20.Size = new System.Drawing.Size(100, 80);
            this.txt20.TabIndex = 20;
            this.txt20.Text = "";
            // 
            // txt15
            // 
            this.txt15.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt15.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt15.ForeColor = System.Drawing.Color.Red;
            this.txt15.Location = new System.Drawing.Point(1244, 333);
            this.txt15.Multiline = false;
            this.txt15.Name = "txt15";
            this.txt15.Size = new System.Drawing.Size(100, 80);
            this.txt15.TabIndex = 15;
            this.txt15.Text = "";
            // 
            // txt10
            // 
            this.txt10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt10.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt10.ForeColor = System.Drawing.Color.Red;
            this.txt10.Location = new System.Drawing.Point(1244, 214);
            this.txt10.Multiline = false;
            this.txt10.Name = "txt10";
            this.txt10.Size = new System.Drawing.Size(100, 80);
            this.txt10.TabIndex = 10;
            this.txt10.Text = "";
            // 
            // txt5
            // 
            this.txt5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txt5.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt5.ForeColor = System.Drawing.Color.Red;
            this.txt5.Location = new System.Drawing.Point(1244, 95);
            this.txt5.Multiline = false;
            this.txt5.Name = "txt5";
            this.txt5.Size = new System.Drawing.Size(100, 80);
            this.txt5.TabIndex = 5;
            this.txt5.Text = "";
            // 
            // lbl1
            // 
            this.lbl1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.Location = new System.Drawing.Point(752, 63);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(37, 29);
            this.lbl1.TabIndex = 147;
            this.lbl1.Text = "E1";
            // 
            // lbl2
            // 
            this.lbl2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl2.AutoSize = true;
            this.lbl2.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.Location = new System.Drawing.Point(877, 63);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(37, 29);
            this.lbl2.TabIndex = 148;
            this.lbl2.Text = "E1";
            // 
            // lbl3
            // 
            this.lbl3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl3.AutoSize = true;
            this.lbl3.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl3.Location = new System.Drawing.Point(1002, 63);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(37, 29);
            this.lbl3.TabIndex = 149;
            this.lbl3.Text = "E1";
            // 
            // lbl4
            // 
            this.lbl4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl4.AutoSize = true;
            this.lbl4.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl4.Location = new System.Drawing.Point(1189, 63);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(37, 29);
            this.lbl4.TabIndex = 150;
            this.lbl4.Text = "E1";
            // 
            // lbl5
            // 
            this.lbl5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl5.AutoSize = true;
            this.lbl5.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5.Location = new System.Drawing.Point(1307, 63);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(37, 29);
            this.lbl5.TabIndex = 151;
            this.lbl5.Text = "E1";
            // 
            // lbl6
            // 
            this.lbl6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl6.AutoSize = true;
            this.lbl6.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl6.Location = new System.Drawing.Point(752, 182);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(37, 29);
            this.lbl6.TabIndex = 152;
            this.lbl6.Text = "E1";
            // 
            // lbl7
            // 
            this.lbl7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl7.AutoSize = true;
            this.lbl7.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl7.Location = new System.Drawing.Point(877, 182);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(37, 29);
            this.lbl7.TabIndex = 153;
            this.lbl7.Text = "E1";
            // 
            // lbl8
            // 
            this.lbl8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl8.AutoSize = true;
            this.lbl8.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl8.Location = new System.Drawing.Point(1002, 182);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(37, 29);
            this.lbl8.TabIndex = 154;
            this.lbl8.Text = "E1";
            // 
            // lbl9
            // 
            this.lbl9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl9.AutoSize = true;
            this.lbl9.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl9.Location = new System.Drawing.Point(1189, 182);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(37, 29);
            this.lbl9.TabIndex = 155;
            this.lbl9.Text = "E1";
            // 
            // lbl10
            // 
            this.lbl10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl10.AutoSize = true;
            this.lbl10.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl10.Location = new System.Drawing.Point(1307, 182);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(37, 29);
            this.lbl10.TabIndex = 156;
            this.lbl10.Text = "E1";
            // 
            // lbl11
            // 
            this.lbl11.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl11.AutoSize = true;
            this.lbl11.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl11.Location = new System.Drawing.Point(752, 301);
            this.lbl11.Name = "lbl11";
            this.lbl11.Size = new System.Drawing.Size(37, 29);
            this.lbl11.TabIndex = 157;
            this.lbl11.Text = "E1";
            // 
            // lbl12
            // 
            this.lbl12.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl12.AutoSize = true;
            this.lbl12.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl12.Location = new System.Drawing.Point(877, 301);
            this.lbl12.Name = "lbl12";
            this.lbl12.Size = new System.Drawing.Size(37, 29);
            this.lbl12.TabIndex = 158;
            this.lbl12.Text = "E1";
            // 
            // lbl13
            // 
            this.lbl13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl13.AutoSize = true;
            this.lbl13.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl13.Location = new System.Drawing.Point(1002, 301);
            this.lbl13.Name = "lbl13";
            this.lbl13.Size = new System.Drawing.Size(37, 29);
            this.lbl13.TabIndex = 159;
            this.lbl13.Text = "E1";
            // 
            // lbl14
            // 
            this.lbl14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl14.AutoSize = true;
            this.lbl14.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl14.Location = new System.Drawing.Point(1192, 301);
            this.lbl14.Name = "lbl14";
            this.lbl14.Size = new System.Drawing.Size(37, 29);
            this.lbl14.TabIndex = 160;
            this.lbl14.Text = "E1";
            // 
            // lbl15
            // 
            this.lbl15.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl15.AutoSize = true;
            this.lbl15.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl15.Location = new System.Drawing.Point(1307, 301);
            this.lbl15.Name = "lbl15";
            this.lbl15.Size = new System.Drawing.Size(37, 29);
            this.lbl15.TabIndex = 161;
            this.lbl15.Text = "E1";
            // 
            // lbl16
            // 
            this.lbl16.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl16.AutoSize = true;
            this.lbl16.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl16.Location = new System.Drawing.Point(752, 416);
            this.lbl16.Name = "lbl16";
            this.lbl16.Size = new System.Drawing.Size(37, 29);
            this.lbl16.TabIndex = 162;
            this.lbl16.Text = "E1";
            // 
            // lbl17
            // 
            this.lbl17.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl17.AutoSize = true;
            this.lbl17.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl17.Location = new System.Drawing.Point(877, 416);
            this.lbl17.Name = "lbl17";
            this.lbl17.Size = new System.Drawing.Size(37, 29);
            this.lbl17.TabIndex = 163;
            this.lbl17.Text = "E1";
            // 
            // lbl18
            // 
            this.lbl18.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl18.AutoSize = true;
            this.lbl18.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl18.Location = new System.Drawing.Point(1002, 416);
            this.lbl18.Name = "lbl18";
            this.lbl18.Size = new System.Drawing.Size(37, 29);
            this.lbl18.TabIndex = 164;
            this.lbl18.Text = "E1";
            // 
            // lbl19
            // 
            this.lbl19.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl19.AutoSize = true;
            this.lbl19.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl19.Location = new System.Drawing.Point(1192, 416);
            this.lbl19.Name = "lbl19";
            this.lbl19.Size = new System.Drawing.Size(37, 29);
            this.lbl19.TabIndex = 165;
            this.lbl19.Text = "E1";
            // 
            // lbl20
            // 
            this.lbl20.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl20.AutoSize = true;
            this.lbl20.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl20.Location = new System.Drawing.Point(1307, 416);
            this.lbl20.Name = "lbl20";
            this.lbl20.Size = new System.Drawing.Size(37, 29);
            this.lbl20.TabIndex = 166;
            this.lbl20.Text = "E1";
            // 
            // frmCheckZokusei
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1356, 742);
            this.Controls.Add(this.lbl20);
            this.Controls.Add(this.lbl19);
            this.Controls.Add(this.lbl18);
            this.Controls.Add(this.lbl17);
            this.Controls.Add(this.lbl16);
            this.Controls.Add(this.lbl15);
            this.Controls.Add(this.lbl14);
            this.Controls.Add(this.lbl13);
            this.Controls.Add(this.lbl12);
            this.Controls.Add(this.lbl11);
            this.Controls.Add(this.lbl10);
            this.Controls.Add(this.lbl9);
            this.Controls.Add(this.lbl8);
            this.Controls.Add(this.lbl7);
            this.Controls.Add(this.lbl6);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.txt20);
            this.Controls.Add(this.txt15);
            this.Controls.Add(this.txt10);
            this.Controls.Add(this.txt5);
            this.Controls.Add(this.txt19);
            this.Controls.Add(this.txt14);
            this.Controls.Add(this.txt9);
            this.Controls.Add(this.txt4);
            this.Controls.Add(this.txt18);
            this.Controls.Add(this.txt13);
            this.Controls.Add(this.txt8);
            this.Controls.Add(this.txt3);
            this.Controls.Add(this.txt17);
            this.Controls.Add(this.txt12);
            this.Controls.Add(this.txt7);
            this.Controls.Add(this.txt2);
            this.Controls.Add(this.txt16);
            this.Controls.Add(this.txt11);
            this.Controls.Add(this.txt6);
            this.Controls.Add(this.imageViewerTR1);
            this.Controls.Add(this.txt1);
            this.Controls.Add(this.lblbatchname);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.lblGroup2);
            this.Controls.Add(this.lblGroup1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblImage);
            this.Controls.Add(this.lblfilename);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lblEntry2);
            this.Controls.Add(this.lblEntry1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnin);
            this.Controls.Add(this.btnout);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnRL);
            this.Controls.Add(this.btnRR);
            this.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "frmCheckZokusei";
            this.Text = "VCB - Check";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCheck_FormClosing);
            this.Load += new System.EventHandler(this.frmCheck_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCheck_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRL;
        private System.Windows.Forms.Button btnRR;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnout;
        private System.Windows.Forms.Button btnin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblEntry1;
        private System.Windows.Forms.Label lblEntry2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblfilename;
        private System.Windows.Forms.Label lblImage;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblGroup2;
        private System.Windows.Forms.Label lblGroup1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblbatchname;
        private System.Windows.Forms.Label label8;
        private System.ComponentModel.BackgroundWorker bgwGetImage1;
        private System.Windows.Forms.RichTextBox txt1;
        private ImageViewerTR.ImageViewerTR imageViewerTR1;
        private System.Windows.Forms.Timer timegetanh;
        private System.Windows.Forms.RichTextBox txt6;
        private System.Windows.Forms.RichTextBox txt11;
        private System.Windows.Forms.RichTextBox txt16;
        private System.Windows.Forms.RichTextBox txt17;
        private System.Windows.Forms.RichTextBox txt12;
        private System.Windows.Forms.RichTextBox txt7;
        private System.Windows.Forms.RichTextBox txt2;
        private System.Windows.Forms.RichTextBox txt18;
        private System.Windows.Forms.RichTextBox txt13;
        private System.Windows.Forms.RichTextBox txt8;
        private System.Windows.Forms.RichTextBox txt3;
        private System.Windows.Forms.RichTextBox txt19;
        private System.Windows.Forms.RichTextBox txt14;
        private System.Windows.Forms.RichTextBox txt9;
        private System.Windows.Forms.RichTextBox txt4;
        private System.Windows.Forms.RichTextBox txt20;
        private System.Windows.Forms.RichTextBox txt15;
        private System.Windows.Forms.RichTextBox txt10;
        private System.Windows.Forms.RichTextBox txt5;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lbl9;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.Label lbl11;
        private System.Windows.Forms.Label lbl12;
        private System.Windows.Forms.Label lbl13;
        private System.Windows.Forms.Label lbl14;
        private System.Windows.Forms.Label lbl15;
        private System.Windows.Forms.Label lbl16;
        private System.Windows.Forms.Label lbl17;
        private System.Windows.Forms.Label lbl18;
        private System.Windows.Forms.Label lbl19;
        private System.Windows.Forms.Label lbl20;
    }
}