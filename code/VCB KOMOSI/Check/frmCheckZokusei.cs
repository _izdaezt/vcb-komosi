﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
namespace VCB_KOMOSI
{
    public partial class frmCheckZokusei : Form
    {
        public int formId;
        public int batchId;
        public int userId;
        public string userName;
        public string batchName;
        public string template;
        public string level;
        Bitmap imageSource;
        string[] arrsh = new string[0];
        string[] arrsl = new string[0];
        string[] arrgm = new string[0];
        string[] arrgb = new string[0];
        private int numbertrtxt = 0;
        private DAEntry_Check dACheck = new DAEntry_Check();
        private BOImage_Check img = new BOImage_Check();
        private BOImageContent_Check imgContent = new BOImageContent_Check();
        List<BOImage_Check> ListImage = new List<BOImage_Check>();
        private bool finish = false;
        private System.Windows.Forms.TextBox txtAutoComplete = new System.Windows.Forms.TextBox();
        DataTable dthct = new DataTable();
        int id = 0;
        float currentzoom;
        int saveimageid = 0;
        public string[] INFperformance = new string[2];
        List<RichTextBox> lsttxt;
        List<Label> lstlbl = new List<Label>();
        List<Image> lstimageload = new List<Image>();
        //nam 
        string type = "";
        private DAEntry_Entry dAEntry = new DAEntry_Entry();
        string batchnameLocal;
        public string typeCheck = "";
        DateTime dtimeBefore = new DateTime();
        string tableform;
        string ocrSBD = "";
        string ocrNTN = "";
        int typeyear = 0;
        public frmCheckZokusei()
        {
            InitializeComponent();
            lsttxt = new List<RichTextBox>() { txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9, txt10, txt11, txt12, txt13, txt14, txt15, txt16, txt17, txt18, txt19, txt20 };
            lstlbl = new List<Label>() { lbl1, lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8, lbl9, lbl10, lbl11, lbl12, lbl13, lbl14, lbl15, lbl16, lbl17, lbl18, lbl19, lbl20 };
        }
        private void frmCheck_Load(object sender, EventArgs e)
        {
            // set bien toan cuc         
            lsttxt.ForEach(a =>
            {
                a.Enter += new System.EventHandler(this.textBox1_Enter);
                a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
                a.Leave += new System.EventHandler(this.textBox1_Leave);
                a.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
                a.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
                a.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rtxt_MouseDown);
            });


            batchnameLocal = batchName.Substring(7);

            tableform = batchId + "--" + batchnameLocal;


            // lấy id image đã entry để check            
            // check đã entry
            try
            {
                if (typeCheck == "Check")
                {
                    id = dACheck.GetIdCheck(tableform);
                }
                else
                {
                    id = dACheck.GetIdCheckQC(tableform);
                }
            }
            catch
            {
                finish = true;
                this.Close();
            }

            if (id == 0)
            {
                //nam 4/5/17 kt với a thi tăng hitpoit
                if (dAEntry.Count_UpHitpointLC(tableform) == 0)
                { dAEntry.UpdateHitpoint_Batch(batchId, 2); }
                finish = true;
                this.Close();
                return;
            }

            timegetanh.Start();

            // lay image đã entry
            try
            {
                img = dACheck.GetImage(id, tableform);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            //nam cat anh
            imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageName, batchnameLocal)));
            Bitmap bm = CropBitmap(imageSource, imageSource.Width / 2, 0, imageSource.Width / 2, imageSource.Height / 2);
            imageViewerTR1.Image = bm;
            currentzoom = imageViewerTR1.CurrentZoom;

            try
            {
                imgContent = dACheck.GetImageContent(id, tableform);
            }
            catch
            {
                this.Close();
                return;
            }

            for (int i = 0; i < imgContent.Content1.Count; i++)
            {
                lstlbl[i].BackColor = SystemColors.Window;
                lsttxt[i].ForeColor = Color.Red;
                lsttxt[i].Text = imgContent.Content1[i];
                numbertrtxt = i;
                string s1, s2;
                s1 = imgContent.Content1[i];
                s2 = imgContent.Content2[i];
                lstlbl[i].Text = "E1";
                if (s1 != s2)
                {
                    lstlbl[i].BackColor = Color.Red;
                }
                c = null;
                c = new int[s1.Length + 1, s2.Length + 1];
                LCS(s1, s2);
                BackTrack(s1, s2, s1.Length, s2.Length);
            }

            if (img.NG1 != 0 && typeCheck == "QC")
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0 && typeCheck == "QC")
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;

            // hiển thị user name của user đã entry            
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblbatchname.Text = batchName;
            dtimeBefore = DateTime.Now;
            if (typeCheck == "QC")
                lblImage.Text = dACheck.ImageExistQC(tableform);
            else
                lblImage.Text = dACheck.ImageExistCheck(tableform);
            txt1.Focus();
        }
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCheck_FormClosing(object sender, FormClosingEventArgs e)
        {
            timegetanh.Stop();
            try
            {
                if (!finish)
                {
                    // unlock image
                    if (typeCheck == "Check")
                    {
                        dACheck.SetHitPointImageCheck(img.Id, tableform);
                        // unlock image       
                        for (int i = 0; i < ListImage.Count; i++)
                        {
                            dACheck.SetHitPointImageCheck(ListImage[i].Id, tableform);
                        }
                    }
                    else
                    {
                        dACheck.SetHitPointImageCheckQC(img.Id, tableform);
                        // unlock image       
                        for (int i = 0; i < ListImage.Count; i++)
                        {
                            dACheck.SetHitPointImageCheckQC(ListImage[i].Id, tableform);
                        }
                    }
                }
                else
                {
                    frmLogIn.fn = true;
                    //dACheck.UpdateHitpoint_Batch(batchId, 2);
                }
            }
            catch { }
        }
        private void btnRL_Click(object sender, EventArgs e)
        {
            imageViewerTR1.RotateImage("90");
        }

        private void btnRR_Click(object sender, EventArgs e)
        {
            imageViewerTR1.RotateImage("270");
        }

        #region Compare string
        static int max(int a, int b)
        {
            return (a > b) ? a : b;
        }
        static int[,] c;
        //Prints one LCS
        private string BackTrack(string s1, string s2, int i, int j)
        {
            if (i == 0 || j == 0)
                return "";
            if (s1[i - 1] == s2[j - 1])
            {
                lsttxt[numbertrtxt].SelectionStart = i - 1;
                lsttxt[numbertrtxt].SelectionLength = 1;
                lsttxt[numbertrtxt].SelectionColor = Color.Black;
                return BackTrack(s1, s2, i - 1, j - 1) + s1[i - 1];
            }
            else if (c[i - 1, j] > c[i, j - 1])
                return BackTrack(s1, s2, i - 1, j);

            else
                return BackTrack(s1, s2, i, j - 1);

        }
        //Nghịch       
        private string BackTrack2(string s1, string s2, int i, int j)
        {
            if (i == 0 || j == 0)
                return "";
            if (s1[i - 1] == s2[j - 1])
            {
                return BackTrack2(s1, s2, i - 1, j - 1) + s1[i - 1];
            }
            else if (c[i - 1, j] > c[i, j - 1])
                return BackTrack2(s1, s2, i - 1, j);

            else
                return BackTrack2(s1, s2, i, j - 1);

        }
        static int LCS(string s1, string s2)
        {
            for (int i = 1; i <= s1.Length; i++)
                c[i, 0] = 0;
            for (int i = 1; i <= s2.Length; i++)
                c[0, i] = 0;

            for (int i = 1; i <= s1.Length; i++)
                for (int j = 1; j <= s2.Length; j++)
                {
                    if (s1[i - 1] == s2[j - 1])
                        c[i, j] = c[i - 1, j - 1] + 1;
                    else
                    {
                        c[i, j] = max(c[i - 1, j], c[i, j - 1]);

                    }

                }

            return c[s1.Length, s2.Length];

        }
        #endregion
        private void frmCheck_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                if (e.KeyCode == Keys.Up)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Down)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    imageViewerTR1.RotateImage("270");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Left)
                {
                    imageViewerTR1.RotateImage("90");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.F)
                {
                    imageViewerTR1.CurrentZoom = currentzoom;
                    e.Handled = true;
                }
            }
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void btnout_Click(object sender, EventArgs e)
        {
            imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
        }

        private void btnin_Click(object sender, EventArgs e)
        {
            imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
        }
        private void textBox1_Enter(object sender, EventArgs e)
        {

        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {

        }
        private void textBox1_Leave(object sender, EventArgs e)
        {

        }
        private void rtxt_MouseDown(object sender, MouseEventArgs e)
        {
            RichTextBox rtxt = (RichTextBox)sender;
            if (e.Button == MouseButtons.Right)
            {
                if (rtxt.Text == imgContent.Content1[rtxt.TabIndex - 1])
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content2[rtxt.TabIndex - 1];
                    if (lstlbl[rtxt.TabIndex - 1].Text.Contains("E1"))
                    {
                        lstlbl[rtxt.TabIndex - 1].Text = lstlbl[rtxt.TabIndex - 1].Text.Replace("E1", "E2");
                    }
                    else
                    {
                        lstlbl[rtxt.TabIndex - 1].Text = lstlbl[rtxt.TabIndex - 1].Text.Replace("E2", "E1");
                    }
                    numbertrtxt = rtxt.TabIndex - 1;
                    string s1, s2;
                    s1 = imgContent.Content2[rtxt.TabIndex - 1];
                    s2 = imgContent.Content1[rtxt.TabIndex - 1];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                else if (rtxt.Text == imgContent.Content2[rtxt.TabIndex - 1])
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1[rtxt.TabIndex - 1];
                    lstlbl[rtxt.TabIndex - 1].Text = lstlbl[rtxt.TabIndex - 1].Text.Replace("E2", "E1");
                    numbertrtxt = rtxt.TabIndex - 1;
                    string s1, s2;
                    s1 = imgContent.Content1[rtxt.TabIndex - 1];
                    s2 = imgContent.Content2[rtxt.TabIndex - 1];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                else
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1[rtxt.TabIndex - 1];
                    lstlbl[rtxt.TabIndex - 1].Text = lstlbl[rtxt.TabIndex - 1].Text.Replace("E2", "E1");
                    numbertrtxt = rtxt.TabIndex - 1;
                    string s1, s2;
                    s1 = imgContent.Content1[rtxt.TabIndex - 1];
                    s2 = imgContent.Content2[rtxt.TabIndex - 1];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
            }
        }

        static bool IsValid(string value)
        {
            return Regex.IsMatch(value, @"^[0-9.* ]*$");
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            btnSubmit.Enabled = false;
            imageViewerTR1.Dispose();
            // update image content vao DB
            string rsEntr1 = "", rsEntr2 = "";
            rsEntr1 = String.Join("|", imgContent.Content1);
            rsEntr2 = String.Join("|", imgContent.Content2);
            string rsCheck = "";
            if (!IsValid(txt1.Text))
                txt1.Text = "";
            if (!IsValid(txt2.Text))
                txt2.Text = "";
            if (!IsValid(txt3.Text))
                txt3.Text = "";
            if (txt4.Text.Length == 4)
            {
                if (!IsValid(txt4.Text) || (txt4.Text[0] != ' ' && txt4.Text[1] == ' ' && txt4.Text[2] == ' ' && txt4.Text[3] != ' ') || (txt4.Text[0] != ' ' && txt4.Text[1] == ' ' && txt4.Text[2] != ' ' && txt4.Text[3] != ' ') || (txt4.Text[0] != ' ' && txt4.Text[1] != ' ' && txt4.Text[2] == ' ' && txt4.Text[3] != ' '))
                    txt4.Text = "";
            }
            else if (txt4.Text.Length > 4)
            {
                txt4.Text = "";
            }
            else if (txt4.Text.Length == 3)
            {
                if (!IsValid(txt4.Text) || (txt4.Text[0] != ' ' && txt4.Text[1] == ' ' && txt4.Text[2] != ' '))
                    txt4.Text = "";
            }
            else
            {
                if (!IsValid(txt4.Text))
                    txt4.Text = "";
            }
            if (!IsValid(txt5.Text))
                txt5.Text = "";
            if (!IsValid(txt6.Text))
                txt6.Text = "";
            if (!IsValid(txt7.Text))
                txt7.Text = "";
            if (!IsValid(txt8.Text))
                txt8.Text = "";
            if (txt9.Text.Length == 4)
            {
                if (!IsValid(txt9.Text) || (txt9.Text[0] != ' ' && txt9.Text[1] == ' ' && txt9.Text[2] == ' ' && txt9.Text[3] != ' ') || (txt9.Text[0] != ' ' && txt9.Text[1] == ' ' && txt9.Text[2] != ' ' && txt9.Text[3] != ' ') || (txt9.Text[0] != ' ' && txt9.Text[1] != ' ' && txt9.Text[2] == ' ' && txt9.Text[3] != ' '))
                    txt9.Text = "";
            }
            else if (txt9.Text.Length > 4)
            {
                txt9.Text = "";
            }
            else if (txt9.Text.Length == 3)
            {
                if (!IsValid(txt9.Text) || (txt9.Text[0] != ' ' && txt9.Text[1] == ' ' && txt9.Text[2] != ' '))
                    txt9.Text = "";
            }
            else
            {
                if (!IsValid(txt9.Text))
                    txt9.Text = "";
            }
            if (!IsValid(txt10.Text))
                txt10.Text = "";
            if (!IsValid(txt11.Text))
                txt11.Text = "";
            if (!IsValid(txt12.Text))
                txt12.Text = "";
            if (!IsValid(txt13.Text))
                txt13.Text = "";
            if (txt14.Text.Length == 4)
            {
                if (!IsValid(txt14.Text) || (txt14.Text[0] != ' ' && txt14.Text[1] == ' ' && txt14.Text[2] == ' ' && txt14.Text[3] != ' ') || (txt14.Text[0] != ' ' && txt14.Text[1] == ' ' && txt14.Text[2] != ' ' && txt14.Text[3] != ' ') || (txt14.Text[0] != ' ' && txt14.Text[1] != ' ' && txt14.Text[2] == ' ' && txt14.Text[3] != ' '))
                    txt14.Text = "";
            }
            else if (txt14.Text.Length > 4)
            {
                txt14.Text = "";
            }
            else if (txt14.Text.Length == 3)
            {
                if (!IsValid(txt14.Text) || (txt14.Text[0] != ' ' && txt14.Text[1] == ' ' && txt14.Text[2] != ' '))
                    txt14.Text = "";
            }
            else
            {
                if (!IsValid(txt14.Text))
                    txt14.Text = "";
            }
            if (!IsValid(txt15.Text))
                txt15.Text = "";
            if (!IsValid(txt16.Text))
                txt16.Text = "";
            if (!IsValid(txt17.Text))
                txt17.Text = "";
            if (!IsValid(txt18.Text))
                txt18.Text = "";
            if (txt19.Text.Length == 4)
            {
                if (!IsValid(txt19.Text) || (txt19.Text[0] != ' ' && txt19.Text[1] == ' ' && txt19.Text[2] == ' ' && txt19.Text[3] != ' ') || (txt19.Text[0] != ' ' && txt19.Text[1] == ' ' && txt19.Text[2] != ' ' && txt19.Text[3] != ' ') || (txt19.Text[0] != ' ' && txt19.Text[1] != ' ' && txt19.Text[2] == ' ' && txt19.Text[3] != ' '))
                    txt19.Text = "";
            }
            else if (txt19.Text.Length > 4)
            {
                txt19.Text = "";
            }
            else if (txt19.Text.Length == 3)
            {
                if (!IsValid(txt19.Text) || (txt19.Text[0] != ' ' && txt19.Text[1] == ' ' && txt19.Text[2] != ' '))
                    txt19.Text = "";
            }
            else
            {
                if (!IsValid(txt19.Text))
                    txt19.Text = "";
            }
            if (!IsValid(txt20.Text))
                txt20.Text = "";

            rsCheck = String.Join("|", lsttxt.Select(x => x.Text.Trim()).ToArray());
            if (typeCheck == "Check")
            {
                //Lỗi sai E1
                c = null;
                c = new int[rsCheck.Length + 1, rsEntr1.Length + 1];
                int vlLCS = LCS(rsCheck, rsEntr1);
                if (rsEntr1 == rsCheck)
                    imgContent.Loisai1 = 0;
                else if (rsEntr1.Length > rsCheck.Length)
                    imgContent.Loisai1 = rsEntr1.Length - vlLCS;
                else
                    imgContent.Loisai1 = rsCheck.Length - vlLCS;

                //Lỗi sai E2 
                c = null;
                c = new int[rsCheck.Length + 1, rsEntr2.Length + 1];
                vlLCS = LCS(rsCheck, rsEntr2);
                if (rsEntr2 == rsCheck)
                    imgContent.Loisai2 = 0;
                else if (rsEntr2.Length > rsCheck.Length)
                    imgContent.Loisai2 = rsEntr2.Length - vlLCS;
                else
                    imgContent.Loisai2 = rsCheck.Length - vlLCS;
            }
            // set image content           
            imgContent.Result = rsCheck;
            imgContent.Tongkytu = imgContent.Result.Length - 19;
            imgContent.CheckerId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            // add image content
            try
            {
                txt1.Text = ""; txt2.Text = ""; txt3.Text = ""; txt4.Text = ""; txt5.Text = ""; txt6.Text = ""; txt7.Text = ""; txt8.Text = ""; txt9.Text = ""; txt10.Text = "";
                txt11.Text = ""; txt12.Text = ""; txt13.Text = ""; txt14.Text = ""; txt15.Text = ""; txt16.Text = ""; txt17.Text = ""; txt18.Text = ""; txt19.Text = ""; txt20.Text = "";
                if (typeCheck == "Check")
                    dACheck.UpdateResutl2(imgContent, userId, ms, tableform);
                else
                    dACheck.UpdateQC(imgContent, userId, ms, tableform);
                imgContent = new BOImageContent_Check();
            }
            catch (Exception exception)
            {
                // rool back hitpoint             
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            saveimageid = img.Id;
            // load image mới lên form
            // lấy id image entry
            id = 0;
            imageSource = null;
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                if (dAEntry.Count_UpHitpointLC(tableform) == 0)
                { dAEntry.UpdateHitpoint_Batch(batchId, 2); }
                finish = true;
                this.Close();
                return;
            }

            // lay image đã entry
            try
            {

                //nam cat anh
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageName, batchnameLocal)));
                Bitmap bm = CropBitmap(imageSource, imageSource.Width / 2, 0, imageSource.Width / 2, imageSource.Height / 2);
                imageViewerTR1.Image = bm;
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageName, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            // lấy image content đã entry
            try
            {
                imgContent = dACheck.GetImageContent(img.Id, tableform);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

           
            for (int i = 0; i < imgContent.Content1.Count; i++)
            {
                lstlbl[i].BackColor = SystemColors.Window;
                lsttxt[i].ForeColor = Color.Red;
                lsttxt[i].Text = imgContent.Content1[i];
                numbertrtxt = i;
                string s1, s2;
                s1 = imgContent.Content1[i];
                s2 = imgContent.Content2[i];
                lstlbl[i].Text = lstlbl[i].Text.Substring(0, lstlbl[i].Text.Length - 1) + "1";
                if (s1 != s2)
                {
                    lstlbl[i].BackColor = Color.Red;
                }
                c = null;
                c = new int[s1.Length + 1, s2.Length + 1];
                LCS(s1, s2);
                BackTrack(s1, s2, s1.Length, s2.Length);
            }


            if (img.NG1 != 0 && typeCheck == "QC")
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0 && typeCheck == "QC")
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;

            // hiển thị user name của user đã entry
            btnSubmit.Enabled = true;
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblbatchname.Text = batchName;
            dtimeBefore = DateTime.Now;
            if (typeCheck == "QC")
                lblImage.Text = dACheck.ImageExistQC(tableform);
            else
                lblImage.Text = dACheck.ImageExistCheck(tableform);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (saveimageid == 0)
            {
                MessageBox.Show("Can not back", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            // load image mới lên form
            // lấy id image entry
            id = 0;
            try
            {
                if (typeCheck == "Check")
                {
                    dACheck.SetHitPointImageCheck(img.Id, tableform);
                }
                else
                {
                    dACheck.SetHitPointImageCheckQC(img.Id, tableform);
                }
                dACheck.Set_result_null(saveimageid, tableform);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            //lấy Imageid trước           
            try
            {
                id = saveimageid;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            saveimageid = 0;

            // lay image đã entry
            try
            {
                img = dACheck.GetImage(id, batchnameLocal);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            try
            {
                imageViewerTR1.Dispose();  

                //nam cat anh
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageName, batchnameLocal)));
                Bitmap bm = CropBitmap(imageSource, imageSource.Width / 2, 0, imageSource.Width / 2, imageSource.Height / 2);
                imageViewerTR1.Image = bm;
                currentzoom = imageViewerTR1.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageName, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // lấy image content đã entry
            try
            {
                imgContent = dACheck.GetImageContent(id, tableform);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            
            for (int i = 0; i < imgContent.Content1.Count; i++)
            {
                lstlbl[i].BackColor = SystemColors.Window;
                lsttxt[i].ForeColor = Color.Red;
                lsttxt[i].Text = imgContent.Content1[i];
                numbertrtxt = i;
                string s1, s2;
                s1 = imgContent.Content1[i];
                s2 = imgContent.Content2[i];
                lstlbl[i].Text = lstlbl[i].Text.Substring(0, lstlbl[i].Text.Length - 1) + "1";
                if (s1 != s2)
                {
                    lstlbl[i].BackColor = Color.Red;
                }
                c = null;
                c = new int[s1.Length + 1, s2.Length + 1];
                LCS(s1, s2);
                BackTrack(s1, s2, s1.Length, s2.Length);
            }

            if (img.NG1 != 0 && typeCheck == "QC")
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0 && typeCheck == "QC")
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;

            // hiển thị user name của user đã entry            
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblbatchname.Text = batchName;
            dtimeBefore = DateTime.Now;
            if (typeCheck == "QC")
                lblImage.Text = dACheck.ImageExistQC(tableform);
            else
                lblImage.Text = dACheck.ImageExistCheck(tableform);
        }
        private void rtxtsbd_MouseDown(object sender, MouseEventArgs e)
        {
            RichTextBox rtxt = (RichTextBox)sender;
            if (e.Button == MouseButtons.Right)
            {
                if (rtxt.Text == imgContent.Content1_OCR[rtxt.TabIndex])
                {
                    lstlbl[rtxt.TabIndex - 1].Text = lstlbl[rtxt.TabIndex - 1].Text.Substring(0, lstlbl[rtxt.TabIndex - 1].Text.Length - 1) + "2";
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content3[rtxt.TabIndex];
                    numbertrtxt = rtxt.TabIndex;
                    string s1, s2;
                    s1 = imgContent.Content3[rtxt.TabIndex];
                    s2 = imgContent.Content1_OCR[rtxt.TabIndex];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                else if (rtxt.Text == imgContent.Content3[rtxt.TabIndex])
                {
                    lstlbl[rtxt.TabIndex - 1].Text = lstlbl[rtxt.TabIndex - 1].Text.Substring(0, lstlbl[rtxt.TabIndex - 1].Text.Length - 1) + "1";
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1_OCR[rtxt.TabIndex];
                    numbertrtxt = rtxt.TabIndex;
                    string s1, s2;
                    s1 = imgContent.Content1_OCR[rtxt.TabIndex];
                    s2 = imgContent.Content3[rtxt.TabIndex];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                else
                {
                    lstlbl[rtxt.TabIndex - 1].Text = lstlbl[rtxt.TabIndex - 1].Text.Substring(0, lstlbl[rtxt.TabIndex - 1].Text.Length - 1) + "1";
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1_OCR[rtxt.TabIndex];
                    numbertrtxt = rtxt.TabIndex;
                    string s1, s2;
                    s1 = imgContent.Content1_OCR[rtxt.TabIndex];
                    s2 = imgContent.Content3[rtxt.TabIndex];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            imageViewerTR1.CurrentZoom = currentzoom;
        }
        //nam  hàm cắt ảnh        
        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }

        private void timegetanh_Tick(object sender, EventArgs e)
        {
            if (ListImage.Count < 2)
            {
                int id2 = 0;
                if (typeCheck == "Check")
                {
                    id2 = dACheck.GetIdCheck(tableform);
                }
                else
                {
                    id2 = dACheck.GetIdCheckQC(tableform);
                }
                if (id2 > 0)
                {
                    BOImage_Check img2 = new BOImage_Check();
                    img2 = dACheck.GetImage(id2, tableform);
                    img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dACheck.getImageOnServer(img2.PageName, batchnameLocal)));
                    ListImage.Add(img2);
                }

            }
        }
    }
}

