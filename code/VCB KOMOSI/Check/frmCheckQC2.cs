﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
namespace VCB_KOMOSI
{
    public partial class frmCheckQC2 : Form
    {
        public int formId;
        public int batchId;
        public int userId;
        public string userName;
        public string batchName;
        public string template;
        public string level;
        Bitmap imageSource;
        int demsohang;
        string[] arrsh = new string[0];
        string[] arrsl = new string[0];
        string[] arrgm = new string[0];
        string[] arrgb = new string[0];
        string x2, x3, x4, x5;
        private double scale = 1;
        private int numbertrtxt = 0;
        private DAEntry_Check dACheck = new DAEntry_Check();
        private BOImage_Check img = new BOImage_Check();
        private BOImageContent_Check imgContent = new BOImageContent_Check();
        private bool finish = false;
        double zom = 20;
        double tong1 = 0;
        int id = 0;
        double tong2 = 0;
        int demid = 0;
        private System.Windows.Forms.TextBox txtAutoComplete = new System.Windows.Forms.TextBox();
        List<BOImage_Check> ListImage = new List<BOImage_Check>();
        double tyleImageNew = 1;
        Bitmap bm_out;
        int saveimageid = 0;
        public string[] INFperformance = new string[2];
        List<RichTextBox> lsttxt = new List<RichTextBox>();
        List<Label> lstlbl = new List<Label>();
        float currentzoom;
        private DAEntry_Entry dAEntry = new DAEntry_Entry();
        string type = "";
        public frmCheckQC2()
        {
            InitializeComponent();
            lsttxt = new List<RichTextBox>() { txt1, txt2 };
            lstlbl = new List<Label>() { lbl1, lbl2 };
        }
        private void frmCheck_Load(object sender, EventArgs e)
        {
            try
            {
                if (batchName[7] == 'A')
                {
                    txt1.Visible = false;
                    lbl1.Visible = false;
                    imageViewerTR1.Visible = false;
                    type = "A";
                }
                //nam 25/7 gan bien khi folder C
                if (batchName[7] == 'C')
                {
                    type = "C";
                }
            }
            catch { }
            // set bien toan cuc         
            lsttxt.ForEach(a =>
            {
                a.Enter += new System.EventHandler(this.textBox1_Enter);
                a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
                a.Leave += new System.EventHandler(this.textBox1_Leave);
                a.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
                a.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
                a.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rtxt_MouseDown);
            });
            // lấy id image đã entry để check
            //int id = 0;
            // check đã entry

            try
            {
                if (batchName[7] == 'A')
                {
                    txt1.Enabled = false;
                    lbl1.Enabled = false;
                }
            }
            catch { }

            try
            {
                id = dACheck.GetIdImageEntriedFromBatchIdQC(batchId);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
            dACheck.SetHitpointBatch(batchId);
            if (id == 0)
            {               
                finish = true;
                this.Close();
                return;
            }

            // lay image đã entry
            try
            {
                img = dACheck.GetImage(id);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            ////Get Image
            //imageSource = new Bitmap(Io_Check.byteArrayToImage(dACheck.getImageOnServer(img.PageName, batchName.Substring(7, batchName.Length - 7))));
            //imageViewerTR1.Image = imageSource;
            //currentzoom = imageViewerTR1.CurrentZoom;
            //// lấy image content đã entry

            //nam 25/7 cắt ảnh
            imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageName, batchName.Substring(7, batchName.Length - 7))));
            Bitmap bm1 = (Bitmap)imageSource.Clone();
            Bitmap bm2 = (Bitmap)imageSource.Clone();

            if (type == "A")
            {
                imageViewerTR2.Image = CropBitmap(bm1, bm1.Width * 57 / 100, 0, bm1.Width * 43 / 100, bm1.Height);
            }
            else
            {
                //tr2:chữ kana tr1:giới tính
                imageViewerTR1.Image = CropBitmap(bm1, (bm1.Width * 89 / 100), bm1.Height * 16 / 100, (bm1.Width * 9 / 100), bm1.Height * 84 / 100);
                imageViewerTR2.Image = CropBitmap(bm2, bm2.Width * 25 / 100, 0, (bm2.Width * 50 / 100), bm2.Height);
            }
            currentzoom = imageViewerTR1.CurrentZoom;
            currentzoom = imageViewerTR2.CurrentZoom;
            try
            {
                imgContent = dACheck.GetImageContent(id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            for (int i = 0; i < 2; i++)
            {
                lsttxt[i].ForeColor = Color.Red;
                lsttxt[i].Text = imgContent.Content1[i];
                numbertrtxt = i;
                string s1, s2;
                s1 = imgContent.Content1[i];
                s2 = imgContent.Content2[i];
                if (s1 != s2)
                {
                    lstlbl[i].BackColor = Color.Red;
                }
                c = null;
                c = new int[s1.Length + 1, s2.Length + 1];
                LCS(s1, s2);
                BackTrack(s1, s2, s1.Length, s2.Length);
            }

            if (img.NG1 != 0)
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0)
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;
            if (img.NG3 != 0)
                lblEntry3.BackColor = Color.Red;
            else
                lblEntry3.BackColor = SystemColors.Window;



            // hiển thị user name của user đã entry            
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblEntry3.Text = imgContent.Name3;
            lblGroup3.Text = imgContent.Group3;
            lblbatchname.Text = batchName;
            txt1.Focus();
            bgwGetImage.RunWorkerAsync();
        }
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCheck_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!finish)
            {
                // unlock image
                dACheck.SetStatusQC(img.Id, "1");
                dACheck.SetHitPointImage(img.Id, 2);
                // unlock image       
                for (int i = 0; i < ListImage.Count; i++)
                {
                    dACheck.SetStatusQC(ListImage[i].Id, "1");
                    dACheck.SetHitPointImage(ListImage[i].Id, 2);
                }
                dACheck.UpdatePerformance(INFperformance[1], int.Parse(INFperformance[0]));
            }
            else
            {
                frmLogIn.fn = true;
            }
        }
        private void btnRL_Click(object sender, EventArgs e)
        {
            imageViewerTR1.RotateImage("90");
        }

        private void btnRR_Click(object sender, EventArgs e)
        {
            imageViewerTR1.RotateImage("270");
        }


        #region Compare string
        static int max(int a, int b)
        {
            return (a > b) ? a : b;
        }
        static int[,] c;
        //Prints one LCS
        private string BackTrack(string s1, string s2, int i, int j)
        {
            if (i == 0 || j == 0)
                return "";
            if (s1[i - 1] == s2[j - 1])
            {
                lsttxt[numbertrtxt].SelectionStart = i - 1;
                lsttxt[numbertrtxt].SelectionLength = 1;
                lsttxt[numbertrtxt].SelectionColor = Color.Black;
                return BackTrack(s1, s2, i - 1, j - 1) + s1[i - 1];
            }
            else if (c[i - 1, j] > c[i, j - 1])
                return BackTrack(s1, s2, i - 1, j);

            else
                return BackTrack(s1, s2, i, j - 1);

        }
        //Nghịch       
        private string BackTrack2(string s1, string s2, int i, int j)
        {
            if (i == 0 || j == 0)
                return "";
            if (s1[i - 1] == s2[j - 1])
            {
                return BackTrack2(s1, s2, i - 1, j - 1) + s1[i - 1];
            }
            else if (c[i - 1, j] > c[i, j - 1])
                return BackTrack2(s1, s2, i - 1, j);

            else
                return BackTrack2(s1, s2, i, j - 1);

        }
        static int LCS(string s1, string s2)
        {
            for (int i = 1; i <= s1.Length; i++)
                c[i, 0] = 0;
            for (int i = 1; i <= s2.Length; i++)
                c[0, i] = 0;

            for (int i = 1; i <= s1.Length; i++)
                for (int j = 1; j <= s2.Length; j++)
                {
                    if (s1[i - 1] == s2[j - 1])
                        c[i, j] = c[i - 1, j - 1] + 1;
                    else
                    {
                        c[i, j] = max(c[i - 1, j], c[i, j - 1]);

                    }

                }

            return c[s1.Length, s2.Length];

        }
        #endregion

        private void frmCheck_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
            if (e.Control)
            {
                if (e.KeyCode == Keys.Up)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Down)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    imageViewerTR1.RotateImage("270");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Left)
                {
                    imageViewerTR1.RotateImage("90");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.F)
                {
                    imageViewerTR1.CurrentZoom = currentzoom;
                    e.Handled = true;
                }
            }
        }

        private void btnSumit2_Click(object sender, EventArgs e)
        {
            btnSubmit.Enabled = false;
            imageViewerTR1.Dispose();
            // update image content vao DB
            string rsEntr1 = String.Join("|", imgContent.Content1_OCR);
            string rsEntr2 = String.Join("|", imgContent.Content3);
            string rsCheck = "";
            rsCheck = String.Join("|", lsttxt.Select(x => x.Text.Trim()).ToArray());
            // set image content
            imgContent.Result = rsCheck;
            imgContent.Tongkytu = imgContent.Result.Length - 1;
            imgContent.CheckerId = userId;            
            // add image content
            try
            {
                dACheck.UpdateResutl2(imgContent, userId);
                imgContent = new BOImageContent_Check();
            }
            catch (Exception exception)
            {
                // rool back hitpoint
                dACheck.SetHitPointImage(img.Id, 2);
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            dACheck.SetHitpointBatch(batchId);
            saveimageid = img.Id;
            // load image mới lên form
            // lấy id image entry
            id = 0;
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                if (!bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                {
                    dACheck.UpdatePerformance(INFperformance[1], int.Parse(INFperformance[0]));
                    finish = true;
                    this.Close();
                    return;
                }
                else
                {
                    while (ListImage.Count == 0)
                    { }
                    img = ListImage[0];
                    ListImage.RemoveAt(0);
                }
            }

            // lay image đã entry
            try
            {
                ////Get Image
                //imageSource = (Bitmap)img.Imagesource.Clone();
                //imageViewerTR1.Image = imageSource;

                //nam 25/7 cắt ảnh
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageName, batchName.Substring(7, batchName.Length - 7))));
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();

                if (type == "A")
                {
                    imageViewerTR2.Image = CropBitmap(bm1, bm1.Width * 57 / 100, 0, bm1.Width * 43 / 100, bm1.Height);
                }
                else
                {
                    //tr2:chữ kana tr1:giới tính
                    imageViewerTR1.Image = CropBitmap(bm1, (bm1.Width * 89 / 100), bm1.Height * 16 / 100, (bm1.Width * 9 / 100), bm1.Height * 84 / 100);
                    imageViewerTR2.Image = CropBitmap(bm2, bm2.Width * 25 / 100, 0, (bm2.Width * 50 / 100), bm2.Height);
                }
                currentzoom = imageViewerTR1.CurrentZoom;
                currentzoom = imageViewerTR2.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageName, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // lấy image content đã entry
            try
            {
                imgContent = dACheck.GetImageContent(img.Id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            for (int i = 0; i < 2; i++)
            {
                lsttxt[i].Text = "";
                lstlbl[i].BackColor = SystemColors.Window;
                lsttxt[i].ForeColor = Color.Red;
                lsttxt[i].Text = imgContent.Content1[i];
                numbertrtxt = i;
                string s1, s2;
                s1 = imgContent.Content1[i];
                s2 = imgContent.Content2[i];
                if (s1 != s2)
                {
                    lstlbl[i].BackColor = Color.Red;
                }
                c = null;
                c = new int[s1.Length + 1, s2.Length + 1];
                LCS(s1, s2);
                BackTrack(s1, s2, s1.Length, s2.Length);
            }

            if (img.NG1 != 0)
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0)
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;
            if (img.NG3 != 0)
                lblEntry3.BackColor = Color.Red;
            else
                lblEntry3.BackColor = SystemColors.Window;

            // hiển thị user name của user đã entry
            btnSubmit.Enabled = true;
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblEntry3.Text = imgContent.Name3;
            lblGroup3.Text = imgContent.Group3;
            lblbatchname.Text = batchName;
            if (!bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                bgwGetImage1.RunWorkerAsync();
            else if (bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                bgwGetImage1.RunWorkerAsync();
            else if (!bgwGetImage.IsBusy && bgwGetImage1.IsBusy)
                bgwGetImage.RunWorkerAsync();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            imageViewerTR1.CurrentZoom = currentzoom;
        }

        private void btnout_Click(object sender, EventArgs e)
        {
            imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
        }

        private void btnin_Click(object sender, EventArgs e)
        {
            imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            zom = 20;
            bm_out = null;
            if (saveimageid == 0)
            {
                MessageBox.Show("Can not back", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            // load image mới lên form
            // lấy id image entry
            id = 0;
            try
            {
                dACheck.SetHitPointImage(img.Id, 2);
                dACheck.Set_result_null(saveimageid);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            //lấy Imageid trước           
            try
            {
                id = saveimageid;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            saveimageid = 0;


            // lay image đã entry
            try
            {
                img = dACheck.GetImage(id);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            imageViewerTR1.Dispose();
            imageViewerTR2.Dispose();
            ////Get Image
            //imageSource = new Bitmap(Io_Entry.byteArrayToImage(dACheck.getImageOnServer(img.PageName, batchName.Substring(7, batchName.Length - 7))));
            //imageViewerTR1.Image = imageSource;

            //nam 25/7 cắt ảnh
            imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageName, batchName.Substring(7, batchName.Length - 7))));
            Bitmap bm1 = (Bitmap)imageSource.Clone();
            Bitmap bm2 = (Bitmap)imageSource.Clone();

            if (type == "A")
            {
                imageViewerTR2.Image = CropBitmap(bm1, bm1.Width * 57 / 100, 0, bm1.Width * 43 / 100, bm1.Height);
            }
            else
            {
                //tr2:chữ kana tr1:giới tính
                imageViewerTR1.Image = CropBitmap(bm1, (bm1.Width * 89 / 100), bm1.Height * 16 / 100, (bm1.Width * 9 / 100), bm1.Height * 84 / 100);
                imageViewerTR2.Image = CropBitmap(bm2, bm2.Width * 25 / 100, 0, (bm2.Width * 50 / 100), bm2.Height);
            }
            currentzoom = imageViewerTR1.CurrentZoom;
            currentzoom = imageViewerTR2.CurrentZoom;
            // lấy image content đã entry
            try
            {
                imgContent = dACheck.GetImageContent(id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            for (int i = 0; i < 2; i++)
            {
                lsttxt[i].Text = "";
                lstlbl[i].BackColor = SystemColors.Window;
                lsttxt[i].ForeColor = Color.Red;
                lsttxt[i].Text = imgContent.Content1[i];
                numbertrtxt = i;
                string s1, s2;
                s1 = imgContent.Content1[i];
                s2 = imgContent.Content2[i];
                if (s1 != s2)
                {
                    lstlbl[i].BackColor = Color.Red;
                }
                c = null;
                c = new int[s1.Length + 1, s2.Length + 1];
                LCS(s1, s2);
                BackTrack(s1, s2, s1.Length, s2.Length);
            }

            if (img.NG1 != 0)
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0)
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;
            if (img.NG3 != 0)
                lblEntry3.BackColor = Color.Red;
            else
                lblEntry3.BackColor = SystemColors.Window;

            // hiển thị user name của user đã entry            
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblEntry3.Text = imgContent.Name3;
            lblGroup3.Text = imgContent.Group3;
            lblbatchname.Text = batchName;
            if (!bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                bgwGetImage1.RunWorkerAsync();
            else if (bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                bgwGetImage1.RunWorkerAsync();
            else if (!bgwGetImage.IsBusy && bgwGetImage1.IsBusy)
                bgwGetImage.RunWorkerAsync();
        }
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            RichTextBox tb = (RichTextBox)sender;
            if (e.KeyCode == Keys.A)
            {
                if (tb.TabIndex >= 0 && tb.TabIndex <= 4)
                {
                    if (!lsttxt[0].Text.ToUpper().Contains("A"))
                    {
                        lsttxt[0].Text = "a" + lsttxt[0].Text;
                        tb.SelectionStart = tb.Text.Length + 1;
                    }
                    else
                    {
                        string chuoithay = Regex.Replace(lsttxt[0].Text, @"a", "");
                        lsttxt[0].Text = chuoithay;
                        tb.SelectionStart = tb.Text.Length + 1;
                    }
                }
                else
                {
                    if (!lsttxt[tb.TabIndex].Text.ToUpper().Contains("A"))
                    {
                        lsttxt[tb.TabIndex].Text = "a" + lsttxt[tb.TabIndex].Text;
                        tb.SelectionStart = tb.Text.Length + 1;
                    }
                    else
                    {
                        string chuoithay = Regex.Replace(tb.Text, @"a", "");
                        tb.Text = chuoithay;
                        tb.SelectionStart = tb.Text.Length + 1;
                    }
                }
            }
            if (e.KeyCode == Keys.F)
            {
                if (!lsttxt[0].Text.ToUpper().Contains("F"))
                {
                    lsttxt[0].Text = "f" + lsttxt[0].Text;
                    if (tb.TabIndex > 0)
                    {
                        lsttxt[tb.TabIndex].Focus();
                    }
                    else
                    {
                        tb.SelectionStart = tb.Text.Length + 1;
                    }
                }
                else
                {
                    string chuoithay2 = Regex.Replace(lsttxt[0].Text, @"f", "");
                    lsttxt[0].Text = chuoithay2;
                    lsttxt[tb.TabIndex + 1].Focus();
                    lsttxt[tb.TabIndex + 1].SelectionStart = lsttxt[tb.TabIndex + 1].Text.Length + 1;
                }
            }
            if (e.KeyCode == Keys.Right)
            {
                if (tb.TabIndex >= 0 && tb.TabIndex < 4)
                {
                    lsttxt[tb.TabIndex].Focus();
                    e.Handled = true;
                }
                else if (tb.TabIndex == 4)
                {
                    lsttxt[0].Focus();
                    e.Handled = true;
                }
            }
            else if (e.KeyCode == Keys.Left)
            {
                if (tb.TabIndex > 0 && tb.TabIndex <= 4)
                {
                    lsttxt[tb.TabIndex - 1].Focus();
                    e.Handled = true;
                }
                else if (tb.TabIndex == 0)
                {
                    lsttxt[4].Focus();
                    e.Handled = true;
                }
            }
            else if (e.KeyCode == Keys.Down)
            {
                if (tb.TabIndex >= 0 && tb.TabIndex <= 4)
                {
                    lsttxt[5].Focus();
                    e.Handled = true;
                }
                else if (tb.TabIndex >= 5 && tb.TabIndex < 10)
                {
                    lsttxt[tb.TabIndex + 1].Focus();
                    e.Handled = true;
                }
                else if (tb.TabIndex == 10)
                {
                    lsttxt[0].Focus();
                    e.Handled = true;
                }
            }
            else if (e.KeyCode == Keys.Up)
            {
                if (tb.TabIndex == 5)
                {
                    lsttxt[0].Focus();
                    e.Handled = true;
                }
                else if (tb.TabIndex > 5 && tb.TabIndex <= 10)
                {
                    lsttxt[tb.TabIndex - 1].Focus();
                    e.Handled = true;
                }
                else if (tb.TabIndex == 0)
                {
                    lsttxt[10].Focus();
                    e.Handled = true;
                }
            }
            //else if (e.KeyCode == Keys.Add)
            //{
            //    if (tb.TabIndex >= 0 && tb.TabIndex <= 4)
            //    {
            //        tb.Text = "";
            //        e.Handled = true;
            //        lsttxt[tb.TabIndex].Focus();
            //    }
            //}
            //else if (e.KeyCode == Keys.Subtract)
            //{
            //    if (tb.TabIndex == 2)
            //    {
            //        tb.Text = x2;
            //        lsttxt[tb.TabIndex].Focus();
            //    }
            //    if (tb.TabIndex == 3)
            //    {
            //        tb.Text = x3;
            //        lsttxt[tb.TabIndex].Focus();
            //    }
            //    if (tb.TabIndex == 4)
            //    {
            //        tb.Text = x4;
            //        lsttxt[tb.TabIndex].Focus();
            //    }
            //    if (tb.TabIndex == 5)
            //    {
            //        tb.Text = x5;
            //        lsttxt[5].Focus();
            //    }
            //}
            else if (e.KeyCode == Keys.Oemtilde)
            {
                if (tb.TabIndex != 0)
                {
                    lsttxt[tb.TabIndex - 1].Focus();
                }
                else
                {
                    lsttxt[10].Focus();
                }
            }
            else if (e.KeyCode == Keys.Q)
            {
                if (tb.Text.Length > 0)
                {
                    tb.Text = tb.Text.Remove(tb.Text.Length - 1);
                    tb.SelectionStart = tb.Text.Length + 1;
                }
                else
                {
                    if (tb.TabIndex != 0)
                    {
                        if (lsttxt[tb.TabIndex - 1].Text.Length > 0)
                        {
                            lsttxt[tb.TabIndex - 1].Text = lsttxt[tb.TabIndex - 1].Text.Remove(lsttxt[tb.TabIndex - 2].Text.Length - 1);
                            lsttxt[tb.TabIndex - 1].Focus();
                            lsttxt[tb.TabIndex - 1].SelectionStart = lsttxt[tb.TabIndex].Text.Length + 1;
                        }
                    }
                }
            }
            else if (e.KeyCode == Keys.E)
            {
                tb.Text = "";
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox1_Enter(object sender, EventArgs e)
        {

        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        private void textBox1_Leave(object sender, EventArgs e)
        {

        }


        private void rtxt_MouseDown(object sender, MouseEventArgs e)
        {
            RichTextBox rtxt = (RichTextBox)sender;
            if (e.Button == MouseButtons.Right)
            {
                if (rtxt.Text == imgContent.Content1[rtxt.TabIndex - 1])
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content2[rtxt.TabIndex - 1];
                    numbertrtxt = rtxt.TabIndex - 1;
                    string s1, s2;
                    s1 = imgContent.Content2[rtxt.TabIndex - 1];
                    s2 = imgContent.Content1[rtxt.TabIndex - 1];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                else if (rtxt.Text == imgContent.Content2[rtxt.TabIndex - 1])
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1[rtxt.TabIndex - 1];
                    numbertrtxt = rtxt.TabIndex - 1;
                    string s1, s2;
                    s1 = imgContent.Content1[rtxt.TabIndex - 1];
                    s2 = imgContent.Content2[rtxt.TabIndex - 1];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                else
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1[rtxt.TabIndex - 1];
                    numbertrtxt = rtxt.TabIndex - 1;
                    string s1, s2;
                    s1 = imgContent.Content1[rtxt.TabIndex - 1];
                    s2 = imgContent.Content2[rtxt.TabIndex - 1];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
            }
        }

        private void bgwGetImage_DoWork(object sender, DoWorkEventArgs e)
        {
            while (ListImage.Count < 2)
            {
                int id2 = dACheck.GetIdImageEntriedFromBatchIdQC(batchId);
                if (id2 > 0)
                {
                    BOImage_Check img2 = dACheck.GetImage(id2);
                    try
                    {
                        img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dACheck.getImageOnServer(img2.PageName, batchName.Substring(7, batchName.Length - 7))));
                    }
                    catch { img2.Imagesource = null; }
                    ListImage.Add(img2);
                }
                else
                    break;
            }
        }

        private void bgwGetImage1_DoWork(object sender, DoWorkEventArgs e)
        {
            while (ListImage.Count < 2)
            {
                int id2 = dACheck.GetIdImageEntriedFromBatchIdQC(batchId);
                if (id2 > 0)
                {
                    BOImage_Check img2 = dACheck.GetImage(id2);
                    try
                    {
                        img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dACheck.getImageOnServer(img2.PageName, batchName.Substring(7, batchName.Length - 7))));
                    }
                    catch { img2.Imagesource = null; }
                    ListImage.Add(img2);
                }
                else
                    break;
            }
        }

        private void bgwGetImage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblImage.Text = dACheck.ImageExistCheckQC(batchId);
        }

        private void txt1_Enter(object sender, EventArgs e)
        {
            txt1.BackColor = Color.PowderBlue;
            txt2.BackColor = SystemColors.Window;
            foreach (InputLanguage lang in InputLanguage.InstalledInputLanguages)
                if (lang.LayoutName.Contains("US"))
                {
                    InputLanguage.CurrentInputLanguage = lang; break;
                }
        }

        private void txt2_Enter(object sender, EventArgs e)
        {
            txt2.BackColor = Color.PowderBlue;
            txt1.BackColor = SystemColors.Window;
            foreach (InputLanguage lang in InputLanguage.InstalledInputLanguages)
                if (lang.LayoutName.ToUpper() == "JAPANESE")
                {
                    InputLanguage.CurrentInputLanguage = lang; break;
                }
            txt2.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
        }
        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }
    }
}

