﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
namespace VCB_KOMOSI
{
    public partial class frmCheckQC1 : Form
    {
        public int formId;
        public int batchId;
        public int userId;
        public string userName;
        public string batchName;
        public string template;
        public string level;
        Bitmap imageSource;
        int demsohang;
        string[] arrsh = new string[0];
        string[] arrsl = new string[0];
        string[] arrgm = new string[0];
        string[] arrgb = new string[0];
        string x2, x3, x4, x5;
        private double scale = 1;
        private int numbertrtxt = 0;
        private DAEntry_Check dACheck = new DAEntry_Check();
        private BOImage_Check img = new BOImage_Check();
        private BOImageContent_Check imgContent = new BOImageContent_Check();
        private bool finish = false;
        double zom = 20;
        double tong1 = 0;
        int id = 0;
        double tong2 = 0;
        int demid = 0;
        private System.Windows.Forms.TextBox txtAutoComplete = new System.Windows.Forms.TextBox();
        List<BOImage_Check> ListImage = new List<BOImage_Check>();
        double tyleImageNew = 1;
        Bitmap bm_out;
        int saveimageid = 0;
        public string[] INFperformance = new string[2];
        List<RichTextBox> lsttxt;
        List<Label> lstlbl = new List<Label>();
        float currentzoom;
        private DAEntry_Entry dAEntry = new DAEntry_Entry();
        string type = "";
        public frmCheckQC1()
        {
            InitializeComponent();
            lsttxt = new List<RichTextBox>() { txt1, txt2 };
            lstlbl = new List<Label>() { lbl1, lbl2 };
        }
        private void frmCheck_Load(object sender, EventArgs e)
        {
            if (batchName[7] == 'A')
            {
                //nam 20/7 ẩn mục ngày sinh
                type = "A";
                txt2.Visible = false;
                lbl2.Visible = false;
                imageViewerTR2.Visible = false;
            }
            //nam 20/7 gan bien khi folder C
            if (batchName[7] == 'C')
            {
                type = "C";
            }

            // set bien toan cuc         
            lsttxt.ForEach(a =>
            {
                a.Enter += new System.EventHandler(this.textBox1_Enter);
                a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
                a.Leave += new System.EventHandler(this.textBox1_Leave);
                a.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
                a.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
                a.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rtxt_MouseDown);
            });
            // lấy id image đã entry để check
            //int id = 0;
            // check đã entry
            try
            {
                id = dACheck.GetIdImageEntriedFromBatchIdQC(batchId);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
            dACheck.SetHitpointBatch(batchId);
            if (id == 0)
            {

                finish = true;
                this.Close();
                return;
            }

            // lay image đã entry
            try
            {
                img = dACheck.GetImage(id);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            //Get Image
            //imageSource = new Bitmap(Io_Check.byteArrayToImage(dACheck.getImageOnServer(img.PageName, batchName.Substring(7, batchName.Length - 7))));
            //imageViewerTR1.Image = imageSource;
            //currentzoom = imageViewerTR1.CurrentZoom;

            //nam cat anh
            imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageName, batchName.Substring(7, batchName.Length - 7))));
            Bitmap bm1 = (Bitmap)imageSource.Clone();
            Bitmap bm2 = (Bitmap)imageSource.Clone();

            if (type == "A")
            {
                imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 21 / 80, bm1.Height * 10 / 100, bm1.Width * 13 / 80, bm1.Height * 85 / 100);
            }
            else
            {
                imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 6 / 80, bm1.Height * 13 / 100, (bm1.Width * 14 / 80), bm1.Height * 77 / 100);
                imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 60 / 80), bm1.Height * 14 / 100, (bm2.Width * 6 / 40), bm1.Height * 75 / 100);
            }
            currentzoom = imageViewerTR1.CurrentZoom;
            currentzoom = imageViewerTR2.CurrentZoom;
            // lấy image content đã entry
            try
            {
                imgContent = dACheck.GetImageContent(id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            for (int i = 0; i < 2; i++)
            {
                lsttxt[i].ForeColor = Color.Red;
                lsttxt[i].Text = imgContent.Content1_OCR[i];
                numbertrtxt = i;
                string s1, s2;
                s1 = imgContent.Content1_OCR[i];
                s2 = imgContent.Content_OCR[i];
                if (s1 != s2)
                {
                    lstlbl[i].BackColor = Color.Red;
                }
                c = null;
                c = new int[s1.Length + 1, s2.Length + 1];
                LCS(s1, s2);
                BackTrack(s1, s2, s1.Length, s2.Length);
            }

            if (img.NG1 != 0)
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0)
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;
            if (img.NG3 != 0)
                lblEntry3.BackColor = Color.Red;
            else
                lblEntry3.BackColor = SystemColors.Window;



            // hiển thị user name của user đã entry            
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblEntry3.Text = imgContent.Name3;
            lblGroup3.Text = imgContent.Group3;
            lblbatchname.Text = batchName;
            txt1.Focus();
            bgwGetImage.RunWorkerAsync();
        }
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCheck_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!finish)
            {
                // unlock image
                dACheck.SetStatusQC(img.Id, "1");
                dACheck.SetHitPointImage(img.Id, 2);
                // unlock image       
                for (int i = 0; i < ListImage.Count; i++)
                {
                    dACheck.SetStatusQC(ListImage[i].Id, "1");
                    dACheck.SetHitPointImage(ListImage[i].Id, 2);
                }
                dACheck.UpdatePerformance(INFperformance[1], int.Parse(INFperformance[0]));
            }
            else
            {
                frmLogIn.fn = true;
            }
        }
        private void btnRL_Click(object sender, EventArgs e)
        {
            imageViewerTR1.RotateImage("90");
        }

        private void btnRR_Click(object sender, EventArgs e)
        {
            imageViewerTR1.RotateImage("270");
        }


        #region Compare string
        static int max(int a, int b)
        {
            return (a > b) ? a : b;
        }
        static int[,] c;
        //Prints one LCS
        private string BackTrack(string s1, string s2, int i, int j)
        {
            if (i == 0 || j == 0)
                return "";
            if (s1[i - 1] == s2[j - 1])
            {
                lsttxt[numbertrtxt].SelectionStart = i - 1;
                lsttxt[numbertrtxt].SelectionLength = 1;
                lsttxt[numbertrtxt].SelectionColor = Color.Black;
                return BackTrack(s1, s2, i - 1, j - 1) + s1[i - 1];
            }
            else if (c[i - 1, j] > c[i, j - 1])
                return BackTrack(s1, s2, i - 1, j);

            else
                return BackTrack(s1, s2, i, j - 1);

        }
        //Nghịch       
        private string BackTrack2(string s1, string s2, int i, int j)
        {
            if (i == 0 || j == 0)
                return "";
            if (s1[i - 1] == s2[j - 1])
            {
                return BackTrack2(s1, s2, i - 1, j - 1) + s1[i - 1];
            }
            else if (c[i - 1, j] > c[i, j - 1])
                return BackTrack2(s1, s2, i - 1, j);

            else
                return BackTrack2(s1, s2, i, j - 1);

        }
        static int LCS(string s1, string s2)
        {
            for (int i = 1; i <= s1.Length; i++)
                c[i, 0] = 0;
            for (int i = 1; i <= s2.Length; i++)
                c[0, i] = 0;

            for (int i = 1; i <= s1.Length; i++)
                for (int j = 1; j <= s2.Length; j++)
                {
                    if (s1[i - 1] == s2[j - 1])
                        c[i, j] = c[i - 1, j - 1] + 1;
                    else
                    {
                        c[i, j] = max(c[i - 1, j], c[i, j - 1]);

                    }

                }

            return c[s1.Length, s2.Length];

        }
        #endregion

        private void frmCheck_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
            if (e.Control)
            {
                if (e.KeyCode == Keys.Up)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Down)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    imageViewerTR1.RotateImage("270");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Left)
                {
                    imageViewerTR1.RotateImage("90");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.F)
                {
                    imageViewerTR1.CurrentZoom = currentzoom;
                    e.Handled = true;
                }
            }
        }

        private void btnSumit2_Click(object sender, EventArgs e)
        {
            btnSubmit.Enabled = false;
            imageViewerTR1.Dispose();
            // update image content vao DB           
            string rsCheck = "";
            rsCheck = String.Join("|", lsttxt.Select(x => x.Text.Trim()).ToArray());
            // set image content
            imgContent.Result = rsCheck;
            imgContent.Tongkytu = imgContent.Result.Length - 1;
            imgContent.CheckerId = userId;          
            // add image content
            try
            {
                dACheck.UpdateResutl2(imgContent, userId);
                imgContent = new BOImageContent_Check();
            }
            catch (Exception exception)
            {
                // rool back hitpoint
                dACheck.SetHitPointImage(img.Id, 2);
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            saveimageid = img.Id;
            // load image mới lên form
            // lấy id image entry
            dACheck.SetHitpointBatch(batchId);
            id = 0;
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                if (!bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                {
                    dACheck.UpdatePerformance(INFperformance[1], int.Parse(INFperformance[0]));
                    finish = true;
                    this.Close();
                    return;
                }
                else
                {
                    while (ListImage.Count == 0)
                    { }
                    img = ListImage[0];
                    ListImage.RemoveAt(0);
                }
            }

            // lay image đã entry
            try
            {
                ////Get Image
                //imageSource = (Bitmap)img.Imagesource.Clone();
                //imageViewerTR1.Image = imageSource;

                //nam cat anh
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageName, batchName.Substring(7, batchName.Length - 7))));
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();

                if (type == "A")
                {
                    imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 21 / 80, bm1.Height * 10 / 100, bm1.Width * 13 / 80, bm1.Height * 85 / 100);
                }
                else
                {
                    imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 6 / 80, bm1.Height * 13 / 100, (bm1.Width * 14 / 80), bm1.Height * 77 / 100);
                    imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 60 / 80), bm1.Height * 14 / 100, (bm2.Width * 6 / 40), bm1.Height * 75 / 100);
                }
                currentzoom = imageViewerTR1.CurrentZoom;
                currentzoom = imageViewerTR2.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageName, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // lấy image content đã entry
            try
            {
                imgContent = dACheck.GetImageContent(img.Id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }


            for (int i = 0; i < 2; i++)
            {
                lsttxt[i].Text = "";
                lstlbl[i].BackColor = SystemColors.Window;
                lsttxt[i].ForeColor = Color.Red;
                lsttxt[i].Text = imgContent.Content1_OCR[i];
                numbertrtxt = i;
                string s1, s2;
                s1 = imgContent.Content1_OCR[i];
                s2 = imgContent.Content_OCR[i];
                if (s1 != s2)
                {
                    lstlbl[i].BackColor = Color.Red;
                }
                c = null;
                c = new int[s1.Length + 1, s2.Length + 1];
                LCS(s1, s2);
                BackTrack(s1, s2, s1.Length, s2.Length);
            }

            if (img.NG1 != 0)
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0)
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;
            if (img.NG3 != 0)
                lblEntry3.BackColor = Color.Red;
            else
                lblEntry3.BackColor = SystemColors.Window;

            // hiển thị user name của user đã entry
            btnSubmit.Enabled = true;
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblEntry3.Text = imgContent.Name3;
            lblGroup3.Text = imgContent.Group3;
            lblbatchname.Text = batchName;
            if (!bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                bgwGetImage1.RunWorkerAsync();
            else if (bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                bgwGetImage1.RunWorkerAsync();
            else if (!bgwGetImage.IsBusy && bgwGetImage1.IsBusy)
                bgwGetImage.RunWorkerAsync();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            imageViewerTR1.CurrentZoom = currentzoom;
        }

        private void btnout_Click(object sender, EventArgs e)
        {
            imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
        }

        private void btnin_Click(object sender, EventArgs e)
        {
            imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            zom = 20;
            bm_out = null;
            if (saveimageid == 0)
            {
                MessageBox.Show("Can not back", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            // load image mới lên form
            // lấy id image entry
            id = 0;
            try
            {
                dACheck.SetHitPointImage(img.Id, 2);
                dACheck.Set_result_null(saveimageid);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            //lấy Imageid trước           
            try
            {
                id = saveimageid;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            saveimageid = 0;


            // lay image đã entry
            try
            {
                img = dACheck.GetImage(id);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            imageViewerTR1.Dispose();
            imageViewerTR2.Dispose();
            ////Get Image
            //imageSource = new Bitmap(Io_Entry.byteArrayToImage(dACheck.getImageOnServer(img.PageName, batchName.Substring(7, batchName.Length - 7))));
            //imageViewerTR1.Image = imageSource;

            //nam cat anh
            imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageName, batchName.Substring(7, batchName.Length - 7))));
            Bitmap bm1 = (Bitmap)imageSource.Clone();
            Bitmap bm2 = (Bitmap)imageSource.Clone();

            if (type == "A")
            {
                imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 21 / 80, bm1.Height * 10 / 100, bm1.Width * 13 / 80, bm1.Height * 85 / 100);
            }
            else
            {
                imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 6 / 80, bm1.Height * 13 / 100, (bm1.Width * 14 / 80), bm1.Height * 77 / 100);
                imageViewerTR2.Image = CropBitmap(bm2, (bm2.Width * 60 / 80), bm1.Height * 14 / 100, (bm2.Width * 6 / 40), bm1.Height * 75 / 100);
            }
            currentzoom = imageViewerTR1.CurrentZoom;
            currentzoom = imageViewerTR2.CurrentZoom;

            // lấy image content đã entry
            try
            {
                imgContent = dACheck.GetImageContent(id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            for (int i = 0; i < 2; i++)
            {
                lsttxt[i].Text = "";
                lstlbl[i].BackColor = SystemColors.Window;
                lsttxt[i].ForeColor = Color.Red;
                lsttxt[i].Text = imgContent.Content1_OCR[i];
                numbertrtxt = i;
                string s1, s2;
                s1 = imgContent.Content1_OCR[i];
                s2 = imgContent.Content_OCR[i];
                if (s1 != s2)
                {
                    lstlbl[i].BackColor = Color.Red;
                }
                c = null;
                c = new int[s1.Length + 1, s2.Length + 1];
                LCS(s1, s2);
                BackTrack(s1, s2, s1.Length, s2.Length);
            }

            if (img.NG1 != 0)
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0)
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;
            if (img.NG3 != 0)
                lblEntry3.BackColor = Color.Red;
            else
                lblEntry3.BackColor = SystemColors.Window;

            // hiển thị user name của user đã entry            
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblEntry3.Text = imgContent.Name3;
            lblGroup3.Text = imgContent.Group3;
            lblbatchname.Text = batchName;
            if (!bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                bgwGetImage1.RunWorkerAsync();
            else if (bgwGetImage.IsBusy && !bgwGetImage1.IsBusy)
                bgwGetImage1.RunWorkerAsync();
            else if (!bgwGetImage.IsBusy && bgwGetImage1.IsBusy)
                bgwGetImage.RunWorkerAsync();
        }
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {

        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox1_Enter(object sender, EventArgs e)
        {

        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            RichTextBox tb = (RichTextBox)sender;
            if (tb.TabIndex >= 0 && tb.TabIndex <= 4)
            {
                if (e.KeyChar == 43)
                    e.Handled = true;
                else if (e.KeyChar == 45)
                    e.Handled = true;
            }
            if (e.KeyChar == 13)
                e.Handled = true;
            if (e.KeyChar == 102)
                e.Handled = true;
            if (e.KeyChar == 97)
                e.Handled = true;
            if (e.KeyChar == 122)
                e.Handled = true;
            if (e.KeyChar == 110)
                e.Handled = true;
            if (e.KeyChar == 113)
                e.Handled = true;
            if (e.KeyChar == 96)
                e.Handled = true;
            if (e.KeyChar == 101)
                e.Handled = true;
        }
        private void textBox1_Leave(object sender, EventArgs e)
        {

        }

        private void rtxt_MouseDown(object sender, MouseEventArgs e)
        {
            RichTextBox rtxt = (RichTextBox)sender;
            if (e.Button == MouseButtons.Right)
            {
                if (rtxt.Text == imgContent.Content1_OCR[rtxt.TabIndex - 1])
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content_OCR[rtxt.TabIndex - 1];
                    numbertrtxt = rtxt.TabIndex - 1;
                    string s1, s2;
                    s1 = imgContent.Content_OCR[rtxt.TabIndex - 1];
                    s2 = imgContent.Content1_OCR[rtxt.TabIndex - 1];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                else if (rtxt.Text == imgContent.Content_OCR[rtxt.TabIndex - 1])
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1_OCR[rtxt.TabIndex - 1];
                    numbertrtxt = rtxt.TabIndex - 1;
                    string s1, s2;
                    s1 = imgContent.Content1_OCR[rtxt.TabIndex - 1];
                    s2 = imgContent.Content_OCR[rtxt.TabIndex - 1];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                else
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1_OCR[rtxt.TabIndex - 1];
                    numbertrtxt = rtxt.TabIndex - 1;
                    string s1, s2;
                    s1 = imgContent.Content1_OCR[rtxt.TabIndex - 1];
                    s2 = imgContent.Content_OCR[rtxt.TabIndex - 1];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
            }
        }

        private void bgwGetImage_DoWork(object sender, DoWorkEventArgs e)
        {
            while (ListImage.Count < 2)
            {
                int id2 = dACheck.GetIdImageEntriedFromBatchIdQC(batchId);
                if (id2 > 0)
                {
                    BOImage_Check img2 = dACheck.GetImage(id2);
                    try
                    {
                        img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dACheck.getImageOnServer(img2.PageName, batchName.Substring(7, batchName.Length - 7))));
                    }
                    catch { img2.Imagesource = null; }
                    ListImage.Add(img2);
                }
                else
                    break;
            }
        }

        private void bgwGetImage1_DoWork(object sender, DoWorkEventArgs e)
        {
            while (ListImage.Count < 2)
            {
                int id2 = dACheck.GetIdImageEntriedFromBatchIdQC(batchId);
                if (id2 > 0)
                {
                    BOImage_Check img2 = dACheck.GetImage(id2);
                    try
                    {
                        img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dACheck.getImageOnServer(img2.PageName, batchName.Substring(7, batchName.Length - 7))));
                    }
                    catch { img2.Imagesource = null; }
                    ListImage.Add(img2);
                }
                else
                    break;
            }
        }

        private void bgwGetImage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblImage.Text = dACheck.ImageExistCheckQC(batchId);
        }
        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }

        private void lbl1_Click(object sender, EventArgs e)
        {

        }
    }
}

