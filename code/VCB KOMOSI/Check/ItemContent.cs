﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VCB_KOMOSI
{
    class ItemContent
    {
        private string content1;
        public string Content1
        {
            get { return content1; }
            set { content1 = value; }
        }
        private string content2;
        public string Content2
        {
            get { return content2; }
            set { content2 = value; }
        }
        RichTextBox  rtxt;
        public RichTextBox Rtxt
        {
            get { return rtxt; }
            set { rtxt = value; }
        }

        Label lbl;
        public Label Lbl
        {
            get { return lbl; }
            set { lbl = value; }
        }
    }
}
