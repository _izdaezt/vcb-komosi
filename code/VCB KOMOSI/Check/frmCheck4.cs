﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
namespace VCB_KOMOSI
{
    public partial class frmCheck4 : Form
    {
        public int formId;
        public int batchId;
        public int userId;
        public string userName;
        public string batchName;
        public string template;
        public string level;
        Bitmap imageSource;
        string[] arrsh = new string[0];
        string[] arrsl = new string[0];
        string[] arrgm = new string[0];
        string[] arrgb = new string[0];
        private double scale = 1;
        private int numbertrtxt = 0;
        private DAEntry_Check dACheck = new DAEntry_Check();
        private BOImage_Check img = new BOImage_Check();
        private BOImageContent_Check imgContent = new BOImageContent_Check();
        List<BOImage_Check> ListImage = new List<BOImage_Check>();
        private bool finish = false;
        double zom = 20;
        private System.Windows.Forms.TextBox txtAutoComplete = new System.Windows.Forms.TextBox();
        DataTable dthct = new DataTable();
        double tyleImageNew = 1;
        Bitmap bm_out;
        int id = 0;
        int demid = 0;
        int saveimageid = 0;
        public string[] INFperformance = new string[2];
        List<RichTextBox> lsttxt;
        List<Label> lstlbl;
        List<TextBox> lstctxt;
        List<Label> lstclbl;
        List<Image> lstimageload = new List<Image>();
        float currentzoom;
        private DAEntry_Entry dAEntry = new DAEntry_Entry();
        string batchnameLocal;
        public string typeCheck = "";
        DateTime dtimeBefore = new DateTime();
        public frmCheck4()
        {
            InitializeComponent();
            lsttxt = new List<RichTextBox>() { txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8 };
            lstlbl = new List<Label>() { lbl1, lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8 };
            lstctxt = new List<TextBox>() { ctxt1, ctxt2, ctxt3, ctxt4, ctxt5, ctxt6, ctxt7, ctxt8, ctxt9, ctxt10, ctxt11, ctxt12, ctxt13, ctxt14, ctxt15, ctxt16, ctxt17, ctxt18, ctxt19, ctxt20, ctxt21, ctxt22, ctxt23, ctxt24, ctxt25, ctxt26, ctxt27, ctxt28, ctxt29 };
            lstclbl = new List<Label>() { clbl1, clbl2, clbl3, clbl4, clbl5, clbl6, clbl7, clbl8, clbl9, clbl10, clbl11, clbl12, clbl13, clbl14, clbl15, clbl16, clbl17, clbl18, clbl19, clbl20, clbl21, clbl22, clbl23, clbl24, clbl25, clbl26, clbl27, clbl28, clbl29 };
            //lstclbl = new List<Label>() { clbl8, clbl9, clbl10, clbl11, clbl12, clbl13, clbl14, clbl15, clbl16, clbl17, clbl18, clbl19, clbl20, clbl21, clbl22, clbl23, clbl24, clbl25, clbl26, clbl27, clbl28, clbl29 };
        }
        private void frmCheck_Load(object sender, EventArgs e)
        {       
            // set bien toan cuc         
            lsttxt.ForEach(a =>
            {
                a.Enter += new System.EventHandler(this.textBox1_Enter);
                a.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
                a.Leave += new System.EventHandler(this.textBox1_Leave);
                a.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
                a.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
                a.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rtxt_MouseDown);
            });
            lstctxt.ForEach(a => a.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txt_MouseDown));

            batchnameLocal = batchName.Substring(7);

            // lấy id image đã entry để check            
            // check đã entry
            try
            {
                id = dACheck.GetIdCheck(batchnameLocal, formId, typeCheck);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }

            if (id == 0)
            {
                dACheck.SetHitpointBatch(batchName, formId);
                finish = true;
                this.Close();
                return;
            }

            timegetanh.Start();

            // lay image đã entry
            try
            {
                img = dACheck.GetImage(id);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            //nam 25/7 cắt ảnh
            imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageName, batchnameLocal)));
            Bitmap bm1 = (Bitmap)imageSource.Clone();
            Bitmap bm2 = (Bitmap)imageSource.Clone();
            imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 9 / 100, bm1.Height * 19 / 100, (bm1.Width * 38 / 100), (bm1.Height * 49 / 100));
            imageViewerTR2.Image = CropBitmap(bm2, bm2.Width * 65 / 100, bm2.Height * 14 / 100, (bm2.Width * 35 / 100), (bm2.Height * 45 / 100));
            currentzoom = imageViewerTR2.CurrentZoom;
            try
            {
                imgContent = dACheck.GetImageContent(id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            for (int i = 0; i < 8; i++)
            {
                try
                {
                    lsttxt[i].ForeColor = Color.Red;
                    lsttxt[i].Text = imgContent.Content1_OCR[i];
                    numbertrtxt = i;
                    string s1, s2;
                    s1 = imgContent.Content1_OCR[i];
                    s2 = imgContent.Content3[i];
                    lstlbl[i].Text = lstlbl[i].Text + "  E1";
                    if (s1 != s2)
                    {
                        lstlbl[i].BackColor = Color.Red;
                    }
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                catch { }
            }

            for (int i = 7; i < 29; i++)
            {
                lstctxt[i].BackColor = SystemColors.Window; ;
                lstclbl[i].BackColor = SystemColors.Window;
                if (imgContent.Content1[i - 5] != "")
                {
                    lstctxt[i].BackColor = Color.YellowGreen;
                }
                if (imgContent.Content1[i - 5] != imgContent.Content2[i - 5])
                {
                    lstclbl[i].BackColor = Color.Red;
                }
            }

            lstctxt[0].BackColor = SystemColors.Window;
            lstclbl[0].BackColor = SystemColors.Window;
            lstctxt[1].BackColor = SystemColors.Window;
            lstclbl[1].BackColor = SystemColors.Window;
            lstctxt[2].BackColor = SystemColors.Window;
            lstclbl[2].BackColor = SystemColors.Window;
            lstctxt[3].BackColor = SystemColors.Window;
            lstclbl[3].BackColor = SystemColors.Window;
            lstctxt[4].BackColor = SystemColors.Window;
            lstclbl[4].BackColor = SystemColors.Window;
            lstctxt[5].BackColor = SystemColors.Window;
            lstclbl[5].BackColor = SystemColors.Window;
            lstctxt[6].BackColor = SystemColors.Window;
            lstclbl[6].BackColor = SystemColors.Window;
            if (imgContent.Content1[0] == imgContent.Content2[0])
            {
                if (imgContent.Content1[0] == "1")
                {
                    lstctxt[0].BackColor = Color.YellowGreen;
                }
                else if (imgContent.Content1[0] == "2")
                {
                    lstctxt[1].BackColor = Color.YellowGreen;
                }
            }
            else
            {
                if (imgContent.Content1[0] == "1")
                {
                    lstctxt[0].BackColor = Color.YellowGreen;
                    lstclbl[0].BackColor = Color.Red;
                }
                else if (imgContent.Content1[0] == "2")
                {
                    lstctxt[1].BackColor = Color.YellowGreen;
                    lstclbl[1].BackColor = Color.Red;
                }
            }
            if (imgContent.Content1[1] == imgContent.Content2[1])
            {
                if (imgContent.Content1[1] != "")
                {
                    lstctxt[Convert.ToInt16(imgContent.Content1[1]) - 1].BackColor = Color.YellowGreen;
                }
            }
            else
            {
                if (imgContent.Content1[1] != "")
                {
                    lstctxt[Convert.ToInt16(imgContent.Content1[1]) - 1].BackColor = Color.YellowGreen;
                    lstclbl[Convert.ToInt16(imgContent.Content1[1]) - 1].BackColor = Color.Red;
                }
            }

            // hiển thị user name của user đã entry            
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblEntry3.Text = imgContent.Name3;
            lblGroup3.Text = imgContent.Group3;
            lblbatchname.Text = batchName;
            if (img.NG1 != 0 && typeCheck == "QC")
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0 && typeCheck == "QC")
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;
            if (img.NG3 != 0 && typeCheck == "QC")
                lblEntry3.BackColor = Color.Red;
            else
                lblEntry3.BackColor = SystemColors.Window;
            dtimeBefore = DateTime.Now;
            if (typeCheck == "QC")
                lblImage.Text = dACheck.ImageExistQC(batchnameLocal, formId);
            else
                lblImage.Text = dACheck.ImageExistCheck(batchnameLocal, formId);
        }
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCheck_FormClosing(object sender, FormClosingEventArgs e)
        {
            timegetanh.Stop();
            try
            {
                if (!finish)
                {
                    // unlock image
                    dACheck.SetHitPointImage(img.Id, typeCheck);
                    // unlock image       
                    for (int i = 0; i < ListImage.Count; i++)
                    {
                        dACheck.SetHitPointImage(ListImage[i].Id, typeCheck);
                    }
                }
                else
                {
                    frmLogIn.fn = true;
                    dACheck.SetHitpointBatch(batchnameLocal, formId);
                }
            }
            catch { }
        }
        private void btnRL_Click(object sender, EventArgs e)
        {
            imageViewerTR1.RotateImage("90");
        }

        private void btnRR_Click(object sender, EventArgs e)
        {
            imageViewerTR1.RotateImage("270");
        }


        #region Compare string
        static int max(int a, int b)
        {
            return (a > b) ? a : b;
        }
        static int[,] c;
        //Prints one LCS
        private string BackTrack(string s1, string s2, int i, int j)
        {
            if (i == 0 || j == 0)
                return "";
            if (s1[i - 1] == s2[j - 1])
            {
                lsttxt[numbertrtxt].SelectionStart = i - 1;
                lsttxt[numbertrtxt].SelectionLength = 1;
                lsttxt[numbertrtxt].SelectionColor = Color.Black;
                return BackTrack(s1, s2, i - 1, j - 1) + s1[i - 1];
            }
            else if (c[i - 1, j] > c[i, j - 1])
                return BackTrack(s1, s2, i - 1, j);

            else
                return BackTrack(s1, s2, i, j - 1);

        }
        //Nghịch       
        private string BackTrack2(string s1, string s2, int i, int j)
        {
            if (i == 0 || j == 0)
                return "";
            if (s1[i - 1] == s2[j - 1])
            {
                return BackTrack2(s1, s2, i - 1, j - 1) + s1[i - 1];
            }
            else if (c[i - 1, j] > c[i, j - 1])
                return BackTrack2(s1, s2, i - 1, j);

            else
                return BackTrack2(s1, s2, i, j - 1);

        }
        static int LCS(string s1, string s2)
        {
            for (int i = 1; i <= s1.Length; i++)
                c[i, 0] = 0;
            for (int i = 1; i <= s2.Length; i++)
                c[0, i] = 0;

            for (int i = 1; i <= s1.Length; i++)
                for (int j = 1; j <= s2.Length; j++)
                {
                    if (s1[i - 1] == s2[j - 1])
                        c[i, j] = c[i - 1, j - 1] + 1;
                    else
                    {
                        c[i, j] = max(c[i - 1, j], c[i, j - 1]);

                    }

                }

            return c[s1.Length, s2.Length];

        }
        #endregion

        private void frmCheck_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
            if (e.Control)
            {
                if (e.KeyCode == Keys.Up)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Down)
                {
                    imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    imageViewerTR1.RotateImage("270");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Left)
                {
                    imageViewerTR1.RotateImage("90");
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.F)
                {
                    imageViewerTR2.CurrentZoom = currentzoom;
                    e.Handled = true;
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            imageViewerTR2.CurrentZoom = currentzoom;
        }

        private void btnout_Click(object sender, EventArgs e)
        {
            imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom <= 0.1f ? 0.1f : imageViewerTR1.CurrentZoom - 0.1f;
        }

        private void btnin_Click(object sender, EventArgs e)
        {
            imageViewerTR1.CurrentZoom = imageViewerTR1.CurrentZoom + 0.1f;
        }
        private void textBox1_Enter(object sender, EventArgs e)
        {

        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {

        }
        private void textBox1_Leave(object sender, EventArgs e)
        {

        }

        private void txt_MouseDown(object sender, MouseEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            if (e.Button == MouseButtons.Right)
            {
                if (txt.BackColor == Color.YellowGreen)
                {
                    txt.BackColor = SystemColors.Window;
                }
                else
                {
                    txt.BackColor = Color.YellowGreen;
                    if (txt.TabIndex == 1)
                    {
                        lstctxt[1].BackColor = SystemColors.Window;
                    }
                    else if (txt.TabIndex == 2)
                    {
                        lstctxt[0].BackColor = SystemColors.Window;
                    }
                    else if (txt.TabIndex == 3)
                    {
                        lstctxt[3].BackColor = SystemColors.Window;
                        lstctxt[4].BackColor = SystemColors.Window;
                        lstctxt[5].BackColor = SystemColors.Window;
                        lstctxt[6].BackColor = SystemColors.Window;
                    }
                    else if (txt.TabIndex == 4)
                    {
                        lstctxt[2].BackColor = SystemColors.Window;
                        lstctxt[4].BackColor = SystemColors.Window;
                        lstctxt[5].BackColor = SystemColors.Window;
                        lstctxt[6].BackColor = SystemColors.Window;
                    }
                    else if (txt.TabIndex == 5)
                    {
                        lstctxt[3].BackColor = SystemColors.Window;
                        lstctxt[2].BackColor = SystemColors.Window;
                        lstctxt[5].BackColor = SystemColors.Window;
                        lstctxt[6].BackColor = SystemColors.Window;
                    }
                    else if (txt.TabIndex == 6)
                    {
                        lstctxt[3].BackColor = SystemColors.Window;
                        lstctxt[4].BackColor = SystemColors.Window;
                        lstctxt[2].BackColor = SystemColors.Window;
                        lstctxt[6].BackColor = SystemColors.Window;
                    }
                    else if (txt.TabIndex == 7)
                    {
                        lstctxt[3].BackColor = SystemColors.Window;
                        lstctxt[4].BackColor = SystemColors.Window;
                        lstctxt[5].BackColor = SystemColors.Window;
                        lstctxt[2].BackColor = SystemColors.Window;
                    }

                }
            }
        }
        private void rtxt_MouseDown(object sender, MouseEventArgs e)
        {
            RichTextBox rtxt = (RichTextBox)sender;
            if (e.Button == MouseButtons.Right)
            {
                if (rtxt.Text == imgContent.Content1_OCR[rtxt.TabIndex - 1])
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content3[rtxt.TabIndex - 1];
                    numbertrtxt = rtxt.TabIndex - 1;
                    string s1, s2;
                    s1 = imgContent.Content3[rtxt.TabIndex - 1];
                    s2 = imgContent.Content1_OCR[rtxt.TabIndex - 1];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                else if (rtxt.Text == imgContent.Content3[rtxt.TabIndex - 1])
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1_OCR[rtxt.TabIndex - 1];
                    numbertrtxt = rtxt.TabIndex - 1;
                    string s1, s2;
                    s1 = imgContent.Content1_OCR[rtxt.TabIndex - 1];
                    s2 = imgContent.Content3[rtxt.TabIndex - 1];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                else
                {
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1_OCR[rtxt.TabIndex - 1];
                    numbertrtxt = rtxt.TabIndex - 1;
                    string s1, s2;
                    s1 = imgContent.Content1_OCR[rtxt.TabIndex - 1];
                    s2 = imgContent.Content3[rtxt.TabIndex - 1];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            btnSubmit.Enabled = false;
            imageViewerTR1.Dispose();
            // update image content vao DB

            string[] arrContent1 = new string[24];
            int loi1 = 0;
            arrContent1[0] = (lstctxt[0].BackColor == Color.YellowGreen ? lstctxt[0].Text : "") + (lstctxt[1].BackColor == Color.YellowGreen ? lstctxt[1].Text : "");
            arrContent1[1] = (lstctxt[2].BackColor == Color.YellowGreen ? lstctxt[2].Text : "") + (lstctxt[3].BackColor == Color.YellowGreen ? lstctxt[3].Text : "") +
                             (lstctxt[4].BackColor == Color.YellowGreen ? lstctxt[4].Text : "") + (lstctxt[5].BackColor == Color.YellowGreen ? lstctxt[5].Text : "") +
                             (lstctxt[6].BackColor == Color.YellowGreen ? lstctxt[6].Text : "");
            int countchars = arrContent1[0].Length + arrContent1[1].Length;
            if (arrContent1[0] != imgContent.Content1[0])
                loi1++;
            if (arrContent1[0] != imgContent.Content2[0])
                imgContent.Loisai2++;
            if (arrContent1[1] != imgContent.Content1[1])
                loi1++;
            if (arrContent1[1] != imgContent.Content2[1])
                imgContent.Loisai2++;
            for (int i = 7; i < 29; i++)
            {
                arrContent1[i - 5] = "";
                if (lstctxt[i].BackColor == Color.YellowGreen)
                {
                    arrContent1[i - 5] = lstctxt[i].Text;
                    countchars++;
                }
                if (arrContent1[i - 5] != imgContent.Content1[i - 5])
                    loi1++;
                if (arrContent1[i - 5] != imgContent.Content2[i - 5])
                    imgContent.Loisai2++;
                lstctxt[i].BackColor = SystemColors.Window;
                lstclbl[i].BackColor = SystemColors.Window;
            }

            string rsEntr1 = String.Join("|", imgContent.Content1_OCR);
            string rsEntr2 = String.Join("|", imgContent.Content3);
            string rsCheck = "";
            rsCheck = String.Join("|", lsttxt.Select(x => x.Text.Trim()).ToArray());
            if (typeCheck == "Check")
            {
                //Lỗi sai E1
                c = null;
                c = new int[rsCheck.Length + 1, rsEntr1.Length + 1];
                int vlLCS = LCS(rsCheck, rsEntr1);
                if (rsEntr1 == rsCheck)
                    imgContent.Loisai1 = 0;
                else if (rsEntr1.Length > rsCheck.Length)
                    imgContent.Loisai1 = rsEntr1.Length - vlLCS;
                else
                    imgContent.Loisai1 = rsCheck.Length - vlLCS;
                //Lỗi sai E3 
                c = null;
                c = new int[rsCheck.Length + 1, rsEntr2.Length + 1];
                vlLCS = LCS(rsCheck, rsEntr2);
                if (rsEntr2 == rsCheck)
                    imgContent.Loisai3 = 0;
                else if (rsEntr2.Length > rsCheck.Length)
                    imgContent.Loisai3 = rsEntr2.Length - vlLCS;
                else
                    imgContent.Loisai3 = rsCheck.Length - vlLCS;
            }
            else
            {
                imgContent.Loisai2 = 0;
            }
            // set image content           
            imgContent.Result = string.Join("|", arrContent1) + "|" + rsCheck;
            imgContent.Tongkytu = imgContent.Result.Length + countchars - 7;
            imgContent.CheckerId = userId;
            TimeSpan span = DateTime.Now - dtimeBefore;
            int ms = (int)span.TotalMilliseconds;
            // add image content
            try
            {
                txt1.Text = ""; txt2.Text = ""; txt3.Text = ""; txt4.Text = ""; txt5.Text = ""; txt6.Text = ""; txt7.Text = ""; txt8.Text = "";
                if (typeCheck == "Check")
                    dACheck.UpdateResutl2(imgContent, userId, ms);
                else
                    dACheck.UpdateQC(imgContent, userId, ms);
                imgContent = new BOImageContent_Check();
            }
            catch (Exception exception)
            {
                // rool back hitpoint              
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            saveimageid = img.Id;
            // load image mới lên form
            // lấy id image entry
            id = 0;
            imageSource = null;
            if (ListImage.Count > 0)
            {
                img = ListImage[0];
                ListImage.RemoveAt(0);
            }
            else
            {
                dACheck.SetHitpointBatch(batchName, formId);
                finish = true;
                this.Close();
                return;
            }

            // lay image đã entry
            try
            {
                //nam 25/7 cắt ảnh
                imageSource = img.Imagesource;
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();
                imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 9 / 100, bm1.Height * 19 / 100, (bm1.Width * 38 / 100), (bm1.Height * 49 / 100));
                imageViewerTR2.Image = CropBitmap(bm2, bm2.Width * 65 / 100, bm2.Height * 18 / 100, (bm2.Width * 35 / 100), (bm2.Height * 51 / 100));
                currentzoom = imageViewerTR2.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageName, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            // lấy image content đã entry
            try
            {
                imgContent = dACheck.GetImageContent(img.Id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            for (int i = 0; i < 8; i++)
            {
                try
                {
                    lstlbl[i].BackColor = SystemColors.Window;
                    lsttxt[i].ForeColor = Color.Red;
                    lsttxt[i].Text = imgContent.Content1_OCR[i];
                    numbertrtxt = i;
                    string s1, s2;
                    s1 = imgContent.Content1_OCR[i];
                    s2 = imgContent.Content3[i];
                    lstlbl[i].Text = lstlbl[i].Text.Substring(0, lstlbl[i].Text.Length - 1) + "1";
                    if (s1 != s2)
                    {
                        lstlbl[i].BackColor = Color.Red;
                    }
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                catch { }
            }

            for (int i = 7; i < 29; i++)
            {
                if (imgContent.Content1[i - 5] != "")
                {
                    lstctxt[i].BackColor = Color.YellowGreen;
                }
                if (imgContent.Content1[i - 5] != imgContent.Content2[i - 5])
                {
                    lstclbl[i].BackColor = Color.Red;
                }
            }

            lstctxt[0].BackColor = SystemColors.Window;
            lstclbl[0].BackColor = SystemColors.Window;
            lstctxt[1].BackColor = SystemColors.Window;
            lstclbl[1].BackColor = SystemColors.Window;
            lstctxt[2].BackColor = SystemColors.Window;
            lstclbl[2].BackColor = SystemColors.Window;
            lstctxt[3].BackColor = SystemColors.Window;
            lstclbl[3].BackColor = SystemColors.Window;
            lstctxt[4].BackColor = SystemColors.Window;
            lstclbl[4].BackColor = SystemColors.Window;
            lstctxt[5].BackColor = SystemColors.Window;
            lstclbl[5].BackColor = SystemColors.Window;
            lstctxt[6].BackColor = SystemColors.Window;
            lstclbl[6].BackColor = SystemColors.Window;
            if (imgContent.Content1[0] == imgContent.Content2[0])
            {
                if (imgContent.Content1[0] == "1")
                {
                    lstctxt[0].BackColor = Color.YellowGreen;
                }
                else if (imgContent.Content1[0] == "2")
                {
                    lstctxt[1].BackColor = Color.YellowGreen;
                }
            }
            else
            {
                if (imgContent.Content1[0] == "1")
                {
                    lstctxt[0].BackColor = Color.YellowGreen;
                    lstclbl[0].BackColor = Color.Red;
                }
                else if (imgContent.Content1[0] == "2")
                {
                    lstctxt[1].BackColor = Color.YellowGreen;
                    lstclbl[1].BackColor = Color.Red;
                }
            }
            if (imgContent.Content1[1] == imgContent.Content2[1])
            {
                if (imgContent.Content1[1] != "")
                {
                    lstctxt[Convert.ToInt16(imgContent.Content1[1]) - 1].BackColor = Color.YellowGreen;
                }
            }
            else
            {
                if (imgContent.Content1[1] != "")
                {
                    lstctxt[Convert.ToInt16(imgContent.Content1[1]) - 1].BackColor = Color.YellowGreen;
                    lstclbl[Convert.ToInt16(imgContent.Content1[1]) - 1].BackColor = Color.Red;
                }
            }

            // hiển thị user name của user đã entry            
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblEntry3.Text = imgContent.Name3;
            lblGroup3.Text = imgContent.Group3;
            if (img.NG1 != 0 && typeCheck == "QC")
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0 && typeCheck == "QC")
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;
            if (img.NG3 != 0 && typeCheck == "QC")
                lblEntry3.BackColor = Color.Red;
            else
                lblEntry3.BackColor = SystemColors.Window;
            btnSubmit.Enabled = true;
            dtimeBefore = DateTime.Now;
            if (typeCheck == "QC")
                lblImage.Text = dACheck.ImageExistQC(batchnameLocal, formId);
            else
                lblImage.Text = dACheck.ImageExistCheck(batchnameLocal, formId);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            zom = 20;
            bm_out = null;
            if (saveimageid == 0)
            {
                MessageBox.Show("Can not back", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            // load image mới lên form
            // lấy id image entry
            id = 0;
            try
            {
                dACheck.SetHitPointImage(img.Id, typeCheck);
                dACheck.Set_result_null(saveimageid);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            //lấy Imageid trước           
            try
            {
                id = saveimageid;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            saveimageid = 0;

            // lay image đã entry
            try
            {
                img = dACheck.GetImage(id);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            try
            {
                imageViewerTR1.Dispose();
                imageViewerTR2.Dispose();
                imageSource = new Bitmap(Io_Entry.byteArrayToImage(dAEntry.getImageOnServer(img.PageName, batchnameLocal)));
                Bitmap bm1 = (Bitmap)imageSource.Clone();
                Bitmap bm2 = (Bitmap)imageSource.Clone();
                imageViewerTR1.Image = CropBitmap(bm1, bm1.Width * 9 / 100, bm1.Height * 19 / 100, (bm1.Width * 38 / 100), (bm1.Height * 49 / 100));
                imageViewerTR2.Image = CropBitmap(bm2, bm2.Width * 65 / 100, bm2.Height * 18 / 100, (bm2.Width * 35 / 100), (bm2.Height * 51 / 100));
                currentzoom = imageViewerTR2.CurrentZoom;
            }
            catch
            {
                MessageBox.Show("Không load được ảnh: " + img.PageName, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            // lấy image content đã entry
            try
            {
                imgContent = dACheck.GetImageContent(id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            for (int i = 0; i < 8; i++)
            {
                try
                {
                    lsttxt[i].Text = "";
                    lstlbl[i].BackColor = SystemColors.Window;
                    lsttxt[i].ForeColor = Color.Red;
                    lsttxt[i].Text = imgContent.Content1_OCR[i];
                    numbertrtxt = i;
                    string s1, s2;
                    s1 = imgContent.Content1_OCR[i];
                    s2 = imgContent.Content3[i];
                    lstlbl[i].Text = lstlbl[i].Text.Substring(0, lstlbl[i].Text.Length - 1) + "1";
                    if (s1 != s2)
                    {
                        lstlbl[i].BackColor = Color.Red;
                    }
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                catch { }
            }

            for (int i = 7; i < 29; i++)
            {
                lstctxt[i].BackColor = SystemColors.Window; ;
                lstclbl[i].BackColor = SystemColors.Window;
                if (imgContent.Content1[i - 5] != "")
                {
                    lstctxt[i].BackColor = Color.YellowGreen;
                }
                if (imgContent.Content1[i - 5] != imgContent.Content2[i - 5])
                {
                    lstclbl[i].BackColor = Color.Red;
                }
            }

            lstctxt[0].BackColor = SystemColors.Window;
            lstclbl[0].BackColor = SystemColors.Window;
            lstctxt[1].BackColor = SystemColors.Window;
            lstclbl[1].BackColor = SystemColors.Window;
            lstctxt[2].BackColor = SystemColors.Window;
            lstclbl[2].BackColor = SystemColors.Window;
            lstctxt[3].BackColor = SystemColors.Window;
            lstclbl[3].BackColor = SystemColors.Window;
            lstctxt[4].BackColor = SystemColors.Window;
            lstclbl[4].BackColor = SystemColors.Window;
            lstctxt[5].BackColor = SystemColors.Window;
            lstclbl[5].BackColor = SystemColors.Window;
            lstctxt[6].BackColor = SystemColors.Window;
            lstclbl[6].BackColor = SystemColors.Window;
            if (imgContent.Content1[0] == imgContent.Content2[0])
            {
                if (imgContent.Content1[0] == "1")
                {
                    lstctxt[0].BackColor = Color.YellowGreen;
                }
                else if (imgContent.Content1[0] == "2")
                {
                    lstctxt[1].BackColor = Color.YellowGreen;
                }
            }
            else
            {
                if (imgContent.Content1[0] == "1")
                {
                    lstctxt[0].BackColor = Color.YellowGreen;
                    lstclbl[0].BackColor = Color.Red;
                }
                else if (imgContent.Content1[0] == "2")
                {
                    lstctxt[1].BackColor = Color.YellowGreen;
                    lstclbl[1].BackColor = Color.Red;
                }
            }
            if (imgContent.Content1[1] == imgContent.Content2[1])
            {
                if (imgContent.Content1[1] != "")
                {
                    lstctxt[Convert.ToInt16(imgContent.Content1[1]) - 1].BackColor = Color.YellowGreen;
                }
            }
            else
            {
                if (imgContent.Content1[1] != "")
                {
                    lstctxt[Convert.ToInt16(imgContent.Content1[1]) - 1].BackColor = Color.YellowGreen;
                    lstclbl[Convert.ToInt16(imgContent.Content1[1]) - 1].BackColor = Color.Red;
                }
            }
            // hiển thị user name của user đã entry            
            lblEntry1.Text = imgContent.Name1;
            lblfilename.Text = img.PageName;
            lblEntry2.Text = imgContent.Name2;
            lblGroup1.Text = imgContent.Group1;
            lblGroup2.Text = imgContent.Group2;
            lblEntry3.Text = imgContent.Name3;
            lblGroup3.Text = imgContent.Group3;
            if (img.NG1 != 0 && typeCheck == "QC")
                lblEntry1.BackColor = Color.Red;
            else
                lblEntry1.BackColor = SystemColors.Window;
            if (img.NG2 != 0 && typeCheck == "QC")
                lblEntry2.BackColor = Color.Red;
            else
                lblEntry2.BackColor = SystemColors.Window;
            if (img.NG3 != 0 && typeCheck == "QC")
                lblEntry3.BackColor = Color.Red;
            else
                lblEntry3.BackColor = SystemColors.Window;
            dtimeBefore = DateTime.Now;
            if (typeCheck == "QC")
                lblImage.Text = dACheck.ImageExistQC(batchnameLocal, formId);
            else
                lblImage.Text = dACheck.ImageExistCheck(batchnameLocal, formId);
        }
        private void rtxtsbd_MouseDown(object sender, MouseEventArgs e)
        {
            RichTextBox rtxt = (RichTextBox)sender;
            if (e.Button == MouseButtons.Right)
            {
                if (rtxt.Text == imgContent.Content1_OCR[rtxt.TabIndex])
                {
                    lstlbl[rtxt.TabIndex - 1].Text = lstlbl[rtxt.TabIndex - 1].Text.Substring(0, lstlbl[rtxt.TabIndex - 1].Text.Length - 1) + "2";
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content3[rtxt.TabIndex];
                    numbertrtxt = rtxt.TabIndex;
                    string s1, s2;
                    s1 = imgContent.Content3[rtxt.TabIndex];
                    s2 = imgContent.Content1_OCR[rtxt.TabIndex];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                else if (rtxt.Text == imgContent.Content3[rtxt.TabIndex])
                {
                    lstlbl[rtxt.TabIndex - 1].Text = lstlbl[rtxt.TabIndex - 1].Text.Substring(0, lstlbl[rtxt.TabIndex - 1].Text.Length - 1) + "1";
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1_OCR[rtxt.TabIndex];
                    numbertrtxt = rtxt.TabIndex;
                    string s1, s2;
                    s1 = imgContent.Content1_OCR[rtxt.TabIndex];
                    s2 = imgContent.Content3[rtxt.TabIndex];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
                else
                {
                    lstlbl[rtxt.TabIndex - 1].Text = lstlbl[rtxt.TabIndex - 1].Text.Substring(0, lstlbl[rtxt.TabIndex - 1].Text.Length - 1) + "1";
                    rtxt.SelectionStart = 0;
                    rtxt.SelectionLength = rtxt.Text.Length;
                    rtxt.SelectionColor = Color.Red;
                    rtxt.Text = imgContent.Content1_OCR[rtxt.TabIndex];
                    numbertrtxt = rtxt.TabIndex;
                    string s1, s2;
                    s1 = imgContent.Content1_OCR[rtxt.TabIndex];
                    s2 = imgContent.Content3[rtxt.TabIndex];
                    c = null;
                    c = new int[s1.Length + 1, s2.Length + 1];
                    LCS(s1, s2);
                    BackTrack(s1, s2, s1.Length, s2.Length);
                }
            }
        }

        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }

        private void lbl8_Click(object sender, EventArgs e)
        {

        }

        private void timegetanh_Tick(object sender, EventArgs e)
        {
            if (ListImage.Count < 2)
            {
                int id2 = dACheck.GetIdCheck(batchnameLocal, formId, typeCheck);
                if (id2 > 0)
                {
                    BOImage_Check img2 = new BOImage_Check();
                    img2 = dACheck.GetImage(id2);
                    img2.Imagesource = new Bitmap(Io_Entry.byteArrayToImage(dACheck.getImageOnServer(img2.PageName, batchnameLocal)));
                    ListImage.Add(img2);
                }

            }
        }
    }
}

