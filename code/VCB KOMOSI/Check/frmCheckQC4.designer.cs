﻿namespace VCB_KOMOSI
{
    partial class frmCheckQC4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCheckQC4));
            this.btnRL = new System.Windows.Forms.Button();
            this.btnRR = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.txt8 = new System.Windows.Forms.RichTextBox();
            this.txt7 = new System.Windows.Forms.RichTextBox();
            this.txt6 = new System.Windows.Forms.RichTextBox();
            this.txt5 = new System.Windows.Forms.RichTextBox();
            this.txt4 = new System.Windows.Forms.RichTextBox();
            this.txt3 = new System.Windows.Forms.RichTextBox();
            this.txt2 = new System.Windows.Forms.RichTextBox();
            this.txt1 = new System.Windows.Forms.RichTextBox();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnout = new System.Windows.Forms.Button();
            this.btnin = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblEntry1 = new System.Windows.Forms.Label();
            this.lblEntry2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblfilename = new System.Windows.Forms.Label();
            this.lblImage = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblGroup2 = new System.Windows.Forms.Label();
            this.lblGroup1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblbatchname = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.bgwGetImage = new System.ComponentModel.BackgroundWorker();
            this.bgwGetImage1 = new System.ComponentModel.BackgroundWorker();
            this.lblGroup3 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblEntry3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.imageViewerTR1 = new ImageViewerTR.ImageViewerTR();
            this.panel2 = new System.Windows.Forms.Panel();
            this.clbl14 = new System.Windows.Forms.Label();
            this.clbl13 = new System.Windows.Forms.Label();
            this.clbl12 = new System.Windows.Forms.Label();
            this.ctxt13 = new System.Windows.Forms.TextBox();
            this.ctxt12 = new System.Windows.Forms.TextBox();
            this.ctxt14 = new System.Windows.Forms.TextBox();
            this.clbl7 = new System.Windows.Forms.Label();
            this.clbl6 = new System.Windows.Forms.Label();
            this.clbl4 = new System.Windows.Forms.Label();
            this.clbl5 = new System.Windows.Forms.Label();
            this.clbl3 = new System.Windows.Forms.Label();
            this.clbl2 = new System.Windows.Forms.Label();
            this.clbl1 = new System.Windows.Forms.Label();
            this.clbl29 = new System.Windows.Forms.Label();
            this.ctxt29 = new System.Windows.Forms.TextBox();
            this.clbl26 = new System.Windows.Forms.Label();
            this.clbl27 = new System.Windows.Forms.Label();
            this.clbl28 = new System.Windows.Forms.Label();
            this.ctxt26 = new System.Windows.Forms.TextBox();
            this.ctxt28 = new System.Windows.Forms.TextBox();
            this.ctxt27 = new System.Windows.Forms.TextBox();
            this.ctxt23 = new System.Windows.Forms.TextBox();
            this.clbl25 = new System.Windows.Forms.Label();
            this.clbl24 = new System.Windows.Forms.Label();
            this.ctxt22 = new System.Windows.Forms.TextBox();
            this.clbl22 = new System.Windows.Forms.Label();
            this.ctxt25 = new System.Windows.Forms.TextBox();
            this.clbl23 = new System.Windows.Forms.Label();
            this.ctxt24 = new System.Windows.Forms.TextBox();
            this.clbl19 = new System.Windows.Forms.Label();
            this.clbl20 = new System.Windows.Forms.Label();
            this.clbl21 = new System.Windows.Forms.Label();
            this.ctxt19 = new System.Windows.Forms.TextBox();
            this.ctxt21 = new System.Windows.Forms.TextBox();
            this.ctxt20 = new System.Windows.Forms.TextBox();
            this.ctxt16 = new System.Windows.Forms.TextBox();
            this.clbl18 = new System.Windows.Forms.Label();
            this.clbl17 = new System.Windows.Forms.Label();
            this.ctxt15 = new System.Windows.Forms.TextBox();
            this.clbl15 = new System.Windows.Forms.Label();
            this.ctxt18 = new System.Windows.Forms.TextBox();
            this.clbl16 = new System.Windows.Forms.Label();
            this.ctxt17 = new System.Windows.Forms.TextBox();
            this.ctxt9 = new System.Windows.Forms.TextBox();
            this.clbl11 = new System.Windows.Forms.Label();
            this.clbl10 = new System.Windows.Forms.Label();
            this.ctxt8 = new System.Windows.Forms.TextBox();
            this.clbl8 = new System.Windows.Forms.Label();
            this.ctxt11 = new System.Windows.Forms.TextBox();
            this.clbl9 = new System.Windows.Forms.Label();
            this.ctxt10 = new System.Windows.Forms.TextBox();
            this.ctxt3 = new System.Windows.Forms.TextBox();
            this.ctxt2 = new System.Windows.Forms.TextBox();
            this.ctxt4 = new System.Windows.Forms.TextBox();
            this.ctxt5 = new System.Windows.Forms.TextBox();
            this.ctxt6 = new System.Windows.Forms.TextBox();
            this.ctxt1 = new System.Windows.Forms.TextBox();
            this.ctxt7 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.imageViewerTR2 = new ImageViewerTR.ImageViewerTR();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRL
            // 
            this.btnRL.BackgroundImage = global::VCBHL_Entry.Properties.Resources.object_rotate_left;
            this.btnRL.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRL.Location = new System.Drawing.Point(85, 4);
            this.btnRL.Name = "btnRL";
            this.btnRL.Size = new System.Drawing.Size(29, 25);
            this.btnRL.TabIndex = 53;
            this.btnRL.UseVisualStyleBackColor = true;
            this.btnRL.Click += new System.EventHandler(this.btnRL_Click);
            // 
            // btnRR
            // 
            this.btnRR.BackgroundImage = global::VCBHL_Entry.Properties.Resources.object_rotate_right;
            this.btnRR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRR.Location = new System.Drawing.Point(50, 4);
            this.btnRR.Name = "btnRR";
            this.btnRR.Size = new System.Drawing.Size(29, 25);
            this.btnRR.TabIndex = 54;
            this.btnRR.UseVisualStyleBackColor = true;
            this.btnRR.Click += new System.EventHandler(this.btnRR_Click);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBack.BackgroundImage")));
            this.btnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBack.Location = new System.Drawing.Point(12, 668);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(91, 60);
            this.btnBack.TabIndex = 13;
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lbl8
            // 
            this.lbl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl8.AutoSize = true;
            this.lbl8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl8.ForeColor = System.Drawing.Color.Blue;
            this.lbl8.Location = new System.Drawing.Point(878, 706);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(17, 18);
            this.lbl8.TabIndex = 213;
            this.lbl8.Text = "8";
            // 
            // lbl7
            // 
            this.lbl7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl7.AutoSize = true;
            this.lbl7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl7.ForeColor = System.Drawing.Color.Blue;
            this.lbl7.Location = new System.Drawing.Point(878, 668);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(17, 18);
            this.lbl7.TabIndex = 212;
            this.lbl7.Text = "7";
            // 
            // lbl6
            // 
            this.lbl6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl6.AutoSize = true;
            this.lbl6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl6.ForeColor = System.Drawing.Color.Blue;
            this.lbl6.Location = new System.Drawing.Point(878, 627);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(17, 18);
            this.lbl6.TabIndex = 211;
            this.lbl6.Text = "6";
            // 
            // lbl5
            // 
            this.lbl5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl5.AutoSize = true;
            this.lbl5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5.ForeColor = System.Drawing.Color.Blue;
            this.lbl5.Location = new System.Drawing.Point(878, 587);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(17, 18);
            this.lbl5.TabIndex = 210;
            this.lbl5.Text = "5";
            // 
            // txt8
            // 
            this.txt8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt8.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt8.ForeColor = System.Drawing.Color.Red;
            this.txt8.Location = new System.Drawing.Point(897, 698);
            this.txt8.Name = "txt8";
            this.txt8.Size = new System.Drawing.Size(201, 38);
            this.txt8.TabIndex = 8;
            this.txt8.Text = "";
            // 
            // txt7
            // 
            this.txt7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt7.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt7.ForeColor = System.Drawing.Color.Red;
            this.txt7.Location = new System.Drawing.Point(897, 659);
            this.txt7.Name = "txt7";
            this.txt7.Size = new System.Drawing.Size(201, 38);
            this.txt7.TabIndex = 7;
            this.txt7.Text = "";
            // 
            // txt6
            // 
            this.txt6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt6.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt6.ForeColor = System.Drawing.Color.Red;
            this.txt6.Location = new System.Drawing.Point(897, 620);
            this.txt6.Name = "txt6";
            this.txt6.Size = new System.Drawing.Size(201, 38);
            this.txt6.TabIndex = 6;
            this.txt6.Text = "";
            // 
            // txt5
            // 
            this.txt5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt5.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt5.ForeColor = System.Drawing.Color.Red;
            this.txt5.Location = new System.Drawing.Point(897, 581);
            this.txt5.Name = "txt5";
            this.txt5.Size = new System.Drawing.Size(201, 38);
            this.txt5.TabIndex = 5;
            this.txt5.Text = "";
            // 
            // txt4
            // 
            this.txt4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt4.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt4.ForeColor = System.Drawing.Color.Red;
            this.txt4.Location = new System.Drawing.Point(642, 698);
            this.txt4.Name = "txt4";
            this.txt4.Size = new System.Drawing.Size(201, 38);
            this.txt4.TabIndex = 4;
            this.txt4.Text = "";
            // 
            // txt3
            // 
            this.txt3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt3.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt3.ForeColor = System.Drawing.Color.Red;
            this.txt3.Location = new System.Drawing.Point(642, 659);
            this.txt3.Name = "txt3";
            this.txt3.Size = new System.Drawing.Size(201, 38);
            this.txt3.TabIndex = 3;
            this.txt3.Text = "";
            // 
            // txt2
            // 
            this.txt2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt2.ForeColor = System.Drawing.Color.Red;
            this.txt2.Location = new System.Drawing.Point(642, 620);
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(201, 38);
            this.txt2.TabIndex = 2;
            this.txt2.Text = "";
            // 
            // txt1
            // 
            this.txt1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt1.ForeColor = System.Drawing.Color.Red;
            this.txt1.Location = new System.Drawing.Point(642, 581);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(201, 38);
            this.txt1.TabIndex = 1;
            this.txt1.Text = "";
            // 
            // lbl4
            // 
            this.lbl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl4.AutoSize = true;
            this.lbl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl4.ForeColor = System.Drawing.Color.Blue;
            this.lbl4.Location = new System.Drawing.Point(622, 707);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(17, 18);
            this.lbl4.TabIndex = 209;
            this.lbl4.Text = "4";
            // 
            // lbl3
            // 
            this.lbl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl3.AutoSize = true;
            this.lbl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl3.ForeColor = System.Drawing.Color.Blue;
            this.lbl3.Location = new System.Drawing.Point(622, 668);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(17, 18);
            this.lbl3.TabIndex = 208;
            this.lbl3.Text = "3";
            this.lbl3.Click += new System.EventHandler(this.lbl3_Click);
            // 
            // lbl2
            // 
            this.lbl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl2.AutoSize = true;
            this.lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.ForeColor = System.Drawing.Color.Blue;
            this.lbl2.Location = new System.Drawing.Point(622, 629);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(17, 18);
            this.lbl2.TabIndex = 207;
            this.lbl2.Text = "2";
            // 
            // lbl1
            // 
            this.lbl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.ForeColor = System.Drawing.Color.Blue;
            this.lbl1.Location = new System.Drawing.Point(622, 591);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(17, 18);
            this.lbl1.TabIndex = 206;
            this.lbl1.Text = "1";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSubmit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSubmit.BackgroundImage")));
            this.btnSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSubmit.Location = new System.Drawing.Point(1149, 640);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(183, 76);
            this.btnSubmit.TabIndex = 12;
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSumit2_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(15, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 25);
            this.button1.TabIndex = 59;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnout
            // 
            this.btnout.BackgroundImage = global::VCBHL_Entry.Properties.Resources.Zoom_Out_icon;
            this.btnout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnout.Location = new System.Drawing.Point(120, 4);
            this.btnout.Name = "btnout";
            this.btnout.Size = new System.Drawing.Size(29, 25);
            this.btnout.TabIndex = 60;
            this.btnout.UseVisualStyleBackColor = true;
            this.btnout.Click += new System.EventHandler(this.btnout_Click);
            // 
            // btnin
            // 
            this.btnin.BackgroundImage = global::VCBHL_Entry.Properties.Resources.Zoom_In_icon;
            this.btnin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnin.Location = new System.Drawing.Point(155, 4);
            this.btnin.Name = "btnin";
            this.btnin.Size = new System.Drawing.Size(29, 25);
            this.btnin.TabIndex = 61;
            this.btnin.UseVisualStyleBackColor = true;
            this.btnin.Click += new System.EventHandler(this.btnin_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(561, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 62;
            this.label2.Text = "User1:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(561, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 16);
            this.label6.TabIndex = 63;
            this.label6.Text = "User2:";
            // 
            // lblEntry1
            // 
            this.lblEntry1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEntry1.AutoSize = true;
            this.lblEntry1.Location = new System.Drawing.Point(600, 4);
            this.lblEntry1.Name = "lblEntry1";
            this.lblEntry1.Size = new System.Drawing.Size(21, 16);
            this.lblEntry1.TabIndex = 64;
            this.lblEntry1.Text = "E1";
            // 
            // lblEntry2
            // 
            this.lblEntry2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEntry2.AutoSize = true;
            this.lblEntry2.Location = new System.Drawing.Point(600, 27);
            this.lblEntry2.Name = "lblEntry2";
            this.lblEntry2.Size = new System.Drawing.Size(21, 16);
            this.lblEntry2.TabIndex = 65;
            this.lblEntry2.Text = "E2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 16);
            this.label10.TabIndex = 66;
            this.label10.Text = "Image Name:";
            // 
            // lblfilename
            // 
            this.lblfilename.AutoSize = true;
            this.lblfilename.ForeColor = System.Drawing.Color.Blue;
            this.lblfilename.Location = new System.Drawing.Point(90, 32);
            this.lblfilename.Name = "lblfilename";
            this.lblfilename.Size = new System.Drawing.Size(49, 16);
            this.lblfilename.TabIndex = 67;
            this.lblfilename.Text = "0001.jpg";
            // 
            // lblImage
            // 
            this.lblImage.AutoSize = true;
            this.lblImage.ForeColor = System.Drawing.Color.Blue;
            this.lblImage.Location = new System.Drawing.Point(511, 4);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(32, 16);
            this.lblImage.TabIndex = 94;
            this.lblImage.Text = "1211";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(466, 4);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 16);
            this.label13.TabIndex = 93;
            this.label13.Text = "Count:";
            // 
            // lblGroup2
            // 
            this.lblGroup2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGroup2.AutoSize = true;
            this.lblGroup2.Location = new System.Drawing.Point(834, 28);
            this.lblGroup2.Name = "lblGroup2";
            this.lblGroup2.Size = new System.Drawing.Size(28, 16);
            this.lblGroup2.TabIndex = 98;
            this.lblGroup2.Text = "TT2";
            // 
            // lblGroup1
            // 
            this.lblGroup1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGroup1.AutoSize = true;
            this.lblGroup1.Location = new System.Drawing.Point(834, 5);
            this.lblGroup1.Name = "lblGroup1";
            this.lblGroup1.Size = new System.Drawing.Size(28, 16);
            this.lblGroup1.TabIndex = 97;
            this.lblGroup1.Text = "TT1";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(778, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 16);
            this.label4.TabIndex = 96;
            this.label4.Text = "Trung tâm:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(777, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 16);
            this.label5.TabIndex = 95;
            this.label5.Text = "Trung tâm:";
            // 
            // lblbatchname
            // 
            this.lblbatchname.AutoSize = true;
            this.lblbatchname.ForeColor = System.Drawing.Color.Blue;
            this.lblbatchname.Location = new System.Drawing.Point(379, 32);
            this.lblbatchname.Name = "lblbatchname";
            this.lblbatchname.Size = new System.Drawing.Size(49, 16);
            this.lblbatchname.TabIndex = 137;
            this.lblbatchname.Text = "0001.jpg";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(302, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 16);
            this.label8.TabIndex = 136;
            this.label8.Text = "Batch Name:";
            // 
            // bgwGetImage
            // 
            this.bgwGetImage.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwGetImage_DoWork);
            this.bgwGetImage.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwGetImage_RunWorkerCompleted);
            // 
            // bgwGetImage1
            // 
            this.bgwGetImage1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwGetImage1_DoWork);
            this.bgwGetImage1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwGetImage_RunWorkerCompleted);
            // 
            // lblGroup3
            // 
            this.lblGroup3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGroup3.AutoSize = true;
            this.lblGroup3.Location = new System.Drawing.Point(1235, 10);
            this.lblGroup3.Name = "lblGroup3";
            this.lblGroup3.Size = new System.Drawing.Size(28, 16);
            this.lblGroup3.TabIndex = 141;
            this.lblGroup3.Text = "TT3";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1179, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 16);
            this.label3.TabIndex = 140;
            this.label3.Text = "Trung tâm:";
            // 
            // lblEntry3
            // 
            this.lblEntry3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEntry3.AutoSize = true;
            this.lblEntry3.Location = new System.Drawing.Point(970, 9);
            this.lblEntry3.Name = "lblEntry3";
            this.lblEntry3.Size = new System.Drawing.Size(21, 16);
            this.lblEntry3.TabIndex = 139;
            this.lblEntry3.Text = "E3";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(929, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 16);
            this.label9.TabIndex = 138;
            this.label9.Text = "User3:";
            // 
            // imageViewerTR1
            // 
            this.imageViewerTR1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.imageViewerTR1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imageViewerTR1.CurrentZoom = 1F;
            this.imageViewerTR1.Image = null;
            this.imageViewerTR1.Location = new System.Drawing.Point(28, 51);
            this.imageViewerTR1.Margin = new System.Windows.Forms.Padding(3, 9, 3, 9);
            this.imageViewerTR1.MaxZoom = 20F;
            this.imageViewerTR1.MinZoom = 0.05F;
            this.imageViewerTR1.Name = "imageViewerTR1";
            this.imageViewerTR1.Size = new System.Drawing.Size(549, 398);
            this.imageViewerTR1.TabIndex = 214;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.clbl14);
            this.panel2.Controls.Add(this.clbl13);
            this.panel2.Controls.Add(this.clbl12);
            this.panel2.Controls.Add(this.ctxt13);
            this.panel2.Controls.Add(this.ctxt12);
            this.panel2.Controls.Add(this.ctxt14);
            this.panel2.Controls.Add(this.clbl7);
            this.panel2.Controls.Add(this.clbl6);
            this.panel2.Controls.Add(this.clbl4);
            this.panel2.Controls.Add(this.clbl5);
            this.panel2.Controls.Add(this.clbl3);
            this.panel2.Controls.Add(this.clbl2);
            this.panel2.Controls.Add(this.clbl1);
            this.panel2.Controls.Add(this.clbl29);
            this.panel2.Controls.Add(this.ctxt29);
            this.panel2.Controls.Add(this.clbl26);
            this.panel2.Controls.Add(this.clbl27);
            this.panel2.Controls.Add(this.clbl28);
            this.panel2.Controls.Add(this.ctxt26);
            this.panel2.Controls.Add(this.ctxt28);
            this.panel2.Controls.Add(this.ctxt27);
            this.panel2.Controls.Add(this.ctxt23);
            this.panel2.Controls.Add(this.clbl25);
            this.panel2.Controls.Add(this.clbl24);
            this.panel2.Controls.Add(this.ctxt22);
            this.panel2.Controls.Add(this.clbl22);
            this.panel2.Controls.Add(this.ctxt25);
            this.panel2.Controls.Add(this.clbl23);
            this.panel2.Controls.Add(this.ctxt24);
            this.panel2.Controls.Add(this.clbl19);
            this.panel2.Controls.Add(this.clbl20);
            this.panel2.Controls.Add(this.clbl21);
            this.panel2.Controls.Add(this.ctxt19);
            this.panel2.Controls.Add(this.ctxt21);
            this.panel2.Controls.Add(this.ctxt20);
            this.panel2.Controls.Add(this.ctxt16);
            this.panel2.Controls.Add(this.clbl18);
            this.panel2.Controls.Add(this.clbl17);
            this.panel2.Controls.Add(this.ctxt15);
            this.panel2.Controls.Add(this.clbl15);
            this.panel2.Controls.Add(this.ctxt18);
            this.panel2.Controls.Add(this.clbl16);
            this.panel2.Controls.Add(this.ctxt17);
            this.panel2.Controls.Add(this.ctxt9);
            this.panel2.Controls.Add(this.clbl11);
            this.panel2.Controls.Add(this.clbl10);
            this.panel2.Controls.Add(this.ctxt8);
            this.panel2.Controls.Add(this.clbl8);
            this.panel2.Controls.Add(this.ctxt11);
            this.panel2.Controls.Add(this.clbl9);
            this.panel2.Controls.Add(this.ctxt10);
            this.panel2.Controls.Add(this.ctxt3);
            this.panel2.Controls.Add(this.ctxt2);
            this.panel2.Controls.Add(this.ctxt4);
            this.panel2.Controls.Add(this.ctxt5);
            this.panel2.Controls.Add(this.ctxt6);
            this.panel2.Controls.Add(this.ctxt1);
            this.panel2.Controls.Add(this.ctxt7);
            this.panel2.Location = new System.Drawing.Point(104, 468);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(388, 290);
            this.panel2.TabIndex = 215;
            // 
            // clbl14
            // 
            this.clbl14.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl14.AutoSize = true;
            this.clbl14.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl14.ForeColor = System.Drawing.Color.Blue;
            this.clbl14.Location = new System.Drawing.Point(10, 261);
            this.clbl14.Name = "clbl14";
            this.clbl14.Size = new System.Drawing.Size(19, 20);
            this.clbl14.TabIndex = 205;
            this.clbl14.Text = "7";
            // 
            // clbl13
            // 
            this.clbl13.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl13.AutoSize = true;
            this.clbl13.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl13.ForeColor = System.Drawing.Color.Blue;
            this.clbl13.Location = new System.Drawing.Point(11, 233);
            this.clbl13.Name = "clbl13";
            this.clbl13.Size = new System.Drawing.Size(19, 20);
            this.clbl13.TabIndex = 204;
            this.clbl13.Text = "6";
            // 
            // clbl12
            // 
            this.clbl12.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl12.AutoSize = true;
            this.clbl12.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl12.ForeColor = System.Drawing.Color.Blue;
            this.clbl12.Location = new System.Drawing.Point(10, 204);
            this.clbl12.Name = "clbl12";
            this.clbl12.Size = new System.Drawing.Size(19, 20);
            this.clbl12.TabIndex = 203;
            this.clbl12.Text = "5";
            // 
            // ctxt13
            // 
            this.ctxt13.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt13.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ctxt13.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ctxt13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt13.Location = new System.Drawing.Point(44, 230);
            this.ctxt13.MaxLength = 3;
            this.ctxt13.Name = "ctxt13";
            this.ctxt13.ReadOnly = true;
            this.ctxt13.ShortcutsEnabled = false;
            this.ctxt13.Size = new System.Drawing.Size(52, 26);
            this.ctxt13.TabIndex = 202;
            this.ctxt13.TabStop = false;
            this.ctxt13.Text = "201";
            this.ctxt13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt12
            // 
            this.ctxt12.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt12.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ctxt12.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ctxt12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt12.Location = new System.Drawing.Point(44, 201);
            this.ctxt12.MaxLength = 2;
            this.ctxt12.Name = "ctxt12";
            this.ctxt12.ReadOnly = true;
            this.ctxt12.ShortcutsEnabled = false;
            this.ctxt12.Size = new System.Drawing.Size(52, 26);
            this.ctxt12.TabIndex = 201;
            this.ctxt12.TabStop = false;
            this.ctxt12.Text = "202";
            this.ctxt12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt14
            // 
            this.ctxt14.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt14.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ctxt14.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ctxt14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt14.Location = new System.Drawing.Point(44, 258);
            this.ctxt14.MaxLength = 3;
            this.ctxt14.Name = "ctxt14";
            this.ctxt14.ReadOnly = true;
            this.ctxt14.ShortcutsEnabled = false;
            this.ctxt14.Size = new System.Drawing.Size(52, 26);
            this.ctxt14.TabIndex = 200;
            this.ctxt14.TabStop = false;
            this.ctxt14.Text = "301";
            this.ctxt14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl7
            // 
            this.clbl7.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl7.AutoSize = true;
            this.clbl7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl7.ForeColor = System.Drawing.Color.Blue;
            this.clbl7.Location = new System.Drawing.Point(338, 1);
            this.clbl7.Name = "clbl7";
            this.clbl7.Size = new System.Drawing.Size(19, 20);
            this.clbl7.TabIndex = 199;
            this.clbl7.Text = "7";
            // 
            // clbl6
            // 
            this.clbl6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl6.AutoSize = true;
            this.clbl6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl6.ForeColor = System.Drawing.Color.Blue;
            this.clbl6.Location = new System.Drawing.Point(295, 1);
            this.clbl6.Name = "clbl6";
            this.clbl6.Size = new System.Drawing.Size(19, 20);
            this.clbl6.TabIndex = 198;
            this.clbl6.Text = "6";
            // 
            // clbl4
            // 
            this.clbl4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl4.AutoSize = true;
            this.clbl4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl4.ForeColor = System.Drawing.Color.Blue;
            this.clbl4.Location = new System.Drawing.Point(204, 0);
            this.clbl4.Name = "clbl4";
            this.clbl4.Size = new System.Drawing.Size(19, 20);
            this.clbl4.TabIndex = 129;
            this.clbl4.Text = "4";
            // 
            // clbl5
            // 
            this.clbl5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl5.AutoSize = true;
            this.clbl5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl5.ForeColor = System.Drawing.Color.Blue;
            this.clbl5.Location = new System.Drawing.Point(251, 1);
            this.clbl5.Name = "clbl5";
            this.clbl5.Size = new System.Drawing.Size(19, 20);
            this.clbl5.TabIndex = 197;
            this.clbl5.Text = "5";
            // 
            // clbl3
            // 
            this.clbl3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl3.AutoSize = true;
            this.clbl3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl3.ForeColor = System.Drawing.Color.Blue;
            this.clbl3.Location = new System.Drawing.Point(160, 0);
            this.clbl3.Name = "clbl3";
            this.clbl3.Size = new System.Drawing.Size(19, 20);
            this.clbl3.TabIndex = 128;
            this.clbl3.Text = "3";
            // 
            // clbl2
            // 
            this.clbl2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl2.AutoSize = true;
            this.clbl2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl2.ForeColor = System.Drawing.Color.Blue;
            this.clbl2.Location = new System.Drawing.Point(65, 1);
            this.clbl2.Name = "clbl2";
            this.clbl2.Size = new System.Drawing.Size(19, 20);
            this.clbl2.TabIndex = 127;
            this.clbl2.Text = "2";
            // 
            // clbl1
            // 
            this.clbl1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl1.AutoSize = true;
            this.clbl1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl1.ForeColor = System.Drawing.Color.Blue;
            this.clbl1.Location = new System.Drawing.Point(21, 1);
            this.clbl1.Name = "clbl1";
            this.clbl1.Size = new System.Drawing.Size(19, 20);
            this.clbl1.TabIndex = 126;
            this.clbl1.Text = "1";
            // 
            // clbl29
            // 
            this.clbl29.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl29.AutoSize = true;
            this.clbl29.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl29.ForeColor = System.Drawing.Color.Blue;
            this.clbl29.Location = new System.Drawing.Point(272, 261);
            this.clbl29.Name = "clbl29";
            this.clbl29.Size = new System.Drawing.Size(19, 20);
            this.clbl29.TabIndex = 125;
            this.clbl29.Text = "8";
            // 
            // ctxt29
            // 
            this.ctxt29.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt29.Location = new System.Drawing.Point(305, 258);
            this.ctxt29.MaxLength = 3;
            this.ctxt29.Name = "ctxt29";
            this.ctxt29.ReadOnly = true;
            this.ctxt29.ShortcutsEnabled = false;
            this.ctxt29.Size = new System.Drawing.Size(52, 26);
            this.ctxt29.TabIndex = 124;
            this.ctxt29.TabStop = false;
            this.ctxt29.Text = "543";
            this.ctxt29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl26
            // 
            this.clbl26.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl26.AutoSize = true;
            this.clbl26.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl26.ForeColor = System.Drawing.Color.Blue;
            this.clbl26.Location = new System.Drawing.Point(272, 177);
            this.clbl26.Name = "clbl26";
            this.clbl26.Size = new System.Drawing.Size(19, 20);
            this.clbl26.TabIndex = 123;
            this.clbl26.Text = "5";
            // 
            // clbl27
            // 
            this.clbl27.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl27.AutoSize = true;
            this.clbl27.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl27.ForeColor = System.Drawing.Color.Blue;
            this.clbl27.Location = new System.Drawing.Point(272, 204);
            this.clbl27.Name = "clbl27";
            this.clbl27.Size = new System.Drawing.Size(19, 20);
            this.clbl27.TabIndex = 122;
            this.clbl27.Text = "6";
            // 
            // clbl28
            // 
            this.clbl28.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl28.AutoSize = true;
            this.clbl28.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl28.ForeColor = System.Drawing.Color.Blue;
            this.clbl28.Location = new System.Drawing.Point(272, 234);
            this.clbl28.Name = "clbl28";
            this.clbl28.Size = new System.Drawing.Size(19, 20);
            this.clbl28.TabIndex = 121;
            this.clbl28.Text = "7";
            // 
            // ctxt26
            // 
            this.ctxt26.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt26.Location = new System.Drawing.Point(305, 173);
            this.ctxt26.MaxLength = 3;
            this.ctxt26.Name = "ctxt26";
            this.ctxt26.ReadOnly = true;
            this.ctxt26.ShortcutsEnabled = false;
            this.ctxt26.Size = new System.Drawing.Size(52, 26);
            this.ctxt26.TabIndex = 119;
            this.ctxt26.TabStop = false;
            this.ctxt26.Text = "513";
            this.ctxt26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt28
            // 
            this.ctxt28.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt28.Location = new System.Drawing.Point(305, 230);
            this.ctxt28.MaxLength = 3;
            this.ctxt28.Name = "ctxt28";
            this.ctxt28.ReadOnly = true;
            this.ctxt28.ShortcutsEnabled = false;
            this.ctxt28.Size = new System.Drawing.Size(52, 26);
            this.ctxt28.TabIndex = 120;
            this.ctxt28.TabStop = false;
            this.ctxt28.Text = "533";
            this.ctxt28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt27
            // 
            this.ctxt27.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt27.Location = new System.Drawing.Point(305, 201);
            this.ctxt27.MaxLength = 3;
            this.ctxt27.Name = "ctxt27";
            this.ctxt27.ReadOnly = true;
            this.ctxt27.ShortcutsEnabled = false;
            this.ctxt27.Size = new System.Drawing.Size(52, 26);
            this.ctxt27.TabIndex = 118;
            this.ctxt27.TabStop = false;
            this.ctxt27.Text = "523";
            this.ctxt27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt23
            // 
            this.ctxt23.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt23.Location = new System.Drawing.Point(304, 89);
            this.ctxt23.MaxLength = 3;
            this.ctxt23.Name = "ctxt23";
            this.ctxt23.ReadOnly = true;
            this.ctxt23.ShortcutsEnabled = false;
            this.ctxt23.Size = new System.Drawing.Size(52, 26);
            this.ctxt23.TabIndex = 112;
            this.ctxt23.TabStop = false;
            this.ctxt23.Text = "452";
            this.ctxt23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl25
            // 
            this.clbl25.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl25.AutoSize = true;
            this.clbl25.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl25.ForeColor = System.Drawing.Color.Blue;
            this.clbl25.Location = new System.Drawing.Point(272, 148);
            this.clbl25.Name = "clbl25";
            this.clbl25.Size = new System.Drawing.Size(19, 20);
            this.clbl25.TabIndex = 117;
            this.clbl25.Text = "4";
            // 
            // clbl24
            // 
            this.clbl24.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl24.AutoSize = true;
            this.clbl24.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl24.ForeColor = System.Drawing.Color.Blue;
            this.clbl24.Location = new System.Drawing.Point(272, 120);
            this.clbl24.Name = "clbl24";
            this.clbl24.Size = new System.Drawing.Size(19, 20);
            this.clbl24.TabIndex = 116;
            this.clbl24.Text = "3";
            // 
            // ctxt22
            // 
            this.ctxt22.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt22.Location = new System.Drawing.Point(304, 61);
            this.ctxt22.MaxLength = 2;
            this.ctxt22.Name = "ctxt22";
            this.ctxt22.ReadOnly = true;
            this.ctxt22.ShortcutsEnabled = false;
            this.ctxt22.Size = new System.Drawing.Size(52, 26);
            this.ctxt22.TabIndex = 111;
            this.ctxt22.TabStop = false;
            this.ctxt22.Text = "451";
            this.ctxt22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl22
            // 
            this.clbl22.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl22.AutoSize = true;
            this.clbl22.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl22.ForeColor = System.Drawing.Color.Blue;
            this.clbl22.Location = new System.Drawing.Point(270, 63);
            this.clbl22.Name = "clbl22";
            this.clbl22.Size = new System.Drawing.Size(19, 20);
            this.clbl22.TabIndex = 114;
            this.clbl22.Text = "1";
            // 
            // ctxt25
            // 
            this.ctxt25.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt25.Location = new System.Drawing.Point(304, 145);
            this.ctxt25.MaxLength = 3;
            this.ctxt25.Name = "ctxt25";
            this.ctxt25.ReadOnly = true;
            this.ctxt25.ShortcutsEnabled = false;
            this.ctxt25.Size = new System.Drawing.Size(52, 26);
            this.ctxt25.TabIndex = 113;
            this.ctxt25.TabStop = false;
            this.ctxt25.Text = "454";
            this.ctxt25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl23
            // 
            this.clbl23.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl23.AutoSize = true;
            this.clbl23.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl23.ForeColor = System.Drawing.Color.Blue;
            this.clbl23.Location = new System.Drawing.Point(271, 91);
            this.clbl23.Name = "clbl23";
            this.clbl23.Size = new System.Drawing.Size(19, 20);
            this.clbl23.TabIndex = 115;
            this.clbl23.Text = "2";
            // 
            // ctxt24
            // 
            this.ctxt24.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt24.Location = new System.Drawing.Point(304, 117);
            this.ctxt24.MaxLength = 3;
            this.ctxt24.Name = "ctxt24";
            this.ctxt24.ReadOnly = true;
            this.ctxt24.ShortcutsEnabled = false;
            this.ctxt24.Size = new System.Drawing.Size(52, 26);
            this.ctxt24.TabIndex = 110;
            this.ctxt24.TabStop = false;
            this.ctxt24.Text = "453";
            this.ctxt24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl19
            // 
            this.clbl19.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl19.AutoSize = true;
            this.clbl19.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl19.ForeColor = System.Drawing.Color.Blue;
            this.clbl19.Location = new System.Drawing.Point(148, 204);
            this.clbl19.Name = "clbl19";
            this.clbl19.Size = new System.Drawing.Size(19, 20);
            this.clbl19.TabIndex = 109;
            this.clbl19.Text = "5";
            // 
            // clbl20
            // 
            this.clbl20.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl20.AutoSize = true;
            this.clbl20.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl20.ForeColor = System.Drawing.Color.Blue;
            this.clbl20.Location = new System.Drawing.Point(149, 233);
            this.clbl20.Name = "clbl20";
            this.clbl20.Size = new System.Drawing.Size(19, 20);
            this.clbl20.TabIndex = 108;
            this.clbl20.Text = "6";
            // 
            // clbl21
            // 
            this.clbl21.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl21.AutoSize = true;
            this.clbl21.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl21.ForeColor = System.Drawing.Color.Blue;
            this.clbl21.Location = new System.Drawing.Point(147, 261);
            this.clbl21.Name = "clbl21";
            this.clbl21.Size = new System.Drawing.Size(19, 20);
            this.clbl21.TabIndex = 107;
            this.clbl21.Text = "7";
            // 
            // ctxt19
            // 
            this.ctxt19.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt19.Location = new System.Drawing.Point(183, 201);
            this.ctxt19.MaxLength = 3;
            this.ctxt19.Name = "ctxt19";
            this.ctxt19.ReadOnly = true;
            this.ctxt19.ShortcutsEnabled = false;
            this.ctxt19.Size = new System.Drawing.Size(52, 26);
            this.ctxt19.TabIndex = 105;
            this.ctxt19.TabStop = false;
            this.ctxt19.Text = "522";
            this.ctxt19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt21
            // 
            this.ctxt21.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt21.Location = new System.Drawing.Point(183, 258);
            this.ctxt21.MaxLength = 3;
            this.ctxt21.Name = "ctxt21";
            this.ctxt21.ReadOnly = true;
            this.ctxt21.ShortcutsEnabled = false;
            this.ctxt21.Size = new System.Drawing.Size(52, 26);
            this.ctxt21.TabIndex = 106;
            this.ctxt21.TabStop = false;
            this.ctxt21.Text = "542";
            this.ctxt21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt20
            // 
            this.ctxt20.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt20.Location = new System.Drawing.Point(183, 230);
            this.ctxt20.MaxLength = 3;
            this.ctxt20.Name = "ctxt20";
            this.ctxt20.ReadOnly = true;
            this.ctxt20.ShortcutsEnabled = false;
            this.ctxt20.Size = new System.Drawing.Size(52, 26);
            this.ctxt20.TabIndex = 104;
            this.ctxt20.TabStop = false;
            this.ctxt20.Text = "532";
            this.ctxt20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt16
            // 
            this.ctxt16.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt16.Location = new System.Drawing.Point(183, 89);
            this.ctxt16.MaxLength = 3;
            this.ctxt16.Name = "ctxt16";
            this.ctxt16.ReadOnly = true;
            this.ctxt16.ShortcutsEnabled = false;
            this.ctxt16.Size = new System.Drawing.Size(52, 26);
            this.ctxt16.TabIndex = 98;
            this.ctxt16.TabStop = false;
            this.ctxt16.Text = "422";
            this.ctxt16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl18
            // 
            this.clbl18.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl18.AutoSize = true;
            this.clbl18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl18.ForeColor = System.Drawing.Color.Blue;
            this.clbl18.Location = new System.Drawing.Point(149, 176);
            this.clbl18.Name = "clbl18";
            this.clbl18.Size = new System.Drawing.Size(19, 20);
            this.clbl18.TabIndex = 103;
            this.clbl18.Text = "4";
            // 
            // clbl17
            // 
            this.clbl17.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl17.AutoSize = true;
            this.clbl17.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl17.ForeColor = System.Drawing.Color.Blue;
            this.clbl17.Location = new System.Drawing.Point(148, 125);
            this.clbl17.Name = "clbl17";
            this.clbl17.Size = new System.Drawing.Size(19, 20);
            this.clbl17.TabIndex = 102;
            this.clbl17.Text = "3";
            // 
            // ctxt15
            // 
            this.ctxt15.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt15.Location = new System.Drawing.Point(183, 61);
            this.ctxt15.MaxLength = 2;
            this.ctxt15.Name = "ctxt15";
            this.ctxt15.ReadOnly = true;
            this.ctxt15.ShortcutsEnabled = false;
            this.ctxt15.Size = new System.Drawing.Size(52, 26);
            this.ctxt15.TabIndex = 97;
            this.ctxt15.TabStop = false;
            this.ctxt15.Text = "412";
            this.ctxt15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl15
            // 
            this.clbl15.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl15.AutoSize = true;
            this.clbl15.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl15.ForeColor = System.Drawing.Color.Blue;
            this.clbl15.Location = new System.Drawing.Point(149, 63);
            this.clbl15.Name = "clbl15";
            this.clbl15.Size = new System.Drawing.Size(19, 20);
            this.clbl15.TabIndex = 100;
            this.clbl15.Text = "1";
            // 
            // ctxt18
            // 
            this.ctxt18.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt18.Location = new System.Drawing.Point(183, 173);
            this.ctxt18.MaxLength = 3;
            this.ctxt18.Name = "ctxt18";
            this.ctxt18.ReadOnly = true;
            this.ctxt18.ShortcutsEnabled = false;
            this.ctxt18.Size = new System.Drawing.Size(52, 26);
            this.ctxt18.TabIndex = 99;
            this.ctxt18.TabStop = false;
            this.ctxt18.Text = "512";
            this.ctxt18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl16
            // 
            this.clbl16.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl16.AutoSize = true;
            this.clbl16.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl16.ForeColor = System.Drawing.Color.Blue;
            this.clbl16.Location = new System.Drawing.Point(149, 93);
            this.clbl16.Name = "clbl16";
            this.clbl16.Size = new System.Drawing.Size(19, 20);
            this.clbl16.TabIndex = 101;
            this.clbl16.Text = "2";
            // 
            // ctxt17
            // 
            this.ctxt17.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt17.Location = new System.Drawing.Point(183, 117);
            this.ctxt17.MaxLength = 3;
            this.ctxt17.Name = "ctxt17";
            this.ctxt17.ReadOnly = true;
            this.ctxt17.ShortcutsEnabled = false;
            this.ctxt17.Size = new System.Drawing.Size(52, 26);
            this.ctxt17.TabIndex = 96;
            this.ctxt17.TabStop = false;
            this.ctxt17.Text = "432";
            this.ctxt17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt9
            // 
            this.ctxt9.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt9.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ctxt9.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ctxt9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt9.Location = new System.Drawing.Point(45, 89);
            this.ctxt9.MaxLength = 3;
            this.ctxt9.Name = "ctxt9";
            this.ctxt9.ReadOnly = true;
            this.ctxt9.ShortcutsEnabled = false;
            this.ctxt9.Size = new System.Drawing.Size(52, 26);
            this.ctxt9.TabIndex = 90;
            this.ctxt9.TabStop = false;
            this.ctxt9.Text = "102";
            this.ctxt9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl11
            // 
            this.clbl11.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl11.AutoSize = true;
            this.clbl11.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl11.ForeColor = System.Drawing.Color.Blue;
            this.clbl11.Location = new System.Drawing.Point(11, 176);
            this.clbl11.Name = "clbl11";
            this.clbl11.Size = new System.Drawing.Size(19, 20);
            this.clbl11.TabIndex = 95;
            this.clbl11.Text = "4";
            // 
            // clbl10
            // 
            this.clbl10.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl10.AutoSize = true;
            this.clbl10.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl10.ForeColor = System.Drawing.Color.Blue;
            this.clbl10.Location = new System.Drawing.Point(11, 120);
            this.clbl10.Name = "clbl10";
            this.clbl10.Size = new System.Drawing.Size(19, 20);
            this.clbl10.TabIndex = 94;
            this.clbl10.Text = "3";
            // 
            // ctxt8
            // 
            this.ctxt8.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt8.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ctxt8.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ctxt8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt8.Location = new System.Drawing.Point(45, 61);
            this.ctxt8.MaxLength = 2;
            this.ctxt8.Name = "ctxt8";
            this.ctxt8.ReadOnly = true;
            this.ctxt8.ShortcutsEnabled = false;
            this.ctxt8.Size = new System.Drawing.Size(52, 26);
            this.ctxt8.TabIndex = 89;
            this.ctxt8.TabStop = false;
            this.ctxt8.Text = "101";
            this.ctxt8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl8
            // 
            this.clbl8.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl8.AutoSize = true;
            this.clbl8.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl8.ForeColor = System.Drawing.Color.Blue;
            this.clbl8.Location = new System.Drawing.Point(12, 64);
            this.clbl8.Name = "clbl8";
            this.clbl8.Size = new System.Drawing.Size(19, 20);
            this.clbl8.TabIndex = 92;
            this.clbl8.Text = "1";
            // 
            // ctxt11
            // 
            this.ctxt11.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt11.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ctxt11.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ctxt11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt11.Location = new System.Drawing.Point(45, 174);
            this.ctxt11.MaxLength = 3;
            this.ctxt11.Name = "ctxt11";
            this.ctxt11.ReadOnly = true;
            this.ctxt11.ShortcutsEnabled = false;
            this.ctxt11.Size = new System.Drawing.Size(52, 26);
            this.ctxt11.TabIndex = 91;
            this.ctxt11.TabStop = false;
            this.ctxt11.Text = "203";
            this.ctxt11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // clbl9
            // 
            this.clbl9.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.clbl9.AutoSize = true;
            this.clbl9.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clbl9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbl9.ForeColor = System.Drawing.Color.Blue;
            this.clbl9.Location = new System.Drawing.Point(12, 91);
            this.clbl9.Name = "clbl9";
            this.clbl9.Size = new System.Drawing.Size(19, 20);
            this.clbl9.TabIndex = 93;
            this.clbl9.Text = "2";
            // 
            // ctxt10
            // 
            this.ctxt10.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt10.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ctxt10.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ctxt10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt10.Location = new System.Drawing.Point(45, 117);
            this.ctxt10.MaxLength = 3;
            this.ctxt10.Name = "ctxt10";
            this.ctxt10.ReadOnly = true;
            this.ctxt10.ShortcutsEnabled = false;
            this.ctxt10.Size = new System.Drawing.Size(52, 26);
            this.ctxt10.TabIndex = 88;
            this.ctxt10.TabStop = false;
            this.ctxt10.Text = "103";
            this.ctxt10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt3
            // 
            this.ctxt3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt3.Location = new System.Drawing.Point(151, 22);
            this.ctxt3.MaxLength = 3;
            this.ctxt3.Name = "ctxt3";
            this.ctxt3.ReadOnly = true;
            this.ctxt3.ShortcutsEnabled = false;
            this.ctxt3.Size = new System.Drawing.Size(39, 35);
            this.ctxt3.TabIndex = 3;
            this.ctxt3.TabStop = false;
            this.ctxt3.Text = "3";
            this.ctxt3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt2
            // 
            this.ctxt2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt2.Location = new System.Drawing.Point(58, 22);
            this.ctxt2.MaxLength = 2;
            this.ctxt2.Name = "ctxt2";
            this.ctxt2.ReadOnly = true;
            this.ctxt2.ShortcutsEnabled = false;
            this.ctxt2.Size = new System.Drawing.Size(39, 35);
            this.ctxt2.TabIndex = 2;
            this.ctxt2.TabStop = false;
            this.ctxt2.Text = "2";
            this.ctxt2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt4
            // 
            this.ctxt4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt4.Location = new System.Drawing.Point(196, 22);
            this.ctxt4.MaxLength = 3;
            this.ctxt4.Name = "ctxt4";
            this.ctxt4.ReadOnly = true;
            this.ctxt4.ShortcutsEnabled = false;
            this.ctxt4.Size = new System.Drawing.Size(39, 35);
            this.ctxt4.TabIndex = 4;
            this.ctxt4.TabStop = false;
            this.ctxt4.Text = "4";
            this.ctxt4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt5
            // 
            this.ctxt5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt5.Location = new System.Drawing.Point(241, 22);
            this.ctxt5.MaxLength = 2;
            this.ctxt5.Name = "ctxt5";
            this.ctxt5.ReadOnly = true;
            this.ctxt5.ShortcutsEnabled = false;
            this.ctxt5.Size = new System.Drawing.Size(39, 35);
            this.ctxt5.TabIndex = 5;
            this.ctxt5.TabStop = false;
            this.ctxt5.Text = "5";
            this.ctxt5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt6
            // 
            this.ctxt6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt6.Location = new System.Drawing.Point(286, 22);
            this.ctxt6.MaxLength = 2;
            this.ctxt6.Name = "ctxt6";
            this.ctxt6.ReadOnly = true;
            this.ctxt6.ShortcutsEnabled = false;
            this.ctxt6.Size = new System.Drawing.Size(39, 35);
            this.ctxt6.TabIndex = 6;
            this.ctxt6.TabStop = false;
            this.ctxt6.Text = "6";
            this.ctxt6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt1
            // 
            this.ctxt1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt1.Location = new System.Drawing.Point(13, 22);
            this.ctxt1.MaxLength = 3;
            this.ctxt1.Name = "ctxt1";
            this.ctxt1.ReadOnly = true;
            this.ctxt1.ShortcutsEnabled = false;
            this.ctxt1.Size = new System.Drawing.Size(39, 35);
            this.ctxt1.TabIndex = 1;
            this.ctxt1.TabStop = false;
            this.ctxt1.Text = "1";
            this.ctxt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ctxt7
            // 
            this.ctxt7.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ctxt7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctxt7.Location = new System.Drawing.Point(331, 22);
            this.ctxt7.MaxLength = 3;
            this.ctxt7.Name = "ctxt7";
            this.ctxt7.ReadOnly = true;
            this.ctxt7.ShortcutsEnabled = false;
            this.ctxt7.Size = new System.Drawing.Size(39, 35);
            this.ctxt7.TabIndex = 7;
            this.ctxt7.TabStop = false;
            this.ctxt7.Text = "7";
            this.ctxt7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Controls.Add(this.label38);
            this.panel1.Controls.Add(this.label37);
            this.panel1.Controls.Add(this.label36);
            this.panel1.Controls.Add(this.label35);
            this.panel1.Controls.Add(this.label34);
            this.panel1.Controls.Add(this.label33);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Location = new System.Drawing.Point(642, 52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(59, 504);
            this.panel1.TabIndex = 217;
            // 
            // label38
            // 
            this.label38.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Blue;
            this.label38.Location = new System.Drawing.Point(21, 436);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(36, 37);
            this.label38.TabIndex = 114;
            this.label38.Text = "8";
            // 
            // label37
            // 
            this.label37.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Blue;
            this.label37.Location = new System.Drawing.Point(19, 379);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(36, 37);
            this.label37.TabIndex = 113;
            this.label37.Text = "7";
            // 
            // label36
            // 
            this.label36.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Blue;
            this.label36.Location = new System.Drawing.Point(19, 329);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(36, 37);
            this.label36.TabIndex = 112;
            this.label36.Text = "6";
            // 
            // label35
            // 
            this.label35.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Blue;
            this.label35.Location = new System.Drawing.Point(19, 150);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(36, 37);
            this.label35.TabIndex = 111;
            this.label35.Text = "3";
            // 
            // label34
            // 
            this.label34.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Blue;
            this.label34.Location = new System.Drawing.Point(19, 94);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(36, 37);
            this.label34.TabIndex = 110;
            this.label34.Text = "2";
            // 
            // label33
            // 
            this.label33.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Blue;
            this.label33.Location = new System.Drawing.Point(19, 270);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(36, 37);
            this.label33.TabIndex = 109;
            this.label33.Text = "5";
            // 
            // label32
            // 
            this.label32.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Blue;
            this.label32.Location = new System.Drawing.Point(19, 211);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(36, 37);
            this.label32.TabIndex = 108;
            this.label32.Text = "4";
            // 
            // label31
            // 
            this.label31.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Blue;
            this.label31.Location = new System.Drawing.Point(21, 36);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(34, 37);
            this.label31.TabIndex = 107;
            this.label31.Text = "1";
            // 
            // imageViewerTR2
            // 
            this.imageViewerTR2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.imageViewerTR2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imageViewerTR2.CurrentZoom = 1F;
            this.imageViewerTR2.Image = null;
            this.imageViewerTR2.Location = new System.Drawing.Point(701, 53);
            this.imageViewerTR2.Margin = new System.Windows.Forms.Padding(3, 9, 3, 9);
            this.imageViewerTR2.MaxZoom = 20F;
            this.imageViewerTR2.MinZoom = 0.05F;
            this.imageViewerTR2.Name = "imageViewerTR2";
            this.imageViewerTR2.Size = new System.Drawing.Size(614, 504);
            this.imageViewerTR2.TabIndex = 216;
            // 
            // frmCheckQC4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1356, 742);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.imageViewerTR2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.imageViewerTR1);
            this.Controls.Add(this.lbl8);
            this.Controls.Add(this.lblGroup3);
            this.Controls.Add(this.lbl7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbl6);
            this.Controls.Add(this.lblEntry3);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txt8);
            this.Controls.Add(this.lblbatchname);
            this.Controls.Add(this.txt7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txt6);
            this.Controls.Add(this.lblGroup2);
            this.Controls.Add(this.txt5);
            this.Controls.Add(this.txt4);
            this.Controls.Add(this.lblGroup1);
            this.Controls.Add(this.txt3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txt1);
            this.Controls.Add(this.lblImage);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lblfilename);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.lblEntry2);
            this.Controls.Add(this.lblEntry1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnin);
            this.Controls.Add(this.btnout);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnRL);
            this.Controls.Add(this.btnRR);
            this.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "frmCheckQC4";
            this.Text = "VCB - QC";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCheck_FormClosing);
            this.Load += new System.EventHandler(this.frmCheck_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCheck_KeyDown);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRL;
        private System.Windows.Forms.Button btnRR;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnout;
        private System.Windows.Forms.Button btnin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblEntry1;
        private System.Windows.Forms.Label lblEntry2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblfilename;
        private System.Windows.Forms.Label lblImage;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblGroup2;
        private System.Windows.Forms.Label lblGroup1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblbatchname;
        private System.Windows.Forms.Label label8;
        private System.ComponentModel.BackgroundWorker bgwGetImage;
        private System.ComponentModel.BackgroundWorker bgwGetImage1;
        private System.Windows.Forms.Label lblGroup3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblEntry3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.RichTextBox txt8;
        private System.Windows.Forms.RichTextBox txt7;
        private System.Windows.Forms.RichTextBox txt6;
        private System.Windows.Forms.RichTextBox txt5;
        private System.Windows.Forms.RichTextBox txt4;
        private System.Windows.Forms.RichTextBox txt3;
        private System.Windows.Forms.RichTextBox txt2;
        private System.Windows.Forms.RichTextBox txt1;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl1;
        private ImageViewerTR.ImageViewerTR imageViewerTR1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label clbl14;
        private System.Windows.Forms.Label clbl13;
        private System.Windows.Forms.Label clbl12;
        private System.Windows.Forms.TextBox ctxt13;
        private System.Windows.Forms.TextBox ctxt12;
        private System.Windows.Forms.TextBox ctxt14;
        private System.Windows.Forms.Label clbl7;
        private System.Windows.Forms.Label clbl6;
        private System.Windows.Forms.Label clbl4;
        private System.Windows.Forms.Label clbl5;
        private System.Windows.Forms.Label clbl3;
        private System.Windows.Forms.Label clbl2;
        private System.Windows.Forms.Label clbl1;
        private System.Windows.Forms.Label clbl29;
        private System.Windows.Forms.TextBox ctxt29;
        private System.Windows.Forms.Label clbl26;
        private System.Windows.Forms.Label clbl27;
        private System.Windows.Forms.Label clbl28;
        private System.Windows.Forms.TextBox ctxt26;
        private System.Windows.Forms.TextBox ctxt28;
        private System.Windows.Forms.TextBox ctxt27;
        private System.Windows.Forms.TextBox ctxt23;
        private System.Windows.Forms.Label clbl25;
        private System.Windows.Forms.Label clbl24;
        private System.Windows.Forms.TextBox ctxt22;
        private System.Windows.Forms.Label clbl22;
        private System.Windows.Forms.TextBox ctxt25;
        private System.Windows.Forms.Label clbl23;
        private System.Windows.Forms.TextBox ctxt24;
        private System.Windows.Forms.Label clbl19;
        private System.Windows.Forms.Label clbl20;
        private System.Windows.Forms.Label clbl21;
        private System.Windows.Forms.TextBox ctxt19;
        private System.Windows.Forms.TextBox ctxt21;
        private System.Windows.Forms.TextBox ctxt20;
        private System.Windows.Forms.TextBox ctxt16;
        private System.Windows.Forms.Label clbl18;
        private System.Windows.Forms.Label clbl17;
        private System.Windows.Forms.TextBox ctxt15;
        private System.Windows.Forms.Label clbl15;
        private System.Windows.Forms.TextBox ctxt18;
        private System.Windows.Forms.Label clbl16;
        private System.Windows.Forms.TextBox ctxt17;
        private System.Windows.Forms.TextBox ctxt9;
        private System.Windows.Forms.Label clbl11;
        private System.Windows.Forms.Label clbl10;
        private System.Windows.Forms.TextBox ctxt8;
        private System.Windows.Forms.Label clbl8;
        private System.Windows.Forms.TextBox ctxt11;
        private System.Windows.Forms.Label clbl9;
        private System.Windows.Forms.TextBox ctxt10;
        private System.Windows.Forms.TextBox ctxt3;
        private System.Windows.Forms.TextBox ctxt2;
        private System.Windows.Forms.TextBox ctxt4;
        private System.Windows.Forms.TextBox ctxt5;
        private System.Windows.Forms.TextBox ctxt6;
        private System.Windows.Forms.TextBox ctxt1;
        private System.Windows.Forms.TextBox ctxt7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private ImageViewerTR.ImageViewerTR imageViewerTR2;
    }
}