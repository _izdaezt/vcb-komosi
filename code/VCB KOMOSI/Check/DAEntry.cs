﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Windows.Forms;
namespace VCB_KOMOSI
{
    class DAEntry_Check
    {
        private string stringconnecttion = String.Format("Data Source=" + Program.server + ";Initial Catalog=VCB_KOMOSI_2017;User Id=entrykomosi;Password=entrykomosi;Integrated Security=no;MultipleActiveResultSets=True;Packet Size=4096;Pooling=false;Connection Timeout=10;");

        /// <summary>
        /// Set status = lock hay free cho record
        /// </summary>
        /// <param name="id">Id của image</param>
        /// <param name="status">lock: không được nhập/ free: được nhập</param>
        public void SetHitPointImageCheck(int id, string tableform)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + tableform + "] SET CheckedDate = dateadd(MS, -700000 , getdate()) where Id = " + id;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật HitPoint\n" + sqlException.Message);
            }
        }

        public void SetHitPointImageCheckQC(int id, string tableform)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + tableform + "] SET QCDate = dateadd(MS, -700000 , getdate()) where Id = " + id;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật HitPoint\n" + sqlException.Message);
            }
        }

        public void Set_result_null(int id, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + table + "] SET Result = NULL WHERE ID = " + id;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật HitPoint\n" + sqlException.Message);
            }
        }

        /// <summary>
        /// Update 1 record vào table ImageContent (khi checker chon content1, content2, hay nhap moi result
        /// </summary>
        /// <param name="imgContent">đối tượng ImageContent cần update</param>
        public void UpdateResutl2(BOImageContent_Check imgContent, int userid, int ms, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + table + "] SET CheckerId = " + imgContent.CheckerId + ", CheckedDate = GetDate(),TimeMilisecondsCheck = " + ms + ", ResultLC = N'" + imgContent.Result + "', Result = N'" + imgContent.Result + "', Loisai2 = " + imgContent.Loisai2 + ", Loisai1 = " + imgContent.Loisai1 + ", Tongkytu = " + imgContent.Tongkytu + " WHERE Id = " + imgContent.Id;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật 1 record imageContent\n" + sqlException.Message);
            }
        }

        public void UpdateQC(BOImageContent_Check imgContent, int userid, int ms, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.[" + table + "] SET TimeMilisecondsCheck = " + ms + ", CheckerId = " + imgContent.CheckerId + ", QCDate = GetDate(), ResultLC = N'" + imgContent.Result + "', Result = N'" + imgContent.Result + "', Tongkytu = " + imgContent.Tongkytu + " WHERE Id = " + imgContent.Id;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật 1 record imageContent\n" + sqlException.Message);
            }
        }

        public DataTable Get_Hangchitiet(int Id)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = @"select Hangchitiet from db_owner.Imagecontent where Imagecontent.Id=" + Id + "";
                SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                con.Open();
                sqldataAdapter.SelectCommand = sqlCommand;
                sqldataAdapter.Fill(dt);
            }
            return dt;
        }

        //Get number image check
        public string ImageExistCheck(string tableform)
        {
            string returnVal = "";
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "SELECT COUNT(1) FROM  dbo.[" + tableform + "] where Result is null AND CheckedDate is not NULL";
                    con.Open();
                    returnVal = sqlCommand.ExecuteScalar().ToString();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return returnVal;
        }
        public string ImageExistQC(string tableform)
        {
            string returnVal = "";
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "SELECT COUNT(1) FROM  dbo.[" + tableform + "] where Result is null AND QCDate is not NULL";
                    con.Open();
                    returnVal = sqlCommand.ExecuteScalar().ToString();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return returnVal;
        }
        //Get number image check
        public string ImageExistQC(int batchId, int lvl)
        {
            string returnVal = "";
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;

                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "SELECT COUNT(1) FROM db_owner.Image JOIN db_owner.Page ON db_owner.Image.PageId = db_owner.Page.Id JOIN db_owner.Field ON db_owner.Image.FieldId = db_owner.Field.Id WHERE db_owner.Page.BatchId =" + batchId + " AND db_owner.Image.Status ='1'";
                    con.Open();
                    int numberOfRecord = (int)sqlCommand.ExecuteScalar();
                    returnVal = numberOfRecord.ToString();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return returnVal;
        }
        public string[] InsertPerformance(int batchId, int userid)
        {

            string[] id = null;
            id = new string[2];
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "InsertPerformanceCheck";
                    sqlCommand.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.Add("@StartTime", SqlDbType.DateTime).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.Add("@BatchID", SqlDbType.Int, 30).Value = batchId;
                    sqlCommand.Parameters.Add("@UserID", SqlDbType.Int, 10).Value = userid;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                    try
                    {
                        id[0] = sqlCommand.Parameters["@ID"].Value.ToString();
                        id[1] = sqlCommand.Parameters["@StartTime"].Value.ToString();
                    }
                    catch
                    {
                        id[0] = "";
                    }
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình đọc dữ liệu\n" + sqlException.Message);
            }
            return id;
        }
        public bool UpdatePerformance(string starttime, int ID)
        {

            bool id = false;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "UpdatePerformanceCheck";
                    sqlCommand.Parameters.Add("@Starttime", SqlDbType.DateTime, 30).Value = starttime;
                    sqlCommand.Parameters.Add("@ID", SqlDbType.Int, 10).Value = ID;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                    id = true;
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình đọc dữ liệu\n" + sqlException.Message);
            }
            return id;
        }
        //lấy status image
        //lấy status image
        public string[] GetbatchNew(int batchid)
        {
            string[] returnValue = new string[] { "", "" };
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT Batch.Id,Batch.Name FROM dbo.Batch WHERE Hitpoint=1 and Batch.id >" + batchid;
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        returnValue[0] = sqlDataReader["Id"].ToString().Trim();
                        returnValue[1] = sqlDataReader["Name"].ToString().Trim();
                        break;
                    }
                    sqlDataReader.Close();
                }
            }
            catch
            { }
            return returnValue;
        }
        public int GetIdCheck(string tableform)
        {
            int id = 0;
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(stringconnecttion))
                    {
                        SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.Connection = con;
                        sqlCommand.CommandText = "select top 1 @ID = id from dbo.[" + tableform + "] where Result is null and datediff(ms, CheckedDate,GETDATE())>600000;" +
                            "Update dbo.[" + tableform + "]  set CheckedDate  = GETDATE() where Id=@ID";
                        sqlCommand.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;
                        con.Open();
                        sqlCommand.ExecuteNonQuery();
                        id = Convert.ToInt32(sqlCommand.Parameters["@ID"].Value);
                    }
                }
                catch
                { id = 0; }
            }
            return id;
        }

        public int GetIdCheckQC(string tableform)
        {
            int id = 0;
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(stringconnecttion))
                    {
                        SqlCommand sqlCommand = new SqlCommand();
                        sqlCommand.Connection = con;
                        sqlCommand.CommandText = "select top 1 @ID = id from dbo.[" + tableform + "] where Content1 is not null and Content2 is not null and Result is null and datediff(ms, QCDate,GETDATE())>600000;" +
                            "Update dbo.[" + tableform + "]  set QCDate  = GETDATE() where Id=@ID";
                        sqlCommand.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;
                        con.Open();
                        sqlCommand.ExecuteNonQuery();
                        id = Convert.ToInt32(sqlCommand.Parameters["@ID"].Value);
                    }
                }
                catch
                { id = 0; }
            }
            return id;
        }

        public int GetIdImageEntriedFromBatchIdQC(int batchId)
        {
            int id = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "GetImageCheckQC";
                    sqlCommand.Parameters.Add("@BatchID", SqlDbType.Int).Value = batchId;
                    con.Open();
                    id = Convert.ToInt32(sqlCommand.ExecuteScalar().ToString());
                }
            }
            catch
            { }
            return id;
        }
        /// <summary>
        /// set hitpoint batch
        /// </summary>
        /// <param name="batchId"></param>
        public void SetHitpointBatch(string batchname, int template)
        {
            // check cheker        
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "SetHitpointBatchCheck";
                    sqlCommand.Parameters.Add("@template", SqlDbType.Int).Value = template;
                    sqlCommand.Parameters.Add("@batchname", SqlDbType.NVarChar).Value = batchname;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            { }
        }

        public void UpdateHitpoint_Batch(int batchid, int hitpoint)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Update dbo.Batch set HitPoint = " + hitpoint + " where Id =" + batchid;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            { }
        }

        public void Update_Hitpoint_Result(int batchId, int formID)
        {
            // check cheker        
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "Update_Hitpoint_Result";
                    sqlCommand.Parameters.Add("@BatchID", SqlDbType.Int).Value = batchId;
                    sqlCommand.Parameters.Add("@FormId", SqlDbType.Int).Value = formID;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            { }
        }
        /// <summary>
        /// Lấy đối tượng image để nhập content
        /// </summary>
        /// <param name="imageId">Id của image</param>
        /// <returns>Image</returns>
        public BOImage_Check GetImage(int imageId, string table)
        {
            BOImage_Check img = new BOImage_Check();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT Id as 'imgID',ImageName as 'PUrl' FROM dbo.[" + table + "] WHERE Id = " + imageId;
                    con.Open();
                    using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                        {
                            img.Id = (int)sqlDataReader["imgID"];
                            img.PageName = sqlDataReader["PUrl"].ToString().Trim();
                            //img.NG1 = (int)sqlDataReader["NG1"];
                            //img.NG2 = (int)sqlDataReader["NG2"];
                            //img.NG3 = (int)sqlDataReader["NG3"];
                            break;
                        }
                        sqlDataReader.Close();
                    }
                }
            }
            catch
            {

            }
            return img;
        }
        /// <summary>
        /// Check co ton tai record image content ko
        /// </summary>
        /// <param name="imageId">id cua image</param>
        /// <returns>ton tai hay ko</returns>
        public byte[] getImageOnServer(string Name, string nameTable)
        {
            byte[] returnVal = null;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT Binarymage FROM dbo.[" + nameTable + "] WHERE ImageName = @NameImage";
                    sqlCommand.Parameters.Add("@NameImage", SqlDbType.NVarChar).Value = Name;
                    con.Open();
                    returnVal = (byte[])sqlCommand.ExecuteScalar();
                }
            }
            catch
            { }
            return returnVal;
        }
        /// <summary>
        /// Lấy đối tượng imageContent để check content
        /// </summary>
        /// <param name="imageId">Id của image</param>
        /// <returns>ImageContent</returns>
        public BOImageContent_Check GetImageContent(int imageId, string table)
        {
            BOImageContent_Check imgContent = new BOImageContent_Check();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT dbo.[" + table + "].Id,Content1,UserId1,Content2,UserId2,us1.Fullname as f1,us1.[Center] as G1, us2.Fullname as f2,us2.[Center] as G2,Content1_OCR,ISNULL(Content3,Content_OCR) as Content3,us3.Fullname as f3,us3.[Center] as G3,Content_OCR FROM dbo.[" + table + "] left join db_owner.[user] as us1 ON userid1=us1.id left join db_owner.[user] as us2 on userid2=us2.id left join db_owner.[user] as us3 on userid3=us3.id where dbo.[" + table + "].Id = " + imageId;
                    con.Open();
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        imgContent.Id = Convert.ToInt32(sqlDataReader["Id"]);
                        //Content1            
                        imgContent.Content1 = sqlDataReader["Content1"].ToString().Split('|').ToList<string>();
                        imgContent.Name1 = sqlDataReader["f1"].ToString();
                        imgContent.Group1 = sqlDataReader["G1"].ToString();
                        //Content2                
                        imgContent.Content2 = sqlDataReader["Content2"].ToString().Split('|').ToList<string>();
                        imgContent.Name2 = sqlDataReader["f2"].ToString();
                        imgContent.Group2 = sqlDataReader["G2"].ToString();
                        //Content3                
                        imgContent.Content3 = sqlDataReader["Content3"].ToString().Split('|').ToList<string>();
                        imgContent.Name3 = sqlDataReader["f3"].ToString();
                        imgContent.Group3 = sqlDataReader["G3"].ToString();
                        //Hangchitiet
                        imgContent.Content1_OCR = sqlDataReader["Content1_OCR"].ToString().Split('|').ToList<string>();
                        //OCR
                        imgContent.Content_OCR = sqlDataReader["Content_OCR"].ToString().Replace('?', '*').Split('|').ToList<string>();
                        break;
                    }
                    sqlDataReader.Close();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Lỗi lấy dư liệu Content1 và Content2\n" + sqlException.Message);
            }
            return imgContent;
        }
        public void SetStatusQC(int imageid, string status)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE db_owner.[Image] SET status = '" + status + "' WHERE Id = " + imageid;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật status cua image\n" + sqlException.Message);
            }
        }

        public DataTable dtUser(string username)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = @"select ID,Fullname,Pair,Role,Lvl,password from db_owner.[User] where UPPER([name])=N'" + username.ToUpper() + "'";
                SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                con.Open();
                sqldataAdapter.SelectCommand = sqlCommand;
                sqldataAdapter.Fill(dt);
            }
            return dt;
        }
        //Get table Batch
        public DataTable dtBatch(int formID)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = "Select RIGHT('000' + CAST(ROW_NUMBER() over (order by (select 1)) as varchar(5)),3)+'     ' +Name as Name,ID,Hitpoint from dbo.batch Where DATEDIFF(MINUTE,UploadDate,getdate())>2 and TempID=" + formID;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlCommand;
                da.Fill(dt);
            }
            return dt;
        }
    }
}
