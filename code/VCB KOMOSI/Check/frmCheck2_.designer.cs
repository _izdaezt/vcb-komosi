﻿namespace VCB_KOMOSI
{
    partial class frmCheck2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCheck2));
            this.btnRL = new System.Windows.Forms.Button();
            this.btnRR = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnout = new System.Windows.Forms.Button();
            this.btnin = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblEntry1 = new System.Windows.Forms.Label();
            this.lblEntry2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblfilename = new System.Windows.Forms.Label();
            this.lblImage = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblGroup2 = new System.Windows.Forms.Label();
            this.lblGroup1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblbatchname = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.bgwGetImage1 = new System.ComponentModel.BackgroundWorker();
            this.txtE1 = new System.Windows.Forms.RichTextBox();
            this.lblEntry3 = new System.Windows.Forms.Label();
            this.lblGroup3 = new System.Windows.Forms.Label();
            this.timegetanh = new System.Windows.Forms.Timer(this.components);
            this.imageViewerTR2 = new ImageViewerTR.ImageViewerTR();
            this.txtE2 = new System.Windows.Forms.RichTextBox();
            this.txtResult = new System.Windows.Forms.RichTextBox();
            this.btSelectE1 = new System.Windows.Forms.Button();
            this.btSelectE2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnRL
            // 
            this.btnRL.BackgroundImage = global::VCBHL_Entry.Properties.Resources.object_rotate_left;
            this.btnRL.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRL.Location = new System.Drawing.Point(85, 12);
            this.btnRL.Name = "btnRL";
            this.btnRL.Size = new System.Drawing.Size(29, 25);
            this.btnRL.TabIndex = 53;
            this.btnRL.UseVisualStyleBackColor = true;
            this.btnRL.Click += new System.EventHandler(this.btnRL_Click);
            // 
            // btnRR
            // 
            this.btnRR.BackgroundImage = global::VCBHL_Entry.Properties.Resources.object_rotate_right;
            this.btnRR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRR.Location = new System.Drawing.Point(50, 12);
            this.btnRR.Name = "btnRR";
            this.btnRR.Size = new System.Drawing.Size(29, 25);
            this.btnRR.TabIndex = 54;
            this.btnRR.UseVisualStyleBackColor = true;
            this.btnRR.Click += new System.EventHandler(this.btnRR_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(15, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 25);
            this.button1.TabIndex = 59;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnout
            // 
            this.btnout.BackgroundImage = global::VCBHL_Entry.Properties.Resources.Zoom_Out_icon;
            this.btnout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnout.Location = new System.Drawing.Point(120, 12);
            this.btnout.Name = "btnout";
            this.btnout.Size = new System.Drawing.Size(29, 25);
            this.btnout.TabIndex = 60;
            this.btnout.UseVisualStyleBackColor = true;
            this.btnout.Click += new System.EventHandler(this.btnout_Click);
            // 
            // btnin
            // 
            this.btnin.BackgroundImage = global::VCBHL_Entry.Properties.Resources.Zoom_In_icon;
            this.btnin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnin.Location = new System.Drawing.Point(155, 12);
            this.btnin.Name = "btnin";
            this.btnin.Size = new System.Drawing.Size(29, 25);
            this.btnin.TabIndex = 61;
            this.btnin.UseVisualStyleBackColor = true;
            this.btnin.Click += new System.EventHandler(this.btnin_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(195, 494);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 62;
            this.label2.Text = "User1:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblEntry1
            // 
            this.lblEntry1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEntry1.AutoSize = true;
            this.lblEntry1.Location = new System.Drawing.Point(241, 494);
            this.lblEntry1.Name = "lblEntry1";
            this.lblEntry1.Size = new System.Drawing.Size(14, 16);
            this.lblEntry1.TabIndex = 64;
            this.lblEntry1.Text = "_";
            // 
            // lblEntry2
            // 
            this.lblEntry2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEntry2.AutoSize = true;
            this.lblEntry2.Location = new System.Drawing.Point(241, 557);
            this.lblEntry2.Name = "lblEntry2";
            this.lblEntry2.Size = new System.Drawing.Size(14, 16);
            this.lblEntry2.TabIndex = 65;
            this.lblEntry2.Text = "_";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(278, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 16);
            this.label10.TabIndex = 66;
            this.label10.Text = "Image Name:";
            // 
            // lblfilename
            // 
            this.lblfilename.AutoSize = true;
            this.lblfilename.ForeColor = System.Drawing.Color.Blue;
            this.lblfilename.Location = new System.Drawing.Point(355, 16);
            this.lblfilename.Name = "lblfilename";
            this.lblfilename.Size = new System.Drawing.Size(49, 16);
            this.lblfilename.TabIndex = 67;
            this.lblfilename.Text = "0001.jpg";
            // 
            // lblImage
            // 
            this.lblImage.AutoSize = true;
            this.lblImage.ForeColor = System.Drawing.Color.Blue;
            this.lblImage.Location = new System.Drawing.Point(897, 16);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(32, 16);
            this.lblImage.TabIndex = 94;
            this.lblImage.Text = "1211";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(852, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 16);
            this.label13.TabIndex = 93;
            this.label13.Text = "Count:";
            // 
            // lblGroup2
            // 
            this.lblGroup2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGroup2.AutoSize = true;
            this.lblGroup2.Location = new System.Drawing.Point(241, 573);
            this.lblGroup2.Name = "lblGroup2";
            this.lblGroup2.Size = new System.Drawing.Size(14, 16);
            this.lblGroup2.TabIndex = 98;
            this.lblGroup2.Text = "_";
            // 
            // lblGroup1
            // 
            this.lblGroup1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGroup1.AutoSize = true;
            this.lblGroup1.Location = new System.Drawing.Point(241, 510);
            this.lblGroup1.Name = "lblGroup1";
            this.lblGroup1.Size = new System.Drawing.Size(14, 16);
            this.lblGroup1.TabIndex = 97;
            this.lblGroup1.Text = "_";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(176, 510);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 16);
            this.label5.TabIndex = 95;
            this.label5.Text = "Trung tâm:";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSubmit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSubmit.BackgroundImage")));
            this.btnSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSubmit.Location = new System.Drawing.Point(1011, 69);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(171, 62);
            this.btnSubmit.TabIndex = 114;
            this.btnSubmit.TabStop = false;
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBack.BackgroundImage")));
            this.btnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBack.Location = new System.Drawing.Point(12, 674);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(96, 56);
            this.btnBack.TabIndex = 115;
            this.btnBack.TabStop = false;
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblbatchname
            // 
            this.lblbatchname.AutoSize = true;
            this.lblbatchname.ForeColor = System.Drawing.Color.Blue;
            this.lblbatchname.Location = new System.Drawing.Point(596, 16);
            this.lblbatchname.Name = "lblbatchname";
            this.lblbatchname.Size = new System.Drawing.Size(49, 16);
            this.lblbatchname.TabIndex = 135;
            this.lblbatchname.Text = "0001.jpg";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(519, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 16);
            this.label8.TabIndex = 134;
            this.label8.Text = "Batch Name:";
            // 
            // txtE1
            // 
            this.txtE1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtE1.Font = new System.Drawing.Font("Microsoft Sans Serif", 69.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtE1.ForeColor = System.Drawing.Color.Red;
            this.txtE1.Location = new System.Drawing.Point(335, 494);
            this.txtE1.Multiline = false;
            this.txtE1.Name = "txtE1";
            this.txtE1.Size = new System.Drawing.Size(745, 107);
            this.txtE1.TabIndex = 2;
            this.txtE1.Text = "";
            this.txtE1.Enter += new System.EventHandler(this.txt2_Enter);
            // 
            // lblEntry3
            // 
            this.lblEntry3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEntry3.AutoSize = true;
            this.lblEntry3.Location = new System.Drawing.Point(241, 623);
            this.lblEntry3.Name = "lblEntry3";
            this.lblEntry3.Size = new System.Drawing.Size(14, 16);
            this.lblEntry3.TabIndex = 143;
            this.lblEntry3.Text = "_";
            // 
            // lblGroup3
            // 
            this.lblGroup3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGroup3.AutoSize = true;
            this.lblGroup3.Location = new System.Drawing.Point(241, 639);
            this.lblGroup3.Name = "lblGroup3";
            this.lblGroup3.Size = new System.Drawing.Size(14, 16);
            this.lblGroup3.TabIndex = 145;
            this.lblGroup3.Text = "_";
            // 
            // timegetanh
            // 
            this.timegetanh.Interval = 200;
            this.timegetanh.Tick += new System.EventHandler(this.timegetanh_Tick);
            // 
            // imageViewerTR2
            // 
            this.imageViewerTR2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.imageViewerTR2.BackColor = System.Drawing.Color.LightGray;
            this.imageViewerTR2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imageViewerTR2.CurrentZoom = 1F;
            this.imageViewerTR2.Image = null;
            this.imageViewerTR2.Location = new System.Drawing.Point(3, 162);
            this.imageViewerTR2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.imageViewerTR2.MaxZoom = 20F;
            this.imageViewerTR2.MinZoom = 0.05F;
            this.imageViewerTR2.Name = "imageViewerTR2";
            this.imageViewerTR2.Size = new System.Drawing.Size(1347, 312);
            this.imageViewerTR2.TabIndex = 148;
            // 
            // txtE2
            // 
            this.txtE2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtE2.Font = new System.Drawing.Font("Microsoft Sans Serif", 69.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtE2.ForeColor = System.Drawing.Color.Red;
            this.txtE2.Location = new System.Drawing.Point(335, 623);
            this.txtE2.Multiline = false;
            this.txtE2.Name = "txtE2";
            this.txtE2.Size = new System.Drawing.Size(745, 107);
            this.txtE2.TabIndex = 149;
            this.txtE2.Text = "";
            // 
            // txtResult
            // 
            this.txtResult.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 70F);
            this.txtResult.ForeColor = System.Drawing.Color.Red;
            this.txtResult.Location = new System.Drawing.Point(215, 46);
            this.txtResult.Multiline = false;
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(745, 107);
            this.txtResult.TabIndex = 150;
            this.txtResult.Text = "";
            // 
            // btSelectE1
            // 
            this.btSelectE1.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSelectE1.Location = new System.Drawing.Point(1120, 494);
            this.btSelectE1.Name = "btSelectE1";
            this.btSelectE1.Size = new System.Drawing.Size(75, 107);
            this.btSelectE1.TabIndex = 151;
            this.btSelectE1.Text = "E1";
            this.btSelectE1.UseVisualStyleBackColor = true;
            this.btSelectE1.Click += new System.EventHandler(this.btSelectE1_Click);
            // 
            // btSelectE2
            // 
            this.btSelectE2.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSelectE2.Location = new System.Drawing.Point(1120, 623);
            this.btSelectE2.Name = "btSelectE2";
            this.btSelectE2.Size = new System.Drawing.Size(75, 107);
            this.btSelectE2.TabIndex = 152;
            this.btSelectE2.Text = "E2";
            this.btSelectE2.UseVisualStyleBackColor = true;
            this.btSelectE2.Click += new System.EventHandler(this.btSelectE2_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(176, 639);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 16);
            this.label3.TabIndex = 156;
            this.label3.Text = "Trung tâm:";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(195, 623);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 16);
            this.label9.TabIndex = 155;
            this.label9.Text = "User3:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(176, 573);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 16);
            this.label4.TabIndex = 154;
            this.label4.Text = "Trung tâm:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(195, 557);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 16);
            this.label6.TabIndex = 153;
            this.label6.Text = "User2:";
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Location = new System.Drawing.Point(14, 538);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(65, 16);
            this.lbl2.TabIndex = 157;
            this.lbl2.Text = "họ tên kana";
            this.lbl2.Visible = false;
            // 
            // frmCheck2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1356, 742);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btSelectE2);
            this.Controls.Add(this.btSelectE1);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.txtE2);
            this.Controls.Add(this.imageViewerTR2);
            this.Controls.Add(this.lblGroup3);
            this.Controls.Add(this.lblEntry3);
            this.Controls.Add(this.txtE1);
            this.Controls.Add(this.lblbatchname);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.lblGroup2);
            this.Controls.Add(this.lblGroup1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblImage);
            this.Controls.Add(this.lblfilename);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lblEntry2);
            this.Controls.Add(this.lblEntry1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnin);
            this.Controls.Add(this.btnout);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnRL);
            this.Controls.Add(this.btnRR);
            this.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "frmCheck2";
            this.Text = "VCB - Check";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCheck_FormClosing);
            this.Load += new System.EventHandler(this.frmCheck_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCheck_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRL;
        private System.Windows.Forms.Button btnRR;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnout;
        private System.Windows.Forms.Button btnin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblEntry1;
        private System.Windows.Forms.Label lblEntry2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblfilename;
        private System.Windows.Forms.Label lblImage;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblGroup2;
        private System.Windows.Forms.Label lblGroup1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblbatchname;
        private System.Windows.Forms.Label label8;
        private System.ComponentModel.BackgroundWorker bgwGetImage1;
        private System.Windows.Forms.RichTextBox txtE1;
        private System.Windows.Forms.Label lblEntry3;
        private System.Windows.Forms.Label lblGroup3;
        private ImageViewerTR.ImageViewerTR imageViewerTR2;
        private System.Windows.Forms.Timer timegetanh;
        private System.Windows.Forms.RichTextBox txtE2;
        private System.Windows.Forms.RichTextBox txtResult;
        private System.Windows.Forms.Button btSelectE1;
        private System.Windows.Forms.Button btSelectE2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl2;
    }
}