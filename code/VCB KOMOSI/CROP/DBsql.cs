﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;
using System.Data;
namespace VCB_KOMOSI
{
    class DBsql
    {
        // bool selecthipoint = true;

        private static string GetConnectionString()
        {
            // To avoid storing the connection string in your code,
            // you can retrive it from a configuration file.
            return "Data Source=" + Program.server + ";Initial Catalog=VCB_KOMOSI_2017;User Id=entrykomosi;Password=entrykomosi;Integrated Security=no;MultipleActiveResultSets=True;Packet Size=4096;Pooling=false;";
        }

        /// <summary>
        /// Insert image
        /// </summary>       
        public void InsertImage(string formname, string contentOCR, string code, string imagename, string batchName)
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "INSERT INTO dbo.[" + formname + "] (ImageName,BatchName,Content_OCR,CodeOCR,InputDate1,InputDate2,InputDate3) VALUES (N'" + imagename + "',N'" + batchName + "',N'" + contentOCR + "', N'" + code + "',dateadd(MS, -600000 , getdate()),dateadd(MS, -600000 , getdate()),dateadd(MS, -600000 , getdate()))";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch { }
        }

        public void InsertImage_Kojin(string formname, string contentOCR, string code, string imagename, string batchName)
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "INSERT INTO dbo.[" + formname + "] (ImageName,BatchName,Content_OCR,CodeOCR,InputDate1,InputDate2,InputDate3,Content1,Content2,Content3,Content1_OCR) VALUES (N'" + imagename + "',N'" + batchName + "',N'" + contentOCR + "', N'" + code + "',dateadd(MS, -600000 , getdate()),dateadd(MS, -600000 , getdate()),dateadd(MS, -600000 , getdate()), '', '', '', '')";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch { }
        }

        /// <summary>
        /// Insert Batch
        /// </summary>    
        public int InsertBatch(string batchname, int formid, string content, byte[] bytecmp)
        {
            int id = 0;
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "INSERT INTO dbo.Batch (Name,UploadDate,Hitpoint,TempId,ContentOCR,clcmp) VALUES (N'" + batchname + "',Getdate(),0," + formid + ",@contentOCR,@Varcmp); Select SCOPE_IDENTITY();";
                    sqlCommand.Parameters.Add("@contentOCR", SqlDbType.NVarChar).Value = content;
                    sqlCommand.Parameters.Add(new SqlParameter("@Varcmp", bytecmp));
                    con.Open();
                    id = Int32.Parse(sqlCommand.ExecuteScalar().ToString());
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Error! Insert Batch\n" + sqlException.Message);
            }
            return id;
        }

        public void InsertBatchZ(string batchname, int formid, string content, byte[] bytecmp)
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "INSERT INTO dbo.Batch (Name,UploadDate,Hitpoint,TempId,ContentOCR,clcmp) VALUES (N'" + batchname + "',Getdate(),3," + formid + ",@contentOCR,@Varcmp);";
                    sqlCommand.Parameters.Add("@contentOCR", SqlDbType.NVarChar).Value = content;
                    sqlCommand.Parameters.Add(new SqlParameter("@Varcmp", bytecmp));
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Error! Insert Batch\n" + sqlException.Message);
            }
        }

        public int InsertBatchZ_New(string batchname, int formid, string content, byte[] bytecmp)
        {
            int id = 0;
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "INSERT INTO dbo.Batch (Name,UploadDate,Hitpoint,TempId,ContentOCR,clcmp) VALUES (N'" + batchname + "',Getdate(),0," + formid + ",@contentOCR,@Varcmp); Select SCOPE_IDENTITY();";
                    sqlCommand.Parameters.Add("@contentOCR", SqlDbType.NVarChar).Value = content;
                    sqlCommand.Parameters.Add(new SqlParameter("@Varcmp", bytecmp));
                    con.Open();
                    id = Int32.Parse(sqlCommand.ExecuteScalar().ToString());
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Error! Insert Batch\n" + sqlException.Message);
            }
            return id;
        }

        /// <summary>
        /// Get Id Batch
        /// </summary>
        /// <param name="template"></param>
        public int GetBatchID(string batchname)
        {
            int id = 0;
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT ID FROM dbo.Batch WHERE Name = N'" + batchname.ToUpper() + "'";
                    con.Open();
                    id = Convert.ToInt32(sqlCommand.ExecuteScalar());
                }
            }
            catch
            { }
            return id;
        }
        public DataTable Get_AllBatchID(string batchname)
        {
            DataTable dt = new DataTable();
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT ID FROM dbo.Batch WHERE Name = N'" + batchname.ToUpper() + "'";
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }
            }
            catch
            { }
            return dt;
        }
        /// <summary>
        /// Get Id Batch
        /// </summary>
        /// <param name="template"></param>
        public bool checkbatch(string batchname, int formid)
        {
            bool vl = false;
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT 1 FROM dbo.Batch WHERE formid=" + formid + " AND Name = N'" + batchname + "'";
                    con.Open();
                    if (sqlCommand.ExecuteScalar() != null)
                    {
                        string vl1 = sqlCommand.ExecuteScalar().ToString();
                        vl = true;
                    }
                }
            }
            catch
            { }
            return vl;
        }

        /// <summary>
        /// Get Page id
        /// </summary>
        /// <param name="template"></param>
        public int PageInBatch(int batchid)
        {
            int id = 0;
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT Count(ID) FROM db_owner.Page WHERE batchid = " + batchid;
                    con.Open();
                    id = (int)sqlCommand.ExecuteScalar();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return id;
        }
        public string countImageCompleted(string batchid)
        {
            string id = "";
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT Count(1) FROM db_owner.Image join db_owner.ImageContent on Image.Id=ImageId WHERE BatchName IN (" + batchid + ")";
                    con.Open();
                    id = sqlCommand.ExecuteScalar().ToString();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return id;
        }


        public void DeleteBatch(string batchname)
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "delete db_owner.[imagecontent] from db_owner.imagecontent JOIN db_owner.[image] ON imageid=[image].id where BatchName=N'" + batchname + "';" +
                                             "delete db_owner.[image] from db_owner.[image] where BatchName=N'" + batchname + "';" +
                                             "delete dbo.[batch] where Name=N'" + batchname + "';";
                    sqlCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = batchname;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình Delete Batch\n" + sqlException.Message);
            }

        }
        public void DeleteTalble(string batchName)
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "DROP TABLE [" + batchName + "]";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }

            }
            catch
            { }
        }

        public List<string> GetImageOnServerWithListImage(string listimage, string nametable)
        {
            List<string> list = new List<string>();

            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select NameImage from dbo.[" + nametable + "] where NameImage in (" + listimage + ")";
                    con.Open();
                    using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                        {
                            list.Add(sqlDataReader["NameImage"].ToString().Trim());
                        }
                        sqlDataReader.Close();
                    }

                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return list;
        }
        public void InsertImageToServer(string Name, byte[] image, string nametable)
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "INSERT INTO dbo.[" + nametable + "] (ImageName,Binarymage,DayUp) VALUES (@NameImage,@VarImage,GETDATE())";
                    sqlCommand.Parameters.Add(new SqlParameter("@VarImage", image));
                    sqlCommand.Parameters.Add("@NameImage", SqlDbType.NVarChar).Value = Name;
                    con.Open();
                    sqlCommand.ExecuteReader();
                }
            }
            catch { }
        }
        public void UpdateHitpointBatch(string idbatch, string template)
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Update db_owner.Image set Hitpoint=4 where BatchName=N'" + idbatch + "' AND Template=" + template;
                    con.Open();
                    sqlCommand.ExecuteReader();
                }
            }
            catch { }
        }
        public void UpdateHitpointBatchAll()
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Update dbo.Batch set Hitpoint=2 where Hitpoint=3";
                    con.Open();
                    sqlCommand.ExecuteReader();
                }
            }
            catch { }
        }
        public DataTable GetBatch()
        {
            DataTable dt = new DataTable();
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select Batch.Name,TempID as Template,UploadDate as 'Ngày up',sysindexes.Rows as N'Số lượng' from dbo.Batch INNER JOIN sysobjects ON sysobjects.Name = Batch.Name INNER JOIN sysindexes  ON sysobjects.id = sysindexes.id WHERE len(sysobjects.Name)=21 and Hitpoint<3 and sysindexes.Rows > 0";
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }

            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return dt;
        }
        public void Creat_table(string name)
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "CREATE TABLE [dbo].[" + name + "](	[Id] [int] IDENTITY(1,1) NOT NULL,	[NameImage] [nvarchar](300) NOT NULL,	[BinaryImage] [varbinary](max) NOT NULL,	[DateCreated] [datetime] NOT NULL, CONSTRAINT [PK_" + name + "] PRIMARY KEY CLUSTERED (	[NameImage] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            {

            }
        }
        //Get number image check
        public string countImage(string name)
        {
            string returnVal = "0";
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Select count(Id) from dbo.[" + name + "]";
                    con.Open();
                    returnVal = sqlCommand.ExecuteScalar().ToString();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return returnVal;
        }
        public DataTable GetNameTemplate()
        {
            DataTable dt = new DataTable();
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "select Name,Id from db_owner.FormTemplate";
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return dt;
        }
        public DataTable get_table()
        {
            DataTable dt = new DataTable();
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "select Id, Name from dbo.Batch";
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return dt;
        }
        public int ktbatch(string name)
        {
            int id = 0;
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "select Count(Id) from Batch where [Name] = N'" + name + "'";
                    con.Open();
                    id = (int)sqlCommand.ExecuteScalar();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return id;
        }
        public void Drop_Table(string batchName)
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "DROP TABLE [" + batchName + "]";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            {

            }
        }
        public void Delete_AllBatch()
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "delete db_owner.[imagecontent];DBCC CHECKIDENT ('db_owner.[imagecontent]', RESEED, 1)" +
                                             "delete db_owner.[image];DBCC CHECKIDENT ('db_owner.[image]', RESEED, 1)" +
                                             "delete db_owner.[page];DBCC CHECKIDENT ('db_owner.[page]', RESEED, 1)" +
                                             "delete dbo.[batch];DBCC CHECKIDENT ('dbo.[batch]', RESEED, 1)" +
                                             "delete dbo.[Performance];DBCC CHECKIDENT ('dbo.[Performance]', RESEED, 1)" +
                                             "delete dbo.[LastCheck];DBCC CHECKIDENT ('dbo.[LastCheck]', RESEED, 1)" +
                                             "delete dbo.[PerformanceCheck];DBCC CHECKIDENT ('dbo.[PerformanceCheck]', RESEED, 1)" +
                                             "delete dbo.[PerformanceLastCheck];DBCC CHECKIDENT ('dbo.[PerformanceLastCheck]', RESEED, 1)";

                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            {

            }
        }

        public void Creat_TableImage(string name)
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "CREATE TABLE [dbo].[" + name + "](	[Id] [int] IDENTITY(1,1) NOT NULL,[ImageName] [nvarchar](200) NULL,[Binarymage] [varbinary](max) NULL, [DayUp] [datetime] NULL CONSTRAINT [PK_" + name + "] PRIMARY KEY CLUSTERED ([Id] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            { }
        }

        public void Creat_TableContent(string name)
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "CREATE TABLE [dbo].[" + name + "]([Id] [int] IDENTITY(1,1) NOT NULL,[ImageName] [nvarchar](100) NULL, [BatchName] [nvarchar](250) NULL,[Content1] [nvarchar](max) NULL, [Content1_OCR] [nvarchar](max) NULL, [UserId1] [int] NULL,[InputDate1] [datetime] NULL, [NG1] [int] NULL, [Content2] [nvarchar](max) NULL,[UserId2] [int] NULL,[InputDate2] [datetime] NULL, [NG2] [int] NULL, [Result] [nvarchar](max) NULL,[CheckerId] [int] NULL,[CheckedDate] [datetime] NULL,[QCID] [int] NULL,[TypeRate] [decimal](18, 0) NULL,[State] [nchar](10) NULL,[QCDate] [datetime] NULL,[LastCheckId] [int] NULL,[Tongkytu] [int] NULL,[Loisai1] [int] NULL,[Loisai2] [int] NULL,[Tongkytu1] [int] NULL,[Tongkytu2] [int] NULL,[TimeMiliseconds1] [int] NULL,[TimeMiliseconds2] [int] NULL,[TimeMilisecondsCheck] [int] NULL,[TimeMilisecondsLC] [int] NULL,[TimeNouhin] [int] NULL,[UserNouhin] [int] NULL,[ResultNouhin] [nvarchar](300) NULL,[ResultLC] [nvarchar](max) NULL, [Loisai3] [int] NULL,[Tongkytu3] [int] NULL,[TimeMiliseconds3] [int] NULL,[Content3] [nvarchar](1000) NULL,[UserID3] [int] NULL,[InputDate3] [datetime] NULL, [NG3] [int] NULL, [CodeOCR] [nvarchar](50) NULL, [Content_OCR] [nvarchar](1000) NULL,CONSTRAINT [PK_" + name + "] PRIMARY KEY CLUSTERED ([Id] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY])";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            { }
        }

        public void Delete_Batch(int batchid)
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Delete from dbo.Batch where Id =" + batchid;
                    //sqlCommand.CommandText = "Update dbo.Batch set Hitpoint11 = -1, Hitpoint12 = -1, Hitpoint31 = -1, Hitpoint32 = -1, Hitpoint51 = -1, Hitpoint52 = -1, Hitpoint71 = -1, Hitpoint72 = -1 where Id =" + batchid;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            { }
        }

        public void Drop_table(string table)
        {
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "DROP TABLE [" + table + "]";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            { }
        }

        public string countImageUp(string name)
        {
            string returnVal = "0";
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "Select SUM(sysindexes.Rows) as N'Số lượng' from dbo.Batch INNER JOIN sysobjects ON sysobjects.Name = Batch.Name INNER JOIN sysindexes  ON sysobjects.id = sysindexes.id WHERE len(sysobjects.Name)=21 and Hitpoint<3 and sysindexes.Rows > 0 and Batch.Id in (" + name + ")";
                    con.Open();
                    returnVal = sqlCommand.ExecuteScalar().ToString();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return returnVal;
        }

        public DataTable GetBatchIdByName(string name)
        {
            DataTable dt = new DataTable();
            try
            {
                string connectionString = GetConnectionString();
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "select Id, TempID from dbo.Batch where Name = N'" + name + "'";
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlCommand;
                    da.Fill(dt);
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception(sqlException.Message);
            }
            return dt;
        }
    }
}
