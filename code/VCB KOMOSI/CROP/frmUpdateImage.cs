﻿using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace VCB_KOMOSI
{
    public partial class frmUpdateImage : Form
    {
        ImageCodecInfo myImageCodecInfo;
        System.Drawing.Imaging.Encoder myEncoder;
        EncoderParameters myEncoderParameters;
        EncoderParameter myEncoderParameter;

        int pagesFilePathLength;
        string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\pathCrop.txt";
        DBsql dbsql = new DBsql();
        DataTable dt = new DataTable();
        string[] arrfolder;
        DataTable dtfolder = new DataTable();
        List<string> lstOCR;
        //List<string> arrbatchid;
        List<string> lsBatch;
        List<string> lsfileUp;
        List<string> Count_Batch;
        string[] arrTemplate = new string[4] { "Kojin_LV1", "Kojin_LV3", "Zokusei_LV1", "Zokusei  3" };
        Stopwatch stopWatch;
        delegate void BarDelegate();
        List<string> lstBatchId;
        int countPage = 0;
        public frmUpdateImage()
        {
            InitializeComponent();
            grThongkeV.IndicatorWidth = 50;
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("Template", typeof(string));
            dt.Columns.Add("Số lượng", typeof(string));
            dt.Columns.Add("Ngày up", typeof(string));
            dtfolder.Columns.Add("Tên batch", typeof(string));
            dtfolder.Columns.Add("Số lượng", typeof(string));

        }

        private void btnSourceDirectoryBrow_Click(object sender, EventArgs e)
        {
            string strPreferenceToRemember = @"C:\";
            if (File.Exists(path))
            {
                strPreferenceToRemember = File.ReadAllText(path);
            }
            else
            {
                File.WriteAllText(path, strPreferenceToRemember);
            }

            FolderBrowserDialogEx fbd = new FolderBrowserDialogEx();
            fbd.SelectedPath = strPreferenceToRemember;
            if (DialogResult.OK == fbd.ShowDialog())
            {
                txtSourceDirectory.Text = fbd.SelectedPath;
                File.WriteAllText(path, txtSourceDirectory.Text);
            }
            else
            {
                return;
            }
            countPage = 0;
            lsBatch = new List<string>();
            lsfileUp = new List<string>();
            lstOCR = new List<string>();
            arrfolder = null;
            txtSourceDirectory.Text = fbd.SelectedPath;
            arrfolder = Directory.GetDirectories(txtSourceDirectory.Text);
            dtfolder.Rows.Clear();
            for (int i = 0; i < arrfolder.Length; i++)
            {
                string batchName = Path.GetFileName(arrfolder[i]);
                if (dbsql.ktbatch(batchName) == 0)
                {
                    lsBatch.Add(arrfolder[i]);
                    if (batchName[0] != 'Z')
                    {
                        var arrfile = Directory.GetFiles(arrfolder[i], "*.tif");
                        dtfolder.Rows.Add(new string[] { batchName, arrfile.Length.ToString() });
                        dtfolder.Rows.Add(new string[] { batchName + "_Lv3", arrfile.Length.ToString() });
                        countPage = countPage + (arrfile.Length * 2);
                    }
                    else
                    {
                        var arrfile = Directory.GetFiles(arrfolder[i], "*.tif");
                        dtfolder.Rows.Add(new string[] { batchName, arrfile.Length.ToString() });
                        countPage = countPage + (arrfile.Length);
                    }
                }
                else
                {
                    var msb = MessageBox.Show("Batch " + Path.GetFileName(arrfolder[i]) + " đã tồn tại, bạn có muốn xóa batch up lại ko?", "Thông báo", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    if (msb == DialogResult.Yes)
                    {
                        dbsql.DeleteBatch(batchName);
                        dbsql.DeleteTalble(batchName);
                        lsBatch.Add(arrfolder[i]);
                        var arrfile = Directory.GetFiles(arrfolder[i], "*.tif");
                        dtfolder.Rows.Add(new string[] { batchName, arrfile.Length.ToString() });
                        dtfolder.Rows.Add(new string[] { batchName + "_Lv3", arrfile.Length.ToString() });
                        countPage = countPage + (arrfile.Length * 2);
                        SelectBatch();
                    }
                }
            }
            grcFolderUP.DataSource = dtfolder;
            grvFolderUp.Columns["Số lượng"].Summary.Clear();
            grvFolderUp.Columns["Số lượng"].Summary.Add(DevExpress.Data.SummaryItemType.Sum, "Số lượng", "{0}");
            lblPage.Text = countPage.ToString();
            lblImageUp.Text = "0";
            lblTimeUp.Text = "0";
            prbUpImage.Value = 0;
        }      

        private void btnUpImage_Click(object sender, EventArgs e)
        {
            lstBatchId = new List<string>();
            Count_Batch = new List<string>();
            if (txtSourceDirectory.Text == "")
                return;
            int countlsBatch = lsBatch.Count;
            for (int i = 0; i < countlsBatch; i++)
            {
                string batchname = Path.GetFileName(lsBatch[i]);
                string pathOCR = Directory.GetFiles(lsBatch[i], "*.txt").FirstOrDefault();
                string strOCR = File.ReadAllText(pathOCR, Encoding.GetEncoding("Shift_jis"));
                string pathCMP = Directory.GetFiles(lsBatch[i], "*.cmp").FirstOrDefault();
                string[] arrOCR = File.ReadAllLines(pathOCR, Encoding.GetEncoding("Shift_jis"));
                Array.Sort(arrOCR);
                byte[] arrcmp = null;
                string[] strCMP = null; 
                if (File.Exists(pathCMP))
                {
                    arrcmp = File.ReadAllBytes(pathCMP);
                    strCMP = File.ReadAllLines(pathCMP, Encoding.GetEncoding("Shift_jis"));
                }
                else
                {
                    MessageBox.Show("Không có file cmp", "Thông báo"); return;
                }
                string[] arrfile = Directory.GetFiles(lsBatch[i], "*.tif");
                if (batchname[0] != 'Z')
                {
                    int batchid = dbsql.InsertBatch(batchname, 1, strOCR, arrcmp);
                    if (batchid != 0)
                    {
                        lstBatchId.Add(batchid.ToString());
                    }
                    string tableform = batchid + "--" + batchname;
                    string tableimage = batchname;
                    try
                    {
                        dbsql.Creat_TableImage(tableimage);
                        dbsql.Creat_TableContent(tableform);
                    }
                    catch
                    {
                        dbsql.Delete_Batch(batchid);
                        dbsql.Drop_table(tableform);
                        dbsql.Drop_table(tableimage);
                    }
                    int batchid_lv3 = dbsql.InsertBatch(batchname, 2, strOCR, arrcmp);
                    if (batchid_lv3 != 0)
                    {
                        lstBatchId.Add(batchid_lv3.ToString());
                    }
                    string tableform_lv3 = batchid_lv3 + "--" + batchname + "--lv3";
                    try
                    {
                        dbsql.Creat_TableContent(tableform_lv3);
                    }
                    catch
                    {
                        dbsql.Delete_Batch(batchid_lv3);
                        dbsql.Drop_table(tableform_lv3);
                    }
                    string[] arrfile1 = Array.ConvertAll(arrfile, x => x + "|1|" + tableform + "|" + tableimage);
                    string[] arrfile3 = Array.ConvertAll(arrfile, x => x + "|2|" + tableform_lv3 + "|" + tableimage);
                    lsfileUp.AddRange(arrfile1);
                    lsfileUp.AddRange(arrfile3);
                    lstOCR.AddRange(arrOCR);
                    lstOCR.AddRange(arrOCR);
                    Count_Batch.Add("N'" + batchname + "'");
                }
                else
                {
                    int batchid = dbsql.InsertBatchZ_New(batchname, 3, strOCR, arrcmp);
                    if (batchid != 0)
                    {
                        lstBatchId.Add(batchid.ToString());
                    }
                    string tableform = batchid + "--" + batchname;
                    string tableimage = batchname;
                    try
                    {
                        dbsql.Creat_TableImage(tableimage);
                        dbsql.Creat_TableContent(tableform);
                    }
                    catch
                    {
                        dbsql.Delete_Batch(batchid);
                        dbsql.Drop_table(tableform);
                        dbsql.Drop_table(tableimage);
                    }
                    string[] arrfile1 = Array.ConvertAll(arrfile, x => x + "|3|" + tableform + "|" + tableimage);
                    lsfileUp.AddRange(arrfile1);
                    lstOCR.AddRange(arrOCR);
                    Count_Batch.Add("N'" + batchname + "'");

                    string[] arrOCR_BatchZ = File.ReadAllLines(pathOCR, Encoding.GetEncoding("Shift_jis"));
                    string batchTemp = batchname;
                    StringBuilder sb = new StringBuilder(batchTemp);
                    if (sb[1] == '1')
                    {
                        sb[1] = '2';
                    }
                    string repl1 = batchTemp.Substring(2, 2);
                    string repl2 = batchTemp.Substring(4, 2);
                    sb.Remove(2, 2);
                    sb.Insert(2, repl2);
                    sb.Remove(4, 2);
                    sb.Insert(4, repl1);
                    batchTemp = sb.ToString();
                    string dir2 = @"\\192.168.1.18\komosi\Output\" + DateTime.Now.ToString("yyMMdd") + @"\" + batchTemp;
                    //string dir2 = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\komosi\Output\" + DateTime.Now.ToString("yyMMdd") + @"\" + batchTemp;
                    dbsql.InsertBatchZ(batchname, 1, strOCR, arrcmp);
                    if (!Directory.Exists(dir2))
                    {
                        Directory.CreateDirectory(dir2);
                    }
                    try
                    {
                        #region edittxt     
                        int vl = arrOCR_BatchZ.Length;
                        for (int j = 0; j < vl; j++)
                        {
                            string line = "";
                            string[] Fields;
                            line = arrOCR_BatchZ[j];
                            //cắt dữ liệu các trường đưa vào mảng 1.8.2015
                            Fields = line.Split(new char[] { ',' });

                            // cột 72 _ 7 ký tự _ thêm space
                            line = line.Insert(63, "       ");
                            // cột 125 _ 50 ký tự _ thêm space
                            line = line.Insert(73, "　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　");
                            // cột 129 _ 1 ký tự _ thêm space
                            line = line.Insert(126, " ");
                            //thay đổi lại cột B thành G 08/08    

                            string cot13 = Fields[12].Replace("\"", "");
                            if (cot13 == "")
                            {
                                line = line.Insert(130, " ");
                            }
                            else
                            {
                                line = line.Remove(130, 1);
                                line = line.Insert(130, cot13);
                            }
                            string cot14 = Fields[13].Replace("\"", "");
                            if (cot14 == "")
                            {
                                line = line.Insert(134, "3");
                            }
                            else
                            {
                                line = line.Remove(134, 1);
                                line = line.Insert(134, cot14);
                            }
                            string cot15_1 = Fields[14].Replace("\"", "");
                            if (cot15_1 == "")
                            {
                                line = line.Insert(138, "   ");
                            }
                            else
                            {
                                line = line.Remove(138, 3);
                                line = line.Insert(138, cot15_1);
                            }
                            string cot15_2 = Fields[15].Replace("\"", "");
                            if (cot15_2 == "")
                            {
                                line = line.Insert(144, "   ");
                            }
                            else
                            {
                                line = line.Remove(144, 3);
                                line = line.Insert(144, cot15_2);
                            }
                            string cot15_3 = Fields[16].Replace("\"", "");
                            if (cot15_3 == "")
                            {
                                line = line.Insert(150, "   ");
                            }
                            else
                            {
                                line = line.Remove(150, 3);
                                line = line.Insert(150, cot15_3);
                            }
                            string cot15_4 = Fields[17].Replace("\"", "");
                            if (cot15_4 == "")
                            {
                                line = line.Insert(156, "   ");
                            }
                            else
                            {
                                line = line.Remove(156, 3);
                                line = line.Insert(156, cot15_4);
                            }
                            string cot15_5 = Fields[18].Replace("\"", "");
                            if (cot15_5 == "")
                            {
                                line = line.Insert(162, "   ");
                            }
                            else
                            {
                                line = line.Remove(162, 3);
                                line = line.Insert(162, cot15_5);
                            }
                            string cot15_6 = Fields[19].Replace("\"", "");
                            if (cot15_6 == "")
                            {
                                line = line.Insert(168, "   ");
                            }
                            else
                            {
                                line = line.Remove(168, 3);
                                line = line.Insert(168, cot15_6);
                            }
                            string cot15_7 = Fields[20].Replace("\"", "");
                            if (cot15_7 == "")
                            {
                                line = line.Insert(174, "   ");
                            }
                            else
                            {
                                line = line.Remove(174, 3);
                                line = line.Insert(174, cot15_7);
                            }
                            string cot15_8 = Fields[21].Replace("\"", "");
                            if (cot15_8 == "")
                            {
                                line = line.Insert(180, "   ");
                            }
                            else
                            {
                                line = line.Remove(180, 3);
                                line = line.Insert(180, cot15_8);
                            }
                            string cot15_9 = Fields[22].Replace("\"", "");
                            if (cot15_9 == "")
                            {
                                line = line.Insert(186, "   ");
                            }
                            else
                            {
                                line = line.Remove(186, 3);
                                line = line.Insert(186, cot15_9);
                            }
                            string cot15_10 = Fields[23].Replace("\"", "");
                            if (cot15_10 == "")
                            {
                                line = line.Insert(192, "   ");
                            }
                            else
                            {
                                line = line.Remove(192, 3);
                                line = line.Insert(192, cot15_10);
                            }
                            string cot15_11 = Fields[24].Replace("\"", "");
                            if (cot15_11 == "")
                            {
                                line = line.Insert(198, "   ");
                            }
                            else
                            {
                                line = line.Remove(198, 3);
                                line = line.Insert(198, cot15_11);
                            }
                            string cot15_12 = Fields[25].Replace("\"", "");
                            if (cot15_12 == "")
                            {
                                line = line.Insert(204, "   ");
                            }
                            else
                            {
                                line = line.Remove(204, 3);
                                line = line.Insert(204, cot15_12);
                            }
                            string cot15_13 = Fields[26].Replace("\"", "");
                            if (cot15_13 == "")
                            {
                                line = line.Insert(210, "   ");
                            }
                            else
                            {
                                line = line.Remove(210, 3);
                                line = line.Insert(210, cot15_13);
                            }
                            string cot15_14 = Fields[27].Replace("\"", "");
                            if (cot15_14 == "")
                            {
                                line = line.Insert(216, "   ");
                            }
                            else
                            {
                                line = line.Remove(216, 3);
                                line = line.Insert(216, cot15_14);
                            }
                            string cot15_15 = Fields[28].Replace("\"", "");
                            if (cot15_15 == "")
                            {
                                line = line.Insert(222, "   ");
                            }
                            else
                            {
                                line = line.Remove(222, 3);
                                line = line.Insert(222, cot15_15);
                            }
                            string cot15_16 = Fields[29].Replace("\"", "");
                            if (cot15_16 == "")
                            {
                                line = line.Insert(228, "   ");
                            }
                            else
                            {
                                line = line.Remove(228, 3);
                                line = line.Insert(228, cot15_16);
                            }
                            string cot15_17 = Fields[30].Replace("\"", "");
                            if (cot15_17 == "")
                            {
                                line = line.Insert(234, "   ");
                            }
                            else
                            {
                                line = line.Remove(234, 3);
                                line = line.Insert(234, cot15_17);
                            }
                            string cot15_18 = Fields[31].Replace("\"", "");
                            if (cot15_18 == "")
                            {
                                line = line.Insert(240, "   ");
                            }
                            else
                            {
                                line = line.Remove(240, 3);
                                line = line.Insert(240, cot15_18);
                            }
                            string cot15_19 = Fields[32].Replace("\"", "");
                            if (cot15_19 == "")
                            {
                                line = line.Insert(246, "   ");
                            }
                            else
                            {
                                line = line.Remove(246, 3);
                                line = line.Insert(246, cot15_19);
                            }
                            string cot15_20 = Fields[33].Replace("\"", "");
                            if (cot15_20 == "")
                            {
                                line = line.Insert(252, "   ");
                            }
                            else
                            {
                                line = line.Remove(252, 3);
                                line = line.Insert(252, cot15_20);
                            }
                            string cot15_21 = Fields[34].Replace("\"", "");
                            if (cot15_21 == "")
                            {
                                line = line.Insert(258, "   ");
                            }
                            else
                            {
                                line = line.Remove(258, 3);
                                line = line.Insert(258, cot15_21);
                            }
                            string cot15_22 = Fields[35].Replace("\"", "");
                            if (cot15_22 == "")
                            {
                                line = line.Insert(264, "   ");
                            }
                            else
                            {
                                line = line.Remove(264, 3);
                                line = line.Insert(264, cot15_22);
                            }
                            string cot15_23 = Fields[36].Replace("\"", "");
                            if (cot15_23 == "")
                            {
                                line = line.Insert(270, "   ");
                            }
                            else
                            {
                                line = line.Remove(270, 3);
                                line = line.Insert(270, cot15_23);
                            }
                            string cot15_24 = Fields[37].Replace("\"", "");
                            if (cot15_24 == "")
                            {
                                line = line.Insert(276, "   ");
                            }
                            else
                            {
                                line = line.Remove(276, 3);
                                line = line.Insert(276, cot15_24);
                            }
                            string cot15_25 = Fields[38].Replace("\"", "");
                            if (cot15_25 == "")
                            {
                                line = line.Insert(282, "   ");
                            }
                            else
                            {
                                line = line.Remove(282, 3);
                                line = line.Insert(282, cot15_25);
                            }
                            string cot15_26 = Fields[39].Replace("\"", "");
                            if (cot15_26 == "")
                            {
                                line = line.Insert(288, "   ");
                            }
                            else
                            {
                                line = line.Remove(288, 3);
                                line = line.Insert(288, cot15_26);
                            }
                            string cot15_27 = Fields[40].Replace("\"", "");
                            if (cot15_27 == "")
                            {
                                line = line.Insert(294, "   ");
                            }
                            else
                            {
                                line = line.Remove(294, 3);
                                line = line.Insert(294, cot15_27);
                            }

                            // cột 307 _ 3 ký tự _ thêm 00000
                            //sửa 00000 thành 5 space nếu ko có giá trị, còn có thì giữ nguyên
                            string cot307 = Fields[41].Replace("\"", "");
                            if (cot307 == "")
                            {
                                line = line.Insert(300, "     ");
                            }
                            else
                            {
                                line = line.Remove(300, 5);
                                line = line.Insert(300, cot307);
                            }
                            // cột 311 _ 1 ký tự _ thêm space
                            line = line.Insert(308, " ");
                            // cột 318 _ 7 ký tự _ thêm 0000
                            // tương tự cột 307
                            string cot318 = Fields[43].Replace("\"", "");
                            if (cot318 == "")
                            {
                                line = line.Insert(312, "    ");
                            }
                            else
                            {
                                line = line.Remove(312, 5);
                                line = line.Insert(312, cot318);
                            }
                            // cột 330 _ 9 ký tự _ thêm space
                            line = line.Insert(319, "         ");
                            // cột 335 _ 2 ký tự _ thêm space
                            line = line.Insert(331, "  ");
                            // cột 341 _ 3 ký tự _ thêm space
                            line = line.Insert(336, "   ");
                            // cột 346 _ 2 ký tự _ thêm space
                            line = line.Insert(342, "  ");
                            // cột 352 _ 3 ký tự _ thêm space
                            line = line.Insert(347, "   ");
                            // cột 357 _ 2 ký tự _ thêm space
                            line = line.Insert(353, "  ");
                            // cột 362 _ 2 ký tự _ thêm space
                            line = line.Insert(358, "  ");
                            // cột 415 _ 50 ký tự _ thêm space
                            line = line.Insert(363, "                                                  ");
                            // cột 448 _ 30 ký tự _ thêm space
                            line = line.Insert(416, "                              ");
                            // cột 491 _ 40 ký tự _ thêm space
                            line = line.Insert(449, "                                        ");

                            string cot504 = Fields[54].Replace("\"", "");
                            if (cot504 == "")
                            {
                                line = line.Insert(492, "          ");
                            }
                            else
                            {
                                line = line.Remove(492, 10);
                                line = line.Insert(492, cot504);
                            }
                            string cot517 = Fields[55].Replace("\"", "");
                            if (cot517 == "")
                            {
                                line = line.Insert(505, "          ");
                            }
                            else
                            {
                                line = line.Remove(505, 10);
                                line = line.Insert(505, cot517);
                            }
                            string cot530 = Fields[56].Replace("\"", "");
                            if (cot530 == "")
                            {
                                line = line.Insert(518, "          ");
                            }
                            else
                            {
                                line = line.Remove(518, 10);
                                line = line.Insert(518, cot530);
                            }
                            string cot543 = Fields[57].Replace("\"", "");
                            if (cot543 == "")
                            {
                                line = line.Insert(531, "          ");
                            }
                            else
                            {
                                line = line.Remove(531, 10);
                                line = line.Insert(531, cot543);
                            }
                            string cot556 = Fields[58].Replace("\"", "");
                            if (cot556 == "")
                            {
                                line = line.Insert(544, "          ");
                            }
                            else
                            {
                                line = line.Remove(544, 10);
                                line = line.Insert(544, cot556);
                            }
                            string cot569 = Fields[59].Replace("\"", "");
                            if (cot569 == "")
                            {
                                line = line.Insert(557, "          ");
                            }
                            else
                            {
                                line = line.Remove(557, 10);
                                line = line.Insert(557, cot569);
                            }
                            string cot582 = Fields[60].Replace("\"", "");
                            if (cot582 == "")
                            {
                                line = line.Insert(570, "          ");
                            }
                            else
                            {
                                line = line.Remove(570, 10);
                                line = line.Insert(570, cot582);
                            }
                            string cot595 = Fields[61].Replace("\"", "");
                            if (cot595 == "")
                            {
                                line = line.Insert(583, "          ");
                            }
                            else
                            {
                                line = line.Remove(583, 10);
                                line = line.Insert(583, cot595);
                            }
                            // cột 605 _ 3 ký tự _ thêm space
                            line = line.Insert(600, "   ");
                            // cột 648 _ 40 ký tự _ thêm space
                            line = line.Insert(606, "                                        ");
                            // cột 811 _ 160 ký tự _ thêm space
                            line = line.Insert(649, "                                                                                                                                                                ");
                            // cột 816 _ 2 ký tự _ thêm space
                            line = line.Insert(812, "  ");
                            // cột 821 _ 2 ký tự _ thêm space
                            line = line.Insert(817, "  ");
                            // cột 826 _ 2 ký tự _ thêm space
                            line = line.Insert(822, "  ");
                            // cột 831 _ 2 ký tự _ thêm space
                            line = line.Insert(827, "  ");
                            // cột 836 _ 2 ký tự _ thêm space
                            line = line.Insert(832, "  ");
                            // cột 841 _ 2 ký tự _ thêm space
                            line = line.Insert(837, "  ");
                            // cột 874 _ 30 ký tự _ thêm space
                            line = line.Insert(842, "                              ");
                            // cột 897 _ 20 ký tự _ thêm space
                            line = line.Insert(875, "                    ");
                            // cột 940 _ 40 ký tự _ thêm space
                            line = line.Insert(898, "                                        ");
                            // cột 946 _ 3 ký tự _ thêm space
                            line = line.Insert(941, "   ");
                            // cột 1049 _ 100 ký tự _ thêm space
                            line = line.Insert(947, "                                                                                                    ");
                            // cột 1252 _ 200 ký tự _ thêm space
                            line = line.Insert(1050, "                                                                                                                                                                                                        ");
                            // cột 1258 _ 3 ký tự _ thêm space
                            line = line.Insert(1253, "   ");
                            // cột 1301 _ 40 ký tự _ thêm space
                            line = line.Insert(1259, "                                        ");
                            // cột 1324 _ 20 ký tự _ thêm space
                            line = line.Insert(1302, "                    ");
                            // cột 1329 _ 2 ký tự _ thêm space
                            line = line.Insert(1325, "  ");
                            // cột 1334 _ 2 ký tự _ thêm space
                            line = line.Insert(1330, "  ");
                            // cột 1339 _ 2 ký tự _ thêm space
                            line = line.Insert(1335, "  ");
                            // cột 1344 _ 1 ký tự _ thêm space
                            line = line.Insert(1344, " ");
                            // cột 1348 _ 2 ký tự _ thêm space
                            line = line.Insert(1348, "  ");
                            // cột 1353 _ 25 ký tự _ thêm space lớn
                            line = line.Insert(1353, "　　　　　　　　　　　　　　　　　　　　　　　　　");
                            // cột 1381 _ 25 ký tự _ thêm space lớn
                            line = line.Insert(1381, "　　　　　　　　　　　　　　　　　　　　　　　　　");
                            //line = line.Replace("*", " ");
                            arrOCR_BatchZ[j] = line;  
                        }
                        #endregion
                        if (File.Exists(dir2 + @"\" + batchTemp + ".txt"))
                            File.Delete(dir2 + @"\" + batchTemp + ".txt");
                        File.WriteAllLines(dir2 + @"\" + batchTemp + ".txt", arrOCR_BatchZ, Encoding.GetEncoding("Shift_jis"));
                    }
                    catch { }

                    try
                    {
                        if (File.Exists(dir2 + @"\" + batchname + ".cmp"))
                            File.Delete(dir2 + @"\" + batchname + ".cmp");
                        File.WriteAllLines(dir2 + @"\" + batchname + ".cmp", strCMP, Encoding.GetEncoding("Shift_jis"));
                    }
                    catch { }
                }
            }

            pagesFilePathLength = lsfileUp.Count;
            btnUpImage.Enabled = false;
            int cr = lsfileUp.Count;
            stopWatch = new Stopwatch();
            stopWatch.Start();
            if (cr > 0)
            {
                prbUpImage.Maximum = cr;
                for (int j = 0; j < cr; j++)
                { ThreadPool.QueueUserWorkItem(new WaitCallback(o => ThreadProc2())); }
            }
        }

        private void ThreadProc2()
        {
            string[] strvl = null;
            string strocr = "";
            lock (lsfileUp)
            {
                strvl = lsfileUp[0].Split('|');
                strocr = lstOCR[0];
                lsfileUp.RemoveAt(0);
                lstOCR.RemoveAt(0);
                string stringOCR = "";
                string code = "";
                string tenfile = "";
                tenfile = Path.GetFileName(strvl[0]);
                if (strvl[1] != "2")
                {
                    if (strvl[3][0] == 'C')
                    {
                        string[] arrOCR = strocr.Replace("\"", "").Split(',');
                        code = arrOCR[5];
                        stringOCR = String.Join("|", new string[2] { arrOCR[9].TrimEnd(), arrOCR[15] + arrOCR[16] + arrOCR[17] });
                        if (arrOCR[15] != "")
                        {
                            if (!arrOCR[9].Contains('?') && !arrOCR[15].Contains('?') && !arrOCR[16].Contains('?') && !arrOCR[17].Contains('?'))
                            {
                                dbsql.InsertImage_Kojin(strvl[2], stringOCR, code, tenfile, strvl[3]);
                            }
                            else
                            {
                                dbsql.InsertImage(strvl[2], stringOCR, code, tenfile, strvl[3]);
                            }
                        }
                        else
                        {
                            if (!arrOCR[9].Contains('?') && !arrOCR[16].Contains('?') && !arrOCR[17].Contains('?'))
                            {
                                dbsql.InsertImage_Kojin(strvl[2], stringOCR, code, tenfile, strvl[3]);
                            }
                            else
                            {
                                dbsql.InsertImage(strvl[2], stringOCR, code, tenfile, strvl[3]);
                            }
                        }
                    }
                    else
                    {
                        string[] arrOCR = strocr.Replace("\"", "").Split(',');
                        code = arrOCR[4];
                        stringOCR = String.Join("|", new string[2] { arrOCR[11].TrimEnd(), "" });
                        dbsql.InsertImage(strvl[2], stringOCR, code, tenfile, strvl[3]);
                    }
                    Bitmap imagepage = new Bitmap(strvl[0]);
                    dbsql.InsertImageToServer(tenfile, imageToByteArray(imagepage), strvl[3]);
                }
                else if (strvl[1] == "3")
                { 
                    string[] arrOCR = strocr.Replace("\"", "").Split(',');
                    code = arrOCR[4];
                    dbsql.InsertImage(strvl[2], stringOCR, code, tenfile, strvl[3]);
                    Bitmap imagepage = new Bitmap(strvl[0]);
                    dbsql.InsertImageToServer(tenfile, imageToByteArray(imagepage), strvl[3]);
                }
                else
                {
                    if (strvl[3] == "C")
                    {
                        string[] arrOCR = strocr.Replace("\"", "").Split(',');
                        code = arrOCR[5];
                    }
                    else
                    {
                        string[] arrOCR = strocr.Replace("\"", "").Split(',');
                        code = arrOCR[4];
                    }
                    dbsql.InsertImage(strvl[2], stringOCR, code, tenfile, strvl[3]);
                }
            }
            this.Invoke(new BarDelegate(UpdateBar));
        }
        private void UpdateBar()
        {
            prbUpImage.Value++;
            lblTimeUp.Text = prbUpImage.Value.ToString();
            if (prbUpImage.Value == prbUpImage.Maximum)
            {
                TimeSpan ts = stopWatch.Elapsed;
                // Format and display the TimeSpan value.
                lblTimeUp.Text = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds / 10);
                btnUpImage.Enabled = true;
                string pageUp = dbsql.countImageUp(string.Join(",", lstBatchId));
                lblImageUp.Text = pageUp;
                if (pageUp == countPage.ToString())
                {
                    MessageBox.Show("Completed!");
                }
                else
                {
                    MessageBox.Show("Error!");
                    int countlsBatch = lsBatch.Count;
                    for (int i = 0; i < countlsBatch; i++)
                    {
                        string batchname = Path.GetFileName(lsBatch[i]);
                        DataTable dtBatchId = new DataTable();
                        dtBatchId = dbsql.GetBatchIdByName(batchname);
                        dbsql.DeleteBatch(batchname);
                        dbsql.DeleteTalble(batchname);
                        if (dtBatchId.Rows.Count > 0)
                        {
                            for (int j = 0; j < dtBatchId.Rows.Count; j++)
                            {
                                if (dtBatchId.Rows[j]["TempID"].ToString().Trim() == "1")
                                {
                                    dbsql.DeleteTalble(dtBatchId.Rows[j]["Id"].ToString() + "--" + batchname);
                                }
                                else
                                {
                                    dbsql.DeleteTalble(dtBatchId.Rows[j]["Id"].ToString() + "--" + batchname + "--lv3");
                                }
                            }
                        }
                    }
                }
                dt.Clear();
                var results = dbsql.GetBatch();
                for (int i = 0; i < results.Rows.Count; i++)
                {
                    var row = dt.NewRow();
                    row["Name"] = results.Rows[i][0];
                    row["Id"] = results.Rows[i][1];
                    row["Template"] = arrTemplate[Convert.ToInt16(results.Rows[i][1]) - 1];
                    row["Số lượng"] = results.Rows[i][3];
                    row["Ngày up"] = results.Rows[i][2];
                    dt.Rows.Add(row);
                }
                grThongke.DataSource = null;
                grThongke.DataSource = dt;
                grThongkeV.Columns["Id"].Visible = false;
            }
        }

        private void grThongkeV_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView view = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void frmUpdateImage_Load(object sender, EventArgs e)
        {
            myImageCodecInfo = GetEncoderInfo("image/tiff");
            myEncoder = System.Drawing.Imaging.Encoder.Compression;
            myEncoderParameters = new EncoderParameters(1);
            myEncoderParameter = new EncoderParameter(myEncoder, (long)EncoderValue.CompressionCCITT4);
            myEncoderParameters.Param[0] = myEncoderParameter;
            var results = dbsql.GetBatch();
            for (int i = 0; i < results.Rows.Count; i++)
            {
                var row = dt.NewRow();
                row["Name"] = results.Rows[i][0];
                row["Id"] = results.Rows[i][1];
                row["Template"] = arrTemplate[Convert.ToInt16(results.Rows[i][1]) - 1];
                row["Số lượng"] = results.Rows[i][3];
                row["Ngày up"] = results.Rows[i][2];
                dt.Rows.Add(row);
            }
            grThongke.DataSource = dt;
            grThongkeV.Columns["Id"].Visible = false;
        }

        private void grThongkeV_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {   //click event
                //MessageBox.Show("you got it!");
                ContextMenu contextMenu = new System.Windows.Forms.ContextMenu();
                MenuItem menuItem = new MenuItem("Hide batchs");
                menuItem.Click += new EventHandler(HideAction);
                contextMenu.MenuItems.Add(menuItem);
                MenuItem menuItem1 = new MenuItem("Delete batchs");
                menuItem1.Click += new EventHandler(DeleteAction);
                contextMenu.MenuItems.Add(menuItem1);          
                grThongke.ContextMenu = contextMenu;
            }
        }
        void DeleteAction(object sender, EventArgs e)
        {
            ColumnView View = (ColumnView)grThongke.FocusedView;
            GridColumn column = View.Columns[grThongkeV.FocusedColumn.FieldName];
            if (grThongkeV.SelectedRowsCount == 0)
            {
                return;
            }
            else if (grThongkeV.SelectedRowsCount > 1)
            {
                for (int i = grThongkeV.SelectedRowsCount - 1; i >= 0; i--)
                {
                    int rw = grThongkeV.GetSelectedRows()[i];
                    string batchName = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["Name"]).ToString();
                    string id = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["Id"]).ToString();
                    DataTable dtBatchId = new DataTable();
                    dtBatchId = dbsql.GetBatchIdByName(batchName);
                    dbsql.DeleteBatch(batchName);
                    dbsql.DeleteTalble(batchName);
                    if (dtBatchId.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtBatchId.Rows.Count; j++)
                        {
                            if (dtBatchId.Rows[j]["TempID"].ToString().Trim() == "1")
                            {
                                dbsql.DeleteTalble(dtBatchId.Rows[j]["Id"].ToString() + "--" + batchName);
                            }
                            else
                            {
                                dbsql.DeleteTalble(dtBatchId.Rows[j]["Id"].ToString() + "--" + batchName + "--lv3");
                            }
                        }
                    }
                }
            }
            else
            {
                int rw = grThongkeV.GetSelectedRows()[0];
                string batchName = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["Name"]).ToString();
                DataTable dtBatchId = new DataTable();
                dtBatchId = dbsql.GetBatchIdByName(batchName);
                dbsql.DeleteBatch(batchName);
                dbsql.DeleteTalble(batchName);
                if (dtBatchId.Rows.Count > 0)
                {
                    for (int j = 0; j < dtBatchId.Rows.Count; j++)
                    {
                        if (dtBatchId.Rows[j]["TempID"].ToString().Trim() == "1")
                        {
                            dbsql.DeleteTalble(dtBatchId.Rows[j]["Id"].ToString() + "--" + batchName);
                        }
                        else
                        {
                            dbsql.DeleteTalble(dtBatchId.Rows[j]["Id"].ToString() + "--" + batchName + "--lv3");
                        }
                    }
                }
            }
            SelectBatch();
            MessageBox.Show("Completed!");
        }
        void HideAction(object sender, EventArgs e)
        {
            ColumnView View = (ColumnView)grThongke.FocusedView;
            GridColumn column = View.Columns[grThongkeV.FocusedColumn.FieldName];
            if (grThongkeV.SelectedRowsCount == 0)
            {
                return;
            }
            else if (grThongkeV.SelectedRowsCount > 1)
            {
                for (int i = grThongkeV.SelectedRowsCount - 1; i >= 0; i--)
                {
                    int rw = grThongkeV.GetSelectedRows()[i];
                    string batchName = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["Name"]).ToString();
                    string id = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["Id"]).ToString();
                    dbsql.UpdateHitpointBatch(batchName, id);
                }
            }
            else
            {
                int rw = grThongkeV.GetSelectedRows()[0];
                string batchName = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["Name"]).ToString();
                string id = grThongkeV.GetRowCellValue(rw, grThongkeV.Columns["Id"]).ToString();
                dbsql.UpdateHitpointBatch(batchName, id);
            }
            SelectBatch();
            MessageBox.Show("Completed!");
        }
        void UnHideAction(object sender, EventArgs e)
        {
            dbsql.UpdateHitpointBatchAll();
            SelectBatch();
            MessageBox.Show("Completed!");
        }
        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }
        private void SelectBatch()
        {
            dt.Rows.Clear();
            var results = dbsql.GetBatch();
            for (int i = 0; i < results.Rows.Count; i++)
            {
                var row = dt.NewRow();
                row["Name"] = results.Rows[i][0];
                row["Id"] = results.Rows[i][1];
                row["Template"] = arrTemplate[Convert.ToInt16(results.Rows[i][1]) - 1];
                row["Số lượng"] = results.Rows[i][3];
                row["Ngày up"] = results.Rows[i][2];
                dt.Rows.Add(row);
            }
            grThongke.DataSource = dt;
        }
        private byte[] imageToByteArray(Bitmap imageIn)
        {
            using (var ms = new MemoryStream())
            {
                try
                {
                    imageIn.Save(ms, myImageCodecInfo, myEncoderParameters);
                    return ms.ToArray();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa hết batch không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                DataTable dttable = new DataTable();
                dttable = dbsql.get_table();
                for (int i = 0; i < dttable.Rows.Count; i++)
                {
                    dbsql.Drop_Table(dttable.Rows[i]["Id"].ToString() + "--" + dttable.Rows[i]["Name"].ToString());
                    dbsql.Drop_Table(dttable.Rows[i]["Id"].ToString() + "--" + dttable.Rows[i]["Name"].ToString() + "--lv3");
                    dbsql.Drop_Table(dttable.Rows[i]["Name"].ToString());
                }
                dbsql.Delete_AllBatch();
            }
            SelectBatch();
        }

        private void frmUpdateImage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void grThongkeV_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {

        }

        private void grvFolderUp_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {

        }

        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }
    }
}
