﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VCB_KOMOSI
{
    class FieldData
    {
        private int fieldId;
        public int FieldId
        {
            get { return fieldId; }
            set { fieldId = value; }
        }

        private string[] points;
        public string[] Points
        {
            get { return points; }
            set { points = value; }
        }

        public FieldData()
        {
            // TODO: Complete member initialization
        }

        public FieldData(int fieldId, string[] points)
        {
            this.fieldId = fieldId;
            this.points = points;
        }
    }
}
