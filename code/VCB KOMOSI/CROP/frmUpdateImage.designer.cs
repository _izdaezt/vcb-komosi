﻿namespace VCB_KOMOSI
{
    partial class frmUpdateImage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUpdateImage));
            this.btnSourceDirectoryBrow = new System.Windows.Forms.Button();
            this.txtSourceDirectory = new System.Windows.Forms.TextBox();
            this.lblSourceDirectoryBrow = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPage = new System.Windows.Forms.Label();
            this.btnUpImage = new System.Windows.Forms.Button();
            this.prbUpImage = new System.Windows.Forms.ProgressBar();
            this.lblTimeUp = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.grThongke = new DevExpress.XtraGrid.GridControl();
            this.grThongkeV = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.grcFolderUP = new DevExpress.XtraGrid.GridControl();
            this.grvFolderUp = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.lblImageUp = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grThongke)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grThongkeV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcFolderUP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvFolderUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSourceDirectoryBrow
            // 
            this.btnSourceDirectoryBrow.Location = new System.Drawing.Point(723, 18);
            this.btnSourceDirectoryBrow.Name = "btnSourceDirectoryBrow";
            this.btnSourceDirectoryBrow.Size = new System.Drawing.Size(75, 36);
            this.btnSourceDirectoryBrow.TabIndex = 18;
            this.btnSourceDirectoryBrow.Text = "Browse...";
            this.btnSourceDirectoryBrow.UseVisualStyleBackColor = true;
            this.btnSourceDirectoryBrow.Click += new System.EventHandler(this.btnSourceDirectoryBrow_Click);
            // 
            // txtSourceDirectory
            // 
            this.txtSourceDirectory.Enabled = false;
            this.txtSourceDirectory.Location = new System.Drawing.Point(6, 27);
            this.txtSourceDirectory.Name = "txtSourceDirectory";
            this.txtSourceDirectory.Size = new System.Drawing.Size(711, 20);
            this.txtSourceDirectory.TabIndex = 17;
            // 
            // lblSourceDirectoryBrow
            // 
            this.lblSourceDirectoryBrow.AutoSize = true;
            this.lblSourceDirectoryBrow.BackColor = System.Drawing.Color.Transparent;
            this.lblSourceDirectoryBrow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSourceDirectoryBrow.Location = new System.Drawing.Point(2, 5);
            this.lblSourceDirectoryBrow.Name = "lblSourceDirectoryBrow";
            this.lblSourceDirectoryBrow.Size = new System.Drawing.Size(101, 20);
            this.lblSourceDirectoryBrow.TabIndex = 16;
            this.lblSourceDirectoryBrow.Text = "Thư mục ảnh";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 30;
            this.label2.Text = "Pages:";
            // 
            // lblPage
            // 
            this.lblPage.AutoSize = true;
            this.lblPage.BackColor = System.Drawing.Color.Transparent;
            this.lblPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPage.Location = new System.Drawing.Point(95, 58);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(18, 20);
            this.lblPage.TabIndex = 31;
            this.lblPage.Text = "0";
            // 
            // btnUpImage
            // 
            this.btnUpImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpImage.Location = new System.Drawing.Point(563, 85);
            this.btnUpImage.Name = "btnUpImage";
            this.btnUpImage.Size = new System.Drawing.Size(100, 53);
            this.btnUpImage.TabIndex = 33;
            this.btnUpImage.Text = "Start";
            this.btnUpImage.UseVisualStyleBackColor = true;
            this.btnUpImage.Click += new System.EventHandler(this.btnUpImage_Click);
            // 
            // prbUpImage
            // 
            this.prbUpImage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.prbUpImage.Location = new System.Drawing.Point(6, 145);
            this.prbUpImage.Name = "prbUpImage";
            this.prbUpImage.Size = new System.Drawing.Size(788, 11);
            this.prbUpImage.TabIndex = 34;
            // 
            // lblTimeUp
            // 
            this.lblTimeUp.AutoSize = true;
            this.lblTimeUp.BackColor = System.Drawing.Color.Transparent;
            this.lblTimeUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimeUp.Location = new System.Drawing.Point(93, 119);
            this.lblTimeUp.Name = "lblTimeUp";
            this.lblTimeUp.Size = new System.Drawing.Size(18, 20);
            this.lblTimeUp.TabIndex = 36;
            this.lblTimeUp.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 20);
            this.label3.TabIndex = 35;
            this.label3.Text = "Time up:";
            // 
            // grThongke
            // 
            this.grThongke.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            gridLevelNode1.RelationName = "Level1";
            this.grThongke.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.grThongke.Location = new System.Drawing.Point(331, 162);
            this.grThongke.MainView = this.grThongkeV;
            this.grThongke.Name = "grThongke";
            this.grThongke.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2});
            this.grThongke.Size = new System.Drawing.Size(463, 403);
            this.grThongke.TabIndex = 37;
            this.grThongke.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grThongkeV});
            // 
            // grThongkeV
            // 
            this.grThongkeV.Appearance.ColumnFilterButton.Options.UseTextOptions = true;
            this.grThongkeV.Appearance.ColumnFilterButton.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grThongkeV.Appearance.Row.Options.UseTextOptions = true;
            this.grThongkeV.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grThongkeV.Appearance.ViewCaption.Options.UseTextOptions = true;
            this.grThongkeV.Appearance.ViewCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grThongkeV.GridControl = this.grThongke;
            this.grThongkeV.Name = "grThongkeV";
            this.grThongkeV.OptionsBehavior.Editable = false;
            this.grThongkeV.OptionsFilter.DefaultFilterEditorView = DevExpress.XtraEditors.FilterEditorViewMode.VisualAndText;
            this.grThongkeV.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.grThongkeV.OptionsFilter.UseNewCustomFilterDialog = true;
            this.grThongkeV.OptionsSelection.MultiSelect = true;
            this.grThongkeV.OptionsView.ShowGroupPanel = false;
            this.grThongkeV.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grThongkeV_CustomDrawRowIndicator);
            this.grThongkeV.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.grThongkeV_RowStyle);
            this.grThongkeV.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grThongkeV_MouseUp);
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(683, 84);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(111, 53);
            this.btnDelete.TabIndex = 38;
            this.btnDelete.Text = "Delete All";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // grcFolderUP
            // 
            this.grcFolderUP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            gridLevelNode2.RelationName = "Level1";
            this.grcFolderUP.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.grcFolderUP.Location = new System.Drawing.Point(6, 162);
            this.grcFolderUP.MainView = this.grvFolderUp;
            this.grcFolderUP.Name = "grcFolderUP";
            this.grcFolderUP.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox3,
            this.repositoryItemComboBox4});
            this.grcFolderUP.Size = new System.Drawing.Size(319, 403);
            this.grcFolderUP.TabIndex = 43;
            this.grcFolderUP.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvFolderUp});
            // 
            // grvFolderUp
            // 
            this.grvFolderUp.GridControl = this.grcFolderUP;
            this.grvFolderUp.Name = "grvFolderUp";
            this.grvFolderUp.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.grvFolderUp.OptionsBehavior.AllowPartialRedrawOnScrolling = false;
            this.grvFolderUp.OptionsBehavior.Editable = false;
            this.grvFolderUp.OptionsBehavior.ReadOnly = true;
            this.grvFolderUp.OptionsCustomization.AllowColumnMoving = false;
            this.grvFolderUp.OptionsCustomization.AllowFilter = false;
            this.grvFolderUp.OptionsCustomization.AllowGroup = false;
            this.grvFolderUp.OptionsCustomization.AllowSort = false;
            this.grvFolderUp.OptionsFilter.AllowColumnMRUFilterList = false;
            this.grvFolderUp.OptionsFilter.AllowFilterEditor = false;
            this.grvFolderUp.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.grvFolderUp.OptionsFilter.AllowMRUFilterList = false;
            this.grvFolderUp.OptionsFilter.DefaultFilterEditorView = DevExpress.XtraEditors.FilterEditorViewMode.VisualAndText;
            this.grvFolderUp.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.grvFolderUp.OptionsFilter.UseNewCustomFilterDialog = true;
            this.grvFolderUp.OptionsFind.AllowFindPanel = false;
            this.grvFolderUp.OptionsSelection.MultiSelect = true;
            this.grvFolderUp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grvFolderUp.OptionsView.ShowFooter = true;
            this.grvFolderUp.OptionsView.ShowGroupPanel = false;
            this.grvFolderUp.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.grvFolderUp_RowStyle);
            // 
            // repositoryItemComboBox3
            // 
            this.repositoryItemComboBox3.Name = "repositoryItemComboBox3";
            // 
            // repositoryItemComboBox4
            // 
            this.repositoryItemComboBox4.Name = "repositoryItemComboBox4";
            // 
            // lblImageUp
            // 
            this.lblImageUp.AutoSize = true;
            this.lblImageUp.BackColor = System.Drawing.Color.Transparent;
            this.lblImageUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImageUp.Location = new System.Drawing.Point(94, 90);
            this.lblImageUp.Name = "lblImageUp";
            this.lblImageUp.Size = new System.Drawing.Size(18, 20);
            this.lblImageUp.TabIndex = 45;
            this.lblImageUp.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 20);
            this.label4.TabIndex = 44;
            this.label4.Text = "Images up:";
            // 
            // frmUpdateImage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 568);
            this.Controls.Add(this.lblImageUp);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.grcFolderUP);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.grThongke);
            this.Controls.Add(this.lblTimeUp);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.prbUpImage);
            this.Controls.Add(this.btnUpImage);
            this.Controls.Add(this.lblPage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSourceDirectoryBrow);
            this.Controls.Add(this.txtSourceDirectory);
            this.Controls.Add(this.lblSourceDirectoryBrow);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmUpdateImage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Up Image";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmUpdateImage_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmUpdateImage_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.grThongke)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grThongkeV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcFolderUP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvFolderUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btnSourceDirectoryBrow;
        internal System.Windows.Forms.TextBox txtSourceDirectory;
        internal System.Windows.Forms.Label lblSourceDirectoryBrow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPage;
        private System.Windows.Forms.Button btnUpImage;
        private System.Windows.Forms.ProgressBar prbUpImage;
        private System.Windows.Forms.Label lblTimeUp;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraGrid.GridControl grThongke;
        private DevExpress.XtraGrid.Views.Grid.GridView grThongkeV;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private System.Windows.Forms.Button btnDelete;
        private DevExpress.XtraGrid.GridControl grcFolderUP;
        private DevExpress.XtraGrid.Views.Grid.GridView grvFolderUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox3;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox4;
        private System.Windows.Forms.Label lblImageUp;
        private System.Windows.Forms.Label label4;
    }
}