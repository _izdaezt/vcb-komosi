﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace VCB_KOMOSI
{
    class filestif
    {
        private List<string> filenames = new List<string>();
        public List<string> Filenames
        {
            get { return filenames; }
            set { filenames = value; }
        }
        private string[] batchnames;
        public string[] Batchnames
        {
            get { return batchnames; }
            set { batchnames = value; }
        }
        private string[] strOCR;
        public string[] StrOCR
        {
            get { return strOCR; }
            set { strOCR = value; }
        }
        private int[] batchids;
        public int[] Batchids
        {
            get { return batchids; }
            set { batchids = value; }
        }
        private int[] batchids_lv3;
        public int[] Batchids_lv3
        {
            get { return batchids_lv3; }
            set { batchids_lv3 = value; }
        }  
      
    }
}
