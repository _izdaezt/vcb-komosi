﻿namespace VCB_KOMOSI
{
    partial class FormLC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLC));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.imageViewerTR1 = new ImageViewerTR.ImageViewerTR();
            this.txtsearch = new System.Windows.Forms.TextBox();
            this.btnNouhin = new System.Windows.Forms.Button();
            this.btnImportExcel = new System.Windows.Forms.Button();
            this.chkComboSubField = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.btnView = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.pictureBox2 = new System.Windows.Forms.ToolStripButton();
            this.btnSaveAs = new System.Windows.Forms.ToolStripButton();
            this.btnrotateRight = new System.Windows.Forms.ToolStripButton();
            this.btnrotateLeft = new System.Windows.Forms.ToolStripButton();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.txtstatus = new System.Windows.Forms.TextBox();
            this.txtFind = new System.Windows.Forms.TextBox();
            this.lblFind = new System.Windows.Forms.Label();
            this.rtxtKetqua = new System.Windows.Forms.RichTextBox();
            this.btnFindP = new System.Windows.Forms.Button();
            this.btnFindN = new System.Windows.Forms.Button();
            this.rtxtError = new System.Windows.Forms.TextBox();
            this.grThongke = new DevExpress.XtraGrid.GridControl();
            this.grThongkeV = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.btnViewResult = new System.Windows.Forms.Button();
            this.cboimage = new System.Windows.Forms.ComboBox();
            this.bgwLoadExel = new System.ComponentModel.BackgroundWorker();
            this.bgwForm2 = new System.ComponentModel.BackgroundWorker();
            this.bgwTxt = new System.ComponentModel.BackgroundWorker();
            this.bgwTouan = new System.ComponentModel.BackgroundWorker();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.btnExit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblBatch = new System.Windows.Forms.Label();
            this.lbltemplate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbltxt = new System.Windows.Forms.Label();
            this.bgwTxt2 = new System.ComponentModel.BackgroundWorker();
            this.bgwForm22 = new System.ComponentModel.BackgroundWorker();
            this.bgwTouan2 = new System.ComponentModel.BackgroundWorker();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkComboSubField.Properties)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grThongke)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grThongkeV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 27);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.button1);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.imageViewerTR1);
            this.splitContainer1.Panel1.Controls.Add(this.txtsearch);
            this.splitContainer1.Panel1.Controls.Add(this.btnNouhin);
            this.splitContainer1.Panel1.Controls.Add(this.btnImportExcel);
            this.splitContainer1.Panel1.Controls.Add(this.chkComboSubField);
            this.splitContainer1.Panel1.Controls.Add(this.btnView);
            this.splitContainer1.Panel1.Controls.Add(this.lblError);
            this.splitContainer1.Panel1.Controls.Add(this.toolStrip1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel2.Controls.Add(this.grThongke);
            this.splitContainer1.Size = new System.Drawing.Size(912, 447);
            this.splitContainer1.SplitterDistance = 184;
            this.splitContainer1.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(759, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 53;
            this.button1.Text = "Sort check";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(353, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 20);
            this.label5.TabIndex = 40;
            this.label5.Text = "Search";
            // 
            // imageViewerTR1
            // 
            this.imageViewerTR1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.imageViewerTR1.BackColor = System.Drawing.Color.LightGray;
            this.imageViewerTR1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imageViewerTR1.CurrentZoom = 1F;
            this.imageViewerTR1.Image = null;
            this.imageViewerTR1.Location = new System.Drawing.Point(3, 30);
            this.imageViewerTR1.MaxZoom = 20F;
            this.imageViewerTR1.MinZoom = 0.05F;
            this.imageViewerTR1.Name = "imageViewerTR1";
            this.imageViewerTR1.Size = new System.Drawing.Size(906, 148);
            this.imageViewerTR1.TabIndex = 52;
            // 
            // txtsearch
            // 
            this.txtsearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsearch.Location = new System.Drawing.Point(418, -3);
            this.txtsearch.Name = "txtsearch";
            this.txtsearch.Size = new System.Drawing.Size(335, 26);
            this.txtsearch.TabIndex = 39;
            this.txtsearch.TextChanged += new System.EventHandler(this.txtsearch_TextChanged);
            // 
            // btnNouhin
            // 
            this.btnNouhin.Location = new System.Drawing.Point(280, 1);
            this.btnNouhin.Name = "btnNouhin";
            this.btnNouhin.Size = new System.Drawing.Size(49, 23);
            this.btnNouhin.TabIndex = 51;
            this.btnNouhin.Text = "Nouhin";
            this.btnNouhin.UseVisualStyleBackColor = true;
            this.btnNouhin.Click += new System.EventHandler(this.btnNouhin_Click);
            // 
            // btnImportExcel
            // 
            this.btnImportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImportExcel.Location = new System.Drawing.Point(813, -1);
            this.btnImportExcel.Name = "btnImportExcel";
            this.btnImportExcel.Size = new System.Drawing.Size(99, 25);
            this.btnImportExcel.TabIndex = 50;
            this.btnImportExcel.Text = "Import Excel";
            this.btnImportExcel.UseVisualStyleBackColor = true;
            this.btnImportExcel.Click += new System.EventHandler(this.btnImportExcel_Click);
            // 
            // chkComboSubField
            // 
            this.chkComboSubField.Location = new System.Drawing.Point(106, 2);
            this.chkComboSubField.Name = "chkComboSubField";
            this.chkComboSubField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chkComboSubField.Size = new System.Drawing.Size(110, 20);
            this.chkComboSubField.TabIndex = 48;
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(222, 1);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(52, 23);
            this.btnView.TabIndex = 47;
            this.btnView.Text = "View";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(282, 30);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 17);
            this.lblError.TabIndex = 25;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pictureBox2,
            this.btnSaveAs,
            this.btnrotateRight,
            this.btnrotateLeft});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(912, 25);
            this.toolStrip1.TabIndex = 22;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // pictureBox2
            // 
            this.pictureBox2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(23, 22);
            this.pictureBox2.ToolTipText = "Save lastcheck";
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSaveAs.Image = global::VCBHL_Entry.Properties.Resources.excel_8;
            this.btnSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.Size = new System.Drawing.Size(23, 22);
            this.btnSaveAs.ToolTipText = "Save Excel";
            this.btnSaveAs.Click += new System.EventHandler(this.btnSaveAs_Click);
            // 
            // btnrotateRight
            // 
            this.btnrotateRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnrotateRight.Image = ((System.Drawing.Image)(resources.GetObject("btnrotateRight.Image")));
            this.btnrotateRight.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnrotateRight.Name = "btnrotateRight";
            this.btnrotateRight.Size = new System.Drawing.Size(23, 22);
            this.btnrotateRight.ToolTipText = "Rotate right";
            this.btnrotateRight.Click += new System.EventHandler(this.btnrotateRight_Click);
            // 
            // btnrotateLeft
            // 
            this.btnrotateLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnrotateLeft.Image = ((System.Drawing.Image)(resources.GetObject("btnrotateLeft.Image")));
            this.btnrotateLeft.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnrotateLeft.Name = "btnrotateLeft";
            this.btnrotateLeft.Size = new System.Drawing.Size(23, 22);
            this.btnrotateLeft.Text = "toolStripButton2";
            this.btnrotateLeft.ToolTipText = "Rotate left";
            this.btnrotateLeft.Click += new System.EventHandler(this.btnrotateLeft_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.txtstatus);
            this.splitContainer2.Panel1.Controls.Add(this.txtFind);
            this.splitContainer2.Panel1.Controls.Add(this.lblFind);
            this.splitContainer2.Panel1.Controls.Add(this.rtxtKetqua);
            this.splitContainer2.Panel1.Controls.Add(this.btnFindP);
            this.splitContainer2.Panel1.Controls.Add(this.btnFindN);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.rtxtError);
            this.splitContainer2.Size = new System.Drawing.Size(912, 259);
            this.splitContainer2.SplitterDistance = 385;
            this.splitContainer2.TabIndex = 53;
            this.splitContainer2.Visible = false;
            // 
            // txtstatus
            // 
            this.txtstatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtstatus.Enabled = false;
            this.txtstatus.Location = new System.Drawing.Point(5, 236);
            this.txtstatus.Name = "txtstatus";
            this.txtstatus.Size = new System.Drawing.Size(377, 20);
            this.txtstatus.TabIndex = 4;
            // 
            // txtFind
            // 
            this.txtFind.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFind.Location = new System.Drawing.Point(67, 3);
            this.txtFind.Name = "txtFind";
            this.txtFind.Size = new System.Drawing.Size(240, 26);
            this.txtFind.TabIndex = 5;
            this.txtFind.TextChanged += new System.EventHandler(this.txtFind_TextChanged);
            // 
            // lblFind
            // 
            this.lblFind.AutoSize = true;
            this.lblFind.BackColor = System.Drawing.SystemColors.Control;
            this.lblFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFind.Location = new System.Drawing.Point(3, 9);
            this.lblFind.Name = "lblFind";
            this.lblFind.Size = new System.Drawing.Size(62, 13);
            this.lblFind.TabIndex = 54;
            this.lblFind.Text = "Find what";
            // 
            // rtxtKetqua
            // 
            this.rtxtKetqua.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtxtKetqua.BackColor = System.Drawing.SystemColors.Window;
            this.rtxtKetqua.EnableAutoDragDrop = true;
            this.rtxtKetqua.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtKetqua.Location = new System.Drawing.Point(2, 30);
            this.rtxtKetqua.Name = "rtxtKetqua";
            this.rtxtKetqua.ReadOnly = true;
            this.rtxtKetqua.Size = new System.Drawing.Size(380, 204);
            this.rtxtKetqua.TabIndex = 2;
            this.rtxtKetqua.Text = "";
            this.rtxtKetqua.Visible = false;
            this.rtxtKetqua.WordWrap = false;
            this.rtxtKetqua.SelectionChanged += new System.EventHandler(this.rtxtKetqua_SelectionChanged);
            this.rtxtKetqua.MouseClick += new System.Windows.Forms.MouseEventHandler(this.rtxtKetqua_MouseClick);
            // 
            // btnFindP
            // 
            this.btnFindP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindP.Location = new System.Drawing.Point(345, 3);
            this.btnFindP.Name = "btnFindP";
            this.btnFindP.Size = new System.Drawing.Size(36, 28);
            this.btnFindP.TabIndex = 53;
            this.btnFindP.Text = "<";
            this.btnFindP.UseVisualStyleBackColor = true;
            this.btnFindP.Click += new System.EventHandler(this.btnFindP_Click);
            // 
            // btnFindN
            // 
            this.btnFindN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindN.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindN.Location = new System.Drawing.Point(309, 3);
            this.btnFindN.Name = "btnFindN";
            this.btnFindN.Size = new System.Drawing.Size(36, 28);
            this.btnFindN.TabIndex = 52;
            this.btnFindN.Text = ">";
            this.btnFindN.UseVisualStyleBackColor = true;
            this.btnFindN.Click += new System.EventHandler(this.btnFindN_Click);
            // 
            // rtxtError
            // 
            this.rtxtError.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtxtError.Location = new System.Drawing.Point(4, 3);
            this.rtxtError.Multiline = true;
            this.rtxtError.Name = "rtxtError";
            this.rtxtError.Size = new System.Drawing.Size(516, 253);
            this.rtxtError.TabIndex = 0;
            this.rtxtError.Click += new System.EventHandler(this.rtxtError_Click);
            this.rtxtError.KeyUp += new System.Windows.Forms.KeyEventHandler(this.rtxtError_KeyUp);
            // 
            // grThongke
            // 
            this.grThongke.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            gridLevelNode1.RelationName = "Level1";
            this.grThongke.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.grThongke.Location = new System.Drawing.Point(0, 3);
            this.grThongke.MainView = this.grThongkeV;
            this.grThongke.Name = "grThongke";
            this.grThongke.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2});
            this.grThongke.Size = new System.Drawing.Size(912, 235);
            this.grThongke.TabIndex = 1;
            this.grThongke.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grThongkeV});
            // 
            // grThongkeV
            // 
            this.grThongkeV.GridControl = this.grThongke;
            this.grThongkeV.Name = "grThongkeV";
            this.grThongkeV.OptionsFilter.DefaultFilterEditorView = DevExpress.XtraEditors.FilterEditorViewMode.VisualAndText;
            this.grThongkeV.OptionsFilter.ShowAllTableValuesInFilterPopup = true;
            this.grThongkeV.OptionsFilter.UseNewCustomFilterDialog = true;
            this.grThongkeV.OptionsSelection.MultiSelect = true;
            this.grThongkeV.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.grThongkeV.OptionsView.ColumnAutoWidth = false;
            this.grThongkeV.OptionsView.ShowGroupPanel = false;
            this.grThongkeV.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.grThongkeV_RowCellClick);
            this.grThongkeV.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grThongkeV_CustomDrawRowIndicator);
            this.grThongkeV.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.grThongkeV_RowCellStyle);
            this.grThongkeV.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.grThongkeV_RowStyle);
            this.grThongkeV.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.grThongkeV_SelectionChanged);
            this.grThongkeV.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.grThongkeV_CellValueChanged);
            this.grThongkeV.KeyDown += new System.Windows.Forms.KeyEventHandler(this.grThongkeV_KeyDown);
            this.grThongkeV.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grThongkeV_MouseUp);
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // btnViewResult
            // 
            this.btnViewResult.Location = new System.Drawing.Point(311, 0);
            this.btnViewResult.Name = "btnViewResult";
            this.btnViewResult.Size = new System.Drawing.Size(66, 25);
            this.btnViewResult.TabIndex = 50;
            this.btnViewResult.Text = "View Txt";
            this.btnViewResult.UseVisualStyleBackColor = true;
            this.btnViewResult.Click += new System.EventHandler(this.btnViewResult_Click);
            // 
            // cboimage
            // 
            this.cboimage.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboimage.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboimage.DisplayMember = "Page";
            this.cboimage.FormattingEnabled = true;
            this.cboimage.Location = new System.Drawing.Point(64, 1);
            this.cboimage.Name = "cboimage";
            this.cboimage.Size = new System.Drawing.Size(241, 21);
            this.cboimage.TabIndex = 23;
            this.cboimage.SelectedValueChanged += new System.EventHandler(this.cboimage_SelectedValueChanged);
            // 
            // bgwLoadExel
            // 
            this.bgwLoadExel.WorkerReportsProgress = true;
            this.bgwLoadExel.WorkerSupportsCancellation = true;
            this.bgwLoadExel.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwLoadExel_DoWork);
            this.bgwLoadExel.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwLoadExel_RunWorkerCompleted);
            // 
            // bgwForm2
            // 
            this.bgwForm2.WorkerReportsProgress = true;
            this.bgwForm2.WorkerSupportsCancellation = true;
            this.bgwForm2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwForm2_DoWork);
            this.bgwForm2.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwTxt_ProgressChanged);
            this.bgwForm2.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwTxt_RunWorkerCompleted);
            // 
            // bgwTxt
            // 
            this.bgwTxt.WorkerReportsProgress = true;
            this.bgwTxt.WorkerSupportsCancellation = true;
            this.bgwTxt.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwTxt_DoWork);
            this.bgwTxt.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwTxt_ProgressChanged);
            this.bgwTxt.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwTxt_RunWorkerCompleted);
            // 
            // bgwTouan
            // 
            this.bgwTouan.WorkerReportsProgress = true;
            this.bgwTouan.WorkerSupportsCancellation = true;
            this.bgwTouan.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwTouan_DoWork);
            this.bgwTouan.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwTxt_ProgressChanged);
            this.bgwTouan.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwTxt_RunWorkerCompleted);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this.toolStripMenuItem1.Size = new System.Drawing.Size(139, 22);
            this.toolStripMenuItem1.Text = "Save As";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.X)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(912, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Location = new System.Drawing.Point(884, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(25, 23);
            this.btnExit.TabIndex = 50;
            this.btnExit.Text = "X";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label1.Location = new System.Drawing.Point(511, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = "Batch:";
            // 
            // lblBatch
            // 
            this.lblBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBatch.AutoSize = true;
            this.lblBatch.Location = new System.Drawing.Point(555, 6);
            this.lblBatch.Name = "lblBatch";
            this.lblBatch.Size = new System.Drawing.Size(49, 13);
            this.lblBatch.TabIndex = 52;
            this.lblBatch.Text = "              ";
            // 
            // lbltemplate
            // 
            this.lbltemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbltemplate.AutoSize = true;
            this.lbltemplate.Location = new System.Drawing.Point(821, 6);
            this.lbltemplate.Name = "lbltemplate";
            this.lbltemplate.Size = new System.Drawing.Size(49, 13);
            this.lbltemplate.TabIndex = 54;
            this.lbltemplate.Text = "              ";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label3.Location = new System.Drawing.Point(764, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 53;
            this.label3.Text = "Template:";
            // 
            // lbltxt
            // 
            this.lbltxt.AutoSize = true;
            this.lbltxt.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.lbltxt.ForeColor = System.Drawing.Color.Red;
            this.lbltxt.Location = new System.Drawing.Point(383, 5);
            this.lbltxt.Name = "lbltxt";
            this.lbltxt.Size = new System.Drawing.Size(0, 13);
            this.lbltxt.TabIndex = 55;
            // 
            // bgwTxt2
            // 
            this.bgwTxt2.WorkerReportsProgress = true;
            this.bgwTxt2.WorkerSupportsCancellation = true;
            this.bgwTxt2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwTxt2_DoWork);
            this.bgwTxt2.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwTxt_ProgressChanged);
            this.bgwTxt2.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwTxt_RunWorkerCompleted);
            // 
            // bgwForm22
            // 
            this.bgwForm22.WorkerReportsProgress = true;
            this.bgwForm22.WorkerSupportsCancellation = true;
            this.bgwForm22.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwForm22_DoWork);
            this.bgwForm22.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwTxt_ProgressChanged);
            this.bgwForm22.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwTxt_RunWorkerCompleted);
            // 
            // bgwTouan2
            // 
            this.bgwTouan2.WorkerReportsProgress = true;
            this.bgwTouan2.WorkerSupportsCancellation = true;
            this.bgwTouan2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwTouan2_DoWork);
            this.bgwTouan2.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwTxt_ProgressChanged);
            this.bgwTouan2.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwTxt_RunWorkerCompleted);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(383, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 18);
            this.label2.TabIndex = 57;
            this.label2.Text = "Ký tự đặt biệt:";
            // 
            // lblCount
            // 
            this.lblCount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblCount.AutoSize = true;
            this.lblCount.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.lblCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.ForeColor = System.Drawing.Color.Blue;
            this.lblCount.Location = new System.Drawing.Point(483, 3);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(16, 18);
            this.lblCount.TabIndex = 58;
            this.lblCount.Text = "0";
            // 
            // FormLC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(912, 475);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbltxt);
            this.Controls.Add(this.lbltemplate);
            this.Controls.Add(this.lblBatch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnViewResult);
            this.Controls.Add(this.cboimage);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.Name = "FormLC";
            this.Text = "FormLC";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmThongke_FormClosing);
            this.Load += new System.EventHandler(this.frmLastCheck_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormLC_KeyDown);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkComboSubField.Properties)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grThongke)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grThongkeV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraGrid.GridControl grThongke;
        private DevExpress.XtraGrid.Views.Grid.GridView grThongkeV;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton pictureBox2;
        private System.Windows.Forms.ToolStripButton btnSaveAs;
        private System.Windows.Forms.ToolStripButton btnrotateRight;
        private System.Windows.Forms.ToolStripButton btnrotateLeft;
        private System.Windows.Forms.ComboBox cboimage;
        private System.Windows.Forms.Label lblError;
        private System.ComponentModel.BackgroundWorker bgwLoadExel;
        private System.Windows.Forms.Button btnView;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkComboSubField;
        private System.Windows.Forms.Button btnViewResult;
        private System.Windows.Forms.RichTextBox rtxtKetqua;
        private System.ComponentModel.BackgroundWorker bgwForm2;
        private System.ComponentModel.BackgroundWorker bgwTxt;
        private System.ComponentModel.BackgroundWorker bgwTouan;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.TextBox txtstatus;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblBatch;
        private System.Windows.Forms.Label lbltemplate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnImportExcel;
        private System.Windows.Forms.Button btnNouhin;
        private ImageViewerTR.ImageViewerTR imageViewerTR1;
        private System.Windows.Forms.Label lbltxt;
        private System.ComponentModel.BackgroundWorker bgwTxt2;
        private System.ComponentModel.BackgroundWorker bgwForm22;
        private System.ComponentModel.BackgroundWorker bgwTouan2;
        private System.Windows.Forms.TextBox txtFind;
        private System.Windows.Forms.Button btnFindP;
        private System.Windows.Forms.Button btnFindN;
        private System.Windows.Forms.Label lblFind;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TextBox rtxtError;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtsearch;
        private System.Windows.Forms.Button button1;
    }
}