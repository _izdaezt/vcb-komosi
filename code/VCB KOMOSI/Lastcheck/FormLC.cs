﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Base;
using System.IO;
using DevExpress.XtraGrid.Views.Grid;
using System.Globalization;
using System.Text.RegularExpressions;
using DevExpress.XtraGrid.Columns;

namespace VCB_KOMOSI
{
    public partial class FormLC : Form
    {
        bool disableExportNouhin1 = false;
           bool disableExportNouhin3 = false;
        int tempid = 0;
        #region variable
        public static string datestring = "";
        const string quote = "\"";
        //class workDB
        WorkDB_LC workdb = new WorkDB_LC();
        string[] arrAlphabet = new string[26] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

        //Số row của dgvthongke
        int row = 0;
        //Crop
        public static bool save = false;
        public static bool db = false;
        public static bool exit = false;
        public static string batchname;
        public static int batchID;
        public static int formID;
        public static string formName;
        public static int userID;
        DataTable dtTxt = new DataTable();
        DataTable dt = new DataTable();
        DataTable LV1_Kojin, LV3_Kojin_Kana, LV1_Zokusei2, LV1_Zokusei3;
        Bitmap bitmapTmp;
        public static string imgLocation;
        DevExpress.XtraGrid.Columns.GridColumn cl = new DevExpress.XtraGrid.Columns.GridColumn();
        int rw;
        public static int LastcheckID;
        public static DataTable dtbatch;
        DataTable dtexcel;
        public static string nameForm;
        DataTable exportBatch = new DataTable();
        string folderImage = "";
        List<string> listclname;
        string phanloai = "";
        string[] Lines;
        string strOCR = "";
        List<string> idrow = new List<string>();
        float vlfl = 11.75F;
        byte[] arrbyteCMP = null;
        int vlchange1;
        int vlchange2;
        DataTable dtsave;
        string[] arrcode;
        int countCompleted = 0;
        List<int[]> lsint;
        string tenanh = "";
        string batchnameLocal;
        DateTime datebefore;
        DateTime datebeforeN;
        bool blImport;
        string filenameTxt;
        string tableform;
        string tableformtemp;
        DataTable dtClone = new DataTable();
        int QA1 = 0, QA3 = 0, IMPORT1 = 0, IMPORT3 = 0;
        List<string> lstSaveZ = new List<string>();
        #endregion
        public FormLC()
        {
            InitializeComponent();
            this.CenterToScreen();
            grThongkeV.IndicatorWidth = 40;
            folderImage = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\ImageKomosi";
            if (Directory.Exists(folderImage))
            {
                try
                {
                    Directory.Delete(folderImage, true);
                    Directory.CreateDirectory(folderImage);
                }
                catch { }
            }
            else
            {
                Directory.CreateDirectory(folderImage);
            }

        }

        private void frmThongke_FormClosing(object sender, FormClosingEventArgs e)
        {
            //return;
            if (save == true)
            {
                var msb = MessageBox.Show("Bạn có muốn save giá trị đã thay đổi không?", "Thông báo", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (msb == DialogResult.Yes)
                {
                    TimeSpan span = DateTime.Now - datebefore;
                    int ms = (int)span.TotalMilliseconds;
                    if (formID != 3)
                    {
                        int cn = listclname.Count;
                        int rw = dtsave.Rows.Count;
                        for (int i = 0; i < rw; i++)
                        {
                            string lccheck = dtsave.Rows[i]["LCCheck"].ToString();
                            string vl = string.Join("|", dtsave.Rows[i].ItemArray.Take(batchname[7] == 'A' ? (cn - 6) : cn - 5).Skip(1));
                            string id = dtsave.Rows[i]["Id"].ToString();
                            workdb.Save_LC(vl, id, tableform, lccheck);
                        }
                    }
                    else
                    {
                        int rw = lstSaveZ.Count;
                        if (rw > 0)
                        {
                            for (int i = 0; i < rw; i++)
                            {
                                string[] arrSpl = lstSaveZ[i].Split(',');
                                string lccheck = arrSpl[2];
                                string vl = arrSpl[1];
                                string id = arrSpl[0];
                                workdb.Save_LC(vl, id, tableform, lccheck);
                            }
                            save = false;
                            lstSaveZ = new List<string>();
                        }
                    }
                    workdb.Save_UserLC(userID, ms, tableform);
                }
                else if (msb == DialogResult.Cancel)
                    e.Cancel = true;
            }
            if (Directory.Exists(folderImage))
            {
                try
                {
                    Directory.Delete(folderImage, true);
                }
                catch { }
            }
        }

        public void getQaImport()
        {
            QA1 = workdb.GetQaImport(1, batchnameLocal, "QA1");
            QA3 = workdb.GetQaImport(2, batchnameLocal, "QA3");
            IMPORT1 = workdb.GetQaImport(1, batchnameLocal, "IMPORT1");
            IMPORT3 = workdb.GetQaImport(2, batchnameLocal, "IMPORT3");
            saveNouhin1 = workdb.GetQaImport(1, batchnameLocal, "saveNouhin1");
            saveNouhin3 = workdb.GetQaImport(2, batchnameLocal, "saveNouhin3");
            
        }

        private void frmLastCheck_Load(object sender, EventArgs e)
        {
            //this.Text = "VCB Lastcheck  -----  Form : " + frmLogIn.form + "  ----- Template : " + formName + "  -----  Batch : " + batchname;
            lbltemplate.Text = formName;
            if (formName.IndexOf("LV1") != -1)
                tempid = 1;
            else if (formName.IndexOf("LV3") != -3)
                tempid = 2;
            lblBatch.Text = batchname;
            batchnameLocal = batchname.Substring(7);
            getQaImport();

            if (formID == 1)
                listclname = batchname[7] == 'A' 
                    ? new List<string>() { "Page", "SBD", "Ngày sinh", "ID", "CheckerId", "LCCheck", "学校コード", "修正フラグ", "科目コード" } 
                    : new List<string>() { "Page", "SBD", "Ngày sinh", "ID", "CheckerId", "LCCheck", "学校コード", "修正フラグ" };
            else if (formID == 2)
                listclname = batchname[7] == 'A' 
                    ? new List<string>() { "Page", "Tên Kana", "ID", "CheckerId", "LCCheck", "学校コード", "修正フラグ", "科目コード" } 
                    : new List<string>() { "Page", "Tên Kana", "ID", "CheckerId", "LCCheck", "学校コード", "修正フラグ" };
            else if (formID == 3)
                listclname = new List<string>() { "Page", "検定番号", "受験年", "受験月", "スコア", "合否", "ID", "CheckerId", "LCCheck", "修正フラグ", };
            else if (formID == 4)
                listclname = new List<string>() { "Page", "Ban ngành", "Phân loại tn", "Môn1", "Môn2", "Môn3", "Môn4", "Môn5", "Môn6", "Môn7", "Môn8", "Môn9", "Môn10", "Môn11", "Môn12", "Môn13", "Môn14", "Môn15", "Môn16", "Môn17", "Môn18", "Môn19", "Môn20", "Môn21", "Môn22", "Code1", "Code2", "Code3", "Code4", "Code5", "Code6", "Code7", "Code8", "ID", "CheckerId", "LCCheck", "学校コード", "修正フラグ" };
            dt = new System.Data.DataTable();
            listclname.ForEach(a =>
            {                
                if (a != "修正フラグ")
                {
                    dt.Columns.Add(a, typeof(string));
                    dtTxt.Columns.Add(a, typeof(string));
                }
                else
                {
                    dt.Columns.Add(a, typeof(int));
                    dtTxt.Columns.Add(a, typeof(int));
                }
            });
            chkComboSubField.Properties.DataSource = listclname;
            //Lấy bảng Lastcheck
            if (formID == 1)
            {
                tableform = batchID + "--" + batchnameLocal;
                int batchIDtemp = workdb.GetBacthIdByNameAndForm(batchnameLocal, 2);
                if (batchIDtemp != 0)
                {
                    tableformtemp = batchIDtemp + "--" + batchnameLocal + "--lv3";
                }
            }
            else if (formID == 3)
            {
                tableform = batchID + "--" + batchnameLocal;
            }
            else
            {
                tableform = batchID + "--" + batchnameLocal + "--lv3";
                int batchIDtemp = workdb.GetBacthIdByNameAndForm(batchnameLocal, 1);
                if (batchIDtemp != 0)
                {
                    tableformtemp = batchIDtemp + "--" + batchnameLocal;
                }
            }
            DataTable newDataTable = new DataTable();

            if (tableform.IndexOf("lv3") == -1)
            {
                if (batchname[7] == 'C')
                {
                    if (workdb.getEntryinBatch(tableform) > 0)
                    {
                        newDataTable = workdb.dtLastcheck(tableform);
                    }
                    else
                    {
                        MessageBox.Show("Không cần xử lý!!!!");
                        newDataTable = workdb.dtLastcheck_OCR(tableform);
                    }
                }
                else
                    newDataTable = workdb.dtLastcheck(tableform);
            }
            else
            {
                newDataTable = workdb.dtLastcheck(tableform);
            }
            DataView dv = newDataTable.DefaultView;
            dv.Sort = "vl";
            newDataTable = dv.ToTable();
            int rwTemp = newDataTable.Rows.Count;
            DataTable newDataTxt = new DataTable();
            int rowTxt = 0;
            if (formID == 1 || formID == 2)
            {
                newDataTxt = workdb.dtLastcheck_Txt(tableformtemp);
                rowTxt = newDataTxt.Rows.Count;
            }

            filenameTxt = batchnameLocal[0] + "2" + batchnameLocal.Substring(4, 2) + batchnameLocal.Substring(2, 2) + batchnameLocal.Substring(6, batchnameLocal.Length - 6);
            //Lay file ocr
            strOCR = workdb.Get_StringOCR(batchID).TrimEnd('\r', '\n');
            try
            {
                arrbyteCMP = workdb.RetrieveCMPFromDatabase(batchID);
            }
            catch { }
            Lines = strOCR.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            List<string> arTemp0 = Lines.Select(x => x.Substring(0, 80)).ToList();
            string[] arTemp = arTemp0.OrderBy(x => x.Substring(batchname[7] == 'A' ? 5 : 9, 15)).ToArray();
            arrcode = arTemp.Select(x => x.Substring(batchname[7] == 'A' ? 36 : 41, 7)).ToArray();
            if (batchname[7] == 'Z')
            {
                for (int i = 0; i < rwTemp; i++)
                {
                    string strtemp1 = newDataTable.Rows[i]["vl"].ToString();
                    string[] arrSplit = strtemp1.Split('|');
                    var row1 = dt.NewRow();
                    row1[0] = arrSplit[0];
                    row1[1] = arrSplit[1];
                    row1[2] = arrSplit[2];
                    row1[3] = arrSplit[3];
                    row1[4] = arrSplit[4];
                    row1[5] = arrSplit[5];
                    row1[6] = arrSplit[21];
                    row1[7] = arrSplit[22];
                    row1[8] = arrSplit[23];
                    string coutstar1 = (arrSplit[1] + arrSplit[2] + arrSplit[3] + arrSplit[4] + arrSplit[5]).Count(x => x == '*' || x == '/').ToString();
                    row1[9] = coutstar1;
                    dt.Rows.Add(row1);
                    var row2 = dt.NewRow();
                    row2[0] = arrSplit[0];
                    row2[1] = arrSplit[6];
                    row2[2] = arrSplit[7];
                    row2[3] = arrSplit[8];
                    row2[4] = arrSplit[9];
                    row2[5] = arrSplit[10];
                    row2[6] = arrSplit[21];
                    row2[7] = arrSplit[22];
                    row2[8] = arrSplit[23];
                    string coutstar2 = (arrSplit[6] + arrSplit[7] + arrSplit[8] + arrSplit[9] + arrSplit[10]).Count(x => x == '*' || x == '/').ToString();
                    row2[9] = coutstar2;
                    dt.Rows.Add(row2);
                    var row3 = dt.NewRow();
                    row3[0] = arrSplit[0];
                    row3[1] = arrSplit[11];
                    row3[2] = arrSplit[12];
                    row3[3] = arrSplit[13];
                    row3[4] = arrSplit[14];
                    row3[5] = arrSplit[15];
                    row3[6] = arrSplit[21];
                    row3[7] = arrSplit[22];
                    row3[8] = arrSplit[23];
                    string coutstar3 = (arrSplit[11] + arrSplit[12] + arrSplit[13] + arrSplit[14] + arrSplit[15]).Count(x => x == '*' || x == '/').ToString();
                    row3[9] = coutstar3;
                    dt.Rows.Add(row3);
                    var row4 = dt.NewRow();
                    row4[0] = arrSplit[0];
                    row4[1] = arrSplit[16];
                    row4[2] = arrSplit[17];
                    row4[3] = arrSplit[18];
                    row4[4] = arrSplit[19];
                    row4[5] = arrSplit[20];
                    row4[6] = arrSplit[21];
                    row4[7] = arrSplit[22];
                    row4[8] = arrSplit[23];
                    string coutstar4 = (arrSplit[16] + arrSplit[17] + arrSplit[18] + arrSplit[19] + arrSplit[20]).Count(x => x == '*' || x == '/').ToString();
                    row4[9] = coutstar4;
                    dt.Rows.Add(row4);
                    
                }
            }
            else if (batchname[7] != 'A')
            {
                for (int i = 0; i < rwTemp; i++)
                {
                    try
                    {
                        string strtemp1 = newDataTable.Rows[i]["vl"].ToString(); 
                        if (strtemp1.IndexOf('*') != -1)
                        {
                            disableExportNouhin3 = true;
                        }    
                        string strtemp = strtemp1 + "|" + (arrcode[i].Contains('\"') ? "" : arrcode[i]) + "|" + strtemp1.Count(x => x == '*' || x == '/').ToString(); 
                        dt.Rows.Add(strtemp.Split('|'));
                        idrow.Add(dt.Rows[i]["ID"].ToString());
                        if (rowTxt > 0)
                        {
                            if (newDataTxt.Rows[i]["vl"].ToString().IndexOf('*') != -1)
                            {
                                disableExportNouhin1 = true;
                            }
                            dtTxt.Rows.Add(newDataTxt.Rows[i]["vl"].ToString().Split('|'));
                        }
                    }
                    catch { }
                }
            }
            else
            {
                string[] arrcode1 = arTemp.Select(x => x.Substring(64, 3)).ToArray();
                for (int i = 0; i < rwTemp; i++)
                {
                    try
                    {
                        string strtemp1 = newDataTable.Rows[i]["vl"].ToString();
                        if (strtemp1.IndexOf('*') != -1)
                        {
                            disableExportNouhin3 = true;
                        }
                        string strtemp = strtemp1 + "|" + (arrcode[i].Contains('\"') ? "" : arrcode[i]) + "|" + strtemp1.Count(x => x == '*' || x == '/').ToString() + "|" + arrcode1[i];
                        dt.Rows.Add(strtemp.Split('|'));
                        idrow.Add(dt.Rows[i]["ID"].ToString());
                        if (rowTxt > 0)
                        {
                            if (newDataTxt.Rows[i]["vl"].ToString().IndexOf('*') != -1)
                            {
                                disableExportNouhin1 = true;
                            }
                            dtTxt.Rows.Add(newDataTxt.Rows[i]["vl"].ToString().Split('|'));
                        }
                    }
                    catch { }
                }
            }
            lblCount.Text = dt.Compute("Sum ([修正フラグ])", "").ToString();
            //Đồng bộ datasource
            grThongke.DataSource = null;
            cboimage.Text = "";
            dtClone = dt;
            if (formID == 1)
            {
                dt = dt.AsEnumerable().Where(x => x.Field<string>("SBD") != "null" && x.Field<string>("Ngày sinh") != "null").CopyToDataTable();
            }
            cboimage.DataSource = dt;
            grThongke.DataSource = dt;
            if (batchname[7] == 'Z')
            {
                grThongkeV.OptionsView.AllowCellMerge = true;
                for (int i = 0; i < grThongkeV.Columns.Count; i++)
                {
                    if (grThongkeV.Columns[i].FieldName == "Page")
                        grThongkeV.Columns[i].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
                    else
                        grThongkeV.Columns[i].OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
                }
            }
            row = grThongkeV.RowCount;
            grThongkeV.Columns["ID"].Visible = false;
            grThongkeV.Columns["CheckerId"].Visible = false;
            grThongkeV.Columns["LCCheck"].Visible = false;
            grThongkeV.Columns["LCCheck"].OptionsColumn.AllowEdit = false;
            dtsave = dt.Clone();
            grThongkeV.BestFitColumns();
            if (formID == 2)
            {
                grThongkeV.Columns["Tên Kana"].AppearanceCell.Font = new System.Drawing.Font("Microsoft Sans Serif", 30, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                grThongkeV.RowHeight = 50;
            }
            datebefore = DateTime.Now;
            datebeforeN = DateTime.Now;
        }

        static Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        private void cboimage_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cboimage.SelectedIndex != -1)
            {
                if (tenanh != cboimage.Text)
                {
                    tenanh = cboimage.Text;
                    imageViewerTR1.Dispose();
                    lblError.Text = "";
                    try
                    {
                        string strfile = folderImage + @"\" + batchname + "-" + cboimage.Text;
                        if (!File.Exists(strfile))
                        {
                            bitmapTmp = new Bitmap(byteArrayToImage(workdb.getImageOnServer(cboimage.Text, batchname.Substring(7, batchname.Length - 7))));
                            if (batchname[7] == 'Z')
                            {
                                Bitmap bm = CropBitmap(bitmapTmp, bitmapTmp.Width / 2, 0, bitmapTmp.Width / 2, bitmapTmp.Height / 2);
                                imageViewerTR1.Image = bm;
                            }
                            else
                            {
                                using (Image img = (Bitmap)bitmapTmp.Clone())
                                {
                                    img.Save(strfile, System.Drawing.Imaging.ImageFormat.Jpeg);
                                }
                                imageViewerTR1.Image = bitmapTmp;
                            }
                        }
                        else
                        {
                            bitmapTmp = null;
                            using (Image img = Image.FromFile(strfile))
                            {
                                bitmapTmp = (Bitmap)img.Clone();
                                imageViewerTR1.Image = bitmapTmp;
                            }
                        }

                    }
                    catch
                    {

                    }
                }
            }
        }

        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {
            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }

        static byte[] Getbytes(string str)
        {
            Encoding encoding = Encoding.GetEncoding("Shift_jis");
            byte[] array = encoding.GetBytes(str);
            return array;
        }
        static byte Getbyteschar(string str)
        {
            byte temp;
            Encoding encoding = Encoding.GetEncoding("Shift_jis");
            byte[] array = encoding.GetBytes(str);
            temp = array[0];
            return temp;
        }
        static string Getstring(byte[] b)
        {
            Encoding encoding = Encoding.GetEncoding("Shift_jis");
            string array = encoding.GetString(b);
            return array;
        }



        private void grThongkeV_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            cl = e.Column;
            rw = grThongkeV.FocusedRowHandle;
            // cboimage.SelectedIndex = rw;
            cboimage.Text = grThongkeV.GetRowCellValue(rw, "Page").ToString();
            cboimage_SelectedValueChanged(sender, e);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (!bgwLoadExel.IsBusy)
            {
                btnViewResult.Focus();
                bgwLoadExel.RunWorkerAsync();

                //idrow = new List<string>();
                //dt.Clear();
                //dtTxt.Clear();
                //var newDataTable = workdb.dtLastcheck(tableform);
                //DataView dv = newDataTable.DefaultView;
                //dv.Sort = "vl";
                //newDataTable = dv.ToTable();
                //int rwTemp = newDataTable.Rows.Count;
                //DataTable newDataTxt = new DataTable();
                //int rowTxt = 0;
                //if (formID == 1 || formID == 2)
                //{
                //    newDataTxt = workdb.dtLastcheck_Txt(tableformtemp);
                //    rowTxt = newDataTxt.Rows.Count;
                //}

                ////filenameTxt = batchnameLocal[0] + "2" + batchnameLocal.Substring(4, 2) + batchnameLocal.Substring(2, 2) + batchnameLocal.Substring(6, batchnameLocal.Length - 6);
                ////Lay file ocr
                //strOCR = workdb.Get_StringOCR(batchID).TrimEnd('\r', '\n');
                //try
                //{
                //    arrbyteCMP = workdb.RetrieveCMPFromDatabase(batchID);
                //}
                //catch { }
                //Lines = strOCR.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                //List<string> arTemp0 = Lines.Select(x => x.Substring(0, 80)).ToList();
                //string[] arTemp = arTemp0.OrderBy(x => x.Substring(batchname[7] == 'A' ? 5 : 9, 15)).ToArray();
                //arrcode = arTemp.Select(x => x.Substring(batchname[7] == 'A' ? 36 : 41, 7)).ToArray();
                //if (batchname[7] != 'A')
                //{
                //    for (int i = 0; i < rwTemp; i++)
                //    {
                //        try
                //        {
                //            string strtemp1 = newDataTable.Rows[i]["vl"].ToString();
                //            string strtemp = strtemp1 + "|" + (arrcode[i].Contains('\"') ? "" : arrcode[i]) + "|" + strtemp1.Count(x => x == '*' || x == '/').ToString();
                //            dt.Rows.Add(strtemp.Split('|'));
                //            idrow.Add(dt.Rows[i]["ID"].ToString());
                //            if (rowTxt > 0)
                //                dtTxt.Rows.Add(newDataTxt.Rows[i]["vl"].ToString().Split('|'));
                //        }
                //        catch { }
                //    }
                //}
                //else
                //{
                //    string[] arrcode1 = arTemp.Select(x => x.Substring(64, 3)).ToArray();
                //    for (int i = 0; i < rwTemp; i++)
                //    {
                //        try
                //        {
                //            string strtemp1 = newDataTable.Rows[i]["vl"].ToString();
                //            string strtemp = strtemp1 + "|" + (arrcode[i].Contains('\"') ? "" : arrcode[i]) + "|" + strtemp1.Count(x => x == '*' || x == '/').ToString() + "|" + arrcode1[i];
                //            dt.Rows.Add(strtemp.Split('|'));
                //            idrow.Add(dt.Rows[i]["ID"].ToString());
                //            if (rowTxt > 0)
                //                dtTxt.Rows.Add(newDataTxt.Rows[i]["vl"].ToString().Split('|'));
                //        }
                //        catch { }
                //    }
                //}
                //grThongke.DataSource = null;
                //cboimage.Text = "";
                //dtClone = dt;
                //if (formID == 1)
                //{
                //    dt = dt.AsEnumerable().Where(x => x.Field<string>("SBD") != "null" && x.Field<string>("Ngày sinh") != "null").CopyToDataTable();
                //}
                //cboimage.DataSource = dt;
                //grThongke.DataSource = dt;
            }
        }

        private void grThongkeV_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            save = true;
            string vlcell = "";
            if (e.RowHandle > -1)
            {
                if (e.Column.FieldName != "Page" && e.Column.FieldName != "学校コード" && e.Column.FieldName != "修正フラグ" && e.Column.FieldName != "科目コード")
                {
                    if (formID != 3)
                    {
                        string idImage = grThongkeV.GetRowCellValue(grThongkeV.FocusedRowHandle, grThongkeV.Columns["ID"]).ToString();
                        DataRow[] arrRow = dt.Select("Id='" + idImage + "'");
                        vlcell = string.Join("", arrRow[0].ItemArray.Take(batchname[7] == 'A' ? (dt.Columns.Count - 4) : dt.Columns.Count - 3).Skip(1));
                        dtsave.Rows.Add(arrRow[0].ItemArray);
                        grThongkeV.SetRowCellValue(e.RowHandle, grThongkeV.Columns["修正フラグ"], vlcell.Count(x => x == '*' || x == '/').ToString());
                        lblCount.Text = dt.Compute("Sum ([修正フラグ])", "").ToString();
                    }
                    else
                    {
                        string idImage = grThongkeV.GetRowCellValue(grThongkeV.FocusedRowHandle, grThongkeV.Columns["ID"]).ToString();
                        DataRow[] arrRow = dt.Select("Id='" + idImage + "'");
                        if (arrRow.Length > 0)
                        {
                            string id = arrRow[0].ItemArray[6].ToString();
                            string vl = "";
                            string vl1 = "";
                            for (int i = 0; i < arrRow.Length; i++)
                            {
                                vl += arrRow[i].ItemArray[1] + "|" + arrRow[i].ItemArray[2] + "|" + arrRow[i].ItemArray[3] + "|" + arrRow[i].ItemArray[4] + "|" + arrRow[i].ItemArray[5] + "|";
                                vl1 += arrRow[i].ItemArray[1].ToString() + arrRow[i].ItemArray[2].ToString() + arrRow[i].ItemArray[3].ToString() + arrRow[i].ItemArray[4].ToString() + arrRow[i].ItemArray[5].ToString();
                            }
                            vl = vl.Remove(vl.Length - 1, 1);
                            if (!IsValid(vl1))
                            {
                                MessageBox.Show("Dữ liệu sửa chứa ký tự không hợp lệ!");
                                return;
                            }
                            //string qa = vl.Count(x => x == '*' || x == '/').ToString();
                            string lccheck = arrRow[0].ItemArray[8].ToString();
                            lstSaveZ.Add(id + "‡" + vl + "‡" + lccheck);
                            grThongkeV.SetRowCellValue(e.RowHandle, grThongkeV.Columns["修正フラグ"], vl.Count(x => x == '*' || x == '/').ToString());
                            lblCount.Text = dt.Compute("Sum ([修正フラグ])", "").ToString();
                        }
                    }
                }
            }

        }

        static bool IsValid(string value)
        {
            return Regex.IsMatch(value, @"^[0-9.*]*$");
        }

        int saveNouhin1 =0 ,saveNouhin3 = 0;
        private void bgwLoadExel_DoWork(object sender, DoWorkEventArgs e)
        {
            TimeSpan span = DateTime.Now - datebefore;
            int ms = (int)span.TotalMilliseconds;
            if (formID != 3)
            {
                int cn = listclname.Count;
                int rw = dtsave.Rows.Count;
                if (rw > 0)
                {
                    for (int i = 0; i < rw; i++)
                    {
                        string lccheck = dtsave.Rows[i]["LCCheck"].ToString();
                        string vl = string.Join("|", dtsave.Rows[i].ItemArray.Take(batchname[7] == 'A' ? (cn - 6) : cn - 5).Skip(1));
                        string id = dtsave.Rows[i]["Id"].ToString();
                        workdb.Save_LC(vl, id, tableform, lccheck);
                    }
                    save = false;
                    dtsave.Rows.Clear();
                }
            }
            else
            {
                int rw = lstSaveZ.Count;
                if (rw > 0)
                {
                    for (int i = 0; i < rw; i++)
                    {
                        string[] arrSpl = lstSaveZ[i].Split('‡');
                        string lccheck = arrSpl[2];
                        string vl = arrSpl[1];
                        string id = arrSpl[0];
                        workdb.Save_LC(vl, id, tableform, lccheck);
                    }
                    save = false;
                    lstSaveZ = new List<string>();
                }
            }
            if(tempid==1)
                workdb.SaveQaImport(tempid, batchnameLocal, "saveNouhin1", 1);
            else
                workdb.SaveQaImport(tempid, batchnameLocal, "saveNouhin3", 1);
            datebefore = DateTime.Now;
        }

        private void bgwLoadExel_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (blImport)
            {
                workdb.UpdateBatch2(batchnameLocal, formID, dt.Compute("Sum ([修正フラグ])", "").ToString());
            }
            else
            {
                workdb.UpdateBatch1(batchnameLocal, formID, dt.Compute("Sum ([修正フラグ])", "").ToString());
            }

            MessageBox.Show(this, "Completed!");
        }

        private void grThongkeV_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView view = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox2_Click(sender, e);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnrotateRight_Click(object sender, EventArgs e)
        {
            imageViewerTR1.RotateImage("270");
        }

        private void btnrotateLeft_Click(object sender, EventArgs e)
        {
            imageViewerTR1.RotateImage("90");
        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            if (grThongkeV.Columns.Count > 0)
            {
                if (tempid == 1)
                {
                    if (QA1 != IMPORT1)
                    {
                        MessageBox.Show("Chưa import!");
                        return;
                    }
                }
                else if (tempid == 2)
                {
                    if (QA3 != IMPORT3)
                    {
                        MessageBox.Show("Chưa import!");
                        return;
                    }
                }
                string nameclomun = "";
                if (tempid == 1)
                {
                    nameclomun = "QA1";
                    workdb.SaveQaImport(tempid, batchnameLocal, nameclomun, QA1 + 1);
                }
                else if (tempid == 2)
                {
                    nameclomun = "QA3";
                    workdb.SaveQaImport(tempid, batchnameLocal, nameclomun, QA3 + 1);
                }
                getQaImport();
                string dir1 = @"\\192.168.1.18\komosi\Output\QA\" + DateTime.Now.ToString("yyMMdd");
                // string dir1 = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\komosi\Output\QA\" + DateTime.Now.ToString("yyMMdd");
                if (!Directory.Exists(dir1))
                {
                    Directory.CreateDirectory(dir1);
                }
                grThongkeV.FindFilterText = "*";
                int r1 = grThongkeV.RowCount;
                grThongkeV.FindFilterText = "";
                grThongkeV.FindFilterText = "/";
                int r2 = grThongkeV.RowCount;
                string exportFilePath = "";
                if (r1 > 0 || r2 > 0)
                {
                    exportFilePath = dir1 + @"\" + batchnameLocal + (formName.Length == 10 ? "" : ("_" + formName.Substring(6, 3))) + "_" + batchname.Substring(0, 2) + "_NG.xlsx";
                }
                else
                {
                    exportFilePath = dir1 + @"\" + batchnameLocal + (formName.Length == 10 ? "" : ("_" + formName.Substring(6, 3))) + "_" + batchname.Substring(0, 2) + ".xlsx";
                }
                if (File.Exists(exportFilePath))
                    File.Delete(exportFilePath);
                grThongkeV.FindFilterText = "";
                grThongkeV.ClearColumnsFilter();
                grThongkeV.ClearSorting();
                grThongkeV.ClearGrouping();
                grThongkeV.ClearSelection();
                int cn = listclname.Count;
                TimeSpan span = DateTime.Now - datebeforeN;
                int ms = (int)span.TotalMilliseconds;
                //Save nouhin
                workdb.Save_nouhin(tableform, userID, ms);
                datebeforeN = DateTime.Now;
                try
                {
                    grThongkeV.ExportToXlsx(exportFilePath);
                }
                catch { }
                MessageBox.Show("Completed!");
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            btnSaveAs_Click(sender, e);
        }

        private void allBatchToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void grThongkeV_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && e.Modifiers == Keys.Control)
            {
                if (MessageBox.Show("Delete row?", "Confirmation", MessageBoxButtons.YesNo) !=
                  DialogResult.Yes)
                    return;
                GridView view = sender as GridView;
                view.DeleteRow(view.FocusedRowHandle);
            }
        }


        private DataTable Pivot(DataTable dt, DataColumn pivotColumn, DataColumn pivotValue)
        {
            // find primary key columns 
            //(i.e. everything but pivot column and pivot value)
            DataTable temp = dt.Copy();
            temp.Columns.Remove(pivotColumn.ColumnName);
            temp.Columns.Remove(pivotValue.ColumnName);
            string[] pkColumnNames = temp.Columns.Cast<DataColumn>()
                .Select(c => c.ColumnName)
                .ToArray();

            // prep results table
            DataTable result = temp.DefaultView.ToTable(true, pkColumnNames).Copy();
            result.PrimaryKey = result.Columns.Cast<DataColumn>().ToArray();
            dt.AsEnumerable()
                .Select(r => r[pivotColumn.ColumnName].ToString())
                .Distinct().ToList()
                .ForEach(c => result.Columns.Add(c, typeof(string)));

            // load it
            foreach (DataRow row in dt.Rows)
            {
                // find row to update
                DataRow aggRow = result.Rows.Find(
                    pkColumnNames
                        .Select(c => row[c])
                        .ToArray());
                // the aggregate used here is LATEST 
                // adjust the next line if you want (SUM, MAX, etc...)
                string field = row[pivotColumn.ColumnName].ToString();
                string vl = "";
                try
                {
                    int positon = Convert.ToInt32(field.Split('_')[1]);
                    vl = row[pivotValue.ColumnName].ToString();
                    vl = vl.Split('|')[positon - 1];
                }
                catch { }
                aggRow[row[pivotColumn.ColumnName].ToString()] = vl;
                string fieldNameCaption = row[pivotColumn.ColumnName].ToString();
            }

            return result;
        }

        private void cboLevel_Click(object sender, EventArgs e)
        {

        }

        private void btnView_Click(object sender, EventArgs e)
        {
            if (chkComboSubField.Text != "")
            {
                for (int i = 0; i < listclname.Count; i++)
                {
                    if (chkComboSubField.Properties.Items[i].CheckState == CheckState.Checked)
                        grThongkeV.Columns[listclname[i]].Visible = true;
                    else
                        grThongkeV.Columns[listclname[i]].Visible = false;
                }
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {

        }

        private void grThongkeV_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            string vl = e.CellValue.ToString();
            if (vl.Contains('*') || vl.Contains('/'))
            {
                e.Appearance.BackColor = Color.GreenYellow;
            }
            if (e.RowHandle == grThongkeV.FocusedRowHandle)
                e.Appearance.BackColor = Color.PowderBlue;
            
        }

        private void grThongkeV_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (grThongkeV.RowCount > 0)
            {
                GridView View = sender as GridView;
                string checker = View.GetRowCellDisplayText(e.RowHandle, View.Columns["CheckerId"]);
                string lccheck = View.GetRowCellDisplayText(e.RowHandle, View.Columns["LCCheck"]);
                if (checker != "")
                {
                    e.Appearance.BackColor = Color.Khaki;
                }
                if (lccheck == "1")
                {
                    e.Appearance.BackColor = Color.LightCoral;
                }
            }
        }


        private void btnViewResult_Click(object sender, EventArgs e)
        {
            tenanh = "";
            DataTable dtclone = dt.Clone();
            dt = new DataTable();
            dt = dtclone.Clone();
            //idrow = new List<string>();
            var newDataTable = workdb.dtLastcheck(tableform);
            DataView dv = newDataTable.DefaultView;
            dv.Sort = "vl";
            newDataTable = dv.ToTable();
            int rwTemp = newDataTable.Rows.Count;

            //filenameTxt = batchnameLocal[0] + "2" + batchnameLocal.Substring(4, 2) + batchnameLocal.Substring(2, 2) + batchnameLocal.Substring(6, batchnameLocal.Length - 6);
            //Lay file ocr
            strOCR = workdb.Get_StringOCR(batchID).TrimEnd('\r', '\n');
            try
            {
                arrbyteCMP = workdb.RetrieveCMPFromDatabase(batchID);
            }
            catch { }
            Lines = strOCR.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            List<string> arTemp0 = Lines.Select(x => x.Substring(0, 80)).ToList();
            string[] arTemp = arTemp0.OrderBy(x => x.Substring(batchname[7] == 'A' ? 5 : 9, 15)).ToArray();
            //if (batchname[7] != 'A')
            if (batchname[7] == 'C')
            {
                for (int i = 0; i < rwTemp; i++)
                {
                    try
                    {
                        string strtemp1 = newDataTable.Rows[i]["vl"].ToString();
                        string strtemp = strtemp1 + "|" + (arrcode[i].Contains('\"') ? "" : arrcode[i]) + "|" + strtemp1.Count(x => x == '*' || x == '/').ToString();
                        dt.Rows.Add(strtemp.Split('|'));
                        // idrow.Add(dt.Rows[i]["ID"].ToString());
                    }
                    catch { }
                }
            }
            else if (batchname[7] == 'A')
            {
                string[] arrcode1 = arTemp.Select(x => x.Substring(64, 3)).ToArray();
                for (int i = 0; i < rwTemp; i++)
                {
                    try
                    {
                        string strtemp1 = newDataTable.Rows[i]["vl"].ToString();
                        string strtemp = strtemp1 + "|" + (arrcode[i].Contains('\"') ? "" : arrcode[i]) + "|" + strtemp1.Count(x => x == '*' || x == '/').ToString() + "|" + arrcode1[i];
                        dt.Rows.Add(strtemp.Split('|'));
                        //idrow.Add(dt.Rows[i]["ID"].ToString());
                    }
                    catch { }
                }
            }
            else
            {
                try
                {
                    dt = workdb.dtLastcheck_BatchZ(tableform);
                }
                catch { }
            }
            dtClone = dt;
            if (formID == 1)
            {
                dt = dt.AsEnumerable().Where(x => x.Field<string>("SBD") != "null" && x.Field<string>("Ngày sinh") != "null").CopyToDataTable();
            }

            if (!bgwTxt.IsBusy && !bgwTxt2.IsBusy && !bgwTouan.IsBusy && !bgwTouan2.IsBusy && !bgwForm2.IsBusy && !bgwForm22.IsBusy)
            {
                if (btnViewResult.Text == "View Txt")
                {
                    lsint = new List<int[]>();
                    countCompleted = 0;
                    rtxtKetqua.Clear();
                    error = new List<string>();
                    Lines = null;
                    Lines = strOCR.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                    if (batchname.Substring(7, 1) == "C")
                    {
                        if (formID == 1)
                        {
                            LV1_Kojin = dtClone;
                            if (dtTxt.Rows.Count > 0)
                                LV3_Kojin_Kana = dtTxt;
                            else
                            {
                                MessageBox.Show("Chưa có batch LV3, ko thể View Txt", "Thông báo");
                                return;
                            }
                        }
                        else
                        {
                            LV3_Kojin_Kana = dt;
                            if (dtTxt.Rows.Count > 0)
                                LV1_Kojin = dtTxt;
                            else
                            {
                                MessageBox.Show("Chưa có batch LV1, ko thể View Txt", "Thông báo");
                                return;
                            }
                        }
                        vlchange1 = 0;
                        vlchange2 = 0;
                        lbltxt.Text = "";
                        bgwTxt.RunWorkerAsync();
                        bgwTxt2.RunWorkerAsync();
                    }
                    else if (batchname.Substring(7, 1) == "A")
                    {
                        if (formID == 1)
                        {
                            LV1_Kojin = dt;
                            if (dtTxt.Rows.Count > 0)
                                LV3_Kojin_Kana = dtTxt;
                            else
                            {
                                MessageBox.Show("Chưa có batch LV3, ko thể View Txt", "Thông báo");
                                return;
                            }
                        }
                        else
                        {
                            LV3_Kojin_Kana = dt;
                            if (dtTxt.Rows.Count > 0)
                                LV1_Kojin = dtTxt;
                            else
                            {
                                MessageBox.Show("Chưa có batch LV1, ko thể View Txt", "Thông báo");
                                return;
                            }
                        }
                        vlchange1 = 0;
                        vlchange2 = 0;
                        lbltxt.Text = "";
                        bgwTouan2.RunWorkerAsync();
                        bgwTouan.RunWorkerAsync();
                    }
                    else
                    {
                        if (formID == 3)
                        {
                            phanloai = "2";
                            LV1_Zokusei2 = dt;
                        }
                        else if (formID == 4)
                        {
                            phanloai = "3";
                            LV1_Zokusei3 = dt;
                        }
                        lbltxt.Text = "";
                        vlchange1 = 0;
                        vlchange2 = 0;
                        //bgwForm22.RunWorkerAsync();
                        bgwForm2.RunWorkerAsync();
                    }
                    grThongke.Visible = false;
                    btnViewResult.Text = "View Grid";
                    splitContainer2.Visible = true;
                    cboimage.DataSource = null;
                    cboimage.Text = "";
                    cboimage.DataSource = dtClone;
                    cboimage.DisplayMember = "Page";
                }
                else
                {
                    btnViewResult.Text = "View Txt";
                    splitContainer2.Visible = false;
                    grThongke.Visible = true;
                    cboimage.DataSource = null;
                    cboimage.Text = "";
                    cboimage.DataSource = dt;
                    cboimage.DisplayMember = "Page";
                }
                txtstatus.Text = "";
            }
        }


        List<string> error;
        private void bgwTxt_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int vl = Lines.Length;// / 2;
            for (int i = 0; i < vl; i++)
            {
                vlchange1++;
                worker.ReportProgress(vlchange1);
                string tp = "";
                string line = "";
                string[] Fields;
                line = Lines[i];
                if (line.IndexOf("A6180713982039") != -1)
                {

                }
                //cắt dữ liệu các trường đưa vào mảng 1.8.2015
                Fields = line.Split(new char[] { ',' });
                //lấy dữ liệu để filter 1.8.2015 
                string idLine = Fields[2].Trim('"');
                DataRow[] rowLV1_Kojin = LV1_Kojin.Select("Page like '" + idLine + "%'");
                DataRow[] rowLV3_Kojin_Kana = LV3_Kojin_Kana.Select("Page like '" + idLine + "%'");
                int SelectedIndex = 0;
                if (rowLV1_Kojin.Length > 0)
                {
                    SelectedIndex = LV1_Kojin.Rows.IndexOf(rowLV1_Kojin[0]);
                }
                if (rowLV1_Kojin.Length == 0)
                {
                    error.Add(idLine + ": không có file LV1");
                    continue;
                }
                if (rowLV1_Kojin.Length >= 2)
                {
                    error.Add(idLine + ": trùng nhau");
                    continue;
                }
                if (rowLV3_Kojin_Kana.Length == 0)
                {
                    error.Add(idLine + ": không có file LV3");
                    continue;
                }
                if (rowLV3_Kojin_Kana.Length >= 2)
                {
                    error.Add(idLine + ": trùng nhau LV3");
                    continue;
                }
                string cotB_lv1 = rowLV1_Kojin[0].ItemArray[1].ToString().ToUpper();
                if (cotB_lv1 != "NULL")
                {
                    if (cotB_lv1.Length < 4 && cotB_lv1.Length > 0)
                    {
                        error.Add(string.Format("{0}, SBD không đúng 4 ký tự", idLine));
                        continue;
                    }
                    else if (cotB_lv1.Contains('/'))
                        cotB_lv1 = "      ";
                    else if (cotB_lv1.Length == 4)
                    {
                        //Bước1
                        string v1 = cotB_lv1.Substring(0, 1);
                        string v2 = cotB_lv1.Substring(1, 1);
                        string v3 = cotB_lv1.Substring(2, 1);
                        string v4 = cotB_lv1.Substring(3, 1);
                        if (arrAlphabet.Contains(v1) && arrAlphabet.Contains(v2))
                        {
                            cotB_lv1 = "00" + cotB_lv1.Substring(2, 2);
                        }
                        if (arrAlphabet.Contains(v3) && arrAlphabet.Contains(v4))
                        {
                            cotB_lv1 = cotB_lv1.Substring(0, 2) + "00";
                        }
                        //Bước 2
                        for (int a = 0; a < 4; a++)
                        {
                            string vltemp = cotB_lv1[a].ToString();
                            int index = Array.IndexOf(arrAlphabet, vltemp) + 1;
                            if (index > 0 && index < 10)
                            {
                                cotB_lv1 = cotB_lv1.Remove(a, 1);
                                cotB_lv1 = cotB_lv1.Insert(a, index.ToString());
                            }
                        }
                        int idex1 = Array.IndexOf(arrAlphabet, cotB_lv1.Substring(0, 1)) + 1;
                        int idex3 = Array.IndexOf(arrAlphabet, cotB_lv1.Substring(2, 1)) + 1;
                        if (idex1 > 9)
                        {
                            cotB_lv1 = cotB_lv1.Remove(0, 1);
                            cotB_lv1 = cotB_lv1.Insert(0, "0");
                        }
                        if (idex3 > 9)
                        {
                            cotB_lv1 = cotB_lv1.Remove(2, 1);
                            cotB_lv1 = cotB_lv1.Insert(2, "0");
                        }
                        //Bước 3
                        int id = Array.IndexOf(arrAlphabet, cotB_lv1.Substring(1, 1));
                        if (id > 8)
                        {
                            if (cotB_lv1.Substring(0, 1) == "0" || cotB_lv1.Substring(0, 1) == "-" || cotB_lv1.Substring(0, 1) == "*")
                            {
                                if (id <= 18)
                                {
                                    cotB_lv1 = "1" + (id - 9) + cotB_lv1.Substring(2, 2);
                                }
                                else
                                {
                                    cotB_lv1 = "2" + (id - 19) + cotB_lv1.Substring(2, 2);
                                }
                            }
                            else
                            {
                                cotB_lv1 = cotB_lv1.Substring(0, 1) + "0" + cotB_lv1.Substring(2, 2);
                            }
                        }
                        id = Array.IndexOf(arrAlphabet, cotB_lv1.Substring(3, 1));
                        if (id > 8)
                        {
                            if (cotB_lv1.Substring(2, 1) == "0" || cotB_lv1.Substring(2, 1) == "-" || cotB_lv1.Substring(2, 1) == "*")
                            {
                                if (id <= 18)
                                {
                                    cotB_lv1 = cotB_lv1.Substring(0, 2) + "1" + (id - 9);
                                }
                                else
                                {
                                    cotB_lv1 = cotB_lv1.Substring(0, 2) + "2" + (id - 19);
                                }
                            }
                            else
                            {
                                cotB_lv1 = cotB_lv1.Substring(0, 2) + cotB_lv1.Substring(2, 1) + "0";
                            }
                        }
                        if (cotB_lv1.Contains('*') || cotB_lv1.Contains('-'))
                            cotB_lv1 = "    ";
                    }
                    else if (cotB_lv1.Length == 6)
                    {
                        if (cotB_lv1.Count(x => char.IsDigit(x)) != 6)
                        {
                            cotB_lv1 = "      ";
                        }
                    }
                    else
                    {
                        if (cotB_lv1 == "")
                        {
                            error.Add(string.Format("{0}: SBD trống", idLine));
                        }
                        else
                        {
                            error.Add(string.Format("{0}: SBD không đúng 4 hoặc 6 ký tự", idLine));
                        }
                    }
                    //thay thế chuỗi space bằng chuỗi nhập nếu chuỗi nhập không đủ thì vẫn để space
                    cotB_lv1 = String.Format("{0,-6}", cotB_lv1);
                    // cột 71 _ 6 ký tự _ thêm LV1_Kojin cột B
                    tp = line.Substring(63, 6);
                    if (tp.Contains(','))
                        line = line.Insert(63, cotB_lv1);
                    else
                    {
                        line = line.Remove(63, 6);
                        line = line.Insert(63, cotB_lv1);
                    }
                }
                // cột 75 _ 1 ký tự _ thêm space
                line = line.Insert(72, " ");
                // cột 85 _ 7 ký tự _ thêm space
                line = line.Insert(76, "       ");



                            //string cotB_lv3 = rowLV3_Kojin_Kana[0].ItemArray[1].ToString();
                string cotB_lv3 = removeSpace(rowLV3_Kojin_Kana[0].ItemArray[1].ToString().Trim());

                if (check_full_width(cotB_lv3))
                {
                    error.Add(string.Format("{0}: có xuất hiện chữ 2 byte", idLine));
                }

                cotB_lv3 = String.Format("{0,-16}", cotB_lv3);
                //dài hơn 16 ký tự thì chỉ lấy 16 ký tự 3/8/2015
                if (cotB_lv3.Length > 16)
                {
                    cotB_lv3 = cotB_lv3.Substring(0, 16);
                }

                //chuyển chữ nhỏ thành chữ to 3/8/2015
                cotB_lv3 = cotB_lv3.Replace("ｬ", "ﾔ");
                cotB_lv3 = cotB_lv3.Replace("ｭ", "ﾕ");
                cotB_lv3 = cotB_lv3.Replace("ｮ", "ﾖ");
                cotB_lv3 = cotB_lv3.Replace("ｧ", "ｱ");
                cotB_lv3 = cotB_lv3.Replace("ｨ", "ｲ");
                cotB_lv3 = cotB_lv3.Replace("ｩ", "ｳ");
                cotB_lv3 = cotB_lv3.Replace("ｪ", "ｴ");
                cotB_lv3 = cotB_lv3.Replace("ｫ", "ｵ");
                cotB_lv3 = cotB_lv3.Replace("ｯ", "ﾂ");
                cotB_lv3 = cotB_lv3.Replace("ヵ", "ｶ");
                cotB_lv3 = cotB_lv3.ToUpper();

                tp = line.Substring(86, 16);
                if (tp.Contains(','))
                    line = line.Insert(86, cotB_lv3);
                else
                {
                    line = line.Remove(86, 16);
                    line = line.Insert(86, cotB_lv3);
                }

                // cột 112 _ 5 ký tự _ thêm space 2 byte
                line = line.Insert(105, "　　　　　");
                // cột 120 _ 5 ký tự _ thêm space 2 byte
                line = line.Insert(113, "　　　　　");

                tp = line.Substring(121, 2);
                if (!tp.Contains(','))
                    line = line.Remove(121, 2);


                if (rowLV1_Kojin[0].ItemArray[2].ToString().ToUpper() != "NULL")
                {
                    string cotC_lv1 = rowLV1_Kojin[0].ItemArray[2].ToString();
                    if (cotC_lv1 == "")
                    {
                        line = line.Insert(121, "00");
                        tp = line.Substring(126, 2);
                        if (tp.Contains(','))
                            line = line.Insert(126, "00");
                        else
                        {
                            line = line.Remove(126, 2);
                            line = line.Insert(126, "00");
                        }
                        // cột 135 _ 2 ký tự _ thêm LV1_Kojin cột C trước + _ ngày
                        tp = line.Substring(131, 2);
                        if (tp.Contains(','))
                            line = line.Insert(131, "00");
                        else
                        {
                            line = line.Remove(131, 2);
                            line = line.Insert(131, "00");
                        }
                        tp = line.Substring(136, 1);
                        if (tp.Contains(','))
                            line = line.Insert(136, "0");
                        else
                        {
                            line = line.Remove(136, 1);
                            line = line.Insert(136, "0");
                        }
                    }
                    else
                    {
                        string firstC1 = "";
                        string firstC2 = "";
                        string firstC3 = "00";
                        //split cột C L1_Kojin
                        string firstC = cotC_lv1;
                        // firstC = firstC.Replace("*", " ");
                        firstC = firstC.Replace("-", " ");
                        firstC = firstC.Replace("/", " ");
                        firstC = firstC.Replace(" ", "a");
                        if (firstC.Length == 4)
                        {
                            firstC1 = firstC.Substring(0, 2).ToLower().Contains("a") ? "00" : firstC.Substring(0, 2);
                            firstC2 = firstC.Substring(2, 2).ToLower().Contains("a") ? "00" : firstC.Substring(2, 2);
                        }
                        else if (firstC.Length == 6)
                        {
                            firstC1 = firstC.Substring(2, 2).ToLower().Contains("a") ? "00" : firstC.Substring(2, 2);
                            firstC2 = firstC.Substring(4, 2).ToLower().Contains("a") ? "00" : firstC.Substring(4, 2);
                            firstC3 = firstC.Substring(0, 2).ToLower().Contains("a") ? "00" : firstC.Substring(0, 2);
                            firstC = firstC.Substring(2);
                        }
                        else
                        {
                            error.Add(string.Format("{0}: ngày sinh", idLine));
                            continue;
                        }
                        int n;
                        if (int.TryParse(firstC, out n))
                        {
                            string thangngay = "";
                            try
                            {
                                thangngay = "2000" + (firstC.Length == 6 ? firstC.Substring(2) : firstC);
                                DateTime.ParseExact(thangngay, "yyyyMMdd", CultureInfo.InvariantCulture);
                                // cột 125 _ 2 ký tự _ thêm LV1_Kojin cột C trước + _ năm 00                           
                                line = line.Insert(121, firstC3);
                                // cột 130 _ 2 ký tự _ thêm LV1_Kojin cột C trước + _ tháng   
                                tp = line.Substring(126, 2);
                                if (tp.Contains(','))
                                    line = line.Insert(126, firstC1);
                                else
                                {
                                    line = line.Remove(126, 2);
                                    line = line.Insert(126, firstC1);
                                }
                                // cột 135 _ 2 ký tự _ thêm LV1_Kojin cột C trước + _ ngày
                                tp = line.Substring(131, 2);
                                if (tp.Contains(','))
                                    line = line.Insert(131, firstC2);
                                else
                                {
                                    line = line.Remove(131, 2);
                                    line = line.Insert(131, firstC2);
                                }
                            }
                            catch
                            {
                                error.Add(string.Format("{0}: ngày sinh", idLine));
                                line = line.Insert(121, firstC3);
                                if (IsNumber(firstC1) && IsNumber(firstC2))
                                {
                                    int thang = Convert.ToInt32(firstC1);
                                    if (thang >= 1 && thang <= 12)
                                    {
                                        tp = line.Substring(126, 2);
                                        if (tp.Contains(','))
                                            line = line.Insert(126, firstC1);
                                        else
                                        {
                                            line = line.Remove(126, 2);
                                            line = line.Insert(126, firstC1);
                                        }
                                    }
                                    else
                                    {
                                        tp = line.Substring(126, 2);
                                        if (tp.Contains(','))
                                            line = line.Insert(126, "00");
                                        else
                                        {
                                            line = line.Remove(126, 2);
                                            line = line.Insert(126, "00");
                                        }
                                    }
                                    int ngay = Convert.ToInt32(firstC2);
                                    switch (thang)
                                    {
                                        case 1:
                                        case 3:
                                        case 5:
                                        case 7:
                                        case 8:
                                        case 10:
                                        case 12:
                                            if (ngay >= 1 && ngay <= 31)
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, firstC2);
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, firstC2);
                                                }
                                            }
                                            else
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, "00");
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, "00");
                                                }
                                            }
                                            break;
                                        case 4:
                                        case 6:
                                        case 9:
                                        case 11:
                                            if (ngay >= 1 && ngay <= 30)
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, firstC2);
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, firstC2);
                                                }
                                            }
                                            else
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, "00");
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, "00");
                                                }
                                            }
                                            break;
                                        case 2:
                                            if (ngay >= 1 && ngay <= 29)
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, firstC2);
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, firstC2);
                                                }
                                            }
                                            else
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, "00");
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, "00");
                                                }
                                            }
                                            break;
                                        default:
                                            if (ngay >= 1 && ngay <= 31)
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, firstC2);
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, firstC2);
                                                }
                                            }
                                            else
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, "00");
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, "00");
                                                }
                                            }
                                            break;
                                    }

                                }
                                else
                                {
                                    error.Add(string.Format("{0}: ngày sinh", idLine));
                                }
                            }
                        }
                        else
                        {

                           
                            int b;
                            if (int.TryParse(firstC1, out b))
                            {
                                if (b < 0 || b > 12)
                                {
                                    line = line.Insert(131, "00");
                                    error.Add(string.Format("{0}: tháng không hợp lệ", idLine));
                                }
                            }
                            else
                            {
                                line = line.Insert(131, "00");
                                error.Add(string.Format("{0}: tháng có ký tự đặc biệt", idLine));
                            }
                            int c;
                            if (int.TryParse(firstC2, out c))
                            {
                                if (c < 0 || c > 31)
                                {
                                    line = line.Insert(126, "00");
                                    error.Add(string.Format("{0}: ngày không hợp lệ", idLine));
                                }
                            }
                            else
                            {
                                line = line.Insert(126, "00");
                                error.Add(string.Format("{0}: ngày có ký tự đặc biệt", idLine));
                            }
                            // cột 125 _ 2 ký tự _ thêm LV1_Kojin cột C trước + _ năm 00
                            line = line.Insert(121, firstC3);

                            // cột 130 _ 2 ký tự _ thêm LV1_Kojin cột C trước + _ tháng    //126
                            tp = line.Substring(126, 2);
                            if (tp.Contains(','))
                                line = line.Insert(126, firstC1);
                            else
                            {
                                line = line.Remove(126, 2);
                                line = line.Insert(126, firstC1);
                            }
                            // cột 135 _ 2 ký tự _ thêm LV1_Kojin cột C trước + _ ngày
                            tp = line.Substring(131, 2);
                            if (tp.Contains(','))
                                line = line.Insert(131, firstC2);
                            else
                            {
                                line = line.Remove(131, 2);
                                line = line.Insert(131, firstC2);
                            }
                        }
                    }
                }
                else
                {
                    string[] splitNam = Lines[i].Split(',');
                    string namString = splitNam[15].Replace("\"","");
                    if (!IsNumber(namString))
                    {
                        line = line.Insert(121, "00");
                    }
                    else
                    {
                        line = line.Insert(121,  namString );
                    }
                }

                // cột 152 _ 10 ký tự _ thêm space
                line = line.Insert(140, "          ");
                // cột 156 _ 1 ký tự _ thêm space
                line = line.Insert(153, " ");
                // cột 160 _ 1 ký tự _ thêm space
                line = line.Insert(157, " ");
                // cột 181 _ 6 ký tự _ thêm space
                line = line.Insert(173, "      ");
                // cột 190 _ 6 ký tự _ thêm space
                line = line.Insert(182, "      ");
                // cột 199 _ 6 ký tự _ thêm space
                line = line.Insert(191, "      ");
                // cột 208 _ 6 ký tự _ thêm space
                line = line.Insert(200, "      ");
                // cột 217 _ 6 ký tự _ thêm space
                line = line.Insert(209, "      ");
                // cột 226 _ 6 ký tự _ thêm space
                line = line.Insert(218, "      ");
                // cột 235 _ 6 ký tự _ thêm space
                line = line.Insert(227, "      ");
                // cột 244 _ 6 ký tự _ thêm space
                line = line.Insert(236, "      ");
                // cột 253 _ 6 ký tự _ thêm space
                line = line.Insert(245, "      ");
                // cột 262 _ 6 ký tự _ thêm space
                line = line.Insert(254, "      ");
                // cột 271 _ 6 ký tự _ thêm space
                line = line.Insert(263, "      ");
                // cột 280 _ 6 ký tự _ thêm space
                line = line.Insert(272, "      ");
                // cột 289 _ 6 ký tự _ thêm space
                line = line.Insert(281, "      ");
                // cột 298 _ 6 ký tự _ thêm space
                line = line.Insert(290, "      ");
                // cột 307 _ 6 ký tự _ thêm space
                line = line.Insert(299, "      ");
                // cột 316 _ 6 ký tự _ thêm space
                line = line.Insert(308, "      ");
                // cột 325 _ 6 ký tự _ thêm space
                line = line.Insert(317, "      ");
                line = line.Replace("*", " ");
                if (KiemTraKyTuDacBiet(line) != "")
                {
                    error.Add(string.Format("{0}: có 1 ký tự đặc biệt", idLine));
                    continue;
                }
                // chuyển "ｰ" thành "-"
                line = line.Replace("ｰ", "-");
                line = line.Replace("ー", "-");
                Lines[i] = line;
            }
        }

        private void bgwTxt_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lbltxt.Text = (vlchange1 + vlchange2).ToString();
        }

        private void bgwTxt_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            countCompleted++;
            if (countCompleted == 2)
            {
                if (error.Count == 0)
                {
                    error.Add("Completed!");
                    error.Add("");
                    error.Add("");
                }
                else
                {
                    error.Add("");
                    error.Add("");
                }
                rtxtError.Text = "";
                string[] arrDis = arrcode.Distinct().ToArray();
                error.Add("Dòng: " + Lines.Length);
                error.Add("Code trường: " + arrDis.Length);
                //error.Add("Code: " + string.Join(",", arrcode.Distinct()));
                if (batchname[7] == 'A')
                {
                    for (int i = 0; i < arrDis.Length; i++)
                    {
                        string[] arrtemp = Lines.Where(x => x.Substring(36, 7) == arrDis[i]).ToArray();
                        error.Add(arrDis[i] + ": " + arrtemp.Select(x => x.Substring(74, 6)).Distinct().Count());
                    }
                }
                rtxtError.AppendText(string.Join(Environment.NewLine, error));
                rtxtKetqua.Lines = Lines;
                rtxtError.Visible = true;
                rtxtKetqua.Visible = true;
                vlfl = 11.75F;
                this.rtxtKetqua.ForeColor = Color.Black;
                this.rtxtKetqua.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                //this.rtxtError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                MessageBox.Show(this, "OK");
            }
            else
            {
                if (formID == 3)
                {
                    if (error.Count == 0)
                    {
                        error.Add("Completed!");
                        error.Add("");
                        error.Add("");
                    }
                    else
                    {
                        error.Add("");
                        error.Add("");
                    }
                    rtxtError.Text = "";
                    rtxtError.AppendText(string.Join(Environment.NewLine, error));
                    rtxtKetqua.Lines = Lines;
                    rtxtError.Visible = true;
                    rtxtKetqua.Visible = true;
                    vlfl = 11.75F;
                    this.rtxtKetqua.ForeColor = Color.Black;
                    this.rtxtKetqua.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    //this.rtxtError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    MessageBox.Show(this, "OK");
                }
            }
        }
        public string removeSpace(string str)
        {
           List<string> temp = str.Split(' ').ToList();
            if (temp.Count() >= 3)
            {
                for (int i = 2; i < temp.Count(); i++)
                {
                    temp[i] = "†" + temp[i];
                }
            }
            string res = string.Join(" ",temp).Replace(" †","");
            return res;
        }
        private void bgwTouan_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int vl = Lines.Length / 2;
            for (int i = 0; i < vl; i++)
            {
                vlchange1++;
                worker.ReportProgress(vlchange1);
                string line = "";
                string[] Fields;
                line = Lines[i];
                //cắt dữ liệu các trường đưa vào mảng 1.8.2015
                Fields = line.Split(new char[] { ',' });
                //lấy dữ liệu để filter 1.8.2015 
                string idLine = Fields[1].Trim('"');
                DataRow[] rowLV1_Kojin = LV1_Kojin.Select("Page like '" + idLine + "%'");
                DataRow[] rowLV3_Kojin_Kana = LV3_Kojin_Kana.Select("Page like '" + idLine + "%'");
                int SelectedIndex = 0;
                if (rowLV1_Kojin.Length > 0)
                {
                    SelectedIndex = LV1_Kojin.Rows.IndexOf(rowLV1_Kojin[0]);
                }
                if (rowLV1_Kojin.Length == 0)
                {
                    error.Add(idLine + ": không có file LV1");
                    continue;
                }
                if (rowLV1_Kojin.Length >= 2)
                {
                    error.Add(idLine + ": trùng nhau");
                    continue;
                }
                if (rowLV3_Kojin_Kana.Length == 0)
                {
                    error.Add(idLine + ": không có file LV3");
                    continue;
                }
                if (rowLV3_Kojin_Kana.Length >= 2)
                {
                    error.Add(idLine + ": trùng nhau LV3");
                    continue;
                }
                string cotB_lv1 = rowLV1_Kojin[0].ItemArray[1].ToString().ToUpper();
                if (cotB_lv1.Length < 4 && cotB_lv1.Length > 0)
                {
                    error.Add(string.Format("{0}, SBD không đúng 4 ký tự", idLine));
                    continue;
                }
                cotB_lv1 = cotB_lv1.Replace('-', 'a').Replace(' ', 'a');
                if (!IsNumber(cotB_lv1))
                {
                    cotB_lv1 = "      ";
                }
                //thay thế chuỗi space bằng chuỗi nhập nếu chuỗi nhập không đủ thì vẫn để space
                cotB_lv1 = String.Format("{0,-6}", cotB_lv1);
                // cột 71 _ 6 ký tự _ thêm LV1_Kojin cột B
                string tp = line.Substring(74, 6);
                if (tp.Contains(','))
                    line = line.Insert(74, cotB_lv1);
                else
                {
                    line = line.Remove(74, 6);
                    line = line.Insert(74, cotB_lv1);
                }
                // cột 75 _ 1 ký tự _ thêm space
                line = line.Insert(83, " ");
                // cột 85 _ 7 ký tự _ thêm space
                line = line.Insert(87, "       ");

                string cotB_lv3 =removeSpace(rowLV3_Kojin_Kana[0].ItemArray[1].ToString().Trim());


                if (check_full_width(cotB_lv3))
                {
                    error.Add(string.Format("{0}: có xuất hiện chữ 2 byte", idLine));
                }

                cotB_lv3 = String.Format("{0,-16}", cotB_lv3);
                //dài hơn 16 ký tự thì chỉ lấy 16 ký tự 3/8/2015
                if (cotB_lv3.Length > 16)
                {
                    cotB_lv3 = cotB_lv3.Substring(0, 16);
                }

                //chuyển chữ nhỏ thành chữ to 3/8/2015
                cotB_lv3 = cotB_lv3.Replace("ｬ", "ﾔ");
                cotB_lv3 = cotB_lv3.Replace("ｭ", "ﾕ");
                cotB_lv3 = cotB_lv3.Replace("ｮ", "ﾖ");
                cotB_lv3 = cotB_lv3.Replace("ｧ", "ｱ");
                cotB_lv3 = cotB_lv3.Replace("ｨ", "ｲ");
                cotB_lv3 = cotB_lv3.Replace("ｩ", "ｳ");
                cotB_lv3 = cotB_lv3.Replace("ｪ", "ｴ");
                cotB_lv3 = cotB_lv3.Replace("ｫ", "ｵ");
                cotB_lv3 = cotB_lv3.Replace("ｯ", "ﾂ");
                cotB_lv3 = cotB_lv3.Replace("ヵ", "ｶ");
                cotB_lv3 = cotB_lv3.ToUpper();

                tp = line.Substring(97, 16);
                if (tp.Contains(','))
                    line = line.Insert(97, cotB_lv3);
                else
                {
                    line = line.Remove(97, 16);
                    line = line.Insert(97, cotB_lv3);
                }

                // cột 112 _ 5 ký tự _ thêm space 2 byte
                line = line.Insert(116, " ");
                line = line.Replace("*", " ");
                if (KiemTraKyTuDacBiet(line) != "")
                {
                    error.Add(String.Format("{0}: có ký tự đặc biệt", idLine));
                }
                // chuyển "ｰ" thành "-"
                line = line.Replace("ｰ", "-");
                line = line.Replace("ー", "-");
                Lines[i] = line;
            }
        }

        private void bgwForm2_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int vl = Lines.Length;
            for (int i = 0; i < vl; i++)
            {
                vlchange1++;
                worker.ReportProgress(vlchange1);
                string line = "";
                string newline = "";
                string[] Fields;
                line = Lines[i];
                //cắt dữ liệu các trường đưa vào mảng 1.8.2015
                Fields = line.Split(new char[] { ',' });
                //lấy dữ liệu để filter 1.8.2015 
                string idLine = Fields[2].Trim('"');
                DataRow[] rowLV1_Zokusei;
                if (phanloai == "2")
                    rowLV1_Zokusei = LV1_Zokusei2.Select("Page like '" + idLine + "%'");
                else
                    rowLV1_Zokusei = LV1_Zokusei3.Select("Page like '" + idLine + "%'");
                if (rowLV1_Zokusei.Length == 0)
                {
                    error.Add(idLine + ": không có file");
                    continue;
                }
                if (rowLV1_Zokusei.Length >= 2)
                {
                    error.Add(idLine + ": trùng nhau");
                    continue;
                }

                #region code cũ
                //// cột 72 _ 7 ký tự _ thêm space
                //line = line.Insert(63, "       ");
                //// cột 125 _ 50 ký tự _ thêm space
                //line = line.Insert(73, "　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　");
                //// cột 129 _ 1 ký tự _ thêm space
                //line = line.Insert(126, " ");
                ////thay đổi lại cột B thành G 08/08    

                //string tp = line.Substring(130, 2);
                //if (!tp.Contains(','))
                //    line = line.Remove(130, 1);
                //string cotG_lv1 = "";
                //if (phanloai == "2")
                //{
                //    cotG_lv1 = rowLV1_Zokusei[0].ItemArray[1].ToString() + "+2";
                //}
                //else
                //{
                //    cotG_lv1 = rowLV1_Zokusei[0].ItemArray[1].ToString() + "+" + rowLV1_Zokusei[0].ItemArray[2].ToString();
                //}
                //if ("+".Equals(cotG_lv1))
                //{
                //    cotG_lv1 = "0+3";
                //}
                //string[] splitG = cotG_lv1.Split('+');
                //if (splitG.Length != 2)
                //{
                //    error.Add(String.Format(idLine + ": Phân loại không đúng"));
                //    continue;
                //}
                //string firstG = splitG[0];
                //// cột 133 _ 1 ký tự _ thêm cột B trước +
                //if ("".Equals(firstG))
                //{
                //    line = line.Insert(130, "0");
                //}
                //else
                //{
                //    line = line.Insert(130, firstG);
                //}
                //string lastG = splitG[1];
                //tp = line.Substring(131, 2);
                //if (!tp.Contains(','))
                //    line = line.Remove(131, 1);
                //// cột 137 _ 1 ký tự _ thêm cột B sau +
                //if ("".Equals(lastG))
                //{
                //    line = line.Insert(134, "3");
                //}
                //else
                //{
                //    line = line.Insert(134, lastG);
                //}
                ////thay đổi cột C thành cột F 08/08
                //string cotF_lv1;
                //if (phanloai == "2")
                //{
                //    cotF_lv1 = string.Join("+", rowLV1_Zokusei[0].ItemArray.Skip(2).Take(18));
                //}
                //else
                //{
                //    cotF_lv1 = string.Join("+", rowLV1_Zokusei[0].ItemArray.Skip(3).Take(22));
                //}
                //string[] splitF = cotF_lv1.Split('+');
                //for (int a = 0; a < splitF.Length; a++)
                //{
                //    if (splitF[a] == "")
                //        splitF[a] = "zzz";
                //}
                ////sort từ nhỏ tới lớn
                //Array.Sort(splitF);
                //// cột 143-299 _ 3 ký tự _ thêm cột C
                //int positionF = 138;
                //for (int j = 0; j < 27; j++)
                //{
                //    tp = line.Substring(positionF, 3);
                //    if (!tp.Contains(','))
                //        line = line.Remove(positionF, 3);
                //    string cotF = "   ";
                //    if (j < splitF.Length)
                //    {
                //        cotF = splitF[j].Replace('z', ' ');
                //    }
                //    line = line.Insert(positionF, cotF);
                //    positionF += 6;
                //}
                //// cột 307 _ 3 ký tự _ thêm 00000
                //line = line.Insert(300, "00000");
                //// cột 311 _ 1 ký tự _ thêm space
                //line = line.Insert(308, " ");
                //// cột 318 _ 7 ký tự _ thêm 0000
                //line = line.Insert(312, "0000");
                //// cột 330 _ 9 ký tự _ thêm space
                //line = line.Insert(319, "         ");
                //// cột 335 _ 2 ký tự _ thêm space
                //line = line.Insert(331, "  ");
                //// cột 341 _ 3 ký tự _ thêm space
                //line = line.Insert(336, "   ");
                //// cột 346 _ 2 ký tự _ thêm space
                //line = line.Insert(342, "  ");
                //// cột 352 _ 3 ký tự _ thêm space
                //line = line.Insert(347, "   ");
                //// cột 357 _ 2 ký tự _ thêm space
                //line = line.Insert(353, "  ");
                //// cột 362 _ 2 ký tự _ thêm space
                //line = line.Insert(358, "  ");
                //// cột 415 _ 50 ký tự _ thêm space
                //line = line.Insert(363, "                                                  ");
                //// cột 448 _ 30 ký tự _ thêm space
                //line = line.Insert(416, "                              ");
                //// cột 491 _ 40 ký tự _ thêm space
                //line = line.Insert(449, "                                        ");

                ////Code1
                //string code1 = "";
                //if (phanloai == "2")
                //{
                //    code1 = rowLV1_Zokusei[0].ItemArray[20].ToString();
                //}
                //else
                //{
                //    code1 = rowLV1_Zokusei[0].ItemArray[25].ToString();
                //}
                //bool blcode1 = false;
                //tp = line.Substring(492, 10);
                //if (!tp.Contains(','))
                //    line = line.Remove(492, 10);
                //if (code1 == "")
                //{
                //    line = line.Insert(492, "          ");
                //    blcode1 = true;
                //}
                //else
                //{
                //    if (code1.Length == 8 || code1.Length == 10)
                //    {
                //        string vl4f = code1.Substring(0, 4);
                //        int number = 0;
                //        if (!int.TryParse(vl4f.Replace(' ', 'a').Replace('-', 'a'), out number))
                //        {
                //            line = line.Insert(492, "          ");
                //        }
                //        else
                //        {
                //            string vl56 = code1.Substring(4, 2);
                //            if (!int.TryParse(vl56.Replace(' ', 'a').Replace('-', 'a'), out number))
                //            {
                //                vl56 = "00";
                //            }
                //            string vl78 = code1.Substring(6, 2);
                //            if (!int.TryParse(vl78.Replace(' ', 'a').Replace('-', 'a'), out number))
                //            {
                //                vl78 = "00";
                //            }
                //            string vl9 = " "; string vl10 = " ";
                //            if (code1.Length == 10)
                //            {
                //                vl9 = code1.Substring(8, 1);
                //                if (!int.TryParse(vl9.Replace(' ', 'a').Replace('-', 'a'), out number))
                //                {
                //                    vl9 = "0";
                //                }
                //                vl10 = code1.Substring(9, 1);
                //                if (!int.TryParse(vl10.Replace(' ', 'a').Replace('-', 'a'), out number))
                //                {
                //                    vl10 = "0";
                //                }
                //            }
                //            line = line.Insert(492, vl4f + vl56 + vl78 + vl9 + vl10);
                //        }
                //    }
                //    else
                //    {
                //        error.Add(String.Format("{0}: Code1 có {1} ký tự", idLine, code1.Length));
                //        continue;
                //    }
                //}
                ////code2-8  
                //List<string> arr2_8 = new List<string>();
                //if (phanloai == "2")
                //{
                //    arr2_8.AddRange(string.Join("+", rowLV1_Zokusei[0].ItemArray.Skip(21).Take(3)).Split('+'));
                //}
                //else
                //{
                //    arr2_8.AddRange(string.Join("+", rowLV1_Zokusei[0].ItemArray.Skip(26).Take(7)).Split('+'));
                //}
                //bool blvl = false;
                //int position = 505;
                //for (int a = 0; a < 7; a++)
                //{
                //    tp = line.Substring(position, 10);
                //    if (!tp.Contains(','))
                //        line = line.Remove(position, 10);
                //    if (a < arr2_8.Count)
                //    {
                //        code1 = arr2_8[a].ToString();
                //        if (code1 == "")
                //        {
                //            code1 = "          ";
                //            arr2_8[a] = "aaaaaaaaaa";
                //        }
                //        else if (code1.Length == 8 || code1.Length == 10)
                //        {
                //            string vl4f = code1.Substring(0, 4);
                //            int number = 0;
                //            if (!int.TryParse(vl4f.Replace(' ', 'a').Replace('-', 'a'), out number))
                //            {
                //                arr2_8[a] = "          ";
                //            }
                //            else
                //            {
                //                string vl56 = code1.Substring(4, 2);
                //                if (!int.TryParse(vl56.Replace(' ', 'a').Replace('-', 'a'), out number))
                //                {
                //                    vl56 = "00";
                //                }
                //                string vl78 = code1.Substring(6, 2);
                //                if (!int.TryParse(vl78.Replace(' ', 'a').Replace('-', 'a'), out number))
                //                {
                //                    vl78 = "00";
                //                }
                //                string vl9 = " "; string vl10 = " ";
                //                if (code1.Length == 10)
                //                {
                //                    vl9 = code1.Substring(8, 1);
                //                    if (!int.TryParse(vl9.Replace(' ', 'a').Replace('-', 'a'), out number))
                //                    {
                //                        vl9 = "0";
                //                    }
                //                    vl10 = code1.Substring(9, 1);
                //                    if (!int.TryParse(vl10.Replace(' ', 'a').Replace('-', 'a'), out number))
                //                    {
                //                        vl10 = "0";
                //                    }
                //                }
                //                arr2_8[a] = vl4f + vl56 + vl78 + vl9 + vl10;
                //            }
                //        }
                //        else
                //        {
                //            error.Add(String.Format("{0}: Code{1} có {2} ký tự", idLine, a + 2, code1.Length));
                //            blvl = true;
                //            break;
                //        }
                //    }
                //    line = line.Insert(position, "          ");
                //    position += 13;
                //}
                //if (blvl)
                //{
                //    continue;
                //}
                //if (blcode1)
                //    position = 492;
                //else
                //    position = 505;
                //for (int a = 0; a < 7; a++)
                //{
                //    if (arr2_8.Count > a)
                //    {
                //        if (arr2_8[a] != "aaaaaaaaaa")
                //        {
                //            line = line.Remove(position, 10);
                //            line = line.Insert(position, arr2_8[a]);
                //            position += 13;
                //        }
                //    }
                //    else
                //    {
                //        break;
                //    }
                //    //line = line.Remove(position, 10);
                //    //line = line.Insert(position, arr2_8[a]);
                //    //position += 13;
                //}
                //// cột 605 _ 3 ký tự _ thêm space
                //line = line.Insert(600, "   ");
                //// cột 648 _ 40 ký tự _ thêm space
                //line = line.Insert(606, "                                        ");
                //// cột 811 _ 160 ký tự _ thêm space
                //line = line.Insert(649, "                                                                                                                                                                ");
                //// cột 816 _ 2 ký tự _ thêm space
                //line = line.Insert(812, "  ");
                //// cột 821 _ 2 ký tự _ thêm space
                //line = line.Insert(817, "  ");
                //// cột 826 _ 2 ký tự _ thêm space
                //line = line.Insert(822, "  ");
                //// cột 831 _ 2 ký tự _ thêm space
                //line = line.Insert(827, "  ");
                //// cột 836 _ 2 ký tự _ thêm space
                //line = line.Insert(832, "  ");
                //// cột 841 _ 2 ký tự _ thêm space
                //line = line.Insert(837, "  ");
                //// cột 874 _ 30 ký tự _ thêm space
                //line = line.Insert(842, "                              ");
                //// cột 897 _ 20 ký tự _ thêm space
                //line = line.Insert(875, "                    ");
                //// cột 940 _ 40 ký tự _ thêm space
                //line = line.Insert(898, "                                        ");
                //// cột 946 _ 3 ký tự _ thêm space
                //line = line.Insert(941, "   ");
                //// cột 1049 _ 100 ký tự _ thêm space
                //line = line.Insert(947, "                                                                                                    ");
                //// cột 1252 _ 200 ký tự _ thêm space
                //line = line.Insert(1050, "                                                                                                                                                                                                        ");
                //// cột 1258 _ 3 ký tự _ thêm space
                //line = line.Insert(1253, "   ");
                //// cột 1301 _ 40 ký tự _ thêm space
                //line = line.Insert(1259, "                                        ");
                //// cột 1324 _ 20 ký tự _ thêm space
                //line = line.Insert(1302, "                    ");
                //// cột 1329 _ 2 ký tự _ thêm space
                //line = line.Insert(1325, "  ");
                //// cột 1334 _ 2 ký tự _ thêm space
                //line = line.Insert(1330, "  ");
                //// cột 1339 _ 2 ký tự _ thêm space
                //line = line.Insert(1335, "  ");
                //// cột 1344 _ 1 ký tự _ thêm space
                //line = line.Insert(1344, " ");
                //// cột 1348 _ 2 ký tự _ thêm space
                //line = line.Insert(1348, "  ");
                //// cột 1353 _ 25 ký tự _ thêm space lớn
                //line = line.Insert(1353, "　　　　　　　　　　　　　　　　　　　　　　　　　");
                //// cột 1381 _ 25 ký tự _ thêm space lớn
                //line = line.Insert(1381, "　　　　　　　　　　　　　　　　　　　　　　　　　");
                //// line = line.Replace("*", " ");
                #endregion

                string[] thang = rowLV1_Zokusei[0].ItemArray[1].ToString().Split('|');
                string thang1 = thang[2];
                if (thang1 != "")
                {
                    string date = "10/" + thang1 + "/2010";
                    DateTime dateTime;
                    bool isvalid = DateTime.TryParse(date, out dateTime);
                    if (!isvalid)
                    {
                        error.Add(idLine + ": có tháng " + thang1 + " ở dòng 1 không hợp lệ");
                    }
                }

                string thang2 = thang[7];
                if (thang2 != "")
                {
                    string date = "10/" + thang2 + "/2010";
                    DateTime dateTime;
                    bool isvalid = DateTime.TryParse(date, out dateTime);
                    if (!isvalid)
                    {
                        error.Add(idLine + ": có tháng " + thang2 + " ở dòng 2 không hợp lệ");
                    }
                }

                string thang3 = thang[12];
                if (thang3 != "")
                {
                    string date = "10/" + thang3 + "/2010";
                    DateTime dateTime;
                    bool isvalid = DateTime.TryParse(date, out dateTime);
                    if (!isvalid)
                    {
                        error.Add(idLine + ": có tháng " + thang3 + " ở dòng 3 không hợp lệ");
                    }
                }

                string thang4 = thang[17];
                if (thang4 != "")
                {
                    string date = "10/" + thang4 + "/2010";
                    DateTime dateTime;
                    bool isvalid = DateTime.TryParse(date, out dateTime);
                    if (!isvalid)
                    {
                        error.Add(idLine + ": có tháng " + thang4 + " ở dòng 4 không hợp lệ");
                    }
                }


                newline += Fields[5].Trim('"').Trim(' ') + "," + Fields[2].Trim('"') + "," + Fields[4].Trim('"') + "," + rowLV1_Zokusei[0].ItemArray[1].ToString().Replace("|", ",");

                if (KiemTraKyTuDacBiet(newline) != "")
                {
                    error.Add(String.Format("{0}: có ký tự đặc biệt!", idLine));
                }
                Lines[i] = newline;
            }

        }
        static string KiemTraKyTuDacBiet(string chuoi)
        {
            string vl = "";
            if (chuoi.Contains('-'))
                vl = "-";
            if (chuoi.Contains('?'))
                vl = vl + "?";
            if (chuoi.Contains('/'))
                vl = vl + "/";
            return vl;
        }
        static bool check_full_width(string vlstr)
        {
            bool bl = false;
            int aaa = vlstr.Length;
            Encoding encoding = Encoding.GetEncoding("Shift_jis");
            for (int i = 0; i < aaa; i++)
            {
                byte[] array = encoding.GetBytes(vlstr[i].ToString());

            }
            byte[] array1 = encoding.GetBytes(vlstr);
            int bbb = array1.Length;
            if (bbb > aaa)
            {
                bl = true;
            }
            //if (bbb / aaa == 2)
            //{
            //    bl = true;
            //}
            return bl;
        }
        static bool IsNumber(string pText)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            return regex.IsMatch(pText);
        }

        private void rtxtKetqua_MouseClick(object sender, MouseEventArgs e)
        {
            //int index = rtxtKetqua.SelectionStart;
            //int line = rtxtKetqua.GetLineFromCharIndex(index);
            //txtstatus.Text = "Line:" + (line + 1) + ", Col:" + (index + 1 - rtxtKetqua.GetFirstCharIndexFromLine(line));
        }
        private void rtxtKetqua_SelectionChanged(object sender, EventArgs e)
        {
            //rtxtKetqua.SelectionChanged -= rtxtKetqua_SelectionChanged;
            int index = rtxtKetqua.SelectionStart;
            //int daudong = rtxtKetqua.GetFirstCharIndexOfCurrentLine();
            int line = rtxtKetqua.GetLineFromCharIndex(index);
            txtstatus.Text = "Line:" + (line + 1) + ", Col:" + (index + 1 - rtxtKetqua.GetFirstCharIndexFromLine(line));
            //if (dongtruoc != null)
            //{
            //    if (dongtruoc[0] != daudong)
            //    {
            //        rtxtKetqua.Select(dongtruoc[0], dongtruoc[1]); //Select text within 0 and 8
            //        rtxtKetqua.SelectionColor = Color.Black;
            //        rtxtKetqua.Select(index, 0);
            //        dongtruoc = null;//Set the selected text color to Red
            //    }

            //}
            //if (daudong == index)
            //{
            //    int c = rtxtKetqua.GetFirstCharIndexFromLine(line + 1);
            //    c = c == -1 ? (rtxtKetqua.Text.Length) : c;
            //    rtxtKetqua.Select(daudong, c - daudong); //Select text within 0 and 8
            //    rtxtKetqua.SelectionColor = Color.Red; //Set the selected text color to Red
            //    rtxtKetqua.Select(daudong, 0);
            //    dongtruoc = new int[2] { daudong, c - daudong };
            //}
            try
            {
                if (formID != 3)
                {
                    string str = rtxtKetqua.Lines[line];
                    string vltemp = str.Substring(batchname[7] == 'A' ? 5 : 10, 14);
                    var rws = dtClone.Select("Page like '" + vltemp + "%'");
                    cboimage.Text = rws[0].ItemArray[0].ToString();
                }
                else
                {
                    string str = rtxtKetqua.Lines[line];
                    string vltemp = str.Split(',')[1].Replace("\"", "");
                    var rws = dtClone.Select("Page like '" + vltemp + "%'");
                    cboimage.Text = rws[0].ItemArray[0].ToString();
                }
            }
            catch { }
            //rtxtKetqua.SelectionChanged += rtxtKetqua_SelectionChanged;
        }

        private void FormLC_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
                if (e.KeyCode == Keys.Add)
                    this.rtxtKetqua.Font = new System.Drawing.Font("Microsoft Sans Serif", vlfl++, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                else if (e.KeyCode == Keys.Subtract)
                    this.rtxtKetqua.Font = new System.Drawing.Font("Microsoft Sans Serif", vlfl--, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            getQaImport();
            if (tempid == 1)
            {
                if (QA1 == IMPORT1)
                {
                    MessageBox.Show("Chưa xuất QA!");
                    return;
                }
            }
            else if (tempid == 3)
            {
                if (QA3 == IMPORT3)
                {
                    MessageBox.Show("Chưa xuất QA!");
                    return;
                }
            }
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Filter = "Excel 2007,Excel 2010 (*.xlsx)|*.xlsx";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string fname = openFileDialog1.FileName;
                if (!fname.Contains(batchnameLocal))
                {
                    MessageBox.Show(this, "File import không đúng", "Thông báo");
                    return;
                }
                if (formID == 2 && !fname.Contains("LV3"))
                {
                    MessageBox.Show(this, "File import không đúng LV3", "Thông báo");
                    return;
                }

                dtexcel = ExcelDataBaseHelper.OpenFile(fname);
                //try
                //{
                //    dt.Rows.Clear();
                //}
                //catch { }


                //Tên Kana
                DataTable dtimport = new DataTable();
                // dtimport = dt.Clone();
                int rimport = idrow.Count;
                if (batchname[7] == 'A')
                {

                    for (int i = 0; i < rimport; i++)
                    {
                        List<string> ls = new List<string>();
                        string t = dtexcel.Rows[i]["修正フラグ"].ToString();
                        if (t == "1")
                        {
                            if (formID == 2)
                            {
                                dt.Rows[i]["Tên Kana"] = dtexcel.Rows[i]["Tên Kana"];
                                ls.AddRange(dtexcel.Rows[i].ItemArray.Select(x => x.ToString()).ToArray());
                                ls.Insert(ls.Count - 3, idrow[i]);
                                ls.Insert(ls.Count - 3, "");
                            }
                            else
                            {
                                dt.Rows[i]["SBD"] = dtexcel.Rows[i]["SBD"];
                                dt.Rows[i]["Ngày sinh"] = dtexcel.Rows[i]["Ngày sinh"];
                                ls.AddRange(dtexcel.Rows[i].ItemArray.Select(x => x.ToString()).ToArray());
                                ls.Insert(ls.Count - 3, idrow[i]);
                                ls.Insert(ls.Count - 3, "");
                            }
                            if (Convert.ToInt32(ls[5]) > 0)
                            {
                                dtsave.Rows.Add(ls.ToArray());
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < grThongkeV.RowCount; i++)
                    //   for (int i = 0; i < rimport; i++)
                    {
                        List<string> ls = new List<string>();
                        string t = dtexcel.Rows[i]["修正フラグ"].ToString();
                        if (t == "1")
                        {
                            if (formID == 2)
                            {
                                dt.Rows[i]["Tên Kana"] = dtexcel.Rows[i]["Tên Kana"];
                                ls.AddRange(dtexcel.Rows[i].ItemArray.Select(x => x.ToString()).ToArray());
                                ls.Insert(ls.Count - 2, idrow[i]);
                                ls.Insert(ls.Count - 2, "");
                            }
                            else
                            {
                                dt.Rows[i]["SBD"] = dtexcel.Rows[i]["SBD"];
                                dt.Rows[i]["Ngày sinh"] = dtexcel.Rows[i]["Ngày sinh"];
                                ls.AddRange(dtexcel.Rows[i].ItemArray.Select(x => x.ToString()).ToArray());
                                ls.Insert(ls.Count - 2, dt.Rows[i]["ID"].ToString());
                                ls.Insert(ls.Count - 2, "");
                            }
                            if (Convert.ToInt32(ls[ls.Count - 1]) > 0)
                            {
                                dtsave.Rows.Add(ls.ToArray());
                            }
                        }
                    }
                }
                dt.Merge(dtimport);
                lblCount.Text = dt.Compute("Sum ([修正フラグ])", "").ToString();
                blImport = true;
                grThongke.DataSource = dt;

                string nameclomun = "";
                if (tempid == 1)
                {
                    nameclomun = "IMPORT1";
                    workdb.SaveQaImport(tempid, batchnameLocal, nameclomun, IMPORT1 + 1);
                }
                else if (tempid == 2)
                {
                    nameclomun = "IMPORT3";
                    workdb.SaveQaImport(tempid, batchnameLocal, nameclomun, IMPORT3 + 1);
                }
                getQaImport();
                MessageBox.Show(this, "OK");
            }
        }

        private void btnNouhin_Click(object sender, EventArgs e)
        {
            if (formID != 3)
            {
                getQaImport();
                DataRow[] temp = dt.Select("[CheckerId]<>''");
                int sumLCCheck = 0;
                for (int i = 0; i < temp.Count(); i++)
                {
                    if (temp[i]["LCCheck"].ToString() == "1")
                        sumLCCheck++;
                }
                if (sumLCCheck != temp.Count())
                {
                    MessageBox.Show("Bạn chưa check xong!");
                    return;
                }
                //  var t=  dt.Compute("SUM(Cast([LCCheck] as INT))", "[CheckerId]<>''").ToString();

                if (saveNouhin1 == 0)
                {
                    MessageBox.Show("LV1 chưa lưu LC!");
                    return;
                }
                if (saveNouhin3 == 0)
                {
                    MessageBox.Show("LV3 chưa lưu LC!");
                    return;
                }
                if (QA1 != IMPORT1 || QA3 != IMPORT3)
                {
                    MessageBox.Show("Chưa import xong!");
                    return;
                }
                if (QA1 == 0 && disableExportNouhin1)
                {
                    MessageBox.Show("LV1 còn  *. Chưa xuất QA!");
                    return;
                }
                if (QA3 == 0 && disableExportNouhin3)
                {
                    MessageBox.Show("LV3 còn  *. Chưa xuất QA!");
                    return;
                }

                if (formName.IndexOf("LV1") != -1)
                    return;
                if (grThongkeV.Columns.Count > 0)
                {
                    if (Lines != null && btnViewResult.Text == "View Grid")
                    {
                        string dir1 = @"\\192.168.1.18\komosi\Output\" + DateTime.Now.ToString("yyMMdd");
                        //string dir1 = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\komosi\Output\" + DateTime.Now.ToString("yyMMdd");
                        if (!Directory.Exists(dir1))
                        {
                            Directory.CreateDirectory(dir1);
                        }

                        //StringBuilder sb = new StringBuilder(filenameTxt);
                        //if (sb[1] == '1')
                        //{
                        //    sb[1] = '2';
                        //}
                        //string repl1 = filenameTxt.Substring(2, 2);
                        //string repl2 = filenameTxt.Substring(4, 2);
                        //sb.Remove(2, 2);
                        //sb.Insert(2, repl2);
                        //sb.Remove(4, 2);
                        //sb.Insert(4, repl1);
                        //filenameTxt = sb.ToString();
                        string dir2 = @"\\192.168.1.18\komosi\Output\" + DateTime.Now.ToString("yyMMdd") + @"\" + filenameTxt;
                        //string dir2 = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\komosi\Output\" + DateTime.Now.ToString("yyMMdd") + @"\" + filenameTxt;
                        if (!Directory.Exists(dir2))
                        {
                            Directory.CreateDirectory(dir2);
                        }
                        try
                        {
                            if (File.Exists(dir2 + @"\" + filenameTxt + ".txt"))
                                File.Delete(dir2 + @"\" + filenameTxt + ".txt");
                            File.WriteAllLines(dir2 + @"\" + filenameTxt + ".txt", Lines, Encoding.GetEncoding("Shift_jis"));
                        }
                        catch { }

                        try
                        {
                            if (File.Exists(dir2 + @"\" + batchnameLocal + ".cmp"))
                                File.Delete(dir2 + @"\" + batchnameLocal + ".cmp");
                            File.WriteAllBytes(dir2 + @"\" + batchnameLocal + ".cmp", arrbyteCMP);
                        }
                        catch { }

                        int cn = listclname.Count;
                        TimeSpan span = DateTime.Now - datebeforeN;
                        int ms = (int)span.TotalMilliseconds;
                        //Save nouhin
                        workdb.Save_nouhin(tableform, userID, ms);
                        datebeforeN = DateTime.Now;
                        MessageBox.Show("Completed!");
                    }
                    else
                    {
                        MessageBox.Show("View Txt trước khi save");
                    }
                }
            }
            else
            {
                if (grThongkeV.Columns.Count > 0)
                {
                    if (Lines != null && btnViewResult.Text == "View Grid")
                    {
                        string dir1 = @"\\192.168.1.18\komosi\komosi-EIGO\Output\" + DateTime.Now.ToString("yyMMdd");
                        //string dir1 = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\komosi-batchZ-new\Output\" + DateTime.Now.ToString("yyMMdd");
                        if (!Directory.Exists(dir1))
                        {
                            Directory.CreateDirectory(dir1);
                        }

                        string dir2 = @"\\192.168.1.18\komosi\komosi-EIGO\Output\" + DateTime.Now.ToString("yyMMdd") + @"\" + filenameTxt;
                        //string dir2 = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\komosi-batchZ-new\Output\" + DateTime.Now.ToString("yyMMdd") + @"\" + filenameTxt;
                        if (!Directory.Exists(dir2))
                        {
                            Directory.CreateDirectory(dir2);
                        }
                        try
                        {
                            if (File.Exists(dir2 + @"\" + filenameTxt + ".csv"))
                                File.Delete(dir2 + @"\" + filenameTxt + ".csv");
                            File.WriteAllLines(dir2 + @"\" + filenameTxt + ".csv", Lines, Encoding.GetEncoding("Shift_jis"));
                        }
                        catch { }

                        try
                        {
                            if (File.Exists(dir2 + @"\" + batchnameLocal + ".cmp"))
                                File.Delete(dir2 + @"\" + batchnameLocal + ".cmp");
                            File.WriteAllBytes(dir2 + @"\" + batchnameLocal + ".cmp", arrbyteCMP);
                        }
                        catch { }

                        int cn = listclname.Count;
                        TimeSpan span = DateTime.Now - datebeforeN;
                        int ms = (int)span.TotalMilliseconds;
                        //Save nouhin
                        workdb.Save_nouhin(tableform, userID, ms);
                        datebeforeN = DateTime.Now;
                        MessageBox.Show("Completed!");
                    }
                    else
                    {
                        MessageBox.Show("View Txt trước khi save");
                    }
                }
            }
        }

        private void bgwTxt2_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int vl = Lines.Length;// / 2;
            for (int i = vl; i < Lines.Length; i++)
            {
                vlchange2++;
                worker.ReportProgress(vlchange2);
                string tp = "";
                string line = "";
                string[] Fields;
                line = Lines[i];
                if (line.IndexOf("A6180713982039") != -1)
                {

                }
                //cắt dữ liệu các trường đưa vào mảng 1.8.2015
                Fields = line.Split(new char[] { ',' });
                //lấy dữ liệu để filter 1.8.2015 
                string idLine = Fields[2].Trim('"');
                DataRow[] rowLV1_Kojin = LV1_Kojin.Select("Page like '" + idLine + "%'");
                DataRow[] rowLV3_Kojin_Kana = LV3_Kojin_Kana.Select("Page like '" + idLine + "%'");
                int SelectedIndex = 0;
                if (rowLV1_Kojin.Length > 0)
                {
                    SelectedIndex = LV1_Kojin.Rows.IndexOf(rowLV1_Kojin[0]);
                }
                if (rowLV1_Kojin.Length == 0)
                {
                    error.Add(idLine + ": không có file LV1");
                    continue;
                }
                if (rowLV1_Kojin.Length >= 2)
                {
                    error.Add(idLine + ": trùng nhau");
                    continue;
                }
                if (rowLV3_Kojin_Kana.Length == 0)
                {
                    error.Add(idLine + ": không có file LV3");
                    continue;
                }
                if (rowLV3_Kojin_Kana.Length >= 2)
                {
                    error.Add(idLine + ": trùng nhau LV3");
                    continue;
                }
                string cotB_lv1 = rowLV1_Kojin[0].ItemArray[1].ToString().ToUpper();
                if (cotB_lv1 != "NULL")
                {
                    if (cotB_lv1.Contains('/'))
                        cotB_lv1 = "    ";
                    else if (cotB_lv1.Length == 4)
                    {
                        //Bước1
                        string v1 = cotB_lv1.Substring(0, 1);
                        string v2 = cotB_lv1.Substring(1, 1);
                        string v3 = cotB_lv1.Substring(2, 1);
                        string v4 = cotB_lv1.Substring(3, 1);
                        if (arrAlphabet.Contains(v1) && arrAlphabet.Contains(v2))
                        {
                            cotB_lv1 = "00" + cotB_lv1.Substring(2, 2);
                        }
                        if (arrAlphabet.Contains(v3) && arrAlphabet.Contains(v4))
                        {
                            cotB_lv1 = cotB_lv1.Substring(0, 2) + "00";
                        }
                        //Bước 2
                        for (int a = 0; a < 4; a++)
                        {
                            string vltemp = cotB_lv1[a].ToString();
                            int index = Array.IndexOf(arrAlphabet, vltemp) + 1;
                            if (index > 0 && index < 10)
                            {
                                cotB_lv1 = cotB_lv1.Remove(a, 1);
                                cotB_lv1 = cotB_lv1.Insert(a, index.ToString());
                            }
                        }
                        int idex1 = Array.IndexOf(arrAlphabet, cotB_lv1.Substring(0, 1)) + 1;
                        int idex3 = Array.IndexOf(arrAlphabet, cotB_lv1.Substring(2, 1)) + 1;
                        if (idex1 > 9)
                        {
                            cotB_lv1 = cotB_lv1.Remove(0, 1);
                            cotB_lv1 = cotB_lv1.Insert(0, "0");
                        }
                        if (idex3 > 9)
                        {
                            cotB_lv1 = cotB_lv1.Remove(2, 1);
                            cotB_lv1 = cotB_lv1.Insert(2, "0");
                        }
                        //Bước 3
                        int id = Array.IndexOf(arrAlphabet, cotB_lv1.Substring(1, 1));
                        if (id > 8)
                        {
                            if (cotB_lv1.Substring(0, 1) == "0" || cotB_lv1.Substring(0, 1) == "-" || cotB_lv1.Substring(0, 1) == "*")
                            {
                                if (id <= 18)
                                {
                                    cotB_lv1 = "1" + (id - 9) + cotB_lv1.Substring(2, 2);
                                }
                                else
                                {
                                    cotB_lv1 = "2" + (id - 19) + cotB_lv1.Substring(2, 2);
                                }
                            }
                            else
                            {
                                cotB_lv1 = cotB_lv1.Substring(0, 1) + "0" + cotB_lv1.Substring(2, 2);
                            }
                        }
                        id = Array.IndexOf(arrAlphabet, cotB_lv1.Substring(3, 1));
                        if (id > 8)
                        {
                            if (cotB_lv1.Substring(2, 1) == "0" || cotB_lv1.Substring(2, 1) == "-" || cotB_lv1.Substring(2, 1) == "*")
                            {
                                if (id <= 18)
                                {
                                    cotB_lv1 = cotB_lv1.Substring(0, 2) + "1" + (id - 9);
                                }
                                else
                                {
                                    cotB_lv1 = cotB_lv1.Substring(0, 2) + "2" + (id - 19);
                                }
                            }
                            else
                            {
                                cotB_lv1 = cotB_lv1.Substring(0, 2) + cotB_lv1.Substring(2, 1) + "0";
                            }
                        }
                        if (cotB_lv1.Contains('*') || cotB_lv1.Contains('-'))
                            cotB_lv1 = "    ";
                    }
                    else if (cotB_lv1.Length == 6)
                    {
                        if (cotB_lv1.Count(x => char.IsDigit(x)) != 6)
                        {
                            cotB_lv1 = "      ";
                        }
                    }
                    else
                    {
                        if (cotB_lv1 == "")
                        {
                            error.Add(string.Format("{0}: SBD trống", idLine));
                        }
                        else
                        {
                            error.Add(string.Format("{0}: SBD không đúng 4 hoặc 6 ký tự", idLine));
                        }
                    }
                    //thay thế chuỗi space bằng chuỗi nhập nếu chuỗi nhập không đủ thì vẫn để space
                    cotB_lv1 = String.Format("{0,-6}", cotB_lv1);
                    // cột 71 _ 6 ký tự _ thêm LV1_Kojin cột B
                    tp = line.Substring(63, 6);
                    if (tp.Contains(','))
                        line = line.Insert(63, cotB_lv1);
                    else
                    {
                        line = line.Remove(63, 6);
                        line = line.Insert(63, cotB_lv1);
                    }
                }

                // cột 75 _ 1 ký tự _ thêm space
                line = line.Insert(72, " ");
                // cột 85 _ 7 ký tự _ thêm space
                line = line.Insert(76, "       ");


              //  string cotB_lv3 = rowLV3_Kojin_Kana[0].ItemArray[1].ToString();
                string cotB_lv3 = removeSpace(rowLV3_Kojin_Kana[0].ItemArray[1].ToString().Trim());
                if (check_full_width(cotB_lv3))
                {
                    error.Add(string.Format("{0}: có xuất hiện chữ 2 byte", idLine));
                }

                cotB_lv3 = String.Format("{0,-16}", cotB_lv3);
                //dài hơn 16 ký tự thì chỉ lấy 16 ký tự 3/8/2015
                if (cotB_lv3.Length > 16)
                {
                    cotB_lv3 = cotB_lv3.Substring(0, 16);
                }

                //chuyển chữ nhỏ thành chữ to 3/8/2015
                cotB_lv3 = cotB_lv3.Replace("ｬ", "ﾔ");
                cotB_lv3 = cotB_lv3.Replace("ｭ", "ﾕ");
                cotB_lv3 = cotB_lv3.Replace("ｮ", "ﾖ");
                cotB_lv3 = cotB_lv3.Replace("ｧ", "ｱ");
                cotB_lv3 = cotB_lv3.Replace("ｨ", "ｲ");
                cotB_lv3 = cotB_lv3.Replace("ｩ", "ｳ");
                cotB_lv3 = cotB_lv3.Replace("ｪ", "ｴ");
                cotB_lv3 = cotB_lv3.Replace("ｫ", "ｵ");
                cotB_lv3 = cotB_lv3.Replace("ｯ", "ﾂ");
                cotB_lv3 = cotB_lv3.Replace("ヵ", "ｶ");
                cotB_lv3 = cotB_lv3.ToUpper();

                tp = line.Substring(86, 16);
                if (tp.Contains(','))
                    line = line.Insert(86, cotB_lv3);
                else
                {
                    line = line.Remove(86, 16);
                    line = line.Insert(86, cotB_lv3);
                }

                // cột 112 _ 5 ký tự _ thêm space 2 byte
                line = line.Insert(105, "　　　　　");
                // cột 120 _ 5 ký tự _ thêm space 2 byte
                line = line.Insert(113, "　　　　　");

                tp = line.Substring(121, 2);
                if (!tp.Contains(','))
                    line = line.Remove(121, 2);

                if (rowLV1_Kojin[0].ItemArray[2].ToString().ToUpper() != "NULL")
                {
                    string cotC_lv1 = rowLV1_Kojin[0].ItemArray[2].ToString();
                    if (cotC_lv1 == "")
                    {
                        line = line.Insert(121, "00");
                        tp = line.Substring(126, 2);
                        if (tp.Contains(','))
                            line = line.Insert(126, "00");
                        else
                        {
                            line = line.Remove(126, 2);
                            line = line.Insert(126, "00");
                        }
                        // cột 135 _ 2 ký tự _ thêm LV1_Kojin cột C trước + _ ngày
                        tp = line.Substring(131, 2);
                        if (tp.Contains(','))
                            line = line.Insert(131, "00");
                        else
                        {
                            line = line.Remove(131, 2);
                            line = line.Insert(131, "00");
                        }
                        tp = line.Substring(136, 1);
                        if (tp.Contains(','))
                            line = line.Insert(136, "0");
                        else
                        {
                            line = line.Remove(136, 1);
                            line = line.Insert(136, "0");
                        }
                    }
                    else
                    {
                        string firstC1 = "";
                        string firstC2 = "";
                        string firstC3 = "00";
                        //split cột C L1_Kojin
                        string firstC = cotC_lv1;
                        // firstC = firstC.Replace("*", " ");
                        firstC = firstC.Replace("-", " ");
                        firstC = firstC.Replace("/", " ");
                        firstC = firstC.Replace(" ", "a");
                        if (firstC.Length == 4)
                        {
                            firstC1 = firstC.Substring(0, 2).ToLower().Contains("a") ? "00" : firstC.Substring(0, 2);
                            firstC2 = firstC.Substring(2, 2).ToLower().Contains("a") ? "00" : firstC.Substring(2, 2);
                        }
                        else if (firstC.Length == 6)
                        {
                            firstC1 = firstC.Substring(2, 2).ToLower().Contains("a") ? "00" : firstC.Substring(2, 2);
                            firstC2 = firstC.Substring(4, 2).ToLower().Contains("a") ? "00" : firstC.Substring(4, 2);
                            firstC3 = firstC.Substring(0, 2).ToLower().Contains("a") ? "00" : firstC.Substring(0, 2);
                            firstC = firstC.Substring(2);
                        }
                        else
                        {
                            error.Add(string.Format("{0}: ngày sinh", idLine));
                            continue;
                        }
                        int n;
                        if (int.TryParse(firstC, out n))
                        {
                            string thangngay = "";
                            try
                            {
                                thangngay = "2000" + (firstC.Length == 6 ? firstC.Substring(2) : firstC);
                                DateTime.ParseExact(thangngay, "yyyyMMdd", CultureInfo.InvariantCulture);
                                // cột 125 _ 2 ký tự _ thêm LV1_Kojin cột C trước + _ năm 00                           
                                line = line.Insert(121, firstC3);
                                // cột 130 _ 2 ký tự _ thêm LV1_Kojin cột C trước + _ tháng   
                                tp = line.Substring(126, 2);
                                if (tp.Contains(','))
                                    line = line.Insert(126, firstC1);
                                else
                                {
                                    line = line.Remove(126, 2);
                                    line = line.Insert(126, firstC1);
                                }
                                // cột 135 _ 2 ký tự _ thêm LV1_Kojin cột C trước + _ ngày
                                tp = line.Substring(131, 2);
                                if (tp.Contains(','))
                                    line = line.Insert(131, firstC2);
                                else
                                {
                                    line = line.Remove(131, 2);
                                    line = line.Insert(131, firstC2);
                                }
                            }
                            catch
                            {
                                error.Add(string.Format("{0}: ngày sinh", idLine));
                                line = line.Insert(121, firstC3);
                                if (IsNumber(firstC1) && IsNumber(firstC2))
                                {
                                    int thang = Convert.ToInt32(firstC1);
                                    if (thang >= 1 && thang <= 12)
                                    {
                                        tp = line.Substring(126, 2);
                                        if (tp.Contains(','))
                                            line = line.Insert(126, firstC1);
                                        else
                                        {
                                            line = line.Remove(126, 2);
                                            line = line.Insert(126, firstC1);
                                        }
                                    }
                                    else
                                    {
                                        tp = line.Substring(126, 2);
                                        if (tp.Contains(','))
                                            line = line.Insert(126, "00");
                                        else
                                        {
                                            line = line.Remove(126, 2);
                                            line = line.Insert(126, "00");
                                        }
                                    }
                                    int ngay = Convert.ToInt32(firstC2);
                                    switch (thang)
                                    {
                                        case 1:
                                        case 3:
                                        case 5:
                                        case 7:
                                        case 8:
                                        case 10:
                                        case 12:
                                            if (ngay >= 1 && ngay <= 31)
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, firstC2);
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, firstC2);
                                                }
                                            }
                                            else
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, "00");
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, "00");
                                                }
                                            }
                                            break;
                                        case 4:
                                        case 6:
                                        case 9:
                                        case 11:
                                            if (ngay >= 1 && ngay <= 30)
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, firstC2);
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, firstC2);
                                                }
                                            }
                                            else
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, "00");
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, "00");
                                                }
                                            }
                                            break;
                                        case 2:
                                            if (ngay >= 1 && ngay <= 29)
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, firstC2);
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, firstC2);
                                                }
                                            }
                                            else
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, "00");
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, "00");
                                                }
                                            }
                                            break;
                                        default:
                                            if (ngay >= 1 && ngay <= 31)
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, firstC2);
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, firstC2);
                                                }
                                            }
                                            else
                                            {
                                                tp = line.Substring(131, 2);
                                                if (tp.Contains(','))
                                                    line = line.Insert(131, "00");
                                                else
                                                {
                                                    line = line.Remove(131, 2);
                                                    line = line.Insert(131, "00");
                                                }
                                            }
                                            break;
                                    }

                                }
                                else
                                {
                                    error.Add(string.Format("{0}: ngày sinh", idLine));
                                }
                            }
                        }
                        else
                        {

                            int b;
                            if (int.TryParse(firstC1, out b))
                            {
                                if (b < 0 || b > 12)
                                {
                                    error.Add(string.Format("{0}: tháng không hợp lệ", idLine));
                                }
                            }
                            else
                            {
                                error.Add(string.Format("{0}: tháng có ký tự đặc biệt", idLine));
                            }
                            int c;
                            if (int.TryParse(firstC2, out c))
                            {
                                if (c < 0 || c > 31)
                                {
                                    error.Add(string.Format("{0}: ngày không hợp lệ", idLine));
                                }
                            }
                            else
                            {
                                error.Add(string.Format("{0}: ngày có ký tự đặc biệt", idLine));
                            }

                            // cột 125 _ 2 ký tự _ thêm LV1_Kojin cột C trước + _ năm 00
                            line = line.Insert(121, firstC3);
                            // cột 130 _ 2 ký tự _ thêm LV1_Kojin cột C trước + _ tháng    
                            tp = line.Substring(126, 2);
                            if (tp.Contains(','))
                                line = line.Insert(126, firstC1);
                            else
                            {
                                line = line.Remove(126, 2);
                                line = line.Insert(126, firstC1);
                            }
                            // cột 135 _ 2 ký tự _ thêm LV1_Kojin cột C trước + _ ngày
                            tp = line.Substring(131, 2);
                            if (tp.Contains(','))
                                line = line.Insert(131, firstC2);
                            else
                            {
                                line = line.Remove(131, 2);
                                line = line.Insert(131, firstC2);
                            }
                        }
                    }
                }
                else
                {
                    string[] splitNam = Lines[i].Split(',');
                    string namString = splitNam[15].Replace("\"", "");
                    if (!IsNumber(namString))
                    {
                        line = line.Insert(121, "00");
                    }
                    else
                    {
                        line = line.Insert(121,  namString );
                    }
                }

                // cột 152 _ 10 ký tự _ thêm space
                line = line.Insert(140, "          ");
                // cột 156 _ 1 ký tự _ thêm space
                line = line.Insert(153, " ");
                // cột 160 _ 1 ký tự _ thêm space
                line = line.Insert(157, " ");
                // cột 181 _ 6 ký tự _ thêm space
                line = line.Insert(173, "      ");
                // cột 190 _ 6 ký tự _ thêm space
                line = line.Insert(182, "      ");
                // cột 199 _ 6 ký tự _ thêm space
                line = line.Insert(191, "      ");
                // cột 208 _ 6 ký tự _ thêm space
                line = line.Insert(200, "      ");
                // cột 217 _ 6 ký tự _ thêm space
                line = line.Insert(209, "      ");
                // cột 226 _ 6 ký tự _ thêm space
                line = line.Insert(218, "      ");
                // cột 235 _ 6 ký tự _ thêm space
                line = line.Insert(227, "      ");
                // cột 244 _ 6 ký tự _ thêm space
                line = line.Insert(236, "      ");
                // cột 253 _ 6 ký tự _ thêm space
                line = line.Insert(245, "      ");
                // cột 262 _ 6 ký tự _ thêm space
                line = line.Insert(254, "      ");
                // cột 271 _ 6 ký tự _ thêm space
                line = line.Insert(263, "      ");
                // cột 280 _ 6 ký tự _ thêm space
                line = line.Insert(272, "      ");
                // cột 289 _ 6 ký tự _ thêm space
                line = line.Insert(281, "      ");
                // cột 298 _ 6 ký tự _ thêm space
                line = line.Insert(290, "      ");
                // cột 307 _ 6 ký tự _ thêm space
                line = line.Insert(299, "      ");
                // cột 316 _ 6 ký tự _ thêm space
                line = line.Insert(308, "      ");
                // cột 325 _ 6 ký tự _ thêm space
                line = line.Insert(317, "      ");
                line = line.Replace("*", " ");
                if (KiemTraKyTuDacBiet(line) != "")
                {
                    error.Add(string.Format("{0}: có 1 ký tự đặc biệt", idLine));
                    continue;
                }
                // chuyển "ｰ" thành "-"
                line = line.Replace("ｰ", "-");
                line = line.Replace("ー", "-");
                Lines[i] = line;
            }
        }

        private void bgwForm22_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int vl = Lines.Length / 2;
            for (int i = vl; i < Lines.Length; i++)
            {
                vlchange2++;
                worker.ReportProgress(vlchange2);
                string line = "";
                string[] Fields;
                line = Lines[i];
                //cắt dữ liệu các trường đưa vào mảng 1.8.2015
                Fields = line.Split(new char[] { ',' });
                //lấy dữ liệu để filter 1.8.2015 
                string idLine = Fields[2].Trim('"');
                DataRow[] rowLV1_Zokusei;
                if (phanloai == "2")
                    rowLV1_Zokusei = LV1_Zokusei2.Select("Page like '" + idLine + "%'");
                else
                    rowLV1_Zokusei = LV1_Zokusei3.Select("Page like '" + idLine + "%'");
                if (rowLV1_Zokusei.Length == 0)
                {
                    error.Add(idLine + ": không có file");
                    continue;
                }
                if (rowLV1_Zokusei.Length >= 2)
                {
                    error.Add(idLine + ": trùng nhau");
                    continue;
                }

                // cột 72 _ 7 ký tự _ thêm space
                line = line.Insert(63, "       ");
                // cột 125 _ 50 ký tự _ thêm space
                line = line.Insert(73, "　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　");
                // cột 129 _ 1 ký tự _ thêm space
                line = line.Insert(126, " ");
                //thay đổi lại cột B thành G 08/08    

                string tp = line.Substring(130, 2);
                if (!tp.Contains(','))
                    line = line.Remove(130, 1);
                string cotG_lv1 = "";
                if (phanloai == "2")
                {
                    cotG_lv1 = rowLV1_Zokusei[0].ItemArray[1].ToString() + "+2";
                }
                else
                {
                    cotG_lv1 = rowLV1_Zokusei[0].ItemArray[1].ToString() + "+" + rowLV1_Zokusei[0].ItemArray[2].ToString();
                }
                if ("+".Equals(cotG_lv1))
                {
                    cotG_lv1 = "0+3";
                }
                string[] splitG = cotG_lv1.Split('+');
                if (splitG.Length != 2)
                {
                    error.Add(String.Format(idLine + ": Phân loại không đúng"));
                    continue;
                }
                string firstG = splitG[0];
                // cột 133 _ 1 ký tự _ thêm cột B trước +
                if ("".Equals(firstG))
                {
                    line = line.Insert(130, "0");
                }
                else
                {
                    line = line.Insert(130, firstG);
                }
                string lastG = splitG[1];
                tp = line.Substring(131, 2);
                if (!tp.Contains(','))
                    line = line.Remove(131, 1);
                // cột 137 _ 1 ký tự _ thêm cột B sau +
                if ("".Equals(lastG))
                {
                    line = line.Insert(134, "3");
                }
                else
                {
                    line = line.Insert(134, lastG);
                }
                //thay đổi cột C thành cột F 08/08
                string cotF_lv1;
                if (phanloai == "2")
                {
                    cotF_lv1 = string.Join("+", rowLV1_Zokusei[0].ItemArray.Skip(2).Take(18));
                }
                else
                {
                    cotF_lv1 = string.Join("+", rowLV1_Zokusei[0].ItemArray.Skip(3).Take(22));
                }
                string[] splitF = cotF_lv1.Split('+');
                for (int a = 0; a < splitF.Length; a++)
                {
                    if (splitF[a] == "")
                        splitF[a] = "zzz";
                }
                //sort từ nhỏ tới lớn
                Array.Sort(splitF);
                // cột 143-299 _ 3 ký tự _ thêm cột C
                int positionF = 138;
                for (int j = 0; j < 27; j++)
                {
                    tp = line.Substring(positionF, 3);
                    if (!tp.Contains(','))
                        line = line.Remove(positionF, 3);
                    string cotF = "   ";
                    if (j < splitF.Length)
                    {
                        cotF = splitF[j].Replace('z', ' ');
                    }
                    line = line.Insert(positionF, cotF);
                    positionF += 6;
                }
                // cột 307 _ 3 ký tự _ thêm 00000
                line = line.Insert(300, "00000");
                // cột 311 _ 1 ký tự _ thêm space
                line = line.Insert(308, " ");
                // cột 318 _ 7 ký tự _ thêm 0000
                line = line.Insert(312, "0000");
                // cột 330 _ 9 ký tự _ thêm space
                line = line.Insert(319, "         ");
                // cột 335 _ 2 ký tự _ thêm space
                line = line.Insert(331, "  ");
                // cột 341 _ 3 ký tự _ thêm space
                line = line.Insert(336, "   ");
                // cột 346 _ 2 ký tự _ thêm space
                line = line.Insert(342, "  ");
                // cột 352 _ 3 ký tự _ thêm space
                line = line.Insert(347, "   ");
                // cột 357 _ 2 ký tự _ thêm space
                line = line.Insert(353, "  ");
                // cột 362 _ 2 ký tự _ thêm space
                line = line.Insert(358, "  ");
                // cột 415 _ 50 ký tự _ thêm space
                line = line.Insert(363, "                                                  ");
                // cột 448 _ 30 ký tự _ thêm space
                line = line.Insert(416, "                              ");
                // cột 491 _ 40 ký tự _ thêm space
                line = line.Insert(449, "                                        ");

                //Code1
                string code1 = "";
                if (phanloai == "2")
                {
                    code1 = rowLV1_Zokusei[0].ItemArray[20].ToString();
                }
                else
                {
                    code1 = rowLV1_Zokusei[0].ItemArray[25].ToString();
                }
                bool blcode1 = false;
                tp = line.Substring(492, 10);
                if (!tp.Contains(','))
                    line = line.Remove(492, 10);
                if (code1 == "")
                {
                    line = line.Insert(492, "          ");
                    blcode1 = true;
                }
                else
                {
                    if (code1.Length == 8 || code1.Length == 10)
                    {
                        string vl4f = code1.Substring(0, 4);
                        int number = 0;
                        if (!int.TryParse(vl4f.Replace(' ', 'a').Replace('-', 'a'), out number))
                        {
                            line = line.Insert(492, "          ");
                        }
                        else
                        {
                            string vl56 = code1.Substring(4, 2);
                            if (!int.TryParse(vl56.Replace(' ', 'a').Replace('-', 'a'), out number))
                            {
                                vl56 = "00";
                            }
                            string vl78 = code1.Substring(6, 2);
                            if (!int.TryParse(vl78.Replace(' ', 'a').Replace('-', 'a'), out number))
                            {
                                vl78 = "00";
                            }
                            string vl9 = " "; string vl10 = " ";
                            if (code1.Length == 10)
                            {
                                vl9 = code1.Substring(8, 1);
                                if (!int.TryParse(vl9.Replace(' ', 'a').Replace('-', 'a'), out number))
                                {
                                    vl9 = "0";
                                }
                                vl10 = code1.Substring(9, 1);
                                if (!int.TryParse(vl10.Replace(' ', 'a').Replace('-', 'a'), out number))
                                {
                                    vl10 = "0";
                                }
                            }
                            line = line.Insert(492, vl4f + vl56 + vl78 + vl9 + vl10);
                        }
                    }
                    else
                    {
                        error.Add(String.Format("{0}: Code1 có {1} ký tự", idLine, code1.Length));
                        continue;
                    }
                }
                //code2-8  
                List<string> arr2_8 = new List<string>();
                if (phanloai == "2")
                {
                    arr2_8.AddRange(string.Join("+", rowLV1_Zokusei[0].ItemArray.Skip(21).Take(3)).Split('+'));
                }
                else
                {
                    arr2_8.AddRange(string.Join("+", rowLV1_Zokusei[0].ItemArray.Skip(26).Take(7)).Split('+'));
                }
                bool blvl = false;
                int position = 505;
                for (int a = 0; a < 7; a++)
                {
                    tp = line.Substring(position, 10);
                    if (!tp.Contains(','))
                        line = line.Remove(position, 10);
                    if (a < arr2_8.Count)
                    {
                        code1 = arr2_8[a].ToString();
                        if (code1 == "")
                        {
                            code1 = "          ";
                            arr2_8[a] = "aaaaaaaaaa";
                        }
                        else if (code1.Length == 8 || code1.Length == 10)
                        {
                            string vl4f = code1.Substring(0, 4);
                            int number = 0;
                            if (!int.TryParse(vl4f.Replace(' ', 'a').Replace('-', 'a'), out number))
                            {
                                arr2_8[a] = "          ";
                            }
                            else
                            {
                                string vl56 = code1.Substring(4, 2);
                                if (!int.TryParse(vl56.Replace(' ', 'a').Replace('-', 'a'), out number))
                                {
                                    vl56 = "00";
                                }
                                string vl78 = code1.Substring(6, 2);
                                if (!int.TryParse(vl78.Replace(' ', 'a').Replace('-', 'a'), out number))
                                {
                                    vl78 = "00";
                                }
                                string vl9 = " "; string vl10 = " ";
                                if (code1.Length == 10)
                                {
                                    vl9 = code1.Substring(8, 1);
                                    if (!int.TryParse(vl9.Replace(' ', 'a').Replace('-', 'a'), out number))
                                    {
                                        vl9 = "0";
                                    }
                                    vl10 = code1.Substring(9, 1);
                                    if (!int.TryParse(vl10.Replace(' ', 'a').Replace('-', 'a'), out number))
                                    {
                                        vl10 = "0";
                                    }
                                }
                                arr2_8[a] = vl4f + vl56 + vl78 + vl9 + vl10;
                            }
                        }
                        else
                        {
                            error.Add(String.Format("{0}: Code{1} có {2} ký tự", idLine, a + 2, code1.Length));
                            blvl = true;
                            break;
                        }
                    }
                    line = line.Insert(position, "          ");
                    position += 13;
                }
                if (blvl)
                {
                    continue;
                }
                if (blcode1)
                    position = 492;
                else
                    position = 505;
                for (int a = 0; a < 7; a++)
                {
                    if (arr2_8.Count > a)
                    {
                        if (a < arr2_8.Count)
                        {
                            if (arr2_8[a] != "aaaaaaaaaa")
                            {
                                line = line.Remove(position, 10);
                                line = line.Insert(position, arr2_8[a]);
                                position += 13;
                            }
                            //line = line.Remove(position, 10);
                            //line = line.Insert(position, arr2_8[a]);
                            //position += 13;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                // cột 605 _ 3 ký tự _ thêm space
                line = line.Insert(600, "   ");
                // cột 648 _ 40 ký tự _ thêm space
                line = line.Insert(606, "                                        ");
                // cột 811 _ 160 ký tự _ thêm space
                line = line.Insert(649, "                                                                                                                                                                ");
                // cột 816 _ 2 ký tự _ thêm space
                line = line.Insert(812, "  ");
                // cột 821 _ 2 ký tự _ thêm space
                line = line.Insert(817, "  ");
                // cột 826 _ 2 ký tự _ thêm space
                line = line.Insert(822, "  ");
                // cột 831 _ 2 ký tự _ thêm space
                line = line.Insert(827, "  ");
                // cột 836 _ 2 ký tự _ thêm space
                line = line.Insert(832, "  ");
                // cột 841 _ 2 ký tự _ thêm space
                line = line.Insert(837, "  ");
                // cột 874 _ 30 ký tự _ thêm space
                line = line.Insert(842, "                              ");
                // cột 897 _ 20 ký tự _ thêm space
                line = line.Insert(875, "                    ");
                // cột 940 _ 40 ký tự _ thêm space
                line = line.Insert(898, "                                        ");
                // cột 946 _ 3 ký tự _ thêm space
                line = line.Insert(941, "   ");
                // cột 1049 _ 100 ký tự _ thêm space
                line = line.Insert(947, "                                                                                                    ");
                // cột 1252 _ 200 ký tự _ thêm space
                line = line.Insert(1050, "                                                                                                                                                                                                        ");
                // cột 1258 _ 3 ký tự _ thêm space
                line = line.Insert(1253, "   ");
                // cột 1301 _ 40 ký tự _ thêm space
                line = line.Insert(1259, "                                        ");
                // cột 1324 _ 20 ký tự _ thêm space
                line = line.Insert(1302, "                    ");
                // cột 1329 _ 2 ký tự _ thêm space
                line = line.Insert(1325, "  ");
                // cột 1334 _ 2 ký tự _ thêm space
                line = line.Insert(1330, "  ");
                // cột 1339 _ 2 ký tự _ thêm space
                line = line.Insert(1335, "  ");
                // cột 1344 _ 1 ký tự _ thêm space
                line = line.Insert(1344, " ");
                // cột 1348 _ 2 ký tự _ thêm space
                line = line.Insert(1348, "  ");
                // cột 1353 _ 25 ký tự _ thêm space lớn
                line = line.Insert(1353, "　　　　　　　　　　　　　　　　　　　　　　　　　");
                // cột 1381 _ 25 ký tự _ thêm space lớn
                line = line.Insert(1381, "　　　　　　　　　　　　　　　　　　　　　　　　　");
                line = line.Replace("*", " ");
                if (KiemTraKyTuDacBiet(line) != "")
                {
                    error.Add(String.Format("{0}: có ký tự đặc biệt!", idLine));
                }
                Lines[i] = line;
            }
        }

        private void bgwTouan2_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int vl = Lines.Length / 2;
            for (int i = vl; i < Lines.Length; i++)
            {
                vlchange2++;
                worker.ReportProgress(vlchange2);
                string line = "";
                string[] Fields;
                line = Lines[i];
                //cắt dữ liệu các trường đưa vào mảng 1.8.2015
                Fields = line.Split(new char[] { ',' });
                //lấy dữ liệu để filter 1.8.2015 
                string idLine = Fields[1].Trim('"');
                DataRow[] rowLV1_Kojin = LV1_Kojin.Select("Page like '" + idLine + "%'");
                DataRow[] rowLV3_Kojin_Kana = LV3_Kojin_Kana.Select("Page like '" + idLine + "%'");
                int SelectedIndex = 0;
                if (rowLV1_Kojin.Length > 0)
                {
                    SelectedIndex = LV1_Kojin.Rows.IndexOf(rowLV1_Kojin[0]);
                }
                if (rowLV1_Kojin.Length == 0)
                {
                    error.Add(idLine + ": không có file LV1");
                    continue;
                }
                if (rowLV1_Kojin.Length >= 2)
                {
                    error.Add(idLine + ": trùng nhau");
                    continue;
                }
                if (rowLV3_Kojin_Kana.Length == 0)
                {
                    error.Add(idLine + ": không có file LV3");
                    continue;
                }
                if (rowLV3_Kojin_Kana.Length >= 2)
                {
                    error.Add(idLine + ": trùng nhau LV3");
                    continue;
                }
                string cotB_lv1 = rowLV1_Kojin[0].ItemArray[1].ToString().ToUpper();
                if (cotB_lv1.Length < 4 && cotB_lv1.Length > 0)
                {
                    error.Add(string.Format("{0}, SBD không đúng 4 ký tự", idLine));
                    continue;
                }
                cotB_lv1 = cotB_lv1.Replace('-', 'a').Replace(' ', 'a');
                if (!IsNumber(cotB_lv1))
                {
                    cotB_lv1 = "      ";
                }
                //thay thế chuỗi space bằng chuỗi nhập nếu chuỗi nhập không đủ thì vẫn để space
                cotB_lv1 = String.Format("{0,-6}", cotB_lv1);
                // cột 71 _ 6 ký tự _ thêm LV1_Kojin cột B
                string tp = line.Substring(74, 6);
                if (tp.Contains(','))
                    line = line.Insert(74, cotB_lv1);
                else
                {
                    line = line.Remove(74, 6);
                    line = line.Insert(74, cotB_lv1);
                }
                // cột 75 _ 1 ký tự _ thêm space
                line = line.Insert(83, " ");
                // cột 85 _ 7 ký tự _ thêm space
                line = line.Insert(87, "       ");

              //  string cotB_lv3 = rowLV3_Kojin_Kana[0].ItemArray[1].ToString();
                string cotB_lv3 = removeSpace(rowLV3_Kojin_Kana[0].ItemArray[1].ToString().Trim());
                if (check_full_width(cotB_lv3))
                {
                    error.Add(string.Format("{0}: có xuất hiện chữ 2 byte", idLine));
                }

                cotB_lv3 = String.Format("{0,-16}", cotB_lv3);
                //dài hơn 16 ký tự thì chỉ lấy 16 ký tự 3/8/2015
                if (cotB_lv3.Length > 16)
                {
                    cotB_lv3 = cotB_lv3.Substring(0, 16);
                }

                //chuyển chữ nhỏ thành chữ to 3/8/2015
                cotB_lv3 = cotB_lv3.Replace("ｬ", "ﾔ");
                cotB_lv3 = cotB_lv3.Replace("ｭ", "ﾕ");
                cotB_lv3 = cotB_lv3.Replace("ｮ", "ﾖ");
                cotB_lv3 = cotB_lv3.Replace("ｧ", "ｱ");
                cotB_lv3 = cotB_lv3.Replace("ｨ", "ｲ");
                cotB_lv3 = cotB_lv3.Replace("ｩ", "ｳ");
                cotB_lv3 = cotB_lv3.Replace("ｪ", "ｴ");
                cotB_lv3 = cotB_lv3.Replace("ｫ", "ｵ");
                cotB_lv3 = cotB_lv3.Replace("ｯ", "ﾂ");
                cotB_lv3 = cotB_lv3.Replace("ヵ", "ｶ");
                cotB_lv3 = cotB_lv3.ToUpper();

                tp = line.Substring(97, 16);
                if (tp.Contains(','))
                    line = line.Insert(97, cotB_lv3);
                else
                {
                    line = line.Remove(97, 16);
                    line = line.Insert(97, cotB_lv3);
                }

                // cột 112 _ 5 ký tự _ thêm space 2 byte
                line = line.Insert(116, " ");
                line = line.Replace("*", " ");
                if (KiemTraKyTuDacBiet(line) != "")
                {
                    error.Add(String.Format("{0}: có ký tự đặc biệt", idLine));
                }
                // chuyển "ｰ" thành "-"
                line = line.Replace("ｰ", "-");
                line = line.Replace("ー", "-");
                Lines[i] = line;
            }
        }

        private void grThongkeV_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //grThongkeV.ActiveFilterString = "[CheckerId] does not equal ''";
            if (button1.Text == "Sort check") {
                button1.Text = "Clear Sort";
              //  grThongkeV.Columns["CheckerId"].SortMode = new DevExpress.XtraGrid.ColumnSortMode("[CheckerId]!=''");
                grThongkeV.Columns["CheckerId"].SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
                grThongkeV.FocusedRowHandle = 0;
            }

            else
            {
                button1.Text = "Sort check";
                grThongkeV.ClearSorting();
            }

        }

        private void grThongkeV_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ContextMenu contextMenu = new System.Windows.Forms.ContextMenu();
                MenuItem menuItem = new MenuItem("Check");
                menuItem.Click += new EventHandler(CheckAction);
                MenuItem menuItem1 = new MenuItem("UnCheck");
                menuItem1.Click += new EventHandler(UnCheckAction);
                contextMenu.MenuItems.Add(menuItem);
                contextMenu.MenuItems.Add(menuItem1);
                //  contextMenu.MenuItems.Add(menuItem);
                grThongke.ContextMenu = contextMenu;
            }
        }
        void CheckAction(object sender, EventArgs e)
        {
            if (formID != 3)
            {
                ColumnView View = (ColumnView)grThongke.FocusedView;
                GridColumn column = View.Columns[grThongkeV.FocusedColumn.FieldName];
                if (grThongkeV.SelectedRowsCount == 0)
                {
                    return;
                }
                int rw = grThongkeV.GetSelectedRows()[0];
                string checkid = grThongkeV.GetRowCellDisplayText(rw, grThongkeV.Columns["CheckerId"]);
                if (checkid == "")
                {
                    MessageBox.Show("Phiếu này không qua check!");
                    return;

                }

                grThongkeV.SetRowCellValue(rw, grThongkeV.Columns["LCCheck"], "1");
            }
            else
            {
                ColumnView View = (ColumnView)grThongke.FocusedView;
                GridColumn column = View.Columns[grThongkeV.FocusedColumn.FieldName];
                if (grThongkeV.SelectedRowsCount == 0)
                {
                    return;
                }
                int rw = grThongkeV.GetSelectedRows()[0];
                string checkid = grThongkeV.GetRowCellDisplayText(rw, grThongkeV.Columns["CheckerId"]);
                if (checkid == "")
                {
                    MessageBox.Show("Phiếu này không qua check!");
                    return;

                }
                string idImage = grThongkeV.GetRowCellValue(grThongkeV.FocusedRowHandle, grThongkeV.Columns["ID"]).ToString();
                DataRow[] arrRow = dt.Select("Id='" + idImage + "'");
                foreach (DataRow dr in arrRow)
                {
                    dr["LCCheck"] = "1"; 
                }
                grThongkeV.SetRowCellValue(rw, grThongkeV.Columns["LCCheck"], "1");
            }

           // MessageBox.Show("Completed!");
        }
        void UnCheckAction(object sender, EventArgs e)
        {
            if (formID != 3)
            {
                ColumnView View = (ColumnView)grThongke.FocusedView;
                GridColumn column = View.Columns[grThongkeV.FocusedColumn.FieldName];
                if (grThongkeV.SelectedRowsCount == 0)
                {
                    return;
                }
                int rw = grThongkeV.GetSelectedRows()[0];
                grThongkeV.SetRowCellValue(rw, grThongkeV.Columns["LCCheck"], "0");
            }
            else
            { 
                ColumnView View = (ColumnView)grThongke.FocusedView;
                GridColumn column = View.Columns[grThongkeV.FocusedColumn.FieldName];
                if (grThongkeV.SelectedRowsCount == 0)
                {
                    return;
                }
                int rw = grThongkeV.GetSelectedRows()[0];
                string idImage = grThongkeV.GetRowCellValue(grThongkeV.FocusedRowHandle, grThongkeV.Columns["ID"]).ToString();
                DataRow[] arrRow = dt.Select("Id='" + idImage + "'");
                foreach (DataRow dr in arrRow)
                {
                    dr["LCCheck"] = "0";
                }
                grThongkeV.SetRowCellValue(rw, grThongkeV.Columns["LCCheck"], "0");
            }
            // MessageBox.Show("Completed!");
        }
        private void txtFind_TextChanged(object sender, EventArgs e)
        {
            if (lsint.Count > 0)
            {
                rtxtKetqua.SelectionStart = lsint[0][0];
                rtxtKetqua.SelectionLength = lsint[0][1];
                rtxtKetqua.SelectionBackColor = Color.White;
            }
            
            lsint = new List<int[]>();
        }

        private void btnFindN_Click(object sender, EventArgs e)
        {
            if (lsint.Count > 0)
            {
                rtxtKetqua.SelectionStart = lsint[0][0];
                rtxtKetqua.SelectionLength = lsint[0][1];
                rtxtKetqua.SelectionBackColor = Color.White;
            }
            lsint = new List<int[]>();
            Find(rtxtKetqua, txtFind.Text, true, true, false);
        }

        private void btnFindP_Click(object sender, EventArgs e)
        {
            if (lsint.Count > 0)
            {
                rtxtKetqua.SelectionStart = lsint[0][0];
                rtxtKetqua.SelectionLength = lsint[0][1];
                rtxtKetqua.SelectionBackColor = Color.White;
            }
            lsint = new List<int[]>();
            Find(rtxtKetqua, txtFind.Text, true, true, true);
        }
        void Find(RichTextBox richText, string text, bool matchCase, bool matchWholeWord, bool upDirection)
        {
            RichTextBoxFinds options = RichTextBoxFinds.None;
            if (matchCase)
                options |= RichTextBoxFinds.MatchCase;
            if (matchWholeWord)
                options |= RichTextBoxFinds.WholeWord;
            if (upDirection)
                options |= RichTextBoxFinds.Reverse;

            int index;
            if (upDirection)
                index = richText.Find(text, 0, richText.SelectionStart, options);
            else
                index = richText.Find(text, richText.SelectionStart + richText.SelectionLength, options);

            if (index >= 0)
            {
                richText.SelectionStart = index;
                richText.SelectionLength = text.Length;
                lsint.Add(new int[2] { index, text.Length });
                richText.SelectionBackColor = Color.GreenYellow;
                richText.Select(index, text.Length);
                richText.ScrollToCaret();
            }
            else // text not found
            {
                richText.SelectionStart = 0;
                index = richText.Find(text, richText.SelectionStart + richText.SelectionLength, options);
                if (index >= 0)
                {
                    richText.SelectionStart = index;
                    richText.SelectionLength = text.Length;
                    lsint.Add(new int[2] { index, text.Length });
                    richText.SelectionBackColor = Color.GreenYellow;
                    richText.Select(index, text.Length);
                    richText.ScrollToCaret();
                }
            }
        }

        private void rtxtError_Click(object sender, EventArgs e)
        {
            string str1 = rtxtError.Text.Substring(0, rtxtError.SelectionStart);
            int index1 = str1.LastIndexOf('\n');
            if (rtxtError.Text.Length > index1 + 14)
            {
                try
                {
                    string str = rtxtError.Text.Substring(index1 + 1, 14);
                    var rws = dt.Select("Page like '" + str + "%'");
                    cboimage.Text = rws[0].ItemArray[0].ToString();
                    txtFind.Text = str;
                    btnFindN_Click(sender, e);
                }
                catch { }

            }
        }

        private void rtxtError_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down)
            {
                string str1 = rtxtError.Text.Substring(0, rtxtError.SelectionStart);
                int index1 = str1.LastIndexOf('\n');
                if (rtxtError.Text.Length > index1 + 14)
                {
                    try
                    {
                        string str = rtxtError.Text.Substring(index1 + 1, 14);
                        var rws = dt.Select("Page like '" + str + "%'");
                        cboimage.Text = rws[0].ItemArray[0].ToString();
                        txtFind.Text = str;
                        btnFindN_Click(sender, e);
                    }
                    catch { }
                }
            }
        }

        private void txtsearch_TextChanged(object sender, EventArgs e)
        {
            grThongkeV.FindFilterText = txtsearch.Text;
        }
    }

}