﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.IO;
namespace VCB_KOMOSI
{
    class WorkDB_LC
    {
        private string stringconnecttion = String.Format("Data Source=" + Program.server + ";Initial Catalog=VCB_KOMOSI_2017;User Id=entrykomosi;Password=entrykomosi;Integrated Security=no;MultipleActiveResultSets=False;Packet Size=4096;Pooling=false;Connection Timeout=1;");
        public DataTable dtLastcheck(string table)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    //sqlCommand.CommandText = "select ImageName+'|'+ResultLC+'|'+Cast(Id as nvarchar(1000)) as vl from dbo.[" + table + "] where ResultLC is not null";
                    sqlCommand.CommandText = "select ImageName+'|'+ISNULL(ResultLC,'null|null')+'|'+Cast(Id as nvarchar(1000))+'|'+ISNULL(Cast(CheckerId as nvarchar(1000)),'')+'|'+ISNULL(ResultNouhin,0) as vl from dbo.[" + table + "]";
                    SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                    sqldataAdapter.SelectCommand = sqlCommand;
                    con.Open();
                    sqldataAdapter.Fill(dt);
                }

            }
            catch (Exception ex)
            {
                throw new System.Exception("Lỗi khi lây du liêu LC\n" + ex.Message);
            }
            return dt;
        }
        public DataTable dtLastcheck_OCR(string table)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    //sqlCommand.CommandText = "select ImageName+'|'+ResultLC+'|'+Cast(Id as nvarchar(1000)) as vl from dbo.[" + table + "] where ResultLC is not null";
                    sqlCommand.CommandText = "select ImageName+'|'+ISNULL(Content_OCR,'null|null')+'|'+Cast(Id as nvarchar(1000))+'|'+ISNULL(Cast(CheckerId as nvarchar(1000)),'') as vl from dbo.[" + table + "]";
                    SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                    sqldataAdapter.SelectCommand = sqlCommand;
                    con.Open();
                    sqldataAdapter.Fill(dt);
                }

            }
            catch (Exception ex)
            {
                throw new System.Exception("Lỗi khi lây du liêu LC\n" + ex.Message);
            }
            return dt;
        }
        public DataTable dtLastcheck_Txt(string table)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    //sqlCommand.CommandText = "select ImageName+'|'+ResultLC+'|'+Cast(Id as nvarchar(1000)) as vl from dbo.[" + table + "] where ResultLC is not null";
                    sqlCommand.CommandText = "select ImageName+'|'+ISNULL(ResultLC,'null|null')+'|'+Cast(Id as nvarchar(1000))+'|'+ISNULL(Cast(CheckerId as nvarchar(1000)),'') as vl from dbo.[" + table + "]";
                    SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                    sqldataAdapter.SelectCommand = sqlCommand;
                    con.Open();
                    sqldataAdapter.Fill(dt);
                }

            }
            catch (Exception ex)
            {
                throw new System.Exception("Lỗi khi lây du liêu LC\n" + ex.Message);
            }
            return dt;
        }


        //Update số phiếu, số dòng trong Batch 
        public void UpdateBatch1(string batchname, int template, string countQA)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.Batch SET clQA1 = " + countQA + " WHERE Name = '" + batchname + "' AND clQA1=0 AND TempID=" + template;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Batch\n" + sqlException.Message);
            }
        }
        public void UpdateBatch2(string batchname, int tempid, string countQA)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.Batch SET clQA2 = " + countQA + " WHERE Name = '" + batchname + "' AND TempID=" + tempid;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Batch\n" + sqlException.Message);
            }
        }

        public string[] InsertPerformance(int batchId, int userid)
        {

            string[] id = null;
            id = new string[2];
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "InsertPerformanceLastCheck";
                    sqlCommand.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.Add("@StartTime", SqlDbType.DateTime).Direction = ParameterDirection.Output;
                    sqlCommand.Parameters.Add("@BatchID", SqlDbType.Int, 30).Value = batchId;
                    sqlCommand.Parameters.Add("@UserID", SqlDbType.Int, 10).Value = userid;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                    try
                    {
                        id[0] = sqlCommand.Parameters["@ID"].Value.ToString();
                        id[1] = sqlCommand.Parameters["@StartTime"].Value.ToString();
                    }
                    catch
                    {
                        id[0] = "";
                    }
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình đọc dữ liệu\n" + sqlException.Message);
            }
            return id;
        }
        public bool UpdatePerformance(string starttime, int ID)
        {

            bool id = false;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandText = "UpdatePerformanceLastCheck";
                    sqlCommand.Parameters.Add("@Starttime", SqlDbType.DateTime, 30).Value = starttime;
                    sqlCommand.Parameters.Add("@ID", SqlDbType.Int, 10).Value = ID;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                    id = true;
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình đọc dữ liệu\n" + sqlException.Message);
            }
            return id;
        }
        public int InsertLastcheck(int batchid, int userid, byte[] obook)
        {
            int id = 0;
            //Insert Page    
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "INSERT INTO dbo.Lastcheck (batchID,userID,FileLC) VALUES (" + batchid + "," + userid + ",@obook); Select SCOPE_IDENTITY();";
                    sqlCommand.Parameters.Add(new SqlParameter("@obook", obook));
                    con.Open();
                    id = Int32.Parse(sqlCommand.ExecuteScalar().ToString());
                }
                //sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Error! Insert page\n" + sqlException.Message);
            }
            return id;
        }

        public string Get_StringOCR(int batchid)
        {
            string ocr = "";
            //Insert Page    
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Select ContentOCR from dbo.Batch where ID=" + batchid;
                    con.Open();
                    ocr = sqlCommand.ExecuteScalar().ToString();
                }
                //sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Error! Insert page\n" + sqlException.Message);
            }
            return ocr;
        }
        //Update Lastcheck
        public void UpdateLastcheck(int batchid, int userid, byte[] obook, int id)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "UPDATE dbo.Lastcheck SET batchID = " + batchid + ",UserID=" + userid + ",FileLC=@obook WHERE ID = " + id;
                    sqlCommand.Parameters.Add(new SqlParameter("@obook", obook));
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Lastcheck\n" + sqlException.Message);
            }
        }
        public byte[] RetrieveCMPFromDatabase(int ID)
        {
            byte[] arrByte = null;

            using (SqlConnection con = new SqlConnection(stringconnecttion))
            {
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = con;
                sqlCommand.CommandText = "SELECT clcmp FROM dbo.Batch WHERE ID = @ID";
                sqlCommand.Parameters.Add("@ID", SqlDbType.Int).Value = ID;
                con.Open();
                arrByte = (byte[])sqlCommand.ExecuteScalar();
            }
            return arrByte;
        }
        public byte[] getImageOnServer(string Name, string batchname)
        {
            byte[] returnVal = null;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT Binarymage FROM dbo.[" + batchname + "] WHERE ImageName = N'" + Name + "'";
                    con.Open();
                    returnVal = (byte[])sqlCommand.ExecuteScalar();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Không có ảnh trên server\n" + sqlException.Message);
            }
            return returnVal;
        }
        /// <summary>
        /// Check batchid
        /// </summary>
        /// <param name="template"></param>        
        public int CheckBatch(int batchid)
        {
            int id = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "SELECT [ID] FROM dbo.LastCheck WHERE batchID = " + batchid;
                    con.Open();
                    id = Int32.Parse(sqlCommand.ExecuteScalar().ToString());
                }
            }
            catch
            {
            }
            return id;
        }
        public void Save_LC(string content, string id, string table,string lcCheck)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Update dbo.[" + table + "] set ResultLC = N'" + content + "',ResultNouhin=N'"+lcCheck+"' where Id =" + id;
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Có lỗi trong quá trình cập nhật Lastcheck\n" + sqlException.Message);
            }
        }
        public void Save_nouhin(string table, int userid, int ms)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Update dbo.[" + table + "] set UserNouhin=" + userid + ", TimeNouhin = " + ms + " Where Id = (select top 1 Id from dbo.[" + table + "] where ResultLC is not null)";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            {

            }
        }
        public void Save_UserLC(int userid, int ms, string table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Update dbo.[" + table + "] set LastCheckId=" + userid + ", TimeMilisecondsLC = " + ms + " Where Id = (select top 1 Id from dbo.[" + table + "] where ResultLC is not null)";
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            {

            }
        }

        public int GetBacthIdByNameAndForm(string batchname, int formid)
        {
            int id = 0;
            //Insert Page    
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "select id from dbo.Batch where Name = N'" + batchname + "' and TempID = " + formid;
                    con.Open();
                    id = Int32.Parse(sqlCommand.ExecuteScalar().ToString());
                }
                //sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException sqlException)
            {
                throw new System.Exception("Error! Insert page\n" + sqlException.Message);
            }
            return id;
        }
        public int getEntryinBatch(string nameTable)
        {
            int returnVal = -1;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "select COUNT(*) as 'Số lượng nhập' from dbo.[" + nameTable + "] join Batch on Batch.Id = cast(LEFT('" + nameTable + "',CHARINDEX('--','" + nameTable + "') - 1) as INT) WHERE dbo.[" + nameTable + "].Content_OCR LIKE '%?%'";
                    con.Open();
                    returnVal = (int)sqlCommand.ExecuteScalar();
                }
            }
            catch
            { returnVal = -1; }
            return returnVal;
        }
        public int GetQaImport(int tempid, string batchName, string namecolumn) 
        {
            int returnVal = -1;
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "select "+namecolumn+" from dbo.[Batch]  WHERE Name=N'"+batchName+"' and TempID="+tempid;
                    con.Open();
                    returnVal = (int)sqlCommand.ExecuteScalar();
                }
            }
            catch
            { returnVal = -1; }
            return returnVal;
        }
        public void SaveQaImport(int tempid, string batchName, string namecolumn,int value)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    sqlCommand.CommandText = "Update dbo.[Batch] set "+namecolumn+"= "+value+"  Where TempID ="+tempid+"  and Name=N'"+batchName+"'"; 
                    con.Open();
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch
            {

            }
        }

        public DataTable dtLastcheck_BatchZ(string table)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(stringconnecttion))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = con;
                    //sqlCommand.CommandText = "select ImageName+'|'+ResultLC+'|'+Cast(Id as nvarchar(1000)) as vl from dbo.[" + table + "] where ResultLC is not null";
                    sqlCommand.CommandText = "select ImageName as 'Page', ResultLC from dbo.[" + table + "]";
                    SqlDataAdapter sqldataAdapter = new SqlDataAdapter();
                    sqldataAdapter.SelectCommand = sqlCommand;
                    con.Open();
                    sqldataAdapter.Fill(dt);
                }

            }
            catch (Exception ex)
            {
                throw new System.Exception("Lỗi khi lây du liêu LC\n" + ex.Message);
            }
            return dt;
        }
    }
}
