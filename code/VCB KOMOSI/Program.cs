﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace VCB_KOMOSI
{
    static class Program
    {
        public static string server = "192.168.1.18,1433";
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                //117.2.164.10,1433
                //210.245.96.36:1433
                TcpClient tcp = new TcpClient();
                var result = tcp.BeginConnect("117.2.164.10", 1433, null, null);
                var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromMilliseconds(500));
                if (success)
                { server = "117.2.164.10,1433"; }
                else
                { server = "192.168.1.18,1433"; }
                string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                if (userName.ToUpper().IndexOf("THANHNH") == -1 && userName.ToUpper().IndexOf("NAMNN") == -1)
                {
                    int hwnd;
                    hwnd = FindWindow("Progman", null);
                    PostMessage(hwnd, /*WM_QUIT*/ 0x12, 0, 0);
                }

                string strfilename = "";
                string[] fileListU;
                DB_Update fc = new DB_Update();
                bool capnhat = false;
                string link = System.IO.Directory.GetCurrentDirectory().ToString();
                string tenF = fc.getTen();
                float phienban1 = float.Parse(tenF.Substring(tenF.Length - 8, 4));

                fileListU = Directory.GetFiles(link);

                //l?c file exe
                List<string> res = new List<string>();
                for (int i = 0; i < fileListU.Length; i++)
                {
                    string D = fileListU[i].Substring(fileListU[i].Length - 4, 4); ;
                    if (D == ".exe")
                    {
                        res.Add(fileListU[i]);
                    }
                }

                //kt ?a co file m?i nh?t ch?a?
                for (int i = 0; i < res.Count; i++)
                {
                    string file = System.IO.Path.GetFileName(res[i]);
                    string duoiFile = file.Substring(file.Length - 4, 4);
                    if (file == tenF)
                    {
                        capnhat = true;
                        break;
                    }  //vshost                     
                }
                if (capnhat == true)
                {
                    for (int i = 0; i < res.Count; i++)
                    {
                        string file = System.IO.Path.GetFileName(res[i]);
                        string duoiFile = file.Substring(file.Length - 4, 4);
                        float phienban = 0;
                        try
                        {
                            phienban = float.Parse(file.Substring(file.Length - 8, 4));
                        }
                        catch { }
                        if (file.IndexOf("vshost") == -1 && file.IndexOf("VCB-KOMOSI-") != -1)
                        {
                            if (phienban < phienban1)
                            {
                                string fl = res[i];
                                try
                                {
                                    System.IO.File.Delete(res[i]);
                                }
                                catch
                                {
                                    Thread.Sleep(1000);
                                    System.IO.File.Delete(res[i]);
                                }
                            }
                        }
                    }
                }

                if (capnhat == false)
                {
                    MessageBox.Show("Chương trình có phiên bản mới", "Thong bao");
                    strfilename = link + "\\" + tenF;
                    try
                    {
                        byte[] bt;
                        bt = fc.getFile();
                        // Open file for reading
                        System.IO.FileStream _FileStream = new System.IO.FileStream(strfilename, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        // Writes a block of bytes to this stream using data from
                        // a byte array.
                        _FileStream.Write(bt, 0, bt.Length);
                        // close file stream
                        _FileStream.Close();
                        Process.Start(strfilename);
                        return;
                    }
                    catch (Exception _Exception)
                    {
                        // Error
                        Console.WriteLine("Exception caught in process: {0}",
                                          _Exception.ToString());
                    }
                }
            }
            catch { }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmLogIn());
            //}
            //else
            //{
            //    return;
            //}

        }
        [DllImport("user32.dll")]
        public static extern int FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll")]
        public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool PostMessage(int hWnd, uint Msg, int wParam, int lParam);
    }
}
